﻿SET CONCAT_NULL_YIELDS_NULL, ANSI_NULLS, ANSI_PADDING, QUOTED_IDENTIFIER, ANSI_WARNINGS, ARITHABORT, XACT_ABORT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS OFF
GO

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO

CREATE TABLE [dbo].[PrepaidExpenseVoucher] (
  [ID] [uniqueidentifier] NOT NULL,
  [PrepaidExpenseID] [uniqueidentifier] NULL,
  [Date] [datetime] NULL,
  [No] [nvarchar](25) NULL,
  [Reason] [nvarchar](512) NULL,
  [DebitAccount] [nvarchar](25) NULL,
  [CreditAccount] [nvarchar](25) NULL,
  [Amount] [money] NULL,
  CONSTRAINT [PK__PrepaidExpenseVoucher] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[PrepaidExpenseAllocation] (
  [ID] [uniqueidentifier] NOT NULL,
  [PrepaidExpenseID] [uniqueidentifier] NULL,
  [AllocationObjectID] [uniqueidentifier] NULL,
  [AllocationObjectType] [int] NULL,
  [AllocationObjectName] [nvarchar](512) NULL,
  [AllocationRate] [decimal](25, 4) NULL,
  [CostAccount] [nvarchar](25) NULL,
  [ExpenseItemID] [uniqueidentifier] NULL,
  CONSTRAINT [PK__PrepaidExpenseAllocation] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[PrepaidExpense] (
  [ID] [uniqueidentifier] NOT NULL,
  [TypeExpense] [int] NULL,
  [PrepaidExpenseCode] [nvarchar](25) NOT NULL,
  [PrepaidExpenseName] [nvarchar](512) NULL,
  [Date] [datetime] NULL,
  [Amount] [money] NULL,
  [AllocationAmount] [money] NULL,
  [AllocationTime] [int] NULL,
  [AllocatedPeriod] [int] NULL,
  [AllocatedAmount] [money] NULL,
  [AllocationAccount] [nvarchar](25) NULL,
  [IsActive] [bit] NULL,
  CONSTRAINT [PK__PrepaidExpense] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[GOtherVoucherDetailExpenseAllocation] (
  [ID] [uniqueidentifier] NOT NULL,
  [GOtherVoucherID] [uniqueidentifier] NULL,
  [PrepaidExpenseID] [uniqueidentifier] NULL,
  [AllocationAmount] [money] NULL,
  [AllocationObjectID] [uniqueidentifier] NULL,
  [AllocationObjectType] [int] NULL,
  [AllocationRate] [decimal](25, 4) NULL,
  [Amount] [money] NULL,
  [CostAccount] [nvarchar](25) NULL,
  [ExpenseItemID] [uniqueidentifier] NULL,
  [PrepaidExpenseName] [nvarchar](512) NULL,
  [AllocationObjectName] [nvarchar](512) NULL,
  CONSTRAINT [PK__GOtherVoucherDetailExpenseAllocation] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[GOtherVoucherDetailExpense] (
  [ID] [uniqueidentifier] NOT NULL,
  [GOtherVoucherID] [uniqueidentifier] NULL,
  [PrepaidExpenseID] [uniqueidentifier] NULL,
  [Amount] [money] NULL,
  [RemainingAmount] [money] NULL,
  [AllocationAmount] [money] NULL,
  [PrepaidExpenseName] [nvarchar](512) NULL,
  CONSTRAINT [PK__GOtherVoucherDetailExpense] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturnDetail]
  ADD [IsPromotion] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAQuote]
  ADD [ContactMobile] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAQuote]
  ADD [ContactEmail] [nvarchar](100) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAQuote]
  ADD [DeliveryTime] [nvarchar](225) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAQuote]
  ADD [GuaranteeDuration] [nvarchar](225) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAQuote]
  ADD [Description] [nvarchar](2000) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoiceDetail]
  ADD [ExportTaxAccountCorresponding] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoice]
  ADD [TotalExportTaxAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SABillDetail]
  ADD [IsPromotion] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_GetBkeInv_Sale]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT
    )
AS
BEGIN 
/*
VATRate type :
-1: Không chịu thuế
0: thuê 0%
5: 5%
10: 10%
*/
if @IsSimilarBranch = 0
	 select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
	  Description as Mat_hang, (Amount-DiscountAmount) as Doanh_so, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  
	   from SAinvoice a join SAInvoiceDetail b on a.id = b.SAInvoiceID where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1 
	union all
	select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST, 
	Description as Mat_hang, (Amount-DiscountAmount) as Doanh_so, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  
	 from  SABill a join SABillDetail b on a.ID = b.SABillID  where cast(InvoiceDate As Date) between @FromDate and @ToDate
	union all
	select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
	  Description as Mat_hang,  (Amount-DiscountAmount) as Doanh_so, VATAmount as Thue_GTGT, VATAccount as TKThue,-1 flag  
	 from SAReturn a join SAReturnDetail b on a.id = b.SAReturnID
	where cast(InvoiceDate As Date) between @FromDate and @ToDate and recorded = 1 
	order by VATRate, InvoiceDate, InvoiceNo,AccountingObjectName
else
select VATRate, 
       so_hd, 
	   ngay_hd, 
	   Nguoi_mua, 
	   MST,
	   Mat_hang, 
	   sum(Doanh_so) Doanh_so, 
	   sum(Thue_GTGT) Thue_GTGT, 
	   TKThue,
	   flag
from
	(
	select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
	  a.Reason as Mat_hang, (Amount-DiscountAmount) as Doanh_so, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  
	   from SAinvoice a join SAInvoiceDetail b on a.id = b.SAInvoiceID where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1 
	union all
	select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST, 
	a.Reason as Mat_hang, (Amount-DiscountAmount) as Doanh_so, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  
	 from  SABill a join SABillDetail b on a.ID = b.SABillID  where cast(InvoiceDate As Date) between @FromDate and @ToDate
	union all
	select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
	  a.Reason as Mat_hang,  (Amount-DiscountAmount) as Doanh_so, VATAmount as Thue_GTGT, VATAccount as TKThue,-1 flag  
	 from SAReturn a join SAReturnDetail b on a.id = b.SAReturnID
	where cast(InvoiceDate As Date) between @FromDate and @ToDate and recorded = 1) aa
group by VATRate, 
       so_hd, 
	   ngay_hd, 
	   Nguoi_mua, 
	   MST,
	   Mat_hang, 
	   TKThue ,
	   flag
order by VATRate, ngay_hd, so_hd,Nguoi_mua
end;
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[RSInwardOutwardDetail]
  ADD [InvoiceTemplate] [nvarchar](50) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'RSInwardOutwardDetail', 'COLUMN', N'InvoiceTemplate'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCPaymentDetail]
  ADD [InvoiceTypeID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MCPaymentDetail]
  ADD [InvoiceTemplate] [nvarchar](30) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_PO_DiaryBook]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER = NULL ,
    @IncludeDependentBranch BIT = 0 ,
    @IsNotPaid BIT = 0
AS 
    BEGIN
        select
		 DetailID,
		 TypeID,
		 PostedDate,
		 RefDate,
		 RefNo,
		 InvoiceDate,
		 InvoiceNo,
		 Description,
		 GoodsAmount,
		 EquipmentAmount,
		 AnotherAccount,
		 AnotherAmount,
		 sum(GoodsAmount) + sum(EquipmentAmount) + sum(AnotherAmount) as PaymentableAmount
		from
		(select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description , sum(DebitAmount) as GoodsAmount, 
		0 as EquipmentAmount, null as AnotherAccount, 0 as AnotherAmount,a.TypeID
		  from GeneralLedger a join PPInvoiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate
		and a.Account =  '156' and AccountCorresponding not in ('3333', '3332') group by  a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description , sum(DebitAmount) as GoodsAmount,
		0 as EquipmentAmount, null as AnotherAccount, 0 as AnotherAmount,a.TypeID
		  from GeneralLedger a join PPServiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate
		and a.Account =  '156' and AccountCorresponding not in ('3333', '3332') group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description, sum(DebitAmount) as GoodsAmount,
		0 as EquipmentAmount, null as AnotherAccount, 0 as AnotherAmount,a.TypeID
		  from GeneralLedger a join TIIncrementDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate
		and a.Account =  '156' and AccountCorresponding not in ('3333', '3332') group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID

		union all

		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount, sum(DebitAmount) as EquipmentAmount,
		null as AnotherAccount, 0 as AnotherAmount,a.TypeID
		  from GeneralLedger a join PPInvoiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate
		and a.Account =  '152' and AccountCorresponding not in ('3333', '3332') group by  a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount, sum(DebitAmount) as EquipmentAmount,
		null as AnotherAccount, 0 as AnotherAmount,a.TypeID
		  from GeneralLedger a join PPServiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate
		and a.Account =  '152' and AccountCorresponding not in ('3333', '3332') group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description,0 as GoodsAmount , sum(DebitAmount) as EquipmentAmount,
		null as AnotherAccount, 0 as AnotherAmount,a.TypeID
		  from GeneralLedger a join TIIncrementDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate
		and a.Account =  '152' and AccountCorresponding not in ('3333', '3332') group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID
		union all

		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description,0 as GoodsAmount,0 as EquipmentAmount , a.Account as AnotherAccount, sum(DebitAmount) as AnotherAmount,a.TypeID
		  from GeneralLedger a join PPInvoiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate
		and a.Account in ('153','154','242') and AccountCorresponding not in ('3333', '3332') and AccountCorresponding in ('331', '111','112')
		group by  a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , a.Account, b.Description, a.DetailID,a.TypeID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount,0 as EquipmentAmount, a.Account as AnotherAccount, sum(DebitAmount) as AnotherAmount,a.TypeID
		  from GeneralLedger a join PPServiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate 
		  and a.Account in ('153','154','242') and AccountCorresponding not in ('3333', '3332') and AccountCorresponding in ('331', '111','112')
		group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , a.Account, b.Description, a.DetailID,a.TypeID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount,0 as EquipmentAmount, a.Account as AnotherAccount,sum(DebitAmount) as AnotherAmount,a.TypeID
		  from GeneralLedger a join TIIncrementDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate
		and a.Account in ('153','154','242') and AccountCorresponding not in ('3333', '3332') and AccountCorresponding in ('331', '111','112')
		group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , a.Account, b.Description, a.DetailID,a.TypeID) t
group by 
         DetailID,
		 TypeID,
		 PostedDate,
		 RefDate,
		 RefNo,
		 InvoiceDate,
		 InvoiceNo,
		 Description,
		 GoodsAmount,
		 EquipmentAmount,
		 AnotherAccount,
		 AnotherAmount
ORDER BY PostedDate ,
               RefDate ,RefNo           
   


           
                              
                              
                              
   
    END
    


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[FAIncrement]
  ADD [TotalAll] [money] NULL DEFAULT (NULL)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[FAIncrement]
  ADD [TotalAllOriginal] [money] NULL DEFAULT (NULL)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'FAIncrement', 'COLUMN', N'TotalAll'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'FAIncrement', 'COLUMN', N'TotalAllOriginal'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_GetBkeInv_Buy]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT
    )
AS
BEGIN

if @IsSimilarBranch = 0    
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
	 b.Description as Mat_hang, InwardAmount  as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag                  
	 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
	where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1
	union all         
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
	 b.Description as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag         
	from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
	join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
	where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1
	union all    
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
	 b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag         
	 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
	where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1 
	union all
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
	b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag																	
	from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
	where InvoiceDate between @FromDate and @ToDate and Recorded = 1
	union all
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
	b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag																
	from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
	where InvoiceDate between @FromDate and @ToDate and Recorded = 1
	union all
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
	 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag																		
	 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
	where InvoiceDate between @FromDate and @ToDate and Recorded = 1 
	union all
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
	 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag																	
	 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
	where InvoiceDate between @FromDate and @ToDate and a.Recorded = 1 
	union all
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
	 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag																		
	 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
	where InvoiceDate between @FromDate and @ToDate and a.Recorded = 1 
	union all
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
	 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag																	
	 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
	where InvoiceDate between @FromDate and @ToDate and a.Recorded = 1 
	order by c.GoodsServicePurchaseCode, InvoiceDate, InvoiceNo;
else
select aa.GoodsServicePurchaseCode,
       aa.GoodsServicePurchaseName,
	   aa.So_HD,
	   aa.Ngay_HD,
	   aa.ten_NBan,
	   aa.MST,
	   aa.Mat_hang,
	   sum(aa.GT_chuathue) GT_chuathue,
	   aa.Thue_suat ,
	   sum(aa.Thue_GTGT) Thue_GTGT,
	   aa.TK_Thue,
	   flag
from (
     select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
	 a.Reason as Mat_hang, InwardAmount  as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag         
	 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
	where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1
	union all         
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
	 a.Reason as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag         
	from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
	join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
	where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1
	union all    
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
	 a.Reason as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue ,1 flag        
	 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
	where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1 
	union all
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
	b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag																	
	from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
	where InvoiceDate between @FromDate and @ToDate and Recorded = 1
	union all
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
	b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag																
	from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
	where InvoiceDate between @FromDate and @ToDate and Recorded = 1
	union all
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
	 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag																		
	 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
	where InvoiceDate between @FromDate and @ToDate and Recorded = 1 
	union all
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
	 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag																	
	 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
	where InvoiceDate between @FromDate and @ToDate and a.Recorded = 1 
	union all
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
	 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag																		
	 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
	where InvoiceDate between @FromDate and @ToDate and a.Recorded = 1 
	union all
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
	 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag																	
	 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
	where InvoiceDate between @FromDate and @ToDate and a.Recorded = 1 
	) aa
group by GoodsServicePurchaseCode,
       GoodsServicePurchaseName,
	   So_HD,
	   Ngay_HD,
	   ten_NBan,
	   MST,
	   Mat_hang,
	   TK_Thue,
	   flag,
	   aa.Thue_suat
order by aa.GoodsServicePurchaseCode, aa.Ngay_HD, aa.So_HD
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DROP VIEW [dbo].[ViewVoucherInvisible]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
Create View [dbo].[ViewVoucherInvisible] AS
SELECT 
	newid() as ID,
	dbo.MCPayment.ID as RefID, 
	dbo.MCPayment.TypeID, 
	dbo.Type.TypeName, 
	dbo.Type.TypeGroupID,
	dbo.MCPayment.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.MCPayment.Date,
	dbo.MCPayment.PostedDate, 
	dbo.MCPayment.CurrencyID, 
	dbo.MCPayment.Reason, 
	dbo.MCPayment.EmployeeID, 
	dbo.MCPayment.BranchID, 
	dbo.MCPayment.Recorded, 
	dbo.MCPayment.TotalAmount, 
	dbo.MCPayment.TotalAmountOriginal, 
	'MCPayment' AS RefTable
FROM dbo.MCPayment INNER JOIN
	dbo.Type ON MCPayment.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.MBDeposit.ID as RefID,  
	dbo.MBDeposit.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.MBDeposit.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.MBDeposit.Date, 
	dbo.MBDeposit.PostedDate, 
	dbo.MBDeposit.CurrencyID, 
	dbo.MBDeposit.Reason, 
	dbo.MBDeposit.EmployeeID, 
	dbo.MBDeposit.BranchID, 
	dbo.MBDeposit.Recorded, 
	dbo.MBDeposit.TotalAmount, 
	dbo.MBDeposit.TotalAmountOriginal, 
	'MBDeposit' AS RefTable
FROM dbo.MBDeposit INNER JOIN
	dbo.Type ON MBDeposit.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.MBInternalTransfer.ID as RefID, 
	dbo.MBInternalTransfer.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.MBInternalTransfer.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.MBInternalTransfer.Date, 
	dbo.MBInternalTransfer.PostedDate, 
	'' AS CurrencyID,
	dbo.MBInternalTransfer.Reason,
	NULL AS EmployeeID, 
	dbo.MBInternalTransfer.BranchID, 
	dbo.MBInternalTransfer.Recorded, 
	dbo.MBInternalTransfer.TotalAmountOriginal, 
	dbo.MBInternalTransfer.TotalAmount, 
	'MBInternalTransfer' AS RefTable
FROM dbo.MBInternalTransfer INNER JOIN
	dbo.Type ON dbo.MBInternalTransfer.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.MBTellerPaper.ID as RefID, 
	dbo.MBTellerPaper.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.MBTellerPaper.No, 
	NULL AS InwardNo,
	NULL AS OutwardNo, 
	dbo.MBTellerPaper.Date, 
	dbo.MBTellerPaper.PostedDate, 
	dbo.MBTellerPaper.CurrencyID, 
	dbo.MBTellerPaper.Reason, 
	dbo.MBTellerPaper.EmployeeID, 
	dbo.MBTellerPaper.BranchID, 
	dbo.MBTellerPaper.Recorded, 
	dbo.MBTellerPaper.TotalAmountOriginal, 
	dbo.MBTellerPaper.TotalAmount, 
	'MBTellerPaper' AS RefTable
FROM dbo.MBTellerPaper INNER JOIN
	dbo.Type ON dbo.MBTellerPaper.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.MBCreditCard.ID as RefID, 
	dbo.MBCreditCard.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.MBCreditCard.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.MBCreditCard.Date, 
	dbo.MBCreditCard.PostedDate, 
	dbo.MBCreditCard.CurrencyID, 
	dbo.MBCreditCard.Reason, 
	dbo.MBCreditCard.EmployeeID, 
	dbo.MBCreditCard.BranchID, 
	dbo.MBCreditCard.Recorded, 
	dbo.MBCreditCard.TotalAmountOriginal, 
	dbo.MBCreditCard.TotalAmount, 
	'MBCreditCard' AS RefTable
FROM dbo.MBCreditCard INNER JOIN
	dbo.Type ON dbo.MBCreditCard.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.MCReceipt.ID as RefID, 
	dbo.MCReceipt.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.MCReceipt.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.MCReceipt.Date, 
	dbo.MCReceipt.PostedDate, 
	dbo.MCReceipt.CurrencyID, 
	dbo.MCReceipt.Reason, 
	dbo.MCReceipt.EmployeeID, 
	dbo.MCReceipt.BranchID, 
	dbo.MCReceipt.Recorded, 
	dbo.MCReceipt.TotalAmountOriginal, 
	dbo.MCReceipt.TotalAmount,
	'MCReceipt' AS RefTable
FROM dbo.MCReceipt INNER JOIN
	dbo.Type ON MCReceipt.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.FAAdjustment.ID as RefID,
	dbo.FAAdjustment.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.FAAdjustment.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.FAAdjustment.Date, 
	dbo.FAAdjustment.PostedDate,
	NULL AS CurrencyID, 
	dbo.FAAdjustment.Reason, 
	NULL AS EmployeeID, 
	dbo.FAAdjustment.BranchID, 
	dbo.FAAdjustment.Recorded, 
	$ 0 AS TotalAmountOriginal, 
	dbo.FAAdjustment.TotalAmount, 
	'FAAdjustment' AS RefTable
FROM dbo.FAAdjustment INNER JOIN
	dbo.Type ON FAAdjustment.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.FADepreciation.ID as RefID, 
	dbo.FADepreciation.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.FADepreciation.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.FADepreciation.Date, 
	dbo.FADepreciation.PostedDate, 
	NULL AS CurrencyID,
	dbo.FADepreciation.Reason,
	NULL AS EmployeeID, 
	dbo.FADepreciation.BranchID, 
	dbo.FADepreciation.Recorded, 
	dbo.FADepreciation.TotalAmountOriginal, 
	dbo.FADepreciation.TotalAmount,
	'FADepreciation' AS RefTable
FROM dbo.FADepreciation INNER JOIN
	dbo.Type ON FADepreciation.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.FAIncrement.ID as RefID, 
	dbo.FAIncrement.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.FAIncrement.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.FAIncrement.Date, 
	dbo.FAIncrement.PostedDate, 
	FAIncrement.CurrencyID, 
	dbo.FAIncrement.Reason, 
	dbo.FAIncrement.EmployeeID, 
	dbo.FAIncrement.BranchID, 
	dbo.FAIncrement.Recorded, 
	dbo.FAIncrement.TotalAmountOriginal, 
	dbo.FAIncrement.TotalAmount, 
	'FAIncrement' AS RefTable
FROM dbo.FAIncrement INNER JOIN
	dbo.Type ON FAIncrement.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.FADecrement.ID as RefID, 
	dbo.FADecrement.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.FADecrement.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.FADecrement.Date, 
	dbo.FADecrement.PostedDate, 
	FADecrement.CurrencyID, 
	dbo.FADecrement.Reason, 
	NULL AS EmployeeID, 
	dbo.FADecrement.BranchID, 
	dbo.FADecrement.Recorded, 
	dbo.FADecrement.TotalAmountOriginal, 
	dbo.FADecrement.TotalAmount, 
	'FADecrement' AS RefTable
FROM dbo.FADecrement INNER JOIN
	dbo.Type ON FADecrement.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.FAAudit.ID as RefID,
	dbo.FAAudit.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.FAAudit.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.FAAudit.Date, 
	dbo.FAAudit.PostedDate,
	NULL AS CurrencyID, 
	dbo.FAAudit.Description AS Reason, 
	NULL AS EmployeeID, 
	dbo.FAAudit.BranchID, 
	0 AS Recorded, 
	0 AS TotalAmountOriginal, 
	0 AS TotalAmount, 
	'FAAudit' AS RefTable
FROM dbo.FAAudit INNER JOIN
	dbo.Type ON FAAudit.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.FATransfer.ID as RefID,
	dbo.FATransfer.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.FATransfer.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.FATransfer.Date, 
	dbo.FATransfer.Date AS PostedDate,
	NULL AS CurrencyID, 
	dbo.FATransfer.Reason, 
	NULL AS EmployeeID, 
	dbo.FATransfer.BranchID, 
	dbo.FATransfer.Recorded, 
	0 AS TotalAmountOriginal, 
	0 AS TotalAmount, 
	'FATransfer' AS RefTable
FROM dbo.FATransfer INNER JOIN
	dbo.Type ON FATransfer.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT 
	newid() as ID,
	dbo.GOtherVoucher.ID as RefID, 
	dbo.GOtherVoucher.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.GOtherVoucher.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.GOtherVoucher.Date, 
	dbo.GOtherVoucher.PostedDate, 
	NULL AS CurrencyID, 
	dbo.GOtherVoucher.Reason, 
	NULL AS EmployeeID, 
	dbo.GOtherVoucher.BranchID, 
	dbo.GOtherVoucher.Recorded, 
	dbo.GOtherVoucher.TotalAmountOriginal, 
	dbo.GOtherVoucher.TotalAmount, 
	'GOtherVoucher' AS RefTable
FROM dbo.GOtherVoucher INNER JOIN
	dbo.Type ON GOtherVoucher.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.RSInwardOutward.ID as RefID, 
	dbo.RSInwardOutward.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.RSInwardOutward.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.RSInwardOutward.Date, 
	dbo.RSInwardOutward.PostedDate, 
	RSInwardOutward.CurrencyID, 
	dbo.RSInwardOutward.Reason, 
	NULL AS EmployeeID, 
	dbo.RSInwardOutward.BranchID, 
	dbo.RSInwardOutward.Recorded, 
	dbo.RSInwardOutward.TotalAmountOriginal, 
	dbo.RSInwardOutward.TotalAmount, 
	'RSInwardOutward' AS RefTable
FROM dbo.RSInwardOutward INNER JOIN
	dbo.Type ON RSInwardOutward.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.RSTransfer.ID as RefID, 
	dbo.RSTransfer.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.RSTransfer.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.RSTransfer.Date, 
	dbo.RSTransfer.PostedDate, 
	RSTransfer.CurrencyID, 
	dbo.RSTransfer.Reason, 
	NULL AS EmployeeID, 
	dbo.RSTransfer.BranchID,
	dbo.RSTransfer.Recorded, 
	dbo.RSTransfer.TotalAmountOriginal, 
	dbo.RSTransfer.TotalAmount, 
	'RSTransfer' AS RefTable
FROM dbo.RSTransfer INNER JOIN
	dbo.Type ON RSTransfer.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.PPInvoice.ID as RefID, 
	dbo.PPInvoice.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.PPInvoice.No, 
	dbo.PPInvoice.InwardNo AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.PPInvoice.Date, 
	dbo.PPInvoice.PostedDate, 
	PPInvoice.CurrencyID,
	dbo.PPInvoice.Reason, 
	PPInvoice.EmployeeID, 
	dbo.PPInvoice.BranchID, 
	dbo.PPInvoice.Recorded, 
	dbo.PPInvoice.TotalAmountOriginal, 
	dbo.PPInvoice.TotalAmount,
	'PPInvoice' AS RefTable
FROM dbo.PPInvoice INNER JOIN
	dbo.Type ON PPInvoice.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.PPDiscountReturn.ID as RefID, 
	dbo.PPDiscountReturn.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.PPDiscountReturn.No, 
	NULL AS InwardNo, 
	dbo.PPDiscountReturn.OutwardNo AS OutwardNo, 
	dbo.PPDiscountReturn.Date, 
	dbo.PPDiscountReturn.PostedDate, 
	PPDiscountReturn.CurrencyID, 
	dbo.PPDiscountReturn.Reason, 
	PPDiscountReturn.EmployeeID, 
	dbo.PPDiscountReturn.BrachID AS BranchID, 
	dbo.PPDiscountReturn.Recorded, 
	dbo.PPDiscountReturn.TotalAmountOriginal, 
	dbo.PPDiscountReturn.TotalAmount, 
	'PPDiscountReturn' AS RefTable
FROM dbo.PPDiscountReturn INNER JOIN
	dbo.Type ON PPDiscountReturn.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.PPService.ID as RefID, 
	dbo.PPService.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.PPService.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.PPService.Date, 
	dbo.PPService.PostedDate, 
	PPService.CurrencyID, 
	dbo.PPService.Reason, 
	PPService.EmployeeID, 
	dbo.PPService.BranchID, 
	dbo.PPService.Recorded, 
	dbo.PPService.TotalAmountOriginal, 
	dbo.PPService.TotalAmount, 
	'PPService' AS RefTable
FROM dbo.PPService INNER JOIN
	dbo.Type ON PPService.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.PPOrder.ID as RefID, 
	dbo.PPOrder.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.PPOrder.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.PPOrder.Date, 
	dbo.PPOrder.DeliverDate AS PostedDate, 
	dbo.PPOrder.CurrencyID, 
	dbo.PPOrder.Reason, 
	dbo.PPOrder.EmployeeID, 
	dbo.PPOrder.BranchID, 
	dbo.PPOrder.Exported AS Recorded, 
	dbo.PPOrder.TotalAmountOriginal, 
	dbo.PPOrder.TotalAmount, 
	'PPOrder' AS RefTable
FROM dbo.PPOrder INNER JOIN 
	dbo.Type ON PPOrder.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.SAInvoice.ID as RefID, 
	dbo.SAInvoice.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.SAInvoice.No, 
	NULL AS InwardNo, 
	dbo.SAInvoice.OutwardNo AS OutwardNo, 
	dbo.SAInvoice.Date, 
	dbo.SAInvoice.PostedDate, 
	SAInvoice.CurrencyID,
	dbo.SAInvoice.Reason, 
	SAInvoice.EmployeeID, 
	dbo.SAInvoice.BranchID, 
	dbo.SAInvoice.Recorded, 
	dbo.SAInvoice.TotalAmountOriginal, 
	dbo.SAInvoice.TotalAmount, 
	'SAInvoice' AS RefTable
FROM dbo.SAInvoice INNER JOIN
	dbo.Type ON dbo.SAInvoice.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.SAReturn.ID as RefID, 
	dbo.SAReturn.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.SAReturn.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.SAReturn.Date, 
	dbo.SAReturn.PostedDate, 
	SAReturn.CurrencyID, 
	dbo.SAReturn.Reason, 
	SAReturn.EmployeeID, 
	dbo.SAReturn.BranchID, 
	SAReturn.Recorded AS Posted, 
	dbo.SAReturn.TotalAmountOriginal, 
	dbo.SAReturn.TotalAmount, 
	'SAReturn' AS RefTable
FROM dbo.SAReturn INNER JOIN
	dbo.Type ON dbo.SAReturn.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.SAOrder.ID as RefID, 
	dbo.SAOrder.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.SAOrder.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.SAOrder.Date, 
	dbo.SAOrder.DeliveDate AS PostedDate,
	SAOrder.CurrencyID, 
	dbo.SAOrder.Reason, 
	SAOrder.EmployeeID, 
	dbo.SAOrder.BranchID,
	SAOrder.Exported AS Recorded, 
	dbo.SAOrder.TotalAmountOriginal, 
	dbo.SAOrder.TotalAmount, 
	'SAOrder' AS RefTable
FROM dbo.SAOrder INNER JOIN
	dbo.Type ON dbo.SAOrder.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.SAQuote.ID as RefID,
	dbo.SAQuote.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.SAQuote.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.SAQuote.Date, 
	dbo.SAQuote.Date AS PostedDate,
	dbo.SAQuote.CurrencyID, 
	dbo.SAQuote.Reason, 
	dbo.SAQuote.EmployeeID, 
	dbo.SAQuote.BranchID, 
	0 AS Recorded, 
	dbo.SAQuote.TotalAmountOriginal, 
	dbo.SAQuote.TotalAmount, 
	'SAQuote' AS RefTable
FROM dbo.SAQuote INNER JOIN
	dbo.Type ON SAQuote.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.TIAdjustment.ID as RefID,
	dbo.TIAdjustment.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.TIAdjustment.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.TIAdjustment.Date, 
	dbo.TIAdjustment.PostedDate,
	NULL AS CurrencyID, 
	dbo.TIAdjustment.Reason, 
	NULL AS EmployeeID, 
	dbo.TIAdjustment.BranchID, 
	dbo.TIAdjustment.Recorded, 
	$ 0 AS TotalAmountOriginal, 
	dbo.TIAdjustment.TotalAmount, 
	'TIAdjustment' AS RefTable
FROM dbo.TIAdjustment INNER JOIN
	dbo.Type ON TIAdjustment.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.TIAllocation.ID as RefID, 
	dbo.TIAllocation.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.TIAllocation.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.TIAllocation.Date, 
	dbo.TIAllocation.PostedDate, 
	NULL AS CurrencyID,
	dbo.TIAllocation.Reason,
	NULL AS EmployeeID, 
	dbo.TIAllocation.BranchID, 
	dbo.TIAllocation.Recorded, 
	dbo.TIAllocation.TotalAmountOriginal, 
	dbo.TIAllocation.TotalAmount,
	'TIAllocation' AS RefTable
FROM dbo.TIAllocation INNER JOIN
	dbo.Type ON TIAllocation.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.TIIncrement.ID as RefID, 
	dbo.TIIncrement.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.TIIncrement.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.TIIncrement.Date, 
	dbo.TIIncrement.PostedDate, 
	dbo.TIIncrement.CurrencyID, 
	dbo.TIIncrement.Reason, 
	dbo.TIIncrement.EmployeeID, 
	dbo.TIIncrement.BranchID, 
	dbo.TIIncrement.Recorded, 
	dbo.TIIncrement.TotalAmountOriginal, 
	dbo.TIIncrement.TotalAmount, 
	'TIIncrement' AS RefTable
FROM dbo.TIIncrement INNER JOIN
	dbo.Type ON TIIncrement.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.TIDecrement.ID as RefID, 
	dbo.TIDecrement.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.TIDecrement.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.TIDecrement.Date, 
	dbo.TIDecrement.PostedDate, 
	NULL AS CurrencyID, 
	dbo.TIDecrement.Reason, 
	NULL AS EmployeeID, 
	dbo.TIDecrement.BranchID, 
	dbo.TIDecrement.Recorded, 
	0 AS TotalAmountOriginal, 
	dbo.TIDecrement.TotalAmount, 
	'TIDecrement' AS RefTable
FROM dbo.TIDecrement INNER JOIN
	dbo.Type ON TIDecrement.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.TIAudit.ID as RefID, 
	dbo.TIAudit.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.TIAudit.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.TIAudit.Date, 
	dbo.TIAudit.PostedDate, 
	NULL AS CurrencyID, 
	NULL AS Reason, 
	NULL AS EmployeeID, 
	dbo.TIAudit.BranchID, 
	0 AS Recorded, 
	0 AS TotalAmountOriginal, 
	0 AS TotalAmount, 
	'TIAudit' AS RefTable
FROM dbo.TIAudit INNER JOIN
	dbo.Type ON TIAudit.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.TITransfer.ID as RefID,
	dbo.TITransfer.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.TITransfer.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.TITransfer.Date, 
	dbo.TITransfer.PostedDate,
	NULL AS CurrencyID, 
	dbo.TITransfer.Reason, 
	NULL AS EmployeeID, 
	dbo.TITransfer.BranchID, 
	dbo.TITransfer.Recorded, 
	0 AS TotalAmountOriginal, 
	0 AS TotalAmount, 
	'TITransfer' AS RefTable
FROM dbo.TITransfer INNER JOIN
	dbo.Type ON TITransfer.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.RSAssemblyDismantlement_new.ID as RefID, 
	dbo.RSAssemblyDismantlement_new.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.RSAssemblyDismantlement_new.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.RSAssemblyDismantlement_new.Date, 
	dbo.RSAssemblyDismantlement_new.Date AS PostedDate, 
	dbo.RSAssemblyDismantlement_new.CurrencyID, 
	dbo.RSAssemblyDismantlement_new.Reason, 
	NULL AS EmployeeID, 
	dbo.RSAssemblyDismantlement_new.BranchID, 
	dbo.RSAssemblyDismantlement_new.Recorded, 
	dbo.RSAssemblyDismantlement_new.AmountOriginal AS TotalAmountOriginal, 
	dbo.RSAssemblyDismantlement_new.Amount AS TotalAmount, 
	'RSAssemblyDismantlement_new' AS RefTable
FROM dbo.RSAssemblyDismantlement_new INNER JOIN
	dbo.Type ON RSAssemblyDismantlement_new.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.EMContract.ID as RefID, 
	dbo.EMContract.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.EMContract.Code AS No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.EMContract.SignedDate AS Date, 
	dbo.EMContract.SignedDate AS PostedDate, 
	dbo.EMContract.CurrencyID, 
	dbo.EMContract.Reason, 
	NULL AS EmployeeID, 
	dbo.EMContract.BranchID, 
	0 as Recored, 
	dbo.EMContract.AmountOriginal AS TotalAmountOriginal, 
	dbo.EMContract.Amount AS TotalAmount, 
	'EMContract' AS RefTable
FROM dbo.EMContract INNER JOIN
	dbo.Type ON EMContract.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT 
	newid() as ID,
	dbo.MCAudit.ID as RefID, 
	dbo.MCAudit.TypeID, 
	dbo.Type.TypeName, 
	dbo.Type.TypeGroupID,
	dbo.MCAudit.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.MCAudit.Date,
	dbo.MCAudit.AuditDate AS PostedDate, 
	dbo.MCAudit.CurrencyID, 
	dbo.MCAudit.Description AS Reason, 
	Null AS EmployeeID, 
	dbo.MCAudit.BranchID, 
	0 AS Recorded, 
	dbo.MCAudit.TotalAuditAmount, 
	dbo.MCAudit.TotalAuditAmount AS TotalAmountOriginal, 
	'MCAudit' AS RefTable
FROM dbo.MCAudit INNER JOIN
	dbo.Type ON MCAudit.TypeID = dbo.Type.ID 
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*

*/
ALTER PROCEDURE [dbo].[Proc_BA_GetOverBalanceBook]
    @BranchID UNIQUEIDENTIFIER = NULL ,
    @AccountNumber NVARCHAR(25) = NULL ,
    @BankAccountID UNIQUEIDENTIFIER = NULL ,
    @CurrencyID NVARCHAR(3) = NULL ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @IsWorkingWithManagementBook BIT ,
    @IncludeDependentBranch BIT
AS 
    SET NOCOUNT ON		

    DECLARE @AccountNumberPercent NVARCHAR(26)
    SET @AccountNumberPercent = @AccountNumber + '%'
    SELECT  ROW_NUMBER() OVER ( ORDER BY B.BankName, BA.BankAccount ) AS RowNum ,
            BA.ID,
            BA.BankAccount,
            B.BankName,
			BA.BankBranchName ,
            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal- GL.CreditAmountOriginal ELSE 0.0 END) AS OpenAmountOC ,
            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0.0 END) AS OpenAmount ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.DebitAmountOriginal ELSE 0.0 END) AS DebitAmountOC ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN  GL.DebitAmount ELSE 0.0 END) AS DebitAmount ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmountOriginal ELSE 0.0 END) AS CreditAmountOC ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmount ELSE 0.0 END) AS CreditAmount ,
            SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) AS CloseAmountOC ,
            SUM(GL.DebitAmount - GL.CreditAmount) AS CloseAmount
    FROM    dbo.GeneralLedger GL
            INNER JOIN dbo.BankAccountDetail BA ON GL.BankAccountDetailID = BA.ID          
            INNER JOIN dbo.Bank B ON BA.BankID =  B.ID
    WHERE   GL.PostedDate <= @ToDate
            AND ( GL.Account LIKE @AccountNumberPercent )
            AND ( GL.BankAccountDetailID = @BankAccountID
                  OR @BankAccountID IS NULL
                )
            AND ( @CurrencyID IS NULL
                  OR GL.CurrencyID = @CurrencyID
                )
            
    GROUP BY BA.ID, 
			BA.BankAccount ,
            B.BankName,BA.BankBranchName
    HAVING
			SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal- GL.CreditAmountOriginal ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.DebitAmountOriginal ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN  GL.DebitAmount ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmountOriginal ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmount ELSE 0.0 END)<>0 OR 
            SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal)<>0 OR 
            SUM(GL.DebitAmount - GL.CreditAmount)<>0
            
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_BA_GetBookDepositListDetail_Bck]
    @BranchID UNIQUEIDENTIFIER = NULL ,
    @AccountNumber NVARCHAR(25) = NULL ,
    @BankAccountID UNIQUEIDENTIFIER = NULL ,
    @CurrencyID NVARCHAR(3) = NULL ,
    @FromDate DATETIME ,
    @ToDate DATETIME 
AS 
    SET NOCOUNT ON		

    DECLARE @AccountNumberPercent NVARCHAR(26)
    SET @AccountNumberPercent = @AccountNumber + '%'
    SELECT  ROW_NUMBER() OVER ( ORDER BY B.BankName, BA.BankAccount ) AS RowNum ,
            BA.ID,
            BA.BankAccount,
            B.BankName,
            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal- GL.CreditAmountOriginal ELSE 0.0 END) AS OpenAmountOC ,
            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0.0 END) AS OpenAmount ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.DebitAmountOriginal ELSE 0.0 END) AS DebitAmountOC ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN  GL.DebitAmount ELSE 0.0 END) AS DebitAmount ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmountOriginal ELSE 0.0 END) AS CreditAmountOC ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmount ELSE 0.0 END) AS CreditAmount ,
            SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) AS CloseAmountOC ,
            SUM(GL.DebitAmount - GL.CreditAmount) AS CloseAmount
    FROM    dbo.GeneralLedger GL
            INNER JOIN dbo.BankAccountDetail BA ON GL.BankAccountDetailID = BA.ID          
            INNER JOIN dbo.Bank B ON BA.BankID =  B.ID
    WHERE   GL.PostedDate <= @ToDate
            AND ( GL.Account LIKE @AccountNumberPercent )
            AND ( GL.BankAccountDetailID = @BankAccountID
                  OR @BankAccountID IS NULL
                )
            AND ( @CurrencyID IS NULL
                  OR GL.CurrencyID = @CurrencyID
                )
            
    GROUP BY BA.ID, 
			BA.BankAccount ,
            B.BankName
    HAVING
			SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal- GL.CreditAmountOriginal ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.DebitAmountOriginal ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN  GL.DebitAmount ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmountOriginal ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmount ELSE 0.0 END)<>0 OR 
            SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal)<>0 OR 
            SUM(GL.DebitAmount - GL.CreditAmount)<>0
            
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE PROCEDURE [dbo].[Proc_GetAccreditative1]
    @BankCode nvarchar(25)
AS

    BEGIN
        SET NOCOUNT ON;
   		                         
		select distinct a.ID, a.No, a.Date, a.BankName, a.AccountingObjectAddress, h.BankAccount as AccountingObjectBankAccount, a.AccountingObjectBankName,
		a.AccountingObjectName, c.BankAccount, c.BankBranchName, d.Tel, e.ID as CurrencyName, Sum(a.TotalAllOriginal) as TotalAmount, a.Reason, h.BankBranchName as Branch, f.BankCode
		from TIIncrement a left join TIIncrementDetail b on a.ID = b.TIIncrementID
		left join BankAccountDetail c on a.BankAccountDetailID = c.ID 
		left join AccountingObject d on a.AccountingObjectID = d.ID
		left join Currency e on a.CurrencyID = e.ID 		
		left join Bank f on c.BankID = f.ID
		left join AccountingObjectBankAccount h on a.AccountingObjectBankAccount = h.ID
		where f.BankCode = @BankCode
		group by a.ID, a.No, a.Date, a.BankName, a.AccountingObjectAddress, h.BankAccount, h.BankBranchName, a.AccountingObjectBankName,
		a.AccountingObjectName, c.BankAccount, c.BankBranchName, d.Tel, e.ID, a.Reason, f.BankCode
    END 
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE PROCEDURE [dbo].[Proc_GetAccreditative]
    @BankCode nvarchar(25)
AS

    BEGIN
        SET NOCOUNT ON;
   		                         
		select distinct a.ID, a.No, a.Date, a.BankName, a.AccountingObjectAddress, h.BankAccount as AccountingObjectBankAccount, a.AccountingObjectBankName,
		a.AccountingObjectName, c.BankAccount, c.BankBranchName, d.Tel, e.ID as CurrencyName, Sum(a.TotalAllOriginal) as TotalAmount, a.Reason, h.BankBranchName as Branch, f.BankCode
		from FAIncrement a left join FAIncrementDetail b on a.ID = b.FAIncrementID
		left join BankAccountDetail c on a.BankAccountDetailID = c.ID 
		left join AccountingObject d on a.AccountingObjectID = d.ID
		left join Currency e on a.CurrencyID = e.ID 		
		left join Bank f on c.BankID = f.ID
		left join AccountingObjectBankAccount h on a.AccountingObjectBankAccount = h.ID
		where f.BankCode = @BankCode
		group by a.ID, a.No, a.Date, a.BankName, a.AccountingObjectAddress, h.BankAccount, h.BankBranchName, a.AccountingObjectBankName,
		a.AccountingObjectName, c.BankAccount, c.BankBranchName, d.Tel, e.ID, a.Reason, f.BankCode
    END 
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*

*/
ALTER PROCEDURE [dbo].[Proc_SA_GetSalesDiaryBook]
    @FromDate DATETIME ,
    @ToDate DATETIME,
    @IsDisplayNotReceiptOnly BIT
AS 
    BEGIN 
		
        DECLARE @listRefType NVARCHAR(MAX)
        IF @IsDisplayNotReceiptOnly = 1 
            SET @listRefType = '3530,3532,3534,3536,'  
        ELSE 
            SET @listRefType = '3530,3531,3532,3534,3535,3536,3537,3538,3540,3541,3542,3543,3544,3545,3550,3551,3552,3553,3554,3555,'

              select ROW_NUMBER() OVER ( ORDER BY a.PostedDate, a.Date, a.No ) AS RowNum ,
			         a.Date as ngay_CTU, 
			         a.PostedDate as ngay_HT, 
					 a.No as So_CTU,
					 a.InvoiceDate as Ngay_HD, 
					 a.InvoiceNo as SO_HD, 
					 d.reason as Dien_giai,
					 SUM(CASE WHEN a.Account IN ( '5111' ) THEN a.CreditAmount
                         ELSE $0
                    END) AS TurnOverAmountInv ,
                    SUM(CASE WHEN a.Account IN ( '5112' ) THEN a.CreditAmount
                         ELSE $0
                    END) AS TurnOverAmountFinishedInv ,
                SUM(CASE WHEN a.Account IN ( '5113' ) THEN a.CreditAmount
                         ELSE $0
                    END) AS TurnOverAmountServiceInv ,
                SUM(CASE WHEN a.Account IN ( '5118' ) THEN a.CreditAmount
                         ELSE $0
                    END)AS TurnOverAmountOther,
                SUM(CASE WHEN a.Account IN ( '5111' ) THEN a.DebitAmount
                         ELSE $0
                    END) AS DiscountAmount ,
				SUM(CASE WHEN (a.typeID = '330' and a.Account IN ( '5111' )) THEN a.DebitAmount
                         ELSE $0
                    END) AS ReturnAmount,
                SUM(CASE WHEN (a.typeID = '340' and a.Account IN ( '5111' )) THEN a.DebitAmount
                         ELSE $0
                    END) AS ReduceAmount,
			    SUM(b.VATAmount) AS VATAmount,
				ao.AccountingObjectCode as CustomerCode, 
				ao.AccountingObjectName as CustomerName 
				INTO    #Result
				from GeneralLedger a 
				join SAInvoiceDetail b on a.DetailID = b.ID 
				join MaterialGoods c on b.MaterialGoodsID = c.ID
				join SAInvoice d on a.ReferenceID = d.ID 
				join Type e on a.TypeID = e.ID
				join AccountingObject ao on a.AccountingObjectID = ao.ID 
				where a.PostedDate between  @FromDate and @ToDate
				group by a.Date,  a.PostedDate, a.No ,a.InvoiceDate , a.InvoiceNo , d.reason,
				ao.AccountingObjectCode,ao.AccountingObjectName;

       select	RowNum ,
				ngay_CTU, 
				ngay_HT, 
				So_CTU,
				Ngay_HD, 
				SO_HD, 
				Dien_giai,
				TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther as SumTurnOver,
				TurnOverAmountInv ,
				TurnOverAmountFinishedInv ,
                TurnOverAmountServiceInv ,
                TurnOverAmountOther,
                DiscountAmount ,
				ReturnAmount,
                ReduceAmount,
				(TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther) - (DiscountAmount + ReturnAmount + ReduceAmount) as TurnOverPure,
				CustomerCode, 
				CustomerName 
   from #Result 
   where TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther <> 0
   ORDER BY RowNum   
    
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

ALTER PROCEDURE [dbo].[Proc_GL_GeneralDiaryBook_S03a]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @GroupTheSameItem BIT ,/*cộng gộp các bút toán giống nhau*/
    @IsShowAccumAmount BIT
AS 
    BEGIN
    
       DECLARE @PrevFromDate AS DATETIME
       SET @PrevFromDate = DATEADD(MILLISECOND, -10, @FromDate)
       DECLARE @StartDate DATETIME
        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'

        IF @GroupTheSameItem = 1 
            BEGIN
			select * from
            (SELECT -1 AS RowNum,
				CAST(0 AS BIT) AS IsSummaryRow,
				CAST (1 AS BIT) AS IsBold ,
                NULL AS [RefID],
				NULL AS RefType,
				NULL AS PostedDate,
				NULL AS RefDate,
              NULL AS [RefNo],
              NULL AS RefNo1 ,
              NULL AS RefOrder ,
              NULL AS [InvDate]  ,
              NULL AS [InvNo]  ,
              N'Số lũy kế kỳ trước chuyển sang' AS [JournalMemo] ,
              /*15.09.2017 bổ sung trường diễn giải thông tin chung cr 137228*/              
              '' AS JournalMemoMaster ,
              NULL AS [AccountNumber],
              NULL AS [CorrespondingAccountNumber],
              NULL AS [RefTypeName],
              NULL AS AccountObjectCode,
              NULL AS AccountObjectName ,
              NULL AS EmployeeCode ,
              NULL AS EmployeeName ,
              NULL AS ExpenseItemCode ,
              NULL AS ExpenseItemName ,
              NULL AS JobCode ,
              NULL AS JobName ,
              NULL AS ProjectWorkCode ,
              NULL AS ProjectWorkName,
              NULL AS OrderNo ,
              NULL AS [PUContractCode]  ,
              NULL AS [ContractCode]  ,
              NULL AS ListItemCode ,
              NULL AS ListItemName,
              SUM(GL.DebitAmount) AS DebitAmount ,
              SUM(GL.CreditAmount) AS CreditAmount,
              NULL AS BranchName,
              NULL AS OrganizationUnitCode,
              NULL AS OrganizationUnitName,
              NULL AS MasterCustomField1,
              NULL AS MasterCustomField2,
              NULL AS MasterCustomField3,
              NULL AS MasterCustomField4 ,
              NULL AS MasterCustomField5 ,
              NULL AS MasterCustomField6 ,
              NULL AS MasterCustomField7 ,
              NULL AS MasterCustomField8 ,
              NULL AS MasterCustomField9 ,
              NULL AS MasterCustomField10,
              NULL AS CustomField1 ,
              NULL AS CustomField2 ,
              NULL AS CustomField3 ,
              NULL AS CustomField4 ,
              NULL AS CustomField5 ,
              NULL AS CustomField6 ,
              NULL AS CustomField7 ,
              NULL AS CustomField8 ,
              NULL AS CustomField9 ,
              NULL AS CustomField10,
              NULL AS UnResonableCost ,
              NULL AS Sort,
              NULL AS RefIDSort	,
              NULL AS SortOrder 
              ,NULL AS DetailPostOrder
			  ,null as OrderPriority 
		FROM    dbo.GeneralLedger GL
        WHERE	
			(@IsShowAccumAmount = 1	AND @StartDate < @FromDate)
                AND GL.PostedDate BETWEEN @StartDate
                                    AND     @PrevFromDate
                AND GL.AccountCorresponding IS NOT NULL
                AND GL.AccountCorresponding <> ''
        HAVING  SUM(GL.[DebitAmount]) <> 0
                OR SUM(GL.[CreditAmount]) <> 0

		UNION ALL
        SELECT  ROW_NUMBER() OVER ( ORDER BY gl.PostedDate, gl.RefDate
						 		,CASE WHEN GL.TypeID = 4012 THEN 1 ELSE 0 END
								, GL.RefNo , GL.TypeID ) AS RowNum ,
                                CAST (1 AS BIT) AS IsSummaryRow ,
						CAST(0 AS BIT) AS IsBold ,
                        GL.[ReferenceID] ,
                        GL.[TypeID] ,
                        GL.[PostedDate] ,
                        GL.[RefDate] ,
                        GL.No,
                        GL.No ,
                        NULL AS RefOrder ,
                        GL.[InvoiceDate] ,
                        GL.[InvoiceNo] ,
                        GL.[Description] AS JournalMemo,
                        /*bổ sung trường diễn giải thông tin chung cr 137228*/              
						GL.Description AS JournalMemoMaster ,
                        GL.[Account] ,
                        GL.[AccountCorresponding] ,
						'' AS [RefTypeName],
                        '' AS AccountObjectCode ,
                        '' AS AccountObjectName ,
                        '' AS EmployeeCode ,
                        '' AS EmployeeName ,
                        '' AS ExpenseItemCode ,
                        '' AS ExpenseItemName ,
                        '' AS JobCode ,
                        '' AS JobName ,
                        '' AS ProjectWorkCode ,
                        '' AS ProjectWorkName ,
                        '' AS OrderNo ,
                        '' AS [PUContractCode] ,
                        '' AS [ContractCode] ,
                        '' AS ListItemCode ,
                        '' AS ListItemName ,
                        SUM(GL.[DebitAmount]) AS DebitAmount ,
                        SUM(GL.[CreditAmount]) AS CreditAmount ,
                        '' as BranchName,
                        '' AS OrganizationUnitCode ,
                        '' AS OrganizationUnitName ,
                        '' AS MasterCustomField1 ,
                        '' AS MasterCustomField2 ,
                        '' AS MasterCustomField3 ,
                        '' AS MasterCustomField4 ,
                        '' AS MasterCustomField5 ,
                        '' AS MasterCustomField6 ,
                        '' AS MasterCustomField7 ,
                        '' AS MasterCustomField8 ,
                        '' AS MasterCustomField9 ,
                        '' AS MasterCustomField10 ,
                        '' AS CustomField1 ,
                        '' AS CustomField2 ,
                        '' AS CustomField3 ,
                        '' AS CustomField4 ,
                        '' AS CustomField5 ,
                        '' AS CustomField6 ,
                        '' AS CustomField7 ,
                        '' AS CustomField8 ,
                        '' AS CustomField9 ,
                        '' AS CustomField10 ,
                        '' AS UnResonableCost ,
                        CASE WHEN SUM(DebitAmount) <> 0
                                THEN Gl.Account
                                    + AccountCorresponding
                                WHEN SUM(CreditAmount) <> 0
                                THEN gl.AccountCorresponding
                                    + GL.Account
                        END AS Sort ,
                        GL.ReferenceID
                        AS RefIDSort,
                        0 AS SortOrder,
                        0 AS DetailPostOrder,
						OrderPriority
                                            
                FROM      [dbo].[GeneralLedger] GL
                        LEFT JOIN dbo.AccountingObject ao ON ao.ID = GL.AccountingObjectID
                WHERE     gl.PostedDate BETWEEN @FromDate AND @ToDate
                        AND ISNULL(gl.AccountCorresponding,'') <> ''
                GROUP BY  GL.[ReferenceID] ,
                        GL.[TypeID] ,
                        GL.[PostedDate] ,
                        GL.[RefDate] ,
                        GL.[RefNo] ,
                        GL.No ,
                        GL.[InvoiceDate] ,
                        GL.[InvoiceNo] ,
                        GL.[Description] ,
                        GL.[Account] ,
                        GL.[AccountCorresponding] ,
						gl.OrderPriority
                                            
                HAVING    SUM(GL.[DebitAmount]) <> 0
                        OR SUM(GL.[CreditAmount]) <> 0
            ) t
			order by PostedDate,OrderPriority
			END
        ELSE 
            BEGIN
select * from
            (SELECT -1 AS RowNum,
				CAST(0 AS BIT) AS IsSummaryRow,
				CAST (1 AS BIT) AS IsBold ,
                NULL AS [RefID],
				NULL AS RefType,
				NULL AS PostedDate,
				NULL AS RefDate,
              NULL AS [RefNo],
              NULL AS RefNo1 ,
              NULL AS RefOrder ,
              NULL AS [InvDate]  ,
              NULL AS [InvNo]  ,
              N'Số lũy kế kỳ trước chuyển sang' AS [JournalMemo] ,
              /*bổ sung trường diễn giải thông tin chung cr 137228*/              
			 '' AS JournalMemoMaster ,
              NULL AS [AccountNumber],
              NULL AS [CorrespondingAccountNumber],
              NULL AS [RefTypeName],
              NULL AS AccountObjectCode,
              NULL AS AccountObjectName ,
              NULL AS EmployeeCode ,
              NULL AS EmployeeName ,
              NULL AS ExpenseItemCode ,
              NULL AS ExpenseItemName ,
              NULL AS JobCode ,
              NULL AS JobName ,
              NULL AS ProjectWorkCode ,
              NULL AS ProjectWorkName,
              NULL AS OrderNo ,
              NULL AS [PUContractCode]  ,
              NULL AS [ContractCode]  ,
              NULL AS ListItemCode ,
              NULL AS ListItemName,
              SUM(GL.DebitAmount) AS DebitAmount ,
              SUM(GL.CreditAmount) AS CreditAmount,
              NULL AS BranchName,
              NULL AS OrganizationUnitCode,
              NULL AS OrganizationUnitName,
              NULL AS MasterCustomField1,
              NULL AS MasterCustomField2,
              NULL AS MasterCustomField3,
              NULL AS MasterCustomField4 ,
              NULL AS MasterCustomField5 ,
              NULL AS MasterCustomField6 ,
              NULL AS MasterCustomField7 ,
              NULL AS MasterCustomField8 ,
              NULL AS MasterCustomField9 ,
              NULL AS MasterCustomField10,
              NULL AS CustomField1 ,
              NULL AS CustomField2 ,
              NULL AS CustomField3 ,
              NULL AS CustomField4 ,
              NULL AS CustomField5 ,
              NULL AS CustomField6 ,
              NULL AS CustomField7 ,
              NULL AS CustomField8 ,
              NULL AS CustomField9 ,
              NULL AS CustomField10,
              NULL AS UnResonableCost ,
              NULL AS Sort,
              NULL AS RefIDSort	,
              NULL AS SortOrder 
              ,NULL AS DetailPostOrder
			  , null as OrderPriority 
		FROM    dbo.GeneralLedger GL
        WHERE	
			(@IsShowAccumAmount = 1	AND @StartDate < @FromDate)
                AND GL.PostedDate BETWEEN @StartDate
                                    AND     @PrevFromDate
                AND GL.AccountCorresponding IS NOT NULL
                AND GL.AccountCorresponding <> ''
        HAVING  SUM(GL.[DebitAmount]) <> 0
                OR SUM(GL.[CreditAmount]) <> 0

		UNION ALL
        SELECT  
                            ROW_NUMBER() OVER ( ORDER BY gl.PostedDate, gl.RefDate
						 		,CASE WHEN GL.TypeID = 4012 THEN 1 ELSE 0 END
								, GL.RefNo , GL.TypeID ) AS RowNum ,							
							CAST (1 AS BIT) AS IsSummaryRow ,
							CAST(0 AS BIT) AS IsBold ,
                            GL.[ReferenceID] ,
							GL.[TypeID] ,
                            GL.[PostedDate] ,
                            GL.[RefDate] ,
                            GL.No,
                            GL.No ,
							'' AS RefOrder ,
                            GL.[InvoiceDate] ,
                            GL.[InvoiceNo] ,
                            GL.Description AS [JournalMemo] ,
                            /* bổ sung trường diễn giải thông tin chung cr 137228*/              
							GL.Reason AS JournalMemoMaster ,
                            GL.[Account] ,
                            GL.[AccountCorresponding] ,
                            '' as RefTypeName,
                            ao.AccountingObjectCode ,
                            ao.AccountingObjectName ,
                            '' as EmployeeCode,
                            '' as EmployeeName,
                            '' as ExpenseItemCode ,
                            '' as ExpenseItemName ,
                            '' as JobCode,
                            '' as JobName,
                            '' as ProjectWorkCode ,
                            '' as ProjectWorkName ,
                            '' as OrderNo,
                            '' as PUContractCode,
                            '' as ContractCode ,
                            '' as ListItemCode ,
                            '' as ListItemName ,
                            GL.[DebitAmount] ,
                            GL.[CreditAmount] ,
                            '' as BranchName,
                            '' as OrganizationUnitCode,
                            '' as OrganizationUnitName,
                            '' as MasterCustomField1,
                            '' as MasterCustomField2,
                            '' as MasterCustomField3,
                            '' as MasterCustomField4,
                            '' as MasterCustomField5,
                            '' as MasterCustomField6,
                            '' as MasterCustomField7,
                            '' as MasterCustomField8,
                            '' as MasterCustomField9,
                            '' as MasterCustomField10,
                            '' as CustomField1,
                            '' as CustomField2,
                            '' as CustomField3,
                            '' as CustomField4,
                            '' as CustomField5,
                            '' as CustomField6,
                            '' as CustomField7,
                            '' as CustomField8,
                            '' as CustomField9,
                            '' as CustomField10,
							'Chi phí hợp lý' AS UnResonableCost ,
                            CASE WHEN DebitAmount <> 0
                                    THEN Gl.Account
                                        + AccountCorresponding
                                        + CAST(DebitAmount AS NVARCHAR(22))
                                    WHEN CreditAmount <> 0
                                    THEN gl.AccountCorresponding
                                        + GL.Account
                                        + CAST(CreditAmount AS NVARCHAR(22))
                            END AS Sort ,
                            GL.ReferenceID
                            AS RefIDSort,
                            0 as SortOrder,
                            0 as DetailPostOrder,
							GL.OrderPriority
                    FROM      [dbo].[GeneralLedger] GL
                            LEFT JOIN dbo.AccountingObject ao ON ao.ID = GL.AccountingObjectID
                    WHERE     gl.PostedDate BETWEEN @FromDate AND @ToDate
                            AND ISNULL(gl.AccountCorresponding,
                                        '') <> ''
                            AND ( GL.[DebitAmount] <> 0
                                    OR GL.[CreditAmount] <> 0
                                )
			 ) t
			order by t. PostedDate , t.OrderPriority     
            END 
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

ALTER PROCEDURE [dbo].[Proc_GetCACashBookInCABook]
    (
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @CurrencyID NVARCHAR(3) ,
      @AccountNumber NVARCHAR(25)
    )
AS
    BEGIN

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
              RefType INT ,
              RefDate DATETIME ,
              PostedDate DATETIME ,
              ReceiptRefNo NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              PaymentRefNo NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CashBookPostedDate DATETIME ,
              AccountObjectName NVARCHAR(128)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              JournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CurrencyID NVARCHAR(3) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              TotalReceiptFBCurrencyID MONEY ,
              TotalPaymentFBCurrencyID MONEY ,
              ClosingFBCurrencyID MONEY ,
              BranchID UNIQUEIDENTIFIER ,
              BranchName NVARCHAR(128) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              RefTypeName NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              Note NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CAType INT ,
              IsBold BIT ,
               /* - bổ sung mã nhân viên , tên nhân viên */
              EmployeeCode NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              EmployeeName NVARCHAR(128) COLLATE SQL_Latin1_General_CP1_CI_AS ,
            )       
       
        INSERT  #Result
                ( RefID ,
                  RefType ,
                  RefDate ,
                  PostedDate ,
                  ReceiptRefNo ,
                  PaymentRefNo ,
                  CashBookPostedDate ,
                  AccountObjectName ,
                  JournalMemo ,
                  CurrencyID ,
                  TotalReceiptFBCurrencyID ,
                  TotalPaymentFBCurrencyID ,
                  ClosingFBCurrencyID ,
                  BranchID ,
                  BranchName ,
                  RefTypeName ,
                  CAType ,
                  IsBold ,
                  EmployeeCode ,
                  EmployeeName
                )
                SELECT  RefID ,
                        RefType ,
                        RefDate ,
                        R.PostedDate ,
                        CASE WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        CASE WHEN TotalPaymentFBCurrencyID
                                  - TotalReceiptFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        R.PostedDate ,
                        ContactName ,
                        JournalMemo ,
                        @CurrencyID ,
                        SUM(CASE WHEN TotalReceiptFBCurrencyID
                                      - TotalPaymentFBCurrencyID > 0
                                 THEN TotalReceiptFBCurrencyID
                                      - TotalPaymentFBCurrencyID
                                 ELSE 0
                            END) AS TotalReceiptFBCurrencyID ,
                        SUM(CASE WHEN TotalPaymentFBCurrencyID
                                      - TotalReceiptFBCurrencyID > 0
                                 THEN TotalPaymentFBCurrencyID
                                      - TotalReceiptFBCurrencyID
                                 ELSE 0
                            END) AS TotalPaymentFBCurrencyID ,
                        SUM(ClosingFBCurrencyID) ,
                        BranchID ,
                        BranchName ,
                        RefTypeName ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END AS CAType ,
                        IsBold ,
                        EmployeeCode ,
                        EmployeeName
                FROM    ( SELECT    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ReferenceID
                                    END AS RefID ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.TypeID
                                    END AS RefType ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefDate
                                    END AS RefDate ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END AS PostedDate ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefNo
                                    END AS RefNo ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ContactName
                                    END AS ContactName ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN N'Số dư đầu kỳ'
                                         ELSE GL.Description
                                    END AS JournalMemo ,
                                    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal > 0
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) AS TotalReceiptFBCurrencyID ,
                                    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.CreditAmountOriginal
                                                  - GL.DebitAmountOriginal > 0
                                             THEN GL.CreditAmountOriginal
                                                  - GL.DebitAmountOriginal
                                             ELSE 0
                                        END) AS TotalPaymentFBCurrencyID ,
                                    SUM(CASE WHEN GL.PostedDate < @FromDate
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) AS ClosingFBCurrencyID ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.BranchID
                                    END AS BranchID ,
                                    null AS BranchName ,
                                    null RefTypeName ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE 0
                                    END AS CAType ,
                                    CASE WHEN GL.PostedDate < @FromDate THEN 1
                                         ELSE 0
                                    END AS IsBold ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectCode
                                    END AS EmployeeCode ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectName
                                    END AS EmployeeName
                          FROM      dbo.GeneralLedger AS GL
                                    LEFT JOIN dbo.AccountingObject AS AO ON AO.ID = GL.EmployeeID
                          WHERE     ( GL.PostedDate <= @ToDate )
                                    AND ( @CurrencyID IS NULL
                                          OR GL.CurrencyID = @CurrencyID
                                        )
                                    AND GL.Account LIKE @AccountNumber
                                    + '%'
                          GROUP BY  CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ReferenceID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.TypeID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefNo
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ContactName
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN N'Số dư đầu kỳ'
                                         ELSE GL.Description
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.BranchID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE 0
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate THEN 1
                                         ELSE 0
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectCode
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectName
                                    END
                          HAVING    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal > 0
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) <> SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                              AND GL.CreditAmountOriginal
                                                              - GL.DebitAmountOriginal > 0
                                                         THEN GL.CreditAmountOriginal
                                                              - GL.DebitAmountOriginal
                                                         ELSE 0
                                                    END)
                                    OR SUM(CASE WHEN GL.PostedDate < @FromDate
                                                THEN GL.DebitAmountOriginal
                                                     - GL.CreditAmountOriginal
                                                ELSE 0
                                           END) <> 0
                        ) AS R
                GROUP BY R.RefID ,
                        R.RefType ,
                        R.RefDate ,
                        R.PostedDate ,
                        CASE WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        CASE WHEN TotalPaymentFBCurrencyID
                                  - TotalReceiptFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        R.RefNo ,
                        R.ContactName ,
                        R.JournalMemo ,
                        R.BranchID ,
                        R.BranchName ,
                        R.RefTypeName ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END ,
                        R.IsBold ,
                        R.EmployeeCode ,
                        R.EmployeeName
                ORDER BY R.PostedDate ,
                        R.RefDate ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END ,
                        R.RefNo	
        DECLARE @ClosingAmount AS DECIMAL(22, 8)
        SET @ClosingAmount = 0
        SELECT  @ClosingAmount = ClosingFBCurrencyID
        FROM    #Result
        WHERE   RefID IS NULL
        UPDATE  #Result
        SET     @ClosingAmount = @ClosingAmount
                + ISNULL(TotalReceiptFBCurrencyID, 0)
                - ISNULL(TotalPaymentFBCurrencyID, 0) ,
                ClosingFBCurrencyID = @ClosingAmount	
    
        SELECT  *
        FROM    #Result R       
    
        DROP TABLE #Result
        
    END


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*=================================================================================================
 * Created By:		thaivh	
 * Created Date:	15/5/2018
 * Description:		Lay du lieu cho hop dong << hợp đồng >>
===================================================================================================== */
ALTER PROCEDURE [dbo].[Proc_EM_GetContract]
  
    @FromDate DATETIME ,
    @ToDate DATETIME,
	@IsContractSale bit
AS

    BEGIN
        SET NOCOUNT ON;

        if @IsContractSale = 1
			Begin   		                         
				select distinct a.SignedDate, a.Code, a.AccountingObjectName, a.AccountingObjectID, c.MaterialGoodsName, b.MaterialGoodsID, b.Unit, b.Quantity, 
				b.QuantityReceipt, b.TotalAmount as Amount, b.DiscountAmount, b.VATAmount, b.AmountReceipt as ActionAmount, a.RevenueType,
				e.AccountObjectGroupID, c.MaterialGoodsCategoryID, a.DepartmentID, a.ContractEmployeeID
				from EMContract a inner join EMContractDetailMG b on a.ID = b.ContractID
				left join MaterialGoods c on b.MaterialGoodsID = c.ID 
				left join AccountingObject e on a.AccountingObjectID = e.ID
				where a.SignedDate between @FromDate and @ToDate and a.TypeID = 860			
				order by a.SignedDate
			End
		else
			Begin   		                         
				select distinct a.SignedDate, a.Code, a.AccountingObjectName, a.AccountingObjectID, c.MaterialGoodsName, b.MaterialGoodsID, b.Unit, b.Quantity, b.QuantityReceipt,
				b.TotalAmount as Amount, b.DiscountAmount, b.VATAmount, b.AmountReceipt as ActionAmount, a.RevenueType,
				e.AccountObjectGroupID, c.MaterialGoodsCategoryID, a.DepartmentID, a.ContractEmployeeID
				from EMContract a inner join EMContractDetailMG b on a.ID = b.ContractID
				left join MaterialGoods c on b.MaterialGoodsID = c.ID 
				left join AccountingObject e on a.AccountingObjectID = e.ID
				where a.SignedDate between @FromDate and @ToDate and a.TypeID = 850
				order by a.SignedDate
			End
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO




ALTER PROCEDURE [dbo].[Proc_BA_GetBookDepositListDetail]
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @CurrencyID NVARCHAR(3) ,
    @AccountNumber NVARCHAR(25) ,
    @BankAccountID UNIQUEIDENTIFIER ,
    @IsSimilarSum BIT = 0 ,
    @IsSoftOrderVoucher BIT = 0
AS
    BEGIN
        SET NOCOUNT ON
        
        CREATE TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
              BankAccount NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              RefType INT ,
              JournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CorrespondingAccountNumber NVARCHAR(20)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              ExchangeRate DECIMAL ,
              DebitAmountOC DECIMAL ,
              DebitAmount DECIMAL ,
              CreditAmountOC DECIMAL ,
              CreditAmount DECIMAL ,
              ClosingAmountOC DECIMAL ,
              ClosingAmount DECIMAL ,
              AccountObjectCode NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectName NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              EmployeeCode NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              EmployeeName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              ProjectWorkCode NVARCHAR(20)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              ProjectWorkName NVARCHAR(128)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              

              ExpenseItemCode NVARCHAR(20)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              ExpenseItemName NVARCHAR(128)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              ListItemCode NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              ListItemName NVARCHAR(128) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              ContractCode NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS ,

              BranchID UNIQUEIDENTIFIER ,
              BranchCode NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              BranchName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              IsBold BIT ,
              PaymentType INT ,
              OrderType INT ,
              RefOrder INT
            )	
		
        CREATE TABLE #Result1
            (
              RowNum INT PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
              BankAccount NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255),
              CorrespondingAccountNumber NVARCHAR(20) ,
              ExchangeRate DECIMAL ,
              DebitAmountOC DECIMAL ,
              DebitAmount DECIMAL ,
              CreditAmountOC DECIMAL ,
              CreditAmount DECIMAL ,
              ClosingAmountOC DECIMAL ,
              ClosingAmount DECIMAL ,
              AccountObjectCode NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectName NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              EmployeeCode NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              EmployeeName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,

              ProjectWorkCode NVARCHAR(20) ,
              ProjectWorkName NVARCHAR(128) ,
              

              ExpenseItemCode NVARCHAR(20) ,
              ExpenseItemName NVARCHAR(128) ,
              ListItemCode NVARCHAR(20) ,
              ListItemName NVARCHAR(128)  ,
              ContractCode NVARCHAR(50) ,

              BranchID UNIQUEIDENTIFIER ,
              BranchCode NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              BranchName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              IsBold BIT ,
              PaymentType INT ,
              OrderType INT ,
              RefOrder INT
            )	
		
        DECLARE @BankAccountAll UNIQUEIDENTIFIER/*tất cả tài khoản ngân hàng*/
        SET @BankAccountAll = 'A0624CFA-D105-422f-BF20-11F246704DC3'
		
        DECLARE @AccountNumberPercent NVARCHAR(25)
        SET @AccountNumberPercent = @AccountNumber + '%'
    
        DECLARE @ClosingAmountOC DECIMAL(29, 4)
        DECLARE @ClosingAmount DECIMAL(29, 4)
   	    DECLARE @tblListBankAccountDetail TABLE
            (
			  ID UNIQUEIDENTIFIER,
              BankAccount NVARCHAR(Max) ,
              BankAccountName NVARCHAR(MAX) 
            ) 
        if(@BankAccountID is null)     
        INSERT  INTO @tblListBankAccountDetail
                SELECT  BAD.ID,
				        BAD.BankAccount,
				        BAD.BankName
                FROM    BankAccountDetail BAD,
				        Bank BA
                WHERE BAD.BankID = BA.ID    
		else 	
			INSERT  INTO @tblListBankAccountDetail
                SELECT  BAD.ID,
				        BAD.BankAccount,
				        BAD.BankName
                FROM    BankAccountDetail BAD,
				        Bank BA
                WHERE BAD.BankID = BA.ID  
				and BAD.ID =  @BankAccountID 
	

        IF @ClosingAmount IS NULL
            SET @ClosingAmount = 0
        IF @ClosingAmountOC IS NULL
            SET @ClosingAmountOC = 0
      

        INSERT  #Result
                ( BankAccount ,
                  JournalMemo ,
                  DebitAmount ,
                  DebitAmountOC ,
                  CreditAmount ,
                  CreditAmountOC ,
                  ClosingAmountOC ,
                  ClosingAmount ,
                  IsBold ,
                  OrderType    
			  
                )
                SELECT  







		                BA.BankAccount + ' - ' + BA.BankAccountName,
                        N'Số dư đầu kỳ' AS JournalMemo ,
                        $0 AS DebitAmountOC ,
                        $0 AS DebitAmount ,
                        $0 AS CreditAmountOC ,
                        $0 AS CreditAmount ,
                        ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) ,
                        ISNULL(SUM(GL.DebitAmount - GL.CreditAmount), 0) ,
                        1 ,
                        0
                FROM    dbo.GeneralLedger AS GL
				        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID


                WHERE   ( GL.PostedDate < @FromDate )
                        AND GL.Account LIKE @AccountNumberPercent








                        AND ( @CurrencyID IS NULL
                              OR GL.CurrencyID = @CurrencyID
                            )
                GROUP BY BA.BankAccount , BA.BankAccountName








                HAVING  ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) <> 0
                        OR ISNULL(SUM(GL.DebitAmount - GL.CreditAmount), 0) <> 0

       INSERT  #Result
	   (
              RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder )
                    SELECT  GL.ReferenceID ,












	                        BA.BankAccount + ' - ' + BA.BankAccountName,
                            GL.PostedDate ,
                            GL.RefDate ,
                            GL.RefNo AS RefNoFinance ,

                            GL.Description,
                            GL.AccountCorresponding ,
                            GL.ExchangeRate ,
                            ISNULL(GL.DebitAmountOriginal, 0) AS DebitAmountOC ,
                            ISNULL(GL.DebitAmount, 0) AS DebitAmount ,
                            ISNULL(GL.CreditAmountOriginal, 0) AS CreditAmountOC ,
                            ISNULL(GL.CreditAmount, 0) AS CreditAmount ,
                            $0 AS ClosingAmountOC ,
                            $0 AS ClosingAmount ,
                            AO.AccountingObjectCode ,
                            AO.AccountingObjectName AS AccountObjectName ,



                            EI.ExpenseItemCode ,
                            EI.ExpenseItemName ,


                            GL.BranchID ,
                            0 ,
                            CASE WHEN GL.DebitAmount <> 0 THEN 0
                                 ELSE 1
                            END AS PaymentType ,
                            1 ,
                            0
                    FROM    dbo.GeneralLedger AS GL
					        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID


                            LEFT JOIN dbo.AccountingObject AO ON GL.AccountingObjectID = AO.ID
                            LEFT JOIN dbo.ExpenseItem EI ON EI.ID = GL.ExpenseItemID
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND GL.Account LIKE @AccountNumberPercent








                            AND ( @CurrencyID IS NULL
                                  OR GL.CurrencyID = @CurrencyID
                                )
                            AND ( GL.DebitAmountOriginal <> 0
                                  OR GL.CreditAmountOriginal <> 0
                                  OR GL.DebitAmount <> 0
                                  OR GL.CreditAmount <> 0
                                )
                    ORDER BY BA.BankAccount ,
                            PostedDate ,
                            RefDate ,
                            PaymentType ,
                            RefNo
                     
          
   
        DECLARE @BankAccount NVARCHAR(500)
        DECLARE @CloseAmountOC MONEY
        DECLARE @CloseAmount MONEY
        SET @BankAccount = NULL
        INSERT  INTO #Result1
		(     RowNum,
		      RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder)
                SELECT  ROW_NUMBER() OVER ( ORDER BY BankAccount , OrderType, PostedDate , CASE
                                                              WHEN @IsSoftOrderVoucher = 1
                                                              THEN RefOrder
                                                              ELSE 0
                                                              END, RefDate , PaymentType , RefNo ) ,
                        RefID ,
                        BankAccount ,
                        PostedDate ,
                        RefDate ,
                        RefNo ,
                        JournalMemo ,
                        CorrespondingAccountNumber ,
                        ExchangeRate ,
                        DebitAmountOC ,
                        DebitAmount ,
                        CreditAmountOC ,
                        CreditAmount ,
                        ClosingAmountOC ,
                        ClosingAmount ,
                        AccountObjectCode ,
                        AccountObjectName ,

                        ExpenseItemCode ,
                        ExpenseItemName ,
                        BranchID ,
						IsBold  ,
					    PaymentType  ,
						OrderType  ,
						RefOrder
                FROM    #Result
                ORDER BY BankAccount ,
                        OrderType ,
                        PostedDate ,
                        CASE WHEN @IsSoftOrderVoucher = 1 THEN RefOrder
                             ELSE 0
                        END ,
                        RefDate ,
                        PaymentType ,
                        RefNo
        
      
        SET @CloseAmount = 0
        SET @CloseAmountOC = 0
      

        UPDATE  #Result1
        SET

                @CloseAmount = ( CASE WHEN OrderType = 0
                                      THEN ( CASE WHEN ClosingAmount = 0
                                                  THEN 0
                                                  ELSE ClosingAmount
                                             END )
                                      WHEN @BankAccount <> BankAccount
                                      THEN DebitAmount - CreditAmount
                                      ELSE @CloseAmount + DebitAmount
                                           - CreditAmount
                                 END ) ,
                @CloseAmountOC = ( CASE WHEN OrderType = 0
                                        THEN ( CASE WHEN ClosingAmountOC = 0
                                                    THEN 0
                                                    ELSE ClosingAmountOC
                                               END )
                                        WHEN @BankAccount <> BankAccount
                                        THEN DebitAmountOC - CreditAmountOC
                                        ELSE @CloseAmountOC + DebitAmountOC
                                             - CreditAmountOC
                                   END ) ,
                ClosingAmount = @CloseAmount ,
                ClosingAmountOC = @CloseAmountOC ,
                @BankAccount = BankAccount
        
        SELECT  *
        FROM    #Result1
        ORDER BY BankAccount ,
                OrderType ,
                PostedDate ,
                CASE WHEN @IsSoftOrderVoucher = 1 THEN RefOrder
                     ELSE 0
                END ,
                RefDate ,
                PaymentType ,
                RefNo
     
        DROP TABLE #Result
        DROP TABLE #Result1
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_Accreditative]
    @BankCode nvarchar(25)
AS

    BEGIN
        SET NOCOUNT ON;
   		                         
		select distinct a.ID, a.No, a.Date, a.BankName, a.AccountingObjectAddress, h.BankAccount as AccountingObjectBankAccount, a.AccountingObjectBankName,
		a.AccountingObjectName, c.BankAccount, c.BankBranchName, d.Tel, e.ID as CurrencyName, a.TotalAllOriginal as TotalAmount, a.Reason, h.BankBranchName as Branch, f.BankCode
		from MBTellerPaper a left join MBTellerPaperDetail b on a.ID = b.MBTellerPaperID
		left join BankAccountDetail c on a.BankAccountDetailID = c.ID 
		left join AccountingObject d on a.AccountingObjectID = d.ID
		left join Currency e on a.CurrencyID = e.ID 		
		left join Bank f on c.BankID = f.ID
		left join AccountingObjectBankAccount h on a.AccountingObjectBankAccount = h.ID
		where f.BankCode = @BankCode		
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER VIEW [dbo].[ViewVouchersCloseBook]
AS
/*Create by tnson 24/08/2011 - View này chỉ giành riêng cho việc kiểm tra các chứng từ chưa ghi sổ trước khi khóa sổ không được dùng cho việc khác */ SELECT dbo.MCPayment.ID, dbo.MCPaymentDetail.ID AS DetailID, 
                         dbo.MCPayment.TypeID, dbo.Type.TypeName, dbo.MCPayment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCPayment.Date, dbo.MCPayment.PostedDate, dbo.MCPaymentDetail.DebitAccount, 
                         dbo.MCPaymentDetail.CreditAccount, dbo.MCPayment.CurrencyID, dbo.MCPaymentDetail.AmountOriginal, dbo.MCPaymentDetail.Amount, NULL AS OrgPrice, dbo.MCPayment.Reason, 
                         dbo.MCPaymentDetail.Description, dbo.MCPaymentDetail.CostSetID, CostSet.CostSetCode, dbo.MCPaymentDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MCPayment.EmployeeID, dbo.MCPayment.BranchID, NULL 
                         AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MCPayment.Recorded, dbo.MCPayment.TotalAmount, 
                         dbo.MCPayment.TotalAmountOriginal, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MCPayment' AS RefTable
/*case AccountingObject.IsEmployee when 1 then  AccountingObject.AccountingObjectName end AS EmployeeName*/ FROM dbo.MCPayment INNER JOIN
                         dbo.MCPaymentDetail ON dbo.MCPayment.ID = dbo.MCPaymentDetail.MCPaymentID INNER JOIN
                         dbo.Type ON MCPayment.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MCPaymentDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MCPaymentDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MCPaymentDetail.ContractID
UNION ALL
SELECT        dbo.MCPayment.ID, dbo.MCPaymentDetailTax.ID AS DetailID, dbo.MCPayment.TypeID, dbo.Type.TypeName, dbo.MCPayment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCPayment.Date, 
                         dbo.MCPayment.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MCPayment.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MCPayment.Reason, 
                         dbo.MCPaymentDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MCPayment.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MCPaymentDetailTax.VATAmountOriginal, dbo.MCPaymentDetailTax.VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MCPayment.Recorded, dbo.MCPayment.TotalAmount, dbo.MCPayment.TotalAmountOriginal, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MCPayment' AS RefTable
FROM            dbo.MCPayment INNER JOIN
                         dbo.MCPaymentDetailTax ON dbo.MCPayment.ID = dbo.MCPaymentDetailTax.MCPaymentID INNER JOIN
                         dbo.Type ON MCPayment.TypeID = dbo.Type.ID
UNION ALL
SELECT        dbo.MBDeposit.ID, dbo.MBDepositDetail.ID AS DetailID, dbo.MBDeposit.TypeID, dbo.Type.TypeName, dbo.MBDeposit.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBDeposit.Date, 
                         dbo.MBDeposit.PostedDate, dbo.MBDepositDetail.DebitAccount, dbo.MBDepositDetail.CreditAccount, dbo.MBDeposit.CurrencyID, dbo.MBDepositDetail.AmountOriginal, dbo.MBDepositDetail.Amount, NULL 
                         AS OrgPrice, dbo.MBDeposit.Reason, dbo.MBDepositDetail.Description, dbo.MBDepositDetail.CostSetID, CostSet.CostSetCode, dbo.MBDepositDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MBDeposit.EmployeeID, 
                         dbo.MBDeposit.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBDeposit.Recorded, 
                         dbo.MBDeposit.TotalAmountOriginal, dbo.MBDeposit.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MBDeposit' AS RefTable
FROM            dbo.MBDeposit INNER JOIN
                         dbo.MBDepositDetail ON dbo.MBDeposit.ID = dbo.MBDepositDetail.MBDepositID INNER JOIN
                         dbo.Type ON MBDeposit.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBDepositDetail.AccountingObjectID = dbo.AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBDepositDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBDepositDetail.ContractID
UNION ALL
SELECT        dbo.MBDeposit.ID, dbo.MBDepositDetailTax.ID AS DetailID, dbo.MBDeposit.TypeID, dbo.Type.TypeName, dbo.MBDeposit.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBDeposit.Date, 
                         dbo.MBDeposit.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MBDeposit.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBDeposit.Reason, 
                         dbo.MBDepositDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBDeposit.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MBDepositDetailTax.VATAmountOriginal, dbo.MBDepositDetailTax.VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MBDeposit.Recorded, dbo.MBDeposit.TotalAmountOriginal, dbo.MBDeposit.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBDeposit' AS RefTable
FROM            dbo.MBDeposit INNER JOIN
                         dbo.MBDepositDetailTax ON dbo.MBDeposit.ID = dbo.MBDepositDetailTax.MBDepositID INNER JOIN
                         dbo.Type ON MBDeposit.TypeID = dbo.Type.ID
UNION ALL
SELECT        dbo.MBInternalTransfer.ID, dbo.MBInternalTransferDetail.ID AS DetailID, dbo.MBInternalTransfer.TypeID, dbo.Type.TypeName, dbo.MBInternalTransfer.No, NULL AS InwardNo, NULL AS OutwardNo, 
                         dbo.MBInternalTransfer.Date, dbo.MBInternalTransfer.PostedDate, dbo.MBInternalTransferDetail.DebitAccount, dbo.MBInternalTransferDetail.CreditAccount, dbo.MBInternalTransferDetail.CurrencyID, 
                         dbo.MBInternalTransferDetail.AmountOriginal, dbo.MBInternalTransferDetail.Amount, NULL AS OrgPrice, dbo.MBInternalTransfer.Reason, dbo.MBInternalTransfer.Reason AS Description, 
                         dbo.MBInternalTransferDetail.CostSetID, CostSet.CostSetCode, dbo.MBInternalTransferDetail.ContractID, EMContract.Code AS ContractCode, NULL AS AccountingObjectID, '' AS AccountingObjectCategoryName, 
                         '' AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBInternalTransfer.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.MBInternalTransfer.Recorded, dbo.MBInternalTransfer.TotalAmountOriginal, dbo.MBInternalTransfer.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBInternalTransfer' AS RefTable
FROM            dbo.MBInternalTransfer INNER JOIN
                         dbo.MBInternalTransferDetail ON dbo.MBInternalTransfer.ID = dbo.MBInternalTransferDetail.MBInternalTransferID INNER JOIN
                         dbo.Type ON dbo.MBInternalTransfer.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBInternalTransferDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBInternalTransferDetail.ContractID
UNION ALL
SELECT        dbo.MBTellerPaper.ID, dbo.MBTellerPaperDetail.ID AS DetailID, dbo.MBTellerPaper.TypeID, dbo.Type.TypeName, dbo.MBTellerPaper.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBTellerPaper.Date, 
                         dbo.MBTellerPaper.PostedDate, dbo.MBTellerPaperDetail.DebitAccount, dbo.MBTellerPaperDetail.CreditAccount, dbo.MBTellerPaper.CurrencyID, dbo.MBTellerPaperDetail.AmountOriginal, 
                         dbo.MBTellerPaperDetail.Amount, NULL AS OrgPrice, dbo.MBTellerPaper.Reason, dbo.MBTellerPaperDetail.Description, dbo.MBTellerPaperDetail.CostSetID, CostSet.CostSetCode, 
                         dbo.MBTellerPaperDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.MBTellerPaper.EmployeeID, dbo.MBTellerPaper.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL 
                         AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MBTellerPaper.Recorded, dbo.MBTellerPaper.TotalAmountOriginal, dbo.MBTellerPaper.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBTellerPaper' AS RefTable
FROM            dbo.MBTellerPaper INNER JOIN
                         dbo.MBTellerPaperDetail ON dbo.MBTellerPaper.ID = dbo.MBTellerPaperDetail.MBTellerPaperID INNER JOIN
                         dbo.Type ON dbo.MBTellerPaper.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBTellerPaperDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBTellerPaperDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBTellerPaperDetail.ContractID
UNION ALL
SELECT        dbo.MBTellerPaper.ID, dbo.MBTellerPaperDetailTax.ID AS DetailID, dbo.MBTellerPaper.TypeID, dbo.Type.TypeName, dbo.MBTellerPaper.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBTellerPaper.Date, 
                         dbo.MBTellerPaper.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MBTellerPaper.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBTellerPaper.Reason, 
                         dbo.MBTellerPaperDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBTellerPaper.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MBTellerPaperDetailTax.VATAmountOriginal, 
                         dbo.MBTellerPaperDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBTellerPaper.Recorded, dbo.MBTellerPaper.TotalAmountOriginal, dbo.MBTellerPaper.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBTellerPaper' AS RefTable
FROM            dbo.MBTellerPaper INNER JOIN
                         dbo.MBTellerPaperDetailTax ON dbo.MBTellerPaper.ID = dbo.MBTellerPaperDetailTax.MBTellerPaperID INNER JOIN
                         dbo.Type ON dbo.MBTellerPaper.TypeID = dbo.Type.ID
UNION ALL
SELECT        dbo.MBCreditCard.ID, dbo.MBCreditCardDetail.ID AS DetailID, dbo.MBCreditCard.TypeID, dbo.Type.TypeName, dbo.MBCreditCard.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBCreditCard.Date, 
                         dbo.MBCreditCard.PostedDate, dbo.MBCreditCardDetail.DebitAccount, dbo.MBCreditCardDetail.CreditAccount, dbo.MBCreditCard.CurrencyID, dbo.MBCreditCardDetail.AmountOriginal, 
                         dbo.MBCreditCardDetail.Amount, NULL AS OrgPrice, dbo.MBCreditCard.Reason, dbo.MBCreditCardDetail.Description, dbo.MBCreditCardDetail.CostSetID, CostSet.CostSetCode, dbo.MBCreditCardDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.MBCreditCard.EmployeeID, dbo.MBCreditCard.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL 
                         AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MBCreditCard.Recorded, dbo.MBCreditCard.TotalAmountOriginal, dbo.MBCreditCard.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBCreditCard' AS RefTable
FROM            dbo.MBCreditCard INNER JOIN
                         dbo.MBCreditCardDetail ON dbo.MBCreditCard.ID = dbo.MBCreditCardDetail.MBCreditCardID INNER JOIN
                         dbo.Type ON dbo.MBCreditCard.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBCreditCardDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBCreditCardDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBCreditCardDetail.ContractID
UNION ALL
SELECT        dbo.MBCreditCard.ID, dbo.MBCreditCardDetailTax.ID AS DetailID, dbo.MBCreditCard.TypeID, dbo.Type.TypeName, dbo.MBCreditCard.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBCreditCard.Date, 
                         dbo.MBCreditCard.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MBCreditCard.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBCreditCard.Reason, 
                         dbo.MBCreditCardDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBCreditCard.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MBCreditCardDetailTax.VATAmountOriginal, 
                         dbo.MBCreditCardDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBCreditCard.Recorded, dbo.MBCreditCard.TotalAmountOriginal, dbo.MBCreditCard.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBCreditCard' AS RefTable
FROM            dbo.MBCreditCard INNER JOIN
                         dbo.MBCreditCardDetailTax ON dbo.MBCreditCard.ID = dbo.MBCreditCardDetailTax.MBCreditCardID INNER JOIN
                         dbo.Type ON dbo.MBCreditCard.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        dbo.MCReceipt.ID, dbo.MCReceiptDetail.ID AS DetailID, dbo.MCReceipt.TypeID, dbo.Type.TypeName, dbo.MCReceipt.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCReceipt.Date, 
                         dbo.MCReceipt.PostedDate, dbo.MCReceiptDetail.DebitAccount, dbo.MCReceiptDetail.CreditAccount, dbo.MCReceipt.CurrencyID, dbo.MCReceiptDetail.AmountOriginal, dbo.MCReceiptDetail.Amount, NULL 
                         AS OrgPrice, dbo.MCReceipt.Reason, dbo.MCReceiptDetail.Description, dbo.MCReceiptDetail.CostSetID, CostSet.CostSetCode, dbo.MCReceiptDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MCReceipt.EmployeeID, 
                         dbo.MCReceipt.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MCReceipt.Recorded, 
                         dbo.MCReceipt.TotalAmountOriginal, dbo.MCReceipt.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MCReceipt' AS RefTable
FROM            dbo.MCReceipt INNER JOIN
                         dbo.MCReceiptDetail ON dbo.MCReceipt.ID = dbo.MCReceiptDetail.MCReceiptID INNER JOIN
                         dbo.Type ON MCReceipt.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MCReceiptDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MCReceiptDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MCReceiptDetail.ContractID
UNION ALL
SELECT        dbo.MCReceipt.ID, dbo.MCReceiptDetailTax.ID AS DetailID, dbo.MCReceipt.TypeID, dbo.Type.TypeName, dbo.MCReceipt.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCReceipt.Date, 
                         dbo.MCReceipt.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MCReceipt.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MCReceipt.Reason, 
                         dbo.MCReceiptDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MCReceipt.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MCReceiptDetailTax.VATAmountOriginal, dbo.MCReceiptDetailTax.VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MCReceipt.Recorded, dbo.MCReceipt.TotalAmountOriginal, dbo.MCReceipt.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MCReceipt' AS RefTable
FROM            dbo.MCReceipt INNER JOIN
                         dbo.MCReceiptDetailTax ON dbo.MCReceipt.ID = dbo.MCReceiptDetailTax.MCReceiptID INNER JOIN
                         dbo.Type ON MCReceipt.TypeID = dbo.Type.ID
UNION ALL
SELECT        dbo.FAAdjustment.ID, dbo.FAAdjustmentDetail.ID AS DetailID, dbo.FAAdjustment.TypeID, dbo.Type.TypeName, dbo.FAAdjustment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FAAdjustment.Date, 
                         dbo.FAAdjustment.PostedDate, dbo.FAAdjustmentDetail.DebitAccount, dbo.FAAdjustmentDetail.CreditAccount, NULL AS CurrencyID, $ 0 AS AmountOriginal, dbo.FAAdjustmentDetail.Amount, $ 0 AS OrgPrice, 
                         dbo.FAAdjustment.Reason, dbo.FAAdjustmentDetail.Description, dbo.FAAdjustmentDetail.CostSetID, CostSet.CostSetCode, dbo.FAAdjustmentDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, NULL AS EmployeeID, 
                         dbo.FAAdjustment.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, $ 0 AS VATAmountOriginal, $ 0 AS VATAmount, $ 0 AS Quantity, $ 0 AS UnitPrice, dbo.FAAdjustment.Recorded, 
                         $ 0 AS TotalAmountOriginal, dbo.FAAdjustment.TotalAmount, $ 0 AS DiscountAmountOriginal, $ 0 AS DiscountAmount, NULL AS DiscountAccount, $ 0 AS FreightAmountOriginal, NULL AS FreightAmount, 
                         0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FAAdjustment' AS RefTable
FROM            dbo.FAAdjustment INNER JOIN
                         dbo.FAAdjustmentDetail ON dbo.FAAdjustment.ID = dbo.FAAdjustmentDetail.FAAdjustmentID INNER JOIN
                         dbo.Type ON FAAdjustment.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FAAdjustmentDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FAAdjustment.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FAAdjustmentDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FAAdjustmentDetail.ContractID
UNION ALL
SELECT        dbo.FADepreciation.ID, dbo.FADepreciationDetail.ID AS DetailID, dbo.FADepreciation.TypeID, dbo.Type.TypeName, dbo.FADepreciation.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FADepreciation.Date, 
                         dbo.FADepreciation.PostedDate, dbo.FADepreciationDetail.DebitAccount, dbo.FADepreciationDetail.CreditAccount, FADepreciationDetail.CurrencyID, dbo.FADepreciationDetail.AmountOriginal, 
                         dbo.FADepreciationDetail.Amount, dbo.FADepreciationDetail.OrgPrice, dbo.FADepreciation.Reason, dbo.FADepreciationDetail.Description, dbo.FADepreciationDetail.CostSetID, CostSet.CostSetCode, 
                         dbo.FADepreciationDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.FADepreciationDetail.EmployeeID, dbo.FADepreciation.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL 
                         AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, FADepreciationDetail.DepartmentID, Department.DepartmentName, NULL AS VATAccount, NULL 
                         AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.FADepreciation.Recorded, dbo.FADepreciation.TotalAmountOriginal, dbo.FADepreciation.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FADepreciation' AS RefTable
FROM            dbo.FADepreciation INNER JOIN
                         dbo.FADepreciationDetail ON dbo.FADepreciation.ID = dbo.FADepreciationDetail.FADepreciationID INNER JOIN
                         dbo.Type ON FADepreciation.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FADepreciationDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FADepreciationDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FADepreciationDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FADepreciationDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FADepreciationDetail.DepartmentID
UNION ALL
SELECT        dbo.FAIncrement.ID, dbo.FAIncrementDetail.ID AS DetailID, dbo.FAIncrement.TypeID, dbo.Type.TypeName, dbo.FAIncrement.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FAIncrement.Date, 
                         dbo.FAIncrement.PostedDate, dbo.FAIncrementDetail.DebitAccount, dbo.FAIncrementDetail.CreditAccount, FAIncrement.CurrencyID, dbo.FAIncrementDetail.AmountOriginal, dbo.FAIncrementDetail.Amount, 
                         dbo.FAIncrementDetail.OrgPriceOriginal AS OrgPrice, dbo.FAIncrement.Reason, dbo.FAIncrementDetail.Description, dbo.FAIncrementDetail.CostSetID, CostSet.CostSetCode, dbo.FAIncrementDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.AccountingObject.ID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, 
                         dbo.FAIncrement.EmployeeID, dbo.FAIncrement.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, FAIncrementDetail.DepartmentID, dbo.Department.DepartmentName, dbo.FAIncrementDetail.VATAccount AS VATAccount, 
                         dbo.FAIncrementDetail.VATAmountOriginal AS VATAmountOriginal, dbo.FAIncrementDetail.VATAmount AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.FAIncrement.Recorded, 
                         dbo.FAIncrement.TotalAmountOriginal, dbo.FAIncrement.TotalAmount, dbo.FAIncrementDetail.DiscountAmountOriginal, dbo.FAIncrementDetail.DiscountAmount, NULL AS DiscountAccount, 
                         dbo.FAIncrementDetail.FreightAmountOriginal, dbo.FAIncrementDetail.FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FAIncrement' AS RefTable
FROM            dbo.FAIncrement INNER JOIN
                         dbo.FAIncrementDetail ON dbo.FAIncrement.ID = dbo.FAIncrementDetail.ID INNER JOIN
                         dbo.Type ON FAIncrement.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FAIncrementDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FAIncrementDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FAIncrementDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FAIncrementDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FAIncrementDetail.DepartmentID
UNION ALL
SELECT        dbo.FADecrement.ID, dbo.FADecrementDetail.ID AS DetailID, dbo.FADecrement.TypeID, dbo.Type.TypeName, dbo.FADecrement.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FADecrement.Date, 
                         dbo.FADecrement.PostedDate, dbo.FADecrementDetail.DebitAccount, dbo.FADecrementDetail.CreditAccount, FADecrement.CurrencyID, dbo.FADecrementDetail.AmountOriginal, 
                         dbo.FADecrementDetail.Amount, NULL AS OrgPrice, dbo.FADecrement.Reason, dbo.FADecrementDetail.Description, dbo.FADecrementDetail.CostSetID, CostSet.CostSetCode, dbo.FADecrementDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.FADecrementDetail.EmployeeID, dbo.FADecrement.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL 
                         AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, FADecrementDetail.DepartmentID, Department.DepartmentName, NULL AS VATAccount, NULL 
                         AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.FADecrement.Recorded, dbo.FADecrement.TotalAmountOriginal, dbo.FADecrement.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FADecrement' AS RefTable
FROM            dbo.FADecrement INNER JOIN
                         dbo.FADecrementDetail ON dbo.FADecrement.ID = dbo.FADecrementDetail.FADecrementID INNER JOIN
                         dbo.Type ON FADecrement.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FADecrementDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FADecrementDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FADecrementDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FADecrementDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FADecrementDetail.DepartmentID
UNION ALL
/*Chứng từ nghiệp vụ khác*/ SELECT dbo.GOtherVoucher.ID, dbo.GOtherVoucherDetail.ID AS DetailID, dbo.GOtherVoucher.TypeID, dbo.Type.TypeName, dbo.GOtherVoucher.No, NULL AS InwardNo, NULL AS OutwardNo, 
                         dbo.GOtherVoucher.Date, dbo.GOtherVoucher.PostedDate, dbo.GOtherVoucherDetail.DebitAccount, dbo.GOtherVoucherDetail.CreditAccount, GOtherVoucherDetail.CurrencyID, 
                         dbo.GOtherVoucherDetail.AmountOriginal, dbo.GOtherVoucherDetail.Amount, NULL AS OrgPrice, dbo.GOtherVoucher.Reason, dbo.GOtherVoucherDetail.Description, dbo.GOtherVoucherDetail.CostSetID, 
                         CostSet.CostSetCode, dbo.GOtherVoucherDetail.ContractID, EMContract.Code AS ContractCode, dbo.GOtherVoucherDetail.DebitAccountingObjectID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode AS AccountingObjectCode, dbo.GOtherVoucherDetail.EmployeeID, 
                         dbo.GOtherVoucher.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.GOtherVoucher.Recorded, 
                         dbo.GOtherVoucher.TotalAmountOriginal, dbo.GOtherVoucher.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'GOtherVoucher' AS RefTable
FROM            dbo.GOtherVoucher INNER JOIN
                         dbo.GOtherVoucherDetail ON dbo.GOtherVoucher.ID = dbo.GOtherVoucherDetail.GOtherVoucherID INNER JOIN
                         dbo.Type ON GOtherVoucher.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON GOtherVoucherDetail.DebitAccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = GOtherVoucherDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = GOtherVoucherDetail.ContractID
UNION ALL
SELECT        dbo.GOtherVoucher.ID, dbo.GOtherVoucherDetailTax.ID AS DetailID, dbo.GOtherVoucher.TypeID, dbo.Type.TypeName, dbo.GOtherVoucher.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.GOtherVoucher.Date, 
                         dbo.GOtherVoucher.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, GOtherVoucherDetailTax.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.GOtherVoucher.Reason, 
                         dbo.GOtherVoucherDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AccountingObjectCode, NULL AS EmployeeID, dbo.GOtherVoucher.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.GOtherVoucherDetailTax.VATAmountOriginal, 
                         dbo.GOtherVoucherDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.GOtherVoucher.Recorded, dbo.GOtherVoucher.TotalAmountOriginal, dbo.GOtherVoucher.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'GOtherVoucher' AS RefTable
FROM            dbo.GOtherVoucher INNER JOIN
                         dbo.GOtherVoucherDetailTax ON dbo.GOtherVoucher.ID = dbo.GOtherVoucherDetailTax.GOtherVoucherID INNER JOIN
                         dbo.Type ON GOtherVoucher.TypeID = dbo.Type.ID
UNION ALL
SELECT        dbo.RSInwardOutward.ID, dbo.RSInwardOutwardDetail.ID AS DetailID, dbo.RSInwardOutward.TypeID, dbo.Type.TypeName, dbo.RSInwardOutward.No, NULL AS InwardNo, NULL AS OutwardNo, 
                         dbo.RSInwardOutward.Date, dbo.RSInwardOutward.PostedDate, dbo.RSInwardOutwardDetail.DebitAccount, dbo.RSInwardOutwardDetail.CreditAccount, RSInwardOutward.CurrencyID, 
                         dbo.RSInwardOutwardDetail.AmountOriginal, dbo.RSInwardOutwardDetail.Amount, NULL AS OrgPrice, dbo.RSInwardOutward.Reason, dbo.RSInwardOutwardDetail.Description, 
                         dbo.RSInwardOutwardDetail.CostSetID, CostSet.CostSetCode, dbo.RSInwardOutwardDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.RSInwardOutwardDetail.EmployeeID, dbo.RSInwardOutward.BranchID, NULL 
                         AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID, dbo.Repository.RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, RSInwardOutwardDetail.Quantity, RSInwardOutwardDetail.UnitPrice, 
                         dbo.RSInwardOutward.Recorded, dbo.RSInwardOutward.TotalAmountOriginal, dbo.RSInwardOutward.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL 
                         AS OutwardAmount, 'RSInwardOutward' AS RefTable
FROM            dbo.RSInwardOutward INNER JOIN
                         dbo.RSInwardOutwardDetail ON dbo.RSInwardOutward.ID = dbo.RSInwardOutwardDetail.RSInwardOutwardID INNER JOIN
                         dbo.Type ON RSInwardOutward.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON RSInwardOutwardDetail.EmployeeID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON RSInwardOutwardDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON RSInwardOutwardDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = RSInwardOutwardDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = RSInwardOutwardDetail.ContractID
UNION ALL
SELECT        dbo.RSTransfer.ID, dbo.RSTransferDetail.ID AS DetailID, dbo.RSTransfer.TypeID, dbo.Type.TypeName, dbo.RSTransfer.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.RSTransfer.Date, 
                         dbo.RSTransfer.PostedDate, dbo.RSTransferDetail.DebitAccount, dbo.RSTransferDetail.CreditAccount, RSTransfer.CurrencyID, dbo.RSTransferDetail.AmountOriginal, dbo.RSTransferDetail.Amount, NULL 
                         AS OrgPrice, dbo.RSTransfer.Reason, dbo.RSTransferDetail.Description, dbo.RSTransferDetail.CostSetID, CostSet.CostSetCode, dbo.RSTransferDetail.ContractID, EMContract.Code AS ContractCode, NULL 
                         AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL AS AccountingObjectCode, dbo.RSTransferDetail.EmployeeID, dbo.RSTransfer.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, 
                         dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, RSTransferDetail.Quantity, RSTransferDetail.UnitPrice, dbo.RSTransfer.Recorded, 
                         dbo.RSTransfer.TotalAmountOriginal, dbo.RSTransfer.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'RSTransfer' AS RefTable
FROM            dbo.RSTransfer INNER JOIN
                         dbo.RSTransferDetail ON dbo.RSTransfer.ID = dbo.RSTransferDetail.RSTransferID INNER JOIN
                         dbo.Type ON RSTransfer.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.MaterialGoods ON RSTransferDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON RSTransferDetail.ToRepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = RSTransferDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = RSTransferDetail.ContractID
UNION ALL
SELECT        dbo.PPInvoice.ID, dbo.PPInvoiceDetail.ID AS DetailID, dbo.PPInvoice.TypeID, dbo.Type.TypeName, dbo.PPInvoice.No, dbo.PPInvoice.InwardNo AS InwardNo, NULL AS OutwardNo, dbo.PPInvoice.Date, 
                         dbo.PPInvoice.PostedDate, dbo.PPInvoiceDetail.DebitAccount, dbo.PPInvoiceDetail.CreditAccount, PPInvoice.CurrencyID, dbo.PPInvoiceDetail.AmountOriginal, dbo.PPInvoiceDetail.Amount, NULL AS OrgPrice, 
                         dbo.PPInvoice.Reason, dbo.PPInvoiceDetail.Description, dbo.PPInvoiceDetail.CostSetID, CostSet.CostSetCode, dbo.PPInvoiceDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPInvoice.EmployeeID, 
                         dbo.PPInvoice.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, 
                         dbo.Repository.RepositoryName, dbo.PPInvoiceDetail.DepartmentID, NULL AS DepartmentName, PPInvoiceDetail.VATAccount, PPInvoiceDetail.VATAmountOriginal, PPInvoiceDetail.VATAmount, 
                         PPInvoiceDetail.Quantity, PPInvoiceDetail.UnitPrice, dbo.PPInvoice.Recorded, dbo.PPInvoice.TotalAmountOriginal, dbo.PPInvoice.TotalAmount, dbo.PPInvoiceDetail.DiscountAmountOriginal, 
                         dbo.PPInvoiceDetail.DiscountAmount, NULL AS DiscountAccount, dbo.PPInvoiceDetail.FreightAmountOriginal, dbo.PPInvoiceDetail.FreightAmount, 0 AS DetailTax, PPInvoiceDetail.InwardAmountOriginal, 
                         PPInvoiceDetail.InwardAmount, dbo.PPInvoice.StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPInvoice' AS RefTable
FROM            dbo.PPInvoice INNER JOIN
                         dbo.PPInvoiceDetail ON dbo.PPInvoice.ID = dbo.PPInvoiceDetail.PPInvoiceID INNER JOIN
                         dbo.Type ON PPInvoice.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPInvoiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPInvoiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON PPInvoiceDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPInvoiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPInvoiceDetail.ContractID
UNION ALL
SELECT        dbo.PPDiscountReturn.ID, dbo.PPDiscountReturnDetail.ID AS DetailID, dbo.PPDiscountReturn.TypeID, dbo.Type.TypeName, dbo.PPDiscountReturn.No, NULL AS InwardNo, 
                         dbo.PPDiscountReturn.OutwardNo AS OutwardNo, dbo.PPDiscountReturn.Date, dbo.PPDiscountReturn.PostedDate, dbo.PPDiscountReturnDetail.DebitAccount, dbo.PPDiscountReturnDetail.CreditAccount, 
                         PPDiscountReturn.CurrencyID, dbo.PPDiscountReturnDetail.AmountOriginal, dbo.PPDiscountReturnDetail.Amount, NULL AS OrgPrice, dbo.PPDiscountReturn.Reason, dbo.PPDiscountReturnDetail.Description, 
                         dbo.PPDiscountReturnDetail.CostSetID, CostSet.CostSetCode, dbo.PPDiscountReturnDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPDiscountReturn.EmployeeID, 
                         dbo.PPDiscountReturn.BrachID AS BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, 
                         dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, PPDiscountReturnDetail.VATAccount, PPDiscountReturnDetail.VATAmountOriginal, 
                         PPDiscountReturnDetail.VATAmount, PPDiscountReturnDetail.Quantity, PPDiscountReturnDetail.UnitPrice, dbo.PPDiscountReturn.Recorded, dbo.PPDiscountReturn.TotalAmountOriginal, 
                         dbo.PPDiscountReturn.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPDiscountReturn' AS RefTable
FROM            dbo.PPDiscountReturn INNER JOIN
                         dbo.PPDiscountReturnDetail ON dbo.PPDiscountReturn.ID = dbo.PPDiscountReturnDetail.PPDiscountReturnID INNER JOIN
                         dbo.Type ON PPDiscountReturn.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPDiscountReturnDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPDiscountReturnDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON PPDiscountReturnDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPDiscountReturnDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPDiscountReturnDetail.ContractID
UNION ALL
SELECT        dbo.PPService.ID, dbo.PPServiceDetail.ID AS DetailID, dbo.PPService.TypeID, dbo.Type.TypeName, dbo.PPService.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.PPService.Date, dbo.PPService.PostedDate, 
                         dbo.PPServiceDetail.DebitAccount, dbo.PPServiceDetail.CreditAccount, PPService.CurrencyID, dbo.PPServiceDetail.AmountOriginal, dbo.PPServiceDetail.Amount, NULL AS OrgPrice, dbo.PPService.Reason, 
                         dbo.PPServiceDetail.Description, dbo.PPServiceDetail.CostSetID, CostSet.CostSetCode, dbo.PPServiceDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPService.EmployeeID, dbo.PPService.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, 
                         PPServiceDetail.VATAccount, PPServiceDetail.VATAmountOriginal, PPServiceDetail.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.PPService.Recorded, dbo.PPService.TotalAmountOriginal, 
                         dbo.PPService.TotalAmount, PPServiceDetail.DiscountAmountOriginal, PPServiceDetail.DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL
                          AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPService' AS RefTable
FROM            dbo.PPService INNER JOIN
                         dbo.PPServiceDetail ON dbo.PPService.ID = dbo.PPServiceDetail.PPServiceID INNER JOIN
                         dbo.Type ON PPService.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPServiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPServiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPServiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPServiceDetail.ContractID
UNION ALL
SELECT        dbo.PPOrder.ID, dbo.PPOrderDetail.ID AS DetailID, dbo.PPOrder.TypeID, dbo.Type.TypeName, dbo.PPOrder.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.PPOrder.Date, 
                         dbo.PPOrder.DeliverDate AS PostedDate, dbo.PPOrderDetail.DebitAccount, dbo.PPOrderDetail.CreditAccount, PPOrder.CurrencyID, dbo.PPOrderDetail.AmountOriginal, dbo.PPOrderDetail.Amount, NULL 
                         AS OrgPrice, dbo.PPOrder.Reason, dbo.PPOrderDetail.Description, dbo.PPOrderDetail.CostSetID, CostSet.CostSetCode, dbo.PPOrderDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPOrder.EmployeeID, 
                         dbo.PPOrder.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, PPOrderDetail.VATAccount, PPOrderDetail.VATAmountOriginal, PPOrderDetail.VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.PPOrder.Exported AS Recorded, dbo.PPOrder.TotalAmountOriginal, dbo.PPOrder.TotalAmount, PPOrderDetail.DiscountAmountOriginal, PPOrderDetail.DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL 
                         AS OutwardAmount, 'PPOrder' AS RefTable
FROM            dbo.PPOrder INNER JOIN
                         dbo.PPOrderDetail ON dbo.PPOrder.ID = dbo.PPOrderDetail.PPOrderID INNER JOIN
                         dbo.Type ON PPOrder.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPOrderDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPOrderDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPOrderDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPOrderDetail.ContractID
UNION ALL
SELECT        dbo.SAInvoice.ID, dbo.SAInvoiceDetail.ID AS DetailID, dbo.SAInvoice.TypeID, dbo.Type.TypeName, dbo.SAInvoice.No, NULL AS InwardNo, dbo.SAInvoice.OutwardNo AS OutwardNo, dbo.SAInvoice.Date, 
                         dbo.SAInvoice.PostedDate, dbo.SAInvoiceDetail.DebitAccount, dbo.SAInvoiceDetail.CreditAccount, SAInvoice.CurrencyID, dbo.SAInvoiceDetail.AmountOriginal, dbo.SAInvoiceDetail.Amount, NULL AS OrgPrice, 
                         dbo.SAInvoice.Reason, dbo.SAInvoiceDetail.Description, dbo.SAInvoiceDetail.CostSetID, CostSet.CostSetCode, dbo.SAInvoiceDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAInvoice.EmployeeID, 
                         dbo.SAInvoice.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, 
                         dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, SAInvoiceDetail.VATAccount, SAInvoiceDetail.VATAmountOriginal, SAInvoiceDetail.VATAmount, SAInvoiceDetail.Quantity, 
                         SAInvoiceDetail.UnitPrice, dbo.SAInvoice.Recorded, dbo.SAInvoice.TotalAmountOriginal, dbo.SAInvoice.TotalAmount, dbo.SAInvoiceDetail.DiscountAmountOriginal, dbo.SAInvoiceDetail.DiscountAmount, 
                         dbo.SAInvoiceDetail.DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, 
                         dbo.SAInvoiceDetail.OWAmountOriginal AS OutwardAmountOriginal, dbo.SAInvoiceDetail.OWAmount AS OutwardAmount, 'SAInvoice' AS RefTable
FROM            dbo.SAInvoice INNER JOIN
                         dbo.SAInvoiceDetail ON dbo.SAInvoice.ID = dbo.SAInvoiceDetail.SAInvoiceID INNER JOIN
                         dbo.Type ON dbo.SAInvoice.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAInvoiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAInvoiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAInvoiceDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAInvoiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAInvoiceDetail.ContractID
UNION ALL
SELECT        dbo.SAReturn.ID, dbo.SAReturnDetail.ID AS DetailID, dbo.SAReturn.TypeID, dbo.Type.TypeName, dbo.SAReturn.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.SAReturn.Date, dbo.SAReturn.PostedDate, NULL 
                         AS DebitAccount, NULL AS CreditAccount, SAReturn.CurrencyID, dbo.SAReturnDetail.AmountOriginal, dbo.SAReturnDetail.Amount, NULL AS OrgPrice, dbo.SAReturn.Reason, dbo.SAReturnDetail.Description, 
                         dbo.SAReturnDetail.CostSetID, CostSet.CostSetCode, dbo.SAReturnDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAReturn.EmployeeID, dbo.SAReturn.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, SAReturnDetail.VATAccount, SAReturnDetail.VATAmountOriginal, SAReturnDetail.VATAmount, SAReturnDetail.Quantity, SAReturnDetail.UnitPrice, SAReturn.Recorded AS Posted, 
                         dbo.SAReturn.TotalAmountOriginal, dbo.SAReturn.TotalAmount, dbo.SAReturnDetail.DiscountAmountOriginal, dbo.SAReturnDetail.DiscountAmount, dbo.SAReturnDetail.DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL 
                         AS OutwardAmount, 'SAReturn' AS RefTable
FROM            dbo.SAReturn INNER JOIN
                         dbo.SAReturnDetail ON dbo.SAReturn.ID = dbo.SAReturnDetail.SAReturnID INNER JOIN
                         dbo.Type ON dbo.SAReturn.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAReturnDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAReturnDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAReturnDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAReturnDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAReturnDetail.ContractID
UNION ALL
SELECT        dbo.SAOrder.ID, dbo.SAOrderDetail.ID AS DetailID, dbo.SAOrder.TypeID, dbo.Type.TypeName, dbo.SAOrder.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.SAOrder.Date, 
                         dbo.SAOrder.DeliveDate AS PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, SAOrder.CurrencyID, dbo.SAOrderDetail.AmountOriginal, dbo.SAOrderDetail.Amount, NULL AS OrgPrice, 
                         dbo.SAOrder.Reason, dbo.SAOrderDetail.Description, dbo.SAOrderDetail.CostSetID, CostSet.CostSetCode, dbo.SAOrderDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAOrder.EmployeeID, 
                         dbo.SAOrder.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, 
                         dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, SAOrderDetail.VATAccount, SAOrderDetail.VATAmountOriginal, SAOrderDetail.VATAmount, SAOrderDetail.Quantity, 
                         SAOrderDetail.UnitPrice, SAOrder.Exported AS Recorded, dbo.SAOrder.TotalAmountOriginal, dbo.SAOrder.TotalAmount, dbo.SAOrderDetail.DiscountAmountOriginal, dbo.SAOrderDetail.DiscountAmount, 
                         dbo.SAOrderDetail.DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'SAOrder' AS RefTable
FROM            dbo.SAOrder INNER JOIN
                         dbo.SAOrderDetail ON dbo.SAOrder.ID = dbo.SAOrderDetail.SAOrderID INNER JOIN
                         dbo.Type ON dbo.SAOrder.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAOrderDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAOrderDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAOrderDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAOrderDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAOrderDetail.ContractID
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*

*/
ALTER FUNCTION [dbo].[Func_SoDu]
      ( @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT 
      )
RETURNS @Result TABLE
      (
        AccountID UNIQUEIDENTIFIER ,
        DetailByAccountObject BIT ,
        AccountObjectType INT ,
		AccountObjectID NVARCHAR(50) ,
        AccountNumber NVARCHAR(10) ,
        AccountName NVARCHAR(255) ,
        AccountNameEnglish NVARCHAR(255) ,
        AccountCategoryKind INT ,
        IsParent BIT ,
        ParentID UNIQUEIDENTIFIER ,
        Grade INT ,
        AccountKind INT ,
        SortOrder INT ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
      )
AS
    BEGIN
        DECLARE @MainCurrency NVARCHAR(3)
        SET @MainCurrency = 'VND'	
        /*ngày bắt đầu chương trình*/
        DECLARE @StartDate DATETIME

        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'                
           BEGIN
					
                 DECLARE @GeneralLedger TABLE
                         (
                           AccountNumber NVARCHAR(30) ,
                           AccountObjectID UNIQUEIDENTIFIER ,
                           BranchID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(3) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4) ,
                           OpenningDebitAmountOC DECIMAL(25, 4) ,
                           DebitAmountOC DECIMAL(25, 4) ,
                           CreditAmountOC DECIMAL(25, 4) ,
                           DebitAmountOCAccum DECIMAL(25, 4) ,
                           CreditAmountOCAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedger
                        SELECT  G.*
                        FROM    (
                                  SELECT    Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                                     ELSE 0
                                                END) AS OpenningDebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal - GL.CreditAmountOriginal
                                                     ELSE 0
                                                END) AS OpenningDebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOCAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOCAccum
                                  FROM      dbo.GeneralLedger AS GL
                                  WHERE    GL.PostedDate <= @ToDate
                                  GROUP BY  Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID
                                ) G
			
                 DECLARE @Data TABLE
                         (
                           AccountNumber NVARCHAR(50) ,
						   AccountObjectID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(20) ,
                           SortOrder INT ,
                           OpeningDebitAmount DECIMAL(25, 4) ,
                           OpeningCreditAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,/*Nợ lũy kế*/
                           CreditAmountAccum DECIMAL(25, 4) ,/*có lũy kế*/
                           ClosingDebitAmount DECIMAL(25, 4) ,
                           ClosingCreditAmount DECIMAL(25, 4)
                         )
	
                 INSERT @Data
                        SELECT  F.AccountNumber ,
						        F.AccountObjectID,
                                '' ,
                                0 ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount > 0
                                                 ) THEN F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount < 0
                                                 ) THEN -1 * F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount ,
                                SUM(F.DebitAmount) AS DebitAmount ,
                                SUM(F.CreditAmount) AS CreditAmount ,
                                SUM(F.DebitAmountAccum) AS DebitAmountAccum ,
                                SUM(F.CreditAmountAccum) AS CreditAmountAccum ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                                 ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                                 ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.AccountNumber ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            GL.AccountObjectID AccountObjectID ,
                                            A.AccountGroupKind AccountCategoryKind ,
                                            GL.BranchID BranchID
                                  FROM      @GeneralLedger AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber = A.AccountNumber
                                  WHERE     GL.AccountNumber NOT LIKE '0%'
                                            AND A.IsParentNode = 0
                                  GROUP BY  A.AccountNumber ,
                                            GL.AccountObjectID,
                                            A.AccountGroupKind ,
                                            GL.BranchID
                                  HAVING    SUM(OpenningDebitAmount) <> 0
                                            OR SUM(DebitAmount) <> 0
                                            OR SUM(CreditAmount) <> 0
                                ) AS F
                        GROUP BY F.AccountNumber ,
                                F.AccountCategoryKind,
								F.AccountObjectID
                 OPTION ( RECOMPILE )		         					    
                 INSERT @Result
                        SELECT  A.ID ,
						        null,
								null,
								D.AccountObjectID,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                0 AccountKind,
                                D.SortOrder ,
                                SUM(D.OpeningDebitAmount) ,
                                SUM(D.OpeningCreditAmount) ,
                                SUM(D.DebitAmount) ,
                                SUM(D.CreditAmount) ,
                                SUM(D.DebitAmountAccum) ,
                                SUM(D.CreditAmountAccum) ,
                                SUM(D.ClosingDebitAmount) ,
                                SUM(D.ClosingCreditAmount)
                        FROM    @Data D
                        INNER JOIN dbo.Account A ON D.AccountNumber LIKE A.AccountNumber + '%'
                        WHERE   A.Grade <= @MaxAccountGrade
                        GROUP BY A.ID ,
                                A.AccountNumber ,
								D.AccountObjectID,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                D.SortOrder
                        HAVING  SUM(D.OpeningDebitAmount) <> 0
                                OR SUM(D.OpeningCreditAmount) <> 0
                                OR SUM(D.DebitAmount) <> 0
                                OR SUM(D.CreditAmount) <> 0
                                OR SUM(D.ClosingDebitAmount) <> 0
                                OR SUM(D.ClosingCreditAmount) <> 0
                 OPTION ( RECOMPILE )
           END
        RETURN
    END



GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER FUNCTION [dbo].[Func_GetBalanceAccountF01_v1]
      (
        @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT ,
        @IsBalanceBothSide BIT
      )
RETURNS @Result TABLE
      (
        AccountID UNIQUEIDENTIFIER ,
        DetailByAccountObject BIT ,
        AccountObjectType INT ,
        AccountNumber NVARCHAR(50) ,
        AccountName NVARCHAR(255) ,
        AccountNameEnglish NVARCHAR(255) ,
        AccountCategoryKind INT ,
        IsParent BIT ,
        ParentID UNIQUEIDENTIFIER ,
        Grade INT ,
        AccountKind INT ,
        SortOrder INT ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
      )
AS
    BEGIN
	   DECLARE @MainCurrency NVARCHAR(3)
        SET @MainCurrency = 'VND'	
        /*ngày bắt đầu chương trình*/
        DECLARE @StartDate DATETIME
        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'
        IF @IsBalanceBothSide = 1
           BEGIN
                 DECLARE @GeneralLedger TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           AccountObjectID UNIQUEIDENTIFIER ,
                           BranchID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(3) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4) ,
                           OpenningDebitAmountOC DECIMAL(25, 4) ,
                           DebitAmountOC DECIMAL(25, 4) ,
                           CreditAmountOC DECIMAL(25, 4) ,
                           DebitAmountOCAccum DECIMAL(25, 4) ,
                           CreditAmountOCAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedger
                        SELECT  G.*
                        FROM    (
                                  SELECT    Account ,
                                            null as AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                                     ELSE 0
                                                END) AS OpenningDebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal - GL.CreditAmountOriginal
                                                     ELSE 0
                                                END) AS OpenningDebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOCAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOCAccum
                                  FROM      dbo.GeneralLedger AS GL
                                  WHERE    GL.PostedDate <= @ToDate
                                  GROUP BY  Account ,
                                            GL.BranchID ,
                                            GL.CurrencyID
                                ) G
			
                 DECLARE @Data TABLE
                         (
                           AccountNumber NVARCHAR(50) ,
						   AccountObjectID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(20) ,
                           SortOrder INT ,
                           OpeningDebitAmount DECIMAL(25, 4) ,
                           OpeningCreditAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,/*Nợ lũy kế*/
                           CreditAmountAccum DECIMAL(25, 4) ,/*có lũy kế*/
                           ClosingDebitAmount DECIMAL(25, 4) ,
                           ClosingCreditAmount DECIMAL(25, 4)
                         )
	
                 INSERT @Data
                        SELECT  F.AccountNumber ,
						        F.AccountObjectID,
                                '' ,
                                0 ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount > 0
                                                 ) THEN F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount < 0
                                                 ) THEN -1 * F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount ,
                                SUM(F.DebitAmount) AS DebitAmount ,
                                SUM(F.CreditAmount) AS CreditAmount ,
                                SUM(F.DebitAmountAccum) AS DebitAmountAccum ,
                                SUM(F.CreditAmountAccum) AS CreditAmountAccum ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                                 ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                                 ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.AccountNumber ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            GL.AccountObjectID AccountObjectID ,
                                            A.AccountGroupKind AccountCategoryKind ,
                                            GL.BranchID BranchID
                                  FROM      @GeneralLedger AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber = A.AccountNumber
                                  WHERE     GL.AccountNumber NOT LIKE '0%'
                                  GROUP BY  A.AccountNumber ,
                                            GL.AccountObjectID,
                                            A.AccountGroupKind ,
                                            GL.BranchID
                                  HAVING    SUM(OpenningDebitAmount) <> 0
                                            OR SUM(DebitAmount) <> 0
                                            OR SUM(CreditAmount) <> 0
                                ) AS F
                        GROUP BY F.AccountNumber ,
                                F.AccountCategoryKind,
								F.AccountObjectID
                 OPTION ( RECOMPILE )		         					    
                 INSERT @Result
                        SELECT  A.ID ,
						        null,
								null,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                0 AccountKind,
                                D.SortOrder ,
                                SUM(D.OpeningDebitAmount) ,
                                SUM(D.OpeningCreditAmount) ,
                                SUM(D.DebitAmount) ,
                                SUM(D.CreditAmount) ,
                                SUM(D.DebitAmountAccum) ,
                                SUM(D.CreditAmountAccum) ,
                                SUM(D.ClosingDebitAmount) ,
                                SUM(D.ClosingCreditAmount)
                        FROM    @Data D
                        INNER JOIN dbo.Account A ON D.AccountNumber LIKE A.AccountNumber + '%'
                        WHERE   A.Grade <= @MaxAccountGrade
                        GROUP BY A.ID ,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                D.SortOrder
                        HAVING  SUM(D.OpeningDebitAmount) <> 0
                                OR SUM(D.OpeningCreditAmount) <> 0
                                OR SUM(D.DebitAmount) <> 0
                                OR SUM(D.CreditAmount) <> 0
                                OR SUM(D.ClosingDebitAmount) <> 0
                                OR SUM(D.ClosingCreditAmount) <> 0
                 OPTION ( RECOMPILE )
           END
        ELSE
           BEGIN
                 DECLARE @GeneralLedgerP1 TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedgerP1
                        SELECT  Account ,
                                SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmountAccum ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmountAccum
                        FROM    dbo.GeneralLedger AS GL
                        WHERE   GL.PostedDate <= @ToDate
                        GROUP BY Account
                 INSERT @Result
                        SELECT  A.ID ,
                                null DetailByAccountObject ,
                                null AccountObjectType,
                                A.AccountNumber ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade AS Grade ,
                                F.AccountKind,
                                F.SortOrder ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount > 0
                                               ) THEN F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount < 0
                                               ) THEN -1 * F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount ,
                                ( F.DebitAmount ) AS DebitAmount ,
                                ( F.CreditAmount ) AS CreditAmount ,
                                ( F.DebitAmountAccum ) AS DebitAmountAccum ,
                                ( F.CreditAmountAccum ) AS CreditAmountAccum ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                               ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                               ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.ID ,
                                            null DetailByAccountObject,
                                            null AccountObjectType,
                                            A.AccountNumber ,
                                            '' AS CurrencyID ,
                                            0 AS SortOrder ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            0 AS AccountKind
                                  FROM      @GeneralLedgerP1 AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber LIKE A.AccountNumber + '%'
                                  WHERE     A.Grade <= @MaxAccountGrade
                                            AND (
                                                  OpenningDebitAmount <> 0
                                                  OR DebitAmount <> 0
                                                  OR CreditAmount <> 0
                                                )
                                  GROUP BY  A.ID ,
                                            A.AccountNumber      
                                ) AS F
                        INNER JOIN Account A ON F.AccountNumber = A.AccountNumber
                 OPTION ( RECOMPILE )      
           END
        RETURN
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER FUNCTION [dbo].[Func_GetBalanceAccountF01]
      (
        @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT ,
        @IsBalanceBothSide BIT
      )
RETURNS @Result TABLE
      (
        AccountID UNIQUEIDENTIFIER ,
        DetailByAccountObject BIT ,
        AccountObjectType INT ,
        AccountNumber NVARCHAR(50) ,
        AccountName NVARCHAR(255) ,
        AccountNameEnglish NVARCHAR(255) ,
        AccountCategoryKind INT ,
        IsParent BIT ,
        ParentID UNIQUEIDENTIFIER ,
        Grade INT ,
        AccountKind INT ,
        SortOrder INT ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
      )
AS
    BEGIN
	   DECLARE @MainCurrency NVARCHAR(3)
        SET @MainCurrency = 'VND'	
        /*ngày bắt đầu chương trình*/
        DECLARE @StartDate DATETIME
        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'
        IF @IsBalanceBothSide = 1
           BEGIN
                 DECLARE @GeneralLedger TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           AccountObjectID UNIQUEIDENTIFIER ,
                           BranchID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(3) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4) ,
                           OpenningDebitAmountOC DECIMAL(25, 4) ,
                           DebitAmountOC DECIMAL(25, 4) ,
                           CreditAmountOC DECIMAL(25, 4) ,
                           DebitAmountOCAccum DECIMAL(25, 4) ,
                           CreditAmountOCAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedger
                        SELECT  G.*
                        FROM    (
                                  SELECT    Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                                     ELSE 0
                                                END) AS OpenningDebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal - GL.CreditAmountOriginal
                                                     ELSE 0
                                                END) AS OpenningDebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOCAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOCAccum
                                  FROM      dbo.GeneralLedger AS GL
                                  WHERE    GL.PostedDate <= @ToDate
                                  GROUP BY  Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID
                                ) G
			
                 DECLARE @Data TABLE
                         (
                           AccountNumber NVARCHAR(50) ,
						   AccountObjectID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(20) ,
                           SortOrder INT ,
                           OpeningDebitAmount DECIMAL(25, 4) ,
                           OpeningCreditAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,/*Nợ lũy kế*/
                           CreditAmountAccum DECIMAL(25, 4) ,/*có lũy kế*/
                           ClosingDebitAmount DECIMAL(25, 4) ,
                           ClosingCreditAmount DECIMAL(25, 4)
                         )
	
                 INSERT @Data
                        SELECT  F.AccountNumber ,
						        null,
                                '' ,
                                0 ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount > 0
                                                 ) THEN F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount < 0
                                                 ) THEN -1 * F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount ,
                                SUM(F.DebitAmount) AS DebitAmount ,
                                SUM(F.CreditAmount) AS CreditAmount ,
                                SUM(F.DebitAmountAccum) AS DebitAmountAccum ,
                                SUM(F.CreditAmountAccum) AS CreditAmountAccum ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                                 ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                                 ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.AccountNumber ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            CASE A.DetailByAccountObject
                                                                  WHEN 0 THEN NULL
                                                                  ELSE GL.AccountObjectID
                                                                END AS AccountObjectID ,
                                            A.AccountGroupKind AccountCategoryKind ,
                                            GL.BranchID BranchID
                                  FROM      @GeneralLedger AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber = A.AccountNumber
                                  WHERE     GL.AccountNumber NOT LIKE '0%'
                                            AND A.IsParentNode = 0
                                  GROUP BY  A.AccountNumber ,
                                            A.AccountGroupKind ,
                                            GL.BranchID,
											CASE A.DetailByAccountObject
                                                                  WHEN 0 THEN NULL
                                                                  ELSE GL.AccountObjectID
                                                                END
                                  HAVING    SUM(OpenningDebitAmount) <> 0
                                            OR SUM(DebitAmount) <> 0
                                            OR SUM(CreditAmount) <> 0
                                ) AS F
                        GROUP BY F.AccountNumber ,
                                F.AccountCategoryKind,
								F.AccountObjectID
                 OPTION ( RECOMPILE )		         					    
                 INSERT @Result
                        SELECT  A.ID ,
						        null,
								null,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                0 AccountKind,
                                D.SortOrder ,
                                SUM(D.OpeningDebitAmount) ,
                                SUM(D.OpeningCreditAmount) ,
                                SUM(D.DebitAmount) ,
                                SUM(D.CreditAmount) ,
                                SUM(D.DebitAmountAccum) ,
                                SUM(D.CreditAmountAccum) ,
                                SUM(D.ClosingDebitAmount) ,
                                SUM(D.ClosingCreditAmount)
                        FROM    @Data D
                        INNER JOIN dbo.Account A ON D.AccountNumber LIKE A.AccountNumber + '%'
                        WHERE   A.Grade <= @MaxAccountGrade
                        GROUP BY A.ID ,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                D.SortOrder
                        HAVING  SUM(D.OpeningDebitAmount) <> 0
                                OR SUM(D.OpeningCreditAmount) <> 0
                                OR SUM(D.DebitAmount) <> 0
                                OR SUM(D.CreditAmount) <> 0
                                OR SUM(D.ClosingDebitAmount) <> 0
                                OR SUM(D.ClosingCreditAmount) <> 0
                 OPTION ( RECOMPILE )
           END
        ELSE
           BEGIN
                 DECLARE @GeneralLedgerP1 TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedgerP1
                        SELECT  Account ,
                                SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmountAccum ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmountAccum
                        FROM    dbo.GeneralLedger AS GL
                        WHERE   GL.PostedDate <= @ToDate
                        GROUP BY Account
                 INSERT @Result
                        SELECT  A.ID ,
                                null DetailByAccountObject ,
                                null AccountObjectType,
                                A.AccountNumber ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade AS Grade ,
                                F.AccountKind,
                                F.SortOrder ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount > 0
                                               ) THEN F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount < 0
                                               ) THEN -1 * F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount ,
                                ( F.DebitAmount ) AS DebitAmount ,
                                ( F.CreditAmount ) AS CreditAmount ,
                                ( F.DebitAmountAccum ) AS DebitAmountAccum ,
                                ( F.CreditAmountAccum ) AS CreditAmountAccum ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                               ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                               ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.ID ,
                                            null DetailByAccountObject,
                                            null AccountObjectType,
                                            A.AccountNumber ,
                                            '' AS CurrencyID ,
                                            0 AS SortOrder ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            0 AS AccountKind
                                  FROM      @GeneralLedgerP1 AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber LIKE A.AccountNumber + '%'
                                  WHERE     A.Grade <= @MaxAccountGrade
                                            AND (
                                                  OpenningDebitAmount <> 0
                                                  OR DebitAmount <> 0
                                                  OR CreditAmount <> 0
                                                )
                                  GROUP BY  A.ID ,
                                            A.AccountNumber      
                                ) AS F
                        INNER JOIN Account A ON F.AccountNumber = A.AccountNumber
                 OPTION ( RECOMPILE )      
           END
        RETURN
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*

*/
ALTER PROCEDURE [dbo].[Proc_GetBalanceAccountF01]
      ( @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT,
		@IsBalanceBothSide BIT
		)
AS
BEGIN
 
SELECT t.AccountID ,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName,
			   sum(t.OpeningDebitAmount) OpeningDebitAmount,
			   sum(t.OpeningCreditAmount) OpeningCreditAmount,
			   sum(t.DebitAmount) DebitAmount,
			   sum(t.CreditAmount) CreditAmount,
			   sum(t.DebitAmountAccum) DebitAmountAccum,
			   sum(t.CreditAmountAccum) CreditAmountAccum,
			   sum(t.ClosingDebitAmount) ClosingDebitAmount,
			   sum(t.ClosingCreditAmount) ClosingCreditAmount
			   FROM [dbo].[Func_GetBalanceAccountF01] (
   @FromDate
  ,@ToDate
  ,@MaxAccountGrade
  ,@IsBalanceBothSide) t
  group by t.AccountID,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName
  order by t.AccountNumber
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*

*/
ALTER FUNCTION [dbo].[Func_GetB01_DN]
    (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT,
      @IsB01bDNN BIT ,
      @isPrintByYear BIT,
      @PrevFromDate DATETIME,
      @PrevToDate DATETIME
    )
RETURNS @Result TABLE
    (
      ItemID UNIQUEIDENTIFIER ,
      ItemCode NVARCHAR(25) ,
      ItemName NVARCHAR(255) ,
      ItemNameEnglish NVARCHAR(255) ,
      ItemIndex INT ,
      Description NVARCHAR(255) ,
      FormulaType INT ,
      FormulaFrontEnd NVARCHAR(MAX) ,
      Formula XML ,
      Hidden BIT ,
      IsBold BIT ,
      IsItalic BIT ,
      Category INT ,
      SortOrder INT
      ,
      Amount DECIMAL(25, 4) ,
      PrevAmount DECIMAL(25, 4)
    )

    BEGIN 
            DECLARE @AccountingSystem INT	
            SET @AccountingSystem = 48 
    
            DECLARE @SubAccountSystem INT
            SELECT  @SubAccountSystem = 133
	
            DECLARE @ReportID NVARCHAR(100)
            SET @ReportID = '1'

            IF @AccountingSystem = 48
                AND @SubAccountSystem = 133
                AND @IsB01bDNN = 0
                BEGIN

                    SET @ReportID = '7'	
                END

            DECLARE @ItemForeignCurrency UNIQUEIDENTIFIER
            DECLARE @ItemIndex INT
            DECLARE @MainCurrency NVARCHAR(3)
            IF @AccountingSystem = 48
                AND @SubAccountSystem <> 133
                SET @ItemForeignCurrency = '6D6BA7EB-E60A-46ED-A556-E674709F5466'
            ELSE
                SET @ItemForeignCurrency = '00000000-0000-0000-0000-000000000000'	
	
            SET @ItemIndex = ( SELECT TOP 1
                                        ItemIndex
                               FROM     FRTemplate
                               WHERE    ItemID = @ItemForeignCurrency
                             )
            SET @MainCurrency = 'VND'
            DECLARE @tblItem TABLE
                (
                  ItemID UNIQUEIDENTIFIER ,
                  ItemIndex INT ,
                  ItemName NVARCHAR(255) ,
                  OperationSign INT ,
                  OperandString NVARCHAR(255) ,
                  AccountNumberPercent NVARCHAR(25) ,
                  AccountNumber NVARCHAR(25) ,
                  CorrespondingAccountNumber VARCHAR(25) ,
                  IsDetailByAO INT ,
                  AccountKind INT
                )
	
            INSERT  @tblItem
                    SELECT  ItemID ,
                            ItemIndex ,
                            ItemName ,
                            x.r.value('@OperationSign', 'INT') ,
                            RTRIM(LTRIM(x.r.value('@OperandString',
                                                  'nvarchar(255)'))) ,
                            x.r.value('@AccountNumber', 'nvarchar(25)') + '%' ,
                            x.r.value('@AccountNumber', 'nvarchar(25)') ,
                            CASE WHEN x.r.value('@CorrespondingAccountNumber',
                                                'nvarchar(25)') <> ''
                                 THEN x.r.value('@CorrespondingAccountNumber',
                                                'nvarchar(25)') + '%'
                                 ELSE ''
                            END ,
                            CASE WHEN x.r.value('@OperandString',
                                                'nvarchar(255)') LIKE '%ChitietTheoTKvaDoituong'
                                 THEN 1
                                 ELSE CASE WHEN x.r.value('@OperandString',
                                                          'nvarchar(255)') LIKE '%ChitietTheoTK'
                                           THEN 2
                                           ELSE 0
                                      END
                            END ,
                            NULL
                    FROM    dbo.FRTemplate
                            CROSS APPLY Formula.nodes('/root/DetailFormula')
                            AS x ( r )
                    WHERE   AccountingSystem = @AccountingSystem
                            AND ReportID = @ReportID
                            AND FormulaType = 0
                            AND Formula IS NOT NULL
                            AND ItemID <> @ItemForeignCurrency
	
            UPDATE  @tblItem
            SET     AccountKind = A.AccountGroupKind
            FROM    dbo.Account A
            WHERE   A.AccountNumber = [@tblItem].AccountNumber
		
            DECLARE @AccountBalance TABLE
                (
                  AccountNumber NVARCHAR(20) ,
                  AccountCategoryKind INT ,
                  IsDetailByAO INT 
	            )
	 
            INSERT  @AccountBalance
                    SELECT 
		DISTINCT            A.AccountNumber ,
                            A.AccountGroupKind ,
                            B.IsDetailByAO
                    FROM    dbo.Account A
                            INNER JOIN @tblItem B ON A.AccountNumber LIKE B.AccountNumberPercent
	
            DECLARE @Balance TABLE
                (
                  AccountNumber NVARCHAR(20) ,
                  AccountCategoryKind INT ,
                  AccountObjectID UNIQUEIDENTIFIER ,
                  IsDetailByAO INT ,
                  OpeningDebit DECIMAL(25, 4) ,
                  OpeningCredit DECIMAL(25, 4) ,
                  ClosingDebit DECIMAL(25, 4) ,
                  ClosingCredit DECIMAL(25, 4) ,
                  BranchID UNIQUEIDENTIFIER
                )
            DECLARE @GeneralLedger TABLE
                (
                  AccountNumber NVARCHAR(20) ,
                  AccountObjectID UNIQUEIDENTIFIER ,
                  BranchID UNIQUEIDENTIFIER ,
                  IsOPN BIT ,
                  DebitAmount DECIMAL(25, 4) ,
                  CreditAmount DECIMAL(25, 4)
                )
	
            INSERT  INTO @GeneralLedger
                    ( AccountNumber ,
                      AccountObjectID ,
                      BranchID ,
                      IsOPN ,
                      DebitAmount ,
                      CreditAmount
	                )
                    SELECT  GL.Account ,
                            GL.AccountingObjectID ,
                            GL.BranchID ,
                            CASE WHEN GL.PostedDate < @FromDate THEN 1
                                 ELSE 0
                            END ,
                            SUM(DebitAmount) AS DebitAmount ,
                            SUM(CreditAmount) AS CreditAmount
                    FROM    dbo.GeneralLedger GL
                    WHERE   PostedDate <= @ToDate
                    GROUP BY GL.Account ,
                            GL.AccountingObjectID ,
                            GL.BranchID ,
                            CASE WHEN GL.PostedDate < @FromDate THEN 1
                                 ELSE 0
                            END
	
	
            DECLARE @StartDate DATETIME
			SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'
	 
            DECLARE @GeneralLedger421 TABLE
                (
                  OperandString NVARCHAR(255) ,
                  ClosingAmount DECIMAL(25, 4) ,
                  OpeningAmount DECIMAL(25, 4)
                )
	
	
	
	
	/* 
	 

Đối với báo cáo kỳ khác năm:
*Cuối kỳ: (Dư Có – Dư Nợ) TK 4211 trên sổ cái tính đến Từ ngày -1 + (Dư Có – Dư Nợ) TK 4212 trên sổ cái tính đến Từ ngày -1 
* Đầu kỳ: 
(Dư Có – Dư Nợ) TK 4211 trên sổ cái tính đến từ ngày -1 của kỳ trước liền kề+ (Dư Có – Dư Nợ) TK 4212 trên sổ cái tính đến Từ ngày -1 của kỳ trước liền kề. 
→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, 
VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10) thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4211 – Dư Nợ TK 4211 trên Sổ cái tính đến Từ ngày -1 

	 */
	 /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
            INSERT  INTO @GeneralLedger421
                    ( OperandString ,
                      ClosingAmount ,
                      OpeningAmount
	                )
                    SELECT  N'Solieuchitieu421a_Ky_khac_nam' AS AccountNumber ,
                            ISNULL( SUM(CASE WHEN GL.Account LIKE '4211%'
                                          AND GL.PostedDate < @FromDate
                                     THEN CreditAmount - DebitAmount
                                     ELSE 0
                                END),0)
                         
                            + ISNULL( SUM(CASE WHEN GL.Account LIKE '4212%'
                                            AND GL.PostedDate < @FromDate
                                       THEN CreditAmount -DebitAmount
                                       ELSE 0
                                  END),0)
                           AS ClosingAmount ,
                                  
                            ISNULL( SUM(
									CASE WHEN DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0 ELSE 
										CASE WHEN GL.Account LIKE '4211%'
													  AND GL.PostedDate < @PrevFromDate
												 THEN CreditAmount -DebitAmount
												 ELSE 0
											END
									end		
                                ),0)
                         
                            + SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0
                                       ELSE CASE WHEN GL.Account LIKE '4212%'
                                                      AND GL.PostedDate < @PrevFromDate
                                                 THEN CreditAmount -DebitAmount
                                                 ELSE 0
                                            END
                                  END)
                     
                        +   ISNULL(SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate) 
									  AND GL.Account LIKE '4211%'
												 THEN CASE WHEN GL.PostedDate < @FromDate then CreditAmount - DebitAmount ELSE 0 end
									 ELSE 0
								END)
								,0) AS OpeningAmount                                  
                                  
                    FROM    dbo.GeneralLedger GL
                    WHERE   PostedDate <= @ToDate
                            AND gl.Account LIKE '421%'
	
	/*
	

Đối với báo cáo kỳ khác năm:
* Cuối kỳ: PS Có TK 4212 trên sổ cái trong kỳ báo cáo (không bao gồm ĐU N4211/Có TK 4212) - PSN TK 4212 trên sổ cái trong kỳ báo cáo (không bao gồm ĐU N4212/Có TK 4211)
* Đầu kỳ: 
PS Có TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4211/Có TK 4212) – PS Nợ TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4212/Có TK 4211)
→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10) thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4212 – Dư Nợ TK 4212 trên Sổ cái tính đến Từ ngày -1 

	*/
	/*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
	
            INSERT  INTO @GeneralLedger421
                    ( OperandString ,
                      ClosingAmount ,
                      OpeningAmount
	                )
                    SELECT  N'Solieuchitieu421b_Ky_khac_nam' AS AccountNumber ,
                           ISNULL( SUM(CASE WHEN GL.Account LIKE '4212%'
                                          AND GL.PostedDate BETWEEN @fromdate AND @ToDate
                                     THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                 THEN 0
                                                 ELSE CreditAmount
                                            END )
                                     ELSE 0
                                END),0)
                            - ISNULL( SUM(CASE WHEN GL.Account LIKE '4212%'
                                            AND GL.PostedDate BETWEEN @Fromdate AND @ToDate
                                       THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                   THEN 0
                                                   ELSE DebitAmount
                                              END )
                                       ELSE 0
                                  END),0) AS ClosingAmount ,
		 /** Đầu kỳ: 
PS Có TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4211/Có TK 4212) – PS Nợ TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4212/Có TK 4211)
→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10)
 thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4212 – Dư Nợ TK 4212 trên Sổ cái tính đến Từ ngày -1 
*/
                            ISNULL( SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0
                                     ELSE ( CASE WHEN GL.Account LIKE '4212%'
                                                      AND GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                                 THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                             THEN 0
                                                             ELSE CreditAmount
                                                        END )
                                                 ELSE 0
                                            END )
                                END),0)
                            - ISNULL(  SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0
                                       ELSE CASE WHEN GL.Account LIKE '4212%'
                                                      AND GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                                 THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                             THEN 0
                                                             ELSE DebitAmount
                                                        END )
                                                 ELSE 0
                                            END
                                  END),0)
                            + /*nếu không  là kỳ đầu tiên
		→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, 
		VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10) thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4212 – Dư Nợ TK 4212 trên Sổ cái tính đến Từ ngày -1 
		*/
			ISNULL(SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate) 
                          AND GL.Account LIKE '4212%'
                     THEN CASE WHEN GL.PostedDate < @FromDate then CreditAmount - DebitAmount ELSE 0 end
                     ELSE 0
                END),0) AS OpeningAmount
                    FROM    dbo.GeneralLedger GL
                    WHERE   PostedDate <= @ToDate
                            AND gl.Account LIKE '421%'
	
            INSERT  INTO @Balance
                    SELECT  GL.AccountNumber ,
                            A.AccountCategoryKind ,
                            CASE WHEN A.IsDetailByAO = 1
                                 THEN GL.AccountObjectID
                                 ELSE NULL
                            END AS AccountObjectID ,
                            A.IsDetailByAO ,
                            SUM(CASE WHEN GL.IsOPN = 1
                                     THEN DebitAmount - CreditAmount
                                     ELSE 0
                                END) OpeningDebit ,
                            0 ,
                            SUM(DebitAmount - CreditAmount) ClosingDebit ,
                            0
                            ,
                            CASE WHEN A.IsDetailByAO = 1
                                      AND @IsSimilarBranch = 0
                                 THEN GL.BranchID
                                 ELSE NULL
                            END AS BranchID
                    FROM    @GeneralLedger GL
                            INNER JOIN @AccountBalance A ON GL.AccountNumber = A.AccountNumber
                    GROUP BY GL.AccountNumber ,
                            A.AccountCategoryKind ,
                            CASE WHEN A.IsDetailByAO = 1
                                 THEN GL.AccountObjectID
                                 ELSE NULL
                            END ,
                            A.IsDetailByAO
                            ,
                            CASE WHEN A.IsDetailByAO = 1
                                      AND @IsSimilarBranch = 0
                                 THEN GL.BranchID
                                 ELSE NULL
                            END
            OPTION  ( RECOMPILE )
            UPDATE  @Balance
            SET     OpeningCredit = -OpeningDebit ,
                    OpeningDebit = 0
            WHERE   AccountCategoryKind = 1
                    OR ( AccountCategoryKind NOT IN ( 0, 1 )
                         AND OpeningDebit < 0
                       )
			
            UPDATE  @Balance
            SET     ClosingCredit = -ClosingDebit ,
                    ClosingDebit = 0
            WHERE   AccountCategoryKind = 1
                    OR ( AccountCategoryKind NOT IN ( 0, 1 )
                         AND ClosingDebit < 0
                       )	
	
            DECLARE @tblMasterDetail TABLE
                (
                  ItemID UNIQUEIDENTIFIER ,
                  DetailItemID UNIQUEIDENTIFIER ,
                  OperationSign INT ,
                  Grade INT ,
                  OpeningAmount DECIMAL(25, 4) ,
                  ClosingAmount DECIMAL(25, 4)
                )	
	
	
            INSERT  INTO @tblMasterDetail
                    SELECT  I.ItemID ,
                            NULL ,
                            1 ,
                            -1
                            ,
                            SUM(( CASE I.OperandString
                                    WHEN 'DUNO'
                                    THEN
                                         CASE I.AccountKind
                                           WHEN 1 THEN 0
                                           WHEN 0
                                           THEN I.OpeningDebit
                                                - I.OpeningCredit
                                           ELSE CASE WHEN I.OpeningDebit
                                                          - I.OpeningCredit > 0
                                                     THEN I.OpeningDebit
                                                          - I.OpeningCredit
                                                     ELSE 0
                                                END
                                         END
                                         /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUNO_ky_nam'
                                    THEN 
                                         CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.AccountKind
                                                     WHEN 1 THEN 0
                                                     WHEN 0
                                                     THEN I.OpeningDebit
                                                          - I.OpeningCredit
                                                     ELSE CASE
                                                              WHEN I.OpeningDebit
                                                              - I.OpeningCredit > 0
                                                              THEN I.OpeningDebit
                                                              - I.OpeningCredit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'DUCO'
                                    THEN CASE I.AccountKind
                                           WHEN 0 THEN 0
                                           WHEN 1
                                           THEN I.OpeningCredit
                                                - I.OpeningDebit
                                           ELSE CASE WHEN I.OpeningCredit
                                                          - I.OpeningDebit > 0
                                                     THEN I.OpeningCredit
                                                          - I.OpeningDebit
                                                     ELSE 0
                                                END
                                         END
                                         /* Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUCO_ky_nam'
                                    THEN CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.AccountKind
                                                     WHEN 0 THEN 0
                                                     WHEN 1
                                                     THEN I.OpeningCredit
                                                          - I.OpeningDebit
                                                     ELSE CASE
                                                              WHEN I.OpeningCredit
                                                              - I.OpeningDebit > 0
                                                              THEN I.OpeningCredit
                                                              - I.OpeningDebit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421a_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.OpeningAmount
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421b_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.OpeningAmount
                                              ELSE 0
                                         END
                                         /* - Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    ELSE CASE WHEN LEFT(I.OperandString, 4) = 'DUNO'
                                              THEN I.OpeningDebit
                                              ELSE I.OpeningCredit
                                         END
                                  END ) * I.OperationSign) AS OpeningAmount ,
                            SUM(( CASE I.OperandString
                                    WHEN 'DUNO'
                                    THEN CASE I.Accountkind
                                           WHEN 1 THEN 0
                                           WHEN 0
                                           THEN I.ClosingDebit
                                                - I.ClosingCredit
                                           ELSE CASE WHEN I.ClosingDebit
                                                          - I.ClosingCredit > 0
                                                     THEN I.ClosingDebit
                                                          - I.ClosingCredit
                                                     ELSE 0
                                                END
                                         END
                                         /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUNO_ky_nam'
                                    THEN CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.Accountkind
                                                     WHEN 1 THEN 0
                                                     WHEN 0
                                                     THEN I.ClosingDebit
                                                          - I.ClosingCredit
                                                     ELSE CASE
                                                              WHEN I.ClosingDebit
                                                              - I.ClosingCredit > 0
                                                              THEN I.ClosingDebit
                                                              - I.ClosingCredit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'DUCO'
                                    THEN CASE I.AccountKind
                                           WHEN 0 THEN 0
                                           WHEN 1
                                           THEN I.ClosingCredit
                                                - I.ClosingDebit
                                           ELSE CASE WHEN I.ClosingCredit
                                                          - I.ClosingDebit > 0
                                                     THEN I.ClosingCredit
                                                          - I.ClosingDebit
                                                     ELSE 0
                                                END
                                         END
                                         /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUCO_ky_nam'
                                    THEN CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.AccountKind
                                                     WHEN 0 THEN 0
                                                     WHEN 1
                                                     THEN I.ClosingCredit
                                                          - I.ClosingDebit
                                                     ELSE CASE
                                                              WHEN I.ClosingCredit
                                                              - I.ClosingDebit > 0
                                                              THEN I.ClosingCredit
                                                              - I.ClosingDebit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421a_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.ClosingAmount
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421b_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.ClosingAmount
                                              ELSE 0
                                         END
                                         /* - Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    ELSE CASE WHEN LEFT(I.OperandString, 4) = 'DUNO'
                                              THEN I.ClosingDebit
                                              ELSE I.ClosingCredit
                                         END
                                  END ) * I.OperationSign) AS ClosingAmount
                    FROM    ( SELECT    I.ItemID ,
                                        I.OperandString ,
                                        I.OperationSign ,
                                        I.AccountNumber ,
                                        I.AccountKind ,
                                        SUM(B.OpeningDebit) AS OpeningDebit ,
                                        SUM(B.OpeningCredit) AS OpeningCredit ,
                                        SUM(B.ClosingDebit) AS ClosingDebit ,
                                        SUM(B.ClosingCredit) AS ClosingCredit
                              FROM      @tblItem I
                                        INNER JOIN @Balance B ON B.AccountNumber LIKE I.AccountNumberPercent
                                                              AND B.IsDetailByAO = I.IsDetailByAO
                              GROUP BY  I.ItemID ,
                                        I.OperandString ,
                                        I.OperationSign ,
                                        I.AccountNumber ,
                                        I.AccountKind
                            ) I
                            /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                            LEFT JOIN @GeneralLedger421 C ON I.OperandString = C.OperandString
                    GROUP BY I.ItemID			
	
            INSERT  @tblMasterDetail
                    SELECT  ItemID ,
                            x.r.value('@ItemID', 'NVARCHAR(100)') ,
                            x.r.value('@OperationSign', 'INT') ,
                            0 ,
                            0.0 ,
                            0.0
                    FROM    dbo.FRTemplate
                            CROSS APPLY Formula.nodes('/root/MasterFormula')
                            AS x ( r )
                    WHERE   AccountingSystem = @AccountingSystem
                            AND ReportID = @ReportID
                            AND FormulaType = 1
                            AND Formula IS NOT NULL 
	
	;
            WITH    V ( ItemID, DetailItemID, OpeningAmount, ClosingAmount, OperationSign )
                      AS ( SELECT   ItemID ,
                                    DetailItemID ,
                                    OpeningAmount ,
                                    ClosingAmount ,
                                    OperationSign
                           FROM     @tblMasterDetail
                           WHERE    Grade = -1
                           UNION ALL
                           SELECT   B.ItemID ,
                                    B.DetailItemID ,
                                    V.OpeningAmount ,
                                    V.ClosingAmount ,
                                    B.OperationSign * V.OperationSign AS OperationSign
                           FROM     @tblMasterDetail B ,
                                    V
                           WHERE    B.DetailItemID = V.ItemID
                         )
	INSERT  @Result
            SELECT  FR.ItemID ,
                    FR.ItemCode ,
                    FR.ItemName ,
                    FR.ItemNameEnglish ,
                    FR.ItemIndex ,
                    FR.Description ,
                    FR.FormulaType ,
                    CASE WHEN FR.FormulaType = 1 THEN FR.FormulaFrontEnd
                         ELSE ''
                    END AS FormulaFrontEnd ,
                    CASE WHEN FR.FormulaType = 1 THEN FR.Formula
                         ELSE NULL
                    END AS Formula ,
                    FR.Hidden ,
                    FR.IsBold ,
                    FR.IsItalic ,
                    FR.Category ,
                    -1 AS SortOrder ,
                    ISNULL(X.Amount, 0) AS Amount ,
                    ISNULL(X.PrevAmount, 0) AS PrevAmount
            FROM    ( SELECT    ItemID ,
                                SUM(V.OperationSign * V.OpeningAmount) AS PrevAmount ,
                                SUM(V.OperationSign * V.ClosingAmount) AS Amount
                      FROM      V
                      GROUP BY  ItemID
                    ) AS X
                    RIGHT JOIN FRTemplate FR ON FR.ItemID = X.ItemID
            WHERE   AccountingSystem = @AccountingSystem
                    AND ReportID = @ReportID
	
            RETURN 
        END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*

*/
ALTER PROCEDURE [dbo].[Proc_GetB01_DN]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT
      ,
      @IsB01bDNN BIT ,
      @isPrintByYear BIT
      ,
      @PrevFromDate DATETIME
      ,
      @PrevToDate DATETIME
    )
AS
BEGIN
  
SELECT * FROM [dbo].[Func_GetB01_DN] (
    @BranchID
  ,@IncludeDependentBranch
  ,@FromDate
  ,@ToDate
  ,@IsSimilarBranch
  ,@IsB01bDNN
  ,@isPrintByYear
  ,@PrevFromDate
  ,@PrevToDate)
order by ItemIndex
end


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

ALTER PROCEDURE [dbo].[Proc_GLR_GetS03a2_DN]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @CurrencyID NVARCHAR(10) ,
    @AccountNumber NVARCHAR(25) ,
    @BankAccountID UNIQUEIDENTIFIER ,
    @IsSimilarSum BIT
AS
    BEGIN
        SET NOCOUNT ON;     
        CREATE TABLE #Result
            (
              BranchName NVARCHAR(255)  COLLATE SQL_Latin1_General_CP1_CI_AS,
              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefNo NVARCHAR(22) COLLATE SQL_Latin1_General_CP1_CI_AS,
              RefDate DATETIME ,
              [Description] NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS,
              AccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS,
              CorrespondingAccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS,
              Amount MONEY ,
              Col2 MONEY DEFAULT 0 ,
              Col3 MONEY DEFAULT 0 ,
              Col4 MONEY DEFAULT 0 ,
              Col5 MONEY DEFAULT 0 ,
              Col6 MONEY DEFAULT 0 ,
              Col7 MONEY DEFAULT 0 ,
              ColOtherAccount NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS,
              AccountNumberList NVARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS,
              /*Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
              SortOrder INT,
              DetailPostOrder int    
            )
                     
         /*tài khoản ngân hàng khác - lấy các phát sinh không chọn tài khoản ngân hàng*/		
        DECLARE @BankAccountOther UNIQUEIDENTIFIER
        SET @BankAccountOther = '12345678-2222-48B8-AE4B-5CF7FA7FB3F5'
        
        INSERT  #Result
                ( BranchName ,
                  RefType ,
                  RefID ,
                  PostedDate ,
                  RefNo ,
                  RefDate ,
                  Description ,
                  AccountNumber ,
                  CorrespondingAccountNumber ,
                  Amount,
                    SortOrder,
                  DetailPostOrder
                )
                SELECT  null as BranchName ,
                        GL.TypeID ,
                        GL.ReferenceID ,
                        GL.PostedDate ,
                        GL.RefNo ,
                        GL.RefDate ,
                        CASE WHEN @IsSimilarSum = 1 THEN GL.Reason
                             ELSE ISNULL(GL.[Description], GL.Reason)
                        END ,
                        @AccountNumber AS AccountNumber ,
                        GL.AccountCorresponding ,
                        GL.CreditAmount AS Amount,
                          /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        GL.OrderPriority,
                        null
                FROM    dbo.GeneralLedger AS GL
                        INNER JOIN dbo.Account AS A ON GL.Account = A.AccountNumber
                WHERE   PostedDate BETWEEN @FromDate AND @ToDate
                        AND GL.Account LIKE @AccountNumber + '%'
                        AND GL.CreditAmount <> 0
                        AND ( @CurrencyID IS NULL
                              OR GL.CurrencyID = @CurrencyID
                            )
                        AND ( @BankAccountID IS NULL
                              OR ( (GL.BankAccountDetailID = @BankAccountID
                                   AND @BankAccountID <> @BankAccountOther )
                                 )
                         
                              OR ( (GL.BankAccountDetailID IS  NULL
                                   AND @BankAccountID = @BankAccountOther )
                                 )
                            )
        IF EXISTS ( SELECT TOP 1
                            *
                    FROM    #Result )
            BEGIN
                DECLARE @AccountNumberAdded NVARCHAR(50)
                DECLARE @AccountNumberList NVARCHAR(MAX)
                DECLARE @Updatesql1 NVARCHAR(MAX)
                DECLARE @Updatesql2 NVARCHAR(MAX)
                DECLARE @tblName NVARCHAR(20)
                DECLARE @colNumber INT
                
                DECLARE @CoresAccountNumberList NVARCHAR(MAX)
        
                SET @tblName = '#Result'
                SET @Updatesql1 = ''
                SET @Updatesql2 = ''
                SET @colNumber = 2
                SET @AccountNumberList = @AccountNumber + ','
                SET @CoresAccountNumberList = ''
		
                DECLARE curAcc CURSOR FAST_FORWARD READ_ONLY
                FOR
                    SELECT DISTINCT TOP 4
                            R.CorrespondingAccountNumber
                    FROM    #Result AS R
                    WHERE   ISNULL(R.CorrespondingAccountNumber, '') <> ''
                    ORDER BY R.CorrespondingAccountNumber
                OPEN curAcc

                FETCH NEXT FROM curAcc INTO @AccountNumberAdded

                WHILE @@FETCH_STATUS = 0
                    BEGIN
                        IF @Updatesql1 <> ''
                            SET @Updatesql1 = @Updatesql1 + ', '
                        SET @Updatesql1 = @Updatesql1 + '[col'
                            + CONVERT(NVARCHAR(10), @colNumber) + ']'
                            + ' = (CASE WHEN ISNULL(CorrespondingAccountNumber,'''') = '''
                            + @AccountNumberAdded
                            + ''' THEN Amount Else 0 END)'                
                        IF @Updatesql2 <> ''
                            SET @Updatesql2 = @Updatesql2 + ' AND '
                        SET @Updatesql2 = @Updatesql2
                            + 'ISNULL(CorrespondingAccountNumber,'''') <> '''
                            + @AccountNumberAdded + ''' '
                        SET @AccountNumberList = @AccountNumberList
                            + @AccountNumberAdded + ','
                            
                            
                        SET @CoresAccountNumberList = @CoresAccountNumberList
                            + ',''' + @AccountNumberAdded + ''''
                        SET @colNumber = @colNumber + 1                          
                        FETCH NEXT FROM curAcc INTO @AccountNumberAdded
                
                    END
                CLOSE curAcc
                DEALLOCATE curAcc
                
			
                SET @Updatesql1 = 'UPDATE #Result SET ' + @Updatesql1
                    +
                    ', [col7] = (CASE WHEN ' + @Updatesql2
                    + ' THEN Amount ELSE 0 END)'
                    +
                    ', [ColOtherAccount] = (CASE WHEN ' + @Updatesql2
                    + ' THEN CorrespondingAccountNumber END)'
                 
       
                EXEC (@Updatesql1)
                UPDATE  #Result
                SET     AccountNumberList = @AccountNumberList          
            END
        
        IF @IsSimilarSum = 1
            BEGIN
                SELECT  BranchName ,
                        RefType ,
                        RefID ,
                        PostedDate ,
                        RefNo ,
                        RefDate ,
                        [Description] ,
                        AccountNumber ,
                        SUM(Amount) AS Amount ,
                        SUM(Col2) AS Col2 ,
                        SUM(Col3) AS Col3 ,
                        SUM(Col4) AS Col4 ,
                        SUM(Col5) AS Col5 ,
                        SUM(Col6) AS Col6 ,
                        SUM(Col7) AS Col7 ,
                        ISNULL(ColOtherAccount,
                               Temp1.CorrespondingAccountNumber) AS ColOtherAccount ,
                        AccountNumberList
                FROM    #Result AS R
                        OUTER APPLY ( SELECT TOP 1
                                                CorrespondingAccountNumber
                                      FROM      #Result AS R1
                                      WHERE     R1.RefID = R.RefID
                                                AND ColOtherAccount NOT IN (
                                                STUFF(@CoresAccountNumberList,
                                                      1, 1, '') )
                                      ORDER BY  CorrespondingAccountNumber
                                    ) AS Temp1
                GROUP BY BranchName ,
                        RefType ,
                        RefID ,
                        PostedDate ,
                        RefNo ,
                        RefDate ,
                        [Description] ,
                        AccountNumber ,
                        ISNULL(ColOtherAccount,
                               Temp1.CorrespondingAccountNumber) ,
                        AccountNumberList
                  /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */       
                ORDER BY 
						R.PostedDate ,
                        R.RefDate ,
                        R.RefNo      
            END
        ELSE
            BEGIN
            
                SELECT  *
                FROM    #Result AS R
                ORDER BY R.PostedDate ,
                        R.RefDate ,
                        R.RefNo ,
                          /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        R.SortOrder,
                        R.DetailPostOrder
            END

        DROP TABLE #Result
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

   
ALTER PROCEDURE [dbo].[Proc_GLR_GetS03a1_DN]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @CurrencyID NVARCHAR(10) ,
    @AccountNumber NVARCHAR(25) ,
    @BankAccountID UNIQUEIDENTIFIER ,
    @IsSimilarSum BIT
AS
    BEGIN
        SET NOCOUNT ON;
     
        CREATE TABLE #Result
            (
			
              BranchName NVARCHAR(255)  COLLATE SQL_Latin1_General_CP1_CI_AS,
              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefNo NVARCHAR(22) COLLATE SQL_Latin1_General_CP1_CI_AS,
              RefDate DATETIME ,
              [Description] NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS,
              AccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS,
              CorrespondingAccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS,
              Amount MONEY ,
              Col2 MONEY DEFAULT 0 ,
              Col3 MONEY DEFAULT 0 ,
              Col4 MONEY DEFAULT 0 ,
              Col5 MONEY DEFAULT 0 ,
              Col6 MONEY DEFAULT 0 ,
              Col7 MONEY DEFAULT 0 ,
              ColOtherAccount NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS,
              AccountNumberList NVARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS,
                  /*sửa lỗi 139606 -Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
              SortOrder INT,
              DetailPostOrder int
              
            )
                     
         /*tài khoản ngân hàng khác - lấy các phát sinh không chọn tài khoản ngân hàng*/		
        DECLARE @BankAccountOther UNIQUEIDENTIFIER
        SET @BankAccountOther = '12345678-2222-48B8-AE4B-5CF7FA7FB3F5'
        
        INSERT  #Result
                ( BranchName ,
                  RefType ,
                  RefID ,
                  PostedDate ,
                  RefNo ,
                  RefDate ,
                  Description ,
                  AccountNumber ,
                  CorrespondingAccountNumber ,
                  Amount,
                  SortOrder,
                  DetailPostOrder
                )
                SELECT  null as BranchName ,
                        GL.TypeID ,
                        GL.ReferenceID ,
                        GL.PostedDate ,
                        GL.RefNo ,
                        GL.RefDate ,
                        CASE WHEN @IsSimilarSum = 1 THEN GL.Reason
                             ELSE ISNULL(GL.[Description], GL.Reason)
                        END ,
                        @AccountNumber AS AccountNumber ,
                        GL.AccountCorresponding ,
                        GL.DebitAmount AS Amount,
                         /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        GL.OrderPriority,
                        null
                FROM    dbo.GeneralLedger AS GL
                        INNER JOIN dbo.Account AS A ON GL.Account = A.AccountNumber
                WHERE   PostedDate BETWEEN @FromDate AND @ToDate
                        AND GL.Account LIKE @AccountNumber + '%'
                        AND GL.DebitAmount <> 0
                        AND ( @CurrencyID IS NULL
                              OR GL.CurrencyID = @CurrencyID
                            )
                        AND ( @BankAccountID IS NULL
                              OR ( (GL.BankAccountDetailID = @BankAccountID
                                   AND @BankAccountID <> @BankAccountOther )
                                 )
                         
                              OR ( (GL.BankAccountDetailID IS  NULL
                                   AND @BankAccountID = @BankAccountOther )
                                 )
                            )

        IF EXISTS ( SELECT TOP 1
                            *
                    FROM    #Result )
            BEGIN
                DECLARE @AccountNumberAdded NVARCHAR(50)
                DECLARE @AccountNumberList NVARCHAR(MAX)
                DECLARE @Updatesql1 NVARCHAR(MAX)
                DECLARE @Updatesql2 NVARCHAR(MAX)
                DECLARE @tblName NVARCHAR(20)
                DECLARE @colNumber INT
                
                DECLARE @CoresAccountNumberList NVARCHAR(MAX)
        
                SET @tblName = '#Result'
                SET @Updatesql1 = ''
                SET @Updatesql2 = ''
                SET @colNumber = 2
                SET @AccountNumberList = @AccountNumber + ','
                SET @CoresAccountNumberList = ''
		
                DECLARE curAcc CURSOR FAST_FORWARD READ_ONLY
                FOR
                    SELECT DISTINCT TOP 4
                            R.CorrespondingAccountNumber
                    FROM    #Result AS R
                    WHERE   ISNULL(R.CorrespondingAccountNumber, '') <> ''
                    ORDER BY R.CorrespondingAccountNumber
                OPEN curAcc

                FETCH NEXT FROM curAcc INTO @AccountNumberAdded

                WHILE @@FETCH_STATUS = 0
                    BEGIN
                        IF @Updatesql1 <> ''
                            SET @Updatesql1 = @Updatesql1 + ', '
                        SET @Updatesql1 = @Updatesql1 + '[col'
                            + CONVERT(NVARCHAR(10), @colNumber) + ']'
                            + ' = (CASE WHEN ISNULL(CorrespondingAccountNumber,'''') = '''
                            + @AccountNumberAdded
                            + ''' THEN Amount Else 0 END)'                
                        IF @Updatesql2 <> ''
                            SET @Updatesql2 = @Updatesql2 + ' AND '
                        SET @Updatesql2 = @Updatesql2
                            + 'ISNULL(CorrespondingAccountNumber,'''') <> '''
                            + @AccountNumberAdded + ''' '
                        SET @AccountNumberList = @AccountNumberList
                            + @AccountNumberAdded + ','
                            
                            
                        SET @CoresAccountNumberList = @CoresAccountNumberList
                            + ',''' + @AccountNumberAdded + ''''
                        SET @colNumber = @colNumber + 1                          
                        FETCH NEXT FROM curAcc INTO @AccountNumberAdded
                
                    END
                CLOSE curAcc
                DEALLOCATE curAcc
                
			
                SET @Updatesql1 = 'UPDATE #Result SET ' + @Updatesql1
                    +
                    ', [col7] = (CASE WHEN ' + @Updatesql2
                    + ' THEN Amount ELSE 0 END)'
                    +
                    ', [ColOtherAccount] = (CASE WHEN ' + @Updatesql2
                    + ' THEN CorrespondingAccountNumber END)'
                 
        
                EXEC (@Updatesql1)
                UPDATE  #Result
                SET     AccountNumberList = @AccountNumberList          
            END
        
        IF @IsSimilarSum = 1
            BEGIN
                SELECT  BranchName ,
                        RefType ,
                        RefID ,
                        PostedDate ,
                        RefNo ,
                        RefDate ,
                        [Description] ,
                        AccountNumber ,
                        SUM(Amount) AS Amount ,
                        SUM(Col2) AS Col2 ,
                        SUM(Col3) AS Col3 ,
                        SUM(Col4) AS Col4 ,
                        SUM(Col5) AS Col5 ,
                        SUM(Col6) AS Col6 ,
                        SUM(Col7) AS Col7 ,
                        ISNULL(ColOtherAccount,
                               Temp1.CorrespondingAccountNumber) AS ColOtherAccount ,
                        AccountNumberList
                FROM    #Result AS R
                        OUTER APPLY ( SELECT TOP 1
                                                CorrespondingAccountNumber
                                      FROM      #Result AS R1
                                      WHERE     R1.RefID = R.RefID
                                                AND ColOtherAccount NOT IN (
                                                STUFF(@CoresAccountNumberList,
                                                      1, 1, '') )
                                      ORDER BY  CorrespondingAccountNumber
                                    ) AS Temp1
                GROUP BY BranchName ,
                        RefType ,
                        RefID ,
                        PostedDate ,
                        RefNo ,
                        RefDate ,
                        [Description] ,
                        AccountNumber ,
                        ISNULL(ColOtherAccount,
                               Temp1.CorrespondingAccountNumber) ,
                        AccountNumberList
                     /* -Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */       
                ORDER BY 
						R.PostedDate ,
                        R.RefDate ,
                        R.RefNo             
                       
                                    
            
            END
        ELSE
            BEGIN
            
            
                SELECT  *
                FROM    #Result AS R                                                
                ORDER BY R.PostedDate ,
                        R.RefDate ,
                        R.RefNo ,
                        /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        R.SortOrder,
                        R.DetailPostOrder
            END

        DROP TABLE #Result
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER FUNCTION [dbo].[Func_GetB03_DN]
(
	@BranchID UNIQUEIDENTIFIER
	,@IncludeDependentBranch BIT
	,@FromDate DATETIME
	,@ToDate DATETIME
	,@PrevFromDate DATETIME
	,@PrevToDate DATETIME	
)
RETURNS 
@Result TABLE 
(	
		ItemID UNIQUEIDENTIFIER
      ,ItemCode NVARCHAR(25)
      ,ItemName NVARCHAR(255)
      ,ItemNameEnglish NVARCHAR(255)
      ,ItemIndex INT
      ,Description NVARCHAR(255)
      ,FormulaType INT
      ,FormulaFrontEnd NVARCHAR(MAX)
      ,Formula XML
      ,Hidden BIT
      ,IsBold BIT
      ,IsItalic BIT
      ,Amount DECIMAL(25,4)
      ,PrevAmount  DECIMAL(25,4)    
)
AS
BEGIN

	DECLARE @AccountingSystem INT	
    SET @AccountingSystem = 48
    
    set @PrevToDate = DATEADD(DD,-1,@FromDate)
	declare @CalPrevDate nvarchar(20)
	declare @CalPrevMonth nvarchar(20)
	declare @CalPrevDay nvarchar(20)
	set @CalPrevDate = CAST(day(@FromDate) AS varchar) + CAST(month(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevMonth = CAST(month(@FromDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevDay = CAST(day(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar)
	if @CalPrevDate = '113112'
	 set @PrevFromDate = convert(DATETIME,dbo.ctod('D',dbo.godtos('M', -12, @FromDate)),103)
	else if (@CalPrevMonth in (55,77,1010,1212) and @CalPrevDay in (131) )
	 set @PrevFromDate = DATEADD(DD,-30,@FromDate)
	else if (@CalPrevMonth in (11,22,88,44,66,99,1111) and @CalPrevDay in (131,130) )
	 set @PrevFromDate = DATEADD(DD,-31,@FromDate)
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 = 0)
	 set @PrevFromDate = DATEADD(DD,-29,@FromDate)
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 <> 0)
	 set @PrevFromDate = DATEADD(DD,-28,@FromDate)
	else if ( (@CalPrevMonth in (13,1012) and @CalPrevDay = '131') or (@CalPrevMonth in (46,79) and @CalPrevDay = '130') )
	 set @PrevFromDate = DATEADD(QQ,-1,@FromDate)
	else set @PrevFromDate = DATEADD(DD,-1-DATEDIFF(day, @FromDate, @ToDate),@FromDate)

	DECLARE @ReportID NVARCHAR(100)
	SET @ReportID = '3'
	
	DECLARE @ItemBeginingCash UNIQUEIDENTIFIER	

	SET @ItemBeginingCash = (Select ItemID From FRTemplate Where ItemCode = '60' AND ReportID = 3 AND AccountingSystem = @AccountingSystem)

	DECLARE @tblItem TABLE
	(
		ItemID				UNIQUEIDENTIFIER
		,ItemIndex			INT
		,ItemName			NVARCHAR(255)	
		,OperationSign		INT
		,OperandString		NVARCHAR(255)	
		,AccountNumber		NVARCHAR(25)
		,AccountNumberPercent NVARCHAR(25) 
		,CorrespondingAccountNumber	VARCHAR(25)
	)
	
	INSERT @tblItem
	SELECT ItemID 
		, ItemIndex
		, ItemName
		, x.r.value('@OperationSign','INT')
		, x.r.value('@OperandString','nvarchar(255)')
		, x.r.value('@AccountNumber','nvarchar(25)') 
		, x.r.value('@AccountNumber','nvarchar(25)') + '%'
		, CASE WHEN x.r.value('@CorrespondingAccountNumber','nvarchar(25)') <>'' 
			THEN x.r.value('@CorrespondingAccountNumber','nvarchar(25)') + '%'
			ELSE ''
		 END
	FROM dbo.FRTemplate
	CROSS APPLY Formula.nodes('/root/DetailFormula') AS x(r)
	WHERE AccountingSystem = @AccountingSystem
		  AND ReportID = @ReportID
		  AND FormulaType = 0 AND Formula IS NOT NULL 
		  
	DECLARE @Balance TABLE 
		(
			AccountNumber NVARCHAR(25)	
			,CorrespondingAccountNumber NVARCHAR(25)		
			,PrevBusinessAmount DECIMAL(25,4)
			,BusinessAmount DECIMAL(25,4)							
			,PrevInvestmentAmount DECIMAL(25,4)
			,InvestmentAmount DECIMAL(25,4)
			,PrevFinancialAmount DECIMAL(25,4)
			,FinancialAmount DECIMAL(25,4)
		)			
		
	INSERT INTO @Balance			
	SELECT 
		GL.Account
		,GL.AccountCorresponding		
		,SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND(AC.ActivityID IS NULL OR AC.ActivityID = 0) 
				THEN DebitAmount  ELSE 0 END) AS PrevBusinessAmount
		,SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND (AC.ActivityID IS NULL OR AC.ActivityID = 0) 
				THEN DebitAmount ELSE 0 END) AS BusinessAmount		
		,SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND  AC.ActivityID = 1 
				THEN DebitAmount  ELSE 0 END) AS PrevInvestmentAmount
		,SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 1 
				THEN DebitAmount ELSE 0 END) AS InvestmentAmount		
		,SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND AC.ActivityID = 2 
				THEN DebitAmount  ELSE 0 END) AS PrevFinancialAmount
		,SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 2 
				THEN DebitAmount ELSE 0 END) AS FinancialAmount		
	FROM dbo.GeneralLedger GL
		INNER JOIN dbo.Account A ON GL.Account = A.AccountNumber
		LEFT JOIN [dbo].[FRB03ReportDetailActivity] AC   ON GL.DetailID = Ac.RefDetailID
													/*Khi JOIN thêm điều kiện sổ*/
	WHERE PostedDate BETWEEN @PrevFromDate AND @ToDate	
	and GL.No <> 'OPN'
	GROUP BY
		GL.Account
		,GL.AccountCorresponding
	HAVING
		SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND(AC.ActivityID IS NULL OR AC.ActivityID = 0) 
			THEN DebitAmount  ELSE 0 END)<>0
		OR SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND (AC.ActivityID IS NULL OR AC.ActivityID = 0) 
			THEN DebitAmount ELSE 0 END) <>0
		OR SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND  AC.ActivityID = 1 
			THEN DebitAmount  ELSE 0 END) <>0
		OR SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 1 
			THEN DebitAmount ELSE 0 END)<>0
		OR SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND AC.ActivityID = 2 
			THEN DebitAmount  ELSE 0 END) <>0
		OR SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 2 
			THEN DebitAmount ELSE 0 END) <>0
	
	DECLARE @DebitBalance TABLE
	(	
		ItemID				UNIQUEIDENTIFIER
		,OperationSign		INT
		,AccountNumber NVARCHAR(25)
		,AccountKind	INT
		,PrevDebitAmount Decimal(25,4)
		,DebitAmount Decimal(25,4)
	)
	
	INSERT @DebitBalance
	SELECT
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
		,SUM(CASE WHEN  GL.PostedDate < @PrevFromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0 END)	
		,SUM(GL.DebitAmount - GL.CreditAmount)		
	FROM  dbo.GeneralLedger AS GL
	INNER JOIN @tblItem I ON GL.Account LIKE I.AccountNumberPercent	
	LEFT JOIN dbo.Account A ON A.AccountNumber = I.AccountNumber
	WHERE GL.PostedDate < @FromDate	
	    AND I.OperandString = 'DUNO'
	    AND I.ItemID = @ItemBeginingCash
	Group By
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
	
	UNION ALL	
	SELECT
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
		,SUM(CASE WHEN  GL.PostedDate <= @PrevToDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0 END)	
		,SUM(GL.DebitAmount - GL.CreditAmount)		
	FROM  dbo.GeneralLedger AS GL
	INNER JOIN @tblItem I ON GL.Account LIKE I.AccountNumberPercent	
	LEFT JOIN dbo.Account A ON A.AccountNumber = I.AccountNumber
	WHERE GL.PostedDate <= @ToDate
	    AND I.OperandString = 'DUNO'
	    AND I.ItemID <> @ItemBeginingCash
	Group By
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
		
		   
	UPDATE @DebitBalance
	SET PrevDebitAmount = 0 WHERE AccountKind = 1 OR (AccountKind NOT IN (0,1)  AND PrevDebitAmount < 0) 
	
	UPDATE @DebitBalance
	SET DebitAmount = 0 WHERE AccountKind = 1 OR (AccountKind NOT IN (0,1)  AND DebitAmount < 0) 
	
	DECLARE @tblMasterDetail Table
		(
			ItemID UNIQUEIDENTIFIER
			,DetailItemID UNIQUEIDENTIFIER
			,OperationSign INT
			,Grade INT
			,PrevAmount DECIMAL(25,4)
			,Amount DECIMAL(25,4)		
		)	
	
	
	INSERT INTO @tblMasterDetail
	SELECT I.ItemID
			,NULL
			,1
			,-1
			,SUM(I.PrevAmount)
			,SUM(I.Amount)
	FROM	
		(SELECT			
			I.ItemID
			,SUM(CASE				
					WHEN I.OperandString = 'PhatsinhDU' THEN b.PrevBusinessAmount + b.PrevInvestmentAmount + b.PrevFinancialAmount
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_DAUTU' THEN b.PrevInvestmentAmount
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_TAICHINH' THEN b.PrevFinancialAmount
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_SXKD' THEN b.PrevBusinessAmount
				 END * I.OperationSign) AS  PrevAmount			
			,SUM(CASE 
					WHEN I.OperandString = 'PhatsinhDU' THEN (b.BusinessAmount + b.InvestmentAmount + b.FinancialAmount)
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_DAUTU' THEN (b.InvestmentAmount)
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_TAICHINH' THEN (b.FinancialAmount)
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_SXKD' THEN (b.BusinessAmount)
			    END * I.OperationSign) AS Amount
			FROM @tblItem I 			
				 INNER JOIN @Balance B 
					ON (B.AccountNumber like I.AccountNumberPercent)
					AND (B.CorrespondingAccountNumber like I.CorrespondingAccountNumber)	
				WHERE I.OperandString IN ('PhatsinhDU','PhatsinhDUChiTietTheoHD_DAUTU','PhatsinhDUChiTietTheoHD_TAICHINH','PhatsinhDUChiTietTheoHD_SXKD')					
			GROUP BY I.ItemID
		UNION ALL
			SELECT
		    I.ItemID
			,SUM((b.PrevBusinessAmount + b.PrevInvestmentAmount + b.PrevFinancialAmount)* I.OperationSign) AS  PrevAmount			
			,SUM((b.BusinessAmount + b.InvestmentAmount + b.FinancialAmount)* I.OperationSign) AS Amount
			FROM @tblItem I 
				 INNER JOIN @Balance B 
					ON (B.AccountNumber like I.AccountNumberPercent)
			WHERE I.OperandString = 'PhatsinhNO'				
			GROUP BY I.ItemID
		UNION ALL
			SELECT
		    I.ItemID
			,SUM((b.PrevBusinessAmount + b.PrevInvestmentAmount + b.PrevFinancialAmount)* I.OperationSign) AS  PrevAmount			
			,SUM((b.BusinessAmount + b.InvestmentAmount + b.FinancialAmount)* I.OperationSign) AS Amount
			FROM @tblItem I 
				 INNER JOIN @Balance B 
					ON (B.CorrespondingAccountNumber like I.AccountNumberPercent)
			WHERE I.OperandString = 'PhatsinhCO'		
					
			GROUP BY I.ItemID
			
		UNION ALL
			SELECT I.ItemID
				,I.PrevDebitAmount * I.OperationSign
				,I.DebitAmount * I.OperationSign
			FROM @DebitBalance I
		 ) AS I
	Group By I.ITemID
		
	
		
	
	       
	    
	INSERT @tblMasterDetail	
	SELECT ItemID 
	, x.r.value('@ItemID','NVARCHAR(100)')
	, x.r.value('@OperationSign','INT')	
	, 0
	, 0.0	
	, 0.0
	FROM dbo.FRTemplate 
	CROSS APPLY Formula.nodes('/root/MasterFormula') AS x(r)
	WHERE AccountingSystem = @AccountingSystem
		  AND ReportID = @ReportID
		  AND FormulaType = 1 AND Formula IS NOT NULL 
	
	;
	WITH V(ItemID,DetailItemID,PrevAmount,Amount,OperationSign)
		AS 
		(
			SELECT ItemID,DetailItemID,PrevAmount, Amount, OperationSign
			FROM @tblMasterDetail WHERE Grade = -1 
			UNION ALL
			SELECT B.ItemID
				, B.DetailItemID				
				, V.PrevAmount
				, V.Amount
				, B.OperationSign * V.OperationSign AS OperationSign
			FROM @tblMasterDetail B, V 
			WHERE B.DetailItemID = V.ItemID	
		)
		
	INSERT @Result      
	SELECT 
		FR.ItemID
		, FR.ItemCode
		, FR.ItemName
		, FR.ItemNameEnglish
		, FR.ItemIndex
		, FR.Description		
		, FR.FormulaType
		, CASE WHEN FR.FormulaType = 1 THEN FR.FormulaFrontEnd	ELSE ''	END AS FormulaFrontEnd
		, CASE WHEN FR.FormulaType = 1 THEN FR.Formula ELSE NULL END AS Formula
		, FR.Hidden
		, FR.IsBold
		, FR.IsItalic
		,CASE WHEN FR.Formula IS NOT NULL THEN ISNULL(X.Amount,0)
			ELSE X.Amount
		 END AS Amount
		,CASE WHEN FR.Formula IS NOT NULL THEN ISNULL(X.PrevAmount,0) 
			ELSE X.PrevAmount 
		END AS PrevAmount
		FROM		
		(
			SELECT V.ItemID
				,SUM(V.OperationSign * V.Amount)  AS Amount
				,SUM(V.OperationSign * V.PrevAmount) AS PrevAmount
			FROM V
			GROUP BY ItemID
		) AS X		
		RIGHT JOIN dbo.FRTemplate FR ON FR.ItemID = X.ItemID	
	WHERE AccountingSystem = @AccountingSystem
		  AND ReportID = @ReportID
		  and Hidden = 0
	Order by ItemIndex	
	RETURN 
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*

*/
ALTER PROCEDURE [dbo].[Proc_GetB03_DN]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @PrevFromDate DATETIME ,
      @PrevToDate DATETIME
    )
AS
BEGIN
  
SELECT * FROM [dbo].[Func_GetB03_DN] (
    @BranchID  ,
      @IncludeDependentBranch  ,
      @FromDate  ,
      @ToDate  ,
      @PrevFromDate  ,
      @PrevToDate )
order by ItemIndex
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER FUNCTION [dbo].[Func_GetB02_DN]
    (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @PrevFromDate DATETIME ,
      @PrevToDate DATETIME
    )
RETURNS @Result TABLE
    (
      ItemID UNIQUEIDENTIFIER ,
      ItemCode NVARCHAR(25) ,
      ItemName NVARCHAR(255) ,
      ItemNameEnglish NVARCHAR(255) ,
      ItemIndex INT ,
      Description NVARCHAR(255) ,
      FormulaType INT ,
      FormulaFrontEnd NVARCHAR(MAX) ,
      Formula XML ,
      Hidden BIT ,
      IsBold BIT ,
      IsItalic BIT
      ,
      Amount DECIMAL(25, 4) ,
      PrevAmount DECIMAL(25, 4)
    )
AS 
    BEGIN
	DECLARE @AccountingSystem INT	
            SET @AccountingSystem = 48
        DECLARE @ReportID NVARCHAR(100)
        SET @ReportID = '2'/*báo cáo kết quả HĐ kinh doanh*/
    set @PrevToDate = DATEADD(DD,-1,@FromDate)
	declare @CalPrevDate nvarchar(20)
	declare @CalPrevMonth nvarchar(20)
	declare @CalPrevDay nvarchar(20)
	set @CalPrevDate = CAST(day(@FromDate) AS varchar) + CAST(month(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevMonth = CAST(month(@FromDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevDay = CAST(day(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar)
	if @CalPrevDate = '113112'
	 set @PrevFromDate = convert(DATETIME,dbo.ctod('D',dbo.godtos('M', -12, @FromDate)),103)
	else if (@CalPrevMonth in (55,77,1010,1212) and @CalPrevDay in (131) )
	 set @PrevFromDate = DATEADD(DD,-30,@FromDate)
	else if (@CalPrevMonth in (11,22,88,44,66,99,1111) and @CalPrevDay in (131,130) )
	 set @PrevFromDate = DATEADD(DD,-31,@FromDate)
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 = 0)
	 set @PrevFromDate = DATEADD(DD,-29,@FromDate)
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 <> 0)
	 set @PrevFromDate = DATEADD(DD,-28,@FromDate)
	else if ( (@CalPrevMonth in (13,1012) and @CalPrevDay = '131') or (@CalPrevMonth in (46,79) and @CalPrevDay = '130') )
	 set @PrevFromDate = DATEADD(QQ,-1,@FromDate)
	else set @PrevFromDate = DATEADD(DD,-1-DATEDIFF(day, @FromDate, @ToDate),@FromDate)
	
        DECLARE @tblItem TABLE
            (
              ItemID UNIQUEIDENTIFIER ,
              ItemName NVARCHAR(255) ,
              OperationSign INT ,
              OperandString NVARCHAR(255) ,
              AccountNumber VARCHAR(25) ,
              CorrespondingAccountNumber VARCHAR(25) ,
              IsDetailGreaterThanZero BIT
	      )
	
        INSERT  @tblItem
                SELECT  ItemID ,
                        ItemName ,
                        x.r.value('@OperationSign', 'INT') ,
                        x.r.value('@OperandString', 'nvarchar(255)') ,
                        x.r.value('@AccountNumber', 'nvarchar(25)') + '%' ,
                        CASE WHEN x.r.value('@CorrespondingAccountNumber',
                                            'nvarchar(25)') <> ''
                             THEN x.r.value('@CorrespondingAccountNumber',
                                            'nvarchar(25)') + '%'
                             ELSE ''
                        END ,
                        CASE WHEN FormulaType = 0 THEN CAST(0 AS BIT)
                             ELSE CAST(1 AS BIT)
                        END
                FROM    FRTemplate
                        CROSS APPLY Formula.nodes('/root/DetailFormula') AS x ( r )
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
                        AND ( FormulaType = 0/*chỉ tiêu chi tiết*/
                              OR FormulaType = 2/*chỉ tiêu chi tiết chỉ được lấy số liệu khi kết quả>0*/
                            )
                        AND Formula IS NOT NULL
	
        DECLARE @Balance TABLE
            (
              AccountNumber NVARCHAR(25) ,
              CorrespondingAccountNumber NVARCHAR(25) ,
              PostedDate DATETIME ,
              CreditAmount DECIMAL(25, 4) ,
              DebitAmount DECIMAL(25, 4) ,
              CreditAmountDetailBy DECIMAL(25, 4) ,
              DebitAmountDetailBy DECIMAL(25, 4) ,
              IsDetailBy BIT,
              /*add by hoant 16.09.2016 theo cr 116741*/
                CreditAmountByBussinessType0 DECIMAL(25, 4) ,/*Có Chi tiết theo loại  0-Chiết khấu thương mai*/
               CreditAmountByBussinessType1 DECIMAL(25, 4) ,/*Có Chi tiết theo loại  1 - Giảm giá hàng bán*/
               CreditAmountByBussinessType2 DECIMAL(25, 4) ,/*Có Chi tiết theo loại   2- trả lại hàng bán*/
              DebitAmountByBussinessType0 DECIMAL(25, 4) ,/*Nợ Chi tiết theo loại  0-Chiết khấu thương mai*/
              DebitAmountByBussinessType1 DECIMAL(25, 4) ,/*Nợ Chi tiết theo loại  1 - Giảm giá hàng bán*/
              DebitAmountByBussinessType2 DECIMAL(25, 4) /*Nợ Chi tiết theo loại   2- trả lại hàng bán*/
            )			
/*
1. Doanh thu bán hàng và cung cấp dịch vụ

PS Có TK 511 (không kể PSĐƯ N911/C511, không kể các nghiệp vụ: Chiết khấu thương mại (bán hàng),

 Giảm giá hàng bán, Trả lại hàng bán)
  – PS Nợ TK 511 (không kể PSĐƯ N511/911, không kể các nghiệp vụ: Chiết khấu thương mại (bán hàng), 
  Giảm giá hàng bán, Trả lại hàng bán)
*/
		
        INSERT  INTO @Balance
                SELECT  GL.Account ,
                        GL.AccountCorresponding ,
                        GL.PostedDate ,
                        SUM(ISNULL(CreditAmount, 0)) AS CreditAmount ,
                        SUM(ISNULL(DebitAmount, 0)) AS DebitAmount ,
                        0 AS CreditAmountDetailBy ,
                        0 AS DebitAmountDetailBy ,
                        0,
                        SUM(CASE WHEN (GL.TypeID in ('320','321','322','323','324','325','350','360') and gl.Account like '511%')
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType0 ,
                        SUM(CASE WHEN GL.TypeID =340
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType1 
                            ,
                        SUM(CASE WHEN GL.TypeID =330
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType2 ,
                        SUM(CASE WHEN (GL.TypeID in ('320','321','322','323','324','325','350','360') and gl.Account like '511%')
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType0
                            ,
                        SUM(CASE WHEN GL.TypeID =340
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType1
                            ,
                        SUM(CASE WHEN GL.TypeID =330
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType2
                FROM    dbo.GeneralLedger GL
                WHERE   PostedDate BETWEEN @PrevFromDate AND @ToDate
                GROUP BY GL.Account ,
                        GL.AccountCorresponding,
                        GL.PostedDate                     
        OPTION  ( RECOMPILE )


        DECLARE @tblMasterDetail TABLE
            (
              ItemID UNIQUEIDENTIFIER ,
              DetailItemID UNIQUEIDENTIFIER ,
              OperationSign INT ,
              Grade INT ,
              DebitAmount DECIMAL(25, 4) ,
              PrevDebitAmount DECIMAL(25, 4)
            )	
	
	
	/*
	PhatsinhCO(511) - PhatsinhDU(911/511) + PhatsinhCO_ChitietChietKhauThuongmai(511)
	 + PhatsinhCO_ChitietGiamgiaHangBan(511) + PhatsinhCO_ChitietTralaiHangBan(511) +
	  PhatsinhNO(511) - PhatsinhDU(511/911) - PhatsinhNO_ChitietChietKhauThuongmai(511) - PhatsinhNO_ChitietGiamgiaHangBan(511) - PhatsinhNO_ChitietTralaiHangBan(511)
	*/
        INSERT  INTO @tblMasterDetail
                SELECT  I.ItemID ,
                        NULL ,
                        1 ,
                        -1
                        ,
                        CASE WHEN I.IsDetailGreaterThanZero = 0
                                  OR ( SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                                THEN ( CASE WHEN I.OperandString = 'PhatsinhCO'
                                                            THEN GL.CreditAmount
                                                            WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                            THEN GL.DebitAmount
                                                        
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                        
                                                            WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
															 THEN GL.CreditAmount
															 WHEN I.OperandString IN (
																  'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
															 THEN GL.DebitAmount
															 ELSE 0
														
                                                       END ) * I.OperationSign
                                                ELSE 0
                                           END) ) > 0
                             THEN ( SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                             THEN ( CASE WHEN I.OperandString = 'PhatsinhCO'
                                                         THEN GL.CreditAmount
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                         THEN GL.DebitAmount
                                                    
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                     
                                                              
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.CreditAmount
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.DebitAmount
                                                         ELSE 0
                                                    END ) * I.OperationSign
                                             ELSE 0
                                        END) )
                             ELSE 0
                        END AS DebitAmount ,
                        CASE WHEN I.IsDetailGreaterThanZero = 0
                                  OR ( SUM(CASE WHEN GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                                THEN CASE WHEN I.OperandString = 'PhatsinhCO'
                                                          THEN GL.CreditAmount
                                                          WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                          THEN GL.DebitAmount
            
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                         
                                                          WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.CreditAmount
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.DebitAmount
                                                         ELSE 0    
                                                     END * I.OperationSign
                                                ELSE 0
                                           END) ) > 0
                             THEN SUM(CASE WHEN GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                           THEN CASE WHEN I.OperandString = 'PhatsinhCO'
                                                     THEN GL.CreditAmount
                                                     WHEN I.OperandString IN (
                                                          'PhatsinhNO',
                                                          'PhatsinhDU' )
                                                     THEN GL.DebitAmount
                                                     
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                              
                                                              
                                                              
                                                     WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.CreditAmount
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.DebitAmount
                                                         ELSE 0
                                                END * I.OperationSign
                                           ELSE 0
                                      END)
                             ELSE 0
                        END AS DebitAmount
                FROM    @tblItem I
                        INNER JOIN @Balance AS GL ON ( GL.AccountNumber LIKE I.AccountNumber )
                                                     AND ( I.OperandString <> 'PhatsinhDU'
                                                           OR ( I.OperandString = 'PhatsinhDU'
                                                              AND GL.CorrespondingAccountNumber LIKE I.CorrespondingAccountNumber
                                                              )
                                                         )
                GROUP BY I.ItemID ,
                        I.IsDetailGreaterThanZero
        OPTION  ( RECOMPILE )
			
        INSERT  @tblMasterDetail
                SELECT  ItemID ,
                        x.r.value('@ItemID', 'UNIQUEIDENTIFIER') ,
                        x.r.value('@OperationSign', 'INT') ,
                        0 ,
                        0.0 ,
                        0.0
                FROM    FRTemplate
                        CROSS APPLY Formula.nodes('/root/MasterFormula') AS x ( r )
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
                        AND FormulaType = 1
                        AND Formula IS NOT NULL 
	
	

	;
        WITH    V ( ItemID, DetailItemID, DebitAmount, PrevDebitAmount, OperationSign )
                  AS ( SELECT   ItemID ,
                                DetailItemID ,
                                DebitAmount ,
                                PrevDebitAmount ,
                                OperationSign
                       FROM     @tblMasterDetail
                       WHERE    Grade = -1
                       UNION ALL
                       SELECT   B.ItemID ,
                                B.DetailItemID ,
                                V.DebitAmount ,
                                V.PrevDebitAmount ,
                                B.OperationSign * V.OperationSign AS OperationSign
                       FROM     @tblMasterDetail B ,
                                V
                       WHERE    B.DetailItemID = V.ItemID
                     )
	INSERT    @Result
                SELECT  FR.ItemID ,
                        FR.ItemCode ,
                        FR.ItemName ,
                        FR.ItemNameEnglish ,
                        FR.ItemIndex ,
                        FR.Description ,
                        FR.FormulaType ,
                        CASE WHEN FR.FormulaType = 1 THEN FR.FormulaFrontEnd
                             ELSE ''
                        END AS FormulaFrontEnd ,
                        CASE WHEN FR.FormulaType = 1 THEN FR.Formula
                             ELSE NULL
                        END AS Formula ,
                        FR.Hidden ,
                        FR.IsBold ,
                        FR.IsItalic ,
                        ISNULL(X.Amount, 0) AS Amount ,
                        ISNULL(X.PrevAmount, 0) AS PrevAmount
                FROM    ( SELECT    V.ItemID ,
                                    SUM(V.OperationSign * V.DebitAmount) AS Amount ,
                                    SUM(V.OperationSign * V.PrevDebitAmount) AS PrevAmount
                          FROM      V
                          GROUP BY  ItemID
                        ) AS X
                        RIGHT JOIN dbo.FRTemplate FR ON FR.ItemID = X.ItemID
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
                ORDER BY ItemIndex
	
        RETURN 
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*

*/
ALTER PROCEDURE [dbo].[Proc_GetB02_DN]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @PrevFromDate DATETIME ,
      @PrevToDate DATETIME
    )
AS
BEGIN
  
SELECT * FROM [dbo].[Func_GetB02_DN] (
    @BranchID  ,
      @IncludeDependentBranch  ,
      @FromDate  ,
      @ToDate  ,
      @PrevFromDate  ,
      @PrevToDate )
order by ItemIndex
PRINT @PrevFromDate
	PRINT @PrevToDate 
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO


/*
	 select * from [dbo].[Func_ConvertStringIntoTable_Nvarchar] ('USD;', ';')
	 Hàm convert một chuỗi thành một bảng 
*/
ALTER FUNCTION [dbo].[Func_ConvertStringIntoTable_Nvarchar]
    (
      @ValueList NVARCHAR(MAX) ,
      @SeparateCharacter NCHAR(1) /*Ký tự phân tách*/
    )
RETURNS @ValueTable TABLE ( Value NVARCHAR(MAX))
AS
    BEGIN
		
 
        DECLARE @ValueListLength INT ,
            @StartingPosition INT ,
            @Value NVARCHAR(MAX) ,
            @SecondPosition INT 
        SET @StartingPosition = 1
        SET @Value = SPACE(0)
        
        IF SUBSTRING(@ValueList, 1, 1) <> @SeparateCharacter
            SET @ValueList = @SeparateCharacter + @ValueList
        SET @ValueListLength = LEN(@ValueList)
			
        IF SUBSTRING(@ValueList, @ValueListLength - 1, @ValueListLength) <> @SeparateCharacter
            SET @ValueList = @ValueList + @SeparateCharacter
			
        WHILE @StartingPosition < @ValueListLength
            BEGIN
                SET @SecondPosition = CHARINDEX(@SeparateCharacter, @ValueList,
                                                @StartingPosition + 1)
                SET @Value = SUBSTRING(@ValueList, @StartingPosition + 1,
                                       @SecondPosition - @StartingPosition - 1)	
                SET @StartingPosition = @SecondPosition
                IF @Value <> SPACE(0)
                    INSERT  INTO @ValueTable
                            ( Value )
                    VALUES  ( LTRIM(RTRIM(@Value)) )
            END
        RETURN 
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GL_GetGLAccountLedgerDiaryBook]
    @BranchID UNIQUEIDENTIFIER,
    @IncludeDependentBranch BIT,
    @StartDate DATETIME,
    @FromDate DATETIME,
    @ToDate DATETIME,    
    @AccountNumber NVARCHAR(MAX),
    @IsSimilarSum BIT,
	@IsSummaryChildAccount BIT
AS
    BEGIN    
        SET NOCOUNT ON       
        CREATE TABLE #Result
            (
             KeyID NVARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS,
             RefID UNIQUEIDENTIFIER,
             RefType INT,
             PostedDate DATETIME,
             RefNo NVARCHAR(25)COLLATE SQL_Latin1_General_CP1_CI_AS,
             RefDate DATETIME,
             InvDate DATETIME,
             InvNo NVARCHAR(MAX)COLLATE SQL_Latin1_General_CP1_CI_AS, 
             JournalMemo NVARCHAR(255)COLLATE SQL_Latin1_General_CP1_CI_AS,
             AccountNumber NVARCHAR(25)COLLATE SQL_Latin1_General_CP1_CI_AS,
			 DetailAccountNumber NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS,
             AccountCategoryKind INT,
             AccountName NVARCHAR(255)COLLATE SQL_Latin1_General_CP1_CI_AS,
             CorrespondingAccountNumber NVARCHAR(25)COLLATE SQL_Latin1_General_CP1_CI_AS,
             DebitAmount MONEY,
             CreditAmount MONEY,
             OrderType INT,
             IsBold BIT,
             OrderNumber int
            )
        CREATE TABLE #tblAccountNumber
            (
             AccountNumber NVARCHAR(25)  COLLATE SQL_Latin1_General_CP1_CI_AS  PRIMARY KEY,
             AccountName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
             AccountNumberPercent NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,
             AccountCategoryKind INT,
			 IsParent BIT
            )
			
       
	
        INSERT  #tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountName,
                        A1.AccountNumber + '%',
                        A1.AccountGroupKind,
						A1.IsParentNode
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
	            					
               
        IF @IsSimilarSum = 0
            INSERT  #Result
                    (KeyID,
                     RefID,
                     RefType,
                     PostedDate,
                     RefNo,
                     RefDate,
                     InvDate,
                     InvNo,
                     JournalMemo,
                     AccountNumber,
					 DetailAccountNumber,
                     AccountName,
                     CorrespondingAccountNumber,
                     AccountCategoryKind,
                     DebitAmount,
                     CreditAmount,
                     ORDERType,
                     IsBold,
                     OrderNumber
					
                    )
                    SELECT 
							convert(nvarchar(100),GL.ID)  + '-' + AC.AccountNumber,
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Description AS JournalMemo,
                            AC.AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind,
                            DebitAmount,
                            CreditAmount,
                            1 AS ORDERType,
                            0,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%') 
											AND GL.DebitAmount <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    dbo.GeneralLedger AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND (GL.DebitAmount <> 0
                                 OR GL.CreditAmount <> 0
                                 or GL.DebitAmountOriginal <> 0
                                 OR GL.CreditAmountOriginal <> 0
                                )                           
                     
        ELSE
			/*Nếu tick Hiển thị phát sinh theo tiết khoản của TK tổng hợp thì lấy lên chi tiết tài khoản và group by (CR 13084)*/
			INSERT  INTO #Result
						(KeyID,
						 RefID,
						 RefType,
						 PostedDate,
						 RefNo,
						 RefDate,
						 InvDate,
						 InvNo,
						 JournalMemo,
						 AccountNumber,
						 DetailAccountNumber,
						 AccountName,
						 CorrespondingAccountNumber,
						 AccountCategoryKind,
						 DebitAmount,
						 CreditAmount,
						 ORDERType,
						 IsBold,
						 OrderNumber
					
						)
						SELECT  CAST(MAX(GL.ID) AS NVARCHAR(20))
								+ '-' + AC.AccountNumber,
								GL.ReferenceID,
								GL.TypeID,
								GL.PostedDate,
								RefNo,
								GL.RefDate,
								Gl.InvoiceDate,
								GL.InvoiceNo,
								GL.Description AS JournalMemo,							
								AC.AccountNumber,
								CASE WHEN @IsSummaryChildAccount = 1 THEN GL.Account
									 ELSE NULL 
								END AS DetailAccountNumber,
								AC.AccountName,
								GL.AccountCorresponding,
								AC.AccountCategoryKind,
								SUM(GL.DebitAmount) AS DebitAmount,
								SUM(GL.CreditAmount) AS CreditAmount,
								1 AS ORDERType,
								0 AS IsBold,
								 CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%') 
										   AND SUM(GL.DebitAmount) <> 0
										   THEN 0
										   ELSE 1
								  END AS OrderNumber
						FROM    dbo.GeneralLedger AS GL
								INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
						WHERE   (GL.PostedDate BETWEEN @FromDate AND @ToDate)                        
						GROUP BY GL.ReferenceID,
								GL.TypeID,
								GL.PostedDate,
								GL.RefNo,
								GL.RefDate,
								Gl.InvoiceDate,
								GL.InvoiceNo,
								GL.Description,
								AC.AccountNumber,
								CASE WHEN @IsSummaryChildAccount = 1 THEN GL.Account 
									 ELSE NULL 
								END,
								AC.AccountName,
								GL.AccountCorresponding,
								AC.AccountCategoryKind
						HAVING  SUM(GL.DebitAmount) <> 0
								OR SUM(GL.CreditAmount) <> 0 
								OR SUM(GL.DebitAmountOriginal) <> 0
								OR SUM(GL.CreditAmountOriginal) <> 0		
       
        SELECT  AccountNumber,
                AccountName,
				IsParent,
                SUM(OpenningDebitAmount) AS OpenningDebitAmount,
                SUM(OpenningCreditAmount) AS OpenningCreditAmount,
                SUM(ClosingDebitAmount) AS ClosingDebitAmount,
                SUM(ClosingCreditAmount) AS ClosingCreditAmount,
                SUM(AccumDebitAmount) AS AccumDebitAmount,
                SUM(AccumCreditAmount) AS AccumCreditAmount,
				AccountCategoryKind
        FROM    (
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS OpenningDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   dbo.GeneralLedger GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate < @FromDate                       
                 GROUP BY GL.BranchID,
                        AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS ClosingDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   dbo.GeneralLedger GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate <= @ToDate                        
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        SUM(GL.DebitAmount) AS AccumDebitAmount,
                        SUM(GL.CreditAmount) AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   dbo.GeneralLedger GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate BETWEEN @StartDate AND @ToDate                        
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                ) AS A
        GROUP BY AccountNumber,
                AccountName,
				IsParent ,
				AccountCategoryKind         
        HAVING
				SUM(OpenningDebitAmount) <>0 OR
                SUM(OpenningCreditAmount)  <>0 OR
                SUM(ClosingDebitAmount)  <>0 OR
                SUM(ClosingCreditAmount) <>0 OR
                SUM(AccumDebitAmount) <>0 OR
                SUM(AccumCreditAmount) <>0 
        ORDER BY AccountNumber
        		
		SELECT  ROW_NUMBER() OVER (ORDER BY AccountNumber,DetailAccountNumber, OrderType, PostedDate, RefDate,OrderNumber, RefNo) AS RowNum,
				KeyID,
				RefID,
				RefType,
				PostedDate,
				RefDate,
				RefNo,
				InvDate,
				InvNo,
				JournalMemo,
				AccountNumber,
				DetailAccountNumber,
				CorrespondingAccountNumber,
				DebitAmount,
				CreditAmount,
				IsBold,
				AccountCategoryKind
		FROM    #Result
			
              
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*





*/
ALTER PROCEDURE [dbo].[Proc_GL_GetBookDetailPaymentByAccountNumber]
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(MAX) ,
	@CurrencyId NVARCHAR(10) ,
    @GroupTheSameItem BIT
AS
    BEGIN

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1) PRIMARY KEY,
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              InvDate DATETIME ,
              InvNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255) ,
              AccountNumber NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS,
              CorrespondingAccountNumber NVARCHAR(25) ,
              DebitAmount DECIMAL(22,4) ,
              CreditAmount DECIMAL(22,4) ,
              ClosingDebitAmount DECIMAL(22,4) ,
              ClosingCreditAmount DECIMAL(22,4) ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              OrderType INT , 
              IsBold BIT ,
              BranchName NVARCHAR(128),
			  AccountGroupKind NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS
            )      
       
        
        DECLARE @tblAccountNumber TABLE
            (              
			  AccountNumber  NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS,  
              AccountNumberPercent NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS  , 
			  AccountGroupKind NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS
            )
        INSERT  @tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountNumber + '%',
						A1.AccountGroupKind
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        IF @GroupTheSameItem = 1
            BEGIN
                INSERT  #Result					
				SELECT
					RefID ,
                    PostedDate ,
                    RefDate ,
                    RefNo ,
                    InvDate ,
                    InvNo ,
                    RefType ,
                    JournalMemo ,
                    AccountNumber,
                    CorrespondingAccountNumber ,
                    ISNULL(DebitAmount,0) ,
                    ISNULL(CreditAmount,0) ,
                    ISNULL(ClosingDebitAmount,0),
                    ISNULL(ClosingCreditAmount,0),
                    AccountingObjectCode,
                    AccountObjectName,
                    OrderType ,
                    IsBold ,
                    BranchName,
					AccountGroupKind
                 FROM
					(SELECT  
								NULL as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư đầu kỳ' as JournalMemo ,
                                A.AccountNumber,
                                NULL as CorrespondingAccountNumber ,
								$0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
									 THEN SUM(GL.DebitAmount - GL.CreditAmount)
									 ELSE 0
								END AS ClosingDebitAmount ,
								CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
									 THEN SUM(GL.CreditAmount - GL.DebitAmount)
									 ELSE 0
								END AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                0 AS OrderType ,
                                1 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
								,A.AccountGroupKind
                            FROM    dbo.GeneralLedger AS GL
							INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
							WHERE   ( GL.PostedDate < @FromDate )  
							and GL.CurrencyID = @CurrencyId                    
							GROUP BY 
								A.AccountNumber,A.AccountGroupKind
							HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Description,
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                SUM(GL.DebitAmount) AS DebitAmount ,
                                SUM(GL.CreditAmount) AS CreditAmount ,
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 As isBold,
                                '' as BranchName
								,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
								,A.AccountGroupKind
                        FROM    dbo.GeneralLedger AS GL
                                INNER JOIN @tblAccountNumber A ON GL.Account LIKE A.AccountNumberPercent
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )    
						and GL.CurrencyID = @CurrencyId 
						and GL.AccountCorresponding <> ''      
                        GROUP BY GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Description ,
                                A.AccountNumber,
                                GL.AccountCorresponding,
								A.AccountGroupKind
                        HAVING  SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0                       
                      )T
                      ORDER BY 
						AccountNumber,
                        OrderType ,
                        postedDate ,
                        RefDate ,
                        RefNo      
                        ,SortOrder,DetailPostOrder,EntryType            
            END
            
        ELSE
            BEGIN             
                
                INSERT  #Result					
				SELECT
					RefID ,
                    PostedDate ,
                    RefDate ,
                    RefNo ,
                    InvDate ,
                    InvNo ,
                    RefType ,
                    JournalMemo ,
                    AccountNumber,
                    CorrespondingAccountNumber ,
                    DebitAmount ,
                    CreditAmount ,
                    ClosingDebitAmount ,
                    ClosingCreditAmount ,
                    AccountingObjectCode,
                    AccountObjectName,
                    OrderType ,
                    IsBold ,
                    BranchName,
					AccountGroupKind
                 FROM
					(SELECT  
								null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư đầu kỳ' as JournalMemo ,
                                A.AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
									 THEN SUM(GL.DebitAmount - GL.CreditAmount)
									 ELSE 0
								END AS ClosingDebitAmount ,
								CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
									 THEN SUM(GL.CreditAmount - GL.DebitAmount)
									 ELSE 0
								END AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                0 AS OrderType ,
                                1 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType
								,A.AccountGroupKind
                            FROM    dbo.GeneralLedger AS GL
							INNER JOIN @tblAccountNumber A ON GL.Account LIKE A.AccountNumberPercent
							WHERE   ( GL.PostedDate < @FromDate )   
							and GL.CurrencyID = @CurrencyId                    
							GROUP BY 
								A.AccountNumber,A.AccountGroupKind
							HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                CASE WHEN GL.DESCRIPTION IS NULL
                                          OR LTRIM(GL.Description) = ''
                                     THEN GL.Reason
                                     ELSE GL.Description
                                END AS JournalMemo ,
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                GL.DebitAmount ,
                                GL.CreditAmount ,
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 ,
                                null BranchName
								,null
								,null
								,null
								,A.AccountGroupKind
                        FROM    dbo.GeneralLedger AS GL
                                INNER JOIN @tblAccountNumber A ON GL.Account LIKE A.AccountNumberPercent
                        WHERE   (GL.PostedDate BETWEEN @FromDate AND @ToDate )                              
                                AND ( GL.DebitAmount <> 0
                                      OR GL.CreditAmount <> 0
                                    )   	
									and GL.CurrencyID = @CurrencyId	
									and GL.AccountCorresponding <> ''                     
                      )T
                      ORDER BY
						AccountNumber,
                        OrderType ,
                        postedDate ,
                        RefDate ,
                        RefNo,SortOrder,DetailPostOrder,EntryType                                                          
               
            END         
       
			 

		DECLARE @AccountNumber1 NVARCHAR(25)

		DECLARE C1 CURSOR FOR  
		SELECT  F.Value
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
	    order by F.Value
		OPEN c1;  
		FETCH NEXT FROM c1 INTO @AccountNumber1  ;  
		WHILE @@FETCH_STATUS = 0  
		   BEGIN  
		      PRINT @AccountNumber1
			  DECLARE @AccountObjectCode NVARCHAR(25)
			 ,@ClosingDebitAmount DECIMAL(22,4)
			 
		      SELECT @ClosingDebitAmount = 0
			  UPDATE  #Result
			  SET     @ClosingDebitAmount = @ClosingDebitAmount + ISNULL(ClosingDebitAmount,0) - 
		                              ISNULL(ClosingCreditAmount,0) + ISNULL(DebitAmount,0) - ISNULL(CreditAmount,0),
		
                ClosingDebitAmount = CASE WHEN @ClosingDebitAmount > 0
                                          THEN @ClosingDebitAmount
                                          ELSE 0
                                     END ,
                ClosingCreditAmount = CASE WHEN @ClosingDebitAmount < 0
                                           THEN - @ClosingDebitAmount
                                           ELSE 0
                                      END 
               where AccountNumber = @AccountNumber1
			   FETCH NEXT FROM c1 INTO @AccountNumber1  ; 
			   
		   END;  
		CLOSE c1;  
		DEALLOCATE c1;  
		
        DECLARE @DebitAmount DECIMAL(22,4) 
		DECLARE @CreditAmount DECIMAL(22,4)  
		DECLARE @ClosingDeditAmount_SDDK DECIMAL(22,4)  
		DECLARE @ClosingCreditAmount_SDDK DECIMAL(22,4)  
	    select @DebitAmount = sum(DebitAmount),
		       @CreditAmount = sum(CreditAmount)
		from #Result Res
		INNER JOIN @tblAccountNumber A ON Res.AccountNumber LIKE A.AccountNumberPercent
		where OrderType = 1
		select @ClosingDeditAmount_SDDK = sum(ClosingDebitAmount),
		       @ClosingCreditAmount_SDDK = sum(ClosingCreditAmount)
		from #Result where OrderType = 0
        SELECT  RowNum ,
                RefID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountNumber,
                PostedDate ,
                RefDate ,
                RefNo ,
                InvDate ,
                InvNo ,
                RefType ,
                JournalMemo ,
                CorrespondingAccountNumber ,
                DebitAmount ,
                CreditAmount ,
                ClosingDebitAmount ,
                ClosingCreditAmount ,               
                OrderType AS InvoiceOrder ,
                IsBold ,
                BranchName,
				AccountGroupKind
        FROM    #Result
        ORDER BY RowNum
		select 
		null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư cuối kỳ' as JournalMemo ,
                                AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 DebitAmount ,
								$0 CreditAmount ,
								case 
								when A1.AccountGroupKind=0 or (A1.AccountGroupKind=2 and (A1.AccountNumber like '1%' or A1.AccountNumber like '2%' or A1.AccountNumber like '6%' 
								or A1.AccountNumber like '8%')) then
								sum(ClosingDebitAmount) + sum(sumDebitAmount) - sum(sumCreditAmount) else $0 
								end as ClosingDebitAmount,
								case 
								when A1.AccountGroupKind=1 or (A1.AccountGroupKind=2 and (A1.AccountNumber like '3%' or A1.AccountNumber like '4%' or A1.AccountNumber like '5%' 
								or A1.AccountNumber like '7%')) then
								sum(ClosingCreditAmount) + sum(sumCreditAmount) - sum(sumDebitAmount) else $0 
								end AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                2 AS OrderType ,
                                2 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType,
								A1.AccountGroupKind
        from (SELECT  
								null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư cuối kỳ' as JournalMemo ,
                                A.AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN GL.OrderType = 0 and gl.IsBold = 1 THEN sum(ClosingDebitAmount)
									 ELSE 0 
								END
								AS ClosingDebitAmount,
								sum(DebitAmount) sumDebitAmount,
								sum(CreditAmount) sumCreditAmount,
								0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                2 AS OrderType ,
                                2 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType,
								A.AccountGroupKind
                            FROM    #Result AS GL
							INNER JOIN @tblAccountNumber A ON GL.AccountNumber = A.AccountNumber
							GROUP BY OrderType,IsBold,
								A.AccountNumber,A.AccountGroupKind) as A1
        group by AccountNumber,AccountGroupKind
        DROP TABLE #Result 
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_FU_GetCashDetailBook]
    (
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @AccountNumber NVARCHAR(20) ,
      @BranchID UNIQUEIDENTIFIER ,
      @CurrencyID NVARCHAR(3)
    )
AS 
    BEGIN

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
			  AccountNumber NVARCHAR(15),
              PostedDate DATETIME ,
              RefDate DATETIME ,
              ReceiptRefNo NVARCHAR(25) ,
              PaymentRefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255) ,
              CorrespondingAccountNumber NVARCHAR(20) ,
              ExchangeRate Numeric ,
              DebitAmount Numeric ,
              DebitAmountOC Numeric ,
              CreditAmount Numeric ,
              CreditAmountOC Numeric ,
              ClosingAmount Numeric ,
              ClosingAmountOC Numeric ,
              ContactName NVARCHAR(255) ,
			  AccountObjectId NVARCHAR(255),
              AccountObjectCode NVARCHAR(255) ,
              AccountObjectName NVARCHAR(255) , 
              EmployeeCode NVARCHAR(255) ,
              EmployeeName NVARCHAR(255) , 
              InvoiceOrder INT , 
              BranchID UNIQUEIDENTIFIER ,
              BranchName NVARCHAR(255) ,
              IsBold BIT
            )
         DECLARE @tblAccountNumber TABLE
            (              
			  AccountNumber  NVARCHAR(25),  
              AccountNumberPercent NVARCHAR(25)              
            )
        INSERT  @tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountNumber + '%'
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        DECLARE @AccountNumberPercent NVARCHAR(25)
        SET @AccountNumberPercent = @AccountNumber + '%'
        
        DECLARE @DiscountNumber NVARCHAR(25)       
        SET @DiscountNumber = '5211%'
   
        DECLARE @ClosingAmountOC MONEY
        DECLARE @ClosingAmount MONEY
   
 INSERT  #Result
	          (RefID ,
			  AccountNumber,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  CorrespondingAccountNumber)
			  select ReferenceID ,
			  Account,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  AccountCorresponding from 
			            (SELECT null as ReferenceID,
						    Acc.AccountNumber as Account,
							null as PostedDate,
							null as RefDate,
							null as ReceiptRefNo,
							null as PaymentRefNo,
							null as RefType,
					        N'Số tồn đầu kỳ' AS JournalMemo ,
                            $0 AS ExchangeRate ,
                            $0 AS DebitAmount ,
                            $0 AS DebitAmountOC ,
                            $0 AS CreditAmount ,
                            $0 AS CreditAmountOC ,
							SUM(GL.DebitAmount - GL.CreditAmount) as ClosingAmount,
							SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) as ClosingAmountOC,
							null as ContactName,
							null as AccountObjectId,
                            0 AS InvoiceOrder ,
                            null BranchID ,
                            0 as BranchName,
                            null AccountCorresponding
							 from GeneralLedger gl
							 inner join @tblAccountNumber Acc on GL.Account = Acc.AccountNumber
							 where ( GL.PostedDate < @FromDate )
								AND ( @CurrencyID IS NULL
								OR GL.CurrencyID = @CurrencyID)
						    group by Acc.AccountNumber
				        union all
                        SELECT  GL.ReferenceID,
						        GL.Account,
                                GL.PostedDate ,
                                GL.RefDate ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS ReceiptRefNo ,
                                CASE WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS PaymentRefNo ,
                                GL.TypeID ,
								case when GL.Reason is null then
                                GL.Description 
								else GL.Reason 
								end as Description, 
                                GL.ExchangeRate ,
                                GL.DebitAmount ,
                                GL.DebitAmountOriginal ,
                                GL.CreditAmount ,
                                GL.CreditAmountOriginal ,
                                $0 AS ClosingAmount ,
                                $0 AS ClosingAmountOC ,
                                GL.ContactName ,
                                GL.AccountingObjectID ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0 THEN 1
                                     WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0 THEN 2
                                     ELSE 3
                                END AS InvoiceOrder ,
                                GL.BranchID ,
                                0,
                               GL.AccountCorresponding
                        FROM    dbo.GeneralLedger AS GL ,
						        @tblAccountNumber Acc
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )
                                AND GL.Account = Acc.AccountNumber
                                AND ( @CurrencyID IS NULL
                                      OR GL.CurrencyID = @CurrencyID
                                    )
                                AND ( GL.CreditAmount <> 0
                                      OR GL.DebitAmount <> 0
                                      OR GL.CreditAmountOriginal <> 0
                                      OR GL.DebitAmountOriginal <> 0
                                    )) T
                        ORDER BY Account,
						        PostedDate ,
                                RefDate ,
                                InvoiceOrder 

		
        SET @ClosingAmount = 0
        SET @ClosingAmountOC = 0
        UPDATE  #Result
        SET     @ClosingAmount = @ClosingAmount  + ClosingAmount +ISNULL(DebitAmount, 0)
                - ISNULL(CreditAmount, 0) ,
                ClosingAmount = @ClosingAmount ,
                @ClosingAmountOC = @ClosingAmountOC + ClosingAmountOC + ISNULL(DebitAmountOC, 0)
                - ISNULL(CreditAmountOC, 0) ,
                ClosingAmountOC = @ClosingAmountOC 
        SELECT  *
        FROM    #Result R   
		order by rownum
        DROP TABLE #Result 
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER FUNCTION [dbo].[Func_ConvertStringIntoTable]
    (
      @GUIString NVARCHAR(MAX),
      @SeparateCharacter NCHAR(1)
    )
RETURNS @ValueTable TABLE (Value UNIQUEIDENTIFIER)
AS BEGIN
 
    DECLARE @ValueListLength INT,
			@StartingPosition INT,
			@Value NVARCHAR(50),
			@SecondPosition INT
			
    SET @ValueListLength = LEN(@GUIString)
    SET @StartingPosition = 1
    SET @Value = SPACE(0)
 
	WHILE @StartingPosition < @ValueListLength
		BEGIN
			SET @SecondPosition = CHARINDEX(@SeparateCharacter,@GUIString,@StartingPosition+1)
			SET @Value = SUBSTRING(@GUIString,@StartingPosition+1,@SecondPosition-@StartingPosition-1)	
			SET @StartingPosition = @SecondPosition
			IF @Value <> SPACE(0)
				INSERT  INTO @ValueTable (Value) VALUES(CAST(@Value AS UNIQUEIDENTIFIER))
		END
	RETURN 
   END



GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DROP FUNCTION [dbo].[Func_GetAccountObjectName]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
create FUNCTION [dbo].[Func_GetAccountObjectName]
    (
      @CategoryListCode NVARCHAR(MAX)
    )
RETURNS NVARCHAR(MAX)
AS 
    BEGIN
        DECLARE @CategoryListName AS NVARCHAR(MAX)
        DECLARE @CategoryListNameTable AS TABLE
            (
              CategoryName NVARCHAR(255)
            )      
        INSERT  INTO @CategoryListNameTable
                ( CategoryName                         
                )
                SELECT AOG.AccountingObjectGroupName
                FROM (SELECT  Value
                        FROM    dbo.Func_ConvertStringIntoTable(';'+ @CategoryListCode+ ';',
                                                              ';')
					) List INNER JOIN AccountingObjectGroup AOG ON List.Value = AOG.AccountingObjectGroupCode
                

        SET @CategoryListName = ''
        SELECT  @CategoryListName = @CategoryListName + '; '
                + ISNULL(CategoryName, '')
        FROM    @CategoryListNameTable
        
        IF LEN(@CategoryListName) > 1 
            BEGIN
                SET @CategoryListName = SUBSTRING(@CategoryListName, 3,
                                                  LEN(@CategoryListName))
            END
        RETURN  ISNULL(@CategoryListName,NULL)
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_ReceivableDetail]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3) ,
    @IsSimilarSum BIT 
AS
    BEGIN
        DECLARE @ID UNIQUEIDENTIFIER
        SET @ID = '00000000-0000-0000-0000-000000000000'
               
        CREATE TABLE #tblListAccountObjectID  
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectName NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectAddress NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectGroupListCode NVARCHAR(MAX)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              AccountObjectGroupListName NVARCHAR(MAX)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              CompanyTaxCode NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
                                          NULL
            ) 
        INSERT  INTO #tblListAccountObjectID
                SELECT  AO.ID ,
                        AccountingObjectCode ,
                        AccountingObjectName ,
                        [Address] ,
                        AOG.AccountingObjectGroupCode , 
                        AOG.AccountingObjectGroupName , 
                        CASE WHEN AO.ObjectType = 0 THEN TaxCode
                             ELSE ''
                        END AS CompanyTaxCode 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                           ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value 
                        left JOIN dbo.AccountingObjectGroup AOG ON AO.AccountObjectGroupID = AOG.ID 
          
        CREATE TABLE #tblResult
            (
              RowNum INT PRIMARY KEY
                         IDENTITY(1, 1) ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              AccountObjectName NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              AccountObjectGroupListCode NVARCHAR(MAX)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL , 
              AccountObjectGroupListName NVARCHAR(MAX)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,          
              AccountObjectAddress NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL , 
              CompanyTaxCode NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
                                          NULL ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS
                                 NULL ,
              InvDate DATETIME ,
              InvNo NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                  NULL ,
              
              RefType INT ,
              RefID UNIQUEIDENTIFIER ,                        
              RefDetailID NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
                                       NULL ,
              JournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                        NULL ,
              AccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,
              DebitAmount MONEY , 
              CreditAmountOC MONEY ,
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY , 
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50)
                COLLATE SQL_Latin1_General_CP1_CI_AS , 
              InventoryItemName NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              UnitName NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
                                    NULL ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(20, 6) ,            
              IsBold BIT , 
              OrderType INT ,
              BranchName NVARCHAR(128) COLLATE SQL_Latin1_General_CP1_CI_AS
                                       NULL ,
              GLJournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                          NULL 
              ,
              MasterCustomField1 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField2 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField3 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField4 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField5 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField6 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField7 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField8 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField9 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField10 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              CustomField1 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField2 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField3 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField4 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField5 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField6 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField7 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField8 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField9 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField10 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                          NULL ,
              /*cột số lượng, đơn giá*/
              MainUnitName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              MainUnitPrice DECIMAL(20, 6) ,
              MainQuantity DECIMAL(22, 8) ,
              /*mã thống kê, và tên loại chứng từ*/
              ListItemCode NVARCHAR(20) ,
              ListItemName NVARCHAR(128) ,
              RefTypeName NVARCHAR(128)
            )
        
        CREATE TABLE #tblAccountNumber
            (
              AccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         PRIMARY KEY ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL
            BEGIN
			INSERT  INTO #tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
            END
        ELSE
            BEGIN 
			   INSERT  INTO #tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   DetailType = '1'
                                AND Grade = 1
                                AND IsParentNode = 0
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END 

/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
		
        IF @IsSimilarSum = 0 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,  
                          AccountObjectName ,	
                          AccountObjectGroupListCode ,
                          AccountObjectGroupListName ,        
                          AccountObjectAddress , 
                          CompanyTaxCode , 
                          PostedDate ,
                          RefDate , 
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,	
                          RefID ,  
                          RefDetailID ,
                          JournalMemo , 				  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,			  
                          ExchangeRate , 		  
                          DebitAmountOC ,
                          DebitAmount , 
                          CreditAmountOC , 
                          CreditAmount , 
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,	
                          ClosingCreditAmount ,               
                          IsBold , 
                          OrderType
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode , 
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,           
                                AccountObjectAddress ,
                                CompanyTaxCode , 
                                PostedDate ,
                                RefDate ,
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID ,
                                RefDetailID ,
                                JournalMemo , 				  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  
                                ExchangeRate ,  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) ,
                                SUM(CreditAmount) ,
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN SUM(ClosingDebitAmountOC)
                                            - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) ) > 0
                                                 THEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,         
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( SUM(ClosingDebitAmount)
                                              - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount)
                                                        - SUM(ClosingCreditAmount) ) > 0
                                                 THEN SUM(ClosingDebitAmount)
                                                      - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmountOC)
                                              - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) ) > 0
                                                 THEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmount)
                                              - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) ) > 0
                                                 THEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,
                                
                                IsBold , 
                                OrderType 
                                
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,	
                                            LAOI.AccountObjectGroupListCode ,
                                            LAOI.AccountObjectGroupListName ,          
                                            LAOI.AccountObjectAddress , 
                                            LAOI.CompanyTaxCode , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,             
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefDate
                                            END AS RefDate , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefNo
                                            END AS RefNo , 
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN N'Số dư đầu kỳ'
                                                 ELSE ISNULL(AOL.DESCRIPTION,
                                                             AOL.Reason)
                                            END AS JournalMemo ,   
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber ,
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ExchangeRate
                                            END AS ExchangeRate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                               
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                     
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC ,           
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 1
                                                 ELSE 0
                                            END AS IsBold ,	
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType 
                                  FROM      dbo.GeneralLedger AS AOL
                                            INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                            INNER JOIN #tblListAccountObjectID
                                            AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )

                                ) AS RSNS
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode , 
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,        
                                RSNS.AccountObjectAddress ,
                                CompanyTaxCode , 
                                RSNS.PostedDate ,
                                RSNS.RefDate ,
                                RSNS.RefNo , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,   
                                RSNS.RefDetailID ,
                                RSNS.JournalMemo , 				  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber , 		  
                                RSNS.ExchangeRate ,           
                                RSNS.IsBold ,
                                RSNS.OrderType
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC
                                       - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount
                                       - ClosingCreditAmount) <> 0
                        ORDER BY RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.OrderType ,
                                RSNS.PostedDate ,
                                RSNS.RefDate ,
                                RSNS.RefNo 
            END
        ELSE 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,   
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,            
                          AccountObjectAddress ,
                          CompanyTaxCode ,
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID ,   
                          RefDetailID ,
                          JournalMemo , 			  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,		  
                          ExchangeRate ,		  
                          DebitAmountOC ,
                          DebitAmount ,
                          CreditAmountOC , 
                          CreditAmount ,
                          ClosingDebitAmountOC ,
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount ,                                    
                          IsBold , 
                          OrderType
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,  
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,             
                                AccountObjectAddress , 
                                CompanyTaxCode ,
                                PostedDate ,	
                                RefDate , 
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID , 
                                RefDetailID ,
                                JournalMemo ,				  
                                AccountNumber ,
                                AccountCategoryKind ,
                                CorrespondingAccountNumber ,			  
                                ExchangeRate , 	  
                                DebitAmountOC ,
                                DebitAmount ,
                                CreditAmountOC ,
                                CreditAmount , 
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmountOC
                                              - ClosingCreditAmountOC )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmountOC
                                                        - ClosingCreditAmountOC ) > 0
                                                 THEN ClosingDebitAmountOC
                                                      - ClosingCreditAmountOC
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,  
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmount
                                              - ClosingCreditAmount )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmount
                                                        - ClosingCreditAmount ) > 0
                                                 THEN ClosingDebitAmount
                                                      - ClosingCreditAmount
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmountOC
                                              - ClosingDebitAmountOC )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC ) > 0
                                                 THEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmount
                                              - ClosingDebitAmount )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount ) > 0
                                                 THEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,                                     
                                IsBold , 
                                OrderType
                        FROM    ( SELECT    AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode ,
                                            AD.AccountObjectGroupListName ,            
                                            AD.AccountObjectAddress ,
                                            CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType , 
                                            MAX(AD.RefDetailID) AS RefDetailID ,
                                            AD.JournalMemo ,   
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber , 
                                            AD.ExchangeRate , 
                                            SUM(AD.DebitAmountOC) AS DebitAmountOC ,                              
                                            SUM(AD.DebitAmount) AS DebitAmount ,                                        
                                            SUM(AD.CreditAmountOC) AS CreditAmountOC , 
                                            SUM(AD.CreditAmount) AS CreditAmount ,
                                            SUM(AD.ClosingDebitAmountOC) AS ClosingDebitAmountOC ,       
                                            SUM(AD.ClosingDebitAmount) AS ClosingDebitAmount ,
                                            SUM(AD.ClosingCreditAmountOC) AS ClosingCreditAmountOC , 
                                            SUM(AD.ClosingCreditAmount) AS ClosingCreditAmount ,
                                            AD.IsBold ,	
                                            AD.OrderType 
                                  FROM      ( SELECT    AOL.AccountingObjectID ,
                                                        LAOI.AccountObjectCode ,   
                                                        LAOI.AccountObjectName ,	
                                                        LAOI.AccountObjectGroupListCode ,
                                                        LAOI.AccountObjectGroupListName ,              
                                                        LAOI.AccountObjectAddress , 
                                                        LAOI.CompanyTaxCode , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.PostedDate
                                                        END AS PostedDate ,             
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefDate
                                                        END AS RefDate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefNo
                                                        END AS RefNo , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceDate
                                                        END AS InvDate ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceNo
                                                        END AS InvNo ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.ReferenceID
                                                        END AS RefID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.TypeID
                                                        END AS RefType ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE CAST(DetailID AS NVARCHAR(50))
                                                        END AS RefDetailID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN N'Số dư đầu kỳ'
                                                             ELSE AOL.Reason
                                                        END AS JournalMemo , 
                                                        TBAN.AccountNumber ,
                                                        TBAN.AccountCategoryKind ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.AccountCorresponding
                                                        END AS CorrespondingAccountNumber , 
                                                        
                                                        CASE WHEN AOL.PostedDate < @FromDate
															 THEN NULL
															 ELSE AOL.ExchangeRate
														END AS ExchangeRate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmountOriginal
                                                        END AS DebitAmountOC ,                         
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmount
                                                        END AS DebitAmount ,                           
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmountOriginal
                                                        END AS CreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmount
                                                        END AS CreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingDebitAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmount
                                                             ELSE $0
                                                        END AS ClosingDebitAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingCreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmount
                                                             ELSE $0
                                                        END AS ClosingCreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 1
                                                             ELSE 0
                                                        END AS IsBold ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 0
                                                             ELSE 1
                                                        END AS OrderType 
                                                        
                                              FROM      dbo.GeneralLedger
                                                        AS AOL
                                                        INNER JOIN #tblListAccountObjectID
                                                        AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                                        INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                              WHERE     AOL.PostedDate <= @ToDate
                                                        AND ( @CurrencyID IS NULL
                                                              OR AOL.CurrencyID = @CurrencyID
                                                            )
                                            ) AS AD
                                  GROUP BY  AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode , 
                                            AD.AccountObjectGroupListName ,     
                                            AD.AccountObjectAddress ,
                                            AD.CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType ,
                                            AD.JournalMemo ,
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber ,  
                                            AD.ExchangeRate ,
                                            AD.IsBold , 	
                                            AD.OrderType
                                ) AS RSS
                        WHERE   RSS.DebitAmountOC <> 0
                                OR RSS.CreditAmountOC <> 0
                                OR RSS.DebitAmount <> 0
                                OR RSS.CreditAmount <> 0
                                OR ClosingCreditAmountOC
                                - ClosingDebitAmountOC <> 0
                                OR ClosingCreditAmount - ClosingDebitAmount <> 0
                        ORDER BY RSS.AccountObjectCode ,
                                RSS.AccountNumber ,
                                RSS.OrderType ,
                                RSS.PostedDate ,
                                RSS.RefDate ,
                                RSS.RefNo ,
                                RSS.InvDate ,
                                RSS.InvNo
            END

/* Tính số tồn */
        DECLARE @CloseAmountOC AS DECIMAL(22, 8) ,
            @CloseAmount AS DECIMAL(22, 8) ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0
                                        THEN ( CASE WHEN ClosingDebitAmountOC = 0
                                                    THEN ClosingCreditAmountOC
                                                    ELSE -1
                                                         * ClosingDebitAmountOC
                                               END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber
                                        THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC
                                             - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0
                                              THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1
                                              THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0
                                                        THEN -1
                                                             * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1
                                               THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0
                                               THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0
                                                         THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0
                                      THEN ( CASE WHEN ClosingDebitAmount = 0
                                                  THEN ClosingCreditAmount
                                                  ELSE -1 * ClosingDebitAmount
                                             END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber
                                      THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount
                                           - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0
                                            THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1
                                            THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0
                                                      THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1
                                             THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0
                                             THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0
                                                       THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
        SELECT  RS.*
        FROM    #tblResult RS
        ORDER BY RowNum
        
        DROP TABLE #tblResult
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*=================================================================================================
 * Created By:		haipl
 * Created Date:	25/12/2017
 * Description:		Lay du lieu cho bao cao << Sổ chi tiết bán hàng >>
===================================================================================================== */
ALTER PROCEDURE [dbo].[Proc_SA_GetSalesBookDetail]
  
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @InventoryItemID NVARCHAR(MAX) , 
    @OrganizationUnitID UNIQUEIDENTIFIER ,
    @AccountObjectID NVARCHAR(MAX) ,
	@SumGiaVon decimal output
AS

    BEGIN
        SET NOCOUNT ON;
        declare @v1 DECIMAL(29,4)                            
        CREATE TABLE #tblListInventoryItemID
            (
              InventoryItemID UNIQUEIDENTIFIER PRIMARY KEY
            ) 
        INSERT  INTO #tblListInventoryItemID
                    SELECT  T.*
                    FROM    dbo.Func_ConvertStringIntoTable(@InventoryItemID,
                                                              ',')	
                                                              AS T  
        select a.Date as ngay_CT, 
		a.PostedDate as ngay_HT, 
		a.No as So_Hieu,
		a.InvoiceDate as Ngay_HD, 
		a.InvoiceNo as SO_HD, 
		d.reason  as Dien_giai, 
		accountCorresponding as TK_DOIUNG,
		c.Unit as DVT, 
		b.Quantity as SL, 
		b.UnitPrice as DON_GIA, 
		sum(debitAmount) as KHAC, 
		sum(CreditAmount) as TT, 
		c.ID as  InventoryItemID,
		c.MaterialGoodsName
		from GeneralLedger a join SAInvoiceDetail b on a.DetailID = b.ID 
		join MaterialGoods c on b.MaterialGoodsID = c.ID
		join SAInvoice d on a.ReferenceID = d.ID
		where a.PostedDate between  @FromDate and @ToDate and c.ID in (select InventoryItemID from #tblListInventoryItemID)
		and Account like '511%'
		group by a.Date,  a.PostedDate, a.No ,a.InvoiceDate , a.InvoiceNo , Sreason , accountCorresponding , 
		c.Unit , b.Quantity , b.UnitPrice, c.ID,c.MaterialGoodsName,d.reason ;


		 select isnull(sum(OWAmount),0) Sum_Gia_Goc, b.MaterialGoodsCode ,b.ID InventoryItemID from SAInvoiceDetail a, MaterialGoods b 
		 where exists (select InventoryItemID from #tblListInventoryItemID where InventoryItemID = a.MaterialGoodsID)
		 and exists ( select ReferenceID from GeneralLedger where  account = '632' and PostedDate between @FromDate and @ToDate and ReferenceID=a.SAinvoiceID )
		 and a.MaterialGoodsID=b.ID group by b.ID,b.MaterialGoodsCode


    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_GetReceivableSummaryByGroup]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountNumber NVARCHAR(25),
    @AccountObjectID AS NVARCHAR(MAX),
    @CurrencyID NVARCHAR(3),
    @AccountObjectGroupID AS NVARCHAR(MAX) 
AS
    BEGIN          
        DECLARE @tblListAccountObjectID TABLE
            (
             AccountObjectID UNIQUEIDENTIFIER ,
             AccountObjectGroupList NVARCHAR(MAX)           
            ) 
             
        INSERT  INTO @tblListAccountObjectID
        SELECT  
			AO.ID
			,AO.AccountObjectGroupID
        FROM AccountingObject AS AO 
        LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') AS TLAO 
			ON AO.ID = TLAO.Value
        WHERE 
		(@AccountObjectID IS NULL OR TLAO.Value IS NOT NULL)
                                    		
        DECLARE @tblAccountNumber TABLE
            (
             AccountNumber NVARCHAR(255) PRIMARY KEY,
             AccountName NVARCHAR(255),
             AccountNumberPercent NVARCHAR(255),
             AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber,
                                A.AccountName,
                                A.AccountNumber + '%',
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber,
                                A.AccountName                 
            END
        ELSE
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber,
                                A.AccountName,
                                A.AccountNumber + '%',
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   DetailType = '1'
                                AND Grade = 1
                                AND IsParentNode = 0
                        ORDER BY A.AccountNumber,
                                A.AccountName
            END
	    
	    DECLARE @tblDebt TABLE
			(
				AccountObjectID			UNIQUEIDENTIFIER
				,AccountNumber			NVARCHAR (25)				
				,AccountObjectGroupList NVARCHAR(Max)				
				,OpenDebitAmountOC		DECIMAL (28,4)
				,OpenDebitAmount		DECIMAL (28,4)
				,OpenCreditAmountOC		DECIMAL (28,4)
				,OpenCreditAmount		DECIMAL (28,4)
				,DebitAmountOC			DECIMAL (28,4)
				,DebitAmount			DECIMAL (28,4)
				,CreditAmountOC			DECIMAL (28,4)
				,CreditAmount			DECIMAL (28,4)
				,CloseDebitAmountOC		DECIMAL (28,4)
				,CloseDebitAmount		DECIMAL (28,4)
				,CloseCreditAmountOC	DECIMAL (28,4)
				,CloseCreditAmount		DECIMAL (28,4)					
			)				
		INSERT @tblDebt
        
        SELECT  
                AccountingObjectID,               
                AccountNumber,       
                AccountObjectGroupList,
                (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN (SUM(OpenningDebitAmountOC - OpenningCreditAmountOC)) > 0
                                THEN (SUM(OpenningDebitAmountOC - OpenningCreditAmountOC))
                                ELSE $0
                           END
                 END) AS OpenningDebitAmountOC,        
                (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmount - OpenningCreditAmount)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN SUM(OpenningDebitAmount - OpenningCreditAmount) > 0
                                THEN SUM(OpenningDebitAmount  - OpenningCreditAmount)
                                ELSE $0
                           END
                 END) AS OpenningDebitAmount,
                (CASE WHEN AccountCategoryKind = 1
                      THEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC))
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC)) > 0
                                THEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC))
                                ELSE $0
                           END
                 END) AS OpenningCreditAmountOC,
                (CASE WHEN AccountCategoryKind = 1
                      THEN (SUM(OpenningCreditAmount - OpenningDebitAmount))
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmount - OpenningDebitAmount)) > 0
                                THEN (SUM(OpenningCreditAmount - OpenningDebitAmount))
                                ELSE $0
                           END
                 END) AS OpenningCreditAmount, 
                SUM(DebitAmountOC) AS DebitAmountOC,
                SUM(DebitAmount) AS DebitAmount, 
                SUM(CreditAmountOC) AS CreditAmountOC,
                SUM(CreditAmount) AS CreditAmount, 
                (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC + DebitAmountOC - CreditAmountOC)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC + DebitAmountOC - CreditAmountOC) > 0
                                THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC + DebitAmountOC - CreditAmountOC)
                                ELSE $0
                           END
                 END) AS CloseDebitAmountOC,	
                 (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                               + DebitAmount - CreditAmount)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN SUM(OpenningDebitAmount
                                         - OpenningCreditAmount - CreditAmount
                                         + DebitAmount) > 0
                                THEN SUM(OpenningDebitAmount
                                         - OpenningCreditAmount - CreditAmount
                                         + DebitAmount)
                                ELSE $0
                           END
                 END) AS CloseDebitAmount,
                (CASE WHEN AccountCategoryKind = 1
                      THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC - DebitAmountOC + CreditAmountOC)
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC - DebitAmountOC + CreditAmountOC)) > 0
                                THEN SUM(OpenningCreditAmountOC  - OpenningDebitAmountOC - DebitAmountOC + CreditAmountOC)
                                ELSE $0
                           END
                 END) AS CloseCreditAmountOC,	     
                (CASE WHEN AccountCategoryKind = 1
                      THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                               + CreditAmount - DebitAmount)
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)) > 0
                                THEN SUM(OpenningCreditAmount
                                         - OpenningDebitAmount + CreditAmount
                                         - DebitAmount)
                                ELSE $0
                           END
                 END) AS CloseCreditAmount
        FROM    (SELECT AOL.AccountingObjectID,                        
                        TBAN.AccountNumber,
                        TBAN.AccountCategoryKind,
                        ISNULL(AccountObjectGroupList,'') AccountObjectGroupList,
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.DebitAmountOriginal
                             ELSE $0
                        END) AS OpenningDebitAmountOC,	
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.DebitAmount
                             ELSE $0
                        END) AS OpenningDebitAmount, 
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.CreditAmountOriginal
                             ELSE $0
                        END) AS OpenningCreditAmountOC, 
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.CreditAmount
                             ELSE $0
                        END) AS OpenningCreditAmount,                                         
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.DebitAmountOriginal
                             ELSE 0
                        END) AS DebitAmountOC,                      
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.DebitAmount
                             ELSE 0
                        END) AS DebitAmount,                    
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.CreditAmountOriginal
                             ELSE 0
                        END) AS CreditAmountOC, 
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.CreditAmount
                             ELSE 0
                        END) AS CreditAmount
                 FROM   dbo.GeneralLedger AS AOL                       
                        INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                        INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                        INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                      
                 WHERE  AOL.PostedDate <= @ToDate
                        AND (@CurrencyID IS NULL OR AOL.CurrencyID = @CurrencyID)
                        AND AN.DetailType = '1'
                 GROUP BY 
						AOL.AccountingObjectID,                       
                        TBAN.AccountNumber, 
                        TBAN.AccountCategoryKind
                        ,AccountObjectGroupList                      
                ) AS RSNS
       
        GROUP BY RSNS.AccountingObjectID,               
                 RSNS.AccountNumber 
                 ,RSNS.AccountCategoryKind
                 ,RSNS.AccountObjectGroupList
        HAVING
				SUM(DebitAmountOC)<>0 OR
                SUM(DebitAmount)<>0 OR
                SUM(CreditAmountOC) <>0 OR
                SUM(CreditAmount) <>0 OR
                SUM(OpenningDebitAmountOC - OpenningCreditAmountOC)<>0 OR
                SUM(OpenningDebitAmount - OpenningCreditAmount)<>0
                

		SELECT   AOG.ID,
		        AOG.AccountingObjectGroupCode,
				AOG.AccountingObjectGroupName
		        ,AO.AccountingObjectCode
				,AO.AccountingObjectName
				,Ao.Address AS AccountObjectAddress
				,D.* FROM @tblDebt D
		INNER JOIN dbo.AccountingObject AO ON D.AccountObjectID = AO.ID
		left JOIN dbo.AccountingObjectGroup AOG ON AO.AccountObjectGroupID = AOG.ID
		ORDER BY D.AccountNumber
		
		
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*

*/
ALTER PROCEDURE [dbo].[Proc_SA_GetReceivableSummary]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER ,
    @AccountNumber NVARCHAR(25) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3) ,
    @IsShowInPeriodOnly BIT 
AS
    BEGIN
               
        DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectGroupListCode NVARCHAR(MAX) ,
              AccountObjectCategoryName NVARCHAR(MAX)
            ) 
             
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID ,
                        AO.AccountObjectGroupID ,
						null
                FROM    AccountingObject AS AO
                        LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                              ',') AS TLAO ON AO.ID = TLAO.Value
                        
                WHERE  
				      ( @AccountObjectID IS NULL
                              OR TLAO.Value IS NOT NULL
                            )
                              			
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(255) PRIMARY KEY ,
              AccountName NVARCHAR(255) ,
              AccountNumberPercent NVARCHAR(255) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   DetailType = '1'
                                AND Grade = 1
                                AND IsParentNode = 0
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
            
	    DECLARE @FirstDateOfYear AS DATETIME
	    SET @FirstDateOfYear = '1/1/' + CAST(Year(@FromDate) AS NVARCHAR(4))	   
	    
        SELECT  ROW_NUMBER() OVER ( ORDER BY AccountingObjectCode ) AS RowNum ,
                AccountingObjectID ,
                AccountingObjectCode ,
                AccountingObjectName ,
                AccountObjectAddress ,
                AccountObjectTaxCode ,
                AccountNumber , 
                AccountCategoryKind ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC)
                            - SUM(OpenningCreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) ) > 0
                                 THEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpenningDebitAmountOC ,        
                ( CASE WHEN AccountCategoryKind = 0
                       THEN ( SUM(OpenningDebitAmount - OpenningCreditAmount) )
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmount
                                            - OpenningCreditAmount) ) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount)
                                 ELSE $0
                            END
                  END ) AS OpenningDebitAmount , 
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmountOC
                                  - OpenningDebitAmountOC) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) ) > 0
                                 THEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpenningCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmount - OpenningDebitAmount) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) ) > 0
                                 THEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) )
                                 ELSE $0
                            END
                  END ) AS OpenningCreditAmount ,
                SUM(DebitAmountOC) AS DebitAmountOC ,
                SUM(DebitAmount) AS DebitAmount ,
                SUM(CreditAmountOC) AS CreditAmountOC ,
                SUM(CreditAmount) AS CreditAmount ,
                SUM(AccumDebitAmountOC) AS AccumDebitAmountOC ,
                SUM(AccumDebitAmount) AS AccumDebitAmount ,
                SUM(AccumCreditAmountOC) AS AccumCreditAmountOC ,
                SUM(AccumCreditAmount) AS AccumCreditAmount , 
                                        
                /* Số dư cuối kỳ = Dư Có đầu kỳ - Dư Nợ đầu kỳ + Phát sinh Có – Phát sinh Nợ
				Nếu Số dư cuối kỳ >0 thì hiển bên cột Dư Có cuối kỳ 
				Nếu số dư cuối kỳ <0 thì hiển thị bên cột Dư Nợ cuối kỳ */
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC
                                + DebitAmountOC - CreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC) > 0
                                 THEN SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseDebitAmountOC ,	
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC
                                - DebitAmountOC + CreditAmountOC)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC
                                            - DebitAmountOC + CreditAmountOC) ) > 0
                                 THEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          - DebitAmountOC + CreditAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmountOC ,	
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                                + DebitAmount - CreditAmount)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount
                                          - CreditAmount + DebitAmount) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount
                                          - CreditAmount + DebitAmount)
                                 ELSE $0
                            END
                  END ) AS CloseDebitAmount ,	
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                                + CreditAmount - DebitAmount)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount
                                            + CreditAmount - DebitAmount) ) > 0
                                 THEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmount ,	
                AccountObjectGroupListCode ,
                AccountObjectCategoryName
        FROM    ( SELECT    AO.ID as AccountingObjectID ,
                            AO.AccountingObjectCode ,  
                            AO.AccountingObjectName ,	
                            AO.Address AS AccountObjectAddress ,
                            AO.TaxCode AS AccountObjectTaxCode , 
                            TBAN.AccountNumber , 
                            TBAN.AccountCategoryKind , 
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmountOC ,
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.DebitAmount
                                 ELSE $0
                            END AS OpenningDebitAmount ,
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmountOC , 
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.CreditAmount
                                 ELSE $0
                            END AS OpenningCreditAmount ,                                                
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.DebitAmountOriginal
                                 ELSE 0
                            END AS DebitAmountOC ,                       
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.DebitAmount
                                 ELSE 0
                            END AS DebitAmount ,                                    
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.CreditAmountOriginal
                                 ELSE 0
                            END AS CreditAmountOC ,
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.CreditAmount
                                 ELSE 0
                            END AS CreditAmount ,
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.DebitAmountOriginal
                                 ELSE 0
                            END AS AccumDebitAmountOC ,                                  
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.DebitAmount
                                 ELSE 0
                            END AS AccumDebitAmount ,                                       
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.CreditAmountOriginal
                                 ELSE 0
                            END AS AccumCreditAmountOC ,
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.CreditAmount
                                 ELSE 0
                            END AS AccumCreditAmount ,
                            LAOI.AccountObjectGroupListCode ,
                            LAOI.AccountObjectCategoryName 
                  FROM      dbo.GeneralLedger AS GL
                            INNER JOIN dbo.AccountingObject AO ON AO.ID = GL.AccountingObjectID
                            INNER JOIN @tblAccountNumber TBAN ON GL.Account LIKE TBAN.AccountNumberPercent
                            INNER JOIN dbo.Account AS AN ON GL.Account = AN.AccountNumber
                            INNER JOIN @tblListAccountObjectID AS LAOI ON GL.AccountingObjectID = LAOI.AccountObjectID
                  WHERE     GL.PostedDate <= @ToDate
                            AND ( @CurrencyID IS NULL
                                  OR GL.CurrencyID = @CurrencyID
                                )
                            AND AN.DetailType = '1'
                ) AS RSNS
        GROUP BY RSNS.AccountingObjectID ,
                RSNS.AccountingObjectCode ,
                RSNS.AccountingObjectName ,    
                RSNS.AccountObjectAddress ,
                RSNS.AccountObjectTaxCode , 
                RSNS.AccountNumber ,
                RSNS.AccountCategoryKind ,
                RSNS.AccountObjectGroupListCode ,
                RSNS.AccountObjectCategoryName 
        HAVING  SUM(DebitAmountOC) <> 0
                OR SUM(DebitAmount) <> 0
                OR SUM(CreditAmountOC) <> 0
                OR SUM(CreditAmount) <> 0
                OR SUM(OpenningDebitAmountOC - OpenningCreditAmountOC) <> 0
                OR SUM(OpenningDebitAmount - OpenningCreditAmount) <> 0                
                OR(@IsShowInPeriodOnly = 0 AND (
                 SUM(AccumDebitAmountOC) <>0	
                OR SUM(AccumDebitAmount) <>0
                OR SUM(AccumCreditAmountOC) <>0  
                OR SUM(AccumCreditAmount)<>0))
        ORDER BY RSNS.AccountingObjectCode
        OPTION(RECOMPILE)
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER procedure [dbo].[Proc_SA_GetDetailPayS12] 
(@fromdate DATETIME,
 @todate DATETIME,
 @account nvarchar(10),
 @currency nvarchar(3),
 @AccountObjectID AS NVARCHAR(MAX))
as
begin
DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(15),
			  AccountGroupKind int
            )
INSERT  INTO @tblAccountNumber
                SELECT  AO.AccountNumber,
				        AO.AccountGroupKind
                FROM    Account AS AO
                        inner JOIN dbo.Func_ConvertStringIntoTable_Nvarchar(@account,
                                                              ',') AS TLAO ON AO.AccountNumber = TLAO.Value
DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
			  AccountObjectCode NVARCHAR(100),
              AccountObjectGroupListCode NVARCHAR(MAX) ,
              AccountObjectCategoryName NVARCHAR(MAX)
            ) 
             
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID ,
				        AO.AccountingObjectCode,
                        AO.AccountObjectGroupID ,
						null
                FROM    AccountingObject AS AO
                        inner JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                              ',') AS TLAO ON AO.ID = TLAO.Value
                        
CREATE TABLE #tblResult
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              AccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS ,
			  AccountGroupKind int, 
              AccountObjectID UNIQUEIDENTIFIER ,
			  AccountObjectCode NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS ,
			  RefDate DATETIME , 
			  RefNo NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,
			  PostedDate DATETIME ,
			  JournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
			  CorrespondingAccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS , 
			  DueDate DATETIME,
			  DebitAmount MONEY , 
              CreditAmount MONEY , 
			  ClosingDebitAmount MONEY , 
              ClosingCreditAmount MONEY,
			  OrderType int, 
            )

if @account = '331'
insert into #tblResult
SELECT  a.AccountNumber,
        A.AccountGroupKind,
        LAOI.AccountObjectID,
		LAOI.AccountObjectCode,
		null as NgayThangGS,
		null as Sohieu, 
		null as ngayCTu,
        N'Số dư đầu kỳ' as Diengiai ,
		null as TKDoiUng, 
		null as ThoiHanDuocCKhau, 
        $0 AS PSNO ,
		$0 AS PSCO ,
		CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
				THEN SUM(GL.DebitAmount - GL.CreditAmount)
				ELSE 0
		END AS DuNo ,
		CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
				THEN SUM(GL.CreditAmount - GL.DebitAmount)
				ELSE 0
		END AS DuCo,
		0 AS OrderType
    FROM    dbo.GeneralLedger AS GL
	INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber 
	INNER JOIN @tblListAccountObjectID AS LAOI ON GL.AccountingObjectID = LAOI.AccountObjectID
	WHERE   ( GL.PostedDate < @FromDate )  
	and GL.CurrencyID = @currency                    
	GROUP BY 
		A.AccountNumber,LAOI.AccountObjectID,A.AccountGroupKind,LAOI.AccountObjectCode
	HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
UNION ALL
select  an.AccountNumber,
        An.AccountGroupKind,
        LAOI.AccountObjectID,
		LAOI.AccountObjectCode,
        a.PostedDate as NgayThangGS, 
		a.No as Sohieu, 
		a.Date as ngayCTu,
		a.Description as Diengiai, 
		a.AccountCorresponding as TKDoiUng, 
		DueDate as ThoiHanDuocCKhau, 
		sum(a.DebitAmount) as PSNO, 
		sum(a.CreditAmount) as PSCO,
		sum(fnsd.DebitAmount) DuNo,
		sum(fnsd.CreditAmount) DuCo,
		1 AS OrderType
 from GeneralLedger a									
left join PPInvoiceDetail dtl on dtl.id = a.DetailID 
left join PPInvoice c on c.id = dtl.PPInvoiceId
inner join @tblAccountNumber as an on a.Account = an.AccountNumber
INNER JOIN @tblListAccountObjectID AS LAOI ON a.AccountingObjectID = LAOI.AccountObjectID
INNER JOIN Func_SoDu (
   @fromdate
  ,@todate
  ,1) AS fnsd ON LAOI.AccountObjectID = fnsd.AccountObjectID	and an.AccountNumber = fnsd.AccountNumber			
where a.PostedDate between @fromdate and @todate
and  a.CurrencyID = @currency  
group by an.AccountNumber,
        LAOI.AccountObjectID,
		a.PostedDate,
		a.No , 
		a.Date ,
		a.Description, 
		a.AccountCorresponding, 
		DueDate,
		An.AccountGroupKind,
		LAOI.AccountObjectCode
order by AccountNumber,NgayThangGS	
else
insert into #tblResult
    SELECT  a.AccountNumber,
	    A.AccountGroupKind,
        LAOI.AccountObjectID,
		LAOI.AccountObjectCode,
		null as NgayThangGS,
		null as Sohieu, 
		null as ngayCTu,
        N'Số dư đầu kỳ' as Diengiai ,
		null as TKDoiUng, 
		null as ThoiHanDuocCKhau, 
        $0 AS PSNO ,
		$0 AS PSCO ,
		CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
				THEN SUM(GL.DebitAmount - GL.CreditAmount)
				ELSE 0
		END AS DuNo ,
		CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
				THEN SUM(GL.CreditAmount - GL.DebitAmount)
				ELSE 0
		END AS DuCo,
		0 AS OrderType 
    FROM    dbo.GeneralLedger AS GL
	INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber 
	INNER JOIN @tblListAccountObjectID AS LAOI ON GL.AccountingObjectID = LAOI.AccountObjectID
	WHERE   ( GL.PostedDate < @FromDate )  
	and GL.CurrencyID = @currency                    
	GROUP BY 
		A.AccountNumber,LAOI.AccountObjectID,A.AccountGroupKind,LAOI.AccountObjectCode
	HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
UNION ALL
	select  an.AccountNumber,
	        An.AccountGroupKind,
			LAOI.AccountObjectID,
			LAOI.AccountObjectCode,
			a.PostedDate as NgayThangGS, 
			a.No as Sohieu, 
			a.Date as ngayCTu,
			a.Description as Diengiai, 
			a.AccountCorresponding as TKDoiUng, 
			DueDate as ThoiHanDuocCKhau, 
			sum(a.DebitAmount) as PSNO, 
			sum(a.CreditAmount) as PSCO,
			sum(fnsd.DebitAmount) DuNo,
			sum(fnsd.CreditAmount) DuCo,
			1 AS OrderType
	 from GeneralLedger a	
	left join SAInvoiceDetail dtl on dtl.id = a.DetailID 			
	left join SAInvoice as c on c.id = dtl.SAInvoiceId				
	inner join @tblAccountNumber as an on a.Account = an.AccountNumber
	INNER JOIN @tblListAccountObjectID AS LAOI ON a.AccountingObjectID = LAOI.AccountObjectID
	INNER JOIN Func_SoDu (
	   @fromdate
	  ,@todate
	  ,1) AS fnsd ON LAOI.AccountObjectID = fnsd.AccountObjectID	and an.AccountNumber = fnsd.AccountNumber			
	where a.PostedDate between @fromdate and @todate
	and  a.CurrencyID = @currency  
	group by an.AccountNumber,
			LAOI.AccountObjectID,
			a.PostedDate,
			a.No , 
			a.Date ,
			a.Description, 
			a.AccountCorresponding, 
			DueDate,An.AccountGroupKind,LAOI.AccountObjectCode
	order by AccountNumber,AccountObjectID,NgayThangGS
DECLARE @CloseAmount AS DECIMAL(22, 8) ,
        @AccountObjectCode_tmp NVARCHAR(100) ,
        @AccountNumber_tmp NVARCHAR(20)
SELECT  @CloseAmount = 0 ,
        @AccountObjectCode_tmp = N'' ,
        @AccountNumber_tmp = N''
UPDATE  #tblResult
        SET     
                 @CloseAmount = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmount = 0 THEN ClosingCreditAmount
                                                                     ELSE -1 * ClosingDebitAmount
                                                                END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode or @AccountNumber_tmp <> AccountNumber THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountGroupKind = 0 THEN -1 * @CloseAmount
                                            WHEN AccountGroupKind = 1 THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0 THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountGroupKind = 1 THEN @CloseAmount
                                             WHEN AccountGroupKind = 0 THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0 THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
				@AccountNumber_tmp = AccountNumber							
select AccountNumber,
			AccountObjectID,
			RefDate as NgayThangGS, 
			RefNo as Sohieu, 
			PostedDate as ngayCTu,
			JournalMemo as Diengiai, 
			CorrespondingAccountNumber as TKDoiUng, 
			DueDate as ThoiHanDuocCKhau, 
			DebitAmount as PSNO, 
			CreditAmount as PSCO,
			ClosingDebitAmount as DuNo,
			ClosingCreditAmount as DuCo
from #tblResult 
order by RowNum
end				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_PO_SummaryPayable]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS
    BEGIN
	    DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              AccountObjectAddress NVARCHAR(255) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        null AccountingObjectGroupCode ,
                        null AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
      
                 
		
		DECLARE @StartDateOfPeriod DATETIME
		SET @StartDateOfPeriod = '1/1/'+CAST(Year(@FromDate) AS NVARCHAR(5))
                         
           
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(255) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
	    
        SELECT  ROW_NUMBER() OVER ( ORDER BY AccountObjectCode ) AS RowNum ,
                AccountingObjectID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountObjectAddress ,
                AccountObjectTaxCode ,
                AccountNumber ,
                AccountCategoryKind ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC)
                            - SUM(OpenningCreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) ) > 0
                                 THEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN ( SUM(OpenningDebitAmount - OpenningCreditAmount) )
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmount
                                            - OpenningCreditAmount) ) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount)
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmountOC
                                  - OpenningDebitAmountOC) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) ) > 0
                                 THEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmount - OpenningDebitAmount) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) ) > 0
                                 THEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmount ,
                SUM(DebitAmountOC) AS DebitAmountOC ,
                SUM(DebitAmount) AS DebitAmount ,
                SUM(CreditAmountOC) AS CreditAmountOC ,
                SUM(CreditAmount) AS CreditAmount ,
                SUM(AccumDebitAmountOC) AS AccumDebitAmountOC ,
                SUM(AccumDebitAmount) AS AccumDebitAmount ,
                SUM(AccumCreditAmountOC) AS AccumCreditAmountOC ,
                SUM(AccumCreditAmount) AS AccumCreditAmount ,
                /* Số dư cuối kỳ = Dư Có đầu kỳ - Dư Nợ đầu kỳ + Phát sinh Có – Phát sinh Nợ
				Nếu Số dư cuối kỳ >0 thì hiển bên cột Dư Có cuối kỳ 
				Nếu số dư cuối kỳ <0 thì hiển thị bên cột Dư Nợ cuối kỳ */
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC
                                + DebitAmountOC - CreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC) > 0
                                 THEN $0
                                 ELSE SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC)
                            END
                  END ) AS CloseDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC
                                + CreditAmountOC - DebitAmountOC)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC
                                            + CreditAmountOC - DebitAmountOC) ) > 0
                                 THEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                                + DebitAmount - CreditAmount)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount) > 0 THEN $0
                                 ELSE SUM(OpenningDebitAmount
                                          - OpenningCreditAmount + DebitAmount
                                          - CreditAmount)
                            END
                  END ) AS ClosingDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                                + CreditAmount - DebitAmount)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount
                                            + CreditAmount - DebitAmount) ) > 0
                                 THEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)
                                 ELSE $0
                            END
                  END ) AS ClosingCreditAmount ,
                AccountObjectGroupListCode ,
                AccountObjectGroupListName
        FROM    ( SELECT    AOL.AccountingObjectID ,
                            LAOI.AccountObjectCode ,
                            LAOI.AccountObjectName ,
                            LAOI.AccountObjectAddress,
                            LAOI.AccountObjectTaxCode ,
                            TBAN.AccountNumber ,
                            TBAN.AccountCategoryKind ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmount
                                 ELSE $0
                            END AS OpenningDebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmount
                                 ELSE $0
                            END AS OpenningCreditAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmountOriginal
                            END AS DebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmount
                            END AS DebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmountOriginal
                            END AS CreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmount
                            END AS CreditAmount ,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmountOriginal ELSE 0 END AS AccumDebitAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmount ELSE 0 END AS AccumDebitAmount,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmountOriginal ELSE 0 END AS AccumCreditAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmount ELSE 0 END AS AccumCreditAmount,
                            LAOI.AccountObjectGroupListCode ,
                            LAOI.AccountObjectGroupListName
                  FROM      dbo.GeneralLedger AS AOL
                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                  WHERE     AOL.PostedDate <= @ToDate
                            AND ( @CurrencyID IS NULL
                                  OR AOL.CurrencyID = @CurrencyID
                                )
                ) AS RSNS
        GROUP BY RSNS.AccountingObjectID ,
                RSNS.AccountObjectCode ,
                RSNS.AccountObjectName ,
                RSNS.AccountObjectAddress ,
                RSNS.AccountObjectTaxCode ,
                RSNS.AccountNumber ,
                RSNS.AccountCategoryKind ,
                RSNS.AccountObjectGroupListCode ,
                RSNS.AccountObjectGroupListName 
        HAVING 
				SUM(DebitAmountOC) <> 0
                OR SUM(DebitAmount) <> 0
                OR SUM(CreditAmountOC) <> 0
                OR SUM(CreditAmount) <> 0
                OR SUM(OpenningDebitAmount - OpenningCreditAmount) <> 0 
                OR SUM(OpenningDebitAmountOC - OpenningCreditAmountOC) <> 0 
				OR ( 
					(SUM(AccumDebitAmountOC) <> 0
					OR SUM(AccumDebitAmount) <> 0
					OR SUM(AccumCreditAmountOC) <> 0
					OR SUM(AccumCreditAmount) <> 0) )
        ORDER BY RSNS.AccountObjectCode
        OPTION (RECOMPILE)
		
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*

*/
ALTER PROCEDURE [dbo].[Proc_PO_PayDetailNCC]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS 
    BEGIN     
        DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              AccountObjectAddress NVARCHAR(255) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        null AccountingObjectGroupCode ,
                        null AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
      
        CREATE TABLE #tblResult
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectGroupListCode NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL ,             
              AccountObjectAddress NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              DueDate DATETIME , 
              InvDate DATETIME ,
              InvNo NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,

              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              JournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS , 
              AccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS , 
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS , 
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,	
              DebitAmount MONEY , 
              CreditAmountOC MONEY , 
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY ,
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,	
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              InventoryItemName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              UnitName NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(18, 4) , 
              BranchName NVARCHAR(128) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              IsBold BIT ,	
              OrderType INT ,
              GLJournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              DocumentIncluded NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField1 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField2 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField3 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField4 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField5 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField6 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField7 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField8 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField9 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField10 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS,
               MainUnitName NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              MainQuantity DECIMAL(22, 8) ,
              MainUnitPrice DECIMAL(18, 4),
			  OrderPriority int  
              
            )
        
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(255) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
            
                
/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode , 
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,      
                          AccountObjectAddress , 
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID , 
                          JournalMemo , 				  
                          AccountNumber , 
                          AccountCategoryKind ,
                          CorrespondingAccountNumber ,	  			  
                          DebitAmountOC ,	
                          DebitAmount , 
                          CreditAmountOC ,
                          CreditAmount ,
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount ,
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount,
						  OrderType ,
						  OrderPriority  
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,
                                AccountObjectName ,	
                                AccountObjectGroupListCode , 
                                AccountObjectGroupListName ,            
                                AccountObjectAddress , 
                                RSNS.PostedDate ,
                                NgayCtu ,
                                SoCtu ,
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RSNS.RefID , 
                                JournalMemo ,			  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  			  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) , 
                                SUM(CreditAmount) , 
                                ( CASE WHEN AccountCategoryKind = 0 THEN SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) ) > 0 THEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,      
                                ( CASE WHEN AccountCategoryKind = 0 THEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) ) > 0 THEN SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) ) > 0 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) ) > 0 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount,  
								  OrderType,
								  OrderPriority
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,
                                            AccountObjectGroupListCode ,
                                            AccountObjectGroupListName ,         
                                            LAOI.AccountObjectAddress ,
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.Date
                                            END AS NgayCtu ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.No
                                            END AS SoCtu ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType , 
                                            CASE WHEN AOL.PostedDate < @FromDate 
												      THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN N'Số dư đầu kỳ'
                                                 ELSE AOL.Description
                                            END AS JournalMemo , 
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                             
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                  
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount,
											CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType,
											OrderPriority 
                                  FROM      dbo.GeneralLedger AS AOL
                                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                                         
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )
                                ) AS RSNS                               
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode ,  
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,
                                RSNS.AccountObjectAddress ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu , 
                                RSNS.SoCtu , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,
                                RSNS.JournalMemo , 		  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber,
								OrderType ,
								OrderPriority		   
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount - ClosingCreditAmount) <> 0
                        ORDER BY 
						        RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu ,
                                RSNS.SoCtu,
								OrderPriority
                OPTION  ( RECOMPILE )                           
/* Tính số tồn */
        DECLARE @CloseAmountOC AS DECIMAL(22, 8) ,
            @CloseAmount AS DECIMAL(22, 8) ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmountOC = 0 THEN ClosingCreditAmountOC
                                                                       ELSE -1 * ClosingDebitAmountOC
                                                                  END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1 THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0 THEN -1 * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0 THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0 THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmount = 0 THEN ClosingCreditAmount
                                                                     ELSE -1 * ClosingDebitAmount
                                                                END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1 THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0 THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0 THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0 THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
        SELECT  RS.PostedDate as Ngay_HT, 
		        RS.RefDate as ngayCtu,
				RS.RefNo as SoCtu,
				Rs.JournalMemo as DienGiai,
				'331' as TK_CONGNO,
				Rs.CorrespondingAccountNumber as TkDoiUng,
                RS.AccountObjectCode,
				RS.AccountObjectName,
				RS.AccountNumber,
				fn.OpeningDebitAmount as OpeningDebitAmount,
				fn.OpeningCreditAmount as OpeningCreditAmount,
				Rs.DebitAmount as DebitAmount,
				Rs.CreditAmount as CreditAmount,
				Rs.ClosingDebitAmount as ClosingDebitAmount,
				Rs.ClosingCreditAmount as ClosingCreditAmount
        FROM    #tblResult RS ,
		        [dbo].[Func_SoDu] (
   @FromDate
  ,@ToDate
  ,3) fn
  where rs.AccountNumber = fn.AccountNumber
  and rs.AccountObjectID = fn.AccountObjectID
  order by RS.AccountObjectCode ,
                                RS.AccountNumber ,
                                RS.PostedDate ,
                                RS.RefDate ,
                                RS.RefNo,
								OrderPriority
        DROP TABLE #tblResult
    END				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*

*/
ALTER PROCEDURE [dbo].[Proc_PO_PayableDetail]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS 
    BEGIN     
        DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              AccountObjectAddress NVARCHAR(255) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        null AccountingObjectGroupCode ,
                        null AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
      
        CREATE TABLE #tblResult
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectGroupListCode NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL ,             
              AccountObjectAddress NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              DueDate DATETIME , 
              InvDate DATETIME ,
              InvNo NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,

              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              JournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS , 
              AccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS , 
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS , 
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,	
              DebitAmount MONEY , 
              CreditAmountOC MONEY , 
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY ,
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,	
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              InventoryItemName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              UnitName NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(18, 4) , 
              BranchName NVARCHAR(128) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              IsBold BIT ,	
              OrderType INT ,
              GLJournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              DocumentIncluded NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField1 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField2 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField3 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField4 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField5 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField6 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField7 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField8 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField9 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField10 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS,
               MainUnitName NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              MainQuantity DECIMAL(22, 8) ,
              MainUnitPrice DECIMAL(18, 4)  
              
            )
        
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(255) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
            
                
/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode , 
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,      
                          AccountObjectAddress , 
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID , 
                          JournalMemo , 				  
                          AccountNumber , 
                          AccountCategoryKind ,
                          CorrespondingAccountNumber ,	  			  
                          DebitAmountOC ,	
                          DebitAmount , 
                          CreditAmountOC ,
                          CreditAmount ,
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount ,
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount,
						  OrderType   
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,
                                AccountObjectName ,	
                                AccountObjectGroupListCode , 
                                AccountObjectGroupListName ,            
                                AccountObjectAddress , 
                                RSNS.PostedDate ,
                                RefDate ,
                                RefNo ,
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RSNS.RefID , 
                                JournalMemo ,			  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  			  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) , 
                                SUM(CreditAmount) , 
                                ( CASE WHEN AccountCategoryKind = 0 THEN SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) ) > 0 THEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,      
                                ( CASE WHEN AccountCategoryKind = 0 THEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) ) > 0 THEN SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) ) > 0 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) ) > 0 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount,  
								  OrderType
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,
                                            AccountObjectGroupListCode ,
                                            AccountObjectGroupListName ,         
                                            LAOI.AccountObjectAddress ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,                      
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE RefDate
                                            END AS RefDate , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.RefNo
                                            END AS RefNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType , 
                                            CASE WHEN AOL.PostedDate < @FromDate 
												      THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN N'Số dư đầu kỳ'
                                                 ELSE AOL.Description
                                            END AS JournalMemo , 
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                             
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                  
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount,
											CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType 
                                  FROM      dbo.GeneralLedger AS AOL
                                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                                         
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )
                                ) AS RSNS                               
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode ,  
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,
                                RSNS.AccountObjectAddress ,
                                RSNS.PostedDate ,
                                RSNS.RefDate , 
                                RSNS.RefNo , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,
                                RSNS.JournalMemo , 		  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber,
								OrderType 		   
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount - ClosingCreditAmount) <> 0
                        ORDER BY RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.PostedDate ,
                                RSNS.RefDate ,
                                RSNS.RefNo
                OPTION  ( RECOMPILE )    
                        
/* Tính số tồn */
        DECLARE @CloseAmountOC AS DECIMAL(22, 8) ,
            @CloseAmount AS DECIMAL(22, 8) ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmountOC = 0 THEN ClosingCreditAmountOC
                                                                       ELSE -1 * ClosingDebitAmountOC
                                                                  END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1 THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0 THEN -1 * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0 THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0 THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmount = 0 THEN ClosingCreditAmount
                                                                     ELSE -1 * ClosingDebitAmount
                                                                END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1 THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0 THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0 THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0 THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
        SELECT  
                RS.AccountObjectCode,
				RS.AccountObjectName,
				RS.AccountNumber,
				sum(fn.OpeningDebitAmount) OpeningDebitAmount,
				sum(fn.OpeningCreditAmount) OpeningCreditAmount,
				sum(Rs.DebitAmount) DebitAmount,
				sum(Rs.CreditAmount) CreditAmount,
				sum(Rs.ClosingDebitAmount) ClosingDebitAmount,
				sum(Rs.ClosingCreditAmount) ClosingCreditAmount
        FROM    #tblResult RS ,
		        [dbo].[Func_SoDu] (
   @FromDate
  ,@ToDate
  ,3) fn
  where rs.AccountNumber = fn.AccountNumber
  and rs.AccountObjectID = fn.AccountObjectID
  group by RS.AccountObjectCode,
				RS.AccountObjectName,
				RS.AccountNumber
        DROP TABLE #tblResult
    END				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER procedure [dbo].[Proc_PO_GetDetailBook]
@FromDate DATETIME ,
    @ToDate DATETIME,
    @AccountObjectID AS NVARCHAR(MAX),
	@MaterialGoods as NVARCHAR(MAX)
as
begin
    CREATE TABLE #tblListAccountObjectID  
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectName NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS 
            ) 
    CREATE TABLE #tblListMaterialGoods  
            (
              MaterialGoodsID UNIQUEIDENTIFIER ,
              MaterialGoodsCode NVARCHAR(25)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              MaterialGoodsName NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
			  Unit NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,
			 Quantity decimal,
			 UnitPrice Money 
            ) 
        INSERT  INTO #tblListAccountObjectID
                SELECT  AO.ID ,
                        ao.AccountingObjectCode ,
                        ao.AccountingObjectName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                           ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value 
        INSERT  INTO #tblListMaterialGoods
                SELECT  MG.ID ,
                        MG.MaterialGoodsCode ,
                        MG.MaterialGoodsName,
						MG.Unit,
						MG.Quantity,
						MG.UnitPrice 
                FROM    dbo.Func_ConvertStringIntoTable(@MaterialGoods,
                                                           ',') tblAccountObjectSelected
                        INNER JOIN dbo.MaterialGoods MG ON MG.ID = tblAccountObjectSelected.Value 

	select d.AccountObjectCode as MaKH, 
		   d.AccountObjectName as TenKH, 
		   a.Date as NgayHachToan, 
		   a.PostedDate as NgayCTu, 
		   a.No as SoCTu, 
		   b.InvoiceNo as SoHoaDon, 
		   b.InvoiceDate as NgayHoaDon, 
		   c.MaterialGoodsCode as Mahang, 
		   c.MaterialGoodsName as Tenhang, 
		   b.Unit as DVT,
		   b.Quantity as SoLuongMua, 
		   b.UnitPrice as DonGia, 
		   b.Amount as GiaTriMua, 
		   b.DiscountAmount as ChietKhau,
		   null as SoLuongTraLai, 
		   null as GiaTriTraLai,
		   null as GiaTriGiamGia  		 
	   from PPInvoice a join PPInvoiceDetail b
	   on a.id = b.PPInvoiceID 
	   left join #tblListMaterialGoods c on c.MaterialGoodsID = b.MaterialGoodsID
	   left join #tblListAccountObjectID d on d.AccountObjectID = a.AccountingObjectID
	   where PostedDate between @FromDate and @ToDate
	   and Recorded = '1' 
union all
	   select  d.AccountObjectCode as MaKH, 
		   d.AccountObjectName as TenKH, 
			   Date as NgayHachToan, 
			   PostedDate as NgayCTu, 
			   No as SoCTu, 
			   InvoiceNo as SoHoaDon, 
			   InvoiceDate as NgayHoaDon, 
			   MaterialGoodsCode as Mahang, 
			   MaterialGoodsName as Tenhang, 
			   c.Unit as DVT,		
			   c.Quantity as SoLuongMua, 
			   c.UnitPrice as DonGia, 
			   Amount as GiaTriMua, 
			   DiscountAmount as ChietKhau,
			   null as SoLuongTraLai, 
			   null as GiaTriTraLai,
			   null as GiaTriGiamGia  		
	   from PPService a join PPServiceDetail b		
	   on a.id = b.PPServiceID 		
       left join #tblListMaterialGoods c on c.MaterialGoodsID = b.MaterialGoodsID
	   left join #tblListAccountObjectID d on d.AccountObjectID = a.AccountingObjectID
	   where PostedDate between @FromDate and @ToDate
	   and Recorded = '1' 
union all
	   select d.AccountObjectCode as MaKH, 
		   d.AccountObjectName as TenKH, 
			  a.Date as NgayHachToan, 
			  PostedDate as NgayCTu, 
			  a.No as SoCTu, 
			  InvoiceNo as SoHoaDon, 
			  InvoiceDate as NgayHoaDon, 
			  MaterialGoodsCode as Mahang, 
			  MaterialGoodsName as Tenhang, 
			  b.Unit as DVT,
			  null SoLuongMua,	
			  b.UnitPrice as DonGia,	
			  Amount as GiaTriMua, 
			  null ChietKhau,
		      b.Quantity as SoLuongTraLai, 
			  b.Amount as GiaTriTraLai,
			  null as GiaTriGiamGia 		
		from PPDiscountReturn a join PPDiscountReturnDetail b		
		on a.id = b.PPDiscountReturnID 		
		left join #tblListMaterialGoods c on c.MaterialGoodsID = b.MaterialGoodsID
	    left join #tblListAccountObjectID d on d.AccountObjectID = a.AccountingObjectID
		where PostedDate between @FromDate and @ToDate and Recorded = '1' and TypeID = '220'		
union all
		select d.AccountObjectCode as MaKH, 
		   d.AccountObjectName as TenKH, 
			   a.Date as NgayHachToan, 
			   PostedDate as NgayCTu, 
			   a.No as SoCTu, 
			   InvoiceNo as SoHoaDon, 
			   InvoiceDate as NgayHoaDon, 
			   MaterialGoodsCode as Mahang, 
			   MaterialGoodsName as Tenhang, 
			   b.Unit as DVT,
			   null SoLuongMua,	
			   b.UnitPrice as DonGia,
			   Amount as GiaTriMua, 
			   TotalDiscountAmount as Chietkhau,		
		       null as SoLuongTraLai,
			   null as GiaTriTraLai,   
			   b.Amount as GiaTriGiamGia	   	
		from PPDiscountReturn a join PPDiscountReturnDetail b		
		on a.id = b.PPDiscountReturnID 		
		left join #tblListMaterialGoods c on c.MaterialGoodsID = b.MaterialGoodsID
	    left join #tblListAccountObjectID d on d.AccountObjectID = a.AccountingObjectID
		where PostedDate between @FromDate and @ToDate and TypeID = '230';		

end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_GetGLBookDetailMoneyBorrow]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountID NVARCHAR(MAX) ,
    @AccountObjectID NVARCHAR(MAX)
AS 
    BEGIN
	
        CREATE  TABLE #Result
            (
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS,
              RefType INT ,
              JournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS,
              AccountNumber NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS,
              AccountName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS,
              CorrespondingAccountNumber NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS,
              DueDate DATETIME ,
              DebitAmount DECIMAL(22, 4) ,
              CreditAmount DECIMAL(22, 4) ,
              AccountObjectCode NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS,
              AccountObjectName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS,
              OrderType INT ,
              IsBold BIT ,
              BranchName NVARCHAR(128) COLLATE SQL_Latin1_General_CP1_CI_AS,
              SortOrder INT ,
              DetailPostOrder INT
            )      

        DECLARE @tblAccountObject TABLE
            (
              AccountObjectID UNIQUEIDENTIFIER PRIMARY KEY,
			  AccountObjectCode NVARCHAR(25)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectName NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
            )                       
        INSERT  INTO @tblAccountObject
                SELECT  f.value,A.AccountingObjectCode,A.AccountingObjectName
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                           ',') f
                 INNER JOIN dbo.AccountingObject A ON A.ID = F.Value 	
        CREATE TABLE #tblSelectedAccountNumber
            (
              AccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NOT NULL
                                         PRIMARY KEY ,
              AccountName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                        NULL
            )
        INSERT  INTO #tblSelectedAccountNumber
                SELECT  A.AccountNumber ,
                        A.AccountName
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountID, ',') F
                        INNER JOIN dbo.Account A ON A.AccountNumber = F.Value 			        
        BEGIN             
                
            INSERT  #Result
                    SELECT  RefID ,
                            PostedDate ,
                            RefDate ,
                            RefNo ,
                            RefType ,
                            JournalMemo ,
                            AccountNumber ,
                            AccountName ,
                            CorrespondingAccountNumber ,
                            DueDate ,
                            DebitAmount ,
                            CreditAmount ,
                            AccountObjectCode ,
                            AccountObjectName ,
                            OrderType ,
                            IsBold ,
                            BranchName ,
                            SortOrder ,
                            DetailPostOrder
                    FROM    ( SELECT    NULL AS RefID ,
                                        NULL AS PostedDate ,
                                        NULL AS RefDate ,
                                        NULL AS RefNo ,
                                        NULL AS RefType ,
                                        N'Số dư đầu kỳ' AS JournalMemo ,
                                        GL.Account AccountNumber,
                                        AC.AccountName ,
                                        NULL AS CorrespondingAccountNumber ,
                                        NULL DueDate ,
                                        CASE WHEN SUM(GL.DebitAmount
                                                      - GL.CreditAmount) > 0
                                             THEN SUM(GL.DebitAmount
                                                      - GL.CreditAmount)
                                             ELSE 0
                                        END AS DebitAmount ,
                                        CASE WHEN SUM(GL.CreditAmount
                                                      - GL.DebitAmount) > 0
                                             THEN SUM(GL.CreditAmount
                                                      - GL.DebitAmount)
                                             ELSE 0
                                        END AS CreditAmount ,
                                        AO.AccountObjectCode ,
                                        AO.AccountObjectName AS AccountObjectName ,
                                        0 AS OrderType ,
                                        1 AS isBold ,
                                        null  AS BranchName,
                                        0 AS sortOrder ,
                                        0 AS DetailPostOrder ,
                                        0 AS EntryType
                              FROM      dbo.GeneralLedger AS GL
                                        inner JOIN #tblSelectedAccountNumber AC ON GL.Account LIKE AC.AccountNumber
                                                              + '%'
                                        inner JOIN @tblAccountObject AO ON AO.AccountObjectID = GL.AccountingObjectID
                              WHERE     ( GL.PostedDate < @FromDate )
                              GROUP BY  GL.Account ,
                                        AC.AccountName ,
                                        AO.AccountObjectCode ,
                                        AO.AccountObjectName
                              HAVING    SUM(GL.DebitAmount - GL.CreditAmount) <> 0
                              UNION ALL
                              SELECT    GL.ReferenceID ,
                                        GL.PostedDate ,
                                        GL.RefDate ,
                                        GL.RefNo ,
                                        GL.TypeID ,
                                        CASE WHEN ( GL.Description IS NOT NULL
                                                    AND GL.Description <> ''
                                                  ) THEN GL.Description
                                             ELSE GL.Reason
                                        END AS JournalMemo ,
                                        GL.Account AccountNumber,
                                        AC.AccountName ,
                                        GL.AccountCorresponding,
                                        null as DueDate ,
                                        GL.DebitAmount ,
                                        GL.CreditAmount ,
                                        AO.AccountObjectCode ,
                                        AO.AccountObjectName AS AccountObjectName ,
                                        2 AS OrderType ,
                                        0 ,
                                        null AS BranchName,
                                        GL.OrderPriority,
                                        null,
                                        null
                              FROM      dbo.GeneralLedger AS GL
                                        inner JOIN #tblSelectedAccountNumber AC ON GL.Account LIKE AC.AccountNumber
                                                              + '%'
                                        inner JOIN @tblAccountObject AO ON AO.AccountObjectID = GL.AccountingObjectID
                              WHERE     ( GL.PostedDate BETWEEN @FromDate AND @ToDate )
                                        AND ( GL.DebitAmount <> 0
                                              OR GL.CreditAmount <> 0
                                            )
                            ) T
                    ORDER BY AccountObjectName ,
                            AccountObjectCode ,
                            OrderType ,
                            postedDate ,
                            RefDate ,
                            RefNo ,
                            DetailPostOrder ,
                            SortOrder ,
                            EntryType                                                          
               
        END         
            
       
      /*Insert lable*/
        INSERT  #Result
                ( JournalMemo ,
                  AccountNumber ,
                  AccountName ,
                  DebitAmount ,
                  CreditAmount ,
                  AccountObjectCode ,
                  AccountObjectName ,
                  OrderType ,
                  IsBold ,
                  BranchName
		        )                                                
                SELECT  N'Số dư cuối kỳ' JournalMemo ,
                        AccountNumber ,
                        AccountName ,
                        CASE WHEN SUM(DebitAmount - CreditAmount) > 0
                             THEN SUM(DebitAmount - CreditAmount)
                             ELSE 0
                        END AS DebitAmount ,
                        CASE WHEN SUM(CreditAmount - DebitAmount) > 0
                             THEN SUM(CreditAmount - DebitAmount)
                             ELSE 0
                        END AS CreditAmount ,
                        AccountObjectCode ,
                        AccountObjectName ,
                        4 AS OrderType ,
                        1 ,
                        null as BranchName
                FROM    #Result
                WHERE   OrderType = 2
                        OR OrderType = 0
                GROUP BY AccountNumber ,
                        AccountName ,
                        AccountObjectCode ,
                        AccountObjectName
                HAVING  ( SUM(DebitAmount) <> 0
                          OR SUM(CreditAmount) <> 0
                        )                        
	     IF EXISTS ( SELECT  * FROM    #Result ) 
			SELECT  * ,
                ROW_NUMBER() OVER ( ORDER BY AccountNumber,AccountObjectName,AccountObjectCode,  OrderType , PostedDate , RefNo , RefDate, SortOrder, DetailPostOrder ) AS RowNum
			FROM    #Result  
        
        ELSE 
           BEGIN
                INSERT  #Result
                        ( JournalMemo ,
                          OrderType ,
                          IsBold ,
                          BranchName
		                )                        
                        SELECT  N'' JournalMemo ,
                                2 ,
                                1 ,
                                ''                                                                     
                SELECT  * ,
                        ROW_NUMBER() OVER ( ORDER BY AccountObjectName, AccountObjectCode , OrderType , PostedDate , RefDate, RefNo, SortOrder, DetailPostOrder ) AS RowNum
                FROM    #Result         
            END                       
        DELETE  #Result
        DELETE  #tblSelectedAccountNumber        
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO


ALTER TABLE dbo.Template
  DROP CONSTRAINT FK_Template_Type
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateDetail
  DROP CONSTRAINT FK_TemplateDetail_Template
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Type
  DROP CONSTRAINT FK_Type_TypeGroup
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF (OBJECT_ID('FK__TT153Adju__TypeI__25869641', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.TT153AdjustAnnouncement DROP CONSTRAINT FK__TT153Adju__TypeI__25869641
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF (OBJECT_ID('FK__TT153Dele__TypeI__63D8CE75', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.TT153DeletedInvoice DROP CONSTRAINT FK__TT153Dele__TypeI__63D8CE75
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF (OBJECT_ID('FK__TT153Dest__TypeI__22AA2996', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.TT153DestructionInvoice DROP CONSTRAINT FK__TT153Dest__TypeI__22AA2996
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF (OBJECT_ID('FK__TT153Lost__TypeI__1FCDBCEB', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.TT153LostInvoice DROP CONSTRAINT FK__TT153Lost__TypeI__1FCDBCEB
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF (OBJECT_ID('FK__TT153Publ__TypeI__1CF15040', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.TT153PublishInvoice DROP CONSTRAINT FK__TT153Publ__TypeI__1CF15040
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF (OBJECT_ID('FK__TT153Regi__TypeI__1A14E395', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.TT153RegisterInvoice DROP CONSTRAINT FK__TT153Regi__TypeI__1A14E395
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'c373a31f-7f96-40b9-ac1f-06fa5ff5835c'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '0f567619-54d2-42ad-a0ad-0a971337ea01'
UPDATE dbo.AccountDefault SET FilterAccount = N'131;1388;156;157;511;642;6421;6422;711', DefaultAccount = N'' WHERE ID = 'fbd4cd71-32bb-493b-8c66-10407058d2e4'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '695141da-ed78-4846-b22a-11f41a2f89d6'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '6888bf87-f954-4664-b658-173edecbb03c'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'bb98da63-d23d-4854-b552-18f9e6743d35'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'f58b5b4a-8d6b-48d5-b0e1-1d8a4cef73dd'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'b56d27f4-a1e2-4a6a-841c-20d4e40946f3'
UPDATE dbo.AccountDefault SET ColumnName = N'ExportTaxAccount' WHERE ID = '01eada3f-b70f-4229-a2c2-2231aeaf3936'
UPDATE dbo.AccountDefault SET ColumnName = N'ExportTaxAccount' WHERE ID = 'b37baafc-1f77-479f-b8df-29533cbcfb4e'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'b4107de7-1296-477a-a936-2a5ac2d93b6a'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'e62b18f8-9f40-4786-9b40-2a75ad2d612b'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '26ea34ff-ca2a-4181-aa6f-2e6bdc6e9d09'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '8608c395-fc09-4f9b-93d3-333813293770'
UPDATE dbo.AccountDefault SET FilterAccount = N'152;153;154;155;156;157;642;811' WHERE ID = '81006a04-70d1-4c1f-9b93-3b4240db0781'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'c1fab516-a28e-4fbd-bb04-3b65774dcc53'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '7d02717f-5e3f-4267-87c3-41df14f8b79f'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '8c08f87c-1c39-45a0-968e-4bae53b8fc15'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '7e49ab8d-112a-46b5-9e7c-4e9b69165976'
UPDATE dbo.AccountDefault SET FilterAccount = N'111;112;1121;511;635;642;6421;6422' WHERE ID = '1a4bb09c-939d-4228-9d02-4f62e68326b5'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '0a76abe4-9e47-49e7-a9da-5ad9aac91c4b'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '73065ec5-5adf-4cad-a856-5c52a32d6775'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '814bbabd-ee91-49a7-814f-61c4b99d38d9'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '72c0cee9-c749-4dac-b808-69a54f1f440e'
UPDATE dbo.AccountDefault SET FilterAccount = N'152;153;155;156;611' WHERE ID = '4204499f-5de6-4a36-b1e8-6a8efe04f250'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '38dbe9bd-2d72-47d1-9ca2-6f5aee5bb7a2'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '22e0e122-eb56-4929-ad36-71b0252a7f49'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'b7feacb1-6569-498a-b03a-71b7830038d7'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '44ff04fc-1b36-4482-b097-7d98f2889f7e'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '7442f0eb-6da4-4ade-a6a7-7d9f96701f59'
UPDATE dbo.AccountDefault SET ColumnName = N'ExportTaxAccount' WHERE ID = 'bb477284-47fd-4455-8168-7f90ccb40bfb'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'f9b846da-2da6-4b3c-9c31-86c650575fb6'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '88cf7782-e79a-4bf1-8343-86fb6ecb5dd4'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '47827e64-bf3d-4a70-946e-8963a2d7a0cf'
UPDATE dbo.AccountDefault SET FilterAccount = N'152;153;154;156;157;241;242;611;631;632;642;811' WHERE ID = 'd7e170cc-39d8-498f-a684-918c4efdb5d8'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '6e040620-d49f-4d4e-a68e-939fbbe200c1'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '61a57378-36e3-4465-9f23-a46747fc2564'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '802bb90b-93f9-4dbc-ae0e-a63fa874d475'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'f2522da9-c810-41c3-bc65-acb37a1543b8'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '970960d3-896b-4352-b08b-b2e581523d49'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '0332b1b2-d842-48b1-8cf5-b6abfe7489f2'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '218421ec-a54a-4bbc-b046-b986443b51d7'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '6f37b0e2-1239-47b0-81c7-baa44fd1d404'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'ec67e45f-86fb-41d9-9eb5-bc4f8f4e7c8e'
UPDATE dbo.AccountDefault SET FilterAccount = N'154;241;242;611;642;6421;6422;811' WHERE ID = '6852a598-c394-48c5-891d-bc91d56439f3'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '390a6fd7-b692-4d97-a67f-bd2b1291628d'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'ba2244e8-d102-4ba0-be1d-c1597095c010'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'c91814da-554e-4a30-8df0-d45af1352959'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '31852cb5-53f5-403c-ad98-d9e0480af11e'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'dbd1af0d-08e3-4220-aa02-e6268de67d0f'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'c1e09a67-ca57-4f36-a853-ed8e6d7401c7'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '42000894-b9d2-4524-be57-ef62cf8acc2c'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.Bank SET BankName = N'Ngân hàng Việt Nam Thịnh Vượng', BankNameRepresent = N'Vietnam Prosperity Bank	', Address = N'', Description = N'Ngân hàng Việt Nam Thịnh Vượng' WHERE ID = '11324817-f740-4f64-bce4-b3b124b0617d'

INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('6ad3e245-9255-4ab0-b161-0d7c5699443d', N'MHB', N'Ngân hàng Phát triển nhà ĐBSCL', N'Mekong Housing Bank', N'', N'Ngân hàng Phát triển nhà ĐBSCL', 1, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('18a73d83-1dec-446f-831f-110006692763', N'ANZVL', N'Ngân hàng TNHH một thành viên ANZ (Việt Nam)', N'ANZ Bank (Vietnam) Limited ', NULL, N'Ngân hàng TNHH một thành viên ANZ (Việt Nam)', 0, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('3426240f-e64f-4383-913e-1eaa2af7435e', N'VIETBANK', N'Ngân hàng TMCP Việt Nam Thương Tín', N'Vietnam Thuong Tin Commercial Joint Stock Bank', N'', N'Ngân hàng TMCP Việt Nam Thương Tín', NULL, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('a91045c4-5e66-4d03-b194-23ac34452e77', N'SHB', N'Ngân hàng TMCP Sài Gòn – Hà Nội', N'Saigon Hanoi Commercial Joint Stock Bank ', NULL, N'Ngân hàng TMCP Sài Gòn – Hà Nội', 0, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('095ddb71-5b3b-4fde-b3d9-397b85fe8c88', N'PNB', N'Ngân hàng TMCP Phương Nam', N'Southern Commercial Joint Stock Bank', NULL, N'Ngân hàng TMCP Phương Nam', 0, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('17b5d00b-b0dc-4e49-b210-3fe9d535591d', N'Habubank', N'Ngân hàng TMCP Nhà Hà Nội', N'Hanoi Building Commercial Joint Stock Bank', NULL, N'Ngân hàng TMCP Nhà Hà Nội', 0, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('50c9f4bd-e397-4baa-999c-436ae873c958', N'VID Public Bank ', N'Ngân hàng VID public', N'VID Public Bank ', NULL, N'Ngân hàng VID public', 0, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('9219d378-24e6-46e0-8198-629e615cf82b', N'LienVietBank', N'Ngân hàng Thương mại Cổ phần Liên Việt', N'LienViet Commercial Joint Stock Bank', NULL, N'Ngân hàng Thương mại Cổ phần Liên Việt', 0, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('ad1419cf-da37-4166-a512-73c8c433ed6e', N'SeABank', N'Ngân hàng TMCP Đông Nam Á', N'Southeast Asia Commercial Joint Stock Bank', NULL, N'Ngân hàng TMCP Đông Nam Á', 0, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('a2c2aee5-10fb-4c7f-ad1e-83db2d315ae1', N'TPBANK', N'Ngân hàng TMCP Tiên Phong', N'Tien Phong Bank', N'', N'Ngân hàng TMCP Tiên Phong', NULL, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('bdcf0d8a-26cb-49fd-9fb7-92ecdc013114', N'VIB', N'Ngân hàng TMCP Quốc Tế Việt Nam', N'Vietnam International Commercial Joint Stock Bank ', N'', N'Ngân hàng TMCP Quốc Tế Việt Nam', 0, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('4d8d7828-fc2a-4836-9ee2-a1ce91e46f62', N'ACB', N'Ngân hàng TMCP Á Châu', N'Asia Commercial Joint Stock Bank', N'', N'Ngân hàng TMCP Á Châu', NULL, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('b8404e87-d365-482f-baf5-a6f6be25579c', N'IVB', N'Ngân hàng TNHH Indovina', N'Indovina Bank Limited', NULL, N'Ngân hàng TNHH Indovina', 0, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('252b3394-d971-4f9a-84de-ac72e8e7e613', N'NAMABANK', N'Ngân hàng TMCP Nam Á', N'Nam A Bank', N'', N'Ngân hàng TMCP Nam Á', NULL, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('50f07954-571a-4f5d-9b6e-c338f7e04fb9', N'MaritimeBank', N'Ngân hàng TMCP Hàng hải Việt Nam', N'Vietnam Maritime Commercial Stock Bank', N'', N'Ngân hàng TMCP Hàng hải Việt Nam', 0, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('80515148-e26b-435d-9cbc-cdf10c5deb21', N'DongA Bank', N'Ngân hàng TMCP Đông Á', N'Eastern Asia Bank', NULL, N'Ngân hàng TMCP Đông Á', 0, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('d6165a83-5ee5-41cd-8951-d35b718f700f', N'BACABANK', N'Ngân hàng TMCP Bắc Á', N'Bac A Bank', N'', N'Ngân hàng TMCP Bắc Á', NULL, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('09ddd03b-e386-404a-ab0b-dfb6d52403a8', N'ABBANK', N'Ngân hàng TMCP An Bình', N'An Binh Bank', N'', N'Ngân hàng TMCP An Bình', NULL, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('b6625d82-9679-4dfa-a8ec-ead4ed3721d0', N'ANZ1', N'Ngân hàng TMCP UC', N'Ten tieng anh', N'', N'Ngân hàng TMCP UC', NULL, NULL, 1)
INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES ('9aabfe29-b12a-4d28-a3a7-f17ca7a7dd70', N'Eximbank', N'Ngân hàng TMCP Xuất nhập khẩu Việt Nam', N'Vietnam Commercial Joint Stock Export – Import Bank', NULL, N'Ngân hàng TMCP Xuất nhập khẩu Việt Nam', 0, NULL, 1)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'AED'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'AFN'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'ALL'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'AMD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'ANG'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'AOA'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'ARS'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'AUD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'AWG'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'AZN'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'BAM'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'BBD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'BDT'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'BGN'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'BHD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'BIF'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'BMD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'BND'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'BOB'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'BRL'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'BSD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'BTC'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'BTN'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'BWP'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'BYR'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'BZD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'CAD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'CDF'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'CHF'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'CLF'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'CLP'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'CNY'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'COP'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'CRC'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'CUC'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'CUP'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'CVE'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'CZK'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'DJF'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'DKK'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'DOP'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'DZD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'EEK'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'EGP'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'ERN'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'ETB'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'EUR'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'FJD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'FKP'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'GBP'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'GEL'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'GGP'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'GHS'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'GIP'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'GMD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'GNF'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'GTQ'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'GYD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'HKD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'HNL'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'HRK'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'HTG'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'HUF'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'IDR'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'ILS'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'IMP'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'INR'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'IQD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'IRR'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'ISK'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'JEP'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'JMD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'JOD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'JPY'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'KES'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'KGS'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'KHR'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'KMF'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'KPW'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'KRW'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'KWD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'KYD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'KZT'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'LAK'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'LBP'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'LKR'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'LRD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'LSL'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'LTL'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'LVL'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'LYD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'MAD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'MDL'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'MGA'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'MKD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'MMK'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'MNT'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'MOP'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'MRO'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'MTL'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'MUR'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'MVR'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'MWK'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'MXN'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'MYR'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'MZN'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'NAD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'NGN'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'NIO'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'NOK'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'NPR'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'NZD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'OMR'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'PAB'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'PEN'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'PGK'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'PHP'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'PKR'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'PLN'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'PYG'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'QAR'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'RON'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'RSD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'RUB'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'RWF'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'SAR'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'SBD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'SCR'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'SDG'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'SEK'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'SGD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'SHP'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'SLL'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'SOS'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'SRD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'STD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'SVC'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'SYP'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'SZL'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'THB'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'TJS'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'TMT'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'TND'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'TOP'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'TRY'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'TTD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'TWD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'TZS'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'UAH'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'UGX'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'UYU'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'UZS'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'VEF'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'VUV'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'WST'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'XAF'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'XAG'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'XAU'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'XCD'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'XDR'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'XOF'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'XPF'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'YER'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'ZAR'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'ZMK'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'ZMW'
UPDATE dbo.Currency SET IsActive = 0 WHERE ID = N'ZWL'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('846d2dfc-9e1f-46dd-87d1-04b8701eb69b', N'I.03', N'Ti-tan', N'', 18.0000000000, '9da806c7-ff3e-43eb-b598-44333ef0bb09', 0, 2, 0, 1, 0, N'1-1')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('6b3d1660-e2e8-4eed-a529-07aac3c1606a', N'IX', N'Khí thiên nhiên, khí than', N'', NULL, NULL, 1, 1, 0, 1, 0, N'9')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('9c6e71d8-6631-46f4-846e-1216147b55c3', N'V.02', N'Nước thiên nhiên dùng cho sản xuất thủy điện', N'', 5.0000000000, 'c4f7bb28-5c24-41f0-b98d-d2877876a53a', 0, 2, 0, 1, 0, N'5-2')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('142d5a0f-aebc-4a08-80b8-170bee0aa0c3', N'I.10', N'Đồng', N'', 15.0000000000, '9da806c7-ff3e-43eb-b598-44333ef0bb09', 0, 2, 0, 1, 0, N'1-10')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('42c02910-639a-4b29-8fc9-199cf33c8e73', N'II.23', N'Thạch anh tinh thể màu tím xanh, vàng lục, da cam; Cờ-ri-ô-lít (Cryolite); Ô-pan (Opan) quý màu trắng, đỏ lửa; Phen-sờ-phát (Fenspat); Birusa; Nê-phờ-rít (Nefrite)', N'', 18.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-23')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('be07c843-94f1-434d-8ba4-1a679ce03b78', N'VIII.01', N'Dầu thô (Đối với dự án khuyến khích đầu tư)', N'', NULL, 'b9f94a5e-7ec9-4f06-aa47-6a18872e4aa4', 1, 2, 0, 1, 0, N'8-1')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('aa18d6cd-0a23-4ead-8298-1b0cf0006eff', N'VI', N'Yến sào thiên nhiên', N'', 20.0000000000, NULL, 0, 1, 0, 1, 0, N'6')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('f5dd416c-9e86-4357-b1ef-1c586be080a6', N'II.24', N'Khoáng sản không kim loại khác', N'', 10.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-24')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('b2ce0bb5-4b3b-47c7-9abd-1fc541e2be31', N'I.13', N'Khoáng sản kim loại khác', N'', 15.0000000000, '9da806c7-ff3e-43eb-b598-44333ef0bb09', 0, 2, 0, 1, 0, N'1-13')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('9d6fd7ae-a8d9-4e59-a8e2-22e707b979e0', N'IV.01', N'Ngọc trai, bào ngư, hải sâm', N'', 10.0000000000, '815b39a1-fbf8-46c3-b5eb-46836fb1f91b', 0, 2, 0, 1, 0, N'4-1')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('10e8ecde-2153-46e3-a88d-2644aa62a280', N'I.12', N'Coban, Molipden, Thủy ngân, Magie, Vanadi', N'', 15.0000000000, '9da806c7-ff3e-43eb-b598-44333ef0bb09', 0, 2, 0, 1, 0, N'1-12')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('586be91f-d5ad-4f63-a7db-28659ca1c8cc', N'II.08', N'Granite', N'', 15.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-8')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('117a4f4d-6e72-4bdf-ab2e-28cc76893717', N'I.01', N'Sắt', N'', 14.0000000000, '9da806c7-ff3e-43eb-b598-44333ef0bb09', 0, 2, 0, 1, 0, N'1-2')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('9cd0464f-fefe-43f7-ab03-2a55ecbcb08f', N'IX.02', N'Khí thiên nhiên, khí than (Đối với dự án khác)', N'', NULL, '6b3d1660-e2e8-4eed-a529-07aac3c1606a', 1, 2, 0, 1, 0, N'9-2')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('cb665bd7-0d27-48b5-a721-2d16f8ba3f43', N'II.17', N'Than An-tra-xít (Antraxit) lộ thiên', N'', 12.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-17')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('f3badb41-37c9-4a0b-b5c9-2d4564894baf', N'II.10', N'Đô-lô-mít (Dolomite), Quắc-zít (Quatzite)', N'', 15.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-10')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('586e1888-434a-4bbe-acec-2e387ae6787d', N'II.20', N'Kim cương, Rubi, Sapphire', N'', 27.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-20')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('7b1b6c7a-1e79-402a-a233-2f77b9bd9fe3', N'IX.01.02', N'Trên 5 triệu m3 đến 10 triệu m3/ngày', N'', 3.0000000000, '0b38eb8d-81c9-4625-b6a2-a3af20ee9f84', 0, 3, 0, 1, 0, N'9-1-2')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('357cec56-2e83-40c3-88e7-301d45c55d40', N'III.02', N'Gỗ nhóm II', N'', 30.0000000000, 'd1bbce3c-cea8-4eab-bbec-77f6c76bac9f', 0, 2, 0, 1, 0, N'3-2')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('cceb8791-c5dd-4dd0-b559-321f572a19bc', N'VIII.02.04', N'Trên 75.000 thùng đến 100.000 thùng/ngày', N'', 19.0000000000, 'ec30fa46-f1dc-4763-b6e2-70809525d531', 0, 3, 0, 1, 0, N'8-2-4')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('3a8cad1b-4055-4c34-af22-367671f5a420', N'VIII.02.05', N'Trên 100.000 thùng đến 150.000 thùng/ngày', N'', 24.0000000000, 'ec30fa46-f1dc-4763-b6e2-70809525d531', 0, 3, 0, 1, 0, N'8-2-5')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('1b45d2c8-f008-46d9-b058-3ee5006a0de9', N'II.18', N'Than nâu, than mỡ', N'', 12.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-18')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('fc61014b-07ca-46c2-be81-40d2ff89386c', N'VII', N'Tài nguyên khác', N'', 10.0000000000, NULL, 0, 1, 0, 1, 0, N'7')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('32a4025f-8a4f-4587-afb3-42ca3522f5b6', N'II.01', N'Đất khai thác để san lấp, xây dựng công trình', N'', 7.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-1')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('9da806c7-ff3e-43eb-b598-44333ef0bb09', N'I', N'Khoáng sản kim loại', N'', NULL, NULL, 1, 1, 0, 1, 0, N'1')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('af818ef8-acbd-4c7c-aec4-466db34e1c79', N'II.05', N'Cát', N'', 15.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-5')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('815b39a1-fbf8-46c3-b5eb-46836fb1f91b', N'IV', N'Hải sản tự nhiên', N'', NULL, NULL, 1, 1, 0, 1, 0, N'4')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('d9157e2c-801d-42d9-a016-46c0aa914b70', N'V.03.01.01', N'Dùng cho sản xuất nước sạch', N'', 1.0000000000, '3a032136-00f1-406c-a256-58778d47f10f', 0, 4, 0, 1, 0, N'5-3-1-1')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('d7fff70e-c38a-407e-b663-484b4f20a800', N'II.16', N'Than an-tra-xít (Antraxit) hầm lò', N'', 10.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-16')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('b0a0d049-b56e-4dac-882d-49873bbc3fb4', N'II.04', N'Đá hoa trắng', N'', 15.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-4')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('56e336bd-a021-4a7e-98cf-4ba9558bd5bf', N'I.08', N'Chì, kẽm', N'', 15.0000000000, '9da806c7-ff3e-43eb-b598-44333ef0bb09', 0, 2, 0, 1, 0, N'1-8')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('00719c29-613c-4966-b4bb-4d711e030f24', N'II.07', N'Đất làm gạch', N'', 15.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-7')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('1f331d9a-e1b4-4a05-aa57-523f14699189', N'III.01', N'Gỗ nhóm I', N'', 35.0000000000, 'd1bbce3c-cea8-4eab-bbec-77f6c76bac9f', 0, 2, 0, 1, 0, N'3-1')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('4b812f08-18bb-4621-8ed7-56f87b7f5b32', N'I.04', N'Vàng (AU)', N'', 17.0000000000, '9da806c7-ff3e-43eb-b598-44333ef0bb09', 0, 2, 0, 1, 0, N'1-4')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('27ef03df-7ee5-456d-86e5-579057a05243', N'I.11', N'Ni-ken (Niken)', N'', 10.0000000000, '9da806c7-ff3e-43eb-b598-44333ef0bb09', 0, 2, 0, 1, 0, N'1-11')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('20cf6971-f64c-432c-9453-5876f745a52e', N'II.06', N'Cát làm thủy tinh', N'', 15.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-6')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('3a032136-00f1-406c-a256-58778d47f10f', N'V.03.01', N'Sử dụng nước mặt', N'', NULL, '1dd7f664-0f9b-4762-87ae-a7b3bf73956a', 1, 3, 0, 1, 0, N'5-3-1')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('5e9abb57-310c-4fbd-82ef-5918e5bbf278', N'VIII.01.05', N'Trên 100.000 thùng đến 150.000 thùng/ngày', N'', 18.0000000000, 'be07c843-94f1-434d-8ba4-1a679ce03b78', 0, 3, 0, 1, 0, N'8-1-5')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('c3f28d29-ae9c-42c4-847b-59f22c220e4a', N'V.03.02.01', N'Dùng cho sản xuất nước sạch', N'', 5.0000000000, '8b8013aa-00a0-4328-a0e0-c49a180123d7', 0, 4, 0, 1, 0, N'5-3-2-1')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('5ffe9cd9-2e2c-43a8-af9e-5c464562eed5', N'I.09', N'Nhôm, Bô-xít (Bouxite)', N'', 12.0000000000, '9da806c7-ff3e-43eb-b598-44333ef0bb09', 0, 2, 0, 1, 0, N'1-9')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('f68c8a22-24d5-4172-8e63-5cc104ace9cf', N'II.14', N'A-pa-tít (Apatit)', N'', 8.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-14')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('b9f94a5e-7ec9-4f06-aa47-6a18872e4aa4', N'VIII', N'Dầu thô', N'', NULL, NULL, 1, 1, 0, 1, 0, N'8')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('331405e0-576b-418d-ab70-6b42790020e7', N'VIII.01.01', N'Đến 20.000 thùng/ngày', N'', 7.0000000000, 'be07c843-94f1-434d-8ba4-1a679ce03b78', 0, 3, 0, 1, 0, N'8-1-1')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('0dff4de7-6fdc-47eb-9bfa-6b4f6d4b29ca', N'VIII.01.04', N'Trên 75.000 thùng đến 100.000 thùng/ngày', N'', 13.0000000000, 'be07c843-94f1-434d-8ba4-1a679ce03b78', 0, 3, 0, 1, 0, N'8-1-4')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('44d0ab15-d54f-418e-bbb5-6da27e06d804', N'V.03.02.02', N'Dùng cho mục đích khác', N'', 8.0000000000, '8b8013aa-00a0-4328-a0e0-c49a180123d7', 0, 4, 0, 1, 0, N'5-3-2-2')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('ee7a6671-2926-4e73-8970-7024ecb65fb8', N'III.07', N'Củi', N'', 5.0000000000, 'd1bbce3c-cea8-4eab-bbec-77f6c76bac9f', 0, 2, 0, 1, 0, N'3-7')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('ec30fa46-f1dc-4763-b6e2-70809525d531', N'VIII.02', N'Dầu thô (Đối với dự án khác)', N'', NULL, 'b9f94a5e-7ec9-4f06-aa47-6a18872e4aa4', 1, 2, 0, 1, 0, N'8-2')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('dcf2d7c6-d127-4602-a74e-7291a1715bb1', N'I.07', N'Vôn-phờ-ram (wolfram), ăng-ti-moan (antimoan)', N'', 20.0000000000, '9da806c7-ff3e-43eb-b598-44333ef0bb09', 0, 2, 0, 1, 0, N'1-7')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('d1bbce3c-cea8-4eab-bbec-77f6c76bac9f', N'III', N'Sản phẩm của rừng tự nhiên', N'', NULL, NULL, 1, 1, 0, 1, 0, N'3')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('c6f22169-5b19-4ddf-808c-7e38c5c39173', N'IX.02.03', N'Trên 10 triệu m3/ngày', N'', 10.0000000000, '9cd0464f-fefe-43f7-ab03-2a55ecbcb08f', 0, 3, 0, 1, 0, N'9-2-3')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('dde7d615-9da8-4001-98b0-8699ea0c6a3a', N'IX.02.02', N'Trên 5 triệu m3 đến 10 triệu m3/ngày', N'', 5.0000000000, '9cd0464f-fefe-43f7-ab03-2a55ecbcb08f', 0, 3, 0, 1, 0, N'9-2-2')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('c29390bc-4ad5-44b3-992d-88f806ed1e0b', N'II.12', N'Mica, thạch anh kỹ thuật', N'', 13.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-12')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('94ed1001-87be-47c0-854e-8cee16fa456b', N'VIII.01.02', N'Trên 20.000 thùng đến 50.000 thùng/ngày', N'', 9.0000000000, 'be07c843-94f1-434d-8ba4-1a679ce03b78', 0, 3, 0, 1, 0, N'8-1-2')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('6741c2cf-6206-409c-97ec-950cc1b432c0', N'II.21', N'E-mo-rốt (Emerald), A-lếch-xan-đờ-rít (Alexandrite), Ô-pan (Opan) quý màu đen', N'', 25.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-21')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('924845b7-a6dd-4ffd-b0fd-952b2e1cd979', N'VIII.02.03', N'Trên 50.000 thùng đến 75.000 thùng/ngày', N'', 14.0000000000, 'ec30fa46-f1dc-4763-b6e2-70809525d531', 0, 3, 0, 1, 0, N'8-2-3')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('3d6c69a6-ada2-4a85-bc34-9f839bdef62a', N'II.11', N'Cao lanh', N'', 13.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-11')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('7ea542a6-a9aa-4f22-a859-a199b54e3e94', N'II.02', N'Đá, sỏi', N'', 10.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-2')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('0b38eb8d-81c9-4625-b6a2-a3af20ee9f84', N'IX.01', N'Khí thiên nhiên, khí than (Đối với dự án khuyến khích đầu tư)', N'', NULL, '6b3d1660-e2e8-4eed-a529-07aac3c1606a', 1, 2, 0, 1, 0, N'9-1')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('1dd7f664-0f9b-4762-87ae-a7b3bf73956a', N'V.03', N'Nước thiên nhiên dùng cho sản xuất, kinh doanh (trừ nước quy định tại Điểm 1 & 2 của nhóm này)', N'', NULL, 'c4f7bb28-5c24-41f0-b98d-d2877876a53a', 1, 2, 0, 1, 0, N'5-3')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('665c9f45-205f-4bd3-99c6-a93dec1db17b', N'III.10', N'Hồi, quế, sa nhân, thảo quả', N'', 10.0000000000, 'd1bbce3c-cea8-4eab-bbec-77f6c76bac9f', 0, 2, 0, 1, 0, N'3-10')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('56073d7f-6df4-41f0-a77e-aef3ab6c3b8f', N'VIII.02.06', N'Trên 150.000 thùng/ngày', N'', 29.0000000000, 'ec30fa46-f1dc-4763-b6e2-70809525d531', 0, 3, 0, 1, 0, N'8-2-6')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('e88fcdec-d28f-4c98-be3b-af1b4f1ff52c', N'I.06', N'Bạch kim, bạc, thiếc', N'', 12.0000000000, '9da806c7-ff3e-43eb-b598-44333ef0bb09', 0, 2, 0, 1, 0, N'1-6')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('bd065593-a452-413e-bf8b-af213e278d71', N'VIII.01.06', N'Trên 150.000 thùng/ngày', N'', 23.0000000000, 'be07c843-94f1-434d-8ba4-1a679ce03b78', 0, 3, 0, 1, 0, N'8-1-6')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('7da9e0d6-7037-4a6f-8d50-b1bed873add4', N'III.11', N'Sản phẩm khác của rừng tự nhiên', N'', 5.0000000000, 'd1bbce3c-cea8-4eab-bbec-77f6c76bac9f', 0, 2, 0, 1, 0, N'3-11')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('e22787e8-6ccf-4c09-a03e-b3d23286ab05', N'VIII.02.01', N'Đến 20.000 thùng/ngày', N'', 10.0000000000, 'ec30fa46-f1dc-4763-b6e2-70809525d531', 0, 3, 0, 1, 0, N'8-2-1')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('5569d4ee-5805-4b79-818a-b5fff97a4609', N'II.09', N'Sét chịu lửa', N'', 13.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-9')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('8fef5880-e050-4fbf-961a-b84051c8519c', N'III.05', N'Gỗ nhóm V, VI, VII, VIII và các loại gỗ khác', N'', 12.0000000000, 'd1bbce3c-cea8-4eab-bbec-77f6c76bac9f', 0, 2, 0, 1, 0, N'3-5')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('8916ef6d-8b64-4eb3-9c7c-b89b0f998b66', N'III.08', N'Tre, trúc, nứa, mai. giang, tranh, vầu, lồ ô', N'', 10.0000000000, 'd1bbce3c-cea8-4eab-bbec-77f6c76bac9f', 0, 2, 0, 1, 0, N'3-8')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('0b770a69-f690-4c27-a425-baded1fca334', N'IX.01.01', N'Đến 5 triệu m3/ngày', N'', 1.0000000000, '0b38eb8d-81c9-4625-b6a2-a3af20ee9f84', 0, 3, 0, 1, 0, N'9-1-1')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('adbaaf07-a674-4e17-9a0a-c0948fca8e7b', N'IX.02.01', N'Đến 5 triệu m3/ trên ngày', N'', 2.0000000000, '9cd0464f-fefe-43f7-ab03-2a55ecbcb08f', 0, 3, 0, 1, 0, N'9-2-1')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('f3371c68-9bd0-41f1-875d-c15ab624f252', N'II.15', N'Séc-păng-tin (Secpentin)', N'', 6.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-15')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('8b8013aa-00a0-4328-a0e0-c49a180123d7', N'V.03.02', N'Sử dụng nước dưới đất', N'', NULL, '1dd7f664-0f9b-4762-87ae-a7b3bf73956a', 1, 3, 0, 1, 0, N'5-3-2')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('ec760f72-255e-46ed-b58b-c7dae4b80555', N'II', N'Khoáng sản không kim loại', N'', NULL, NULL, 1, 1, 0, 1, 0, N'2')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('01429db7-5886-4375-970c-cb2db024a982', N'IV.02', N'Hải sản tự nhiên khác', N'', 2.0000000000, '815b39a1-fbf8-46c3-b5eb-46836fb1f91b', 0, 2, 0, 1, 0, N'4-2')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('f24b9cf7-446a-4b69-ab2e-cd4d91e85d82', N'VIII.01.03', N'Trên 50.000 thùng đến 75.000 thùng/ngày', N'', 11.0000000000, 'be07c843-94f1-434d-8ba4-1a679ce03b78', 0, 3, 0, 1, 0, N'8-1-3')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('18acacb5-87d1-46e9-9168-ce1af977b83d', N'III.04', N'Gỗ nhóm IV', N'', 18.0000000000, 'd1bbce3c-cea8-4eab-bbec-77f6c76bac9f', 0, 2, 0, 1, 0, N'3-4')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('4783b61e-e66c-4707-a510-ce92fb96e953', N'V.01', N'Nước khoáng thiên nhiên, nước nóng thiên nhiên, nước thiên nhiên tinh lọc đóng chai, đóng hộp', N'', 10.0000000000, 'c4f7bb28-5c24-41f0-b98d-d2877876a53a', 0, 2, 0, 1, 0, N'5-1')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('bf585566-5eb5-4ba4-9a4c-d1f27f024ee7', N'I.05', N'Đất hiếm', N'', 18.0000000000, '9da806c7-ff3e-43eb-b598-44333ef0bb09', 0, 2, 0, 1, 0, N'1-5')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('c4f7bb28-5c24-41f0-b98d-d2877876a53a', N'V', N'Nước thiên nhiên', N'', NULL, NULL, 1, 1, 0, 1, 0, N'5')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('7d21894e-54c2-4a30-a6e2-d5c4fb381bb0', N'II.13', N'Pi-rít (pirite), Phốt-pho-rít (Phosphorite)', N'', 10.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-13')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('582909f0-f87a-42a5-8cad-d78cabbee814', N'III.06', N'Cành, ngọn, gốc, rễ', N'', 10.0000000000, 'd1bbce3c-cea8-4eab-bbec-77f6c76bac9f', 0, 2, 0, 1, 0, N'3-6')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('b2dbea74-3f39-4133-91e4-d871daec8909', N'III.09', N'Trầm hương, kỳ nam', N'', 25.0000000000, 'd1bbce3c-cea8-4eab-bbec-77f6c76bac9f', 0, 2, 0, 1, 0, N'3-9')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('792f098c-fcae-4cab-bf01-ec3e5963f437', N'VIII.02.02', N'Trên 20.000 thùng đến 50.000 thùng/ngày', N'', 12.0000000000, 'ec30fa46-f1dc-4763-b6e2-70809525d531', 0, 3, 0, 1, 0, N'8-2-2')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('941140c0-7430-4aa0-b19b-f1aa39a5d951', N'I.02', N'Măng-gan (Mangan)', N'', 14.0000000000, '9da806c7-ff3e-43eb-b598-44333ef0bb09', 0, 2, 0, 1, 0, N'1-3')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('3994d158-56ca-4a56-8d7b-f1b3b0e37cd6', N'II.03', N'Đá nung vôi và sản xuất xi măng', N'', 10.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-3')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('6c8336e3-308f-450b-8f15-f217ac6f3d46', N'IX.01.03', N'Trên 10 triệu m3/ngày', N'', 6.0000000000, '0b38eb8d-81c9-4625-b6a2-a3af20ee9f84', 0, 3, 0, 1, 0, N'9-1-3')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('ec4f0d9b-b247-48a6-9601-f5d5f9551cfd', N'V.03.01.02', N'Dùng cho mục đích khác', N'', 3.0000000000, '3a032136-00f1-406c-a256-58778d47f10f', 0, 4, 0, 1, 0, N'5-3-1-2')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('e58259da-512e-4f16-9424-f850c79eda0b', N'II.19', N'Than khác', N'', 10.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-19')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('1cf045a1-4640-4152-b8b4-f91b6850f57b', N'III.03', N'Gỗ nhóm III', N'', 20.0000000000, 'd1bbce3c-cea8-4eab-bbec-77f6c76bac9f', 0, 2, 0, 1, 0, N'3-3')
INSERT dbo.MaterialGoodsResourceTaxGroup(ID, MaterialGoodsResourceTaxGroupCode, MaterialGoodsResourceTaxGroupName, Unit, TaxRate, ParentID, IsParentNode, Grade, OrderPriority, IsActive, IsSecurity, OrderFixCode) VALUES ('da6283d1-7466-4adf-944f-f943737cc285', N'II.22', N'A-dít, Rô-đô-lít (Rodolite), Py-rốt (Pyrope), Bê-rin (Berin), Sờ-pi-nen (Spinen), Tô-paz (Topaz)', N'', 18.0000000000, 'ec760f72-255e-46ed-b58b-c7dae4b80555', 0, 2, 0, 1, 0, N'2-22')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('4f49c3b7-3ee3-42b6-b3d1-0fcf681b52e5', N'109', N'Điều hòa nhiệt độ công suất từ 90.000 BTU trở xuống', N'1-9', 'c36825a9-3e51-4586-bf7f-8e532b4ade91', 0, 10.00, 2, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('3394b159-f3f7-433b-98a3-1ca29635dc7a', N'10472', N'Loại chở người từ 10 đến dưới 16 chỗ', N'1-4-7-2', 'b5c283d9-a085-4740-bed0-cbebe12e088e', 0, 10.00, 4, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('a51e1b7b-8629-441d-9977-257fbe54ab76', N'204', N'Kinh doanh đặt cược', N'2-4', '98cff596-b53d-40c8-9f22-84fa60c8b1ea', 0, 30.00, 2, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('6f8135eb-bffa-48cb-acf1-2612c902927b', N'10417', N'Loại có dung tích xi lanh trên 5.000cc tới 6.000cc', N'1-4-1-7', 'ea63f748-b692-45e6-8e68-a079aee29cf0', 0, 130.00, 4, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('79172063-5416-40c6-bbd6-2a43a417bf08', N'1046', N'Xe ô tô chạy bằng năng lượng sinh học', N'1-4-6', '757048cb-32d1-41ba-ab69-91faba077c7e', 0, 0.00, 3, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('ea1fa202-ab64-46f5-954e-2ad37620713d', N'102', N'Rượu', N'1-2', 'c36825a9-3e51-4586-bf7f-8e532b4ade91', 1, 0.00, 2, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('ca674619-bcb5-494f-814a-2d060fa3ee85', N'1082', N'Xăng E5', N'1-8-2', '6812d063-4cc3-43c9-bbd3-5c43e66ec9d7', 0, 8.00, 3, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('92e2f8e4-a52c-40cc-b91f-32b7eb4c8448', N'10416', N'Loại có dung tích xi lanh trên 4.000cc tới 5.000cc', N'1-4-1-6', 'ea63f748-b692-45e6-8e68-a079aee29cf0', 0, 110.00, 4, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('29bba896-0640-4766-b7a7-3363258e9304', N'101', N'Thuốc lá điếu, xì gà và các chế phẩm khác từ cây thuốc lá', N'1-1', 'c36825a9-3e51-4586-bf7f-8e532b4ade91', 0, 70.00, 2, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('64f9235d-ceed-466b-a591-3735c0723e9d', N'10441', N'Loại có dung tích xi lanh từ 2.500cc trở xuống', N'1-4-4-1', 'cc8d6740-12eb-4ef4-98e7-9ea05dc3c1f7', 0, 10.00, 4, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('38bb4d45-2f25-48f7-9310-42b1c390aea6', N'1045', N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng', N'1-4-5', '757048cb-32d1-41ba-ab69-91faba077c7e', 0, 0.00, 3, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('1c378f3b-3ee7-4fdf-a438-436a1f7b4138', N'107', N'Du thuyền', N'1-7', 'c36825a9-3e51-4586-bf7f-8e532b4ade91', 0, 30.00, 2, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('825308c2-f727-4651-97c8-47aca01c152e', N'10412', N'Loại có dung tích xi lanh trên 1.500 cc tới 2.000 cc', N'1-4-1-2', 'ea63f748-b692-45e6-8e68-a079aee29cf0', 0, 40.00, 4, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('3ba757dd-6b89-4e0e-994e-58f7a7298540', N'1042', N'Xe ô tô chờ người từ 10 đến dưới 16 chỗ ngồi', N'1-4-2', '757048cb-32d1-41ba-ab69-91faba077c7e', 0, 15.00, 3, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('065ec1a9-0d8a-4ac4-8a9e-5a06d4032e1b', N'202', N'Kinh doanh mát-xa, ka-ra-o-kê', N'2-2', '98cff596-b53d-40c8-9f22-84fa60c8b1ea', 0, 30.00, 2, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('dce48d99-a9a8-4638-8ce3-5bcdfbab2e83', N'10473', N'Loại chờ người từ 16 đến dưới 24 chỗ', N'1-4-7-3', 'b5c283d9-a085-4740-bed0-cbebe12e088e', 0, 5.00, 4, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('6812d063-4cc3-43c9-bbd3-5c43e66ec9d7', N'108', N'Xăng các loại', N'1-8', 'c36825a9-3e51-4586-bf7f-8e532b4ade91', 1, 0.00, 2, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('8a9ad41c-af2d-4a12-9d41-5fe2bef13f83', N'205', N'Kinh doanh gôn', N'2-5', '98cff596-b53d-40c8-9f22-84fa60c8b1ea', 0, 20.00, 2, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('67be9a0a-2340-4154-95e3-62e10ee74de1', N'10442', N'Loại có dung tích xi lanh trên 2.500cc tới 3.000cc', N'1-4-4-2', 'cc8d6740-12eb-4ef4-98e7-9ea05dc3c1f7', 0, 20.00, 4, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('731cc25b-d1e8-48d7-8748-7f512d8f3ac6', N'10443', N'Loại có dung tích xi lanh trên 3.000cc', N'1-4-4-3', 'cc8d6740-12eb-4ef4-98e7-9ea05dc3c1f7', 0, 25.00, 4, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('98cff596-b53d-40c8-9f22-84fa60c8b1ea', N'2', N'Dịch vụ chịu thuế TTĐB', N'2', NULL, 1, 0.00, 1, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('c36825a9-3e51-4586-bf7f-8e532b4ade91', N'1', N'Hàng hóa chịu thuế TTĐB', N'1', NULL, 1, 0.00, 1, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('757048cb-32d1-41ba-ab69-91faba077c7e', N'104', N'Xe ô tô dưới 24 chỗ', N'1-4', 'c36825a9-3e51-4586-bf7f-8e532b4ade91', 1, 0.00, 2, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('fdd84bd7-dea8-4cc8-8ece-9d222cd1c85b', N'10471', N'Loại chở người từ 9 chỗ trở xuống', N'1-4-7-1', 'b5c283d9-a085-4740-bed0-cbebe12e088e', 0, 15.00, 4, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('851e677b-7efc-4d96-af38-9db163cf4c8a', N'10414', N'Loại có dung tích xi lanh trên 2.500cc tới 3.000cc', N'1-4-1-4', 'ea63f748-b692-45e6-8e68-a079aee29cf0', 0, 60.00, 4, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('4b0c794d-9355-442f-9d77-9e7331b05124', N'106', N'Tàu bay', N'1-6', 'c36825a9-3e51-4586-bf7f-8e532b4ade91', 0, 30.00, 2, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('cc8d6740-12eb-4ef4-98e7-9ea05dc3c1f7', N'1044', N'Xe ô tô vừa chở người, vừa chở hàng', N'1-4-4', '757048cb-32d1-41ba-ab69-91faba077c7e', 1, 0.00, 3, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('ea63f748-b692-45e6-8e68-a079aee29cf0', N'1041', N'Xe ô tô chở người từ 9 chỗ trở xuống', N'1-4-1', '757048cb-32d1-41ba-ab69-91faba077c7e', 1, 0.00, 3, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('0ce52aab-adf8-4522-892e-a679b08786d1', N'111', N'Vàng mã, hàng mã', N'1-11', 'c36825a9-3e51-4586-bf7f-8e532b4ade91', 0, 70.00, 2, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('eae38dc6-1ef1-4982-8459-ad5554484872', N'10415', N'Loại có dung tích xi lanh trên 3.000cc tới 4.000cc', N'1-4-1-5', 'ea63f748-b692-45e6-8e68-a079aee29cf0', 0, 90.00, 4, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('b22109e9-376e-481c-b1c4-ae8a5418c027', N'1021', N'Rượu từ 20 độ trở lên', N'1-2-1', 'ea1fa202-ab64-46f5-954e-2ad37620713d', 0, 65.00, 3, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('2b247f67-76dc-4ac6-bb3e-b835b6beaa3d', N'10413', N'Loại có dung tích xi lanh trên 2.000 cc tới 2.500 cc', N'1-4-1-3', 'ea63f748-b692-45e6-8e68-a079aee29cf0', 0, 50.00, 4, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('eee2e9ae-46fb-408d-9a1d-bb755e5849d4', N'203', N'Kinh doanh ca-si-no, trò chơi điện tử có thưởng', N'2-3', '98cff596-b53d-40c8-9f22-84fa60c8b1ea', 0, 35.00, 2, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('09e284f9-e7e2-41c9-9c84-bd6657af78c6', N'206', N'Kinh doanh xổ số', N'2-6', '98cff596-b53d-40c8-9f22-84fa60c8b1ea', 0, 15.00, 2, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('f0dd22e0-3e97-4720-bf25-bfa5b91bea0c', N'103', N'Bia', N'1-3', 'c36825a9-3e51-4586-bf7f-8e532b4ade91', 0, 65.00, 2, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('017dda4b-251f-4086-a752-c113426490be', N'105', N'Xe mô tô hai bánh, xe mô tô ba bánh có dung tích xi lanh trên 125cc', N'1-5', 'c36825a9-3e51-4586-bf7f-8e532b4ade91', 0, 20.00, 2, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('3eb99e78-62d5-4662-a13f-c2f844df3b42', N'1022', N'Rượu dưới 20 độ', N'1-2-2', 'ea1fa202-ab64-46f5-954e-2ad37620713d', 0, 35.00, 3, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('10f45647-804e-42fa-a49b-c405763ce78f', N'1048', N'Xe motorhome không phân biệt dung tích xi lanh', N'1-4-8', '757048cb-32d1-41ba-ab69-91faba077c7e', 0, 75.00, 3, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('8432ab8f-3838-49e5-a1a4-c4fa20f54979', N'201', N'Kinh doanh vũ trường', N'2-1', '98cff596-b53d-40c8-9f22-84fa60c8b1ea', 0, 40.00, 2, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('b5c283d9-a085-4740-bed0-cbebe12e088e', N'1047', N'Xe ô tô chạy bằng điện', N'1-4-7', '757048cb-32d1-41ba-ab69-91faba077c7e', 1, 0.00, 3, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('3a044b16-eaad-4fd2-8879-d2aba54af279', N'110', N'Bài lá', N'1-10', 'c36825a9-3e51-4586-bf7f-8e532b4ade91', 0, 40.00, 2, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('b1b22985-f1c4-4128-a3c9-d2b6a1ab9094', N'1081', N'Xăng', N'1-8-1', '6812d063-4cc3-43c9-bbd3-5c43e66ec9d7', 0, 10.00, 3, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('9fa4e286-1b1e-408c-89da-d5a4213c96ee', N'10474', N'Loại thiết kế vừa chở người, vừa chở hàng', N'1-4-7-4', 'b5c283d9-a085-4740-bed0-cbebe12e088e', 0, 10.00, 4, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('e8d0caa2-0ac0-47dc-96fd-e7aecce54264', N'1043', N'Xe ô tô chở người từ 16 đến dưới 24 chỗ', N'1-4-3', '757048cb-32d1-41ba-ab69-91faba077c7e', 0, 10.00, 3, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('b90a7a82-dc39-4a32-9af9-f6e5b7f1ac3e', N'1083', N'Xăng E10', N'1-8-3', '6812d063-4cc3-43c9-bbd3-5c43e66ec9d7', 0, 7.00, 3, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('8fcc4cb3-b84f-4ee6-8644-fed585ad1f36', N'10411', N'Loại có dung tích xi lanh từ 1.500 cc trở xuống', N'1-4-1-1', 'ea63f748-b692-45e6-8e68-a079aee29cf0', 0, 35.00, 4, N'', 1, 0)
INSERT dbo.MaterialGoodsSpecialTaxGroup(ID, MaterialGoodsSpecialTaxGroupCode, MaterialGoodsSpecialTaxGroupName, OrderFixCode, ParentID, IsParentNode, TaxRate, Grade, Unit, IsActive, IsSecurity) VALUES ('8db1776f-e188-4e47-acea-ffb6725259a9', N'10418', N'Loại có dung tích xi lanh trên 6.000cc', N'1-4-1-8', 'ea63f748-b692-45e6-8e68-a079aee29cf0', 0, 150.00, 4, N'', 1, 0)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.PersonalSalaryTax(ID, PersonalSalaryTaxName, PersonalSalaryTaxGrade, SalaryType, TaxRate, FromAmount, ToAmount, Formula) VALUES ('2c69576f-946c-438d-a671-0471c5596844', N'Thu nhập từ tiền lương, tiền công', 6, 0, 30.0000000000, 52000001.00, 80000000.00, NULL)
INSERT dbo.PersonalSalaryTax(ID, PersonalSalaryTaxName, PersonalSalaryTaxGrade, SalaryType, TaxRate, FromAmount, ToAmount, Formula) VALUES ('3f12764c-c4b7-4a23-ba29-14d88d18c294', N'Thu nhập từ tiền lương, tiền công', 4, 0, 20.0000000000, 18000001.00, 32000000.00, NULL)
INSERT dbo.PersonalSalaryTax(ID, PersonalSalaryTaxName, PersonalSalaryTaxGrade, SalaryType, TaxRate, FromAmount, ToAmount, Formula) VALUES ('ece149ef-c962-446a-882c-18df3c4a7765', N'Thu nhập từ tiền lương, tiền công', 5, 0, 25.0000000000, 32000001.00, 52000000.00, NULL)
INSERT dbo.PersonalSalaryTax(ID, PersonalSalaryTaxName, PersonalSalaryTaxGrade, SalaryType, TaxRate, FromAmount, ToAmount, Formula) VALUES ('925f2688-4ce6-42cd-9085-2645b4532608', N'Thu nhập từ tiền lương, tiền công', 2, 0, 10.0000000000, 5000001.00, 10000000.00, NULL)
INSERT dbo.PersonalSalaryTax(ID, PersonalSalaryTaxName, PersonalSalaryTaxGrade, SalaryType, TaxRate, FromAmount, ToAmount, Formula) VALUES ('9be78b0f-7d88-475a-a2e4-28113e4dd23f', N'Thu nhập từ tiền lương, tiền công', 3, 0, 15.0000000000, 10000001.00, 18000000.00, NULL)
INSERT dbo.PersonalSalaryTax(ID, PersonalSalaryTaxName, PersonalSalaryTaxGrade, SalaryType, TaxRate, FromAmount, ToAmount, Formula) VALUES ('7ace19eb-49d5-4fc7-b1a0-2b6088e458f7', N'Thu nhập từ tiền lương, tiền công', 1, 0, 5.0000000000, 0.00, 5000000.00, NULL)
INSERT dbo.PersonalSalaryTax(ID, PersonalSalaryTaxName, PersonalSalaryTaxGrade, SalaryType, TaxRate, FromAmount, ToAmount, Formula) VALUES ('1a7e4230-18ab-423e-95e1-4f7ef5aa60b7', N'Thu nhập từ tiền lương, tiền công', 7, 0, 35.0000000000, 800000001.00, 99999999999999.00, NULL)
INSERT dbo.PersonalSalaryTax(ID, PersonalSalaryTaxName, PersonalSalaryTaxGrade, SalaryType, TaxRate, FromAmount, ToAmount, Formula) VALUES ('05dc805e-eca5-4cbd-b373-6fba72bf89c8', N'Thu nhập từ hợp tác đầu tư vốn', 1, 1, 5.0000000000, 0.00, 99999999999999.00, NULL)
INSERT dbo.PersonalSalaryTax(ID, PersonalSalaryTaxName, PersonalSalaryTaxGrade, SalaryType, TaxRate, FromAmount, ToAmount, Formula) VALUES ('249f0a3f-6112-4790-8d8d-72b869771ea1', N'Thu nhập của cá nhân cư trú không có HĐLD hoặc có HĐLĐ dưới 3 tháng', 2, 1, 10.0000000000, 0.00, 99999999999999.00, NULL)
INSERT dbo.PersonalSalaryTax(ID, PersonalSalaryTaxName, PersonalSalaryTaxGrade, SalaryType, TaxRate, FromAmount, ToAmount, Formula) VALUES ('7c9df1c0-db66-42b6-b275-97dfb713ec5e', N'Thu nhập bất thường', 2, 1, 10.0000000000, 0.00, 99999999999999.00, NULL)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.SupplierService(ID, SupplierServiceCode, SupplierServiceName, PathAccess) VALUES ('61498d78-5ac2-403c-b645-2e98896eb4b9', N'VTE', N'Tập đoàn Công nghiệp - Viễn thông Quân đội Viettel', N'https://demo-sinvoice.viettel.vn')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.SystemOption(ID, Code, Name, Type, Data, DefaultData, Note, IsSecurity) VALUES (125, N'PB_HangKy', N'Phân bổ đều hàng kỳ', 6, N'1', N'1', N'0: Not check
1: Check', 0)
INSERT dbo.SystemOption(ID, Code, Name, Type, Data, DefaultData, Note, IsSecurity) VALUES (126, N'PB_HangKyCCDC', N'Phân bổ đều hàng kỳ', 6, N'1', N'1', N'0: Not check
1: Check', 0)
INSERT dbo.SystemOption(ID, Code, Name, Type, Data, DefaultData, Note, IsSecurity) VALUES (127, N'PB_TheoNgay', N'Phân bổ theo ngày ghi nhận chi phí', 6, N'0', N'0', N'0: Not check
1: Check', 0)
INSERT dbo.SystemOption(ID, Code, Name, Type, Data, DefaultData, Note, IsSecurity) VALUES (128, N'PB_TheoNgayCCDC', N'Phân bổ theo ngày ghi tăng', 6, N'0', N'0', N'0: Not check
1: Check', 0)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

SET IDENTITY_INSERT dbo.Template ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.Template(ID, TemplateName, TypeID, IsDefault, IsActive, IsSecurity, OrderPriority) VALUES ('b5b01477-1001-40de-980e-616d5a6a2d70', N'Mẫu chuẩn', 690, 0, 1, 1, 1)
GO
SET IDENTITY_INSERT dbo.Template OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.TemplateColumn WHERE ID = '8ae3ef8b-e12e-4221-a599-000efe7e1213'
DELETE dbo.TemplateColumn WHERE ID = 'b337c1fd-0ab5-4a5d-b81c-049c86c170ea'
DELETE dbo.TemplateColumn WHERE ID = '4fe1df2c-e72d-4c12-9e7c-0a405617248b'
DELETE dbo.TemplateColumn WHERE ID = '6d40dcef-4dae-4737-80e8-0a45d264ad24'
DELETE dbo.TemplateColumn WHERE ID = '7526cd33-a517-4d65-9b68-0a72731726c8'
DELETE dbo.TemplateColumn WHERE ID = '9e304084-5dec-4651-8533-0bdc7f685316'
DELETE dbo.TemplateColumn WHERE ID = '7f4d04eb-5a1f-4d47-994b-0d6666ce4562'
DELETE dbo.TemplateColumn WHERE ID = '92c6d057-2ebd-437a-8f45-0e343bd1bb74'
DELETE dbo.TemplateColumn WHERE ID = '9adac4c6-32cc-4642-a5c4-0f92be5e7cad'
DELETE dbo.TemplateColumn WHERE ID = '2ca5fb8b-6c46-4a62-9ea9-104b2e8e0446'
DELETE dbo.TemplateColumn WHERE ID = '9469ed1f-2513-4a09-af7f-1129a8ed0beb'
DELETE dbo.TemplateColumn WHERE ID = 'b2f5da0c-76f7-49b4-83ab-13bd4f23447d'
DELETE dbo.TemplateColumn WHERE ID = 'a6c904f1-170d-45be-90b0-234f50927b1a'
DELETE dbo.TemplateColumn WHERE ID = '86134846-0417-410c-a682-27475e5e893c'
DELETE dbo.TemplateColumn WHERE ID = '0f167198-c28f-4bf7-a02b-30c2910e59fa'
DELETE dbo.TemplateColumn WHERE ID = '9237327a-3252-4b06-baa1-3159edd8272b'
DELETE dbo.TemplateColumn WHERE ID = '20a1fd92-0278-45a1-8a62-34384f1bc8af'
DELETE dbo.TemplateColumn WHERE ID = 'c8e379f1-ae67-42bd-9c4e-350ffb9693d6'
DELETE dbo.TemplateColumn WHERE ID = 'e93a9d87-fe0f-4b93-ab1a-390af949f364'
DELETE dbo.TemplateColumn WHERE ID = 'f38a38ca-854a-4eb9-82ee-392919ff5b44'
DELETE dbo.TemplateColumn WHERE ID = 'c5563213-b665-4655-83ad-3ccd94897939'
DELETE dbo.TemplateColumn WHERE ID = '27eb1bb7-c264-4242-89d1-43683b7d327f'
DELETE dbo.TemplateColumn WHERE ID = 'a7effc1a-4930-4fac-9bc4-44c93778a74b'
DELETE dbo.TemplateColumn WHERE ID = 'a829288b-0b95-454c-81da-4effdebf5531'
DELETE dbo.TemplateColumn WHERE ID = '5d445a47-f835-4cbc-aeee-52d1febdef71'
DELETE dbo.TemplateColumn WHERE ID = '33e766a5-dd57-447e-a80b-55c6fe8df695'
DELETE dbo.TemplateColumn WHERE ID = '7f9d9b53-e50c-4981-9a42-58289ce96019'
DELETE dbo.TemplateColumn WHERE ID = 'b11b33b9-06c6-4820-9ecb-69eb51eff0d4'
DELETE dbo.TemplateColumn WHERE ID = '1dc5d3af-a652-4c09-8f74-6acce2b5abcd'
DELETE dbo.TemplateColumn WHERE ID = 'b88f3e77-0921-48ba-890f-7201871e33ef'
DELETE dbo.TemplateColumn WHERE ID = '4d8bfbcc-bfd2-4989-935a-730fe4d600f3'
DELETE dbo.TemplateColumn WHERE ID = '5e0928ff-4d48-49db-af35-79308b605bbe'
DELETE dbo.TemplateColumn WHERE ID = '95edac74-cb31-4fb5-b23b-83f11569e5e0'
DELETE dbo.TemplateColumn WHERE ID = '15361286-ad58-4571-8cba-85af9c7e0b06'
DELETE dbo.TemplateColumn WHERE ID = '3a54802d-38e0-429e-8730-8d1049e90b43'
DELETE dbo.TemplateColumn WHERE ID = 'f8cbffdc-08b1-4862-9614-95ff9cb86f5c'
DELETE dbo.TemplateColumn WHERE ID = 'dc27fc7a-f8bd-48ca-88c0-9ada3bda62f6'
DELETE dbo.TemplateColumn WHERE ID = 'dc985ff8-ab26-46cd-8cc5-9b88606e3bf9'
DELETE dbo.TemplateColumn WHERE ID = 'ebdbff6d-c921-4b7e-842a-a1357522af93'
DELETE dbo.TemplateColumn WHERE ID = '14f97c7a-5770-4e83-bce1-a19b303caca1'
DELETE dbo.TemplateColumn WHERE ID = 'c2bf8e89-ca73-4842-9d4f-a50c5d4160d5'
DELETE dbo.TemplateColumn WHERE ID = 'def1eb05-b94a-4d3e-ad0c-a88477ab5004'
DELETE dbo.TemplateColumn WHERE ID = '0103e1d3-222d-46b5-81fd-b5a687011962'
DELETE dbo.TemplateColumn WHERE ID = 'a33639f6-f7ac-4781-90e9-bd44750b3404'
DELETE dbo.TemplateColumn WHERE ID = '863f2fc2-5dda-4198-acc7-bd7284b4267f'
DELETE dbo.TemplateColumn WHERE ID = '6db5050a-d741-4ead-9a7d-c0655cd54f59'
DELETE dbo.TemplateColumn WHERE ID = 'b6c6d168-3f2e-4421-ae89-c6611f177b2d'
DELETE dbo.TemplateColumn WHERE ID = 'e0a49f5c-5dc2-4288-b3b7-d0d9f45f589a'
DELETE dbo.TemplateColumn WHERE ID = 'e44f8e85-dc08-4522-94b0-d74212428161'
DELETE dbo.TemplateColumn WHERE ID = '4af07a86-b1c9-40f2-9105-dbe698f1f73c'
DELETE dbo.TemplateColumn WHERE ID = '57d33ec1-9207-40e5-b73a-de2f65382c53'
DELETE dbo.TemplateColumn WHERE ID = '67e34c1a-aa26-4bda-a9b7-ebfe4175d93d'
DELETE dbo.TemplateColumn WHERE ID = 'edc69ad3-5c9b-4f25-9b61-ec1af519c041'
DELETE dbo.TemplateColumn WHERE ID = 'a3cedc63-a2a8-4322-a36d-ecab4568f8e3'
DELETE dbo.TemplateColumn WHERE ID = '81ba9f6f-434b-405b-9be7-ef04ae3b2b74'
DELETE dbo.TemplateColumn WHERE ID = '8d523549-dc3e-4e56-845f-ef0678d186c0'
DELETE dbo.TemplateColumn WHERE ID = '624cb7d2-e466-4f4a-8ccb-f7f4c990a1a2'
DELETE dbo.TemplateColumn WHERE ID = '9a3c3afd-7e32-459f-99a3-fa9367d372ce'

UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'ae78be82-018d-45b9-9264-002fa535bb2b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'c79af571-a158-4e08-a309-00af0ddbb85d'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '78cd24b5-72e5-4720-91d0-00bf5a37a591'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '8fbe4b7d-547d-4fb3-ad4b-00fcdf1f3dd1'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'b1755ef6-435a-44aa-a464-01dba7bababa'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '8eddcfda-9926-4b34-89ab-020d774dfec1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'eb18f889-7544-42ea-a45a-025ea5232167'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'bb639c8f-2400-4250-bc7f-02825d67fb0b'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate' WHERE ID = '06dd3202-863d-4135-9446-0292a04c2d54'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '2606e4a1-ee9c-4ee5-9ba6-02b227e61423'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '20989740-5031-4eec-bfd4-02fda885251c'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'cfc3da5e-c042-459e-8d30-03004863e9fd'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'c358e60a-24b9-4b00-87ec-0306c3daaf6c'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '79ceeeda-06ef-4e1c-ac6a-031da7e472f3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 70 WHERE ID = '6e428c4a-c0d1-48ce-a685-03274424c42b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '57e3c931-998c-4ec1-86ea-0369d69bef75'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '8e254896-4131-4c04-821b-046eb79482a7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '313f37d3-dee5-42fb-9ca1-049a64db3a6b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'bd2c87de-01c4-4f65-b601-04c0ac1dff9a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 60 WHERE ID = '50d28322-9524-44fa-8c25-04c387097f18'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'de04adcb-ab08-48d3-8125-053dd721a3ed'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'fa434d27-80f6-4ff5-a88f-059d1b447fc1'
UPDATE dbo.TemplateColumn SET IsVisible = 1, VisiblePosition = 35 WHERE ID = '726ff4ea-f465-48ec-86ed-05ddf9b82c07'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '626fa5be-3b68-4925-97a8-060bba6c356e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '75d64748-ba9f-4d60-a879-063b346da59b'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '2f390996-b943-4319-b948-06f9857593b7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '2c5af507-d548-4f55-ba43-06fd8e740be8'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '6d314884-3ef9-4f51-8234-0703ec25d28c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '22feb002-6939-469e-a2e8-070f9a823b93'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '259b1153-585f-4d1b-8842-071dfb8bc50a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'a3737fc8-b3e3-491e-8797-073481bfb5be'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = 'c14a7b88-e71c-4e4b-91b2-078a7379ac7c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Phí trước hải quan', ColumnToolTip = N'Phí trước hải quan' WHERE ID = 'b6a1af70-72cc-43af-97c8-078cbc2f5911'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '8d08e8fd-f9b8-48f9-9295-07b2032141ad'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'ad03a45f-4390-4ee4-b73a-08562409ac0e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '6267e047-e83e-47b9-b9fb-08571e521ddf'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'fb12cf28-6a84-4763-937b-0898c6fece5d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 27 WHERE ID = '1d1bc4b2-dd7f-4c27-b86d-093fda795f73'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0, VisiblePosition = 23 WHERE ID = '1893972d-b30b-440a-8512-09f3977d5d73'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '73e491e7-7161-418b-8e91-0a086a2ea083'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '76ddf666-6bee-47b8-94e3-0a1b8cbddb22'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '8255ce5c-b261-4671-b6aa-0a21a3926093'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '963ca6ec-47a8-41ed-b4f9-0a2605f83f07'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '9309a54f-00f5-4721-a546-0a43bf06b734'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '1f2a1ba2-b55b-48c8-8858-0a57e29f8aa1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '165f2521-cb53-4d0a-bbf4-0a58285c9731'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'babfed26-6140-4694-bc1f-0b450297f169'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '13f5cdbc-ae55-4914-8395-0c2af774c1b6'
UPDATE dbo.TemplateColumn SET VisiblePosition = 25 WHERE ID = 'c2b6870c-92b2-4745-8e99-0c53396030af'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '277e4757-f632-4852-8682-0c5ec642838b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'afe47b11-a85b-48db-a01e-0d02063b531f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'd759bc12-7641-4ef2-90d7-0d5119f9b9d2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '592ba499-68fe-4eb8-ab8d-0d87310fe7d0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 56 WHERE ID = '9eeddbc3-13bb-465a-98d3-0da04d6264bd'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '5445aafc-979d-4d6b-a157-0dc760a023b2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '98d6b072-2e17-4c8d-9292-0e4e41a7d261'
UPDATE dbo.TemplateColumn SET VisiblePosition = 33 WHERE ID = 'e1d091ef-e31a-440d-b294-0e511f593e3f'
UPDATE dbo.TemplateColumn SET ColumnWidth = 100 WHERE ID = 'f184d5bf-b5a3-44bb-a118-0efa04e6b903'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Phí trước hải quan', ColumnToolTip = N'Phí trước hải quan' WHERE ID = '58216e62-1a3f-4fbb-b517-0f1c2b0ee322'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Phí trước hải quan', ColumnToolTip = N'Phí trước hải quan' WHERE ID = '59a891c0-5e5a-4e9f-ac7d-0fb199cb0aba'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '2160045f-3468-4a82-afb8-1047fb77f38c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '2de3aef0-4d2d-41e0-a3ca-10b2fb53661b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '96212f0c-39a7-4bd5-97d6-10d0f3c93f82'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'd9e64605-7382-4ccc-b43f-10e46c0d910b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '3cf0fcfe-8279-4384-a1d0-110b9bbe0bae'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'e126ffb1-569f-436c-8e45-11a2d9b63900'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '698b218e-f69b-4e86-80b0-11e150a310c3'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 0 WHERE ID = 'b42bdab2-22fd-45e0-8cce-11f6e6d401ce'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'ddf029b8-ba6c-417c-a05f-12219a5b64de'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '35ec9370-e469-4eb5-843a-123d0c8d078e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '4816189d-8a46-400b-96e1-124f4b9edbf3'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '08e7bcd8-0e77-4cad-af1e-12e04c74d046'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '45d2ba4f-732d-4a84-8e0c-1309b6dcba9a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '06bf3cb1-be31-4153-b82e-13dd76f9c4d6'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 0 WHERE ID = '518d6fc1-1bab-4fe8-9769-13ea19e35b81'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '211736d7-702b-4879-9ba2-14101fdba57e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '0dc4ebcd-7f53-4b0f-9a45-141229a2612e'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '6e14eb8d-36ff-4be2-a061-149ee6bacd77'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '5584334d-07bc-46cc-ac21-14fada965b05'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'cc42e6e4-cad2-4ded-983e-155dcfd008b5'
UPDATE dbo.TemplateColumn SET VisiblePosition = 45 WHERE ID = '43239407-5836-4c03-a195-157443030061'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '1f5ebd3e-aed2-4968-b4c6-15b9c93b4589'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'e28179cc-3e33-40e8-9a3f-1680c0088306'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'baa5175e-80bc-459b-b44c-168617c55d1c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'b59dbd1d-f83e-4bd9-a4a3-16f2c0217a9e'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 0 WHERE ID = '2aeceb85-308c-4f4d-a673-17522398e4c8'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'a5a44754-3baf-49a4-8f06-179f70a92e74'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'aac71e75-7b2c-4e3b-b920-17af8088a1b9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Chi phí mua', IsVisible = 1 WHERE ID = '383b5103-7b5c-4140-970a-180cd2b6449e'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = 'c574dbc5-edd8-4ff0-a8b1-183ccba55ffe'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '300ca74e-0e3c-4fbd-8e4a-18776b0c536b'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '42b10f10-2c9f-4e5b-a031-1a52720b0ed5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '996ef596-91a2-4812-acbf-1af510c2008f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'b485e09c-ac31-4b8d-8953-1b2dc80b76c6'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '02ec537d-e7b7-4400-aa61-1b8c2f4dadf2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '6345d8b6-8a35-48b4-a830-1ba09584d2b9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '7dab254a-625c-4df1-a755-1be3d4c9db55'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'ad12604e-e35e-4acd-bd63-1cee4256221e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '48fdbe62-73ad-413d-978d-1d91d8f3191d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '0ace9599-a88d-440e-b811-1dcb78521df7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'e9096718-2cf5-4aae-bad6-1e46ad1d0fae'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '404841ee-4287-412d-9258-1e68f23a780b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '8094879a-a7b9-4f7b-96e8-1e7a2d7482e9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '92beb9d0-6c39-47ae-840c-1ef236543cf5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'b9d05ed5-c440-46c3-a0bc-1ef5f1294cfb'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '779f36d2-eb32-468c-81a3-1f0eb2d9ce34'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 0 WHERE ID = '12d114d5-a71b-4ffd-831c-1f62206abf3f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Giá trị nhập kho ', ColumnToolTip = N'Giá trị nhập kho', IsVisible = 1 WHERE ID = '6e10306a-baf4-4315-b465-1f71faa770bb'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '778773d6-dc6a-4655-99b1-1feaa99ae332'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'b09811d9-2b0f-4b6e-997a-208d04ede5af'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'd40e0335-7ac2-42ab-a2d9-209b9cea5793'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '31fed95e-c3fc-4ec6-9996-20fbe77f689a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '2a3384c0-eb79-43f4-8d63-21035e09628c'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '85fa76f7-3e2a-4ade-9013-2183c27c36e7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'c020ca50-0841-46ba-82ef-21942fe0093a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '44dcc5c6-a8da-4366-a60b-21dcfb906f80'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '23261a0a-fdff-4c7c-8a52-22180a871217'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'dc057be5-3764-40ab-9383-22211647dab9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 5 WHERE ID = 'e8233d9d-489a-4c83-8b89-22364ec957ae'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '5553cc39-98ff-41ae-ad6d-2293a22e1cc7'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'd729f19f-a9d0-4895-9da9-22afa52ab912'
UPDATE dbo.TemplateColumn SET VisiblePosition = 59 WHERE ID = '697ebc1a-9dea-461a-b9a5-233274c20a75'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '47a6bc58-704a-423c-9790-236cb357c0cf'
UPDATE dbo.TemplateColumn SET VisiblePosition = 67 WHERE ID = '19e28965-8e23-412c-96f7-23c992dbecb3'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '81acea1d-c9e6-46ae-8509-23e941082a9e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'dea62161-2439-4de8-9645-2445de1f8b54'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'cc1742af-9d98-4915-a046-256cc01d5f3c'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'f0de49b5-f9ee-47d9-a68a-259db80f6783'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '515faea5-b98d-40e6-b2f5-25d006a3d479'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '0a215c97-da91-4c38-b6cd-25dd6e7c74f2'
UPDATE dbo.TemplateColumn SET VisiblePosition = 55 WHERE ID = 'd9adf138-4de2-4f0f-aa78-265a52661f67'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '0f0db542-7b5f-46f9-9c02-2694f3c92439'
UPDATE dbo.TemplateColumn SET VisiblePosition = 6 WHERE ID = '8e3375cf-f4f1-465c-a528-281d13b2d9ff'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '638f18d4-5d20-4614-9d7e-284f0db996f1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'a1feb412-a9f4-46b2-97c8-285f98818485'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '49760a85-ab48-41a4-b45f-28a10c4c2f96'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '4a332e55-7430-418f-9bbf-2909e5124c97'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 0 WHERE ID = '67d76ca2-8881-476e-9dd2-29350ff331cc'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '1802ff3c-7df5-4ccb-9a50-2936fe518ad4'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'b80ea7e9-3926-4d7f-9486-29427a63ca38'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '9c7f1c48-b4b9-4c0f-82d2-2963d220b4d4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = 'c006342f-136d-4d66-b26e-297f0566e8d4'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsVisible = 1 WHERE ID = '1233a43b-d483-4271-8d5a-299f6d1adcce'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'd09a4a98-7602-4aec-9bf9-299fed4518fe'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '6a8a20dd-5f4d-4ec7-9aae-29a5a2a96063'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '938b0c0b-895f-477d-b885-29ba7faab38f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'e77581d2-0574-4c60-a797-2a06af4f0d15'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate' WHERE ID = 'd4ec31d9-e15e-4863-9f8a-2a3b6f4b046f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '3966c64e-71e7-4d0e-86f1-2adf4c617d3a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '0dfd79cb-e3cb-4c64-b596-2b0c63d63a0d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'c1f3d44a-b6bd-4d45-947e-2b4bf50a78ae'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsVisible = 1 WHERE ID = '9fcd7b67-7e0c-4dbd-a83d-2b694f7da829'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'f49179a1-716e-4858-9f9e-2b6fc4cf285f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '5b05fc44-6b2e-4445-97c5-2bb1c20384a6'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '52ab7bf3-1d9d-45ea-8692-2bf1eb0f223c'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 0 WHERE ID = 'e2efa898-9b89-45c2-b66e-2c97a82e8b76'
UPDATE dbo.TemplateColumn SET VisiblePosition = 38 WHERE ID = '687d101f-f497-4102-8eb5-2cd5ab2d7310'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'a33fedc3-cee6-41a3-b66f-2d5bf4bf4824'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '37cdd762-9724-4e79-8642-2dc7aee9e181'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '96bb657e-2400-4c66-9da8-2de31baef7b0'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '51ce1fac-f8b3-403c-9c20-2e25133c9841'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'b1911c6f-b177-4b4a-a2a7-2ebdf29a656e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'd8ce59cd-516c-4079-a86a-2ed73bed481f'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'bc9212dc-da00-4782-bc8e-2f18218f581f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'ce70c486-36fa-46d3-aec3-2f85bdeeb425'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'd1ba651c-607c-4aa9-a9af-2ff5dc4a0a24'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '6678b778-9bb2-4452-b946-3009f293fde8'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'c2030dc7-5d08-4eca-a7a5-30244bfba37d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '070b88cb-105f-495a-bc14-31974d6f8faa'
UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = '92034183-1700-48b6-ac89-3236cb854db5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '055a1b6e-c018-4d9e-b1ab-32a0160950f4'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'a57d8ed4-4fd4-4d0c-9858-32c67ce5f999'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '1af02228-24fc-4adc-b495-32f6954228d1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'fff368b5-b6b8-4c8d-bc0d-33289f6ad239'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '736f85f5-f309-4045-83f7-3342fe0e2790'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'db3fc719-ba0c-4b8e-912b-335537928577'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '6091b737-14a2-4f83-8abb-3380c4231418'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = 'f845ba78-987f-45bf-b51e-33f0013df423'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '87efdb12-6ace-46c9-8506-344cfc1129e6'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '67412ccd-dc4d-4af7-b4fd-34933f1441ac'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = '557ba5c2-728e-41ae-b3fc-34b8827cc74b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 63 WHERE ID = '937ea0b0-87e0-4bed-b031-350dd22f3a44'
UPDATE dbo.TemplateColumn SET VisiblePosition = 22 WHERE ID = 'd06b107a-812b-4c70-9c97-352361e1660d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 69 WHERE ID = '785dd4ad-1eaa-4753-abef-354f8c923997'
UPDATE dbo.TemplateColumn SET VisiblePosition = 25 WHERE ID = '238c32c2-908f-4d5f-a6ca-35f59778dd68'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'ee394e73-045d-4309-9d0a-3623635bdd39'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '800f10bf-d5e5-4f00-ae91-3658cb8db26d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'd9097f9f-bd58-4f06-b578-36c01b9ea203'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'd4f29763-00b6-44a2-a78e-36fb6afd90c9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Phí trước hải quan', ColumnToolTip = N'Phí trước hải quan' WHERE ID = '5f888594-c495-4db6-95cf-378c79b8f36d'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '9e8a4d56-6271-4f70-b583-378f081dc65c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Phí trước hải quan', ColumnToolTip = N'Phí trước hải quan' WHERE ID = '5ca5d0d7-4440-4c04-8453-384fcbf233f8'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '730e7d24-1e4e-4210-a7b9-386a2af29470'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'c7ddbfe1-112f-4d73-837e-387b17e426e8'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'dc83d6e6-4760-41d5-bdd9-38f46b6e5eed'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '00c9073e-6abc-4afa-a93b-390ed7aac3c4'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'e02f84f3-0566-46c4-ac35-39407ed79c7e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'd88ce338-06cd-4837-9264-39ac6e111517'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '4a8d103a-7006-4f53-a9bc-39ba29a79a95'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '5c0bc484-c72f-4f8e-be81-39c101c0e7dd'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'd35e1305-7307-4848-91b0-3a0b29add8bd'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '855cb914-4657-4264-b4fc-3a195eb2d4fd'
UPDATE dbo.TemplateColumn SET VisiblePosition = 32 WHERE ID = '7a3bc0be-f511-489d-a46e-3a34456c975f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '0ae5e9ff-357e-4f5a-a9f3-3a6d056a4476'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '2b76394b-eb67-45e4-a705-3aebf703ad80'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'e18b45c3-50a9-4dd8-9246-3b430d712cea'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '1f2cceca-0799-4899-8fde-3b62b67aca8d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '8cb9b5f4-e3cc-4972-9ad7-3b879c2ba35a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '55d4e474-a54a-4f27-b176-3b9d132caed5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '708794ac-c6f7-43d6-bab5-3befbc821813'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'acdcc4ba-7507-4d51-bafa-3c01b55b15a3'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '5f51b3eb-b69f-4c74-af00-3c3e396e249f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'd6c79bbf-1964-4710-ac4f-3c4fb2d93856'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '1cb00173-5c25-43a8-94c3-3dd62acd2520'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '780837f7-31bb-4c37-bc84-3e2a9cf20f2c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '97e8c9f8-4230-4050-8f85-3e2bfbed6b61'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate' WHERE ID = '08c8b25a-a204-4eb2-8bf3-3e74ed893834'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '19be11b7-8f87-4b50-86a4-3e7c70412efd'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = '19513349-1bbf-424f-ac94-3eab2a558d11'
UPDATE dbo.TemplateColumn SET ColumnName = N'VATDescription' WHERE ID = 'cd3f641e-7b86-4f66-af94-401db4ad137a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'f99748ed-fffa-4f22-a604-4176e8036bc0'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'eca084a4-1f1f-42c0-b9e1-422285180455'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'ec0fe074-2467-46bd-9edc-42744110cc45'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '404d380c-742e-441b-ba89-4283d2d85612'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '5a71964a-a6b8-4c0a-b52b-429e06193e8d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'cc36f7ea-7a5b-4e79-9323-42b9284be72a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0, VisiblePosition = 46 WHERE ID = '30861833-dd4d-488f-a36d-430d490a7949'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '6d1e172e-5d58-4d52-a20f-432d9275f94a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '1804f193-f136-4f7a-86c3-4336cff515bb'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '48fa9221-9087-494d-be2a-433d2f419498'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '95b88fe3-6a34-4665-85cf-435b5f5a2a8b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '7064175a-865a-4b51-a408-43a2599b6ac1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 24 WHERE ID = '7dce12a2-1513-4fe6-b358-43b9f770d354'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '44568612-69bf-4272-bba4-43e318b90681'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'b9946e27-64b0-4c61-a463-43efbc86b992'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Giá trị nhập kho', ColumnToolTip = N'Giá trị nhập kho', IsVisible = 1 WHERE ID = '908d755f-4dfe-4ac6-8d99-441f229fd870'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Phí trước hải quan', ColumnToolTip = N'Phí trước hải quan' WHERE ID = '3dff835f-77ea-4a0a-aa52-4460ab9fe5a7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'b9447528-9d56-4eb0-b5e8-44de524459a6'
UPDATE dbo.TemplateColumn SET VisiblePosition = 62 WHERE ID = '9df6a75d-2094-4d73-8093-4575563df510'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'cd8aa429-8040-46c9-b88f-457c3c4b384f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '52d9da90-1234-4980-899c-45d5291b57d5'
UPDATE dbo.TemplateColumn SET VisiblePosition = 58 WHERE ID = '92541174-e690-4072-a087-46070c5ba747'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'e04eb4b5-00dd-4ecf-a7b9-465f1c0eea1a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '4b3b78a9-08e2-461b-a433-4695dc35937d'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '83dd520f-5de0-48e5-9c8a-46caec464d64'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '092b655e-e3ab-4cb6-81a8-46f4930988ad'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'c7a14490-481a-4d3f-a823-474414cb74bc'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'b5d5e922-a707-48c5-8ee8-479959763241'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '4ed9d3a3-090d-4382-b954-47ded2bf1a5b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 68 WHERE ID = 'a114886b-ddb2-479c-8e78-4820bd3569e2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '5d03e979-93af-4083-bdea-489533d1fff8'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '20e37439-597b-470e-8c53-48bca81034b0'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Giá trị nhập kho', IsVisible = 1 WHERE ID = 'ad29ece0-da64-44ff-89ca-48d6a07a8ce4'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'ce50217d-d5e9-474a-bc92-494543dcdb0c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '11546051-56c8-437d-8dd8-49d90da95e87'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'f4fb66d8-f419-4e90-a727-4a61eba9ba77'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'cde96a4a-ce54-4bbc-be97-4b63a24bc336'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'dc5a99f3-ece3-404f-916f-4b7a3329f355'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'f2c98d0c-7462-42c4-a63d-4c14940781e5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '233318cf-0886-4c0d-8e0c-4c6299c94817'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '209e81ec-78f3-4dca-97f5-4c861aa6d89c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'c76781bb-7fa5-48fa-b56b-4c9a319a473d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'bf0a0278-7c09-4d43-987b-4cd0c4aa2b9d'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '118833a0-62ed-46a4-8c87-4ce52d86a045'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'cffcb164-bfad-42ef-b5c7-4d1077fc254b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '66e162a3-6d54-4820-b1cb-4d17220474b8'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'd942f377-e59e-46fc-a4bd-4d57ab001b31'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '366746f8-ad26-4f20-995f-4d5fcc32fcb2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '30e1b047-aa73-47e7-a074-4d758eb6fa19'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'f605f9f1-7642-423f-8461-4e386fc9f146'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '24942e07-ba02-4bd0-96e4-4e4d622669d0'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'e101da12-7a82-4950-9ca6-4e5de4ec0967'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '5632468c-e030-452a-b360-4e77777d4db8'
UPDATE dbo.TemplateColumn SET VisiblePosition = 57 WHERE ID = '44071238-1178-4e3d-82a1-4eceb71771aa'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '511eea5e-d84c-4517-b5a4-4f5eff7bfaff'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '4b8a9380-b3e2-49c8-b1cf-4f70c75eeff8'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '69b9a54e-6762-4297-a9e7-4fa8f704521b'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '0a142b97-e46b-4157-a51e-4fb269e76d39'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'be49a0d0-d859-4001-9a1d-4ff8ca550b74'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '31d44fa3-7118-4fcc-b6da-50d092d5b3e5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'f8e57c0d-027e-45c8-ac40-51195c15e0f6'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '52c5f8df-6177-4d88-957f-5167e466450c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 8 WHERE ID = 'be33eddb-1947-4b52-8bcf-51b63e252e66'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'd9225de9-78cb-4ed9-95f1-51c463de4e56'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '07d354f2-c354-4701-99ad-51d96453e7d2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '4f96e391-999d-4522-912e-525ea9410d70'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '5dac1fc0-8d0b-4f7b-a9ea-5275a89ead2f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '92004d40-1c81-490a-be00-5275d0bd741a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '851d77c8-760a-489d-bf2f-52874035a7c2'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'aa4faa98-be0d-4151-a5df-53184a26aa8b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'd297e915-c7f1-4a03-98db-537e1015023b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = '49866e5b-08be-4527-90a0-53e30c2d8943'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1, VisiblePosition = 54 WHERE ID = '312ec950-9e77-4580-a5d9-53efd694ee33'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '417cd1a1-337a-4986-91fd-542284299460'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '2056b9d6-5113-4d63-9bd4-54268af31885'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '3035dbd7-561a-4461-b369-542da4567ed8'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'b69d187f-dc33-4816-bc2b-544aead9523a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'c9b68cb2-f9ff-4ed9-83a8-5489e1477a8d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'b0b55e01-66a3-4427-a8b4-549569f974af'
UPDATE dbo.TemplateColumn SET VisiblePosition = 51 WHERE ID = '3f4d1fa8-ffc1-470b-8bcd-551c5ba0b174'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '0acde92a-980d-4a2b-bde4-554477e80903'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '811c809e-2660-43be-b32d-559d983beb49'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '4e3ff869-b0f7-41d8-9122-55be03a627be'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '9b3d50cc-6393-47e4-b5cf-55de9f65bea5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '0bbecd7a-7efd-4d7b-b3ea-560623ed7d0e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '04c1d649-1087-4c51-b9fd-561ae9dadaa3'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '3a127b29-6de3-4008-8fed-564953150741'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '6fa3a7cd-ac4f-4823-b727-5659ffe0d9d4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = '9eec8d5a-730b-4417-a3c3-5727d5d50b98'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'bb209d33-e7f1-463c-9658-588b1f275682'
UPDATE dbo.TemplateColumn SET VisiblePosition = 74 WHERE ID = '1eb342a1-416c-4ed1-93f5-58984da9d6d6'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '388dfe6a-c613-40bd-aa46-58e2069aa902'
UPDATE dbo.TemplateColumn SET ColumnWidth = 100 WHERE ID = '8d02f016-a919-4f06-89aa-596bb6864e23'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'a1aca78a-3fe2-4d9a-b2b5-598826abf289'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = '577600a0-23a8-47fa-9015-5a41fa738d8e'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'f945ab9d-1a54-45a8-b1b3-5a5c5dcb95c6'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '7763f409-e545-440d-8dd5-5a9bb87c9f3d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '623c5961-2f4b-437a-80ad-5ad841cb74f3'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'edabc92f-01a7-4037-b4d9-5c34df6d4b25'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Giá trị nhập kho ', IsVisible = 1 WHERE ID = 'a59eef43-0ae3-4804-affc-5c425be6578c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Chi phí mua ', ColumnToolTip = N'Chi phí mua', IsVisible = 1 WHERE ID = '73dddb39-7911-4861-b75c-5c53386510d9'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '93f0f4c0-0b1d-4c35-8f52-5c7226694fda'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '85eca231-0dd9-4cfa-bf9f-5ce20fa6df70'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'bd40f8ae-bc63-42e6-a4f5-5d81360e1b83'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '5ac3c369-05f7-4d8a-86a6-5d9b490124e7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '1fcb1d86-2a0e-4a07-b1f8-5e9a183df6f8'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '5aa80809-aff3-4162-8d71-5f005936185f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'b833d2d2-a6e2-47ad-9d45-5f12aaa42ae7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'b126320b-b684-41a8-98be-5f72e6c6aacc'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '25a0a819-3771-471a-b21c-611676508b60'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'c008a165-c98d-4bdf-81f5-61c5162de270'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'f72d706b-bcaf-4fb3-8853-62755a6cbe39'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '7982e464-2c94-4522-b4a7-62884f6b41d4'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '03add716-c19c-496a-ae31-62cab74cd942'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'a27bdb9d-4ed9-453b-966f-634292d35af0'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '27b35977-c16d-4d48-b486-6347fa6b820c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1, VisiblePosition = 50 WHERE ID = '9ae5deeb-d734-45c2-800a-634a9c21d0d0'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '9c4dda69-a1d6-45b0-a9d4-6390aa942ba7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1, VisiblePosition = 11 WHERE ID = 'ef9dd06a-276a-4107-a4f5-652b8e2bf607'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'de67bdb6-05bd-4380-afe5-65305e07a88f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'be2681f7-5528-4e45-b179-6580dfe91234'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'b669d52a-bb7b-4389-a6d8-65844edb6ffb'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '491ab521-17d2-486f-88f8-658daf174483'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '1ff11948-6e7a-49fa-a12f-6594deb1c476'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'ab2e982f-f003-49af-ae27-661a3a706b61'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'a5ec0f8d-9949-484e-a601-66d5b00b7b5e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'adc5047b-c552-4c26-9ed3-66fdec970628'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '908c1dfb-cd44-42f9-b1b1-67840b63f606'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '1f3872b4-44e9-457d-aa54-67bfe47c89b6'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate' WHERE ID = 'd9e344e4-1ab5-4aa1-a0fd-68c24296ac2a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '4bc02d6a-15bb-4d58-9b6d-6927193fd66f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '27a02555-3d9c-4614-af04-6930d122d968'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '87aed74e-d603-4658-8528-69a52dd905eb'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '466420ad-2fbe-4f13-a87b-69a718e5a2f6'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'ad744d91-08b9-49db-bd25-6b2ef60ab6d0'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '0c8d5810-2949-4228-8bf0-6b890c146a82'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '5910e7ef-8088-4461-80e5-6bff4b695e24'
UPDATE dbo.TemplateColumn SET VisiblePosition = 42 WHERE ID = 'aa1d34b4-c8b3-46ca-a7c8-6c0f7126c46d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '82b4ec54-2590-47aa-a0d5-6c1241385dda'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'b0f4e9f5-45d7-410d-84cc-6c285cfc03ab'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'bab727d3-1abb-4595-9fd0-6cc141bfa337'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '2c300c09-9991-4cfe-aa11-6d3d5a080ee9'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'a840feda-dcff-48a4-b318-6d4edbc5984b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'ca8c2cc1-4f9b-4e61-afb9-6d53fb1578af'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '27869a59-1c49-4828-bdaa-6dc3938a16e7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'f29582cf-d1af-48b7-91f6-6e061dd72bb2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '05d93c57-a5f3-4557-beb6-6e63928d6eaa'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '0c793dfd-41a6-4719-a2de-6e7cd81e40c8'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = 'ce748e30-fc48-4300-a37b-6e8775cc0385'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '7b35a00d-cbcc-4dd4-ae02-6ea00e5e9633'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '8e52e9fd-d48a-4c67-8bc9-6f09ac2573fd'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '414ffefb-e1f8-42e5-8f6f-6f5045afd937'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '0f2c594b-a769-48a8-9f23-6f6ddf5b2519'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '95ff1910-38bf-451e-b2da-6f7172d2120e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '150abd90-47e0-40e7-a1f0-6fae6d295627'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '3d1d9109-26d0-4eec-9feb-6fc6010d8211'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '3b49cbbb-2dcc-47af-9bd0-70234b76f4ca'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '8341ca16-ffe6-4775-a0e2-702ec03867d9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'c3417c66-965f-4576-a369-706e96e951d2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '136b2316-b222-43c3-8923-70af2330e6e7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'ebde0a61-5736-493b-9ad8-7106bbc3ac6b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '56187519-6526-473f-b528-713f3fde0b44'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '7b41810f-2a58-408d-a0b8-719a262480f2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'a969b13c-2ae6-46d4-86f1-71a13c0a0a76'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '48edd745-b3fc-4f2a-84c4-71cdfbb3ba6c'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '4db0733b-9d0e-4f7e-9ee4-7223f8217bac'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '3257e073-0afd-444d-9408-726eff45a8b6'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '03c1a5af-1e6c-4e04-92d8-72cb94198703'
UPDATE dbo.TemplateColumn SET VisiblePosition = 7 WHERE ID = '9606604d-43e6-4a3b-8c47-73b4b7d62b15'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '9f986c9a-faf1-4477-bc94-7433c73dfddf'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = 'ed534220-ca55-484a-bdc8-743a83f78f1c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '3a2f71f1-1092-40b8-980e-747b8b4a6b14'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'deaaa619-d9f4-41af-9f1b-7489093b0327'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '30ca3323-2b10-4371-ad71-74938ab002d6'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'b5ea8db9-55ff-44da-9a24-74c424645d50'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'fc565a55-2d1e-4d09-be1e-74c7f21b35b7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '01a467d5-8540-451e-8cdd-75451f358a8d'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '5c4f4b14-3ce2-4c59-9569-75a1f8f36429'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'b41bacda-0de6-46d7-8fe8-75accd724d86'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'db743fd3-4590-4f2b-9b30-76728966b39a'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'c868f9e4-26ef-4069-9f07-769a1f5eee94'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '090b3a15-0a2b-46f5-a673-771f8f241110'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '990529c5-4696-4c85-a8f5-772446dd85fd'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'd144b9fa-e0f1-4f78-bbfb-77bc3de92eaa'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '7fe436e4-f656-4acc-94f3-77ccee2c5f05'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'a2a20f72-b396-4cae-9e8c-7856303fc32b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '00954fbb-b1c9-4711-b09c-786ff9ba3b78'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'a7de897b-60db-4710-b268-78715eeea859'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'c3a4f157-251c-4f6a-8230-78f623c7a637'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '2f0703c7-75c9-4bb1-934d-78f697a0e849'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '0d2e914d-78dd-40b0-bad9-78f78f8fe6a8'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'ac3b2668-1edc-490f-bc2c-78ff173d613b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'fa7afa04-82ca-423b-a8ae-791050d4b47f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'fe88c59c-3926-49e7-8d61-79dcda446cd8'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = '7c464ede-c5c9-4ef3-85a8-7a325c359cbf'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'bd3ffc13-66c4-4028-b6bc-7a7894043573'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '8e2580c0-6b0a-4b95-ab9a-7af3f0601829'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'ba3f3dac-0cc9-4964-b031-7b36fe0cc14f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '56c74a39-7b8f-4ae8-a96d-7bc5b318a1be'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '291898f2-69a1-4fd0-9f17-7be34fab4cf3'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Giá trị nhập kho', IsVisible = 1 WHERE ID = '94358f2a-6623-4ff1-be5d-7be989cda2f7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'af7e3056-bde1-4732-8949-7c04fcb9cd13'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'ea464aeb-cf62-4514-93c3-7c2af736e5ad'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '88f3c79c-8081-4f6f-8ee9-7c9ae4cff046'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '2c352eee-b311-48a5-a4bc-7d0b4e0a2d43'
UPDATE dbo.TemplateColumn SET VisiblePosition = 34 WHERE ID = '8e050688-8a51-47b8-9d13-7d838b80d6f6'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '822af77b-1ea4-4502-97ff-7dd5e0813ea0'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate' WHERE ID = '85246edd-2e89-45f4-a971-7e5af7231ba3'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'e0b9abba-b9e2-419e-ac1e-7e9eadab4c9e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '098e8b5f-ded0-4cc2-b828-7ff1f82e40b4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 55 WHERE ID = '9a09d1e2-a16f-4e29-aa68-805dadaca85c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '8eb36143-0663-45b2-90e3-8069975f75e4'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '8907f97a-c208-496d-9d29-80f7e62f020d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'fdef18ea-1e0a-47f5-81ad-813e4440b0b2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '66258efe-2eeb-4a06-9c25-81a7d717ea3a'
UPDATE dbo.TemplateColumn SET ColumnWidth = 120 WHERE ID = '6623c11e-a24c-45de-8905-81bd89efada0'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'cd4a6828-74d6-4e64-8fe8-82090e01a546'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '0b8789cf-704b-4b2a-99b9-82412a83003e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'f01c466a-3495-46f8-942e-825cc855e146'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '7f0e79fe-72b4-498c-be62-827a99b3fd3f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = '9f1e4a0d-768b-4742-870b-829fe1309965'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '0c427983-2d7d-47a7-8e21-82db0a576e9e'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'c8e58dfd-0b67-471d-9db7-839c2b809042'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '38e953c6-2d3c-4fbd-a129-8405b7a30e00'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '3472d1c2-e429-402e-8ff9-84272d841ac4'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '5d4c2858-d379-4190-bf1a-848138015c98'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '39969d96-d9ba-4907-9caf-84d153c819ab'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Phí trước hải quan', ColumnToolTip = N'Phí trước hải quan' WHERE ID = '00932e7e-5846-41ed-aa52-84d3646ac30d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '5d920195-afaf-425e-a060-84ed8f110fa9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 9 WHERE ID = 'b5dace54-6ddb-42a7-8b8f-8505ef8a2ce6'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '3db115b5-8b39-45c5-834b-851a0d98ed22'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '3b7e5ea1-9e5a-41b1-a48e-853b75dc0c9a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, VisiblePosition = 27 WHERE ID = 'de9f469b-48af-4167-bd47-857e0e4a7bad'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'b77b5548-a4a5-4756-a82a-85862ee6cd92'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Chi phí mua', IsVisible = 1 WHERE ID = 'e513c2c6-67f0-4c29-9217-85958c7b6adf'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '9ce57914-069c-45f9-a0c4-85993aae4b97'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'bdc958d6-09e5-42a8-9470-85b5c3a8a0a3'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '0e7198a5-d707-4330-a6ed-860b472e09c4'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 0 WHERE ID = 'e2148a98-b0d3-4a79-978a-86888fab28bc'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'b87d0149-cb61-415d-b6b7-869a9e6b3032'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '67776264-e28a-4d9f-be15-86b7b4afccd9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = 'ebcb8be0-5d06-4bc0-a33d-8705d5eb217d'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate' WHERE ID = '269f53d6-cd4b-4122-84b9-8706a399f70a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 9 WHERE ID = 'd4b75fa8-55e0-4f1f-9722-87259f5f2e8f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'e399fcd1-9371-48e3-825d-87e75423d763'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '1098bfef-33b4-40dc-b92f-880189aaae6d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'd9823b63-e2c2-43b7-b313-8827b35e16df'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'be061a49-3fa3-4509-b8e0-884e54c5eb87'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '362abe2f-9139-481a-8900-8896c8da3d5c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'eab1217a-9c94-4ca2-88b4-88c2fc39c53f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '93cb16b5-4c8e-4e65-abdc-8905747124e5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'aae63e91-9e81-4d0c-bf50-8944c6e8fa35'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '08b89543-56a8-4340-a027-89e1ffc32415'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'e24ddcd4-bb1f-47d9-91ac-8a2c9da1e0c4'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '96ef8496-c746-485b-be47-8a6ee622c7f2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '1f9feed2-66f9-4b6e-afa5-8aa2b54bed6c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'ad1c2bdf-314a-4bb1-acd9-8b0120ff62c7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '77bf9578-ef41-4e4c-b415-8b3ab321e0ac'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate' WHERE ID = '990ed372-83ba-4155-85be-8b3fa6386102'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '24e444fd-780d-45cb-a0d5-8b678c95a2ae'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '88899a47-861d-4486-bede-8ba88ec02636'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'dc1156c9-74c2-4251-8237-8c124146844b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'de6520d7-91d3-45fa-a761-8c1fac3ca972'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '546aea67-bf64-4fd2-a787-8c9745b96d90'
UPDATE dbo.TemplateColumn SET VisiblePosition = 49 WHERE ID = 'ff20ff01-06fa-4b7e-96d5-8d803e0d3e73'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'a9039df6-9d32-477d-8715-8db76ef2a0aa'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '9627ebe3-e07b-4adb-aa06-8e1610e1a2c5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '9f2ce413-caa7-4d3b-9d8b-8e294b4bf371'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '95152a5d-e61b-4c6b-8ae3-8e6d52504069'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '28e904aa-1546-4adb-8415-8ea812a7e65a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK' WHERE ID = 'd832765f-741f-41cb-a530-8ee473baed5f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'f98b9e5f-4119-4c92-be95-8f3faf1e2454'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'd2b298e4-ea54-4909-9b03-8f5f1d7a0474'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '6f67d85d-c6f2-4a94-87d8-9035c6695b90'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'de79daf2-76a1-44e2-9b36-90ccf7da6083'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'e58e7c57-80ee-467d-bd28-9108d0def742'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'bc1769a2-9ef7-449a-80c0-914382efa96d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 24 WHERE ID = '0a680027-f16a-490e-9114-91a7944b0243'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Nguyên giá ', IsReadOnly = 1, IsVisible = 1 WHERE ID = '3224832a-fe7b-4503-8dff-91c1333d6b3f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'b8b59027-fd5a-42f1-8f76-921e702793e0'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '913336d7-3d10-442e-ac1e-924f5da193f4'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '1edc6ba1-f55e-4eb3-a408-925f891ec3da'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '5ef36b93-2a46-4a6e-861e-928367874cf0'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '890c5318-79ce-4a80-afb7-9285fa620219'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '75d3bc80-6686-469e-b8a5-9395b6f35448'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'd617f58c-afe4-4014-a930-93cf8e75f13a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 5 WHERE ID = '65719083-864b-4e29-9ae9-93f70616be3c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 52 WHERE ID = '86b89369-14a7-4ead-a683-944a14da3526'
UPDATE dbo.TemplateColumn SET VisiblePosition = 31 WHERE ID = '8d9eac25-fb7d-42b6-984b-94571f516bcf'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '6cbef718-fa9f-43d9-bf1d-94dc1cdf032e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'da604501-c552-4d15-b437-94e693ddae14'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '37133136-2221-4f9c-91aa-955e76ed6331'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate' WHERE ID = '8012a5cc-b5e9-4c71-9574-95724d03b971'
UPDATE dbo.TemplateColumn SET VisiblePosition = 73 WHERE ID = '47869352-30d4-480c-b4e7-9589655f87cc'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'c72dcf54-f9ee-4a9f-90c7-95a35910e2ec'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'c90814e2-9638-4070-9efb-95a758c2e348'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Chi phí mua ', IsVisible = 1 WHERE ID = '8c69e82c-5951-47c4-85f6-95bfe36fe346'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '1560899a-5120-484f-bdb9-95cc509a6a01'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAllOriginal' WHERE ID = 'a3813947-cf61-4ca1-94cb-96067bca9adf'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'bf2482ed-5ce4-45b4-a6b0-9651c7b7df13'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = '2e1db53b-5400-4ce6-b420-966b50ed17a9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'f7bc9492-b5c6-44ab-a212-96cdc67dad20'
UPDATE dbo.TemplateColumn SET VisiblePosition = 39 WHERE ID = '1c04ffdb-0e12-4460-b03e-97044ceabb60'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'db2bbc65-c811-4183-b5b3-973e24e3aa73'
UPDATE dbo.TemplateColumn SET VisiblePosition = 30 WHERE ID = 'e0d40d86-9222-438c-ba57-975313fe1e7f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '4234b261-3955-4c99-96c7-97a158158bc3'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Giá trị nhập kho ', ColumnToolTip = N'Giá trị nhập kho', IsVisible = 1, VisiblePosition = 31 WHERE ID = 'fec0f6cf-66c7-4c4d-b957-97c4da71c024'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'df6956bd-38a2-4d3d-a775-97fc982681c9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'c97dddcd-9420-46a1-8404-980e7cd11ff4'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '7fe1cacd-af89-4232-a3de-9818e789f9f5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'd983b473-8f4e-4e0e-a9e9-984b94c3f3e5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'b403e9b6-fc80-4440-949f-98a023559bde'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = '69a4ce7d-35ed-4b15-9927-98b31a2917e3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 56 WHERE ID = 'a7d45607-ceb6-4723-9a0d-98bdc8d2edc1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'd993dd4a-a5fb-4179-a5d7-992de4ea19c9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'fc95db13-00cb-49f2-87e7-99463d35e0da'
UPDATE dbo.TemplateColumn SET VisiblePosition = 33 WHERE ID = '5258a5bf-4e49-4bee-b54c-997918468ae5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '8f725818-b0fe-4a1d-a5a5-997ef2d604f8'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '58de721c-c720-4086-adef-99e6e02d06a3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 54 WHERE ID = 'b200d490-334c-4a89-a4d8-9a6e2821f2cd'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '60fadfd7-6e3a-49b4-98e1-9aa537d25d19'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '56029874-ef0a-4edf-a143-9ab9048205cd'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '9c4f454b-38a6-4245-ac93-9b17b55a3dd9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '20bdd01a-5e3a-4c9b-9f8b-9b670dce6dbe'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '9405000b-27e7-4fd7-9999-9be1d8cbd79e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'a30358fb-8553-4a64-86d8-9c03d36c8aa9'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '7190b0e1-e725-4c77-a867-9c09425424d9'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 0 WHERE ID = '15a3a87d-d874-4177-9e7b-9cce46c0a461'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '7e6ee407-c236-42e8-8b68-9d3df620cca4'
UPDATE dbo.TemplateColumn SET ColumnWidth = 120 WHERE ID = 'a4d56a74-793d-4b64-a7fb-9e123c97ce63'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'e5a259aa-1651-4972-860e-9e4135466f4b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '03de9d4c-9c41-4ce2-a61c-9e9b709f6fbd'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '179d01c9-3efe-46f0-bb7f-9ef22449e275'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '11abf832-48db-46d1-9bdf-9f041aa9376d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'caaab5f3-cbc8-4206-b695-9f4a1c8d4915'
UPDATE dbo.TemplateColumn SET VisiblePosition = 28 WHERE ID = '74bd0a78-016c-4d63-8107-9f967664b281'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '747f51c2-b867-44e7-9765-9f9d74c94870'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'e4af21dc-0582-41de-8ee4-9fe1e319d67a'
UPDATE dbo.TemplateColumn SET IsVisible = 0, VisiblePosition = 53 WHERE ID = '65400ad0-9e91-401c-86f6-a0499ecf6f7e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '54f79973-2e8f-4fc9-911b-a084d9f1d03a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '1d8408e3-347b-4035-b9b8-a08f20208384'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'e00b116e-1a99-42e3-8d0f-a0ebae1b02c0'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'f3e8ccee-07d1-46ba-b715-a137fc80d3b2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'a0499cf0-96ad-47cb-9230-a154daf62fbc'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'e5750894-f933-40f8-9fd3-a182d9ee9783'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '1498c44a-10a2-47f2-bc32-a1dda63ae309'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '09c0ad96-04e0-4217-84e7-a2020589f5db'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '675274b3-8723-4ebc-b9e6-a2098844d9e7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'f8130381-9cfe-4d74-9350-a22969872cf7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 26 WHERE ID = '51764095-14c0-4be0-bda5-a22b7d21fda6'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '6b9f29ae-e231-4dbc-852c-a238b6cff371'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'ff13d65c-bb37-461e-b744-a27febc0e7d8'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Chi phí mua', IsVisible = 1 WHERE ID = '15e10914-4352-4adc-adc4-a32afb73b840'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'cae1b85c-5563-40aa-9827-a38f76fbfe5c'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '02942ada-205e-46d2-b479-a39f33f220f9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '9f8699fa-aaea-4c7a-9937-a3dbc469db82'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '2da34f03-9433-4e3c-b4fd-a4028150be46'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '2ba958f8-d663-4df4-9ee4-a43c4009782e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 35 WHERE ID = 'fa90728e-11a4-4fa9-9ad7-a4453acfa0af'
UPDATE dbo.TemplateColumn SET VisiblePosition = 15 WHERE ID = 'e90c83d6-e94a-4e12-bffb-a4783da4db26'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'e7430cc7-2cb3-4408-a7db-a50d1f66f4f8'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = 'a5470001-c0ff-421a-8c55-a51a4f0f6f00'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'b2d01117-b605-48be-a69c-a54474378fd6'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '924e40e2-020f-4e58-aa5e-a55672496001'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'ad725a2d-5fd2-4e32-81a7-a5a23300d2d1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '3ae6b40a-8905-4a8b-a7a8-a5ae37e37549'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'e464a82b-0cf7-405a-b109-a61862730618'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '098e6df6-2dea-44eb-8ce8-a62e00f5db3b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '09f6778c-2f40-455b-9a4e-a63ca19f7093'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '671378c4-be2d-4292-9100-a64b3efb0ee3'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'd238662d-409b-4f1d-83de-a6669ca8dbe0'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '41e7cb52-a03d-43be-ba3d-a6a5c39dcb40'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '03fc795e-bd12-4fe9-abda-a6ce08c52f3e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '61ab5da4-e942-425b-8b7f-a7234adc8041'
UPDATE dbo.TemplateColumn SET VisiblePosition = 9 WHERE ID = '59994d84-4f4f-4bd3-a38f-a752a241cb2e'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '7391fed8-0f01-47f3-97e6-a7d44d098286'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '6d03d7f2-1644-4b5e-b4df-a7ecd007e144'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '092a8056-cf15-4f5a-ae6a-a85814f7122a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '32edf215-3f23-4a80-a205-a8b311304114'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'b6cb555c-82cc-4b67-be62-a9625406dbf8'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '3560b309-cf09-451c-b19a-a96e70618876'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '888741e9-1896-4355-a93e-a9a9ae9bb0a9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '5db83f27-6eab-46a0-b6c0-a9a9be79a6cb'
UPDATE dbo.TemplateColumn SET VisiblePosition = 30 WHERE ID = '6ca07f7e-3ab4-40b6-adc4-a9b66e68f9a8'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '0b608b36-279b-46dd-b432-a9e88611e1ab'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 0, IsVisible = 0 WHERE ID = '4490bec3-c7f8-40d6-8df0-a9ec96984fa7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '0b2bb902-af47-441e-ade4-aa6877915fee'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '5384f2e7-829d-41b7-b9e5-ab281ace1ce9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '825d2a59-1c1f-4659-a63b-ab318364c22b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '4fe7b916-0a5a-43bc-8849-ab7061696be5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'c017485f-aeab-4123-9f3b-ab743959bd06'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '44fc04b2-c786-4af8-b179-abb7b79f6915'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '4a91e769-91aa-4965-9562-abceda445aea'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '7bd69bdd-2d1a-484e-8326-abda3a7bb4a3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 29 WHERE ID = '37f2c31b-9674-4dc1-92a2-ac3ead062743'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '3df411f5-704f-4969-9057-ac41c4cf2858'
UPDATE dbo.TemplateColumn SET VisiblePosition = 28 WHERE ID = '597a6fed-2034-4221-9c0b-ad4313cf2f44'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '56c445d6-c096-4529-a00a-addfe72b261c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'd4f61dca-1d33-4828-a231-adf207bed867'
UPDATE dbo.TemplateColumn SET VisiblePosition = 31 WHERE ID = '67fb23e9-9cf5-430c-b7b9-aebc841b04ba'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '74a9f737-d9d8-48b2-a55a-af3f97214e77'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'bcf3abdb-9b85-4079-8465-af8014c10f3d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '010c0903-461b-4b6d-b8d7-b002511fee2c'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '52fa6f90-1c28-4624-985d-b0e93a861dc8'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'f3c9c37f-aeff-4f9d-8d7d-b1a635d142ff'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'f847df81-e2db-45bb-be71-b1c68b3df807'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1, VisiblePosition = 34 WHERE ID = 'c07cc787-46b6-4eef-8c63-b1facf811c4f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'cfede1f4-6903-42ab-baa6-b26d0a391088'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '0ea8f332-cf7a-4d89-86c8-b2934d4f4210'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'fb4fc96d-540c-45bb-a1d8-b2eacf235a97'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '1586a17e-a0c2-4633-ad27-b319739776d6'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '8658aa26-dd79-430e-955c-b356568cac33'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'bd48f19d-0490-440b-8b62-b3aa2649415a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '6ac6bcf5-039e-43e6-8211-b3e10801294d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '27975dc3-1123-4aac-adc4-b48e816fa130'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '0c750efe-840e-48be-aae9-b49480da4d9b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'c4abf46c-8d67-4809-9b32-b4f37db6ec3f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'cf42b8df-3dfe-4adc-af03-b56bab8899ac'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '6866e643-6a82-4c66-9b41-b576bbd842f1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '6595bebc-980b-425f-8a99-b5cfa653a978'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '3aa63e7a-bfd5-46de-a058-b5f7dfe694e6'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', VisiblePosition = 15 WHERE ID = '7bf19bcd-829e-4274-a86e-b61392f44561'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'aa169626-13d8-4773-9338-b639ac7a9fb7'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '27aabb84-2199-4f97-8e00-b63d81efc860'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '577e21c6-1e9a-4af0-829e-b75d0316b669'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '5400a51c-50ac-40a3-a824-b82d59365fa1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '0ffb4596-fe84-43ed-99a9-b871fd8244d3'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'e9b01f75-c624-40ee-9ae4-b87e646f550f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Phí trước hải quan', ColumnToolTip = N'Phí trước hải quan' WHERE ID = '97c7fda7-8e6c-44db-bfb5-b88dc3775ec6'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '32a72092-d34c-45a1-bd41-b8b626afb0ac'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '9e9901c9-f46d-445b-8d4c-b912cd886a6e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 34 WHERE ID = '51ec940a-8888-4abe-9453-b91531b9eef5'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '8c98573c-7e88-482e-bc77-ba3382ac1f03'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = 'beb33586-b4fe-453d-8496-ba7e7e35225c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Chi phí mua', ColumnToolTip = N'Chi phí mua', IsVisible = 1 WHERE ID = '1ebdb663-b699-41b1-ad0d-ba9bb1e56b6f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'c8af2b03-70c7-4764-85e1-bb23e9935b8a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '045f70f8-f47a-43c5-be25-bb25928026b1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'a1c66c27-c503-4e1e-9507-bb2a55f0bc12'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '3ab03dcb-d857-4ba7-b03c-bb84978a882a'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'ff045414-0c9a-4fa6-97a1-bbb9eaf5fd4b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '22bdd366-260a-4221-9985-bc6c281deafe'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'f3ee5ffc-6f51-4794-8215-bca386460219'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'f6d24217-d332-4ea0-8927-bcf9c1f9ae28'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '8afa6828-c43b-46f9-a49d-bd82f5028e3b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'a5e87786-d88b-4397-817c-bd95a126c36e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'd49a8672-9211-42c5-b73e-bdb836db3428'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '3ecbdab7-2bf3-462a-930c-be1a6d411b11'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'f3340976-ba2d-46d5-86e5-bf89bcac28ee'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '648db23a-7a3a-477c-8c23-bf92dcba78e6'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'c79c7457-4038-476e-88b3-bfa6d60df3b1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'f734856c-1460-4531-993c-c03f5d017e0e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '87b5759f-dc82-4f7d-b1b6-c09709bb05ec'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'c912ee88-b343-403c-93de-c0bdb2933866'
UPDATE dbo.TemplateColumn SET VisiblePosition = 41 WHERE ID = 'ef556ea3-49ed-49de-9f97-c0c9a76b743c'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'f76564eb-e726-441e-a98b-c117a85396bd'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'beda9c8e-018a-4224-ad92-c151bd8179a2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '91aba27c-e663-4273-b40a-c17374a20196'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '0bcbf68e-8dbe-43d0-ad5b-c1c5667ad1f9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '09032669-f3c7-41d2-bade-c215e85249ad'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '33e693ed-d4ea-4244-8ada-c246bc95aa33'
UPDATE dbo.TemplateColumn SET ColumnName = N'ExportTaxAccountCorresponding', ColumnCaption = N'TK đối ứng thuế XK' WHERE ID = 'ff2c1aaf-abf1-4312-87b8-c25d6f355bcc'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '91425207-1d73-4cf6-8c15-c303ce218aa0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 65 WHERE ID = '4691d04e-9b9a-4106-a1b9-c313603df8db'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '45103bc8-02ca-4bbc-a26a-c375aafba833'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'c11debc3-0033-459d-8f86-c3b5b3940bdb'
UPDATE dbo.TemplateColumn SET VisiblePosition = 37 WHERE ID = '3c8ebfcb-0333-4ef6-b332-c3bf23090a2c'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'e38d0fd6-4916-4667-9be0-c3d2bea1dcd6'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'f9c68312-e957-43dd-909b-c3e4d4429b5d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '6eca65bd-cad1-42f6-a8ab-c4360f156666'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '245667a9-acc2-4ad7-879f-c44e310c3fd4'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '8e97a0bf-b434-44fd-a09d-c46619d71e68'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'c5a3c8a3-9ee5-479b-8470-c4beefd8af20'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'ff1af6d1-ea21-41ec-8885-c4ed531df460'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '6396f2da-f132-4d40-ac7e-c526f7ce697a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'a32e2a94-0a9b-4326-8549-c551505ca86c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 34 WHERE ID = '89678b26-947b-47a8-897d-c5a8a846f840'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '6959adde-e7de-4c15-995b-c6f0e81cf57e'
UPDATE dbo.TemplateColumn SET IsVisible = 0, VisiblePosition = 26 WHERE ID = '9ab2618c-242d-455f-b6f7-c759a6b710e1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '3d14de2f-30eb-4e94-a7ba-c7e504a56798'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '75217b90-f876-46b5-9d3f-c81ff8312729'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'f6d897af-fe47-407a-8946-c8c6f6b4aca9'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '1bce0102-7355-4821-b0c7-c9500c375565'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'b691d8b9-5da6-4a04-827f-c9c1d1a1795f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '42af796d-e9f6-4b0f-bfe5-ca0076f4a78b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = 'd70976f2-3881-4489-a1de-ca2161b99150'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'f4408396-a83f-4786-96b2-ca3f3e21e337'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '3619c9b4-1af7-4606-b3d3-cb1cacef87ec'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '4d906310-7f89-4ed6-a796-cb483b00f62d'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 0 WHERE ID = '1116939e-1de4-42ec-815d-cb73a36980b3'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'b583d659-1692-4609-82cb-cbd2b657c3d1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'acc269fe-eef6-4b0f-b62a-cbf43c6496bc'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '005232a0-482f-40b9-ac71-cc1a903e3950'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'a3222c91-2f73-44a4-baf3-cc6f4d5eaf9c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '7fecad6f-7d9f-491b-8e7e-cc8a20df1919'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '361c907a-3181-4e69-b159-cc96037a3900'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '0a59bc82-8167-4db2-810b-ccf9a3bf0d19'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'a6718c3c-d672-463a-9aa5-cd45781c6156'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'eea15c3c-77f7-41cf-a305-cd48265f339c'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = '8681a251-cbdb-431b-abb2-cd8c0cf44c64'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, VisiblePosition = 3 WHERE ID = '60160434-8168-42d2-9328-cda610ef9a4b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'd4baa077-5d9d-474c-9893-cdffd04c1e90'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'e406548b-ca8c-435d-9787-ce04bd4eed95'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '92b6efd2-5e6a-4e3f-8c8b-ce05445a6a5e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'c0075ca9-84b7-4643-ab73-ce240185b7b4'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'f0a6ac7c-034e-45ae-b184-ceaa67ab141b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 72 WHERE ID = '82a2d63d-bea8-4979-9b92-cf7363bb7679'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '56ce684e-4d86-4a3b-89a9-cf7464483d62'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '89a40599-47a4-47c3-b13e-cf821d4430cb'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'f7d5267d-5264-4ca6-a006-cfe6001eb1b3'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'aaad2231-8329-435a-835e-cff6c5503ce9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 44 WHERE ID = '18137840-5fba-4e6e-846c-d004283b89f5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '8655189d-8a3a-47d5-93ac-d1569f7490f7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '6fd6db25-7d8a-4b61-8346-d168175c63d6'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '376a9cba-2ff9-4017-a2ab-d22310d3c3ab'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '2791715d-487c-400d-966f-d3d9841ed4de'
UPDATE dbo.TemplateColumn SET IsVisible = 0, VisiblePosition = 10 WHERE ID = '60bd3b40-520c-4650-9bd8-d3f6233cf1f7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '50ae044f-f65a-4127-8e28-d41b2b192723'
UPDATE dbo.TemplateColumn SET VisiblePosition = 43 WHERE ID = 'd73a16d4-7be4-4bb7-8cd3-d46b14a9283c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '59b297d1-e8f6-44df-8ba6-d537938c16e5'
UPDATE dbo.TemplateColumn SET VisiblePosition = 64 WHERE ID = '51f6ecbf-9cdf-425f-8753-d55f067b81a9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'f7ce580c-b658-43a3-819b-d5b23fdb3ab4'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '397f171e-5e99-4f75-a1ac-d5f2d932ba88'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '426bdcdf-3beb-420b-b1be-d5f8f7fccbbe'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'ec29b307-c5a9-4960-97ab-d64817e2e5fc'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '2f34d7ce-f8a8-4b60-a9d6-d64951a57d7d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '3d8837cd-8203-434e-b463-d69662297834'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'be5dc9e5-195d-413b-9411-d699d502439e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '9f14b911-e670-45b5-888b-d73c4c3a4151'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '94130cc6-f68f-4aa0-a6e5-d7870c574bf2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '3925e254-bce0-4d58-9c84-d7fef5940dd5'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '0eb8bd41-1be5-452f-a590-d82c380db25e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'ebce5016-d1b7-44b5-bd6f-d8460baee996'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'dea488a2-e0c1-4e74-9215-d8b64ff5bbe1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '1163a5e4-c552-4bd3-9fdc-d8cd82b6437d'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '63ab2db8-de05-4a65-b9a5-d8de4b0ef2dd'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '03d495cc-5079-4e01-b989-d8ea433a010a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '9f84720a-fb83-40e8-844b-d90efdbc06bc'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'dcbf55e5-f8fa-404d-9760-d9152e706152'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '7b5e5894-a8f4-454c-8100-d9457db9cb2f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '8e89eb4c-6594-491e-ae75-d95425e2321d'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '73cb0836-d5d1-490a-9461-d9928f416794'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '1ac153d4-1e8b-4d20-9ed6-da87e28d2a68'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'e3ab2514-0d77-4ed8-a5cc-daa4b9b6e8e6'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'fdf86b98-f59f-4f22-8419-daad25b0e28f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'a030cf2f-d598-4024-a901-daf3e0eba754'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '47cef2ed-9708-4f4d-9a43-db05ab3333c1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'c6a9b51c-bcda-4d24-9397-db1f7eb45f2b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '2e750573-5e65-47e3-bd89-db53ea0d00f0'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '8345f61d-1d0a-4919-b551-dbadf036ca87'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '7ed68b54-224e-42ae-9010-dc84b1fc9f5a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '199239b1-5eab-4cdf-a7b9-dcbb304fbd31'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '179e0e11-d03b-4171-a5d3-dcbbb5445e87'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = '9ce41ee1-20db-4f34-b492-dd5c47e7b98e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '43a19c28-d9bd-41c4-9976-dd665d4eb132'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'c983c0d5-b679-4479-aefb-dd7085808516'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '4abdad52-5a54-46b1-b7ba-ddda71a8a8c1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'f6a30b55-ad2a-4785-b577-dea201b05ed2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '16829d7c-e2d8-4e75-82bf-deb7ec1f59c8'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'b442b4e1-4d45-4534-8284-dee834e08691'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'd6e373fb-7a72-42bb-b1ec-df23db8beb75'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Giá trị nhập kho', IsVisible = 1 WHERE ID = '371cc3c1-05d8-4a90-aefb-df3c500b4ca8'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = '09f4aadd-a99d-472a-9c51-df4b8884be08'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '606956d6-a81d-494c-92f4-df4f3336c33a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '11cebd0d-7daf-4614-a2e4-dfc0a35f80e3'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '429e0c80-f2c0-42f0-ade0-e0346c689451'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '7ad9db4a-5ab2-42dc-bb37-e0395c4542f1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'e0732844-6a84-4eb1-b67c-e046b7b0baf7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 33 WHERE ID = '0914b6a2-d3d7-42a8-82b8-e06391788b12'
UPDATE dbo.TemplateColumn SET VisiblePosition = 75 WHERE ID = '49fbf93e-790a-49bf-abc1-e0b82809202f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '2fee27be-1f8c-497d-b49c-e0f9c171b720'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'a14fa51e-b215-4521-b6b7-e12cf13ce402'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = '92208569-ff8d-4695-8ab9-e16266b8f03a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'aa1a25df-aa33-4075-94e1-e2a278b3a6d5'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 0 WHERE ID = 'cf3ac808-5482-49f1-89a7-e2ff7a3153b5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '6d27ee13-dd0d-4cee-b657-e3212c5eb22d'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'bb51ad3b-1bca-4347-b9db-e32209778171'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '6c77d8dd-cf39-41de-9428-e32500ad76e7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 55 WHERE ID = '74d2590d-3109-440d-953c-e3449974b02a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 71 WHERE ID = '7e5e631f-2527-4f2c-bf36-e3b0c1d56233'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '723c85ea-2968-451e-82bc-e423f3e99e7b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'eee000c4-2d6e-43da-a430-e47fad3e5c83'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '25b3ac44-a4ff-483f-81a6-e4b4b82aa9f8'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '9cf2ba01-dc4d-4cb4-bac3-e4e6c560ac57'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '7ba3fdfa-8c65-4284-a281-e51d8d8d9633'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'b5e27ec2-5087-47f3-90c6-e55a2116bfb8'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '7f5f10d3-fb44-413a-ab01-e562106a5f05'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '0e409557-4464-45bd-94d4-e5c9c7129add'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '08ddd71a-2537-47d4-b5a7-e63ef153a0ca'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '3a3afd59-eedf-4a56-a062-e6da765bbec2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '646c78b0-3770-44a3-a1d1-e707a069de30'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'c817542e-8d08-45d1-a218-e73ba96558a8'
UPDATE dbo.TemplateColumn SET VisiblePosition = 47 WHERE ID = 'db5a171a-ad6d-4703-beaa-e7527e4d671b'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'cb0fa4a8-75b0-48b3-b67a-e76492aba983'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'e7ecae5c-472b-4e4f-95e9-e7a6b3f092bf'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'c68f2d07-e079-4814-ab5c-e7febd4af15f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'ab3fdbdc-9b07-4821-88ab-e814dcdf3128'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '9b002418-8014-4949-b4c8-e815b7f225b6'
UPDATE dbo.TemplateColumn SET VisiblePosition = 44 WHERE ID = '6d6ba396-a70b-437f-ac0d-e81eee9d16c9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '31a98213-7656-4958-bb54-e838de22bee2'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = '972e547e-cb1a-461f-9b9d-e839bca07fb3'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '3835f7a8-0d45-4f37-a33b-e84d1e088590'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '35bbdcfd-8fc8-4971-9a49-e8d0a23c0787'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '171e6d09-4cc3-4d3e-88a4-e8ec22c95364'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate' WHERE ID = '21145937-c383-4ee1-8eb1-e93450d15f3e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '9f1c804a-cbe1-434a-b7d4-e97918f449b7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'd7886c7f-c210-4261-8f38-e9b4fc5556ef'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAll' WHERE ID = 'dd07e6d1-12bb-48e8-b65b-e9be777932d9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'ba3f996f-6e97-46b7-b42d-e9c1a76952d9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '0705d1f0-3423-4f59-97ae-ea02ac4e905c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '48febfee-80bb-4077-943f-ea451f94abf3'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '6062458b-fd80-4b26-a72a-ea6050a62bb7'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate' WHERE ID = '37f88939-f9ba-4238-9053-ead3ac0f43f5'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '23f611cc-5ddf-4ebc-a544-ebc24915a75c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '4b9b7ff2-c254-436c-9b94-ebc9583aba30'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '6acb32a6-ddfb-4b54-a227-ebe83a55c651'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '2d822c1c-6615-4d01-93a4-ecca4270181e'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'faa1a375-3f44-47ba-974a-eccd18ea0472'
UPDATE dbo.TemplateColumn SET VisiblePosition = 48 WHERE ID = '564c7a85-39c9-4afb-87a3-ed0e04a8b6c1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'aaf6dcb9-a591-455c-866a-ee7136102f84'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'f5d61fbc-9b3b-44d6-a7b5-eea5e5204713'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'a1254f93-30aa-409e-9a24-eec242ffa3ae'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'c3423999-01d6-41e6-babf-ef5aec8b25c8'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '35c1337c-0a10-485d-b5e2-f0113378d76b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'e8f176f2-8d55-4ef3-bed8-f082f5c80d2f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'ec45fcd4-7714-4e43-a638-f08ab360879c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Phí trước hải quan', ColumnToolTip = N'Phí trước hải quan' WHERE ID = 'c4f6ea7e-17fe-4133-ba24-f0ad6f7d4cfa'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '60eda7a3-07ed-4293-ad76-f0db0406cf98'
UPDATE dbo.TemplateColumn SET ColumnName = N'ExportTaxAccountCorresponding', ColumnCaption = N'TK đối ứng thuế XK', VisiblePosition = 8 WHERE ID = '6853f365-4fb4-4510-8182-f16109f6a4d5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '992d3114-d0f7-4784-87cc-f1814fa12a7f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '1223983a-1fcd-4ffb-877d-f2a4cdc16d41'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 0 WHERE ID = '89acc391-95c0-4e84-b287-f2d1130a3670'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '8c05c882-9826-4475-bbeb-f36aa086f040'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = '8b3119b3-b91e-4e1a-ad1e-f377223b728b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'c206b629-3afb-4bc9-a706-f38345959c18'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'f7226e51-d492-4361-9ff8-f392e7f547fc'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '4a588853-82ab-45fe-b66d-f3943ef4a711'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'f1b66789-58ca-408e-9ea1-f414ad8d87fd'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '8b09fa3f-1211-46bf-a157-f42eb6e8f344'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '7870874e-a2c1-4738-a33d-f46a5097a048'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '7dab1c8a-ce41-4cc3-a471-f499da3d959b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '53378295-3e56-4575-ab80-f4a59a1919a2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '43168f65-6428-49f5-b13e-f4e92358bab5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '2879c47e-3a52-47a3-b70d-f4ed9b184d86'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'dcffe8ec-3cde-4413-a2f8-f50eb69c2afe'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '73f43502-678f-45d7-8cae-f5424226c515'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '935ddc47-43e4-43e8-981d-f57fd0b9b988'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '4bb5aadb-4c9e-41a4-aa05-f5f7a5bb9cd9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '3092d96f-8b03-42f0-84fb-f61554483b55'
UPDATE dbo.TemplateColumn SET VisiblePosition = 61 WHERE ID = '383d61d1-349f-449c-9485-f626d490aade'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '4d0c7420-25d1-4500-8f6e-f6323aa22a85'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'faec3693-a3a8-42ad-95c9-f6fe37aee8e4'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '75b728f6-b17e-4ccb-8c14-f744205a09fc'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '24fe4fa7-fb8f-46fa-8d9e-f750d7382602'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'afe283cc-59ad-40fc-b81e-f75ee09a3f7f'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'fa285f3a-6712-4b64-ab9b-f7a08670b180'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'ecd736c6-db4c-4a73-93c1-f7a14641e0f2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '1139b3be-cf7c-4fe1-8d06-f8d0206d82cc'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '25353c4d-eab7-4fc6-a99a-f8d7720be014'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '97cf4ab5-89ea-4a0a-b4d9-f91418eaec00'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '8a544708-add4-40c3-92c5-f9149e2394e2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '0db3ff52-adc0-4152-bb99-f97d8b002a96'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = '14ebadfa-174e-49d6-8897-f985732d50bc'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'f7002a3b-21ce-4086-878a-f9a2152bfdc6'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '36744603-b551-46a1-acb0-fa1cf509ddb2'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Phí trước hải quan', ColumnToolTip = N'Phí trước hải quan' WHERE ID = '8daec1fb-8c4e-4927-8a2f-fb018fe2fdd9'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'a358ac7f-2dc7-4ad0-a994-fbb6035c3621'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'db466504-1d34-4a54-81f7-fc2353909326'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '0416e0e0-4e84-4259-a60f-fc23e8e1bc62'
UPDATE dbo.TemplateColumn SET VisiblePosition = 55 WHERE ID = 'aafd36b8-f896-49fe-9bb8-fc9c102ec51c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0, IsVisible = 1 WHERE ID = '9a2ceb0a-c6c8-45fc-a368-fc9c13545387'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '674c5484-2d1e-4b5e-b1d6-fd15745b223e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = 'ed5d200e-5376-4d6c-9268-fd390a9764ba'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'a4a019c9-0c34-46ff-8cb2-fdb976982112'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '78297f5c-1466-45dd-8de6-fdf214362707'
UPDATE dbo.TemplateColumn SET VisiblePosition = 40 WHERE ID = '2cc39514-5dd3-4fa4-852f-fe12591c5bba'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế NK', IsReadOnly = 0 WHERE ID = 'cfe7cb90-e45f-4a99-bd57-fe54a0f7570e'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '46c3707c-c031-40e4-896e-fe68107da1f8'
UPDATE dbo.TemplateColumn SET VisiblePosition = 66 WHERE ID = 'cb0bb422-12fd-4c7f-aafb-fe6cc59072f3'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '2e4d7d7b-d05d-4576-99b2-fe7a1d766d74'
UPDATE dbo.TemplateColumn SET VisiblePosition = 54 WHERE ID = '45e01c26-6617-4967-b093-fec195b43328'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '5e19a5b3-5953-4f8e-bff3-fefc5c48cbb4'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'b2f7c854-7498-4d1d-89da-ff3de80885aa'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0 WHERE ID = '87f4844a-a581-476c-87ca-ff5c811607bf'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = 'd5f3b903-0a00-4ea0-a791-ff653cb7aca3'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = 'e81341b3-8f7a-4e41-b04d-ff70f7f739fe'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế TTĐB', IsReadOnly = 0, IsVisible = 1 WHERE ID = '058e9c51-f373-4f10-aa30-ff7837f801db'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '5206bae3-5ad1-42d4-92fe-ff8310afde20'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tiền thuế GTGT', IsReadOnly = 0 WHERE ID = '9fa80f57-1d61-48d0-b256-ff8fc1d32714'

INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8ac99902-4778-41ca-8bca-0082f8f3fb0a', '23775f08-4da3-4a50-836c-c9fa0cd858cd', N'AccountingObjectName', N'Nhà cung cấp', N'', 0, 0, 261, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a99d6040-2bb1-40ed-af9c-009438313405', '08165530-e307-4e72-a716-f0d2272832e5', N'PrepaidExpenseID', N'Mã CP trả trước', NULL, 1, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3d4ed8c9-6233-4c80-9d01-0457968bb248', 'd4249611-64ea-4476-82d1-5312810b8afc', N'AccumulatedAllocateAmount', N'Chi phí đã lũy kế', NULL, 0, 0, 145, 0, 0, 1, 10)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('46fabcb9-43c8-44d3-a49c-0bd2bda4f136', '98a80e46-13bd-4134-a896-7fdb61feda61', N'Date', N'Ngày chứng từ', N'', 0, 0, 100, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('add8715b-eb7e-4dc2-a575-0f8afe07d4bb', 'd4249611-64ea-4476-82d1-5312810b8afc', N'AccountingObjectID', N'Đối tượng', N'', 0, 0, 245, 0, 0, 0, 11)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('523a16d8-9585-4a73-8d26-119e1cdd5d15', '0d531a1c-82bd-48b8-926d-b4a6da6f749e', N'AccumulatedAllocateAmount', N'Chi phí đã lũy kế', NULL, 0, 0, 145, 0, 0, 1, 10)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('61f879dd-f328-460a-967b-129c9416ad30', '23775f08-4da3-4a50-836c-c9fa0cd858cd', N'PostedDate', N'Ngày hạch toán', N'', 0, 0, 103, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('efdab792-3bad-4d90-90ed-1301f46f6e4a', '08165530-e307-4e72-a716-f0d2272832e5', N'CostAccount', N'TK chi phí', NULL, 0, 0, 110, 0, 0, 1, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d256eda9-7a64-449f-a4a3-149c859ba66c', 'd4249611-64ea-4476-82d1-5312810b8afc', N'AccumulatedAllocateAmountOriginal', N'Chi phí đã lũy kế', N'', 0, 0, 145, 0, 0, 0, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c6a0b98e-3974-4594-9c5c-165c09053f2c', '98a80e46-13bd-4134-a896-7fdb61feda61', N'AccountingObjectName', N'Nhà cung cấp', N'', 0, 0, 261, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e035a9eb-b35a-42a7-87fe-1705d1bbacc9', '9900370d-13d7-4cef-a7f6-b39718973c0e', N'AmountPBOriginal', N'Chi phí phân bổ lần này', N'', 0, 0, 145, 0, 0, 0, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3ca9c611-8a57-463f-8053-1934e0aa40e7', 'd4249611-64ea-4476-82d1-5312810b8afc', N'AmountPB', N'Chi phí phân bổ lần này', NULL, 0, 0, 145, 0, 0, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9359b678-33bf-4d37-804f-1de385c90179', '9900370d-13d7-4cef-a7f6-b39718973c0e', N'AmountPB', N'Chi phí phân bổ lần này', NULL, 0, 0, 145, 0, 0, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a6cea09c-6a6b-4682-9c79-20870ffb35a4', '0d531a1c-82bd-48b8-926d-b4a6da6f749e', N'AccumulatedAllocateAmountOriginal', N'Chi phí đã lũy kế', N'', 0, 0, 145, 0, 0, 0, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('dcd57163-1d93-43fb-a884-21177157e050', 'c1dc0dd0-ec4b-49ac-ac8d-259c53feef2a', N'PrepaidExpenseName', N'Tên CP trả trước', NULL, 0, 1, 130, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ee8ad9ec-aa0c-4e2d-8fc1-23b4df2fca79', 'd4249611-64ea-4476-82d1-5312810b8afc', N'No', N'Số chứng từ', N'', 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9897e249-d767-46b3-98d7-23cb85ad0418', 'd4249611-64ea-4476-82d1-5312810b8afc', N'TotalFreightAmountOriginal', N'Tổng chi phí mua', N'', 0, 0, 145, 0, 0, 0, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('476b5df6-e058-42a6-8d3b-272baa3045f7', '59b3b657-0cc0-4385-9ab1-af399b009e40', N'ImportTaxExpenseAmount', N'Phí trước hải quan', N'Phí trước hải quan', 0, 0, 115, 0, 0, 0, 35)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('431c2f4d-08db-4048-afcf-27544eb87444', '9900370d-13d7-4cef-a7f6-b39718973c0e', N'TotalFreightAmountOriginal', N'Tổng chi phí mua', N'', 0, 0, 145, 0, 0, 0, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e00eb894-b721-48a1-818a-29a0d8d79499', '23775f08-4da3-4a50-836c-c9fa0cd858cd', N'No', N'Số chứng từ', N'', 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('57e52e47-538c-4995-953f-2da2467797d1', '23775f08-4da3-4a50-836c-c9fa0cd858cd', N'AmountPBOriginal', N'Chi phí phân bổ lần này', N'', 0, 0, 145, 0, 0, 0, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('96819147-9c89-46a6-aaef-2eb9d936329e', '47cbf57b-e96c-42e4-a8f6-d0535922b3fc', N'AmountPB', N'Chi phí phân bổ lần này', NULL, 0, 0, 145, 0, 0, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('76bfc1f3-9af5-46e8-92c8-30423197d81e', '23775f08-4da3-4a50-836c-c9fa0cd858cd', N'TotalFreightAmountOriginal', N'Tổng chi phí mua', N'', 0, 0, 145, 0, 0, 0, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9632e18c-aaa5-4243-a7e9-32a72a4380ce', 'd570e478-b4f6-477d-a391-681cc0f26820', N'ExportTaxAccountCorresponding', N'TK đối ứng thuế XK', NULL, 0, 0, 100, 0, 0, 0, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a8b269fe-633d-4d3a-9886-39e124b67508', '47cbf57b-e96c-42e4-a8f6-d0535922b3fc', N'AccountingObjectName', N'Nhà cung cấp', N'', 0, 0, 261, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('34a6dc21-9260-4f5f-92ef-3ddf0e684136', '47cbf57b-e96c-42e4-a8f6-d0535922b3fc', N'No', N'Số chứng từ', N'', 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ff09214f-759e-48cc-842a-3e0eadcb741b', '23775f08-4da3-4a50-836c-c9fa0cd858cd', N'AccountingObjectID', N'Đối tượng', N'', 0, 0, 245, 0, 0, 0, 11)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('dd40be74-a3e1-4e0e-b20c-405e78b5a7d3', '9900370d-13d7-4cef-a7f6-b39718973c0e', N'AccumulatedAllocateAmount', N'Chi phí đã lũy kế', NULL, 0, 0, 145, 0, 0, 1, 10)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('23b92e43-2ab5-446d-a59a-4062234f3956', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'DebitAccountingObjectID', N'Đối tượng Nợ', N'', 0, 0, 105, 105, 105, 1, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('41d4f10c-4433-4907-b58d-407b4d9ceaff', '9900370d-13d7-4cef-a7f6-b39718973c0e', N'TotalFreightAmount', N'Tổng chi phí mua', NULL, 0, 0, 145, 0, 0, 1, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fec4be28-7560-4183-aba1-41668df828b8', '08165530-e307-4e72-a716-f0d2272832e5', N'PrepaidExpenseName', N'Tên CP trả trước', NULL, 1, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('08ae6459-2ad8-4a18-9499-417244c38520', '893f18ac-5ef9-4172-9760-f8ebb55d4090', N'ImportTaxExpenseAmount', N'Phí trước hải quan', N'Phí trước hải quan', 0, 0, 115, 0, 0, 0, 57)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4ae37477-765d-4d9d-beab-4352ce4ff97d', 'd4249611-64ea-4476-82d1-5312810b8afc', N'TotalFreightAmount', N'Tổng chi phí mua', NULL, 0, 0, 145, 0, 0, 1, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b329555e-fd95-46f6-93fa-4452aefc21ab', '98a80e46-13bd-4134-a896-7fdb61feda61', N'AccountingObjectID', N'Đối tượng', N'', 0, 0, 245, 0, 0, 0, 11)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bb479a0e-f01a-4a2d-8789-44c6725a16bb', '23775f08-4da3-4a50-836c-c9fa0cd858cd', N'AccumulatedAllocateAmount', N'Chi phí đã lũy kế', NULL, 0, 0, 145, 0, 0, 1, 10)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b0f1342c-e316-4326-9b77-474f3588600e', '47cbf57b-e96c-42e4-a8f6-d0535922b3fc', N'AccountingObjectID', N'Đối tượng', N'', 0, 0, 245, 0, 0, 0, 11)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7a322353-0886-481b-921b-4a333a522c0c', '98a80e46-13bd-4134-a896-7fdb61feda61', N'No', N'Số chứng từ', N'', 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c6ad1f99-394e-4bbe-8adb-4ca11e34fbaf', '98a80e46-13bd-4134-a896-7fdb61feda61', N'AmountPB', N'Chi phí phân bổ lần này', NULL, 0, 0, 145, 0, 0, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4102623f-0e1c-4856-8bc7-4ff74f13d9f5', 'd3bd03cb-d7f6-43b6-bb7c-0189cf524d21', N'ImportTaxExpenseAmount', N'Phí trước hải quan', N'Phí trước hải quan', 0, 0, 115, 0, 0, 0, 35)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6c24db71-8eec-425f-b6c3-505c9e23078e', '0d531a1c-82bd-48b8-926d-b4a6da6f749e', N'No', N'Số chứng từ', N'', 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('40657871-57b1-494e-a1b6-50ef7ad1bd0a', '20531b2c-206d-42ba-9635-83a3a6d8213d', N'ImportTaxExpenseAmountOriginal', N'Phí trước hải quan', N'Phí trước hải quan', 0, 0, 115, 0, 0, 0, 57)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('429bf3d6-dcf4-4a6d-97a1-593649793f01', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'DebitAccount', N'TK Nợ', N'Tài khoản Nợ', 1, 0, 95, 95, 95, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a8c443a4-f2ae-4bbe-b0bc-59c3000e0a4c', '47cbf57b-e96c-42e4-a8f6-d0535922b3fc', N'AccumulatedAllocateAmountOriginal', N'Chi phí đã lũy kế', N'', 0, 0, 145, 0, 0, 0, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9080a931-d23b-4f2f-a637-5ef39c14d9e6', '08165530-e307-4e72-a716-f0d2272832e5', N'ExpenseItemID', N'Khoản mục CP', NULL, 0, 0, 110, 0, 0, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2037f3cc-5ea0-4a2e-bbf6-671b57c0f559', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'Description', N'Diễn giải', N'', 1, 0, 250, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4924ee81-2816-4c35-b018-678cf18420d4', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'EmployeeID', N'Nhân viên', N'', 0, 0, 105, 105, 105, 1, 13)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8cf47798-655a-4da1-a3d2-6d87fdbcadf7', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'CostSetID', N'ĐT tập hợp CP', N'Đối tượng tập hợp chi phí', 0, 0, 150, 150, 150, 1, 17)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5995a529-7a42-4d06-8958-6db3722d086f', '0d531a1c-82bd-48b8-926d-b4a6da6f749e', N'AccountingObjectID', N'Đối tượng', N'', 0, 0, 245, 0, 0, 0, 11)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2686132b-ce61-4931-b42c-6fdf8f5beb17', '08165530-e307-4e72-a716-f0d2272832e5', N'Amount', N'Số tiền', NULL, 0, 1, 110, 0, 0, 1, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a5de1d81-e46c-4259-bd67-71ab65cf30c3', '47cbf57b-e96c-42e4-a8f6-d0535922b3fc', N'AccumulatedAllocateAmount', N'Chi phí đã lũy kế', NULL, 0, 0, 145, 0, 0, 1, 10)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('38504584-d0fd-4a8a-a7fb-72d7c2de2d9a', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'CustomProperty3', N'Cột 3', N'', 0, 0, 98, 98, 98, 0, 23)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('96f80a72-a336-4237-9654-72f4fc425767', 'c48c8288-1140-4378-bc0f-93162e902164', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 17)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0826b864-a497-451b-8538-76a13751979f', 'd2605109-637b-4751-9a1d-b0d743a10333', N'ImportTaxExpenseAmount', N'Phí trước hải quan', N'Phí trước hải quan', 0, 0, 115, 0, 0, 0, 56)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a467082d-60b5-49ea-a007-76b3b4c4a8d7', 'c1dc0dd0-ec4b-49ac-ac8d-259c53feef2a', N'RemainingAmount', N'Số tiền chưa PB', NULL, 0, 1, 130, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('03912f4d-8bf4-40be-9ab6-799737343f7e', 'c1dc0dd0-ec4b-49ac-ac8d-259c53feef2a', N'PrepaidExpenseID', N'Mã CP trả trước', NULL, 0, 1, 130, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('29477685-d010-4f6a-bc25-7bacdf221d94', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'DepartmentID', N'Phòng ban', N'', 0, 0, 150, 0, 0, 1, 16)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7b28753a-94eb-4cf3-ad0b-7fe50665fea9', '0d531a1c-82bd-48b8-926d-b4a6da6f749e', N'AccountingObjectName', N'Nhà cung cấp', N'', 0, 0, 261, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('886917e5-d355-4024-af01-807449d66ad4', 'c1dc0dd0-ec4b-49ac-ac8d-259c53feef2a', N'AllocationAmount', N'Số tiền PB kỳ này', NULL, 0, 1, 100, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('cfee2a96-5b14-46bb-ae47-8140582f2ec1', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'BankAccountDetailID', N'TK ngân hàng', N'Tài khoản ngân hàng', 1, 0, 95, 0, 0, 0, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6781b964-5822-4739-8fdc-81abf58be20f', '21bf5f6f-da51-47d1-9581-071ed46e2ff2', N'IsPromotion', N'Là hàng KM', N'Tích chọn nếu là hàng khuyến mại', 0, 0, 100, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('cb433f40-06c9-4d4b-bb8a-83702a6b977a', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'ExchangeRate', N'Tỷ giá', N'', 0, 0, 101, 0, 0, 0, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f29a6e9f-ab65-48c2-873e-85494a056e1d', '9900370d-13d7-4cef-a7f6-b39718973c0e', N'AccountingObjectName', N'Nhà cung cấp', N'', 0, 0, 261, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9dcfeee7-ad64-43d9-b542-868cd3e3c1a1', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'IsIrrationalCost', N'CP không hợp lý', N'Chi phí không hợp lý', 0, 0, 100, 0, 0, 0, 19)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('742ee252-ae82-40b5-8f98-8812d436fec2', '23775f08-4da3-4a50-836c-c9fa0cd858cd', N'AmountPB', N'Chi phí phân bổ lần này', NULL, 0, 0, 145, 0, 0, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('07f6bec3-a4e8-49ed-8c1a-8c19bc33c90d', '0d531a1c-82bd-48b8-926d-b4a6da6f749e', N'AmountPBOriginal', N'Chi phí phân bổ lần này', N'', 0, 0, 145, 0, 0, 0, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2998acd9-baa3-47d7-81b6-8cb59631abd2', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'AmountOriginal', N'Số tiền', N'', 0, 0, 120, 120, 120, 0, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bb3f4cc7-9356-4a23-98d5-8d13fd1c329d', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'CustomProperty2', N'Cột 2', N'', 0, 0, 98, 98, 98, 0, 22)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('beb3c4d8-94f8-4265-a84d-8f5f847c13e2', '47cbf57b-e96c-42e4-a8f6-d0535922b3fc', N'PostedDate', N'Ngày hạch toán', N'', 0, 0, 103, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('731b1385-2428-4063-8da6-931c54ddd985', '98a80e46-13bd-4134-a896-7fdb61feda61', N'PostedDate', N'Ngày hạch toán', N'', 0, 0, 103, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b02404d0-e612-4cb7-bf8b-94795230a119', '08165530-e307-4e72-a716-f0d2272832e5', N'AllocationObjectName', N'Tên đối tượng PB', NULL, 1, 0, 110, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('cebb5544-15d1-4436-885b-94a3e278b023', '08165530-e307-4e72-a716-f0d2272832e5', N'AllocationAmount', N'Số tiền PB kỳ này', NULL, 1, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8f31d1d4-f913-4ebf-8e0e-9eeba29a0f04', '23775f08-4da3-4a50-836c-c9fa0cd858cd', N'Date', N'Ngày chứng từ', N'', 0, 0, 100, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('95948bdd-f220-41ad-846f-a060961b6b9a', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'CreditAccountingObjectID', N'Đối tượng Có', N'', 0, 0, 105, 105, 105, 1, 11)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c69768de-98c9-45fa-8cbc-a094af960db0', '0d531a1c-82bd-48b8-926d-b4a6da6f749e', N'TotalFreightAmountOriginal', N'Tổng chi phí mua', N'', 0, 0, 145, 0, 0, 0, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bf03cf62-fbb9-4504-81b9-a0a02f59036f', '9900370d-13d7-4cef-a7f6-b39718973c0e', N'No', N'Số chứng từ', N'', 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('152ea172-c040-48ff-b022-a6abca211b4b', '625ec698-4f75-4e77-8619-4983751c1568', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 17)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d5d08a72-909e-4d1e-aaa9-a77fba3f2662', '98a80e46-13bd-4134-a896-7fdb61feda61', N'AccumulatedAllocateAmountOriginal', N'Chi phí đã lũy kế', N'', 0, 0, 145, 0, 0, 0, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7a369bf3-c83a-4d92-af8b-a79445113d09', 'd4249611-64ea-4476-82d1-5312810b8afc', N'AccountingObjectName', N'Nhà cung cấp', N'', 0, 0, 261, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e190929b-2e3a-4d37-b164-a8694a8c1675', '08165530-e307-4e72-a716-f0d2272832e5', N'AllocationRate', N'Tỷ lệ PB', NULL, 0, 0, 110, 0, 0, 1, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ee399772-e879-4895-a8cb-ab3204d73e52', '747265e0-ebd9-4e07-8279-1149550fcebd', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 17)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('72bb5cb3-614c-471c-926d-ab37f15290f4', '9900370d-13d7-4cef-a7f6-b39718973c0e', N'Date', N'Ngày chứng từ', N'', 0, 0, 100, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('75716d7f-dd3b-49e8-90de-abeb466069d9', '47cbf57b-e96c-42e4-a8f6-d0535922b3fc', N'TotalFreightAmountOriginal', N'Tổng chi phí mua', N'', 0, 0, 145, 0, 0, 0, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d07a2d8f-b76f-4d4b-8753-ad508c49334c', '47cbf57b-e96c-42e4-a8f6-d0535922b3fc', N'AmountPBOriginal', N'Chi phí phân bổ lần này', N'', 0, 0, 145, 0, 0, 0, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('913a6c2b-9b93-4dea-9dec-ad90f8d52c66', '696f0072-3a6d-4402-96ec-45fd1d69e1f6', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 17)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('183e4fb5-1fa1-4a19-be6c-b3068daa9487', '9900370d-13d7-4cef-a7f6-b39718973c0e', N'PostedDate', N'Ngày hạch toán', N'', 0, 0, 103, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ad8eabfb-8f0c-40f3-99c8-b4715ab1dc39', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'StatisticsCodeID', N'Mã thống kê', N'', 0, 0, 105, 0, 0, 1, 20)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('198dc8ad-ae6f-4a6e-bd7b-b6f631b37ca8', '47cbf57b-e96c-42e4-a8f6-d0535922b3fc', N'TotalFreightAmount', N'Tổng chi phí mua', NULL, 0, 0, 145, 0, 0, 1, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('03b1dc7d-b5b8-4b0f-928f-bb4f3bb4f978', '0d531a1c-82bd-48b8-926d-b4a6da6f749e', N'AmountPB', N'Chi phí phân bổ lần này', NULL, 0, 0, 145, 0, 0, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f6746949-3178-4506-97a0-bc33767e78ef', 'd2605109-637b-4751-9a1d-b0d743a10333', N'ImportTaxExpenseAmountOriginal', N'Phí trước hải quan', N'Phí trước hải quan', 0, 0, 115, 0, 0, 0, 57)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9958ef0b-cc97-49d4-b3f6-bcd4b5cad4bb', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'BudgetItemID', N'Mục thu/chi', N'', 0, 0, 105, 105, 105, 1, 14)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('868930f5-c3a2-4e24-81c0-bced2afdec65', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'CustomProperty1', N'Cột 1', N'', 0, 0, 98, 98, 98, 0, 21)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c04f0b23-c5b1-4503-83c1-be8582f6e52c', '47cbf57b-e96c-42e4-a8f6-d0535922b3fc', N'Date', N'Ngày chứng từ', N'', 0, 0, 100, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d424736e-7580-4b17-9dd2-bf38a7d6fc99', '9900370d-13d7-4cef-a7f6-b39718973c0e', N'AccumulatedAllocateAmountOriginal', N'Chi phí đã lũy kế', N'', 0, 0, 145, 0, 0, 0, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('89de7449-8911-436a-838f-c96c4fd985e0', 'b44d72f8-5972-45f9-b590-cfec6c4fdc3e', N'ImportTaxExpenseAmount', N'Phí trước hải quan', NULL, 0, 0, 115, 0, 0, 0, 43)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fa9eeab5-186e-4520-9dc8-ca1e075ed996', '893f18ac-5ef9-4172-9760-f8ebb55d4090', N'ImportTaxExpenseAmountOriginal', N'Phí trước hải quan', N'Phí trước hải quan', 0, 0, 115, 0, 0, 0, 58)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('dbcf9c6a-3b69-43b3-952d-ce28e7e57ab2', '0d531a1c-82bd-48b8-926d-b4a6da6f749e', N'Date', N'Ngày chứng từ', N'', 0, 0, 100, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ddf13ee6-e670-4f11-bb4c-ce9e5eca2bf8', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'CurrencyID', N'Loại tiền', N'', 0, 0, 118, 0, 0, 0, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('77bf3085-28a5-4c55-9d1c-d03e79fc1267', '20531b2c-206d-42ba-9635-83a3a6d8213d', N'ImportTaxExpenseAmount', N'Phí trước hải quan', N'Phí trước hải quan', 0, 0, 115, 0, 0, 0, 56)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8c535dd1-7e78-470a-8ef2-d0721533b247', '23775f08-4da3-4a50-836c-c9fa0cd858cd', N'AccumulatedAllocateAmountOriginal', N'Chi phí đã lũy kế', N'', 0, 0, 145, 0, 0, 0, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9ffcf9e4-b492-485a-8a0a-d11d44082147', '23775f08-4da3-4a50-836c-c9fa0cd858cd', N'TotalFreightAmount', N'Tổng chi phí mua', NULL, 0, 0, 145, 0, 0, 1, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('81488f15-31b2-48e3-a821-d23a1db23972', 'd3bd03cb-d7f6-43b6-bb7c-0189cf524d21', N'ImportTaxExpenseAmountOriginal', N'Phí trước hải quan', N'Phí trước hải quan', 0, 0, 115, 0, 0, 0, 36)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('be91684d-31d2-4716-8b3f-d401994330cf', '08165530-e307-4e72-a716-f0d2272832e5', N'AllocationObjectID', N'Mã đối tượng PB', NULL, 1, 0, 110, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('41d9b831-4962-4aef-ac16-d664ed3fa7bb', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'Amount', N'Số tiền', N'', 0, 1, 120, 120, 120, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b4bcbbf7-5f64-41d8-ae78-d9d8ffea6c8f', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'DebitAccountingObjectName', N'Tên đối tượng Nợ', N'', 0, 0, 115, 0, 0, 1, 10)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f786b338-3390-4f56-8d17-dafcc1f48a5f', 'd4249611-64ea-4476-82d1-5312810b8afc', N'PostedDate', N'Ngày hạch toán', N'', 0, 0, 103, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6a96c629-2497-47d0-84d5-dd4af0d2c581', '98a80e46-13bd-4134-a896-7fdb61feda61', N'AmountPBOriginal', N'Chi phí phân bổ lần này', N'', 0, 0, 145, 0, 0, 0, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('176f3a22-672b-433e-9caa-df69276985a5', '98a80e46-13bd-4134-a896-7fdb61feda61', N'TotalFreightAmount', N'Tổng chi phí mua', NULL, 0, 0, 145, 0, 0, 1, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6cba3bf7-efb2-43d4-bfed-e2b4f55298b8', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'CreditAccountingObjectName', N'Tên đối tượng Có', N'', 0, 0, 115, 0, 0, 1, 12)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('aefb840c-f29a-4ec7-8d2c-e7b950377146', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'ExpenseItemID', N'Khoản mục CP', N'Khoản mục chi phí', 0, 0, 130, 0, 0, 1, 15)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('234f2d74-12b2-4269-904a-ec23a2cd3981', '98a80e46-13bd-4134-a896-7fdb61feda61', N'AccumulatedAllocateAmount', N'Chi phí đã lũy kế', NULL, 0, 0, 145, 0, 0, 1, 10)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f930fcd2-12d0-478a-98a9-ed111d1a699d', 'd4249611-64ea-4476-82d1-5312810b8afc', N'AmountPBOriginal', N'Chi phí phân bổ lần này', N'', 0, 0, 145, 0, 0, 0, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2ea52ae7-0e5e-46b0-b0bb-eda791320ed1', '1e5718a2-7e72-4196-bd42-a48d2cda082d', N'TaxExchangeRate', N'Tỷ giá tính thuế', N'', 0, 0, 105, 0, 0, 0, 16)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('027b6363-f58f-440a-be1f-f087e397b442', '0d531a1c-82bd-48b8-926d-b4a6da6f749e', N'PostedDate', N'Ngày hạch toán', N'', 0, 0, 103, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('19978982-c289-4b89-a5eb-f2bb9744c9be', 'd4249611-64ea-4476-82d1-5312810b8afc', N'Date', N'Ngày chứng từ', N'', 0, 0, 100, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7550a023-258c-4688-8366-f48779f64c45', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'CreditAccount', N'TK Có', N'Tài khoản Có', 1, 0, 95, 95, 95, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('944ba767-e8b8-44bf-813d-f690af8fb593', '98a80e46-13bd-4134-a896-7fdb61feda61', N'TotalFreightAmountOriginal', N'Tổng chi phí mua', N'', 0, 0, 145, 0, 0, 0, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5cab7b9a-afb1-442f-9ee9-f6d015044e36', 'c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', N'ContractID', N'Hợp đồng', N'', 0, 0, 105, 105, 105, 1, 18)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ebce6cd2-fc34-4700-acd9-f7d6436cf766', '59b3b657-0cc0-4385-9ab1-af399b009e40', N'ImportTaxExpenseAmountOriginal', N'Phí trước hải quan', N'Phí trước hải quan', 0, 0, 115, 0, 0, 0, 36)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1b5663bc-0b8f-488c-96b9-fb3ac53deab9', '9900370d-13d7-4cef-a7f6-b39718973c0e', N'AccountingObjectID', N'Đối tượng', N'', 0, 0, 245, 0, 0, 0, 11)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('60d0b090-2296-40bf-ac57-fc75ae79a9a1', '0d531a1c-82bd-48b8-926d-b4a6da6f749e', N'TotalFreightAmount', N'Tổng chi phí mua', NULL, 0, 0, 145, 0, 0, 1, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5d4e54fc-1d54-480a-852b-fdad1e38f069', 'c1dc0dd0-ec4b-49ac-ac8d-259c53feef2a', N'Amount', N'Số tiền', NULL, 0, 1, 130, 0, 0, 1, 2)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('c1dc0dd0-ec4b-49ac-ac8d-259c53feef2a', 'b5b01477-1001-40de-980e-616d5a6a2d70', 2, 0, N'&1. Chi tiết chi phí')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('08165530-e307-4e72-a716-f0d2272832e5', 'b5b01477-1001-40de-980e-616d5a6a2d70', 2, 1, N'&2. Chi tiết phân bổ')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('c1d88d69-6dec-4b56-a497-fcde9fe0e5d1', 'b5b01477-1001-40de-980e-616d5a6a2d70', 2, 2, N'&3. Hạch toán')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

SET IDENTITY_INSERT dbo.Type ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.Type(ID, TypeName, TypeGroupID, Recordable, Searchable, PostType, OrderPriority) VALUES (690, N'Phân bổ chi phí trả trước', 60, 1, 1, 1, 0)
GO
SET IDENTITY_INSERT dbo.Type OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.VoucherPatternsReport WHERE ID = 131
DELETE dbo.VoucherPatternsReport WHERE ID = 177

UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'110,111,112,113,114,115,118,119,902' WHERE ID = 103
UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'110,111,112,113,114,115,116,118' WHERE ID = 104
UPDATE dbo.VoucherPatternsReport SET VoucherPatternsName = N'Báo giá(Mẫu đầy đủ)' WHERE ID = 126
UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'132,142,119,129,172,500' WHERE ID = 134
UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'600,690' WHERE ID = 173
UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'110,111,112,113,114,115,118,119,902' WHERE ID = 194

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
SET IDENTITY_INSERT dbo.VoucherPatternsReport ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (209, 0, 0, N'FAIncrement-GBNUNC', N'Giấy báo nợ', N'FAIncrement-GBNUNC.rst', 1, N'129')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (210, 0, 0, N'FA_UNC_Agribank', N'Ủy nhiệm chi ngân hàng Agribank', N'FAIncrement_UNC_Agribank.rst', 1, N'129')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (211, 0, 0, N'FA_UNC_BIDV', N'Ủy nhiệm chi ngân hàng BIDV', N'FAIncrement_UNC_BIDV.rst', 1, N'129')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (212, 0, 0, N'FA_UNC_Techcombank', N'Ủy nhiệm chi ngân hàng Techcombank', N'FAIncrement_UNC_Techcombank.rst', 1, N'129')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (213, 0, 0, N'FA_UNC_Vietcombank', N'Ủy nhiệm chi ngân hàng Vietcombank', N'FAIncrement_UNC_Vietcombank.rst', 1, N'129')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (214, 0, 0, N'FA_UNC_Vietinbank', N'Ủy nhiệm chi ngân hàng Vietinbank', N'FAIncrement_UNC_Vietinbank.rst', 1, N'129')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (215, 0, 0, N'TIIncrement-GBNUNC', N'Giấy báo nợ', N'TIIncrement-GBNUNC.rst', 1, N'903')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (217, 0, 0, N'TI_UNC_Agribank', N'Ủy nhiệm chi ngân hàng Agribank', N'TIIncrement_UNC_Agribank.rst', 1, N'903')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (218, 0, 0, N'TI_UNC_BIDV', N'Ủy nhiệm chi ngân hàng BIDV', N'TIIncrement_UNC_BIDV.rst', 1, N'903')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (219, 0, 0, N'TI_UNC_Techcombank', N'Ủy nhiệm chi ngân hàng Techcombank', N'TIIncrement_UNC_Techcombank.rst', 1, N'903')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (220, 0, 0, N'TI_UNC_Vietcombank', N'Ủy nhiệm chi ngân hàng Vietcombank', N'TIIncrement_UNC_Vietcombank.rst', 1, N'903')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (221, 0, 0, N'TI_UNC_Vietinbank', N'Ủy nhiệm chi ngân hàng Vietinbank', N'TIIncrement_UNC_Vietinbank.rst', 1, N'903')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (222, 0, 0, N'SAQuote_BG_1', N'Báo giá(Mẫu chuẩn)', N'SAQuote_BG_1.rst', 1, N'300')
GO
SET IDENTITY_INSERT dbo.VoucherPatternsReport OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DBCC CHECKIDENT('dbo.VoucherPatternsReport', RESEED, 222)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Template WITH NOCHECK
  ADD CONSTRAINT FK_Template_Type FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateDetail WITH NOCHECK
  ADD CONSTRAINT FK_TemplateDetail_Template FOREIGN KEY (TemplateID) REFERENCES dbo.Template(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Type WITH NOCHECK
  ADD CONSTRAINT FK_Type_TypeGroup FOREIGN KEY (TypeGroupID) REFERENCES dbo.TypeGroup(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153AdjustAnnouncement WITH NOCHECK
  ADD CONSTRAINT FK__TT153Adju__TypeI__25869641 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153DeletedInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Dele__TypeI__63D8CE75 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153DestructionInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Dest__TypeI__22AA2996 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153LostInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Lost__TypeI__1FCDBCEB FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153PublishInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Publ__TypeI__1CF15040 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153RegisterInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Regi__TypeI__1A14E395 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

Update Account Set DetailByAccountObject = 1 where AccountNumber in ('131', '141', '331');

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF
GO
