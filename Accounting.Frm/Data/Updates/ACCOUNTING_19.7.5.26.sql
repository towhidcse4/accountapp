SET CONCAT_NULL_YIELDS_NULL, ANSI_NULLS, ANSI_PADDING, QUOTED_IDENTIFIER, ANSI_WARNINGS, ARITHABORT, XACT_ABORT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS OFF
GO

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO

CREATE TABLE [dbo].[RefVoucherRSInwardOutward] (
  [ID] [uniqueidentifier] NOT NULL,
  [RefID1] [uniqueidentifier] NULL DEFAULT (NULL),
  [RefID2] [uniqueidentifier] NULL DEFAULT (NULL),
  [No] [nvarchar](512) NULL DEFAULT (NULL),
  [Date] [datetime] NULL DEFAULT (NULL),
  [PostedDate] [datetime] NULL DEFAULT (NULL),
  [Reason] [nvarchar](512) NULL DEFAULT (NULL),
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[RSAssemblyDismantlement]
  ADD [TypeVoucher] [int] NULL DEFAULT (NULL)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[RSAssemblyDismantlement]
  ADD [OriginalVoucher] [nvarchar](4000) NULL DEFAULT (NULL)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'RSAssemblyDismantlement', 'COLUMN', N'TypeVoucher'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'RSAssemblyDismantlement', 'COLUMN', N'OriginalVoucher'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[RefVoucher]
  ADD [No] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[RefVoucher]
  ADD [Date] [datetime] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[RefVoucher]
  ADD [PostedDate] [datetime] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[RefVoucher]
  ADD [Reason] [nvarchar](512) NULL DEFAULT (NULL)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'RefVoucher', 'COLUMN', N'No'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'RefVoucher', 'COLUMN', N'Date'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'RefVoucher', 'COLUMN', N'PostedDate'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'RefVoucher', 'COLUMN', N'Reason'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheet]
  ADD [WorkDayInMonth] [decimal](18, 2) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GVoucherListDetail]
  ADD [GeneralLedgerID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE PROCEDURE [dbo].[Proc_SoDangKyChungTuGhiSoS02bDNN]
    @FromDate DATETIME,
    @ToDate DATETIME
AS
BEGIN     
	DECLARE @tblS02bDNN	 TABLE(
			SoHieu NVARCHAR(MAX),	
			NgayChungTu Date,
			SoTien decimal(25,0),
			Thang int,
			Quy NCHAR(10),
			Nam int)

                              			
		INSERT INTO @tblS02bDNN
		SELECT a.No,CONVERT(Date,Date,103) as NgayThang, TotalAmount as SoTien,MONTH(Date), 
		(Case  when MONTH(Date) >0 AND MONTH(Date)<=3 then 'I'
				when MONTH(Date) >3 AND MONTH(Date)<=6 then 'II'
				when MONTH(Date) >6 AND MONTH(Date)<=9 then 'III'
				when MONTH(Date) >9 AND MONTH(Date)<=12 then 'IV'
		END) as Quy, YEAR(Date)
		FROM [GvoucherList] a 
		where Date > = @FromDate and Date < = @ToDate
		order by Date

		Select * from @tblS02bDNN order by NgayChungTu
		
END
/**/



GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GOtherVoucherDetailDebtPayment]
  ADD [Date] [datetime] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[FAIncrement]', N'tmp_devart_FAIncrement', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename table [dbo].[FAIncrement] to [dbo].[tmp_devart_FAIncrement]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[PK_FAIncrement]', N'tmp_devart_PK_FAIncrement', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename index [PK_FAIncrement] to [tmp_devart_PK_FAIncrement] on table [dbo].[FAIncrement]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[DF_IsMatch_FAIncrement]', N'tmp_devart_DF_IsMatch_FAIncrement', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename default [DF_IsMatch_FAIncrement] to [tmp_devart_DF_IsMatch_FAIncrement] on table [dbo].[FAIncrement]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[FAIncrement] (
  [ID] [uniqueidentifier] NOT NULL,
  [BranchID] [uniqueidentifier] NULL,
  [TypeID] [int] NOT NULL,
  [PostedDate] [datetime] NOT NULL,
  [Date] [datetime] NOT NULL,
  [No] [nvarchar](25) NOT NULL,
  [DueDate] [datetime] NULL,
  [CreditCardNumber] [nvarchar](25) NULL,
  [BankAccountDetailID] [uniqueidentifier] NULL,
  [BankName] [nvarchar](512) NULL,
  [AccountingObjectID] [uniqueidentifier] NULL,
  [AccountingObjectName] [nvarchar](512) NULL,
  [AccountingObjectAddress] [nvarchar](512) NULL,
  [IdentificationNo] [nvarchar](25) NULL,
  [IssueDate] [datetime] NULL,
  [IssueBy] [nvarchar](512) NULL,
  [TaxCode] [nvarchar](50) NULL,
  [AccountingObjectBankAccount] [uniqueidentifier] NULL,
  [AccountingObjectBankName] [nvarchar](512) NULL,
  [Reason] [nvarchar](512) NULL,
  [NumberAttach] [nvarchar](512) NULL,
  [IsImportPurchase] [bit] NULL,
  [IsSpecialConsumeTax] [bit] NULL,
  [TotalAmount] [money] NULL,
  [TotalAmountOriginal] [money] NULL,
  [TotalVATAmount] [money] NULL,
  [TotalVATAmountOriginal] [money] NULL,
  [TotalDiscountAmount] [money] NULL,
  [TotalDiscountAmountOriginal] [money] NULL,
  [TotalFreightAmount] [money] NULL,
  [TotalFreightAmountOriginal] [money] NULL,
  [TotalOrgPrice] [money] NULL,
  [TotalOrgPriceOriginal] [money] NULL,
  [IsMatch] [bit] NOT NULL CONSTRAINT [DF_IsMatch_FAIncrement] DEFAULT (0),
  [MatchDate] [datetime] NULL,
  [CurrencyID] [nvarchar](3) NULL,
  [ExchangeRate] [decimal](25, 10) NULL,
  [TransportMethodID] [uniqueidentifier] NULL,
  [PaymentClauseID] [uniqueidentifier] NULL,
  [IsVATPaid] [bit] NULL,
  [CustomProperty1] [nvarchar](512) NULL,
  [CustomProperty2] [nvarchar](512) NULL,
  [CustomProperty3] [nvarchar](512) NULL,
  [TemplateID] [uniqueidentifier] NULL,
  [Recorded] [bit] NOT NULL,
  [Exported] [bit] NOT NULL,
  [EmployeeID] [uniqueidentifier] NULL,
  [MContactName] [nvarchar](512) NULL,
  [ExpenditureAccount] [nvarchar](512) NULL,
  [TotalSpecialConsumeTaxAmount] [money] NULL DEFAULT (0),
  [TotalSpecialConsumeTaxAmountOriginal] [money] NULL DEFAULT (0),
  [TotalImportTaxAmount] [money] NULL DEFAULT (0),
  [TotalImportTaxAmountOriginal] [money] NULL DEFAULT (0),
  [TotalAll] [money] NULL DEFAULT (NULL),
  [TotalAllOriginal] [money] NULL DEFAULT (NULL),
  [TypeCT] [nvarchar](1) NULL DEFAULT (NULL),
  [OriginalVoucher] [nvarchar](4000) NULL DEFAULT (NULL)
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

INSERT [dbo].[FAIncrement](ID, BranchID, TypeID, PostedDate, Date, No, DueDate, CreditCardNumber, BankAccountDetailID, BankName, AccountingObjectID, AccountingObjectName, AccountingObjectAddress, IdentificationNo, IssueDate, IssueBy, TaxCode, AccountingObjectBankAccount, AccountingObjectBankName, Reason, NumberAttach, IsImportPurchase, IsSpecialConsumeTax, TotalAmount, TotalAmountOriginal, TotalVATAmount, TotalVATAmountOriginal, TotalDiscountAmount, TotalDiscountAmountOriginal, TotalFreightAmount, TotalFreightAmountOriginal, TotalOrgPrice, TotalOrgPriceOriginal, IsMatch, MatchDate, CurrencyID, ExchangeRate, TransportMethodID, PaymentClauseID, IsVATPaid, CustomProperty1, CustomProperty2, CustomProperty3, TemplateID, Recorded, Exported, EmployeeID, MContactName, ExpenditureAccount, TotalSpecialConsumeTaxAmount, TotalSpecialConsumeTaxAmountOriginal, TotalImportTaxAmount, TotalImportTaxAmountOriginal, OriginalVoucher, TotalAll, TotalAllOriginal)
  SELECT ID, BranchID, TypeID, PostedDate, Date, No, DueDate, CreditCardNumber, BankAccountDetailID, BankName, AccountingObjectID, AccountingObjectName, AccountingObjectAddress, IdentificationNo, IssueDate, IssueBy, TaxCode, AccountingObjectBankAccount, AccountingObjectBankName, Reason, NumberAttach, IsImportPurchase, IsSpecialConsumeTax, TotalAmount, TotalAmountOriginal, TotalVATAmount, TotalVATAmountOriginal, TotalDiscountAmount, TotalDiscountAmountOriginal, TotalFreightAmount, TotalFreightAmountOriginal, TotalOrgPrice, TotalOrgPriceOriginal, IsMatch, MatchDate, CurrencyID, ExchangeRate, TransportMethodID, PaymentClauseID, IsVATPaid, CustomProperty1, CustomProperty2, CustomProperty3, TemplateID, Recorded, Exported, EmployeeID, MContactName, ExpenditureAccount, TotalSpecialConsumeTaxAmount, TotalSpecialConsumeTaxAmountOriginal, TotalImportTaxAmount, TotalImportTaxAmountOriginal, OriginalVoucher, TotalAll, TotalAllOriginal FROM [dbo].[tmp_devart_FAIncrement] WITH (NOLOCK)

if @@ERROR = 0
  DROP TABLE [dbo].[tmp_devart_FAIncrement]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[FAIncrement]
  ADD CONSTRAINT [PK_FAIncrement] PRIMARY KEY CLUSTERED ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'FAIncrement', 'COLUMN', N'OriginalVoucher'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'FAIncrement', 'COLUMN', N'TypeCT'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'FAIncrement', 'COLUMN', N'TotalAllOriginal'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'FAIncrement', 'COLUMN', N'TotalAll'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO


ALTER VIEW [dbo].[ViewVoucherInvisible]
AS
SELECT        newid() AS ID, dbo.MCPayment.ID AS RefID, dbo.MCPayment.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.MCPayment.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.MCPayment.Date, 
                         dbo.MCPayment.PostedDate, dbo.MCPayment.CurrencyID, dbo.MCPayment.Reason, dbo.MCPayment.EmployeeID, dbo.MCPayment.BranchID, dbo.MCPayment.Recorded, dbo.MCPayment.TotalAmount, 
                         dbo.MCPayment.TotalAmountOriginal, 'MCPayment' AS RefTable
FROM            dbo.MCPayment INNER JOIN
                         dbo.Type ON MCPayment.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.MBDeposit.ID AS RefID, dbo.MBDeposit.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.MBDeposit.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.MBDeposit.Date, dbo.MBDeposit.PostedDate, 
                         dbo.MBDeposit.CurrencyID, dbo.MBDeposit.Reason, dbo.MBDeposit.EmployeeID, dbo.MBDeposit.BranchID, dbo.MBDeposit.Recorded, dbo.MBDeposit.TotalAmount, dbo.MBDeposit.TotalAmountOriginal, 
                         'MBDeposit' AS RefTable
FROM            dbo.MBDeposit INNER JOIN
                         dbo.Type ON MBDeposit.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.MBInternalTransfer.ID AS RefID, dbo.MBInternalTransfer.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.MBInternalTransfer.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, 
                         dbo.MBInternalTransfer.Date, dbo.MBInternalTransfer.PostedDate, '' AS CurrencyID, dbo.MBInternalTransfer.Reason, NULL AS EmployeeID, dbo.MBInternalTransfer.BranchID, dbo.MBInternalTransfer.Recorded, 
                         dbo.MBInternalTransfer.TotalAmountOriginal, dbo.MBInternalTransfer.TotalAmount, 'MBInternalTransfer' AS RefTable
FROM            dbo.MBInternalTransfer INNER JOIN
                         dbo.Type ON dbo.MBInternalTransfer.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.MBTellerPaper.ID AS RefID, dbo.MBTellerPaper.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.MBTellerPaper.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.MBTellerPaper.Date, 
                         dbo.MBTellerPaper.PostedDate, dbo.MBTellerPaper.CurrencyID, dbo.MBTellerPaper.Reason, dbo.MBTellerPaper.EmployeeID, dbo.MBTellerPaper.BranchID, dbo.MBTellerPaper.Recorded, 
                         dbo.MBTellerPaper.TotalAmountOriginal, dbo.MBTellerPaper.TotalAmount, 'MBTellerPaper' AS RefTable
FROM            dbo.MBTellerPaper INNER JOIN
                         dbo.Type ON dbo.MBTellerPaper.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.MBCreditCard.ID AS RefID, dbo.MBCreditCard.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.MBCreditCard.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.MBCreditCard.Date, 
                         dbo.MBCreditCard.PostedDate, dbo.MBCreditCard.CurrencyID, dbo.MBCreditCard.Reason, dbo.MBCreditCard.EmployeeID, dbo.MBCreditCard.BranchID, dbo.MBCreditCard.Recorded, dbo.MBCreditCard.TotalAmountOriginal, 
                         dbo.MBCreditCard.TotalAmount, 'MBCreditCard' AS RefTable
FROM            dbo.MBCreditCard INNER JOIN
                         dbo.Type ON dbo.MBCreditCard.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.MCReceipt.ID AS RefID, dbo.MCReceipt.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.MCReceipt.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.MCReceipt.Date, dbo.MCReceipt.PostedDate, 
                         dbo.MCReceipt.CurrencyID, dbo.MCReceipt.Reason, dbo.MCReceipt.EmployeeID, dbo.MCReceipt.BranchID, dbo.MCReceipt.Recorded, dbo.MCReceipt.TotalAmountOriginal, dbo.MCReceipt.TotalAmount, 
                         'MCReceipt' AS RefTable
FROM            dbo.MCReceipt INNER JOIN
                         dbo.Type ON MCReceipt.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.FAAdjustment.ID AS RefID, dbo.FAAdjustment.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.FAAdjustment.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.FAAdjustment.Date, 
                         dbo.FAAdjustment.PostedDate, NULL AS CurrencyID, dbo.FAAdjustment.Reason, NULL AS EmployeeID, dbo.FAAdjustment.BranchID, dbo.FAAdjustment.Recorded, $ 0 AS TotalAmountOriginal, dbo.FAAdjustment.TotalAmount, 
                         'FAAdjustment' AS RefTable
FROM            dbo.FAAdjustment INNER JOIN
                         dbo.Type ON FAAdjustment.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.FADepreciation.ID AS RefID, dbo.FADepreciation.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.FADepreciation.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.FADepreciation.Date, 
                         dbo.FADepreciation.PostedDate, NULL AS CurrencyID, dbo.FADepreciation.Reason, NULL AS EmployeeID, dbo.FADepreciation.BranchID, dbo.FADepreciation.Recorded, dbo.FADepreciation.TotalAmountOriginal, 
                         dbo.FADepreciation.TotalAmount, 'FADepreciation' AS RefTable
FROM            dbo.FADepreciation INNER JOIN
                         dbo.Type ON FADepreciation.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.FAIncrement.ID AS RefID, dbo.FAIncrement.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.FAIncrement.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.FAIncrement.Date, 
                         dbo.FAIncrement.PostedDate, FAIncrement.CurrencyID, dbo.FAIncrement.Reason, dbo.FAIncrement.EmployeeID, dbo.FAIncrement.BranchID, dbo.FAIncrement.Recorded, dbo.FAIncrement.TotalAmountOriginal, 
                         dbo.FAIncrement.TotalAmount, 'FAIncrement' AS RefTable
FROM            dbo.FAIncrement INNER JOIN
                         dbo.Type ON FAIncrement.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.FADecrement.ID AS RefID, dbo.FADecrement.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.FADecrement.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.FADecrement.Date, 
                         dbo.FADecrement.PostedDate, FADecrement.CurrencyID, dbo.FADecrement.Reason, NULL AS EmployeeID, dbo.FADecrement.BranchID, dbo.FADecrement.Recorded, dbo.FADecrement.TotalAmountOriginal, 
                         dbo.FADecrement.TotalAmount, 'FADecrement' AS RefTable
FROM            dbo.FADecrement INNER JOIN
                         dbo.Type ON FADecrement.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.FAAudit.ID AS RefID, dbo.FAAudit.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.FAAudit.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.FAAudit.Date, dbo.FAAudit.PostedDate, NULL 
                         AS CurrencyID, dbo.FAAudit.Description AS Reason, NULL AS EmployeeID, dbo.FAAudit.BranchID, 0 AS Recorded, 0 AS TotalAmountOriginal, 0 AS TotalAmount, 'FAAudit' AS RefTable
FROM            dbo.FAAudit INNER JOIN
                         dbo.Type ON FAAudit.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.FATransfer.ID AS RefID, dbo.FATransfer.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.FATransfer.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.FATransfer.Date, 
                         dbo.FATransfer.Date AS PostedDate, NULL AS CurrencyID, dbo.FATransfer.Reason, NULL AS EmployeeID, dbo.FATransfer.BranchID, dbo.FATransfer.Recorded, 0 AS TotalAmountOriginal, 0 AS TotalAmount, 
                         'FATransfer' AS RefTable
FROM            dbo.FATransfer INNER JOIN
                         dbo.Type ON FATransfer.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.GOtherVoucher.ID AS RefID, dbo.GOtherVoucher.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.GOtherVoucher.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.GOtherVoucher.Date, 
                         dbo.GOtherVoucher.PostedDate, NULL AS CurrencyID, dbo.GOtherVoucher.Reason, NULL AS EmployeeID, dbo.GOtherVoucher.BranchID, dbo.GOtherVoucher.Recorded, dbo.GOtherVoucher.TotalAmountOriginal, 
                         dbo.GOtherVoucher.TotalAmount, 'GOtherVoucher' AS RefTable
FROM            dbo.GOtherVoucher INNER JOIN
                         dbo.Type ON GOtherVoucher.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.RSInwardOutward.ID AS RefID, dbo.RSInwardOutward.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.RSInwardOutward.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.RSInwardOutward.Date, 
                         dbo.RSInwardOutward.PostedDate, RSInwardOutward.CurrencyID, dbo.RSInwardOutward.Reason, NULL AS EmployeeID, dbo.RSInwardOutward.BranchID, dbo.RSInwardOutward.Recorded, 
                         dbo.RSInwardOutward.TotalAmountOriginal, dbo.RSInwardOutward.TotalAmount, 'RSInwardOutward' AS RefTable
FROM            dbo.RSInwardOutward INNER JOIN
                         dbo.Type ON RSInwardOutward.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.RSTransfer.ID AS RefID, dbo.RSTransfer.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.RSTransfer.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.RSTransfer.Date, dbo.RSTransfer.PostedDate, 
                         RSTransfer.CurrencyID, dbo.RSTransfer.Reason, NULL AS EmployeeID, dbo.RSTransfer.BranchID, dbo.RSTransfer.Recorded, dbo.RSTransfer.TotalAmountOriginal, dbo.RSTransfer.TotalAmount, 'RSTransfer' AS RefTable
FROM            dbo.RSTransfer INNER JOIN
                         dbo.Type ON RSTransfer.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.PPInvoice.ID AS RefID, dbo.PPInvoice.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.PPInvoice.No, dbo.PPInvoice.InwardNo AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.PPInvoice.Date, 
                         dbo.PPInvoice.PostedDate, PPInvoice.CurrencyID, dbo.PPInvoice.Reason, PPInvoice.EmployeeID, dbo.PPInvoice.BranchID, dbo.PPInvoice.Recorded, dbo.PPInvoice.TotalAmountOriginal, dbo.PPInvoice.TotalAmount, 
                         'PPInvoice' AS RefTable
FROM            dbo.PPInvoice INNER JOIN
                         dbo.Type ON PPInvoice.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.PPDiscountReturn.ID AS RefID, dbo.PPDiscountReturn.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.PPDiscountReturn.No, NULL AS InwardNo, dbo.PPDiscountReturn.OutwardNo AS OutwardNo, NULL AS MoneydNo, 
                         dbo.PPDiscountReturn.Date, dbo.PPDiscountReturn.PostedDate, PPDiscountReturn.CurrencyID, dbo.PPDiscountReturn.Reason, PPDiscountReturn.EmployeeID, dbo.PPDiscountReturn.BrachID AS BranchID, 
                         dbo.PPDiscountReturn.Recorded, dbo.PPDiscountReturn.TotalAmountOriginal, dbo.PPDiscountReturn.TotalAmount, 'PPDiscountReturn' AS RefTable
FROM            dbo.PPDiscountReturn INNER JOIN
                         dbo.Type ON PPDiscountReturn.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.PPService.ID AS RefID, dbo.PPService.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.PPService.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.PPService.Date, dbo.PPService.PostedDate, 
                         PPService.CurrencyID, dbo.PPService.Reason, PPService.EmployeeID, dbo.PPService.BranchID, dbo.PPService.Recorded, dbo.PPService.TotalAmountOriginal, dbo.PPService.TotalAmount, 'PPService' AS RefTable
FROM            dbo.PPService INNER JOIN
                         dbo.Type ON PPService.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.PPOrder.ID AS RefID, dbo.PPOrder.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.PPOrder.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.PPOrder.Date, 
                         dbo.PPOrder.DeliverDate AS PostedDate, dbo.PPOrder.CurrencyID, dbo.PPOrder.Reason, dbo.PPOrder.EmployeeID, dbo.PPOrder.BranchID, dbo.PPOrder.Exported AS Recorded, dbo.PPOrder.TotalAmountOriginal, 
                         dbo.PPOrder.TotalAmount, 'PPOrder' AS RefTable
FROM            dbo.PPOrder INNER JOIN
                         dbo.Type ON PPOrder.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.SAInvoice.ID AS RefID, dbo.SAInvoice.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.SAInvoice.No, NULL AS InwardNo, dbo.SAInvoice.OutwardNo AS OutwardNo, dbo.SAInvoice.MNo AS MoneydNo, dbo.SAInvoice.Date, 
                         dbo.SAInvoice.PostedDate, SAInvoice.CurrencyID, dbo.SAInvoice.Reason, SAInvoice.EmployeeID, dbo.SAInvoice.BranchID, dbo.SAInvoice.Recorded, dbo.SAInvoice.TotalAmountOriginal, dbo.SAInvoice.TotalAmount, 
                         'SAInvoice' AS RefTable
FROM            dbo.SAInvoice INNER JOIN
                         dbo.Type ON dbo.SAInvoice.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.SAReturn.ID AS RefID, dbo.SAReturn.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.SAReturn.No, dbo.SAReturn.IWNo AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.SAReturn.Date, dbo.SAReturn.PostedDate, 
                         SAReturn.CurrencyID, dbo.SAReturn.Reason, SAReturn.EmployeeID, dbo.SAReturn.BranchID, SAReturn.Recorded AS Posted, dbo.SAReturn.TotalAmountOriginal, dbo.SAReturn.TotalAmount, 'SAReturn' AS RefTable
FROM            dbo.SAReturn INNER JOIN
                         dbo.Type ON dbo.SAReturn.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.SAOrder.ID AS RefID, dbo.SAOrder.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.SAOrder.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.SAOrder.Date, dbo.SAOrder.DeliveDate AS PostedDate, 
                         SAOrder.CurrencyID, dbo.SAOrder.Reason, SAOrder.EmployeeID, dbo.SAOrder.BranchID, SAOrder.Exported AS Recorded, dbo.SAOrder.TotalAmountOriginal, dbo.SAOrder.TotalAmount, 'SAOrder' AS RefTable
FROM            dbo.SAOrder INNER JOIN
                         dbo.Type ON dbo.SAOrder.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.SAQuote.ID AS RefID, dbo.SAQuote.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.SAQuote.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.SAQuote.Date, dbo.SAQuote.Date AS PostedDate, 
                         dbo.SAQuote.CurrencyID, dbo.SAQuote.Reason, dbo.SAQuote.EmployeeID, dbo.SAQuote.BranchID, 0 AS Recorded, dbo.SAQuote.TotalAmountOriginal, dbo.SAQuote.TotalAmount, 'SAQuote' AS RefTable
FROM            dbo.SAQuote INNER JOIN
                         dbo.Type ON SAQuote.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.TIAdjustment.ID AS RefID, dbo.TIAdjustment.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.TIAdjustment.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.TIAdjustment.Date, 
                         dbo.TIAdjustment.PostedDate, NULL AS CurrencyID, dbo.TIAdjustment.Reason, NULL AS EmployeeID, dbo.TIAdjustment.BranchID, dbo.TIAdjustment.Recorded, $ 0 AS TotalAmountOriginal, dbo.TIAdjustment.TotalAmount, 
                         'TIAdjustment' AS RefTable
FROM            dbo.TIAdjustment INNER JOIN
                         dbo.Type ON TIAdjustment.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.TIAllocation.ID AS RefID, dbo.TIAllocation.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.TIAllocation.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.TIAllocation.Date, 
                         dbo.TIAllocation.PostedDate, NULL AS CurrencyID, dbo.TIAllocation.Reason, NULL AS EmployeeID, dbo.TIAllocation.BranchID, dbo.TIAllocation.Recorded, dbo.TIAllocation.TotalAmountOriginal, dbo.TIAllocation.TotalAmount, 
                         'TIAllocation' AS RefTable
FROM            dbo.TIAllocation INNER JOIN
                         dbo.Type ON TIAllocation.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.TIIncrement.ID AS RefID, dbo.TIIncrement.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.TIIncrement.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.TIIncrement.Date, 
                         dbo.TIIncrement.PostedDate, dbo.TIIncrement.CurrencyID, dbo.TIIncrement.Reason, dbo.TIIncrement.EmployeeID, dbo.TIIncrement.BranchID, dbo.TIIncrement.Recorded, dbo.TIIncrement.TotalAmountOriginal, 
                         dbo.TIIncrement.TotalAmount, 'TIIncrement' AS RefTable
FROM            dbo.TIIncrement INNER JOIN
                         dbo.Type ON TIIncrement.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.TIDecrement.ID AS RefID, dbo.TIDecrement.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.TIDecrement.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.TIDecrement.Date, 
                         dbo.TIDecrement.PostedDate, NULL AS CurrencyID, dbo.TIDecrement.Reason, NULL AS EmployeeID, dbo.TIDecrement.BranchID, dbo.TIDecrement.Recorded, 0 AS TotalAmountOriginal, dbo.TIDecrement.TotalAmount, 
                         'TIDecrement' AS RefTable
FROM            dbo.TIDecrement INNER JOIN
                         dbo.Type ON TIDecrement.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.TIAudit.ID AS RefID, dbo.TIAudit.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.TIAudit.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.TIAudit.Date, dbo.TIAudit.PostedDate, NULL 
                         AS CurrencyID, NULL AS Reason, NULL AS EmployeeID, dbo.TIAudit.BranchID, 0 AS Recorded, 0 AS TotalAmountOriginal, 0 AS TotalAmount, 'TIAudit' AS RefTable
FROM            dbo.TIAudit INNER JOIN
                         dbo.Type ON TIAudit.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.TITransfer.ID AS RefID, dbo.TITransfer.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.TITransfer.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.TITransfer.Date, dbo.TITransfer.PostedDate, NULL
                          AS CurrencyID, dbo.TITransfer.Reason, NULL AS EmployeeID, dbo.TITransfer.BranchID, dbo.TITransfer.Recorded, 0 AS TotalAmountOriginal, 0 AS TotalAmount, 'TITransfer' AS RefTable
FROM            dbo.TITransfer INNER JOIN
                         dbo.Type ON TITransfer.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.RSAssemblyDismantlement_new.ID AS RefID, dbo.RSAssemblyDismantlement_new.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.RSAssemblyDismantlement_new.No, NULL AS InwardNo, NULL 
                         AS OutwardNo, NULL AS MoneydNo, dbo.RSAssemblyDismantlement_new.Date, dbo.RSAssemblyDismantlement_new.Date AS PostedDate, dbo.RSAssemblyDismantlement_new.CurrencyID, dbo.RSAssemblyDismantlement_new.Reason, NULL 
                         AS EmployeeID, dbo.RSAssemblyDismantlement_new.BranchID, dbo.RSAssemblyDismantlement_new.Recorded, dbo.RSAssemblyDismantlement_new.AmountOriginal AS TotalAmountOriginal, 
                         dbo.RSAssemblyDismantlement_new.Amount AS TotalAmount, 'RSAssemblyDismantlement_new' AS RefTable
FROM            dbo.RSAssemblyDismantlement_new INNER JOIN
                         dbo.Type ON RSAssemblyDismantlement_new.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.EMContract.ID AS RefID, dbo.EMContract.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.EMContract.Code AS No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.EMContract.SignedDate AS Date, 
                         dbo.EMContract.SignedDate AS PostedDate, dbo.EMContract.CurrencyID, dbo.EMContract.Name, NULL AS EmployeeID, dbo.EMContract.BranchID, 0 AS Recored, dbo.EMContract.AmountOriginal AS TotalAmountOriginal, 
                         dbo.EMContract.Amount AS TotalAmount, 'EMContract' AS RefTable
FROM            dbo.EMContract INNER JOIN
                         dbo.Type ON EMContract.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.MCAudit.ID AS RefID, dbo.MCAudit.TypeID, dbo.Type.TypeName, dbo.Type.TypeGroupID, dbo.MCAudit.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.MCAudit.Date, dbo.MCAudit.AuditDate AS PostedDate, 
                         dbo.MCAudit.CurrencyID, dbo.MCAudit.Description AS Reason, NULL AS EmployeeID, dbo.MCAudit.BranchID, 0 AS Recorded, dbo.MCAudit.TotalAuditAmount, dbo.MCAudit.TotalAuditAmount AS TotalAmountOriginal, 
                         'MCAudit' AS RefTable
FROM            dbo.MCAudit INNER JOIN
                         dbo.Type ON MCAudit.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        newid() AS ID, dbo.GVoucherList.ID AS RefID, dbo.GVoucherList.TypeID, dbo.Type.TypeName, 61 AS TypeGroupID, dbo.GVoucherList.No, NULL AS InwardNo, NULL AS OutwardNo, NULL AS MoneydNo, dbo.GVoucherList.Date AS Date, 
                         dbo.GVoucherList.Date AS PostedDate, NULL AS CurrencyID, NULL AS Reason, NULL AS EmployeeID, NULL AS BranchID, dbo.GVoucherList.Exported AS Recorded, dbo.GVoucherList.TotalAmount AS TotalAuditAmount, 
                         dbo.GVoucherList.TotalAmount AS TotalAmountOriginal, 'GVoucherList' AS RefTable
FROM            dbo.GVoucherList INNER JOIN
                         dbo.Type ON GVoucherList.TypeID = dbo.Type.ID


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[6] 4[3] 2[77] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 19
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'ViewVoucherInvisible'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_DiagramPaneCount', 1, 'SCHEMA', N'dbo', 'VIEW', N'ViewVoucherInvisible'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO



ALTER VIEW [dbo].[ViewVouchersCloseBook]
AS
/*Create by tnson 24/08/2011 - View này chỉ giành riêng cho việc kiểm tra các chứng từ chưa ghi sổ trước khi khóa sổ không được dùng cho việc khác */ SELECT dbo.MCPayment.ID, dbo.MCPaymentDetail.ID AS DetailID, dbo.MCPayment.TypeID, 
                         dbo.Type.TypeName, dbo.MCPayment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCPayment.Date, dbo.MCPayment.PostedDate, dbo.MCPaymentDetail.DebitAccount, dbo.MCPaymentDetail.CreditAccount, 
                         dbo.MCPayment.CurrencyID, dbo.MCPaymentDetail.AmountOriginal, dbo.MCPaymentDetail.Amount, NULL AS OrgPrice, dbo.MCPayment.Reason, dbo.MCPaymentDetail.Description, dbo.MCPaymentDetail.CostSetID, 
                         CostSet.CostSetCode, dbo.MCPaymentDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.MCPayment.EmployeeID, dbo.MCPayment.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.MCPayment.Recorded, dbo.MCPayment.TotalAmount, dbo.MCPayment.TotalAmountOriginal, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MCPayment' AS RefTable
/*case AccountingObject.IsEmployee when 1 then  AccountingObject.AccountingObjectName end AS EmployeeName*/ FROM dbo.MCPayment INNER JOIN
                         dbo.MCPaymentDetail ON dbo.MCPayment.ID = dbo.MCPaymentDetail.MCPaymentID INNER JOIN
                         dbo.Type ON MCPayment.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MCPaymentDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MCPaymentDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MCPaymentDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.MCPayment.ID, dbo.MCPaymentDetailTax.ID AS DetailID, dbo.MCPayment.TypeID, dbo.Type.TypeName, dbo.MCPayment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCPayment.Date, 
                         dbo.MCPayment.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MCPayment.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MCPayment.Reason, 
                         dbo.MCPaymentDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, dbo.MCPaymentDetailTax.AccountingObjectID , NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MCPayment.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MCPaymentDetailTax.VATAmountOriginal, dbo.MCPaymentDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.MCPayment.Recorded, dbo.MCPayment.TotalAmount, dbo.MCPayment.TotalAmountOriginal, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MCPayment' AS RefTable
FROM            dbo.MCPayment INNER JOIN
                         dbo.MCPaymentDetailTax ON dbo.MCPayment.ID = dbo.MCPaymentDetailTax.MCPaymentID INNER JOIN
                         dbo.Type ON MCPayment.TypeID = dbo.Type.ID
/*========================*/ UNION ALL
SELECT        dbo.MBDeposit.ID, dbo.MBDepositDetail.ID AS DetailID, dbo.MBDeposit.TypeID, dbo.Type.TypeName, dbo.MBDeposit.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBDeposit.Date, dbo.MBDeposit.PostedDate, 
                         dbo.MBDepositDetail.DebitAccount, dbo.MBDepositDetail.CreditAccount, dbo.MBDeposit.CurrencyID, dbo.MBDepositDetail.AmountOriginal, dbo.MBDepositDetail.Amount, NULL AS OrgPrice, dbo.MBDeposit.Reason, 
                         dbo.MBDepositDetail.Description, dbo.MBDepositDetail.CostSetID, CostSet.CostSetCode, dbo.MBDepositDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MBDeposit.EmployeeID, dbo.MBDeposit.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL 
                         AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBDeposit.Recorded, dbo.MBDeposit.TotalAmountOriginal, dbo.MBDeposit.TotalAmount, NULL AS DiscountAmountOriginal, NULL 
                         AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBDeposit' AS RefTable
FROM            dbo.MBDeposit INNER JOIN
                         dbo.MBDepositDetail ON dbo.MBDeposit.ID = dbo.MBDepositDetail.MBDepositID INNER JOIN
                         dbo.Type ON MBDeposit.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBDepositDetail.AccountingObjectID = dbo.AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBDepositDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBDepositDetail.ContractID
/*========================      */ UNION ALL
SELECT        dbo.MBDeposit.ID, dbo.MBDepositDetailTax.ID AS DetailID, dbo.MBDeposit.TypeID, dbo.Type.TypeName, dbo.MBDeposit.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBDeposit.Date, dbo.MBDeposit.PostedDate, NULL 
                         AS DebitAccount, NULL AS CreditAccount, dbo.MBDeposit.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBDeposit.Reason, dbo.MBDepositDetailTax.Description, NULL AS CostSetID, NULL 
                         AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, dbo.MBDepositDetailTax.AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBDeposit.BranchID, NULL 
                         AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL 
                         AS VATAccount, dbo.MBDepositDetailTax.VATAmountOriginal, dbo.MBDepositDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBDeposit.Recorded, dbo.MBDeposit.TotalAmountOriginal, 
                         dbo.MBDeposit.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBDeposit' AS RefTable
FROM            dbo.MBDeposit INNER JOIN
                         dbo.MBDepositDetailTax ON dbo.MBDeposit.ID = dbo.MBDepositDetailTax.MBDepositID INNER JOIN
                         dbo.Type ON MBDeposit.TypeID = dbo.Type.ID
/*Chuyển tiền nội bộ*/ UNION ALL
SELECT        dbo.MBInternalTransfer.ID, dbo.MBInternalTransferDetail.ID AS DetailID, dbo.MBInternalTransfer.TypeID, dbo.Type.TypeName, dbo.MBInternalTransfer.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBInternalTransfer.Date, 
                         dbo.MBInternalTransfer.PostedDate, dbo.MBInternalTransferDetail.DebitAccount, dbo.MBInternalTransferDetail.CreditAccount, dbo.MBInternalTransferDetail.CurrencyID, dbo.MBInternalTransferDetail.AmountOriginal, 
                         dbo.MBInternalTransferDetail.Amount, NULL AS OrgPrice, dbo.MBInternalTransfer.Reason, dbo.MBInternalTransfer.Reason AS Description, dbo.MBInternalTransferDetail.CostSetID, CostSet.CostSetCode, 
                         dbo.MBInternalTransferDetail.ContractID, EMContract.Code AS ContractCode, NULL AS AccountingObjectID, '' AS AccountingObjectCategoryName, '' AS AccountingObjectCode, dbo.MBInternalTransferDetail.EmployeeID, 
                         dbo.MBInternalTransfer.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBInternalTransfer.Recorded, dbo.MBInternalTransfer.TotalAmountOriginal, 
                         dbo.MBInternalTransfer.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBInternalTransfer' AS RefTable
FROM            dbo.MBInternalTransfer INNER JOIN
                         dbo.MBInternalTransferDetail ON dbo.MBInternalTransfer.ID = dbo.MBInternalTransferDetail.MBInternalTransferID INNER JOIN
                         dbo.Type ON dbo.MBInternalTransfer.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBInternalTransferDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBInternalTransferDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.MBTellerPaper.ID, dbo.MBTellerPaperDetail.ID AS DetailID, dbo.MBTellerPaper.TypeID, dbo.Type.TypeName, dbo.MBTellerPaper.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBTellerPaper.Date, 
                         dbo.MBTellerPaper.PostedDate, dbo.MBTellerPaperDetail.DebitAccount, dbo.MBTellerPaperDetail.CreditAccount, dbo.MBTellerPaper.CurrencyID, dbo.MBTellerPaperDetail.AmountOriginal, 
                         dbo.MBTellerPaperDetail.Amount, NULL AS OrgPrice, dbo.MBTellerPaper.Reason, dbo.MBTellerPaperDetail.Description, dbo.MBTellerPaperDetail.CostSetID, CostSet.CostSetCode, dbo.MBTellerPaperDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, 
                         dbo.MBTellerPaper.EmployeeID, dbo.MBTellerPaper.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBTellerPaper.Recorded, 
                         dbo.MBTellerPaper.TotalAmountOriginal, dbo.MBTellerPaper.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 
                         0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBTellerPaper' AS RefTable
FROM            dbo.MBTellerPaper INNER JOIN
                         dbo.MBTellerPaperDetail ON dbo.MBTellerPaper.ID = dbo.MBTellerPaperDetail.MBTellerPaperID INNER JOIN
                         dbo.Type ON dbo.MBTellerPaper.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBTellerPaperDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBTellerPaperDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBTellerPaperDetail.ContractID
/*===========================================*/ UNION ALL
SELECT        dbo.MBTellerPaper.ID, dbo.MBTellerPaperDetailTax.ID AS DetailID, dbo.MBTellerPaper.TypeID, dbo.Type.TypeName, dbo.MBTellerPaper.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBTellerPaper.Date, 
                         dbo.MBTellerPaper.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MBTellerPaper.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBTellerPaper.Reason, 
                         dbo.MBTellerPaperDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, dbo.MBTellerPaperDetailTax.AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBTellerPaper.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MBTellerPaperDetailTax.VATAmountOriginal, dbo.MBTellerPaperDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.MBTellerPaper.Recorded, dbo.MBTellerPaper.TotalAmountOriginal, dbo.MBTellerPaper.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MBTellerPaper' AS RefTable
FROM            dbo.MBTellerPaper INNER JOIN
                         dbo.MBTellerPaperDetailTax ON dbo.MBTellerPaper.ID = dbo.MBTellerPaperDetailTax.MBTellerPaperID INNER JOIN
                         dbo.Type ON dbo.MBTellerPaper.TypeID = dbo.Type.ID
/*========================*/ UNION ALL
SELECT        dbo.MBCreditCard.ID, dbo.MBCreditCardDetail.ID AS DetailID, dbo.MBCreditCard.TypeID, dbo.Type.TypeName, dbo.MBCreditCard.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBCreditCard.Date, 
                         dbo.MBCreditCard.PostedDate, dbo.MBCreditCardDetail.DebitAccount, dbo.MBCreditCardDetail.CreditAccount, dbo.MBCreditCard.CurrencyID, dbo.MBCreditCardDetail.AmountOriginal, dbo.MBCreditCardDetail.Amount, NULL 
                         AS OrgPrice, dbo.MBCreditCard.Reason, dbo.MBCreditCardDetail.Description, dbo.MBCreditCardDetail.CostSetID, CostSet.CostSetCode, dbo.MBCreditCardDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MBCreditCard.EmployeeID, 
                         dbo.MBCreditCard.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBCreditCard.Recorded, dbo.MBCreditCard.TotalAmountOriginal, 
                         dbo.MBCreditCard.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBCreditCard' AS RefTable
FROM            dbo.MBCreditCard INNER JOIN
                         dbo.MBCreditCardDetail ON dbo.MBCreditCard.ID = dbo.MBCreditCardDetail.MBCreditCardID INNER JOIN
                         dbo.Type ON dbo.MBCreditCard.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBCreditCardDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBCreditCardDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBCreditCardDetail.ContractID
/*=========================================*/ UNION ALL
SELECT        dbo.MBCreditCard.ID, dbo.MBCreditCardDetailTax.ID AS DetailID, dbo.MBCreditCard.TypeID, dbo.Type.TypeName, dbo.MBCreditCard.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBCreditCard.Date, 
                         dbo.MBCreditCard.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MBCreditCard.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBCreditCard.Reason, 
                         dbo.MBCreditCardDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, dbo.MBCreditCardDetailTax.AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBCreditCard.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MBCreditCardDetailTax.VATAmountOriginal, dbo.MBCreditCardDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.MBCreditCard.Recorded, dbo.MBCreditCard.TotalAmountOriginal, dbo.MBCreditCard.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MBCreditCard' AS RefTable
FROM            dbo.MBCreditCard INNER JOIN
                         dbo.MBCreditCardDetailTax ON dbo.MBCreditCard.ID = dbo.MBCreditCardDetailTax.MBCreditCardID INNER JOIN
                         dbo.Type ON dbo.MBCreditCard.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        dbo.MCReceipt.ID, dbo.MCReceiptDetail.ID AS DetailID, dbo.MCReceipt.TypeID, dbo.Type.TypeName, dbo.MCReceipt.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCReceipt.Date, dbo.MCReceipt.PostedDate, 
                         dbo.MCReceiptDetail.DebitAccount, dbo.MCReceiptDetail.CreditAccount, dbo.MCReceipt.CurrencyID, dbo.MCReceiptDetail.AmountOriginal, dbo.MCReceiptDetail.Amount, NULL AS OrgPrice, dbo.MCReceipt.Reason, 
                         dbo.MCReceiptDetail.Description, dbo.MCReceiptDetail.CostSetID, CostSet.CostSetCode, dbo.MCReceiptDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MCReceipt.EmployeeID, dbo.MCReceipt.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL 
                         AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MCReceipt.Recorded, dbo.MCReceipt.TotalAmountOriginal, dbo.MCReceipt.TotalAmount, NULL AS DiscountAmountOriginal, NULL 
                         AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MCReceipt' AS RefTable
FROM            dbo.MCReceipt INNER JOIN
                         dbo.MCReceiptDetail ON dbo.MCReceipt.ID = dbo.MCReceiptDetail.MCReceiptID INNER JOIN
                         dbo.Type ON MCReceipt.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MCReceiptDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MCReceiptDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MCReceiptDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.MCReceipt.ID, dbo.MCReceiptDetailTax.ID AS DetailID, dbo.MCReceipt.TypeID, dbo.Type.TypeName, dbo.MCReceipt.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCReceipt.Date, dbo.MCReceipt.PostedDate, NULL 
                         AS DebitAccount, NULL AS CreditAccount, dbo.MCReceipt.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MCReceipt.Reason, dbo.MCReceiptDetailTax.Description, NULL AS CostSetID, NULL 
                         AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, dbo.MCReceiptDetailTax.AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL AS AccountingObjectCode, NULL AS EmployeeID, dbo.MCReceipt.BranchID, NULL 
                         AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL 
                         AS VATAccount, dbo.MCReceiptDetailTax.VATAmountOriginal, dbo.MCReceiptDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MCReceipt.Recorded, dbo.MCReceipt.TotalAmountOriginal, 
                         dbo.MCReceipt.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MCReceipt' AS RefTable
FROM            dbo.MCReceipt INNER JOIN
                         dbo.MCReceiptDetailTax ON dbo.MCReceipt.ID = dbo.MCReceiptDetailTax.MCReceiptID INNER JOIN
                         dbo.Type ON MCReceipt.TypeID = dbo.Type.ID
/*========================*/ UNION ALL
SELECT        dbo.FAAdjustment.ID, dbo.FAAdjustmentDetail.ID AS DetailID, dbo.FAAdjustment.TypeID, dbo.Type.TypeName, dbo.FAAdjustment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FAAdjustment.Date, 
                         dbo.FAAdjustment.PostedDate, dbo.FAAdjustmentDetail.DebitAccount, dbo.FAAdjustmentDetail.CreditAccount, NULL AS CurrencyID, $ 0 AS AmountOriginal, dbo.FAAdjustmentDetail.Amount, $ 0 AS OrgPrice, 
                         dbo.FAAdjustment.Reason, dbo.FAAdjustmentDetail.Description, dbo.FAAdjustmentDetail.CostSetID, CostSet.CostSetCode, dbo.FAAdjustmentDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, NULL AS EmployeeID, 
                         dbo.FAAdjustment.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, $ 0 AS VATAmountOriginal, $ 0 AS VATAmount, $ 0 AS Quantity, $ 0 AS UnitPrice, dbo.FAAdjustment.Recorded, $ 0 AS TotalAmountOriginal, 
                         dbo.FAAdjustment.TotalAmount, $ 0 AS DiscountAmountOriginal, $ 0 AS DiscountAmount, NULL AS DiscountAccount, $ 0 AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FAAdjustment' AS RefTable
FROM            dbo.FAAdjustment INNER JOIN
                         dbo.FAAdjustmentDetail ON dbo.FAAdjustment.ID = dbo.FAAdjustmentDetail.FAAdjustmentID INNER JOIN
                         dbo.Type ON FAAdjustment.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FAAdjustmentDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FAAdjustment.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FAAdjustmentDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FAAdjustmentDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.FADepreciation.ID, dbo.FADepreciationDetail.ID AS DetailID, dbo.FADepreciation.TypeID, dbo.Type.TypeName, dbo.FADepreciation.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FADepreciation.Date, 
                         dbo.FADepreciation.PostedDate, dbo.FADepreciationDetail.DebitAccount, dbo.FADepreciationDetail.CreditAccount, FADepreciationDetail.CurrencyID, dbo.FADepreciationDetail.AmountOriginal, dbo.FADepreciationDetail.Amount, 
                         dbo.FADepreciationDetail.OrgPrice, dbo.FADepreciation.Reason, dbo.FADepreciationDetail.Description, dbo.FADepreciationDetail.CostSetID, CostSet.CostSetCode, dbo.FADepreciationDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, 
                         dbo.FADepreciationDetail.EmployeeID, dbo.FADepreciation.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, FADepreciationDetail.ObjectID, Department.DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.FADepreciation.Recorded, dbo.FADepreciation.TotalAmountOriginal, dbo.FADepreciation.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'FADepreciation' AS RefTable
FROM            dbo.FADepreciation INNER JOIN
                         dbo.FADepreciationDetail ON dbo.FADepreciation.ID = dbo.FADepreciationDetail.FADepreciationID INNER JOIN
                         dbo.Type ON FADepreciation.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FADepreciationDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FADepreciationDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FADepreciationDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FADepreciationDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FADepreciationDetail.ObjectID
/*========================*/ UNION ALL
SELECT        dbo.FAIncrement.ID, dbo.FAIncrementDetail.ID AS DetailID, dbo.FAIncrement.TypeID, dbo.Type.TypeName, dbo.FAIncrement.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FAIncrement.Date, dbo.FAIncrement.PostedDate, 
                         dbo.FAIncrementDetail.DebitAccount, dbo.FAIncrementDetail.CreditAccount, FAIncrement.CurrencyID, dbo.FAIncrementDetail.AmountOriginal, dbo.FAIncrementDetail.Amount, dbo.FAIncrementDetail.OrgPriceOriginal AS OrgPrice, 
                         dbo.FAIncrement.Reason, dbo.FAIncrementDetail.Description, dbo.FAIncrementDetail.CostSetID, CostSet.CostSetCode, dbo.FAIncrementDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.FAIncrement.EmployeeID, dbo.FAIncrement.BranchID, dbo.FixedAsset.ID AS FixedAssetID, 
                         dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, FAIncrementDetail.DepartmentID, dbo.Department.DepartmentName, 
                         dbo.FAIncrementDetail.VATAccount AS VATAccount, dbo.FAIncrementDetail.VATAmountOriginal AS VATAmountOriginal, dbo.FAIncrementDetail.VATAmount AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.FAIncrement.Recorded, dbo.FAIncrement.TotalAmountOriginal, dbo.FAIncrement.TotalAmount, dbo.FAIncrementDetail.DiscountAmountOriginal, dbo.FAIncrementDetail.DiscountAmount, NULL AS DiscountAccount, 
                         dbo.FAIncrementDetail.FreightAmountOriginal, dbo.FAIncrementDetail.FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FAIncrement' AS RefTable
FROM            dbo.FAIncrement INNER JOIN
                         dbo.FAIncrementDetail ON dbo.FAIncrement.ID = dbo.FAIncrementDetail.ID INNER JOIN
                         dbo.Type ON FAIncrement.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FAIncrementDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FAIncrementDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FAIncrementDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FAIncrementDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FAIncrementDetail.DepartmentID
/*========================*/ UNION ALL
SELECT        dbo.FADecrement.ID, dbo.FADecrementDetail.ID AS DetailID, dbo.FADecrement.TypeID, dbo.Type.TypeName, dbo.FADecrement.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FADecrement.Date, 
                         dbo.FADecrement.PostedDate, dbo.FADecrementDetail.DebitAccount, dbo.FADecrementDetail.CreditAccount, FADecrement.CurrencyID, dbo.FADecrementDetail.AmountOriginal, dbo.FADecrementDetail.Amount, NULL AS OrgPrice, 
                         dbo.FADecrement.Reason, dbo.FADecrementDetail.Description, dbo.FADecrementDetail.CostSetID, CostSet.CostSetCode, dbo.FADecrementDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.FADecrementDetail.EmployeeID, 
                         dbo.FADecrement.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, 
                         FADecrementDetail.DepartmentID, Department.DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.FADecrement.Recorded, 
                         dbo.FADecrement.TotalAmountOriginal, dbo.FADecrement.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 
                         0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FADecrement' AS RefTable
FROM            dbo.FADecrement INNER JOIN
                         dbo.FADecrementDetail ON dbo.FADecrement.ID = dbo.FADecrementDetail.FADecrementID INNER JOIN
                         dbo.Type ON FADecrement.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FADecrementDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FADecrementDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FADecrementDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FADecrementDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FADecrementDetail.DepartmentID
/*========================*/ UNION ALL
/*Chứng từ nghiệp vụ khác*/ SELECT dbo.GOtherVoucher.ID, dbo.GOtherVoucherDetail.ID AS DetailID, dbo.GOtherVoucher.TypeID, dbo.Type.TypeName, dbo.GOtherVoucher.No, NULL AS InwardNo, NULL AS OutwardNo, 
                         dbo.GOtherVoucher.Date, dbo.GOtherVoucher.PostedDate, dbo.GOtherVoucherDetail.DebitAccount, dbo.GOtherVoucherDetail.CreditAccount, GOtherVoucherDetail.CurrencyID, dbo.GOtherVoucherDetail.AmountOriginal, 
                         dbo.GOtherVoucherDetail.Amount, NULL AS OrgPrice, dbo.GOtherVoucher.Reason, dbo.GOtherVoucherDetail.Description, dbo.GOtherVoucherDetail.CostSetID, CostSet.CostSetCode, dbo.GOtherVoucherDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.GOtherVoucherDetail.DebitAccountingObjectID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode AS AccountingObjectCode, dbo.GOtherVoucherDetail.EmployeeID, dbo.GOtherVoucher.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL 
                         AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL 
                         AS UnitPrice, dbo.GOtherVoucher.Recorded, dbo.GOtherVoucher.TotalAmountOriginal, dbo.GOtherVoucher.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'GOtherVoucher' AS RefTable
FROM            dbo.GOtherVoucher INNER JOIN
                         dbo.GOtherVoucherDetail ON dbo.GOtherVoucher.ID = dbo.GOtherVoucherDetail.GOtherVoucherID INNER JOIN
                         dbo.Type ON GOtherVoucher.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON GOtherVoucherDetail.DebitAccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = GOtherVoucherDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = GOtherVoucherDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.GOtherVoucher.ID, dbo.GOtherVoucherDetailTax.ID AS DetailID, dbo.GOtherVoucher.TypeID, dbo.Type.TypeName, dbo.GOtherVoucher.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.GOtherVoucher.Date, 
                         dbo.GOtherVoucher.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, GOtherVoucherDetailTax.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.GOtherVoucher.Reason, 
                         dbo.GOtherVoucherDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AccountingObjectCode, NULL AS EmployeeID, dbo.GOtherVoucher.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.GOtherVoucherDetailTax.VATAmountOriginal, dbo.GOtherVoucherDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.GOtherVoucher.Recorded, dbo.GOtherVoucher.TotalAmountOriginal, dbo.GOtherVoucher.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'GOtherVoucher' AS RefTable
FROM            dbo.GOtherVoucher INNER JOIN
                         dbo.GOtherVoucherDetailTax ON dbo.GOtherVoucher.ID = dbo.GOtherVoucherDetailTax.GOtherVoucherID INNER JOIN
                         dbo.Type ON GOtherVoucher.TypeID = dbo.Type.ID
/*========================*/ UNION ALL
SELECT        dbo.RSInwardOutward.ID, dbo.RSInwardOutwardDetail.ID AS DetailID, dbo.RSInwardOutward.TypeID, dbo.Type.TypeName, dbo.RSInwardOutward.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.RSInwardOutward.Date, 
                         dbo.RSInwardOutward.PostedDate, dbo.RSInwardOutwardDetail.DebitAccount, dbo.RSInwardOutwardDetail.CreditAccount, RSInwardOutward.CurrencyID, dbo.RSInwardOutwardDetail.AmountOriginal, 
                         dbo.RSInwardOutwardDetail.Amount, NULL AS OrgPrice, dbo.RSInwardOutward.Reason, dbo.RSInwardOutwardDetail.Description, dbo.RSInwardOutwardDetail.CostSetID, CostSet.CostSetCode, 
                         dbo.RSInwardOutwardDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.RSInwardOutwardDetail.EmployeeID, dbo.RSInwardOutward.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, 
                         dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, 
                         RSInwardOutwardDetail.Quantity, RSInwardOutwardDetail.UnitPrice, dbo.RSInwardOutward.Recorded, dbo.RSInwardOutward.TotalAmountOriginal, dbo.RSInwardOutward.TotalAmount, NULL AS DiscountAmountOriginal, NULL 
                         AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'RSInwardOutward' AS RefTable
FROM            dbo.RSInwardOutward INNER JOIN
                         dbo.RSInwardOutwardDetail ON dbo.RSInwardOutward.ID = dbo.RSInwardOutwardDetail.RSInwardOutwardID INNER JOIN
                         dbo.Type ON RSInwardOutward.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON RSInwardOutwardDetail.EmployeeID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON RSInwardOutwardDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON RSInwardOutwardDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = RSInwardOutwardDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = RSInwardOutwardDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.RSTransfer.ID, dbo.RSTransferDetail.ID AS DetailID, dbo.RSTransfer.TypeID, dbo.Type.TypeName, dbo.RSTransfer.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.RSTransfer.Date, dbo.RSTransfer.PostedDate, 
                         dbo.RSTransferDetail.DebitAccount, dbo.RSTransferDetail.CreditAccount, RSTransfer.CurrencyID, dbo.RSTransferDetail.AmountOriginal, dbo.RSTransferDetail.Amount, NULL AS OrgPrice, dbo.RSTransfer.Reason, 
                         dbo.RSTransferDetail.Description, dbo.RSTransferDetail.CostSetID, CostSet.CostSetCode, dbo.RSTransferDetail.ContractID, EMContract.Code AS ContractCode, dbo.RSTransfer.AccountingObjectID, NULL 
                         AS AccountingObjectCategoryName, NULL AS AccountingObjectCode, dbo.RSTransferDetail.EmployeeID, dbo.RSTransfer.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, 
                         dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL 
                         AS VATAmount, RSTransferDetail.Quantity, RSTransferDetail.UnitPrice, dbo.RSTransfer.Recorded, dbo.RSTransfer.TotalAmountOriginal, dbo.RSTransfer.TotalAmount, NULL AS DiscountAmountOriginal, NULL 
                         AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'RSTransfer' AS RefTable
FROM            dbo.RSTransfer INNER JOIN
                         dbo.RSTransferDetail ON dbo.RSTransfer.ID = dbo.RSTransferDetail.RSTransferID INNER JOIN
                         dbo.Type ON RSTransfer.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.MaterialGoods ON RSTransferDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON RSTransferDetail.ToRepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = RSTransferDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = RSTransferDetail.ContractID
/*======================== */ UNION ALL
SELECT        dbo.PPInvoice.ID, dbo.PPInvoiceDetail.ID AS DetailID, dbo.PPInvoice.TypeID, dbo.Type.TypeName, dbo.PPInvoice.No, dbo.PPInvoice.InwardNo AS InwardNo, NULL AS OutwardNo, dbo.PPInvoice.Date, 
                         dbo.PPInvoice.PostedDate, dbo.PPInvoiceDetail.DebitAccount, dbo.PPInvoiceDetail.CreditAccount, PPInvoice.CurrencyID, dbo.PPInvoiceDetail.AmountOriginal, dbo.PPInvoiceDetail.Amount, NULL AS OrgPrice, 
                         dbo.PPInvoice.Reason, dbo.PPInvoiceDetail.Description, dbo.PPInvoiceDetail.CostSetID, CostSet.CostSetCode, dbo.PPInvoiceDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPInvoice.EmployeeID, 
                         dbo.PPInvoice.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, 
                         dbo.Repository.RepositoryName, dbo.PPInvoiceDetail.DepartmentID, NULL AS DepartmentName, PPInvoiceDetail.VATAccount, PPInvoiceDetail.VATAmountOriginal, PPInvoiceDetail.VATAmount, PPInvoiceDetail.Quantity, 
                         PPInvoiceDetail.UnitPrice, dbo.PPInvoice.Recorded, dbo.PPInvoice.TotalAmountOriginal, dbo.PPInvoice.TotalAmount, dbo.PPInvoiceDetail.DiscountAmountOriginal, dbo.PPInvoiceDetail.DiscountAmount, NULL 
                         AS DiscountAccount, dbo.PPInvoiceDetail.FreightAmountOriginal, dbo.PPInvoiceDetail.FreightAmount, 0 AS DetailTax, PPInvoiceDetail.InwardAmountOriginal, PPInvoiceDetail.InwardAmount, 
                         dbo.PPInvoice.StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPInvoice' AS RefTable
FROM            dbo.PPInvoice INNER JOIN
                         dbo.PPInvoiceDetail ON dbo.PPInvoice.ID = dbo.PPInvoiceDetail.PPInvoiceID INNER JOIN
                         dbo.Type ON PPInvoice.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPInvoiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPInvoiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON PPInvoiceDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPInvoiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPInvoiceDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.PPDiscountReturn.ID, dbo.PPDiscountReturnDetail.ID AS DetailID, dbo.PPDiscountReturn.TypeID, dbo.Type.TypeName, dbo.PPDiscountReturn.No, NULL AS InwardNo, dbo.PPDiscountReturn.OutwardNo AS OutwardNo, 
                         dbo.PPDiscountReturn.Date, dbo.PPDiscountReturn.PostedDate, dbo.PPDiscountReturnDetail.DebitAccount, dbo.PPDiscountReturnDetail.CreditAccount, PPDiscountReturn.CurrencyID, 
                         dbo.PPDiscountReturnDetail.AmountOriginal, dbo.PPDiscountReturnDetail.Amount, NULL AS OrgPrice, dbo.PPDiscountReturn.Reason, dbo.PPDiscountReturnDetail.Description, dbo.PPDiscountReturnDetail.CostSetID, 
                         CostSet.CostSetCode, dbo.PPDiscountReturnDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPDiscountReturn.EmployeeID, dbo.PPDiscountReturn.BrachID AS BranchID, NULL 
                         AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, PPDiscountReturnDetail.VATAccount, PPDiscountReturnDetail.VATAmountOriginal, PPDiscountReturnDetail.VATAmount, PPDiscountReturnDetail.Quantity, 
                         PPDiscountReturnDetail.UnitPrice, dbo.PPDiscountReturn.Recorded, dbo.PPDiscountReturn.TotalAmountOriginal, dbo.PPDiscountReturn.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL 
                         AS OutwardAmount, 'PPDiscountReturn' AS RefTable
FROM            dbo.PPDiscountReturn INNER JOIN
                         dbo.PPDiscountReturnDetail ON dbo.PPDiscountReturn.ID = dbo.PPDiscountReturnDetail.PPDiscountReturnID INNER JOIN
                         dbo.Type ON PPDiscountReturn.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPDiscountReturnDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPDiscountReturnDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON PPDiscountReturnDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPDiscountReturnDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPDiscountReturnDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.PPService.ID, dbo.PPServiceDetail.ID AS DetailID, dbo.PPService.TypeID, dbo.Type.TypeName, dbo.PPService.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.PPService.Date, dbo.PPService.PostedDate, 
                         dbo.PPServiceDetail.DebitAccount, dbo.PPServiceDetail.CreditAccount, PPService.CurrencyID, dbo.PPServiceDetail.AmountOriginal, dbo.PPServiceDetail.Amount, NULL AS OrgPrice, dbo.PPService.Reason, 
                         dbo.PPServiceDetail.Description, dbo.PPServiceDetail.CostSetID, CostSet.CostSetCode, dbo.PPServiceDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPService.EmployeeID, dbo.PPService.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, 
                         PPServiceDetail.VATAccount, PPServiceDetail.VATAmountOriginal, PPServiceDetail.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.PPService.Recorded, dbo.PPService.TotalAmountOriginal, dbo.PPService.TotalAmount, 
                         PPServiceDetail.DiscountAmountOriginal, PPServiceDetail.DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPService' AS RefTable
FROM            dbo.PPService INNER JOIN
                         dbo.PPServiceDetail ON dbo.PPService.ID = dbo.PPServiceDetail.PPServiceID INNER JOIN
                         dbo.Type ON PPService.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPServiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPServiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPServiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPServiceDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.PPOrder.ID, dbo.PPOrderDetail.ID AS DetailID, dbo.PPOrder.TypeID, dbo.Type.TypeName, dbo.PPOrder.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.PPOrder.Date, dbo.PPOrder.DeliverDate AS PostedDate, 
                         dbo.PPOrderDetail.DebitAccount, dbo.PPOrderDetail.CreditAccount, PPOrder.CurrencyID, dbo.PPOrderDetail.AmountOriginal, dbo.PPOrderDetail.Amount, NULL AS OrgPrice, dbo.PPOrder.Reason, dbo.PPOrderDetail.Description, 
                         dbo.PPOrderDetail.CostSetID, CostSet.CostSetCode, dbo.PPOrderDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPOrder.EmployeeID, dbo.PPOrder.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, 
                         PPOrderDetail.VATAccount, PPOrderDetail.VATAmountOriginal, PPOrderDetail.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.PPOrder.Exported AS Recorded, dbo.PPOrder.TotalAmountOriginal, dbo.PPOrder.TotalAmount, 
                         PPOrderDetail.DiscountAmountOriginal, PPOrderDetail.DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPOrder' AS RefTable
FROM            dbo.PPOrder INNER JOIN
                         dbo.PPOrderDetail ON dbo.PPOrder.ID = dbo.PPOrderDetail.PPOrderID INNER JOIN
                         dbo.Type ON PPOrder.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPOrderDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPOrderDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPOrderDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPOrderDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.SAInvoice.ID, dbo.SAInvoiceDetail.ID AS DetailID, dbo.SAInvoice.TypeID, dbo.Type.TypeName, dbo.SAInvoice.No, NULL AS InwardNo, dbo.SAInvoice.OutwardNo AS OutwardNo, dbo.SAInvoice.Date, 
                         dbo.SAInvoice.PostedDate, dbo.SAInvoiceDetail.DebitAccount, dbo.SAInvoiceDetail.CreditAccount, SAInvoice.CurrencyID, dbo.SAInvoiceDetail.AmountOriginal, dbo.SAInvoiceDetail.Amount, NULL AS OrgPrice, 
                         dbo.SAInvoice.Reason, dbo.SAInvoiceDetail.Description, dbo.SAInvoiceDetail.CostSetID, CostSet.CostSetCode, dbo.SAInvoiceDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAInvoice.EmployeeID, dbo.SAInvoice.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, SAInvoiceDetail.VATAccount, SAInvoiceDetail.VATAmountOriginal, SAInvoiceDetail.VATAmount, SAInvoiceDetail.Quantity, SAInvoiceDetail.UnitPrice, dbo.SAInvoice.Recorded, 
                         dbo.SAInvoice.TotalAmountOriginal, dbo.SAInvoice.TotalAmount, dbo.SAInvoiceDetail.DiscountAmountOriginal, dbo.SAInvoiceDetail.DiscountAmount, dbo.SAInvoiceDetail.DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, dbo.SAInvoiceDetail.OWAmountOriginal AS OutwardAmountOriginal, 
                         dbo.SAInvoiceDetail.OWAmount AS OutwardAmount, 'SAInvoice' AS RefTable
FROM            dbo.SAInvoice INNER JOIN
                         dbo.SAInvoiceDetail ON dbo.SAInvoice.ID = dbo.SAInvoiceDetail.SAInvoiceID INNER JOIN
                         dbo.Type ON dbo.SAInvoice.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAInvoiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAInvoiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAInvoiceDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAInvoiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAInvoiceDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.SAReturn.ID, dbo.SAReturnDetail.ID AS DetailID, dbo.SAReturn.TypeID, dbo.Type.TypeName, dbo.SAReturn.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.SAReturn.Date, dbo.SAReturn.PostedDate, NULL 
                         AS DebitAccount, NULL AS CreditAccount, SAReturn.CurrencyID, dbo.SAReturnDetail.AmountOriginal, dbo.SAReturnDetail.Amount, NULL AS OrgPrice, dbo.SAReturn.Reason, dbo.SAReturnDetail.Description, 
                         dbo.SAReturnDetail.CostSetID, CostSet.CostSetCode, dbo.SAReturnDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAReturn.EmployeeID, dbo.SAReturn.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, SAReturnDetail.VATAccount, SAReturnDetail.VATAmountOriginal, SAReturnDetail.VATAmount, SAReturnDetail.Quantity, SAReturnDetail.UnitPrice, SAReturn.Recorded AS Posted, 
                         dbo.SAReturn.TotalAmountOriginal, dbo.SAReturn.TotalAmount, dbo.SAReturnDetail.DiscountAmountOriginal, dbo.SAReturnDetail.DiscountAmount, dbo.SAReturnDetail.DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'SAReturn' AS RefTable
FROM            dbo.SAReturn INNER JOIN
                         dbo.SAReturnDetail ON dbo.SAReturn.ID = dbo.SAReturnDetail.SAReturnID INNER JOIN
                         dbo.Type ON dbo.SAReturn.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAReturnDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAReturnDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAReturnDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAReturnDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAReturnDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.SAOrder.ID, dbo.SAOrderDetail.ID AS DetailID, dbo.SAOrder.TypeID, dbo.Type.TypeName, dbo.SAOrder.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.SAOrder.Date, dbo.SAOrder.DeliveDate AS PostedDate, NULL 
                         AS DebitAccount, NULL AS CreditAccount, SAOrder.CurrencyID, dbo.SAOrderDetail.AmountOriginal, dbo.SAOrderDetail.Amount, NULL AS OrgPrice, dbo.SAOrder.Reason, dbo.SAOrderDetail.Description, 
                         dbo.SAOrderDetail.CostSetID, CostSet.CostSetCode, dbo.SAOrderDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAOrder.EmployeeID, dbo.SAOrder.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, SAOrderDetail.VATAccount, SAOrderDetail.VATAmountOriginal, SAOrderDetail.VATAmount, SAOrderDetail.Quantity, SAOrderDetail.UnitPrice, SAOrder.Exported AS Recorded, dbo.SAOrder.TotalAmountOriginal, 
                         dbo.SAOrder.TotalAmount, dbo.SAOrderDetail.DiscountAmountOriginal, dbo.SAOrderDetail.DiscountAmount, dbo.SAOrderDetail.DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'SAOrder' AS RefTable
FROM            dbo.SAOrder INNER JOIN
                         dbo.SAOrderDetail ON dbo.SAOrder.ID = dbo.SAOrderDetail.SAOrderID INNER JOIN
                         dbo.Type ON dbo.SAOrder.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAOrderDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAOrderDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAOrderDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAOrderDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAOrderDetail.ContractID


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[Account]
  ADD [IsForeignCurrency] [bit] NULL DEFAULT (0)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sp_refreshview '[dbo].[ViewGLPayExceedCash]'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE PROCEDURE [dbo].[Proc_NhatKySoCai]
    @FromDate DATETIME,
    @ToDate DATETIME

AS
BEGIN     
	DECLARE @q NVARCHAR(MAX)
	DECLARE @tblNhatKySoCai	 TABLE(
			ID int identity(1,1),
			PostedDate date,	
			No NVARCHAR(20),
			Date date,
			Reason NVARCHAR(MAX),
			PhatSinh decimal(25,0),
			DebitAmount decimal(25,0),
			CreditAmount decimal(25,0),
			Account NVARCHAR(25),		
			AccountCorresponding NVARCHAR(25),
			Orderby NVARCHAR(25))
	Select top 0 * into #tg from @tblNhatKySoCai

Insert into #tg
Select PostedDate, No, Date, Reason, ISNULL(DebitAmount,CreditAmount) as PhatSinh, DebitAmount,CreditAmount, Account, AccountCorresponding,null
From GeneralLedger
where PostedDate > = @FromDate and PostedDate < = @ToDate
order by PostedDate

Select PostedDate, No, Date, Reason,null as PhatSinh,DebitAmount,CreditAmount, Account, AccountCorresponding
into #tg2
From GeneralLedger
where PostedDate < @FromDate
order by PostedDate



DECLARE @Count int
SET @Count = (Select Count(*) from #tg2)

Select top 0 * into #tg1 from @tblNhatKySoCai

INSERT INTO #tg1
SELECT null,null,null,N'- Dư đầu kỳ',null,
CASE WHEN (DebitAmount - CreditAmount) > 0 then (DebitAmount - CreditAmount)
ELSE null
END AS DebitAmount,
CASE WHEN (DebitAmount - CreditAmount) < 0 then (CreditAmount - DebitAmount)
ELSE null
END AS CreditAmount
,Account,AccountCorresponding,'a'
From #tg2
group by Account,AccountCorresponding,DebitAmount,CreditAmount
order by Account,AccountCorresponding

Insert into @tblNhatKySoCai Select PostedDate,No,Date,Reason,PhatSinh,Sum(DebitAmount),Sum(CreditAmount),Account,AccountCorresponding,Orderby From #tg1 group by Account,AccountCorresponding,PostedDate,No,Date,Reason,PhatSinh,Orderby


INSERT INTO @tblNhatKySoCai
Select PostedDate, No, Date, Reason, ISNULL(DebitAmount,CreditAmount) as PhatSinh, DebitAmount,CreditAmount, Account, AccountCorresponding,'b'
From GeneralLedger
where PostedDate >= @FromDate and PostedDate <= @ToDate   AND (DebitAmount >  0 OR CreditAmount >0)
order by PostedDate








Select ID,CONVERT(NVARCHAR(50),PostedDate,103) as PostedDate,No,CONVERT(NVARCHAR(50),Date,103) as Date,Reason,Sum(PhatSinh) as PhatSinh,Sum(DebitAmount) as DebitAmount,Sum(CreditAmount) as CreditAmount,Account,AccountCorresponding,Orderby  from @tblNhatKySoCai
group by Account,AccountCorresponding,PostedDate,No,Date,Reason,Orderby,ID
order by ID

	 







END
/**/


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

ALTER PROCEDURE [dbo].[Proc_GL_GeneralDiaryBook_S03a]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @GroupTheSameItem BIT ,/*cộng gộp các bút toán giống nhau*/
    @IsShowAccumAmount BIT
AS 
    BEGIN
    
       DECLARE @PrevFromDate AS DATETIME
       SET @PrevFromDate = DATEADD(MILLISECOND, -10, @FromDate)
       DECLARE @StartDate DATETIME
        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'

		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        IF @GroupTheSameItem = 1 
            BEGIN
			select * from
            (

        SELECT  ROW_NUMBER() OVER ( ORDER BY gl.PostedDate, gl.RefDate
						 		,CASE WHEN GL.TypeID = 4012 THEN 1 ELSE 0 END
								, GL.RefNo , GL.TypeID ) AS RowNum ,
                        CAST (1 AS BIT) AS IsSummaryRow ,
						CAST(0 AS BIT) AS IsBold ,
                        GL.[ReferenceID] ,
                        GL.[TypeID] ,
                        GL.[PostedDate] ,
                        GL.[RefDate] ,
                        GL.No AS [RefNo],
                        GL.No AS RefNo1,
                        NULL AS RefOrder ,
                        GL.[InvoiceDate] ,
                        GL.[InvoiceNo] ,
                        GL.[Reason] AS JournalMemo,/*edit by cuongpv Description -> Reason*/
                        /*bổ sung trường diễn giải thông tin chung cr 137228*/              
						GL.Reason AS JournalMemoMaster ,/*edit by cuongpv Description -> Reason*/
                        GL.[Account] as AccountNumber ,
                        GL.[AccountCorresponding] as CorrespondingAccountNumber ,
						'' AS [RefTypeName],
                        '' AS AccountObjectCode ,
                        '' AS AccountObjectName ,
                        '' AS EmployeeCode ,
                        '' AS EmployeeName ,
                        '' AS ExpenseItemCode ,
                        '' AS ExpenseItemName ,
                        '' AS JobCode ,
                        '' AS JobName ,
                        '' AS ProjectWorkCode ,
                        '' AS ProjectWorkName ,
                        '' AS OrderNo ,
                        '' AS [PUContractCode] ,
                        '' AS [ContractCode] ,
                        '' AS ListItemCode ,
                        '' AS ListItemName ,
                        SUM(GL.[DebitAmount]) AS DebitAmount ,
                        null AS CreditAmount ,
                        '' as BranchName,
                        '' AS OrganizationUnitCode ,
                        '' AS OrganizationUnitName ,
                        '' AS MasterCustomField1 ,
                        '' AS MasterCustomField2 ,
                        '' AS MasterCustomField3 ,
                        '' AS MasterCustomField4 ,
                        '' AS MasterCustomField5 ,
                        '' AS MasterCustomField6 ,
                        '' AS MasterCustomField7 ,
                        '' AS MasterCustomField8 ,
                        '' AS MasterCustomField9 ,
                        '' AS MasterCustomField10 ,
                        '' AS CustomField1 ,
                        '' AS CustomField2 ,
                        '' AS CustomField3 ,
                        '' AS CustomField4 ,
                        '' AS CustomField5 ,
                        '' AS CustomField6 ,
                        '' AS CustomField7 ,
                        '' AS CustomField8 ,
                        '' AS CustomField9 ,
                        '' AS CustomField10 ,
                        '' AS UnResonableCost ,
                        CASE WHEN SUM(DebitAmount) <> 0
                                THEN Gl.Account
                                    + AccountCorresponding
                                WHEN SUM(CreditAmount) <> 0
                                THEN gl.AccountCorresponding
                                    + GL.Account
                        END AS Sort ,
                        GL.ReferenceID
                        AS RefIDSort,
                        0 AS SortOrder,
                        0 AS DetailPostOrder,
						MAX(GL.OrderPriority) as OrderPriority,
                        SUM(DebitAmount) as OrderTotal
                FROM      @tbDataGL GL /*edit by cuongpv [dbo].[GeneralLedger] -> @tbDataGL*/
                        LEFT JOIN dbo.AccountingObject ao ON ao.ID = GL.AccountingObjectID
                WHERE     gl.PostedDate BETWEEN @FromDate AND @ToDate
                        AND ISNULL(gl.AccountCorresponding,'') <> ''
                GROUP BY  GL.[ReferenceID] ,
                        GL.[TypeID] ,
                        GL.[PostedDate] ,
                        GL.[RefDate] ,
                        GL.[RefNo] ,
                        GL.No ,
                        GL.[InvoiceDate] ,
                        GL.[InvoiceNo] ,
                        GL.[Reason] ,
                        GL.[Account] ,
                        GL.[AccountCorresponding]
						/*gl.OrderPriority comment by cuongpv*/
                                            
                HAVING    SUM(GL.[DebitAmount]) <> 0 /*edit by cuongpv bo (OR SUM(GL.[CreditAmount]) <> 0)*/
			UNION ALL 
			/*add by cuongpv*/
			SELECT  ROW_NUMBER() OVER ( ORDER BY gl.PostedDate, gl.RefDate
						 		,CASE WHEN GL.TypeID = 4012 THEN 1 ELSE 0 END
								, GL.RefNo , GL.TypeID ) AS RowNum ,
                        CAST (1 AS BIT) AS IsSummaryRow ,
						CAST(0 AS BIT) AS IsBold ,
                        GL.[ReferenceID] ,
                        GL.[TypeID] ,
                        GL.[PostedDate] ,
                        GL.[RefDate] ,
                        GL.No,
                        GL.No ,
                        NULL AS RefOrder ,
                        GL.[InvoiceDate] ,
                        GL.[InvoiceNo] ,
                        GL.[Reason] AS JournalMemo, /*edit by cuongpv Description -> Reason*/
                        /*bổ sung trường diễn giải thông tin chung cr 137228*/              
						GL.Reason AS JournalMemoMaster ,/*edit by cuongpv Description -> Reason*/
                        GL.[Account] as AccountNumber ,
                        GL.[AccountCorresponding] as CorrespondingAccountNumber ,
						'' AS [RefTypeName],
                        '' AS AccountObjectCode ,
                        '' AS AccountObjectName ,
                        '' AS EmployeeCode ,
                        '' AS EmployeeName ,
                        '' AS ExpenseItemCode ,
                        '' AS ExpenseItemName ,
                        '' AS JobCode ,
                        '' AS JobName ,
                        '' AS ProjectWorkCode ,
                        '' AS ProjectWorkName ,
                        '' AS OrderNo ,
                        '' AS [PUContractCode] ,
                        '' AS [ContractCode] ,
                        '' AS ListItemCode ,
                        '' AS ListItemName ,
                        null AS DebitAmount ,
                        SUM(GL.[CreditAmount]) AS CreditAmount ,
                        '' as BranchName,
                        '' AS OrganizationUnitCode ,
                        '' AS OrganizationUnitName ,
                        '' AS MasterCustomField1 ,
                        '' AS MasterCustomField2 ,
                        '' AS MasterCustomField3 ,
                        '' AS MasterCustomField4 ,
                        '' AS MasterCustomField5 ,
                        '' AS MasterCustomField6 ,
                        '' AS MasterCustomField7 ,
                        '' AS MasterCustomField8 ,
                        '' AS MasterCustomField9 ,
                        '' AS MasterCustomField10 ,
                        '' AS CustomField1 ,
                        '' AS CustomField2 ,
                        '' AS CustomField3 ,
                        '' AS CustomField4 ,
                        '' AS CustomField5 ,
                        '' AS CustomField6 ,
                        '' AS CustomField7 ,
                        '' AS CustomField8 ,
                        '' AS CustomField9 ,
                        '' AS CustomField10 ,
                        '' AS UnResonableCost ,
                        CASE WHEN SUM(DebitAmount) <> 0
                                THEN Gl.Account
                                    + AccountCorresponding
                                WHEN SUM(CreditAmount) <> 0
                                THEN gl.AccountCorresponding
                                    + GL.Account
                        END AS Sort ,
                        GL.ReferenceID
                        AS RefIDSort,
                        1 AS SortOrder,
                        0 AS DetailPostOrder,
						MAX(GL.OrderPriority) as OrderPriority,
                        SUM(CreditAmount) as OrderTotal                    
                FROM      @tbDataGL GL /*edit by cuongpv [dbo].[GeneralLedger] -> @tbDataGL*/
                        LEFT JOIN dbo.AccountingObject ao ON ao.ID = GL.AccountingObjectID
                WHERE     gl.PostedDate BETWEEN @FromDate AND @ToDate
                        AND ISNULL(gl.AccountCorresponding,'') <> ''
                GROUP BY  GL.[ReferenceID] ,
                        GL.[TypeID] ,
                        GL.[PostedDate] ,
                        GL.[RefDate] ,
                        GL.[RefNo] ,
                        GL.No ,
                        GL.[InvoiceDate] ,
                        GL.[InvoiceNo] ,
                        GL.Reason ,
                        GL.[Account] ,
                        GL.[AccountCorresponding]
						/*gl.OrderPriority comment by cuongpv*/
                                            
                HAVING    SUM(GL.[CreditAmount]) <> 0
			/*end add by cuongpv*/
            ) t
			order by PostedDate,RefNo,OrderTotal,SortOrder,OrderPriority
			END
        ELSE 
            BEGIN
			select * from
				(

			SELECT  
								ROW_NUMBER() OVER ( ORDER BY gl.PostedDate, gl.RefDate
						 			,CASE WHEN GL.TypeID = 4012 THEN 1 ELSE 0 END
									, GL.RefNo , GL.TypeID ) AS RowNum ,							
								CAST (1 AS BIT) AS IsSummaryRow ,
								CAST(0 AS BIT) AS IsBold ,
								GL.[ReferenceID] ,
								GL.[TypeID] ,
								GL.[PostedDate] ,
								GL.[RefDate] ,
								GL.No AS [RefNo],
								GL.No AS RefNo1,
								'' AS RefOrder ,
								GL.[InvoiceDate] ,
								GL.[InvoiceNo] ,
								GL.Description AS [JournalMemo] ,
								/* bổ sung trường diễn giải thông tin chung cr 137228*/              
								GL.Reason AS JournalMemoMaster ,
								GL.[Account] as AccountNumber ,
								GL.[AccountCorresponding] as CorrespondingAccountNumber ,
								'' as RefTypeName,
								ao.AccountingObjectCode ,
								ao.AccountingObjectName ,
								'' as EmployeeCode,
								'' as EmployeeName,
								'' as ExpenseItemCode ,
								'' as ExpenseItemName ,
								'' as JobCode,
								'' as JobName,
								'' as ProjectWorkCode ,
								'' as ProjectWorkName ,
								'' as OrderNo,
								'' as PUContractCode,
								'' as ContractCode ,
								'' as ListItemCode ,
								'' as ListItemName ,
								GL.[DebitAmount] ,
								GL.[CreditAmount] ,
								'' as BranchName,
								'' as OrganizationUnitCode,
								'' as OrganizationUnitName,
								'' as MasterCustomField1,
								'' as MasterCustomField2,
								'' as MasterCustomField3,
								'' as MasterCustomField4,
								'' as MasterCustomField5,
								'' as MasterCustomField6,
								'' as MasterCustomField7,
								'' as MasterCustomField8,
								'' as MasterCustomField9,
								'' as MasterCustomField10,
								'' as CustomField1,
								'' as CustomField2,
								'' as CustomField3,
								'' as CustomField4,
								'' as CustomField5,
								'' as CustomField6,
								'' as CustomField7,
								'' as CustomField8,
								'' as CustomField9,
								'' as CustomField10,
								N'Chi phí hợp lý' AS UnResonableCost ,
								CASE WHEN DebitAmount <> 0
										THEN Gl.Account
											+ AccountCorresponding
											+ CAST(DebitAmount AS NVARCHAR(22))
										WHEN CreditAmount <> 0
										THEN gl.AccountCorresponding
											+ GL.Account
											+ CAST(CreditAmount AS NVARCHAR(22))
								END AS Sort ,
								GL.ReferenceID
								AS RefIDSort,
								0 as SortOrder,
								0 as DetailPostOrder,
								GL.OrderPriority
						FROM      @tbDataGL GL /*edit by cuongpv [dbo].[GeneralLedger] -> @tbDataGL*/
								LEFT JOIN dbo.AccountingObject ao ON ao.ID = GL.AccountingObjectID
						WHERE     gl.PostedDate BETWEEN @FromDate AND @ToDate
								AND ISNULL(gl.AccountCorresponding,
											'') <> ''
								AND ( GL.[DebitAmount] <> 0
										OR GL.[CreditAmount] <> 0
									)
				 ) t
				order by t.PostedDate, t.OrderPriority
            END 
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE PROCEDURE [dbo].[Proc_S02C1DNN]
    @BranchID UNIQUEIDENTIFIER,
    @IncludeDependentBranch BIT,
    @StartDate DATETIME,
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountNumber NVARCHAR(MAX),
    @IsSimilarSum BIT
AS
    BEGIN
        SET NOCOUNT ON
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)
		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		CREATE TABLE #Result
            (
             RefID UNIQUEIDENTIFIER,
             RefType INT,
             PostedDate DATETIME,
             RefNo NVARCHAR(25),
             RefDate DATETIME,
             InvDate DATETIME,
             InvNo NVARCHAR(MAX),
             JournalMemo NVARCHAR(255),
             AccountNumber NVARCHAR(25),
			 DetailAccountNumber NVARCHAR(25) ,
             AccountCategoryKind INT,
             AccountName NVARCHAR(255),
             CorrespondingAccountNumber NVARCHAR(25),
             DebitAmount MONEY,
             CreditAmount MONEY,
             OrderType INT,
             IsBold BIT,
             OrderNumber int
            )
        CREATE TABLE #tblAccountNumber
            (
             AccountNumber NVARCHAR(25)    PRIMARY KEY,
             AccountName NVARCHAR(255)  ,
             AccountNumberPercent NVARCHAR(25)  ,
             AccountCategoryKind INT,
			 IsParent BIT
            )
        INSERT  #tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountName,
                        A1.AccountNumber + '%',
                        A1.AccountGroupKind,
						A1.IsParentNode
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        IF @IsSimilarSum = 0 /*khong cong gop but ke toan*/
		Begin
            INSERT  #Result
                    (
                     RefID,
                     RefType,
                     PostedDate,
                     RefNo,
                     RefDate,
                     InvDate,
                     InvNo,
                     JournalMemo,
                     AccountNumber,
					 DetailAccountNumber,
                     AccountName,
                     CorrespondingAccountNumber,
                     AccountCategoryKind,
                     DebitAmount,
                     CreditAmount,
                     ORDERType,
                     IsBold,
                     OrderNumber
                    )
                    SELECT
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Description AS JournalMemo,
                            AC.AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind,
                            DebitAmount,
                            CreditAmount,
                            1 AS ORDERType,
                            0,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%')
											AND GL.DebitAmount <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND (GL.DebitAmount <> 0
                                 OR GL.CreditAmount <> 0
                                 or GL.DebitAmountOriginal <> 0
                                 OR GL.CreditAmountOriginal <> 0
                                )
              End
			  Else
			  Begin /*cong gop but toan thue*/
				INSERT  #Result(RefID, RefType, PostedDate, RefNo, RefDate, InvDate, InvNo, JournalMemo, AccountNumber, DetailAccountNumber, AccountName, CorrespondingAccountNumber,
                     AccountCategoryKind, DebitAmount, CreditAmount, ORDERType, IsBold, OrderNumber)
                SELECT RefID, RefType, PostedDate, RefNo, RefDate, InvDate, InvNo, JournalMemo, AccountNumber, DetailAccountNumber, AccountName, CorrespondingAccountNumber,
                     AccountCategoryKind, DebitAmount, CreditAmount, ORDERType, IsBold, OrderNumber
				FROM
				    (SELECT
                            GL.ReferenceID as RefID,
                            GL.TypeID as RefType,
                            GL.PostedDate as PostedDate,
                            RefNo,
                            GL.RefDate as RefDate,
                            Gl.InvoiceDate as InvDate,
                            GL.InvoiceNo as InvNo,
                            GL.Reason AS JournalMemo,
                            AC.AccountNumber as AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName as AccountName,
                            GL.AccountCorresponding as CorrespondingAccountNumber,
                            AC.AccountCategoryKind as AccountCategoryKind,
                            SUM(GL.DebitAmount) as DebitAmount,
                            0 as CreditAmount,
                            1 AS ORDERType,
                            0 as IsBold,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%')
											AND SUM(GL.DebitAmount) <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
					GROUP BY
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Reason,
                            AC.AccountNumber,
							GL.Account,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind
					HAVING SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0
				UNION ALL
				SELECT
                            GL.ReferenceID as RefID,
                            GL.TypeID as RefType,
                            GL.PostedDate as PostedDate,
                            RefNo,
                            GL.RefDate as RefDate,
                            Gl.InvoiceDate as InvDate,
                            GL.InvoiceNo as InvNo,
                            GL.Reason AS JournalMemo,
                            AC.AccountNumber as AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName as AccountName,
                            GL.AccountCorresponding as CorrespondingAccountNumber,
                            AC.AccountCategoryKind as AccountCategoryKind,
                            0 as DebitAmount,
                            SUM(GL.CreditAmount) as CreditAmount,
                            1 AS ORDERType,
                            0 as IsBold,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%')
											AND SUM(GL.CreditAmount) <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
					GROUP BY
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Reason,
                            AC.AccountNumber,
							GL.Account,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind
					HAVING SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0
				) T
				where T.DebitAmount > 0 OR T.CreditAmount>0
			  End
        SELECT  AccountNumber,
                AccountName,
				IsParent,
                SUM(OpenningDebitAmount) AS OpenningDebitAmount,
                SUM(OpenningCreditAmount) AS OpenningCreditAmount,
                SUM(ClosingDebitAmount) AS ClosingDebitAmount,
                SUM(ClosingCreditAmount) AS ClosingCreditAmount,
                SUM(AccumDebitAmount) AS AccumDebitAmount,
                SUM(AccumCreditAmount) AS AccumCreditAmount,
				AccountCategoryKind
        FROM    (
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS OpenningDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate < @FromDate
                 GROUP BY GL.BranchID,
                        AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS ClosingDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate <= @ToDate
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        SUM(GL.DebitAmount) AS AccumDebitAmount,
                        SUM(GL.CreditAmount) AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate BETWEEN @StartDate AND @ToDate
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                ) AS A
        GROUP BY AccountNumber,
                AccountName,
				IsParent ,
				AccountCategoryKind
        HAVING
				SUM(OpenningDebitAmount) <>0 OR
                SUM(OpenningCreditAmount)  <>0 OR
                SUM(ClosingDebitAmount)  <>0 OR
                SUM(ClosingCreditAmount) <>0 OR
                SUM(AccumDebitAmount) <>0 OR
                SUM(AccumCreditAmount) <>0
        ORDER BY AccountNumber
		SELECT  ROW_NUMBER() OVER (ORDER BY AccountNumber,DetailAccountNumber, OrderType, PostedDate, RefDate,OrderNumber, RefNo) AS RowNum,
				RefID,
				RefType,
				PostedDate,
				RefDate,
				RefNo,
				InvDate,
				InvNo,
				JournalMemo,
				AccountNumber,
				DetailAccountNumber,
				CorrespondingAccountNumber,
				DebitAmount,
				CreditAmount,
				IsBold,
				AccountCategoryKind
		FROM    #Result
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE PROCEDURE [dbo].[Proc_SoTongHopLuongNhanVien]
	@BeginMonth int,
	@BeginYear int,
	@EndMonth int,
	@EndYear int,
    @AccountingObjectCode NVARCHAR(MAX)
AS
BEGIN     
	DECLARE @tblSoTongHopLuongNhanVien TABLE(
			ID uniqueidentifier,
			AccountingObjectCode  NVARCHAR(25),
			AccountingObjectName NVARCHAR(512),
			InMonth int,
			InYear int,
			TaxCode NVARCHAR(50),
			IdentificationNo NVARCHAR(25),
			TotalPersonalTaxIncomeAmount decimal(25,0),
			GiamTruGiaCanh decimal(25,0),
			BaoHiemDuocTru decimal(25,0),
			IncomeForTaxCalcuation  decimal(25,0),
			IncomeTaxAmount  decimal(25,0),
			NetAmount  decimal(25,0)
			)
	Declare @NgayThang Date
	DECLARE @Count int
	DECLARE @tblListAccountingObjectCode TABLE
	(
	AccountingObjectCode NVARCHAR(MAX)
	)

		 INSERT  INTO @tblListAccountingObjectCode
         SELECT  TG.ID
         FROM    AccountingObject AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountingObjectCode,',') AS AccountingObjectCode ON TG.ID = AccountingObjectCode.Value
         WHERE  AccountingObjectCode.Value IS NOT NULL
		 
		 SELECT a.EmployeeID,b.AccountingObjectCode , b.AccountingObjectName ,Month as InMonth, Year as InYear,b.TaxCode,b.IdentificationNo, a.TotalPersonalTaxIncomeAmount ,(a.ReduceSelfTaxAmount + a.ReduceDependTaxAmount ) as GiamTruGiaCanh,(a.EmployeeSocityInsuranceAmount  + a.EmployeeAccidentInsuranceAmount  + a.EmployeeMedicalInsuranceAmount + a.EmployeeUnEmployeeInsuranceAmount + a.EmployeeTradeUnionInsuranceAmount ) as BaoHiemDuocTru , a.IncomeForTaxCalcuation  , a.IncomeTaxAmount  ,a.NetAmount 
		 into #tg
		FROM [PSSalarySheetDetail] a join [AccountingObject] b on a.EmployeeID = b.ID join [PSSalarySheet] c on a.PSSalarySheetID=c.ID
		 where employeeid in (Select AccountingObjectCode From @tblListAccountingObjectCode)
		 
		 DECLARE @ID uniqueidentifier
		 DECLARE @MONTH int
		 DECLARE @YEAR int
		 DECLARE @TG int
		 SET @TG = @BeginYear
		 WHILE(@BeginYear = @TG)
		 BEGIN
		  SET @Count = (SELECT COUNT(*) From #tg where  InMonth >= @BeginMonth and InMonth <= 12 AND InYear = @BeginYear)
		  print @Count
		 WHILE (@Count>0)
		 BEGIN
		  SET @ID = (SELECT TOP 1 EmployeeID FROM #tg where   InMonth >= @BeginMonth and InMonth <= 12 AND InYear = @BeginYear)
		 SET @MONTH = (SELECT TOP 1 InMonth FROM #tg where   InMonth >= @BeginMonth and InMonth <= 12 AND InYear = @BeginYear)
		 SET @YEAR = (SELECT TOP 1 InYear FROM #tg where   InMonth >= @BeginMonth and InMonth <= 12 AND InYear = @BeginYear)
		 	INSERT INTO @tblSoTongHopLuongNhanVien
			SELECT TOP 1 * FROM #tg where     InMonth >= @BeginMonth and InMonth <= 12 AND InYear = @BeginYear
			DELETE FROM #tg WHERE @ID = EmployeeID AND InMonth = @MONTH and InYear = @YEAR
			SET @Count = @Count - 1
		END
		SET @BeginYear = @BeginYear+1
		END
		
		 While(@BeginYear<=@EndYear)
		 BEGIN
		 SET @Count = (SELECT COUNT(*) From #tg where   InMonth >= 1 and InMonth <= @EndMonth  AND InYear = @BeginYear)
		 WHILE(@Count>0)
		 BEGIN
		 IF(@BeginYear = @EndYear)
		 BEGIN
		  SET @ID = (SELECT TOP 1 EmployeeID FROM #tg where   InMonth >= 1 and InMonth <= @EndMonth  AND InYear = @BeginYear)
		 SET @MONTH = (SELECT TOP 1 InMonth FROM #tg where   InMonth >= 1  and InMonth <= @EndMonth  AND InYear = @BeginYear)
		 SET @YEAR = (SELECT TOP 1 InYear FROM #tg where   InMonth >= 1 and InMonth <= @EndMonth  AND InYear = @BeginYear)
		 	INSERT INTO @tblSoTongHopLuongNhanVien
			SELECT TOP 1 * FROM #tg where     InMonth >= @BeginMonth and InMonth <= 12 AND InYear = @BeginYear
			DELETE FROM #tg WHERE @ID = EmployeeID AND InMonth = @MONTH and InYear = @YEAR
			SET @Count = @Count - 1
		 END
		 ELSE
		 BEGIN
		 SET @ID = (SELECT TOP 1 EmployeeID FROM #tg where   InMonth >= 1 and InMonth <= 12  AND InYear = @BeginYear)
		 SET @MONTH = (SELECT TOP 1 InMonth FROM #tg where   InMonth >= 1  and InMonth <= 12  AND InYear = @BeginYear)
		 SET @YEAR = (SELECT TOP 1 InYear FROM #tg where   InMonth >= 1 and InMonth <= 12  AND InYear = @BeginYear)
		 	INSERT INTO @tblSoTongHopLuongNhanVien
			SELECT TOP 1 * FROM #tg where     InMonth >= @BeginMonth and InMonth <= 12 AND InYear = @BeginYear
			DELETE FROM #tg WHERE @ID = EmployeeID AND InMonth = @MONTH and InYear = @YEAR
			SET @Count = @Count - 1
			END
		END
		
		SET @BeginYear = @BeginYear+1
	 END
	
		Select ID,AccountingObjectCode,AccountingObjectName, TaxCode,IdentificationNo,Sum(TotalPersonalTaxIncomeAmount) as TotalPersonalTaxIncomeAmount ,Sum(GiamTruGiaCanh) as GiamTruGiaCanh,Sum(BaoHiemDuocTru) as BaoHiemDuocTru,Sum(IncomeForTaxCalcuation) as IncomeForTaxCalcuation,Sum(IncomeTaxAmount) as IncomeTaxAmount,Sum(NetAmount) as NetAmount From @tblSoTongHopLuongNhanVien where TotalPersonalTaxIncomeAmount >0 group by ID,AccountingObjectCode,AccountingObjectName,TaxCode,IdentificationNo 
		order by AccountingObjectCode
END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE PROCEDURE [dbo].[Proc_SoTongHopDoanhThuTheoNhanVien]
	@FromDate DATETIME,
    @ToDate DATETIME,
    @AccountingObjectID NVARCHAR(MAX),
	@MaterialGoodsID NVARCHAR(MAX)
AS
BEGIN     
	DECLARE @tblSoTongHopDoanhThuTheoNV TABLE(
			AccountingObjectID uniqueidentifier,
			AccountingObjectCode  NVARCHAR(25),
			AccountingObjectName NVARCHAR(512),
			MaterialGoodsID uniqueidentifier,
			MaterialGoodsCode  NVARCHAR(25),
			MaterialGoodsName NVARCHAR(512),
			DoanhSo decimal(25,0),
			GiamTruDoanhThu decimal (25,0)
			)
	DECLARE @tbltemp TABLE(
			AccountingObjectID uniqueidentifier,
			MaterialGoodsID uniqueidentifier,
			DoanhSo decimal(25,0),
			GiamTruDoanhThu decimal (25,0)
			)
	DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate >= @FromDate and GL.PostedDate <= @ToDate


	DECLARE @tblListAccountingObjectID TABLE
	(
	AccountingObjectID uniqueidentifier
	)
	
		 INSERT  INTO @tblListAccountingObjectID
         SELECT  TG.ID
         FROM    AccountingObject AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountingObjectID,',') AS AccountingObjectID ON TG.ID = AccountingObjectID.Value
         WHERE  AccountingObjectID.Value IS NOT NULL


	DECLARE @tblListMaterialGoodsID TABLE
	(
	MaterialGoodsID uniqueidentifier
	)
	
		 INSERT  INTO @tblListMaterialGoodsID
         SELECT  TG.ID
         FROM    MaterialGoods AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@MaterialGoodsID,',') AS MaterialGoodsID ON TG.ID = MaterialGoodsID.Value
         WHERE  MaterialGoodsID.Value IS NOT NULL
		INSERT INTO @tbltemp (AccountingObjectID,MaterialGoodsID,DoanhSo,GiamTruDoanhThu)
		SELECT a.employeeid,c.MaterialGoodsID, a.CreditAmount,a.DebitAmount
		FROM @tbDataGL a
		join [SAInvoiceDetail] c ON a.DetailID = c.ID
		 where a.employeeid in (Select AccountingObjectID From @tblListAccountingObjectID)
		 and c.MaterialGoodsID in (Select MaterialGoodsID From @tblListMaterialGoodsID)
		 And Account like '511%'
		 and PostedDate >= @FromDate and PostedDate <= @ToDate
		 INSERT INTO @tbltemp (AccountingObjectID,MaterialGoodsID,DoanhSo,GiamTruDoanhThu)
		SELECT a.employeeid,c.MaterialGoodsID,a.CreditAmount, a.DebitAmount
		FROM @tbDataGL a 
		join [SAReturnDetail] c ON a.DetailID = c.ID
		 where a.employeeid in (Select AccountingObjectID From @tblListAccountingObjectID)
		 and c.MaterialGoodsID in (Select MaterialGoodsID From @tblListMaterialGoodsID)
		 And Account like '511%'
		 and PostedDate >= @FromDate and PostedDate <= @ToDate

	
	INSERT INTO @tblSoTongHopDoanhThuTheoNV(AccountingObjectID,MaterialGoodsID,DoanhSo,GiamTruDoanhThu)
		SELECT a.AccountingObjectID,a.MaterialGoodsID,SUM(a.DoanhSo), SUM(a.GiamTruDoanhThu)
		FROM @tbltemp a
		GROUP BY AccountingObjectID,a.MaterialGoodsID		

	Update @tblSoTongHopDoanhThuTheoNV 
	Set MaterialGoodsCode = k.MaterialGoodsCode, MaterialGoodsName = k.MaterialGoodsName
	From (Select ID,MaterialGoodsCode, MaterialGoodsName From MaterialGoods ) k
	where MaterialGoodsID=k.ID

	Update @tblSoTongHopDoanhThuTheoNV 
	Set AccountingObjectCode = k.AccountingObjectCode, AccountingObjectName = k.AccountingObjectName
	From (Select ID,AccountingObjectCode, AccountingObjectName From AccountingObject ) k
	where AccountingObjectID=k.ID

	Select AccountingObjectID,AccountingObjectCode,AccountingObjectName,MaterialGoodsID,MaterialGoodsCode,MaterialGoodsName,DoanhSo,GiamTruDoanhThu
	 From @tblSoTongHopDoanhThuTheoNV
	 Order By MaterialGoodsCode
END


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE PROCEDURE [dbo].[Proc_SoTongHopCongTrinh]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @CostSetID  NVARCHAR(MAX)
AS
BEGIN     
	DECLARE @tblSoTongHopCongTrinh TABLE(
			CostSetID uniqueidentifier,
			CostSetCode  NVARCHAR(25),
			CostSetName NVARCHAR(512),
			DoanhThu decimal(25,0),
			 GiamTru decimal(25,0),
			 ChiPhi decimal(25,0),
			 GiaVon decimal(25,0),
			 DoDang decimal(25,0),
			 LaiLo decimal(25,0))
	DECLARE @tblSoTongHopCTVV TABLE(
			CostSetID uniqueidentifier,
			CostSetCode  NVARCHAR(25),
			CostSetName NVARCHAR(512),	
			DoanhThu decimal(25,0),
			 GiamTru decimal(25,0),
			 ChiPhi decimal(25,0),
			 GiaVon decimal(25,0),
			 DoDang decimal(25,0),
			 LaiLo decimal(25,0)) 

				DECLARE @tbDataGL TABLE(
				ID uniqueidentifier,
				BranchID uniqueidentifier,
				ReferenceID uniqueidentifier,
				TypeID int,
				Date datetime,
				PostedDate datetime,
				No nvarchar(25),
				InvoiceDate datetime,
				InvoiceNo nvarchar(25),
				Account nvarchar(25),
				AccountCorresponding nvarchar(25),
				BankAccountDetailID uniqueidentifier,
				CurrencyID nvarchar(3),
				ExchangeRate decimal(25, 10),
				DebitAmount decimal(25,0),
				DebitAmountOriginal decimal(25,0),
				CreditAmount decimal(25,0),
				CreditAmountOriginal decimal(25,0),
				Reason nvarchar(512),
				Description nvarchar(512),
				VATDescription nvarchar(512),
				AccountingObjectID uniqueidentifier,
				EmployeeID uniqueidentifier,
				BudgetItemID uniqueidentifier,
				CostSetID uniqueidentifier,
				ContractID uniqueidentifier,
				StatisticsCodeID uniqueidentifier,
				InvoiceSeries nvarchar(25),
				ContactName nvarchar(512),
				DetailID uniqueidentifier,
				RefNo nvarchar(25),
				RefDate datetime,
				DepartmentID uniqueidentifier,
				ExpenseItemID uniqueidentifier,
				OrderPriority int,
				IsIrrationalCost bit
			)

			INSERT INTO @tbDataGL
			SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
	
	DECLARE @tblListCostSetID TABLE
	(
	CostSetID uniqueidentifier
	)


		 INSERT  INTO @tblListCostSetID
         SELECT  TG.id
         FROM    CostSet AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@CostSetID,',') AS CostSetID ON TG.ID = CostSetID.Value
         WHERE  CostSetID.Value IS NOT NULL

		 INSERT INTO @tblSoTongHopCongTrinh SELECT DISTINCT(CostSetID),null,null,0,0,0,0,0,0
		 From @tbDataGL a,CostSet b  WHERE  a.CostSetID IN (Select * From @tblListCostSetID)  AND CostSetType=1 AND a.CostSetID=b.ID  AND CostSetID IN (Select * From @tblListCostSetID)
		 AND PostedDate >= @FromDate AND PostedDate<@ToDate
		 
		
		 DECLARE @tblDoanhThu TABLE
		 (
			CostSetID uniqueidentifier,
			DoanhThu decimal(25,0)
		 )
		 INSERT INTO @tblDoanhThu Select CostSetID,Sum(CreditAmount) 
		 From @tbDataGL a
		 WHERE a.CostSetID IN (Select * From @tblListCostSetID)  AND Account LIKE '511%' AND PostedDate >= @FromDate AND PostedDate<@ToDate
		 group by CostSetID
		

		
	UPDATE @tblSoTongHopCongTrinh SET DoanhThu = f.DoanhThu
	FROM (select CostSetID as csid, DoanhThu from @tblDoanhThu) f
	WHERE CostSetID = f.csid
		
	 DECLARE @tblGiamTru TABLE
		 (
			CostSetID uniqueidentifier,
			GiamTru decimal(25,0)
		 )
		 INSERT INTO @tblGiamTru Select CostSetID,Sum(DebitAmount) 
		 From @tbDataGL a
		 WHERE a.CostSetID IN (Select * From @tblListCostSetID) AND Account LIKE '511%' AND PostedDate >= @FromDate AND PostedDate<=@ToDate
		 group by CostSetID
		
		
	UPDATE @tblSoTongHopCongTrinh SET GiamTru = f.GiamTru
	FROM (select CostSetID as csid, GiamTru from @tblGiamTru) f
	WHERE CostSetID = f.csid
	
		 DECLARE @tblChiPhi TABLE
		 (
			CostSetID uniqueidentifier,
			ChiPhi decimal(25,0)
		 )	 
		 
		  INSERT INTO @tblChiPhi Select CostSetID,Sum(DebitAmount)
		 From @tbDataGL 
		 WHERE CostSetID in (Select * from @tblListCostSetID) AND Account like '154%' AND PostedDate >= @FromDate AND PostedDate<=@ToDate
		 group by CostSetID
		

		  DECLARE @tblGiamTruCP TABLE
		 (
			CostSetID uniqueidentifier,
			GiamTruCP decimal(25,0)
		 )	 
		
		  INSERT INTO @tblGiamTruCP Select CostSetID,Sum(CreditAmount) 
		 From @tbDataGL a
		 WHERE a.CostSetID IN (SELECT * from @tblListCostSetID) AND PostedDate >= @FromDate AND PostedDate<=@ToDate AND Account LIKE '154%' AND AccountCorresponding NOT IN ('632%')
		 group by CostSetID
	

		 	  DECLARE @tblChiPhiPhatSinh TABLE
		 (
			CostSetID uniqueidentifier,
			ChiPhiPhatSinh decimal(25,0)
		 )	
		  	  DECLARE @tbltg TABLE
		 (
			CostSetID uniqueidentifier,
			ChiPhi decimal(25,0),
			GiamTruCP decimal(25,0),
			ChiPhiChung decimal(25,0)
		 )	 
		 INSERT INTO @tbltg SELECT CostSetID,ChiPhi,null,null From @tblChiPhi
		 UPDATE @tbltg SET GiamTruCP = f.GiamTruCP
	FROM (select CostSetID as csid, GiamTruCP from @tbltg) f
	WHERE CostSetID = f.csid 
	
	DECLARE @tblCPChung TABLE
	(
	CostSetId uniqueidentifier,
	ReferenceID uniqueidentifier,
	CPAllocationGeneralExpenseID uniqueidentifier,
	AllocatedAmount decimal(25,0),
	PostedDate Date
	)
	INSERT INTO @tblCPChung 
	Select CostSetID,null,CPAllocationGeneralExpenseID,AllocatedAmount,null From CPAllocationGeneralExpenseDetail WHERE CostSetID IN (SELECT * from @tblListCostSetID)

	DECLARE @ReferenceID TABLE
	(
	ID uniqueidentifier,
	ReferenceID uniqueidentifier
	)
	INSERT INTO @ReferenceID 
	SELECT ID,ReferenceID From [CPAllocationGeneralExpense]

	UPDATE @tblCPChung SET ReferenceID = f.ReferenceID
	FROM (SELECT ID,ReferenceID From @ReferenceID) f
	WHERE CPAllocationGeneralExpenseID = f.ID

	DECLARE @DetailID TABLE 
	(
	CostSetID uniqueidentifier,
	DetailID uniqueidentifier,
	PostedDate date	
	)
	INSERT INTO @DetailID 
	SELECT CostSetID,DetailId,PostedDate From [GeneralLedger] 
	WHERE PostedDate >=@FromDate AND PostedDate <=@ToDate AND CostSetID IN (SELECT * from @tblListCostSetID)


	UPDATE @tblCPChung SET PostedDate = f.PostedDate
	FROM (SELECT CostSetID as CSI,DetailID as DI,PostedDate From @DetailID) f
	WHERE  ReferenceID = f.DI
	


	 UPDATE @tbltg SET ChiPhiChung = f.CPC
	FROM (select CostSetID as csid, Sum(AllocatedAmount) as CPC from @tblCPChung group by CostSetId) f
	WHERE CostSetID = f.csid 
	

		 INSERT INTO @tblChiPhiPhatSinh Select CostSetID,(ISNULL(ChiPhi,0) - ISNULL(GiamTruCP,0) + ISNULL(ChiPhiChung,0)) as ChiPhiPhatSinh
		 From @tbltg

	
		
	UPDATE @tblSoTongHopCongTrinh SET ChiPhi = f.ChiPhiPhatSinh
	FROM (select CostSetID as csid, ChiPhiPhatSinh from @tblChiPhiPhatSinh) f
	WHERE CostSetID = f.csid 
	
		INSERT INTO @tblSoTongHopCTVV SELECT DISTINCT(CostSetID),CostSetCode,CostSetName,SUM(DoanhThu),SUM(GiamTru),SUM(ChiPhi),GiaVon,DoDang,LaiLo From @tblSoTongHopCongTrinh
		group by CostSetID,CostSetCode,CostSetName,GiaVon,DoDang,LaiLo
		
		 DECLARE @tblGiaVon TABLE
		 (
			CostSetID uniqueidentifier,
			GiaVon decimal(25,0)
		 )
		 INSERT INTO @tblGiaVon Select CostSetID,Sum(DebitAmount)
		 From @tbDataGL a, CostSet b
		 WHERE a.CostSetID = b.ID  AND Account LIKE '632%' AND PostedDate >= @FromDate AND PostedDate<=@ToDate AND AccountCorresponding  LIKE '154%'
		 group by CostSetID,a.AccountingObjectID
		
	
		
	UPDATE @tblSoTongHopCTVV SET GiaVon = f.GiaVon
	FROM (select CostSetID as csid, GiaVon from @tblGiaVon) f
	WHERE CostSetID = f.csid 

	UPDATE @tblSoTongHopCTVV SET DoDang = (ChiPhi - GiaVon), LaiLo = (DoanhThu - GiamTru - GiaVon)

	 DECLARE @tblInfo TABLE
		 (
			CostSetID uniqueidentifier,
			CostSetCode  NVARCHAR(25),
			CostSetName NVARCHAR(512)
		 )
		 INSERT INTO @tblInfo Select CostSetID,CostSetCode,CostSetName
		 From @tbDataGL a, CostSet b
		 WHERE a.CostSetID = b.ID  AND PostedDate >= @FromDate AND PostedDate<=@ToDate 
	
	UPDATE @tblSoTongHopCTVV SET CostSetCode = f.CostSetCode, CostSetName = f.CostSetName
	FROM (select CostSetID as csid, CostSetCode,CostSetName from @tblInfo) f
	WHERE CostSetID = f.csid 

	SELECT * from @tblSoTongHopCTVV



END




 



GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE PROCEDURE [dbo].[Proc_SoTheoDoiDoiTuongTHCP]
	@FromDate DATETIME,
    @ToDate DATETIME,
    @CostSetID  NVARCHAR(MAX),
	@ExpenseItemID  NVARCHAR(MAX)
AS
BEGIN
	DECLARE @temp TABLE(
			CostSetID uniqueidentifier,
			ExpenseItemID uniqueidentifier,	 
			SoDauKyNo decimal(25,0),
			SoDauKyCo decimal(25,0),
			SoPhatSinhNo decimal(25,0),
			SoPhatSinhCo decimal(25,0)
	)
	DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)
		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
	DECLARE @tblListCostSetID TABLE
	(
	CostSetID uniqueidentifier
	)
		 INSERT  INTO @tblListCostSetID
         SELECT  TG.id
         FROM    CostSet AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@CostSetID,',') AS CostSetID ON TG.ID = CostSetID.Value
         WHERE  CostSetID.Value IS NOT NULL
	DECLARE @tblListExpenseItemID TABLE
	(
	ExpenseItemID uniqueidentifier
	)
		 INSERT  INTO @tblListExpenseItemID
         SELECT  TG.id
         FROM    ExpenseItem AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@ExpenseItemID,',') AS ExpenseItemID ON TG.ID = ExpenseItemID.Value
         WHERE  ExpenseItemID.Value IS NOT NULL
		INSERT INTO @temp (CostSetID,ExpenseItemID ,SoDauKyNo)
		SELECT a.CostSetID,a.ExpenseItemID ,Sum(DebitAmount)
		FROM @tbDataGL a 
		where CostSetID in (select CostSetID from @tblListCostSetID)
		and ExpenseItemID in (select ExpenseItemID from @tblListExpenseItemID)
		and Date < @FromDate and Account like '111%'
		Group By a.CostSetID,a.ExpenseItemID
	  INSERT INTO @temp (CostSetID,ExpenseItemID ,SoDauKyCo)
		SELECT a.CostSetID,a.ExpenseItemID ,Sum(CreditAmount)
		FROM @tbDataGL a
		where CostSetID in (select CostSetID from @tblListCostSetID)
		and ExpenseItemID in (select ExpenseItemID from @tblListExpenseItemID)
		and Date < @FromDate and Account like '111%' and AccountCorresponding not in ('133%')
		Group By a.CostSetID,a.ExpenseItemID
		
		INSERT INTO @temp (CostSetID,ExpenseItemID ,SoPhatSinhNo)
		SELECT a.CostSetID,a.ExpenseItemID ,Sum(DebitAmount)
		FROM @tbDataGL a 
		where CostSetID in (select CostSetID from @tblListCostSetID)
		and ExpenseItemID in (select ExpenseItemID from @tblListExpenseItemID)
		and Date >= @FromDate and Date <= @ToDate and Account like '111%'
		Group By a.CostSetID,a.ExpenseItemID
	  INSERT INTO @temp (CostSetID,ExpenseItemID ,SoPhatSinhCo)
		SELECT a.CostSetID,a.ExpenseItemID ,Sum(CreditAmount)
		FROM @tbDataGL a
		where CostSetID in (select CostSetID from @tblListCostSetID)
		and ExpenseItemID in (select ExpenseItemID from @tblListExpenseItemID)
		and Date >= @FromDate and Date <= @ToDate and Account like '111%' and AccountCorresponding not in ('133%')
		Group By a.CostSetID,a.ExpenseItemID
	DECLARE @tblSoTheoDoiDTTHCP TABLE(
			CostSetID uniqueidentifier,
			CostSetCode  NVARCHAR(25),
			CostSetName NVARCHAR(512),
			ExpenseItemID uniqueidentifier,
			ExpenseItemCode  NVARCHAR(25),
			ExpenseItemName NVARCHAR(512),	 
			SoDauKy decimal(25,0),
			SoPhatSinh decimal(25,0),
			LuyKeCuoiKy decimal(25,0)
	)
	INSERT INTO @tblSoTheoDoiDTTHCP (CostSetID,ExpenseItemID ,SoDauKy,SoPhatSinh)
		SELECT a.CostSetID,a.ExpenseItemID, Sum(a.SoDauKyNo - a.SoDauKyCo),Sum(a.SoPhatSinhNo - a.SoPhatSinhCo)
		FROM @temp a 
		Group By a.CostSetID,a.ExpenseItemID
	
	UPDATE @tblSoTheoDoiDTTHCP SET CostSetCode = k.CostSetCode, CostSetName = k.CostSetName
	FROM (select ID,CostSetCode,CostSetName from CostSet) k
	WHERE CostSetID = k.ID
	
	UPDATE @tblSoTheoDoiDTTHCP SET ExpenseItemCode = k.ExpenseItemCode, ExpenseItemName = k.ExpenseItemName
	FROM (select ID,ExpenseItemCode,ExpenseItemName from ExpenseItem) k
	WHERE ExpenseItemID = k.ID
		Select CostSetID,CostSetCode,CostSetName,ExpenseItemID, ExpenseItemCode, ExpenseItemName,ISNULL(SoDauKy,0) as SoDauKy,ISNULL(SoPhatSinh,0) as SoPhatSinh,ISNULL(SoDauKy,0)+ISNULL(SoPhatSinh,0) as LuyKeCuoiKy
		From @tblSoTheoDoiDTTHCP 
		order by CostSetCode
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE PROCEDURE [dbo].[Proc_SoTheoDoiChiTietTheoMaThongKe]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @StatisticsCodeID  NVARCHAR(MAX),
	@Account NVARCHAR(MAX)
AS
BEGIN     
	DECLARE @tblSoTheoDoiTheoMTK TABLE(
			StatisticsCodeID uniqueidentifier,
			StatisticsCode  NVARCHAR(25),
			StatisticsCodeName NVARCHAR(512),
			NgayChungTu Date,
			 SoChungTu NCHAR(20),
			 DienGiai NVARCHAR(MAX),
			 TK NVARCHAR(50),
			 TKDoiUng NVARCHAR(50),
			 SoTienNo decimal(25,0),
			 SoTienCo decimal(25,0),
			 OrderPriority int)
	
	
	DECLARE @tblListStatisticsCodeID TABLE
	(
	StatisticsCodeID uniqueidentifier
	)
	DECLARE @tblListAccount TABLE
	(
	Account NVARCHAR(25)
	)

		 INSERT  INTO @tblListStatisticsCodeID
         SELECT  TG.id
         FROM    StatisticsCode AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@StatisticsCodeID,',') AS StatisticCodeID ON TG.ID = StatisticCodeID.Value
         WHERE  StatisticCodeID.Value IS NOT NULL

		 INSERT  INTO @tblListAccount
         SELECT  TG.AccountNumber
         FROM    Account AS TG
                 LEFT JOIN dbo.Func_SplitString(@Account,',') AS Account ON TG.AccountNumber = Account.splitdata
         WHERE  Account.splitdata IS NOT NULL
                              			
		INSERT INTO @tblSoTheoDoiTheoMTK
		SELECT a.StatisticsCodeID, b.StatisticsCode, b.StatisticsCodeName ,Date as NgayChungTu , No as SoChungTu, Reason as DienGiai, Account as TK, AccountCorresponding as TKDoiUng, DebitAmount as SoTienNo, CreditAmount as SoTienCo, a.OrderPriority
		FROM [GeneralLedger] a 
		LEFT JOIN [StatisticsCode] b ON a.StatisticsCodeID = b.ID
		where StatisticsCodeID in (select StatisticsCodeID from @tblListStatisticsCodeID)   AND a.Account in (select Account from @tblListAccount)
		and Date > = @FromDate and Date < = @ToDate
		order by Date, a.OrderPriority

		Select * From @tblSoTheoDoiTheoMTK order by NgayChungTu,SoChungTu,OrderPriority
END


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE PROCEDURE [dbo].[Proc_SoTheoDoiChiTietTheoDoiTuongTHCP]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @CostSetID  NVARCHAR(MAX),
	@Account NVARCHAR(MAX)
AS
BEGIN     
	DECLARE @tblSoTheoDoiTheoDT TABLE(
			CostSetID uniqueidentifier,
			CostSetCode  NVARCHAR(25),
			CostSetName NVARCHAR(512),
			NgayChungTu Date,
			 SoChungTu NCHAR(20),
			 DienGiai NVARCHAR(MAX),
			 TK NVARCHAR(50),
			 TKDoiUng NVARCHAR(50),
			 SoTienNo decimal(25,0),
			 SoTienCo decimal(25,0),
			 OrderPriority int)
	
	
	DECLARE @tblListCostSetID TABLE
	(
	CostSetID uniqueidentifier
	)
	DECLARE @tblListAccount TABLE
	(
	Account NVARCHAR(25)
	)


		 INSERT  INTO @tblListCostSetID
         SELECT  TG.id
         FROM    CostSet AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@CostSetID,',') AS CostSetID ON TG.ID = CostSetID.Value
         WHERE  CostSetID.Value IS NOT NULL

	
		INSERT  INTO @tblListAccount
         SELECT  TG.AccountNumber
         FROM    Account AS TG
                 LEFT JOIN dbo.Func_SplitString(@Account,',') AS Account ON TG.AccountNumber = Account.splitdata
         WHERE  Account.splitdata IS NOT NULL
		
                              			
		INSERT INTO @tblSoTheoDoiTheoDT
		SELECT a.CostSetID, b.CostSetCode, b.CostSetName ,Date as NgayChungTu , No as SoChungTu, Reason as DienGiai, Account as TK, AccountCorresponding as TKDoiUng, DebitAmount as SoTienNo, CreditAmount as SoTienCo, a.OrderPriority
		FROM [GeneralLedger] a 
		LEFT JOIN [CostSet] b ON a.CostSetID = b.ID
		where CostSetID in (select CostSetID from @tblListCostSetID) AND a.Account in (select Account from @tblListAccount)
		and Date > = @FromDate and Date < = @ToDate
		order by Date, a.OrderPriority

		Select CostSetID,CostSetCode,CostSetName,NgayChungTu,SoChungTu,DienGiai,TK,TKDoiUng,SoTienNo,SoTienCo From @tblSoTheoDoiTheoDT order by NgayChungTu,SoChungTu,OrderPriority
END






GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE PROCEDURE [dbo].[Proc_SoChiTietDoanhThuTheoNhanVien]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountingObjectCode NVARCHAR(MAX)
AS
BEGIN     
	DECLARE @tblSoChiTietDoanhThuTheoNV TABLE(
			AccountingObjectCode  NVARCHAR(25),
			AccountingObjectName NVARCHAR(512),
			NgayChungTu Date,
			NgayHachToan Date,
			 SoChungTu NCHAR(20),
			 DienGiai NVARCHAR(MAX),
			 DoanhSo decimal(25,0),
			 GiamDoanhThu decimal (25,0),
			 GhiChu NVARCHAR(MAX))

	DECLARE @tblListAccountingObjectCode TABLE
	(
	AccountingObjectCode uniqueidentifier
	)
	
		 INSERT  INTO @tblListAccountingObjectCode
         SELECT  TG.ID
         FROM    AccountingObject AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountingObjectCode,',') AS AccountingObjectCode ON TG.ID = AccountingObjectCode.Value
         WHERE  AccountingObjectCode.Value IS NOT NULL

		INSERT INTO @tblSoChiTietDoanhThuTheoNV
		SELECT b.accountingobjectcode , b.accountingobjectname , a.date, a.postedDate , a.No , a.Description , a.CreditAmount , a.DebitAmount,''
		FROM [GeneralLedger] a join [AccountingObject] b on a.employeeid = b.id
		 where employeeid in (Select AccountingObjectCode From @tblListAccountingObjectCode)
		 And Account like '511%'
		 and PostedDate >= @FromDate and PostedDate <= @ToDate
	

		Select * From @tblSoChiTietDoanhThuTheoNV order by AccountingObjectCode,NgayChungTu
END


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

CREATE PROCEDURE [dbo].[Proc_SoChiTietCongTrinh]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @CostSetID  NVARCHAR(MAX)
AS
BEGIN     
	DECLARE @tblSoChiTietCongTrinh TABLE(
			CostSetID uniqueidentifier,
			CostSetCode  NVARCHAR(25),
			CostSetName NVARCHAR(512),	
			No Nvarchar(25),
			ExpenseItemID uniqueidentifier,
			PostedDate date,
			Date date,
			Reason NVARCHAR(512),
			DoanhThu decimal(25,0),
			 GiamTru decimal(25,0),
			NVLTT decimal(25,0),
			NCTT decimal(25,0),
			CPSXC decimal(25,0),
			Cong decimal(25,0),
			 GiaVon decimal(25,0),
			 LaiLo decimal(25,0),
			 ExpenseType int,
			 CostSetType int) 
	

				DECLARE @tbDataGL TABLE(
				ID uniqueidentifier,
				BranchID uniqueidentifier,
				ReferenceID uniqueidentifier,
				TypeID int,
				Date datetime,
				PostedDate datetime,
				No nvarchar(25),
				InvoiceDate datetime,
				InvoiceNo nvarchar(25),
				Account nvarchar(25),
				AccountCorresponding nvarchar(25),
				BankAccountDetailID uniqueidentifier,
				CurrencyID nvarchar(3),
				ExchangeRate decimal(25, 10),
				DebitAmount decimal(25,0),
				DebitAmountOriginal decimal(25,0),
				CreditAmount decimal(25,0),
				CreditAmountOriginal decimal(25,0),
				Reason nvarchar(512),
				Description nvarchar(512),
				VATDescription nvarchar(512),
				AccountingObjectID uniqueidentifier,
				EmployeeID uniqueidentifier,
				BudgetItemID uniqueidentifier,
				CostSetID uniqueidentifier,
				ContractID uniqueidentifier,
				StatisticsCodeID uniqueidentifier,
				InvoiceSeries nvarchar(25),
				ContactName nvarchar(512),
				DetailID uniqueidentifier,
				RefNo nvarchar(25),
				RefDate datetime,
				DepartmentID uniqueidentifier,
				ExpenseItemID uniqueidentifier,
				OrderPriority int,
				IsIrrationalCost bit
			)

			INSERT INTO @tbDataGL
			SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
	
	DECLARE @tblListCostSetID TABLE
	(
	CostSetID uniqueidentifier
	)

		 INSERT  INTO @tblListCostSetID
         SELECT  TG.id
         FROM    CostSet AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@CostSetID,',') AS CostSetID ON TG.ID = CostSetID.Value
         WHERE  CostSetID.Value IS NOT NULL

		 INSERT INTO @tblSoChiTietCongTrinh SELECT DISTINCT(CostSetID),null,null,a.No,a.ExpenseItemID,null,null,null,0,0,0,0,0,0,0,0,0,0
		 From @tbDataGL a
		 WHERE  a.CostSetID IN (Select * From @tblListCostSetID)
		 AND PostedDate >= @FromDate AND PostedDate<=@ToDate
	DECLARE @CostSetType TABLE
	(
	CostSetID uniqueidentifier,
	CostSetCode NVARCHAR(25),
	CostSetName NVARCHAR(512),
	CostSetType int
	)
	INSERT INTO @CostSetType SELECT ID,CostSetCode,CostSetName,CostSetType from CostSet a 
	WHERE ID IN (SELECT  * from @tblListCostSetID) AND CostSetType = 1
	UPDATE @tblSoChiTietCongTrinh SET CostSetType = f.CostSetType, CostSetCode = f.CostSetCode,CostSetName = f.CostSetName
	FROM (SELECT CostSetID as CSI,CostSetCode,CostSetName, CostSetType FROM @CostSetType) f
	WHERE CostSetID = f.CSI

	DECLARE @ExpenseType TABLE
	(
	ExpenseItemID uniqueidentifier,
	ExpenseType int
	)
	INSERT INTO @ExpenseType SELECT ID,ExpenseType from ExpenseItem a 
	WHERE ExpenseType IN (0,1,2)
	UPDATE @tblSoChiTietCongTrinh SET ExpenseType = f.ExpenseType
	FROM (SELECT ExpenseItemID as EII, ExpenseType FROM @ExpenseType) f
	WHERE ExpenseItemID = f.EII

	
		 DECLARE @tblDoanhThu TABLE
		 (
			CostSetID uniqueidentifier,
			No Nvarchar(25),
			DoanhThu decimal(25,0)
		 )
		 INSERT INTO @tblDoanhThu Select CostSetID,No,CreditAmount
		 From @tbDataGL a
		 WHERE a.CostSetID IN (Select * From @tblListCostSetID)  AND Account LIKE '511%' AND PostedDate >= @FromDate AND PostedDate<@ToDate
		

	
	UPDATE @tblSoChiTietCongTrinh SET DoanhThu = f.DoanhThu
	FROM (select CostSetID as csid,No as N, DoanhThu from @tblDoanhThu) f
	WHERE CostSetID = f.csid AND  No = f.N
	

		
		 DECLARE @tblGiamTru TABLE
		 (
			CostSetID uniqueidentifier,
			No Nvarchar(25),
			GiamTru decimal(25,0)
		 )
		 INSERT INTO @tblGiamTru Select CostSetID,No,DebitAmount
		 From @tbDataGL a
		 WHERE a.CostSetID IN (Select * From @tblListCostSetID)  AND Account LIKE '511%' AND PostedDate >= @FromDate AND PostedDate<@ToDate
		

	UPDATE @tblSoChiTietCongTrinh SET GiamTru = f.GiamTru
	FROM (select CostSetID as csid,No as N, GiamTru from @tblGiamTru) f
	WHERE CostSetID = f.csid AND  No = f.N
	
	
	
		 DECLARE @tblGiaVon TABLE
		 (
			CostSetID uniqueidentifier,
			No Nvarchar(25),
			GiaVon decimal(25,0)
		 )
		 INSERT INTO @tblGiaVon Select CostSetID,No,DebitAmount
		 From @tbDataGL a
		  WHERE a.CostSetID IN (SELECT * from @tblListCostSetID) AND Account LIKE '632%' AND PostedDate >= @FromDate AND PostedDate<=@ToDate AND AccountCorresponding  LIKE '154%'
		

	UPDATE @tblSoChiTietCongTrinh SET GiaVon = f.GiaVon
	FROM (select CostSetID as csid,No as N, GiaVon from @tblGiaVon) f
	WHERE CostSetID = f.csid AND  No = f.N
	
		

	
	

	 DECLARE @tblInfo TABLE
		 (
			CostSetID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			PostedDate Date,
			Date Date,
			No NVARCHAR(25),
			Reason NVARCHAR(512)
		 )
		 INSERT INTO @tblInfo Select CostSetID,a.ExpenseItemID,PostedDate,Date,No,Reason
		 From @tbDataGL a
		 WHERE a.CostSetID IN (SELECT * from @tblListCostSetID)  AND PostedDate >= @FromDate AND PostedDate<=@ToDate 
	
	
	UPDATE @tblSoChiTietCongTrinh SET  PostedDate = f.PostedDate,Date = f.Date,No = f.N,Reason = f.Reason
	FROM (select CostSetID as csid,ExpenseItemID as EII, PostedDate,Date,No as N,Reason from @tblInfo) f
	WHERE CostSetID = f.csid AND No = f.N

	UPDATE @tblSoChiTietCongTrinh SET LaiLo = (DoanhThu - GiamTru - GiaVon)
		 DECLARE @ChiPhiPhatSinh TABLE
		 (
			CostSetID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			SumDebit decimal(25,0),
			ExpenseType int
		 )
		  INSERT INTO @ChiPhiPhatSinh Select CostSetID,ExpenseItemID,Sum(DebitAmount) as SumDebit,b.ExpenseType
		 From @tbDataGL a, ExpenseItem b
		 WHERE a.CostSetID IN (select * from @tblListCostSetID) AND a.ExpenseItemID = b.ID  AND Account LIKE '154%' AND PostedDate >= @FromDate AND PostedDate<=@ToDate
		 group by CostSetID,ExpenseItemID,ExpenseType
		
		
	UPDATE @tblSoChiTietCongTrinh SET NVLTT = ISNULL(f.SumDebit,0)
	FROM (select CostSetID as csid,ExpenseItemID as  EII,SumDebit,ExpenseType as ET  from @ChiPhiPhatSinh) f
	WHERE CostSetID = f.csid AND ExpenseItemID = f.EII AND ExpenseType = f.ET AND ExpenseType = 0

	UPDATE @tblSoChiTietCongTrinh SET NCTT = ISNULL(f.SumDebit,0)
	FROM (select CostSetID as csid,ExpenseItemID as  EII,SumDebit,ExpenseType as ET  from @ChiPhiPhatSinh) f
	WHERE CostSetID = f.csid AND ExpenseItemID = f.EII AND ExpenseType = f.ET AND ExpenseType = 1

		UPDATE @tblSoChiTietCongTrinh SET CPSXC = ISNULL(f.SumDebit,0)
	FROM (select CostSetID as csid,ExpenseItemID as  EII,SumDebit,ExpenseType as ET  from @ChiPhiPhatSinh) f
	WHERE CostSetID = f.csid AND ExpenseItemID = f.EII AND ExpenseType = f.ET AND ExpenseType = 2

	DECLARE @tblCPChung TABLE
	(
	CostSetId uniqueidentifier,
	ReferenceID uniqueidentifier,
	CPAllocationGeneralExpenseID uniqueidentifier,
	AllocatedAmount decimal(25,0),
	PostedDate Date
	)
	INSERT INTO @tblCPChung 
	Select CostSetID,null,CPAllocationGeneralExpenseID,AllocatedAmount,null From CPAllocationGeneralExpenseDetail WHERE CostSetID IN (SELECT * from @tblListCostSetID)

	DECLARE @ReferenceID TABLE
	(
	ID uniqueidentifier,
	ReferenceID uniqueidentifier
	)
	INSERT INTO @ReferenceID 
	SELECT ID,ReferenceID From [CPAllocationGeneralExpense]

	UPDATE @tblCPChung SET ReferenceID = f.ReferenceID
	FROM (SELECT ID,ReferenceID From @ReferenceID) f
	WHERE CPAllocationGeneralExpenseID = f.ID

	DECLARE @DetailID TABLE 
	(
	CostSetID uniqueidentifier,
	DetailID uniqueidentifier,
	PostedDate date	
	)
	INSERT INTO @DetailID 
	SELECT CostSetID,DetailId,PostedDate From [GeneralLedger] 
	WHERE PostedDate >=@FromDate AND PostedDate <=@ToDate AND CostSetID IN (SELECT * from @tblListCostSetID)


	UPDATE @tblCPChung SET PostedDate = f.PostedDate
	FROM (SELECT CostSetID as CSI,DetailID as DI,PostedDate From @DetailID) f
	WHERE  ReferenceID = f.DI
	Select CostSetId,Sum(AllocatedAmount) as AllocatedAmount into #tg  from @tblCPChung group by CostSetId
	Insert into @tblSoChiTietCongTrinh
	Select CostSetID,null,null,null,null,PostedDate,Date,(N'Kỳ tính giá thành từ ' + CONVERT(NVARCHAR,@FromDate,103) + N' đến ' + CONVERT(NVARCHAR,PostedDate,103)) as Reason,0,0,0,0,0,0,0,0,2,1 From @tblSoChiTietCongTrinh WHERE Reason LIKE N'%Nghiệm thu kỳ%' group by CostSetId,PostedDate,Date
	UPDATE @tblSoChiTietCongTrinh SET CPSXC = f.AllocatedAmount
	FROM (Select CostSetID as CSI,AllocatedAmount From #tg) f
	WHERE CostSetID = f.CSI AND NO IS NULL
	UPDATE @tblSoChiTietCongTrinh SET Cong = (NVLTT + NCTT + CPSXC)
	 
	SELECT * from @tblSoChiTietCongTrinh order by CostSetID,Date,No



END






GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE PROCEDURE [dbo].[Proc_SA_TongHopCongNoNhanVien]
	@FromDate DATETIME,
    @ToDate DATETIME,
    @Account NVARCHAR(25),
    @AccountObjectID AS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @tbluutru TABLE (
		AccountingObjectID UNIQUEIDENTIFIER,
		AccountingObjectCode NVARCHAR(25),
		AccountingObjectName NVARCHAR(255),
		TK nvarchar(25),
		NoDK decimal(19,0),
		CoDK decimal(19,0),
		NoPS decimal(19,0),
		CoPS decimal(19,0),
		NoCK decimal(19,0),
		CoCK decimal(19,0)
    )
	DECLARE @tbAccountObjectID TABLE(
			AccountingObjectID UNIQUEIDENTIFIER
		)
	INSERT @tbAccountObjectID
	SELECT tblAccOjectSelect.Value as AccountingObjectID
	FROM dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') tblAccOjectSelect
	WHERE tblAccOjectSelect.Value in (SELECT ID FROM AccountingObject)
    INSERT INTO @tbluutru(AccountingObjectID, AccountingObjectCode, AccountingObjectName)
		SELECT ID as AccountingObjectID, AccountingObjectCode, AccountingObjectName
		FROM AccountingObject
		WHERE ID in (select AccountingObjectID from @tbAccountObjectID )

	DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate

	if(@Account='all')
	begin
		UPDATE @tbluutru SET NoPS = d.NoPS , CoPS = d.CoPS, TK = d.TK
		FROM (select c.MaNV,c.TenNV,c.TK,sum(c.NoPS) as NoPS, SUM(c.CoPS) as CoPS
				from
					(SELECT b.AccountingObjectCode as MaNV, b.AccountingObjectName as TenNV, a.DebitAmount as NoPS, a.CreditAmount as CoPS, a.Account as TK
					FROM @tbDataGL a join @tbluutru b on a.AccountingObjectID = b.AccountingObjectID
					where Date between @FromDate and @ToDate
					and a.Account in (select AccountNumber from Account where DetailType = '2')
			) c
			group by c.MaNV, c.TenNV, c.TK) d
			where AccountingObjectCode = d.MaNV
		UPDATE @tbluutru SET NoDK = d.NoDK , CoDK = d.CoDK, TK = d.TK
		FROM (select c.MaNV,c.TenNV,c.TK,sum(c.NoDK) as NoDK, SUM(c.CoDK) as CoDK
				from
					(SELECT b.AccountingObjectCode as MaNV, b.AccountingObjectName as TenNV, a.DebitAmount as NoDK, a.CreditAmount as CoDK, a.Account as TK
					FROM @tbDataGL a join @tbluutru b on a.AccountingObjectID = b.AccountingObjectID
					where Date < @FromDate
					and a.Account in (select AccountNumber from Account where DetailType = '2')
			) c
			group by c.MaNV, c.TenNV, c.TK) d
			where AccountingObjectCode = d.MaNV
	end
	else
	begin
		UPDATE @tbluutru SET NoPS = d.NoPS , CoPS = d.CoPS, TK = d.TK
		FROM (select c.MaNV,c.TenNV,c.TK,sum(c.NoPS) as NoPS, SUM(c.CoPS) as CoPS
				from
					(SELECT b.AccountingObjectCode as MaNV, b.AccountingObjectName as TenNV, a.DebitAmount as NoPS, a.CreditAmount as CoPS, a.Account as TK
					FROM @tbDataGL a join @tbluutru b on a.AccountingObjectID = b.AccountingObjectID
					where Date between @FromDate and @ToDate
					and a.Account LIKE (@Account +'%')
			) c
			group by c.MaNV, c.TenNV, c.TK) d
			where AccountingObjectCode = d.MaNV
		UPDATE @tbluutru SET NoDK = d.NoDK , CoDK = d.CoDK, TK = d.TK
		FROM (select c.MaNV,c.TenNV,c.TK,sum(c.NoDK) as NoDK, SUM(c.CoDK) as CoDK
				from
					(SELECT b.AccountingObjectCode as MaNV, b.AccountingObjectName as TenNV, a.DebitAmount as NoDK, a.CreditAmount as CoDK, a.Account as TK
					FROM @tbDataGL a join @tbluutru b on a.AccountingObjectID = b.AccountingObjectID
					where Date < @FromDate
					and a.Account LIKE (@Account +'%')
			) c
			group by c.MaNV, c.TenNV, c.TK) d
			where AccountingObjectCode = d.MaNV
	end

	select * from @tbluutru
	where NoDK != 0 or CoDK != 0 or NoPS != 0 or CoPS != 0
	order by AccountingObjectCode

END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoTheoDoiLaiLoTheoMatHang]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @MaterialGoodsID AS NVARCHAR(MAX)
AS
BEGIN          
    
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
		/*end add by cuongpv*/
	
	DECLARE @tbluutru TABLE (
		MaterialGoodsID UNIQUEIDENTIFIER,
		MaterialGoodsCode NVARCHAR(25),
		MaterialGoodsName NVARCHAR(512),
		Unit NVARCHAR(25),
		Quantity decimal(25,10),
		DoanhSoBan money,
		ChietKhau money,
		GiamGia money,
		TraLai money,
		GiaVon money,
		LaiLo money
    )
    INSERT INTO @tbluutru
		SELECT M.ID as MaterialGoodsID, M.MaterialGoodsCode, null MaterialGoodsName, null Unit, null Quantity, null DoanhSoBan, null ChietKhau,
				null GiamGia, null TraLai, null GiaVon, null LaiLo
		FROM dbo.Func_ConvertStringIntoTable(@MaterialGoodsID,',') tblMatHangSelect LEFT JOIN dbo.MaterialGoods M ON M.ID=tblMatHangSelect.Value
		WHERE tblMatHangSelect.Value in (select SA.MaterialGoodsID from SAInvoiceDetail SA LEFT JOIN @tbDataGL GL on GL.DetailID=SA.ID 
										 where GL.PostedDate between @FromDate and @ToDate)
		
	UPDATE @tbluutru SET MaterialGoodsName = a.MaterialGoodsName, Unit = a.Unit, Quantity=a.Quantity
	FROM ( select MaterialGoodsID as MId, MAX(b.Description) as MaterialGoodsName, MAX(b.Unit) as Unit, SUM(b.Quantity) as Quantity  from SAInvoiceDetail b
	where (b.ID in (select DetailID from @tbDataGL GL where GL.PostedDate between @FromDate and @ToDate))
	group by MaterialGoodsID) a
	WHERE MaterialGoodsID = a.MId


	DECLARE @tbDataDoanhSoBan TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		DoanhSoBan money
	)
	
	INSERT INTO @tbDataDoanhSoBan 
		SELECT a.MaterialGoodsID,SUM(c.CreditAmount) as DoanhSoBan 
		FROM SAInvoiceDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '511%' AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET DoanhSoBan = k.DoanhSoBan
	FROM (select MaterialGoodsID as Mid, DoanhSoBan from @tbDataDoanhSoBan) k
	WHERE MaterialGoodsID = k.Mid
	
	DECLARE @tbDataChietKhau TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		ChietKhauBan money,
		ChietKhau_TLGG money,
		ChietKhau money
	)
	
	INSERT INTO @tbDataChietKhau 
		SELECT a.MaterialGoodsID, SUM(c.DebitAmount) as ChietKhauBan, null ChietKhau_TLGG, null ChietKhau
		FROM SAInvoiceDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '511%' AND c.TypeID not in (330,340) 
		AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
		
	UPDATE @tbDataChietKhau SET ChietKhau_TLGG = g.ChietKhau_TLGG
	FROM (
		SELECT a.MaterialGoodsID as Mid, SUM(c.CreditAmount) as ChietKhau_TLGG
		FROM SAInvoiceDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '511%' AND c.TypeID in (330,340) 
		AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	) g
	WHERE MaterialGoodsID=g.Mid
	
	UPDATE @tbDataChietKhau SET ChietKhau = (ISNULL(ChietKhauBan,0) - ISNULL(ChietKhau_TLGG,0))
	
	UPDATE @tbluutru SET ChietKhau = e.ChietKhau
	FROM (select MaterialGoodsID as Mid, ChietKhau from @tbDataChietKhau) e
	WHERE MaterialGoodsID = e.Mid
	
	DECLARE @tbDataGiamGia TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		GiamGia money
	)

	INSERT INTO @tbDataGiamGia 
		SELECT a.MaterialGoodsID,SUM(c.DebitAmount - c.CreditAmount) as GiamGia 
		FROM SAReturnDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '511%' AND c.TypeID=340
		 AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET GiamGia = f.GiamGia
	FROM (select MaterialGoodsID as Mid, GiamGia from @tbDataGiamGia) f
	WHERE MaterialGoodsID = f.Mid
	
	DECLARE @tbDataTraLai TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		TraLai money
	)

	INSERT INTO @tbDataTraLai 
		SELECT a.MaterialGoodsID,SUM(c.DebitAmount - c.CreditAmount) as TraLai 
		FROM SAReturnDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '511%' AND c.TypeID=330
		 AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET TraLai = h.TraLai
	FROM (select MaterialGoodsID as Mid, TraLai from @tbDataTraLai) h
	WHERE MaterialGoodsID = h.Mid
	
	DECLARE @tbDataGiaVon TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		GiaVon money
	)
	
	INSERT INTO @tbDataGiaVon 
		SELECT a.MaterialGoodsID,SUM(c.DebitAmount - c.CreditAmount) as GiaVon 
		FROM SAInvoiceDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '632%'
		 AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
		UNION ALL
		SELECT a.MaterialGoodsID,SUM(c.DebitAmount - c.CreditAmount) as GiaVon 
		FROM SAReturnDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '632%'
		 AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
		
		
	UPDATE @tbluutru SET GiaVon = j.GiaVon
	FROM (select MaterialGoodsID as Mid, SUM(GiaVon) as GiaVon from @tbDataGiaVon group by MaterialGoodsID) j
	WHERE MaterialGoodsID = j.Mid
	
	UPDATE @tbluutru SET LaiLo = ISNULL(DoanhSoBan,0) - (ISNULL(ChietKhau,0)+ISNULL(GiamGia,0)+ISNULL(TraLai,0)+ISNULL(GiaVon,0))
	
	select MaterialGoodsID,	MaterialGoodsCode, MaterialGoodsName, Unit, Quantity, DoanhSoBan, ChietKhau, GiamGia, TraLai, GiaVon, LaiLo
	from @tbluutru WHERE MaterialGoodsID is not null AND ((ISNULL(Quantity,0)>0) OR (ISNULL(DoanhSoBan,0)>0) OR (ISNULL(ChietKhau,0)>0)
	OR (ISNULL(GiamGia,0)>0) OR (ISNULL(TraLai,0)>0) OR (ISNULL(GiaVon,0)>0) OR (ISNULL(LaiLo,0)>0))
	order by MaterialGoodsName
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE dbo.GenCode
  DROP CONSTRAINT FK_GenCode_TypeGroup
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Template
  DROP CONSTRAINT FK_Template_Type
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateDetail
  DROP CONSTRAINT FK_TemplateDetail_Template
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '115d21df-affc-44f3-983d-0154d0184960'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '6336e36b-d8ef-4f60-9851-02f73a7f6843'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '293410a6-a1d8-4b8f-8a11-039ba7b296d7'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '61163644-3894-4257-9a21-0940eddd836f'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'fd3f6e50-c1d7-40a7-868f-09ff29086205'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '6ba57631-c48f-43d1-8663-0ca651358974'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '33600939-6890-4946-9e3f-0d51a304a77c'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'a724e9e7-bcdc-4e83-96d1-12a4ee7de3f7'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '8b908b63-011e-49cd-9460-13708ba63717'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '072a239a-21ac-45bb-b0d9-19b61ce777a8'
UPDATE dbo.Account SET IsForeignCurrency = 1 WHERE ID = '80fe6d05-83d0-432d-806f-1c0655aa4919'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '7061c24b-3931-4ea3-99d8-1d87dafe6217'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '9afa59f4-c269-4fc8-8623-1f2137d70bf5'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '8eed1e1f-fab1-45e5-8750-1f7de5b8b2f7'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '76a9df4a-9452-46ec-ac98-1f983292de8f'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '7261a5da-cec1-457c-bbe9-20f1bca122d9'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '61b686e2-a5a3-4899-a586-21082bcd8687'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '76011dd3-7b97-4279-861e-24322ebbf0f1'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '162a30b8-b517-4cf8-9c43-24a64d0925c8'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'f96f1df3-c26c-4ae0-bf88-274eb95a87ca'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '0455b75f-1b2c-4af5-b842-2a006a8f0479'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '747727cd-3d84-49e6-befd-2a549a62ff65'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '52464609-8146-462d-a98b-2f66cb7ee5e7'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '399f97ce-8ca2-46c2-9069-2fe7a0d854cc'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '8a26ee37-ac3d-475b-907c-31084a5ead77'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '7583753f-0793-4852-b913-33d7ab80102f'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'b9834976-0b39-4ff8-bf0c-38e065753f0b'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '59db7a86-ff15-467f-ae02-396827874d7e'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'ae7fcbbb-b811-4a54-9c0d-3b5292fdde26'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '4a0760b1-fdfa-4439-b0ee-3f6d5fdabc42'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'f46b94f7-e0c5-46ad-9089-42fc0634ddba'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '39d57678-9ac6-45b7-82f0-43ad5d7d2fcf'
UPDATE dbo.Account SET IsForeignCurrency = 1 WHERE ID = '00e0acfe-02b2-476b-b99e-457065523383'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '04cfeb78-c9d3-47e4-94e1-461e00e98926'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'ffa2144a-ca74-4880-9f2e-4afef0f9609c'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '8b30b5f3-b6d8-41b2-8691-512316278fff'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '999efce1-ba4f-4ebe-83c0-561c2dc32d8f'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '70440878-3fbb-4034-83f0-5be6369a8e16'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'c87c8ad9-5bfe-4056-b52b-5da82bbd3708'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '82ee395e-3a7c-41f8-a6bf-67f1710b5d83'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '13431aac-c5f0-4efc-a05b-68533f647576'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '8a56abc3-274c-46fe-a8c7-68e074270c98'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '2a570657-3768-4b8d-b575-6a95ea96a80a'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'f2673bfe-f9a9-4e69-a92e-6bf9390027f6'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '6761628c-f99f-4234-995a-6c492c79e9f6'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '733d8864-5735-4673-8159-6db85abab6a9'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'b9b17b43-02f6-4eab-955e-6f3861230223'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '05d067f4-5cd2-49a1-9c5f-70e03d927cf4'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'a0f55128-0a61-4101-aaa8-72cc4cffc624'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'd0a7bca6-f58d-4b79-bbbb-73dd6517e704'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '4955afa1-c87e-4f8f-acef-7666946e652c'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '0f464ec8-858b-4802-9097-7d8ed6e02565'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '3b29167f-c10e-427e-bebd-84095c1e3567'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'a9024e2b-8c31-439a-8c73-864fb148a2bf'
UPDATE dbo.Account SET IsForeignCurrency = 1 WHERE ID = 'fd349ed4-8176-460d-9beb-877c2aed92ca'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '703532aa-d5e3-4eb7-8490-8816b5a32e5a'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '42760517-e110-45c8-886b-89d9b7f9fa02'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'e971c7a2-acbf-49fe-ba48-8c12cc072960'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'df6c256f-3d79-42c3-9658-8cee022bdbeb'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'da5e6c9c-38ce-4012-93a9-8ecc90702ec2'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'e22d515a-8492-415d-8a91-97c9ec576a7d'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '7ca64be9-ce4c-47df-81e7-986b5bcd2cb4'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'd303c286-c4a6-496d-ad6f-9899e23050ea'
UPDATE dbo.Account SET IsForeignCurrency = 1 WHERE ID = '398ac7af-432c-4e2f-84ee-9b02aeeebb2b'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '3d7138ed-b8c4-4ae6-b38a-9bcd2b35cd07'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '8cb3fef4-5ee4-413c-bd1a-a19d0d668301'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '40df88db-962d-466d-b169-a2c00a7f9375'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '5d9adfd2-bb4e-4694-bed0-a347f8c257f6'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'b7005980-1d6c-4c77-9a00-a6e326b63e5f'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '987fa92c-7b7b-496d-8447-a8b6fbf83dee'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'c5f9765c-bc6c-4427-801f-a98fc6280e8f'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'e179f840-6b6f-4300-9a17-ac3338f4ea92'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '76f01739-d301-40c8-9c2f-ad5f790b66fd'
UPDATE dbo.Account SET Description = N'', DetailType = N'-1', IsForeignCurrency = 0 WHERE ID = 'e307b2c8-aad7-41fc-9b01-af758f52f740'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'e7d30aca-4854-4dc2-8384-b0c7eeca02ce'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '9987673d-db59-46f9-a577-b162a33c9068'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '7c6f1112-377f-4f95-aef8-b1c37deabc8f'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'b30a4b77-34f9-488d-a0e7-b35c59242add'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '98a56e04-d383-46d5-b551-b3d1d7140cbc'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'da8ed7fc-845b-40a6-ae5f-b4632d787e68'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '76ed0734-2372-4644-9efe-b55baf57870a'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '81369fa4-a45c-4020-920f-b8698a042698'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'c5895fb7-d1cb-4493-91cb-b9b3cc32d96d'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'cfb957c2-6d7c-41b0-bcbe-b9e7444e7aff'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '89d80be7-29fb-49c7-be72-bb7ce995d577'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '96c2bbb2-4ddd-4404-9a45-bce76cdd7bbb'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'cc103408-c0d4-47cd-8606-bd8bd4e1b440'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '181852e3-adb3-4222-a22b-be5132859a3d'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '2f077f7d-6a3e-48d6-b327-c0ae0cbc6faa'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'b0a2bffd-d7e1-4116-b3f1-c36662fb989e'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'e5dc7000-da05-4808-b65e-cc5f948e70c7'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'f298fac8-fea0-449b-982c-cca1d92e9200'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'b94a2806-d514-4b0b-9248-ceea38fecc0c'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'ea2eaa7a-7450-4f91-8225-cfc6fc4ab16f'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'a05d95ca-0641-4eba-ae48-d010326d3863'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '7cc5abbe-e26b-4d5c-aa3c-d01ae4afa07d'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'd3adda5d-9564-477c-b76f-d02511497841'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '6159f4ca-f2f0-45d6-aff6-d2156255116a'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '2969dc08-9c7c-4d80-9da2-d290d354f70d'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '580759e3-3322-4058-9e6d-d303df12f9f3'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '9dd673d2-de90-4592-9324-d3b21f05ca1e'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '2554b1ee-4db4-4d3c-a2ee-dad12bc57d82'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'a278b707-c226-42f0-a46c-dcf9c351f35a'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '18c9b9b5-f2a0-45c8-9b2b-dd523e6f7bc5'
UPDATE dbo.Account SET IsForeignCurrency = 1 WHERE ID = 'df4adbb7-0dc7-4e23-ab82-e011bd40d93d'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'a81ead52-1d99-471e-9d24-e1228a056878'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '8b0c2a4e-24ee-4950-8824-e24300a5883c'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '20051cf2-0828-4722-9dc6-e2b130fe0112'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '8a614b4d-b528-4e56-ba97-e2bfc68fa896'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '4d47ebe7-a76e-4c82-890e-e5ce5f5437c3'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '646d0e5e-9d13-44c2-a4da-e7c8b7dc1f02'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '9046f5cf-8e7b-43cc-b2a6-e81ccc306c21'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'cfa92323-a8b2-4355-b0a3-e868f54e3aed'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '97ac7260-27ad-4d64-81ca-e96712fdb118'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '4dc41060-a86c-4bf1-88f1-ecbb724c9d0b'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'eae725af-6702-486d-8f7b-efed6cc51725'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '80acf9c2-19bd-40ac-bd67-f472683f2721'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'ca302434-105c-4dc1-81bf-f76403eefa52'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '8a664628-08d0-4933-afa0-f7e9b18b2865'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '194e95c4-db0e-4db3-85f1-f8b9f0eb52d3'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = 'b44c72a7-a4ae-4a6a-8d24-fc0dfeedff5c'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '2c2f861c-d50d-4b10-929d-fdcf63e0c2b4'
UPDATE dbo.Account SET IsForeignCurrency = 0 WHERE ID = '168820be-7173-476e-bab3-ff4caa7efd1d'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.AccountDefault SET ColumnCaption = N'TK Chiết khấu' WHERE ID = '3d02b3c5-6812-4054-9f27-68fca2e835b3'

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
SET IDENTITY_INSERT dbo.AccountDefault ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.AccountDefault(ID, TypeID, ColumnName, ColumnCaption, FilterAccount, DefaultAccount, ReduceAccount, PPType, OrderPriority) VALUES ('fff4ae68-fd69-4f39-bbc8-6983e944f8fd', 326, N'VATAccount', N'TK thuế GTGT', N'33311', N'33311', NULL, 0, 374)
GO
SET IDENTITY_INSERT dbo.AccountDefault OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DBCC CHECKIDENT('dbo.AccountDefault', RESEED, 374)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.AccountTransfer SET AccountTransferOrder = 15, TransferSide = 2 WHERE ID = '1cb59ece-affa-422b-ae98-11ce3f7cffee'
UPDATE dbo.AccountTransfer SET AccountTransferOrder = 40, TransferSide = 2 WHERE ID = 'c6dc538f-2f83-4c3e-b062-1500a30dc45f'
UPDATE dbo.AccountTransfer SET AccountTransferOrder = 10, TransferSide = 2 WHERE ID = 'adf390cc-8f19-47a8-bef8-2cde0ed70bca'
UPDATE dbo.AccountTransfer SET AccountTransferOrder = 55, TransferSide = 2 WHERE ID = 'c09e4c82-f412-40e8-8dfd-39d406a3cecd'
UPDATE dbo.AccountTransfer SET AccountTransferOrder = 50, TransferSide = 2 WHERE ID = '0257dd5a-c5f6-49ae-829e-4fbc410cddb6'
UPDATE dbo.AccountTransfer SET AccountTransferOrder = 65, TransferSide = 2 WHERE ID = 'db04a999-b983-48f4-a89d-6083d6b0ab0e'
UPDATE dbo.AccountTransfer SET AccountTransferOrder = 20, TransferSide = 2 WHERE ID = '0ba23d3c-b263-4d2d-8085-6bf8a5792dc8'
UPDATE dbo.AccountTransfer SET AccountTransferOrder = 30, TransferSide = 2 WHERE ID = 'b5277067-e1c5-4aba-9405-7d191ea633f1'
UPDATE dbo.AccountTransfer SET AccountTransferOrder = 70 WHERE ID = '72a2d142-cc0f-43ec-9288-8233e044fe87'
UPDATE dbo.AccountTransfer SET AccountTransferOrder = 45, TransferSide = 2 WHERE ID = 'dddf9d54-71ea-4232-96a5-8f0a59894bf0'
UPDATE dbo.AccountTransfer SET AccountTransferOrder = 25, TransferSide = 2 WHERE ID = 'd3ccb7fa-717e-4b80-b536-92db83e8ea65'
UPDATE dbo.AccountTransfer SET AccountTransferOrder = 60, TransferSide = 2 WHERE ID = 'c5646029-347e-4aa8-b7e3-b3447ede9a94'
UPDATE dbo.AccountTransfer SET AccountTransferOrder = 35, TransferSide = 2 WHERE ID = 'ffb75ace-618f-4afb-98eb-df1418bcf8c6'

INSERT dbo.AccountTransfer(ID, AccountTransferCode, AccountTransferOrder, AccountTransferType, FromAccount, ToAccount, TransferSide, Description, IsActive, IsSecurity) VALUES ('38c56604-ac8c-4908-9526-6c4134d11a0c', N'413-635', 5, 0, N'413', N'635', 0, N'Kết chuyển lỗ chênh lệch tỷ giá hối đoái', 1, 0)
INSERT dbo.AccountTransfer(ID, AccountTransferCode, AccountTransferOrder, AccountTransferType, FromAccount, ToAccount, TransferSide, Description, IsActive, IsSecurity) VALUES ('e8338dbd-e2f3-4fc8-89c5-86f2e219d975', N'413-515', 1, 1, N'413', N'515', 1, N'Kết chuyển lãi chênh lệch tỷ giá hối đoái', 1, 0)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.GenCode SET IsReset = 1 WHERE ID = 'c1b08452-c45b-4a8f-a049-c24b449fdb32'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.MaterialGoodsResourceTaxGroup SET Unit = N'ml' WHERE ID = '9c6e71d8-6631-46f4-846e-1216147b55c3'
UPDATE dbo.MaterialGoodsResourceTaxGroup SET Unit = N'ml' WHERE ID = 'aa18d6cd-0a23-4ead-8298-1b0cf0006eff'
UPDATE dbo.MaterialGoodsResourceTaxGroup SET MaterialGoodsResourceTaxGroupName = N'Khoáng sản kim loại khác 1', Unit = N'kg', TaxRate = 75.0000000000 WHERE ID = 'b2ce0bb5-4b3b-47c7-9abd-1fc541e2be31'
UPDATE dbo.MaterialGoodsResourceTaxGroup SET MaterialGoodsResourceTaxGroupName = N'Ngọc trai, bào ngư, hải sâm 1', Unit = N'kg' WHERE ID = '9d6fd7ae-a8d9-4e59-a8e2-22e707b979e0'
UPDATE dbo.MaterialGoodsResourceTaxGroup SET MaterialGoodsResourceTaxGroupName = N'Sắt ' WHERE ID = '117a4f4d-6e72-4bdf-ab2e-28cc76893717'
UPDATE dbo.MaterialGoodsResourceTaxGroup SET Unit = N'ml' WHERE ID = 'fc61014b-07ca-46c2-be81-40d2ff89386c'
UPDATE dbo.MaterialGoodsResourceTaxGroup SET Unit = N'kg' WHERE ID = '32a4025f-8a4f-4587-afb3-42ca3522f5b6'
UPDATE dbo.MaterialGoodsResourceTaxGroup SET Unit = N'ml' WHERE ID = 'd9157e2c-801d-42d9-a016-46c0aa914b70'
UPDATE dbo.MaterialGoodsResourceTaxGroup SET Unit = N'kg' WHERE ID = '1f331d9a-e1b4-4a05-aa57-523f14699189'
UPDATE dbo.MaterialGoodsResourceTaxGroup SET Unit = N'ml' WHERE ID = 'c3f28d29-ae9c-42c4-847b-59f22c220e4a'
UPDATE dbo.MaterialGoodsResourceTaxGroup SET Unit = N'ml' WHERE ID = '331405e0-576b-418d-ab70-6b42790020e7'
UPDATE dbo.MaterialGoodsResourceTaxGroup SET Unit = N'kg' WHERE ID = '7ea542a6-a9aa-4f22-a859-a199b54e3e94'
UPDATE dbo.MaterialGoodsResourceTaxGroup SET Unit = N'ml' WHERE ID = 'e22787e8-6ccf-4c09-a03e-b3d23286ab05'
UPDATE dbo.MaterialGoodsResourceTaxGroup SET Unit = N't' WHERE ID = '0b770a69-f690-4c27-a425-baded1fca334'
UPDATE dbo.MaterialGoodsResourceTaxGroup SET Unit = N'kg' WHERE ID = 'adbaaf07-a674-4e17-9a0a-c0948fca8e7b'
UPDATE dbo.MaterialGoodsResourceTaxGroup SET Unit = N'ml' WHERE ID = '4783b61e-e66c-4707-a510-ce92fb96e953'
UPDATE dbo.MaterialGoodsResourceTaxGroup SET Unit = N'kgg' WHERE ID = '3994d158-56ca-4a56-8d7b-f1b3b0e37cd6'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.SupplierService SET PathAccess = N'https://0107489961-democadmin.vnpt-invoice.com.vn' WHERE ID = 'e424067d-cbe9-4949-96c8-f4086f5b6384'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.Sys_Role(RoleID, RoleCode, RoleName, Description, IsSystem) VALUES ('1053ed42-1d6a-4d98-b7dd-b2a5d1300563', N'Kế toán hợp đồng', N'Kế toán hợp đồng', N'Quyền Kế toán hợp đồng', 0)
INSERT dbo.Sys_Role(RoleID, RoleCode, RoleName, Description, IsSystem) VALUES ('b17679c4-42e9-4495-97c7-c6a01a09630f', N'Kế toán thuế', N'Kế toán thuế', N'Quyền Kế toán thuế', 0)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.Sys_RolePermission WHERE ID = '39ccd792-5567-4304-9716-0314cb3e5886'
DELETE dbo.Sys_RolePermission WHERE ID = 'ec1d4d62-1885-4c9b-8762-07f85dc06c28'
DELETE dbo.Sys_RolePermission WHERE ID = '50e2e77d-9f20-4b41-ba3a-147f6002dfda'
DELETE dbo.Sys_RolePermission WHERE ID = '9bc94751-727a-46b0-951e-18d2647f69d2'
DELETE dbo.Sys_RolePermission WHERE ID = '431b29fe-d4f1-4654-8683-2fe2d41aeddf'
DELETE dbo.Sys_RolePermission WHERE ID = 'd5ebb02f-9f2d-465e-9df0-3edf3318ac7c'
DELETE dbo.Sys_RolePermission WHERE ID = '6bb1382c-6821-4d8e-aa2a-56313611d035'
DELETE dbo.Sys_RolePermission WHERE ID = '0cfb6b51-8205-49ec-9bb9-5e769a9ee959'
DELETE dbo.Sys_RolePermission WHERE ID = '72a8a2d3-1d9d-487b-9630-623ce6e461c4'
DELETE dbo.Sys_RolePermission WHERE ID = '39e7ee24-8a7e-483b-9a4c-752a6f24d9d8'
DELETE dbo.Sys_RolePermission WHERE ID = '09446be3-46fd-4ad8-acb7-a88952cd83ad'
DELETE dbo.Sys_RolePermission WHERE ID = 'afa5a251-7824-498d-b138-f027eb375e98'

INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1baf7958-abfe-42d5-af14-00067a28ec96', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_RegistrationGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7014f3a7-9871-42ab-b925-01080917c8b6', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FRContractStatusSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2d535acd-43db-47da-af28-0108a715a53f', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FDeductionGTGT01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2f18f012-f8b3-4667-895c-01e20d563f75', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_Account')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0d876563-1f48-4b3c-99fc-020fd1224dc6', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_StockCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('550e240c-cb1d-48c4-9a0c-0327ac718456', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FEMContractSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e597fb06-ce11-4a86-84f5-03441989f2d9', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_Currency')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('9577ab98-3522-4e60-bf97-038fdd82ee7e', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_MaterialQuantum')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a578135d-d7af-456a-9725-03c499cff56f', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_StockCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('22f14ef4-7676-4dfb-aa29-03f253b7cc69', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FTaxSubmit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6390cc06-716a-42ab-a49d-0402969faee0', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_Account')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6853f0ae-f1bf-4a96-93fd-052b9d84d6f1', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_CostSet')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('40b15c34-6270-4112-9e96-05635c943776', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'TaxPeriod')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('bbcec279-71aa-469e-bc7e-07e902ee5d9a', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_Investor')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('61b48081-722b-4c12-a343-088110b56563', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_MaterialQuantum')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d3362fab-5366-468c-a7f4-089640930346', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FRContractStatusSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4eaeb77d-3293-4177-b95e-0a1d595ae0fc', 'c4e0a64b-9aa0-430c-ab9a-4c23449d6c64', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'Rpt_FRS11DNN_PBCCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a8c7c278-ba0c-4e34-b54c-0a3719a7c3db', '12b1bbd7-498d-4386-9203-ccf0bf462aff', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'Rpt_FRTongHopTonKho')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8281dcf5-1ba4-43ba-8dd0-0ad7591863a1', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_Account')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7f81f201-35cd-43ff-b32a-0b33d226d2e0', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'FEMContractSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0b9c4340-4da8-48a8-8d1a-0b37855790f5', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_MaterialGoodsCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('463f5301-9acd-4e2d-8b53-0b81088da4ce', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_BankAccountDetail')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('9407e6e5-a5cb-4bac-91a7-0bea484a7434', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_FixedAsset')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b36eb609-4cdf-47b2-9213-0bfef285cf7c', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('64a5d60a-22c2-43f2-84da-0c929defebfb', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_Account')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6cc2c830-2abe-4f7f-bcb1-0c987865ce8c', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_StatisticsCode')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0eb7764b-1300-4f1d-b8fa-0d557de45708', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_Investor')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6803fe47-3ccd-4dc6-8c8d-0db30ab51255', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_TimeSheetSymbols')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0196f0af-9912-4b31-8f1d-0dee7ba659b2', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_Employee')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8133d23c-d82e-4d61-8eaa-0ed6b3bc5f0d', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FDirectGTGT04')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('fe36696f-deef-4f79-ae89-0eeb0ef5a56c', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FTaxSubmit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2b5f374d-b44f-46ea-89ed-0ef654206c31', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_RegistrationGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('aeeea511-087a-4846-9c58-0f2d348f8c3d', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_BudgetItem')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e6591993-e3a2-412e-b13c-0f80bc70dea3', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_MaterialGoodsResourceTaxGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1754a157-8070-4f20-8dcd-1032430b1715', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'FTaxFinalization')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('26ab0a6b-6721-4e82-8809-104561ce344b', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_TransportMethod')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('53424946-4cd4-48e9-93f1-1081866bf88d', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_ShareHolderGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('dc61fb4c-dac7-40d8-9348-11391d6cdb44', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_AccountingObjectGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ecfa3779-7ab2-44ed-8ef7-11fd61069d78', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_SalePriceGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('35a126dd-7bdd-4c02-8c55-12051e693e82', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'TaxPeriod')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6a975c2f-c152-4a73-94ba-123b46bf1489', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FEMContractSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('cf565bf0-2922-47d6-9041-12dace0008ec', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_InvestorGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f4938903-1301-4fbd-990c-131bba2ff395', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_PaymentClause')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0be2bab5-ce46-4b54-bd04-132c31bdc2ea', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_SalePriceGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f477b719-6905-4ab5-85e2-1352ee285212', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FDeductionTTDB01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c58c0a69-2909-4f99-a56a-1365b5a4f8a6', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_PersonalSalaryTax')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('67a55dfe-726c-4c44-a09a-138fb9a4d8a9', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_AccountDefault')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('340c7c40-f76c-4a2a-b298-13ce9263da84', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_ExpenseItem')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6cf1fc28-5184-40fd-b020-140e91db5487', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_AccountTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('adf33753-977b-4371-b04e-1481333454e8', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_CCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('95aa15a8-2be5-4c6b-b028-159fb5db4eaa', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_ExpenseItem')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4030fa18-6c72-4722-87e2-168febc4960d', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FEMContractSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b7ad003b-173f-4ee9-b84d-16d6f623f731', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_Department')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('23664996-45a1-422a-a8ca-16ee50019e39', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_TransportMethod')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('62d3d031-51ba-4364-a5c9-1800e6076543', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FDeductionGTGT01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('30fd81dd-6e03-49ff-82da-188fc3a5edba', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FDeductionTTDB01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('64d204b1-bb55-4e15-bfc6-19b797d48285', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_CostSet')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('62710873-727a-4ff6-b717-19b850f464d1', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_MaterialGoods')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6250cf20-ff2b-4491-92d7-19cf9cb34e18', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_MaterialGoodsCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('193fc1a0-e55c-4743-a79f-1b93a0d568e3', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_ContractState')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4930af3c-9169-40f8-8325-1bab22c84323', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_CCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3f31c15c-a772-4568-8afb-1bb48c749390', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FEMContractBuy')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0f016673-24a1-4e8a-8dd0-1c4d9e192770', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_Bank')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a8142ebf-1871-4523-9ec0-1c7e1b95e4c5', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_ContractState')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e1ccadc5-9eec-454d-9f15-1d9e708fdf01', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FRContractStatusSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('fd58fa5c-baf0-44aa-a926-1dbaea580996', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_Bank')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c099bddd-29c2-44c8-b5ca-1e66cb7660cc', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_MaterialGoodsSpecialTaxGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ae643a04-b66f-412e-bf05-1ea3f4b7a755', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'TaxPeriod')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1183f506-2687-4a9f-a05d-1f2678f7ca7d', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_Investor')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d7b3ca5d-71be-43f0-b96e-1f9dbfc204e5', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_FixedAsset')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c25e2775-3f33-4b1a-ac55-1fea1273bce9', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FEMContractBuy')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('16e0efa8-0fa7-4b06-ac7d-20532875d97d', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FEMContractBuy')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('15872cf7-0eb5-4308-8720-21ee07d4cc35', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_SalePriceGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('516cf4e6-02ba-4c40-935d-2202ac122b9d', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_MaterialGoodsResourceTaxGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('9a7ec7f0-1aa7-4206-b457-224caeff6af2', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_TimeSheetSymbols')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e828dff2-4c63-427f-8cbb-232f46830dac', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'TaxPeriod')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7f1b5708-c7f2-4d64-9231-237b5894a99b', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_Repository')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f0730656-0753-4baa-8c94-23cbc5bdf9c5', '12b1bbd7-498d-4386-9203-ccf0bf462aff', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'Rpt_FRTongHopTonKho')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8c034c7e-573e-4986-b63c-25361ec56647', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_InvestorGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('28868cf6-3364-4c42-8db3-253c65293e3b', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FTaxFinalization')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('53b641ba-3ad9-4a54-95b4-2579c7cb6da9', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_AutoPrinciple')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('423a03d8-4c0b-4d59-866a-259ee4b048a6', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_BankAccountDetail')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('10c07d8c-3b59-4ff7-b4cc-259f325f150f', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_TransportMethod')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('fa2eae47-2673-45cb-b93f-2768a44d5134', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FTaxSubmit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('82d46d3a-47f9-4210-bec2-27f69bf0d3c5', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_PersonalSalaryTax')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('eb5d8517-2e1f-4ff2-9754-284944273aa1', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_FixedAsset')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ef79e1fe-dd5f-43fb-8655-2976fddd0633', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_CCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6f7c8379-f623-43eb-bf86-298e6ca34c9f', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_BudgetItem')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('774a923f-8faf-44d6-8582-29efa794e01d', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c8d0369e-9bab-44be-973c-2a64bd22be84', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FTaxSubmit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('86977857-9205-41ea-a380-2baa2baecad1', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FTaxFinalizationResourcesTAIN02')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e539823b-4cdd-4c28-b79b-2c27bb91f561', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_AutoPrinciple')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6b9d4ff1-7876-4ff5-aa3d-2c50ffca2f1a', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_CCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('be668a01-e349-4a9c-9c3f-2d10d9c01638', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_BudgetItem')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ebd7b360-1957-45e8-b47c-2de7d9f81775', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_ShareHolderGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b73853fe-02fb-45d0-93f3-2e4b869a2fed', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'FDeductionTTDB01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b4f4e1ca-b9d2-4ff9-a416-2eaf91b4e224', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_AccountingObjectCustorm')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('aadcc5d8-1ebc-418b-9019-2f49b4d1c2d8', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_CreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6a3c772c-ef9b-4fa7-9daf-3106ad000c2b', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_CCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d7426c64-690c-4390-ba56-31841952ddc0', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_MaterialQuantum')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('57abff60-3faa-40c8-b7bc-33a571fa1a60', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_PaymentClause')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c9554acb-4180-48ec-9896-33a81ccbc6a6', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FDeductionGTGT01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e6e97103-86cf-4f41-ba6e-351f994cd132', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_CCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f96474d7-f990-464c-8c6b-358ae031cffc', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_TimeSheetSymbols')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('be4db2b4-869a-4462-9afc-3630085ca046', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_CreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('5f13da72-6069-4f20-a759-36a4365bf956', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_Investor')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d82559f6-7dd0-4ce6-852c-374f73477a59', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_AccountingObjectCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('af28b2a3-58aa-4108-adb0-37ca4d4f03b5', 'c4e0a64b-9aa0-430c-ab9a-4c23449d6c64', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'Rpt_FRS11DNN_PBCCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b3f8ea48-d368-4846-8df9-3814c5e1e53a', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_AccountTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('01908a16-d6e5-4aed-9ebf-381dfe9ef0f9', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_MaterialGoods')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6197e8fb-4c40-485a-9b2b-387dc81026ce', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_ShareHolderGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a55d8cbe-7935-4bd7-be4c-399479cf1b93', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('9eb6f2f2-ab91-41b3-9464-39eda78760af', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FRContractStatusPurchase')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d42ab617-7fad-4e1d-8158-3a80116e3005', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_AccountingObjectCustorm')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('258c24ce-bac6-4300-8892-3b0945fad219', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FTaxSubmit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2443f0f6-2631-4112-8bac-3b2b4c609b5f', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_Employee')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('9050cc01-bfa1-42cb-81b0-3b42a78665c0', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_AccountingObjectCustorm')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('89a56b13-95d7-4a45-81aa-3c2f1fe05433', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_CreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('88ffb4b5-a1b7-404a-9c03-3c412735dd5b', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_Investor')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e4ac4453-2dae-4703-825d-3c963ba29b48', '12b1bbd7-498d-4386-9203-ccf0bf462aff', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'Rpt_FRTongHopTonKho')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('72730895-c56a-476b-a225-3d076d95a2fc', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'FTaxFinalizationResourcesTAIN02')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0f4a2729-132d-4011-86e7-3d0a352e0be5', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('60d4de47-95db-4522-9109-3e50681caf27', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_AccountingObjectCustorm')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('00b42e0a-b569-4e7c-9cfc-3efa6501e5cf', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_CostSet')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('50c13348-38b1-43ab-8763-3fa48fda748e', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_StockCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2b3c6be1-4c77-4a29-8c32-415a53710517', 'c4e0a64b-9aa0-430c-ab9a-4c23449d6c64', '48e1816b-43a7-4799-a391-c881e98c7b36', N'Rpt_FRS11DNN_PBCCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4f340662-80f8-452f-b787-41acd257233e', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FDeductionTTDB01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('18848007-880e-4403-80e4-420c5b2278be', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_MaterialGoodsResourceTaxGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('46b104d4-5f02-485e-9ebd-421f0436e720', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_MaterialQuantum')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e296e614-f89d-4a41-87f1-427ab5006cca', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FDirectGTGT04')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('89084f52-9ae9-44a7-ba0c-42d3c5c92308', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_MaterialQuantum')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0be06d1a-3725-4865-8cbe-43e22827642a', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FDeductionGTGT01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('243567a6-f2c9-4e1b-8a3f-441e3313802d', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_PersonalSalaryTax')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('09212d98-6d9a-452e-a235-4465332d4cf8', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_TransportMethod')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('41830095-109d-48fa-8998-456e33edc8bf', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_Repository')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d52905e3-82eb-4163-80db-45cfed1f4da3', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FTaxFinalization')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b50a9af0-cfaa-4756-ab32-45de1b807b41', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FTaxInvestmentProjects')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('eb8731b1-7232-4442-a4e1-46455551c1f2', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_Account')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1e383506-a2f4-426d-87e5-4663b6f1073c', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FTaxFinalizationResourcesTAIN02')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8a1005e3-504f-407e-8c4b-47275145ce5a', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_MaterialGoodsSpecialTaxGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('02f33c3c-d43b-41e4-88e2-473d8f01d152', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_AccountTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('cfe5e6c4-0183-4106-8529-47873ba210de', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FRContractStatusPurchase')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('19947a31-8689-4a2a-99bf-48e3100e8984', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FDeductionResourcesTAIN01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('86a7f9b4-709f-4f2b-86bf-4958ce6820ee', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_SalePriceGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('68ec8ba2-5f5a-471a-b623-4979df07aa35', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_MaterialGoods')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e476ec64-c725-4908-9f7a-498cf38e22e8', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_SalePriceGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f51073d0-d524-470b-9843-4a0e0199507b', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_Employee')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('5317e6cc-7d52-431b-a556-4a2275fabab4', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_InvestorGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0da6784f-9954-4c6c-99d3-4bf2fd821670', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_AccountTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('312667c8-0a2b-41d2-a192-4c48141284ae', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_Account')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3dcf289e-826b-4716-bd86-4dbc93a0a4f2', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4d027993-a8d8-4723-844c-4df858708140', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_TimeSheetSymbols')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0d1b95ce-abaa-4375-bda5-4e94faf63ae0', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'FTaxSubmit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1087b1dc-fe0a-4b0c-886d-4fd2c0d32824', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_CreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('129bec06-903c-46a5-92d2-5001d069feb2', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_AccountingObjectCustorm')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4302a6e7-aa6e-4d33-98de-5036bff1b9d7', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_TransportMethod')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('13bfae38-05e9-482d-b39d-515aff806644', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_MaterialGoodsSpecialTaxGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1bbeeea2-27c2-46c5-92f6-515e74d05cf6', '12b1bbd7-498d-4386-9203-ccf0bf462aff', '18906ddd-6508-4431-89ce-374154d52695', N'Rpt_FRTongHopTonKho')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2a482255-0f87-4430-845a-52a05359ced9', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FTaxFinalization')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('9d11adf7-6a70-4ff3-b5e2-52e2fc7bebcd', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FRContractStatusPurchase')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e28208d8-2a1c-4e79-828c-53b84679a838', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FTaxFinalization')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6951705c-4395-4796-afce-544321dacb67', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_AccountingObjectGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('dfb91035-0c3e-42e2-8568-54613c279b79', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_ExpenseItem')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('17fabaa7-cc1b-422e-b7c5-5468110b9538', '00919a7f-5681-4890-9d5d-f436ea2f9524', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'Rpt_FRS11DNN_PBCCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6f6dcba1-ec5e-40bf-af72-54cdd5fd7a90', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FDeductionTTDB01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d904558a-6126-4652-99bb-553ebc9664fd', '00919a7f-5681-4890-9d5d-f436ea2f9524', '48e1816b-43a7-4799-a391-c881e98c7b36', N'Rpt_FRS11DNN_PBCCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('5296ed55-64d1-40b8-8eb5-55812597dfd4', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3542fe19-b895-420e-91b0-55ac7b06d104', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_ShareHolderGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('43ef0ecb-70f2-41b5-a4b8-55f9878ecb1f', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_StatisticsCode')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2e46620a-8316-493a-958f-56484b2718e3', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FTaxFinalizationResourcesTAIN02')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b34fdcec-49e7-48ee-8e27-57ae16dea79b', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FDeductionGTGT01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d37b2446-f3d2-4509-b088-57f114b1bb1a', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_RegistrationGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d57885d7-0ec7-443e-a077-58580373ad7a', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_Repository')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1c2462a1-0430-4c1e-8ea5-585b0eec370c', '00919a7f-5681-4890-9d5d-f436ea2f9524', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'Rpt_FRTongHopTonKho')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('95fcd8ef-65f1-4553-826b-589a450901df', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_AccountingObjectGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f11183ec-1ee7-4ce3-932a-59304f2e0562', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FEMContractSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('99f8015a-81ab-4b5c-8216-5ac0d77d0473', 'c4e0a64b-9aa0-430c-ab9a-4c23449d6c64', '18906ddd-6508-4431-89ce-374154d52695', N'Rpt_FRS11DNN_PBCCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('27a9be38-afd3-4a6a-8d37-5beb5cc982b3', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FEMContractBuy')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0641f988-0673-40dd-948e-5cf1fe83d18e', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_CreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('168ee759-1d92-422e-a3c5-5d14b280161f', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_TimeSheetSymbols')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('485f1b74-709f-4ea0-87ca-5d439ea9694a', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FTaxFinalizationResourcesTAIN02')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('37e46ea9-6287-48b1-866c-5d5163862ff4', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FDeductionTTDB01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d8d87024-dec8-4a7a-8ea1-5d98ed9488b7', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FDeductionResourcesTAIN01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('261962e5-d0f8-421e-93b0-5dc15d7b39a8', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_ContractState')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('9a894891-3e73-4cdb-b551-5dcfe56b8f96', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_CCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e5840894-ca4e-4f99-916a-5dd340f27ee1', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FRContractStatusSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4f6f70d6-4627-497d-a4cc-5ee0efac4563', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_SalePriceGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('db3f9d3f-befd-4f69-988a-60380f3d73a9', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FTaxInvestmentProjects')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b809fea1-ce26-4619-a1e5-637121bdf664', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_Employee')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('dba1652b-7503-4566-a61c-65481c38b6b8', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FDirectGTGT04')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2ac381bb-ba78-42ec-86e0-6548fc29ee10', '00919a7f-5681-4890-9d5d-f436ea2f9524', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'Rpt_FRS11DNN_PBCCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('5b0196bd-0fd8-4ecd-880f-662072515194', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_Currency')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('35b1a992-74f2-48c7-bc16-673b64de16d1', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_StockCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f7158d69-da35-4903-b3ac-67a678342996', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FRContractStatusSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('08ab0edc-1c59-4770-aa58-6938d02a27f6', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6cd9eaa3-fb5f-4cdb-9778-69c017ed652e', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_TimeSheetSymbols')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('44d56366-4156-4865-bae0-6a24ca84f7c1', '00919a7f-5681-4890-9d5d-f436ea2f9524', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'Rpt_FRS11DNN_PBCCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('059bbeb4-1113-40f1-94e4-6a4737ba192b', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('94baf617-ba45-43b4-b269-6b1a64a60d8f', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_InvestorGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('973d2b91-6847-450a-baa3-6b8205962c59', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FRContractStatusSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7eb04cfb-b242-41f8-a95e-6ee7f8892fbc', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_CreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2890c11c-6b66-4ab0-a309-6f330183c59b', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FDeductionTTDB01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('621961d7-6587-4a22-b4b0-6ff6dd7ac9d2', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FEMContractBuy')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c7f1341b-8c13-4954-bd29-7095cf907c9c', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_Repository')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('58f501c0-da30-4e49-ad2f-70c9290d0750', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_Employee')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0b16320d-4621-4f52-a47d-71ebd4b75408', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_AccountingObjectCustorm')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('44b35e52-8878-4dfc-a625-7210375b454c', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_AccountingObjectGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('585a6d32-b033-4060-a48b-735cb45aae6d', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_FixedAsset')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('04cfef0f-c4aa-4a81-8889-736eb410c855', '00919a7f-5681-4890-9d5d-f436ea2f9524', '18906ddd-6508-4431-89ce-374154d52695', N'Rpt_FRTongHopTonKho')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3d8ca1b4-82ec-4998-9c33-74b94351c674', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FEMContractSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2e45bfda-0eff-476b-ab94-757730d61ec3', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_Employee')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6222d532-baa7-44c5-9795-75a0107eecf9', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_StatisticsCode')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ac5a31e6-8308-4928-96b4-768609c04f55', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_Currency')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1bf8450e-136e-46d0-8567-775f2416ec20', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FDeductionTTDB01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('fec6e018-587a-4300-9bf1-777f89177c20', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_MaterialGoodsSpecialTaxGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('954f489f-3bb7-40b3-8598-78568b2a0247', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FDeductionResourcesTAIN01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('bb86fa3b-72ed-4ec6-98d3-7860949694da', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_StatisticsCode')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('94c06eae-e170-4d1e-a0d4-7877fd1b2df2', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FTaxFinalizationResourcesTAIN02')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7145c657-c0c7-41d6-8b4e-78b27efc7166', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_AccountDefault')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('411d7806-78e4-4c20-9dfa-78bdfdc3b4d6', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FEMContractBuy')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3186228b-e5ed-4f67-ab31-793c301982d8', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_BudgetItem')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4d1ccd4c-0430-43b7-997b-7966996aeeea', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_FixedAssetCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('fcf72bc1-7688-4851-96d7-7a8219b0f85d', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FRContractStatusSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('50e2e8ae-7dc7-4d3e-9ec2-7b0f042695e0', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_AccountingObjectGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c47d0324-4aad-4eac-aec7-7b5b0464ccc0', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FEMContractBuy')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2f0804e8-06f0-4ef4-8346-7bff0ed5e0e8', '00919a7f-5681-4890-9d5d-f436ea2f9524', '48e1816b-43a7-4799-a391-c881e98c7b36', N'Rpt_FRTongHopTonKho')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b2dc0cf3-63d2-4ad3-8a78-7c13042af979', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_MaterialGoodsResourceTaxGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6b2e66a0-6b4f-47a8-a5c8-7c2b000b617a', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_MaterialGoodsSpecialTaxGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a101d912-15e4-48ae-9199-7cdd7f4f13a2', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_InvestorGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4517e0b1-1377-4fc2-87d6-7d52147bcbef', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_MaterialGoodsCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e2b516cf-6fe4-4d77-87a2-7e4756469c3a', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_ExpenseItem')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('780c0ed3-c152-4241-be13-7e8d8c440366', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_RegistrationGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('5c3b44b1-ded5-43a4-baa3-814638a92c43', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FTaxInvestmentProjects')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('29ebf488-0484-4414-a2ff-8249ade18fee', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FRContractStatusPurchase')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('20c92326-ee51-4bf9-916f-82b422b5284c', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FDeductionResourcesTAIN01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('89966b1b-a9b9-493f-a73f-830e4ae67127', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FDirectGTGT04')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7fe97686-df4c-4f00-ad82-8322f372a57f', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_MaterialGoodsSpecialTaxGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0cff67ec-9a7b-475e-8fd1-836e267fb97a', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_AccountingObjectGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('baa7ea6b-cb35-4e84-b901-845897954da4', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_InvestorGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2c1ab990-6a22-4a0f-a681-851c9f381f0c', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_AccountDefault')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e833ffd5-92a6-4263-8e01-8527b788bd71', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FTaxSubmit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8ee77a25-680b-408a-b996-852d082d48bd', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_Bank')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8b819548-b72f-41bc-8114-85be6a685698', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_BudgetItem')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('37d6c49b-d29b-4eae-9c67-85c1bbc79e96', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FEMContractSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4d240519-edad-48b7-ada0-87acdd6985fa', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FDeductionTTDB01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b3e5b26d-f730-48ba-bc98-87b652849c73', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FDeductionResourcesTAIN01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8db89d03-161d-498d-9971-87e574cab5b8', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_PersonalSalaryTax')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('cf4f9564-63bd-44fd-9efd-88d4e3c43647', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_Account')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d4771bfc-97c3-47d4-8029-891b8ec80f15', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_BankAccountDetail')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ffc9b49c-c9c9-4a16-a00a-8965900060e7', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FTaxFinalizationResourcesTAIN02')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('899a8d41-6626-4e26-a056-89a120bbe6fc', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_MaterialGoodsCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d0b25812-485e-49db-85a4-8bf6af16d9ff', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_AccountingObjectCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3ee3d8db-c57b-4536-af7a-8caa8e111518', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_Bank')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c3ba8a4a-ed05-406a-ac4b-8d1e91f4df99', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_Account')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('530997a0-4d43-40dd-a167-8d44eaa400e8', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_MaterialGoodsResourceTaxGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c37cd349-3177-4e6a-be07-8df32dcac1b3', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_Employee')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2f974c9d-8ae1-4554-ac05-8e5d23eb682b', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_ContractState')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('da434838-62e0-4011-93db-8ec779e444ce', '00919a7f-5681-4890-9d5d-f436ea2f9524', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'Rpt_FRTongHopTonKho')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4b2c2f7e-2973-45fb-8f7b-8f041b5431c0', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_BudgetItem')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('715021ab-0810-469d-b4f4-8f85f7f9f897', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_MaterialGoods')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e226edb9-d763-4df8-a487-903b3f62a87e', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FDeductionResourcesTAIN01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('30caa9ab-fd74-4229-a0a0-9141101e1261', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_FixedAssetCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('534ce8a6-6112-40d8-aad8-91bcf89df021', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a886435f-8b33-4e4b-a62a-91bfc179306f', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'TaxPeriod')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('46dac8ce-0dd4-41b8-bbf9-91cc9e2822c7', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_PaymentClause')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e3530e3b-47e8-4677-865d-9230e9df4fdc', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('29df2d01-6f49-4eb9-b7b3-9261c8e36f2f', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_ShareHolderGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4ef73a0c-5a37-48de-a3ea-929dcb73a473', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'FEMContractBuy')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('640a6fa3-bd0f-450c-9418-933ed623cf1b', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'TaxPeriod')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6512c622-4c5f-4bc4-badf-93a788343ee2', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_TransportMethod')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3eb0672c-2f7f-42be-a390-93e99686ec6e', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FDirectGTGT04')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('eeec2614-b9c4-4e52-aa13-94159f29559a', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_Currency')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('65adb5bd-4512-41b9-ad77-944e8f67ede9', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FTaxInvestmentProjects')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0a72f273-284d-4bfa-857c-94e2f292df32', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_CostSet')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('93f0c3ce-c291-4981-b947-959232406aa8', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_AutoPrinciple')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('03c100bd-857d-431d-9aad-96662d3a0880', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_AccountTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b8acd476-79c3-4c05-a636-978d746ff65a', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_PaymentClause')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c55afccd-5ca4-4340-81dc-983d867a17e3', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FRContractStatusPurchase')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0992c29a-a017-4ec3-a96c-996f8c622f34', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_AccountDefault')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1ca2d0a4-7ec0-461f-a7a1-9999bdb67d13', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_ExpenseItem')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('dc81f605-d2f8-42be-8569-9a80d376cc1b', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FDeductionResourcesTAIN01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c3ab405d-4a30-4d21-8b53-9ba0e226fab4', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_MaterialQuantum')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('afc42bf1-089b-4751-8e60-9de07219d7c4', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_StatisticsCode')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('63fd5cde-c503-438c-bfe1-9e4d5f01d724', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_AccountingObjectGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7a12a3f3-6fbf-4f12-8a9b-9f2a59ced224', 'c4e0a64b-9aa0-430c-ab9a-4c23449d6c64', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'Rpt_FRS11DNN_PBCCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0542e026-60d5-44df-bcd2-9fa58c966cb7', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_MaterialQuantum')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('902fad78-a40b-40ef-97c2-9fe8a6d60a29', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'FTaxInvestmentProjects')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0c877564-bba0-4059-b160-a12e5da90472', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_FixedAsset')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d2a2ebac-2fc8-492b-b718-a17b5cebc4f4', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_AccountDefault')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('9e9f17f6-93b8-46e7-bca7-a198473609cc', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FDeductionGTGT01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('5c1105ae-1e18-421a-9e2d-a364a99e23ba', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_CostSet')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7d6a7bf6-3dc6-4d11-adac-a3a15f229bad', '00919a7f-5681-4890-9d5d-f436ea2f9524', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'Rpt_FRTongHopTonKho')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0c15e250-6484-4103-9f0a-a4324172d4c0', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_BankAccountDetail')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4d7cf432-8996-4f21-a215-a4adc12e4935', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FDeductionResourcesTAIN01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2b2a3b46-87d3-4f0f-b757-a4bf23c19086', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_AutoPrinciple')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8c66856f-b13d-459a-b945-a5f3a3067985', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FDeductionResourcesTAIN01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1924114f-58ba-4726-a253-a688a373eb41', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_CreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('5b9bd385-e2ac-4f67-afa8-a6ebf320a785', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FDeductionGTGT01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ecbe1df5-c8e1-4089-ac31-a82a38e30c93', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_ShareHolderGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('dff4f0c3-fc41-4ca1-990c-a8fd4080e295', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_BankAccountDetail')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7503f8b8-3197-47eb-9bc5-a9a06f54093c', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FTaxInvestmentProjects')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('824c8c07-f7fb-4f7c-ab91-aa76f823beee', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_ShareHolderGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7d39abd4-b3e0-47af-ad4b-ab00acc32830', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_FixedAssetCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b6cfad4f-1edf-4f0a-9893-ab36c3070bc5', '00919a7f-5681-4890-9d5d-f436ea2f9524', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'Rpt_FRTongHopTonKho')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('9d2b181c-839a-4307-9aea-abfe6f54dc7b', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_Investor')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ff9686f5-1dc9-4524-8c3a-ad2611fe20e5', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_MaterialGoodsCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a5cebe88-e4f3-4e9c-998d-ad73b281007b', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_TimeSheetSymbols')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('815b1088-df7f-4f89-a8e6-adae899d8dcd', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FDeductionTTDB01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('262ddc53-aaf4-4f42-8da6-ae2d54eec83b', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_Repository')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('df6cbb65-aff2-4974-9529-aee9466a4bac', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_AccountingObjectCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('97cc3575-f2ea-47ac-82b7-afa4d5722da8', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_Bank')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('666e8f80-5574-459c-9444-b04afc8c00c3', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FTaxFinalization')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('cee32e90-d275-49b5-b598-b0a7e97ba82e', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_Department')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3b8ce4ab-de60-4de7-8cbe-b0bac58e4beb', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_ContractState')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('65350161-15f6-4bbc-91a3-b0bbf8317c16', '00919a7f-5681-4890-9d5d-f436ea2f9524', '18906ddd-6508-4431-89ce-374154d52695', N'Rpt_FRS11DNN_PBCCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('136e0064-590e-4e83-9a16-b0dd767403fd', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_MaterialGoodsCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('50551813-0392-4682-b447-b18b89dba568', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_AccountDefault')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a92836b5-e0b1-419c-b2c6-b1bc39450338', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_MaterialGoodsCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('60a7c40b-66db-4bd4-9876-b24b6266e6b8', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_Department')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2a4fecd6-80f6-4c5a-80ae-b2e7ffcf46ae', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_AutoPrinciple')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('13ece585-494a-451c-9ae0-b35d7a1df4bb', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'FRContractStatusPurchase')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('339f4630-61ef-473a-8da0-b39ed34c4916', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FTaxInvestmentProjects')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8888694a-4555-4e50-b4c3-b4c013cc2fb0', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FTaxFinalization')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('64c3fd93-047a-435f-a80e-b4f5c35e1c3a', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_MaterialGoodsSpecialTaxGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e10cf7c1-6112-412a-81d0-b500e192e873', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_PersonalSalaryTax')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('92af992b-cb92-447c-91d9-b570d52eb033', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_TransportMethod')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('054654ed-5961-4217-9a43-b5be5f316932', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_MaterialGoods')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ae1bc6b9-bcfa-4d48-9bbb-b6ca5d835c8d', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6a437d57-5dad-43c3-9933-b6dd6e6899fb', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_BudgetItem')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d17d69a2-e8b0-47e5-b8ab-b7140a2954c9', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_PersonalSalaryTax')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('eb5ab149-31d4-42f7-b9fe-b7e202113126', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'FDirectGTGT04')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('5ab69c0f-eca9-4079-b70e-b80d717d71b8', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FDirectGTGT04')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2eb508c7-4319-4aa2-a4f9-b90214d42443', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_RegistrationGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('aa05a888-510b-4c60-a6ca-b9f2db500253', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FRContractStatusSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('14509bf5-d14d-4d97-a359-ba554f20b67c', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_RegistrationGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('84c4d925-8aac-4bac-9b63-ba66a831b647', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_Employee')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8acba144-9241-4b77-bffc-ba67e9fa4a44', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FDeductionResourcesTAIN01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d3c18b87-c665-44b3-8998-baa0a955de23', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FTaxFinalization')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1b1d90c8-c6b0-48e4-944b-baf25828ae43', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_AccountingObjectCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('df7e6aa0-daaf-4ac5-9328-bc50366f2b3f', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('cc10cbde-fdd1-4cb7-8372-bcb9de747fe3', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'FRContractStatusSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2cf4113b-c8d3-4abd-b5d3-bdb613847f95', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FEMContractBuy')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1a77ba9c-f7ef-4d55-a851-be01d006c253', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_Repository')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('50863d4e-128b-4e3d-a33b-beab4ca88bef', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_FixedAssetCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2a93231e-948a-4560-b796-c08f8fe5e793', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_PaymentClause')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f456823c-0b2a-450b-a27f-c11bdfd30078', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_PaymentClause')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('71e67aa2-320a-4c8f-b66b-c19f076e9c8b', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_ContractState')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e826b634-aa44-4567-a11f-c1d79706f8aa', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_AccountingObjectGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('60658cb9-9a82-4d91-b739-c225f070ca0e', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_Repository')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('38b3c700-d394-4379-800f-c3119371a6f0', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_StockCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('87e02092-46dc-402a-ba01-c42256a2122c', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FEMContractSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a51dc53d-acdc-4669-ae5c-c426ea56baba', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FEMContractSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a201fd9a-0822-4add-b5fc-c4c5ccfb0c29', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_MaterialGoods')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7e8164e2-f605-4871-881a-c507bbce468e', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_Department')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6a90e376-c9dc-445f-abde-c570a782b67e', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_AccountingObjectCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e5cf54f4-0f8e-42fb-9596-c5e0764b4dc5', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_StockCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('aec00143-4bba-4148-a146-c5ea9493f242', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_AccountingObjectCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('263404b1-8aef-4fee-b9a8-c60f9364c4e5', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_StatisticsCode')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('18380bf8-5e60-4b67-8f31-c625db923a01', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FRContractStatusPurchase')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('99384f15-7588-49a5-b823-c6547b328f89', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c139fec8-47cf-4284-a34b-c68a309d3117', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_MaterialGoodsSpecialTaxGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d1427b7c-59f1-4415-bbd5-c72bbe9c3417', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d9ec6919-19fa-4441-9f57-c72fdaa1b8b8', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'FTaxFinalization')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7d0f654e-1bb7-4213-9647-c845da35c934', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('79fdb13b-e4e1-4654-a4f3-c92a2cb8e4ff', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_MaterialGoods')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c1537347-48ae-476f-8eca-c998d9d8a53d', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_Currency')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('702aebd0-f854-4a41-a9d3-c99daae2b3cf', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FEMContractBuy')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('005dbcf4-cfa4-4b83-9026-caf23eb53ca0', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_PersonalSalaryTax')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('cff29c3c-d14b-4068-83e4-cb16d5e46be0', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_AutoPrinciple')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('6042e432-085d-4b75-990d-cb6bfec7c320', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_SalePriceGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('9af82fed-d78d-4401-b01c-cbb39df3da52', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_FixedAssetCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('cdb0455a-6417-4789-a267-cc7525968fa0', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_BankAccountDetail')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1e208032-1863-4f05-942c-ccaba19c7dc9', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_FixedAssetCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ee16819a-f0d9-450d-9e80-cd2026025242', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FDeductionGTGT01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('aff1ee3e-022b-4d4e-ac54-cd3a05eae617', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'TaxPeriod')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('caf84d30-1f91-4ed9-a9bc-cd3c0085fee6', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_PaymentClause')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('08443cd8-ae0a-4129-bbe0-cd74a373c7f3', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'TaxPeriod')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a4e25af8-30f5-43df-9b92-cd797374c467', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'TaxPeriod')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0838f58d-c176-4773-9d53-cdd3398ffa32', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e41f782f-b281-4fe6-be5b-ce5d252cd28a', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_Currency')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('50ffa33d-fd3e-4c51-9496-ceddf4201fb0', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_StatisticsCode')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e1bdbcc6-ad9a-4280-b672-cee6372c6d78', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FTaxFinalizationResourcesTAIN02')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d8c19f63-e8da-46e7-9ec9-cfb915aba4af', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_StatisticsCode')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('91179adc-fc3e-434a-be5a-cfd932bfbd85', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_PersonalSalaryTax')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('65f82cb5-24f6-4592-a035-d14d7c52502f', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_AccountDefault')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e9952e4c-35b1-442d-97fe-d1d7bbc295d2', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_MaterialGoodsResourceTaxGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7f6e0752-f09b-4184-b29a-d20bc2c5c707', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_Bank')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ae213539-509a-42e5-bffb-d29598d8bd46', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_Bank')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('23edc715-5547-4392-bb0d-d3625fdfa905', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FTaxSubmit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('94adc915-8a46-4666-8189-d43e7a25df86', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FEMContractSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0928be9e-2cf2-44e7-b45d-d4667a18d9a8', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_ShareHolderGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('01c8b8b0-ef1c-42b3-ab6a-d5a9a40f6db6', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_PaymentClause')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('38b1dc95-0a10-4381-8b37-d61bfe666654', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FTaxFinalization')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7ac9796e-e1df-4f3d-9b43-d6517e9e10de', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_CostSet')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f0804077-3eb8-4db6-bd52-d6c96b0f238e', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_CostSet')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('067f84f6-c290-4353-8bec-d701e40bb1ec', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FTaxSubmit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('86c215b8-e351-4eb8-88c6-d71c977a31e2', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_Department')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c649dc2a-3488-4bf0-9066-d86a9444a4d5', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_Investor')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('0d428351-c68e-4dfa-9b17-d86cee7a2bbd', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_AccountTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ad062151-4656-45c6-bad4-d884f6bc3f7f', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('54e263f2-4416-40dc-b44b-d8ab2df2e497', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_Bank')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('72ce561d-79e0-4744-9097-d8cb27fbc3aa', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_FixedAssetCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('09e9ff77-c59e-4199-8100-d8e1378334fb', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_BankAccountDetail')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('fe326ac1-7f7d-4711-960e-d91e49a2fe9d', '12b1bbd7-498d-4386-9203-ccf0bf462aff', '48e1816b-43a7-4799-a391-c881e98c7b36', N'Rpt_FRTongHopTonKho')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('23d9145f-8eb2-45ac-88ba-d9a8c8ecddc5', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'd939f97a-1cde-46c6-b852-a579b0bf1337', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d67f267f-240d-48a1-805d-d9ab2a1c9e13', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FDeductionTTDB01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c43cea9d-030e-4c4d-a76b-dbbe08cc2544', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FEMContractSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f7d8fcab-c172-4113-aabc-dc89d75f0aa8', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_Currency')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1144c5e7-9a63-440a-9ca8-dd89304f86bf', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_StockCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('833f0230-b698-450b-b3e7-dd99e1334b96', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a5e92844-25a1-45d7-bfc2-de26883db597', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FRContractStatusPurchase')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('85a41a22-2336-4142-bd27-de36546fea61', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_RegistrationGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('9f2350c9-c938-46e6-968b-de797aee8799', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_BankAccountDetail')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('661e30f6-39b0-490e-bf41-dea202d7dbd6', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_Repository')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('84db086a-415e-4340-801b-df3f3fc40503', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_InvestorGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4f8222fc-eac7-421c-a366-df5b84d0437f', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FEMContractBuy')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b96215e1-06a0-43d9-b34c-dff36f27876e', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8a998f9a-5613-47e0-90e5-e0578c5294b7', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_CostSet')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b3a3d057-dff7-4077-bfaa-e096e3e1925d', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_MaterialQuantum')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1acb9d8a-a77a-483c-bb90-e10222ea403b', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_TransportMethod')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('afe29b7e-608c-4c79-9fa6-e10629d23d2d', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FRContractStatusPurchase')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b7cf4a62-47ae-4ed2-8a89-e156904c93fb', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_MaterialGoods')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d4d2f7f3-6ea2-4c55-bf8e-e19261412558', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FDirectGTGT04')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b2adf3af-814e-4151-8f5a-e2025e224156', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FDeductionGTGT01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2175985a-4203-4cb6-ae1c-e2238efe432f', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_Department')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('c25a4ff5-31ed-4206-9d31-e25aec528e3b', '12b1bbd7-498d-4386-9203-ccf0bf462aff', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'Rpt_FRTongHopTonKho')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('067a29da-7767-4363-98da-e2e1429df722', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'FTaxFinalization')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a778cb9e-df59-4a25-aed1-e42e80615dfc', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_AccountingObjectCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3588d1a7-0c68-4c10-a570-e440917a943f', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'FDeductionResourcesTAIN01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('121d14fb-a50d-4498-a457-e55b03ef3f34', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FTaxInvestmentProjects')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('765d951b-39c0-4e3d-8124-e5b79ee906e2', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'eada2a3f-dcee-461a-b150-de8f6a0672a5', N'FDeductionGTGT01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e4958240-dfc3-477c-b16f-e63cfe2096d3', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FRContractStatusPurchase')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('34444e0b-156d-43fc-ad62-e643707da33d', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_AccountingObjectCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7ffec5d0-cb08-4ed9-b055-e6eec5c81906', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FRContractStatusPurchase')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ea79669f-56b1-4e30-a15f-e7cdf9d63828', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_AutoPrinciple')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f9c25683-95eb-4e7e-8eec-e9625af7a387', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_AccountTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ce56f494-7607-4e18-b789-e973495249f1', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_CCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f28517ba-a98e-4299-994a-ea82f60c73ec', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FTaxSubmit')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e65e6b41-12a1-4073-a1b2-eb1a0da28847', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_FixedAsset')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('fbc829a4-4335-4894-bc9a-eb3d4d3d4232', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_Investor')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('2a711b1a-ea0c-4e6d-9f55-eb7ccb11f958', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_ExpenseItem')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('34305195-4c78-40e4-aef5-eba2594ccd68', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_AccountingObjectCustorm')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('08cd26c3-8f35-4f4c-87f4-ebc9232b681d', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_AccountTransfer')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('591f7053-2b47-4a8a-b9ac-ebf27c987001', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FDirectGTGT04')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('31aa187e-c4f7-4029-9100-ebf941821440', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_FixedAsset')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('1e2028a4-d20c-4904-9055-ec2b7c926e9d', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_InvestorGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('317d8b7c-1690-4f6b-95a1-ecc39763d68c', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_FixedAssetCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('59805f31-1f34-4936-b919-ed058baa0ff3', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_MaterialGoodsCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4cfc27c2-dd4d-4edb-b44d-ed17184bcf98', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'FDeductionGTGT01')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('449a6094-df49-4658-b134-ef29ef5e4eb2', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_BudgetItem')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3b8255f4-c184-4f18-8be7-efa790311e80', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_SalePriceGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('78f8da30-6490-412b-a3b4-efe97422781f', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_RegistrationGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('436fc749-80c4-4911-a39d-f01dcdf6a62d', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'FTaxInvestmentProjects')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3319a22a-4778-43e4-aea3-f0a4ac3ebc3b', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FRContractStatusSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4eabe6ad-348d-4bed-acc5-f1916be4c762', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('34e6c2ba-05c2-4e44-aa07-f1af8e04babf', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FDirectGTGT04')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3a73392d-c4e2-44f5-80dc-f23fb6cca760', 'c4e0a64b-9aa0-430c-ab9a-4c23449d6c64', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'Rpt_FRS11DNN_PBCCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('8494cd8c-7e8f-43ea-87b2-f247f3fcd875', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_ExpenseItem')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('3f65c911-f50c-423c-abf4-f25b5f1fd94c', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_Department')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('de87c041-5991-4233-9e38-f2b26546d423', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_MaterialGoodsResourceTaxGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('b43a871f-78bc-4b99-816b-f2f312bd29f3', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FRContractStatusSale')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('04514040-9b90-4868-af31-f334b8c88bb0', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_Currency')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('dc776846-dc28-47ed-adc8-f336831d05fc', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'btnQuyDinhLuongThueBh')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('433702ef-6c00-449e-8c1e-f36a52c6eb28', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'TaxPeriod')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ee538db4-6c1e-4f3d-9c6d-f5e2820c9478', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_ContractState')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('9740e51b-98d5-4ba7-83ed-f6621af2af64', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_ExpenseItem')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('07e01d67-e87d-440f-94b0-f6ccbd79aa59', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FTaxInvestmentProjects')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('cd03f9ac-f899-40d4-941a-f74779093214', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'FDirectGTGT04')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('900f1364-4417-4273-9bf6-f77f2f019a9d', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '48aa6d0a-2797-4068-9561-e876c7e9b94b', N'FTaxFinalizationResourcesTAIN02')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('5dc9e8a5-45f4-4bf2-999b-f78076a7995e', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_FixedAsset')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('709f7a70-3423-4f75-8d4e-f78fc955820d', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_AutoPrinciple')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('7baa2023-5276-4aec-aac5-f8671b717c76', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'f04171ba-a4d6-48da-8970-06548efcfeb4', N'TaxPeriod')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('ef7e55a4-992f-4ff7-9dbc-f8a1d1ea0a7d', '00919a7f-5681-4890-9d5d-f436ea2f9524', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'Rpt_FRS11DNN_PBCCDC')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e374824a-5f46-4741-8421-f9b57bce9c0d', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_TimeSheetSymbols')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('acfede02-e751-4499-bca2-f9c1d854c5f0', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'CAT_Department')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('f7743760-a84f-4be9-9839-f9da7b11c54c', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '782abe32-4eca-4a1d-9f35-375ed60ca632', N'CAT_AccountingObjectCustorm')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('73934f3f-be0f-4dcd-83c6-fa0396ab62cb', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_CreditCard')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('e1ef108c-3687-4ad5-8eed-fba0b038c2b9', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_ContractState')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('58ab9ffb-4904-4170-ad34-fc7f8286defd', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_AccountDefault')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('a550b93b-1ea1-42e2-b341-fcb4012c242f', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1c04fbb7-958b-4aba-a9bc-a32c4a8a91d5', N'FTaxFinalizationResourcesTAIN02')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('bb092023-3290-4896-ab8c-fcbb09290838', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '1485b1c3-d0c5-4d4f-97eb-706133235da1', N'CAT_MaterialGoodsResourceTaxGroup')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('36659600-4b88-42e3-abb5-fe2d16d39f8e', 'b17679c4-42e9-4495-97c7-c6a01a09630f', 'a3fb9e36-b930-4090-8aa9-957d9e82c139', N'FTaxFinalizationResourcesTAIN02')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('d1deca82-ec90-40c5-b2df-fe696a8a9423', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '944b2211-d659-4a4f-adad-c2043de0f5fa', N'FTaxInvestmentProjects')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('20cefcdd-7eae-40d7-9117-fe833f514607', '1053ed42-1d6a-4d98-b7dd-b2a5d1300563', '18906ddd-6508-4431-89ce-374154d52695', N'CAT_StockCategory')
INSERT dbo.Sys_RolePermission(ID, RoleID, PermissionID, SubSystemCode) VALUES ('4c3b4de2-3854-4e07-845d-ffd884ee7f34', 'b17679c4-42e9-4495-97c7-c6a01a09630f', '815398c1-06aa-451a-ba46-1d8e446f5e38', N'FTaxSubmit')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.SystemOption SET Code = N'SLUU_SauKhiDongCT', Name = N'Tự động sao lưu dữ liệu khi đóng chương trình', Type = 6 WHERE ID = 78
UPDATE dbo.SystemOption SET Code = N'SLUU_KhongLuu', Name = N'Không sao lưu dữ liệu khi đóng chương trình', Data = N'0', DefaultData = N'0' WHERE ID = 80
UPDATE dbo.SystemOption SET Code = N'SLUU_NhacKhiDong', Name = N'Nhắc nhở sao lưu dữ liệu khi đóng chương trình' WHERE ID = 81

INSERT dbo.SystemOption(ID, Code, Name, Type, Data, DefaultData, Note, IsSecurity) VALUES (133, N'SLUU_NgaySLUU', N'Khoảng thời gian sau lần sao lưu gần nhất', 1, N'5', N'5', NULL, 0)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.Template SET IsSecurity = 0 WHERE ID = '335f1c3e-62ab-4403-99b6-4f2b1e9a5e38'
UPDATE dbo.Template SET IsSecurity = 0 WHERE ID = '85dc5dee-81cf-4b54-a3a4-b6f57fb4fa96'
UPDATE dbo.Template SET IsSecurity = 0 WHERE ID = '774c10b8-d918-454f-af43-d4d5805a0ef3'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.TemplateColumn SET ColumnName = N'DiscountAmountOriginal', ColumnCaption = N'Tiền CK ', ColumnToolTip = N'Tiền chiết khấu', ColumnWidth = 110, VisiblePosition = 36 WHERE ID = '9124b608-885a-4029-a356-00d5d1f22864'
UPDATE dbo.TemplateColumn SET ColumnName = N'QuantityConvert', ColumnCaption = N'SL chuyển đổi', ColumnToolTip = N'Số lượng chuyển đổi', ColumnWidth = 130, IsVisible = 0 WHERE ID = '8e254896-4131-4c04-821b-046eb79482a7'
UPDATE dbo.TemplateColumn SET ColumnName = N'VATAmount', ColumnCaption = N'Tiền thuế GTGT', ColumnToolTip = N'Tiền thuế giá trị gia tăng quy đổi', ColumnWidth = 125, IsVisible = 1 WHERE ID = '95af5782-5672-4214-8863-0687a5731586'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 0 WHERE ID = 'd3271f71-0242-4c91-a486-07fc2ece0fe0'
UPDATE dbo.TemplateColumn SET ColumnName = N'AmountOriginal', ColumnCaption = N'Thành tiền', ColumnToolTip = N'', IsReadOnly = 0, IsVisible = 0 WHERE ID = 'ef010b0b-6d91-4b76-a6a2-0c6e4391730c'
UPDATE dbo.TemplateColumn SET ColumnName = N'Waranty', ColumnCaption = N'Thời hạn BH', ColumnToolTip = N'Thời hạn bảo hành', ColumnWidth = 95, IsVisible = 0, VisiblePosition = 45 WHERE ID = 'd867edf1-df6f-4d42-b120-1129787f470f'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 0 WHERE ID = '6dc9d8e1-fa25-4d8b-88cb-14dc1e49e2db'
UPDATE dbo.TemplateColumn SET ColumnName = N'VATRate', ColumnCaption = N'% thuế GTGT', ColumnToolTip = N'% thuế giá trị gia tăng', ColumnWidth = 101, IsVisible = 1 WHERE ID = '1af330be-0051-433b-985e-15fcc313da16'
UPDATE dbo.TemplateColumn SET ColumnToolTip = N'', ColumnWidth = 85 WHERE ID = '82dcfade-65b3-4b7f-9b93-1c2af9f3cf88'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng', ColumnToolTip = N'', ColumnWidth = 400, IsVisible = 0 WHERE ID = 'bf6e1a64-1776-4e57-aaad-1d2d4e0d2b64'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'c020ca50-0841-46ba-82ef-21942fe0093a'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '5a9609ae-16c6-453b-a49f-266e2ed62d36'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Số chứng từ' WHERE ID = '51a336cb-3535-45aa-a44f-27cfa50cccfc'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '0dfd79cb-e3cb-4c64-b596-2b0c63d63a0d'
UPDATE dbo.TemplateColumn SET ColumnName = N'LotNo', ColumnCaption = N'Số lô', ColumnWidth = 85, IsVisible = 0, VisiblePosition = 44 WHERE ID = 'e7cdd038-0858-4a98-bee3-2fcdbebb4670'
UPDATE dbo.TemplateColumn SET ColumnName = N'ExpenseItemID', ColumnCaption = N'Khoản mục CP', ColumnToolTip = N'Khoản mục chi phí', ColumnWidth = 130 WHERE ID = '5a136b51-e271-4ee4-9125-461e901aee07'
UPDATE dbo.TemplateColumn SET ColumnName = N'DiscountAmountAfterTaxOriginal', ColumnCaption = N'Tiền CK sau thuế', ColumnToolTip = N'Tiền chiết khấu sau thuế', ColumnWidth = 110, VisiblePosition = 38 WHERE ID = '1b7b90fe-bf6d-4583-a0fc-4dc2569b5ec1'
UPDATE dbo.TemplateColumn SET ColumnName = N'UnitPrice', ColumnCaption = N'Đơn giá QĐ', ColumnToolTip = N'Đơn giá quy đổi', ColumnWidth = 140 WHERE ID = 'be49a0d0-d859-4001-9a1d-4ff8ca550b74'
UPDATE dbo.TemplateColumn SET ColumnName = N'DebitAccount', ColumnCaption = N'TK Nợ', ColumnToolTip = N'Tài khoản nợ', ColumnWidth = 95 WHERE ID = '47e2af38-19ee-4e9f-b9a2-5131c51956e9'
UPDATE dbo.TemplateColumn SET ColumnName = N'DiscountAmountAfterTax', ColumnCaption = N'Tiền CK sau thuế QĐ', ColumnToolTip = N'Tiền chiết khấu sau thuế quy đổi', ColumnWidth = 130, VisiblePosition = 39 WHERE ID = '9609e0c2-d268-420f-bc26-5253f8c69a43'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = '577600a0-23a8-47fa-9015-5a41fa738d8e'
UPDATE dbo.TemplateColumn SET ColumnName = N'CreditAccount', ColumnCaption = N'TK có', ColumnToolTip = N'Tài khoản có', ColumnWidth = 95 WHERE ID = '8d2846b3-c72d-422d-a3ee-5e5bdbaf7988'
UPDATE dbo.TemplateColumn SET ColumnName = N'VATAccount', ColumnCaption = N'TK thuế GTGT', ColumnWidth = 150, IsVisible = 1 WHERE ID = 'a516a2ad-7a5c-47b9-86f6-5ee4c69cd03d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2a4f02a9-8ad5-4fad-8fc3-6e0f5304eff9'
UPDATE dbo.TemplateColumn SET ColumnName = N'CareerGroupID', ColumnCaption = N'Nhóm ngành nghề tính thuế GTGT', ColumnToolTip = N'Nhóm ngành nghề tính thuế GTGT', ColumnWidth = 120 WHERE ID = 'fc4f034a-84a3-47aa-90cd-71dce3f58e29'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 0 WHERE ID = '2e3fc0fd-384e-4cdc-b4b9-75870485aaf6'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 0 WHERE ID = 'e722e877-fbde-47fa-b271-795a2f33692a'
UPDATE dbo.TemplateColumn SET ColumnName = N'IsPromotion', ColumnCaption = N'Là hàng KM', ColumnToolTip = N'Tích chọn nếu là hàng khuyến mại', IsColumnHeader = 1, ColumnWidth = 100 WHERE ID = 'abfd2616-cb42-4931-806d-7ac696e19b7a'
UPDATE dbo.TemplateColumn SET ColumnName = N'UnitPriceOriginal', ColumnCaption = N'Đơn giá', ColumnWidth = 110 WHERE ID = '498d64f5-3b0a-48db-ab55-7dd98e4e0a3e'
UPDATE dbo.TemplateColumn SET ColumnName = N'OWPrice', ColumnCaption = N'Đơn giá vốn', ColumnToolTip = N'Đơn giá vốn quy đổi', IsReadOnly = 1, ColumnWidth = 130, IsVisible = 1 WHERE ID = '0b4cb582-0d00-448e-9bc5-7e2414004bb8'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 0 WHERE ID = 'e8a76f99-66b4-4a20-af48-82aac1ec00e7'
UPDATE dbo.TemplateColumn SET ColumnName = N'RepositoryAccount', ColumnCaption = N'TK kho', ColumnToolTip = N'Tài khoản kho', ColumnWidth = 95, IsVisible = 1 WHERE ID = '61fe6333-aa79-4a27-8414-8cdf74d49c57'
UPDATE dbo.TemplateColumn SET ColumnName = N'AmountAfterTax', ColumnCaption = N'Thành tiền sau thuế QĐ', ColumnToolTip = N'Thành tiền sau thuế quy đổi', IsReadOnly = 0, ColumnWidth = 130 WHERE ID = '09b60c1b-e4bb-4565-8e2f-8de2893ac9ed'
UPDATE dbo.TemplateColumn SET ColumnName = N'CreditAccountingObjectID', ColumnCaption = N'Đối tượng Có', ColumnToolTip = N'', VisiblePosition = 43 WHERE ID = 'eebc9565-d0f7-4f4d-a134-8ef6834cfd2d'
UPDATE dbo.TemplateColumn SET ColumnName = N'ConfrontID', ColumnCaption = N'Số CT đối trừ', ColumnToolTip = N'Số chứng từ đối trừ', ColumnWidth = 95, VisiblePosition = 41 WHERE ID = '650d88a5-d2c7-475f-8b86-94d25fb7cb58'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = '2e1db53b-5400-4ce6-b420-966b50ed17a9'
UPDATE dbo.TemplateColumn SET ColumnName = N'OWAmount', ColumnCaption = N'Tiền vốn', ColumnToolTip = N'Tiền vốn quy đổi', IsReadOnly = 1, ColumnWidth = 109, IsVisible = 1 WHERE ID = '62c2d9ab-783b-4427-9e37-9e8110748089'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7c92c1be-16d8-4b7d-94cf-9ff5b8040d98'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '2070be10-cf42-44ce-9ad2-a366e48d3556'
UPDATE dbo.TemplateColumn SET ColumnName = N'Quantity', ColumnCaption = N'Số lượng', ColumnToolTip = N'', ColumnWidth = 110, IsVisible = 0 WHERE ID = '6df63380-31b1-41a5-a080-a46efc000a95'
UPDATE dbo.TemplateColumn SET ColumnName = N'UnitPriceConvert', ColumnCaption = N'Đơn giá chuyển đổi QĐ', ColumnToolTip = N'Đơn giá chuyển đổi quy đổi', IsReadOnly = 0, ColumnWidth = 140, IsVisible = 0 WHERE ID = '1aaa689e-daa9-4a06-be0a-a99305ba59aa'
UPDATE dbo.TemplateColumn SET ColumnName = N'VATDescription', ColumnCaption = N'Diễn giải thuế', ColumnToolTip = N'', IsColumnHeader = 1, ColumnWidth = 400, IsVisible = 1 WHERE ID = 'a43945b5-8fbb-46a8-9f3d-c1d03dad3770'
UPDATE dbo.TemplateColumn SET ColumnName = N'DiscountAmount', ColumnCaption = N'Tiền CK quy đổi', ColumnToolTip = N'Tiền chiết khấu quy đổi', IsReadOnly = 1, ColumnWidth = 110, VisiblePosition = 37 WHERE ID = '3d93a6b2-805f-4017-a40f-c5e49c80bb24'
UPDATE dbo.TemplateColumn SET ColumnName = N'Unit', ColumnCaption = N'ĐVT', ColumnToolTip = N'Đơn vị tính', ColumnWidth = 75 WHERE ID = '0e159c67-c89b-445b-a173-c76ea04aa131'
UPDATE dbo.TemplateColumn SET ColumnName = N'SAOrderNo', ColumnCaption = N'Số đơn hàng', ColumnToolTip = N'', IsReadOnly = 1, ColumnWidth = 110 WHERE ID = '61f3694e-1ef9-44e7-a3da-c7bc526453c1'
UPDATE dbo.TemplateColumn SET ColumnName = N'AmountAfterTaxOriginal', ColumnCaption = N'Thành tiền sau thuế', ColumnToolTip = N'', ColumnWidth = 130 WHERE ID = '9f2de2b7-7a32-461a-adef-cb7561ca7da3'
UPDATE dbo.TemplateColumn SET ColumnName = N'PanelQuantity', ColumnCaption = N'SL tấm', ColumnToolTip = N'Số lượng tấm', ColumnWidth = 110 WHERE ID = 'f6d54274-cc86-4fa1-91d5-cc1a68fdba1f'
UPDATE dbo.TemplateColumn SET ColumnName = N'BudgetItemID', ColumnCaption = N'Mục thu/chi', ColumnToolTip = N'', ColumnWidth = 105 WHERE ID = '1abb611c-d55b-420a-ae73-d1b3f586c926'
UPDATE dbo.TemplateColumn SET ColumnName = N'UnitPriceConvertOriginal', ColumnCaption = N'Đơn giá chuyển đổi', ColumnToolTip = N'', ColumnWidth = 140, IsVisible = 0 WHERE ID = '1e5b6bac-3960-47e6-931a-d3d151c9f5a9'
UPDATE dbo.TemplateColumn SET ColumnName = N'DiscountRate', ColumnCaption = N'Tỷ lệ CK', ColumnToolTip = N'Tỷ lệ chiết khấu', ColumnWidth = 80, VisiblePosition = 35 WHERE ID = 'ee812239-871f-40e0-8fa4-d5be1c6ffecd'
UPDATE dbo.TemplateColumn SET ColumnName = N'ExpiryDate', ColumnCaption = N'Hạn dùng', ColumnToolTip = N'', ColumnWidth = 95, VisiblePosition = 40 WHERE ID = 'cfae60c4-ded4-4ced-9f30-def4d9cf7124'
UPDATE dbo.TemplateColumn SET ColumnName = N'PanelHeight', ColumnCaption = N'Chiều cao', ColumnToolTip = N'', ColumnWidth = 110 WHERE ID = 'aaf1c0f4-317a-4f5b-9c9a-e09e676fe53a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 0 WHERE ID = '1896259c-96e1-4a9e-b8b8-e3d76bce48c1'
UPDATE dbo.TemplateColumn SET ColumnName = N'VATAmountOriginal', ColumnCaption = N'Tiền thuế GTGT', ColumnToolTip = N'Tiền thuế giá trị gia tăng', ColumnWidth = 105 WHERE ID = 'e014c5a9-597c-4df1-af2c-e405b85be158'
UPDATE dbo.TemplateColumn SET ColumnName = N'UnitPriceAfterTax', ColumnCaption = N'Đơn giá sau thuế QĐ', ColumnToolTip = N'Đơn giá sau thuế quy đổi', ColumnWidth = 130 WHERE ID = '7f3b3c63-af62-480d-b20d-e474e35e007a'
UPDATE dbo.TemplateColumn SET ColumnName = N'PanelLength', ColumnCaption = N'Chiều dài', ColumnToolTip = N'', IsReadOnly = 0 WHERE ID = 'b6694a47-a55d-49df-a65d-e9698e1fac1d'
UPDATE dbo.TemplateColumn SET ColumnName = N'AccountingObjectID', ColumnCaption = N'Đối tượng Nợ', ColumnToolTip = N'', VisiblePosition = 42 WHERE ID = '5822e8e8-e47c-46ad-b0aa-ea52dab59250'
UPDATE dbo.TemplateColumn SET ColumnName = N'Amount', ColumnCaption = N'Thành tiền QĐ', ColumnToolTip = N'Thành tiền quy đổi', IsReadOnly = 1, ColumnWidth = 120, IsVisible = 0 WHERE ID = '6bd5f4bc-ae79-4b5e-a51a-ebe2afd7e39d'
UPDATE dbo.TemplateColumn SET ColumnName = N'PanelWidth', ColumnCaption = N'Chiều rộng', ColumnToolTip = N'' WHERE ID = '7a2cec3e-7659-446f-bcbb-efc2a11a1530'

INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fc27a63f-a860-4923-93d8-00c50b90981d', '6dcee98d-b159-44df-a3b0-ff96b7b7249e', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b68e16f1-b7c6-46ce-8189-02c4d6065bca', '80ebf9fe-9489-48c9-8a89-e153d4519d40', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3e824812-60f9-4098-8f12-044539eebf9e', 'f8b946d8-0078-47fd-a4f5-562498692501', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('72769688-9606-4f50-94e1-04d382091e3c', '64dcc499-6f8c-40d6-bbea-8467ee84c64d', N'ContractID', N'Hợp đồng', N'', 0, 0, 105, 0, 0, 0, 48)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('72a6a2ff-c6b2-4503-9961-056c340b967b', '910ccbed-14f1-4884-b793-e18d3f4f42c4', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2097d86a-080b-47c0-9e03-0593f918f59a', 'fd6cf906-6c8b-4a0f-bec0-f374e4aa09a2', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6e08ad6d-d749-45a0-b781-05b3c92619e6', 'b19ef4f8-0dd0-479f-9f81-da396e394809', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a355f881-7056-4a6f-92e6-07314c79477c', '127a2513-754b-4b4e-b667-5689faec1df6', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5bf8213f-160d-4726-9d69-0792610a2ce8', 'eab768db-a758-4b31-b168-ae956b220a49', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0eaa12dc-77f5-4655-9074-079ec49c422a', '0b9df75a-d40c-414e-8483-0e30961b4846', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7de835dd-7fa4-45a3-88a5-07b94c3faa30', '742becbf-87e5-4b93-899f-ed6561ff9ce2', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('962231e7-047e-4dd9-ae39-083a02296e40', '95bc5426-66eb-48c7-ad2d-573b948f2f1d', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1fc87c66-d82e-475a-9053-083d52f2e8a6', 'd526ef06-0afd-4579-b737-386bdd0941bd', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c316d687-25d8-4f92-97c7-0958ed16a0a4', '9e823114-3384-4996-8ee9-e942a3f0e930', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('899ac36a-50e6-4af6-9bdd-097cc6155605', '60f24c74-f4a1-46e9-a5ea-4659cd78ffe4', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f51eb1e7-0e5e-4464-a370-09c45483108d', '220dce55-01ee-4d0d-b78f-1439f0aafd58', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7759f004-1039-4ab6-b305-09f0db235033', '6ab6f01d-395e-4197-86e7-d71dca8184bc', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6851efb6-ced4-43da-b6af-09fdeb45d92e', '8d126f53-e80b-4356-b06c-3eaba0c66c59', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bdc916a4-0597-4ece-b33a-0a26bbcdf7fc', 'd28de887-cf9f-472c-bf80-e799d3a7036b', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d0477fb1-f7b4-41a2-83f4-0b3761958acf', 'debede28-b5e8-4bee-946b-a62bc6cf02d8', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ff3b4087-8396-479c-9cc0-0c25f220b9d7', '44e02da2-f5d6-4547-8b5b-4f8e56e291b0', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ad7cf3cf-4051-4380-a975-0c8950097344', 'e48c5be1-9ffe-496b-93bd-66524d43c14d', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('49f59a01-66c8-4a26-a7ad-0db298ca8933', '307049c0-0dee-4e98-bb6b-611c20670b91', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('dbdc415e-e756-4063-8fa5-0e291b4a1c42', '0d0f164f-d1b7-4036-b791-f41bcefb5f04', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ac3a58cb-3a24-49f1-a548-0e2de80387ed', 'c8813262-e966-4559-a991-55bd10eb176a', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('62efca9a-2c18-43a5-9c49-0e494346f5cb', 'd2e41fbc-4e2f-426f-ae64-cb0da81062e2', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('80e3373e-e963-49d1-ab37-0f526a76605f', '6ab4e507-cb5c-45dd-b9c4-8370881c9359', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('cdf26ba4-28cd-48f1-ad0f-10a98775cfe1', '53dfb90d-677a-47fe-8b9b-1d3fc56b337e', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d5e4730d-151f-4a1c-b50f-117d60d18d05', '53dfb90d-677a-47fe-8b9b-1d3fc56b337e', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0802e018-2265-4c23-9301-12cfdea022ea', 'e8ab4430-3205-4998-bd9a-cf69491638b3', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4add69ae-3aa4-4b63-acac-14459d74bf19', '7e353bcf-5e4b-4139-9634-2b727ae120ba', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4890c84f-b25b-4b2e-9290-14495eb42df8', '524cf43f-e155-4815-8545-00a59150be51', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a25e1c82-fd59-4f9f-964a-14619a42d8d6', 'b6c34904-d85b-45c0-aab9-fa9fdebe0d49', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4da8fb5b-36a6-442b-8130-14df271d5e61', 'b3b0bccd-b1f3-42ed-b230-c9b62af7e8be', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('29fae542-7941-44a3-ac95-15c946b1788a', 'b6c34904-d85b-45c0-aab9-fa9fdebe0d49', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4f3dd4e3-4382-4a4d-b8c2-15fd85ee8193', '321906dc-cf8b-4c96-8941-b48c84b4b357', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4684cdbb-daed-4d09-a4d1-1613b1402d74', 'c4b31ce4-abd9-45f7-844a-140ef7396fee', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('41be0902-33a0-4d74-a4d0-1aad0bdf8724', '307049c0-0dee-4e98-bb6b-611c20670b91', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8a125905-1710-4b2b-af24-1b371b1ed42c', '64dcc499-6f8c-40d6-bbea-8467ee84c64d', N'StatisticsCodeID', N'Mã thống kê', N'', 0, 0, 105, 0, 0, 0, 47)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d80a650b-a567-4ab6-b216-1b7cd1d4268c', '9e823114-3384-4996-8ee9-e942a3f0e930', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ffe39249-3f3f-4f5a-b821-1c2a64d70fa2', '659b432e-8980-4975-b501-999730677599', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8cebe70b-192d-4ed1-a292-1c924137f344', '79d71976-5232-429b-a750-68beab405914', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fa4311b7-71c1-4c5c-94d8-1db350a8aa25', '80ebf9fe-9489-48c9-8a89-e153d4519d40', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('832709e4-fa3b-4189-90fd-1f1eb25b41f5', 'd66b4f70-faab-401a-a2b3-c52bef49aaba', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a6370f07-75be-4d3e-b88a-1f8cd246d082', '01f16c6a-b7a3-4f56-b56b-7e03c6e1df68', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8e23107c-d371-44ee-903e-1ffd8b36e6ed', 'a33105d3-ff8a-4c3f-b83b-4bbd27a73ecf', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9b2dc173-c370-4794-81a9-2001446421ba', '60443339-1f83-4032-abae-202bfe984b75', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b76c5295-91bd-4782-b481-2019eb44c10a', '60f24c74-f4a1-46e9-a5ea-4659cd78ffe4', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0a0ed8c2-f6ef-4920-a120-2064f7e93d17', '57e33a38-5d5b-4d16-a8e9-6fbb0ad46491', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('664fe7f8-2878-4e43-b6b5-2107ea22a732', '868fd85f-ca24-4409-a8f4-ccb7fd952f01', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('aa5f55b6-37ed-491d-ac63-223f6af7915c', '771f6f3e-f9df-44c3-96e0-453c4b849dbe', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('903da45e-5f58-428a-ab21-2449d7dabb2e', '28b2480c-4875-491b-b3ee-b9b1a96494e7', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bc2d7563-b866-42cd-813c-24731c1c5aee', '72c53312-4ee4-45ca-afd2-910ced175c62', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a727931b-58e5-4845-8200-24aeeb3d04d6', 'ad648e0c-02b0-4dbd-a5a5-0df4c72ea399', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e73aa1b1-d1fc-4cea-95ce-24fa670050a4', '220dce55-01ee-4d0d-b78f-1439f0aafd58', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d013fd2e-cda3-4bc0-90f4-2538dea955d5', '85bf8ed9-70a9-409c-b63b-5553b2480888', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6fc3ac71-9683-4362-ac51-253d8e8f94ce', '01f16c6a-b7a3-4f56-b56b-7e03c6e1df68', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d50ad3de-083b-44f3-82c5-25d4310f775b', '7d1e859c-4b35-4a98-8fe9-29f7ac9d0ad6', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2dd486af-e2cc-4a9a-aafa-260780468d36', 'a33105d3-ff8a-4c3f-b83b-4bbd27a73ecf', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('652e3ecf-0287-4d98-a473-276b7a32e988', 'c26f395f-4e5d-4fc6-8389-802dd75b5746', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('36367dc6-097e-47f7-8f5d-277bb77023eb', '4af728c9-c642-413d-9955-df1b7fcf90b5', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('298486c9-7af9-4ca0-b918-27fbb609ed01', '61a3322d-63e8-48d0-aa47-ecd029de2257', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('457b0774-2664-43e9-ba43-28597ed2ec15', '742becbf-87e5-4b93-899f-ed6561ff9ce2', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d98b5693-5c1f-4b4a-91f5-29150f1e998f', 'd2e41fbc-4e2f-426f-ae64-cb0da81062e2', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('61ae42b1-3e92-405b-80b9-29bb408318c2', '0b9df75a-d40c-414e-8483-0e30961b4846', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9058cb4d-1c02-45cc-a75c-2b076b668e96', 'd526ef06-0afd-4579-b737-386bdd0941bd', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('143cdc34-d48e-40a8-a794-2c9292b6ba3a', 'bf31964e-6ca8-4eef-a6dd-de108424ad11', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('cc1eb229-f402-4cc1-b168-2ced4bfb3b2e', 'ad5cac1d-5b8e-42e8-9bd9-8b64019a142a', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9c9366d7-afcf-44b3-981f-2f75ce5aaf56', 'ad648e0c-02b0-4dbd-a5a5-0df4c72ea399', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('668c3d53-2a8f-4d3f-aec6-32d47cb5e43f', '1930711d-1428-4d51-bd27-b0195da69932', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5b2fa02d-8b77-42ca-ae88-345fcc96bf90', '6a4d55aa-8b6d-44d3-abd1-f9c78bb71afa', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2095412f-193d-412b-8acb-34b61abb747f', '8d126f53-e80b-4356-b06c-3eaba0c66c59', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6d15f371-4b2e-4b20-814f-368cea00d559', 'e5f80603-9851-4268-8526-55b36b378df6', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('74e8d606-d278-4ace-bdf8-36b6c42060ff', '01f16c6a-b7a3-4f56-b56b-7e03c6e1df68', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('81a1caca-e350-47c4-89fc-37478d564c64', '68b51815-0fb6-46a5-bbbd-e62957ce64ca', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5d0f56ec-71f7-409d-86db-37f3fa8dc290', '64e1f3db-6a76-4ee0-830e-6e1bb8a26386', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3cfd3ec2-1de7-47f7-8f5d-3816165758d3', '95bc5426-66eb-48c7-ad2d-573b948f2f1d', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b0f9e916-ecd6-424e-a713-39ed4e050738', '9e823114-3384-4996-8ee9-e942a3f0e930', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e237e73c-bf01-4a55-92e7-3a1b6e9a1206', '8b6efee0-76e3-4eaa-95f5-2edb055ad8e8', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d11f70a4-4331-45db-8c9c-3b257fd84731', '091b4939-9eed-4917-a3f5-1c28887f6d27', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('893f7d1b-1dfe-464d-982c-3b832562f013', '7d1e859c-4b35-4a98-8fe9-29f7ac9d0ad6', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d8e7628e-291f-4d01-9290-3b9e6a91f0d6', 'd2e41fbc-4e2f-426f-ae64-cb0da81062e2', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2ec50789-67fb-4b0c-9c01-3c515234c8f0', '868fd85f-ca24-4409-a8f4-ccb7fd952f01', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('77afbe83-0adb-4c61-b213-3ccf8b9433b9', '85bf8ed9-70a9-409c-b63b-5553b2480888', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('69756ea2-4ab2-4ac6-9344-3d6079d48e97', 'd66b4f70-faab-401a-a2b3-c52bef49aaba', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('97dfed3d-11ba-448e-89ae-3f36991c8a3d', 'db65556d-7613-422a-8109-165ce9185b3e', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c41bfbd1-9a96-4f91-b9df-3f629ada1827', 'a34d8517-b960-45e5-9e14-d5bae0c0837b', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d258f003-258f-4ba8-a4d0-3fb532626c08', '57e33a38-5d5b-4d16-a8e9-6fbb0ad46491', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5fb019ce-44f9-47ff-a31c-41918cb49d47', 'c8813262-e966-4559-a991-55bd10eb176a', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1ac31c57-f1b4-44c5-8593-42260ba9089a', 'a34d8517-b960-45e5-9e14-d5bae0c0837b', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('07b995a2-fee9-4d13-8ad9-42f419f0732a', 'a33105d3-ff8a-4c3f-b83b-4bbd27a73ecf', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b687faa5-9217-4f9d-b4a0-435f5541056a', '868fd85f-ca24-4409-a8f4-ccb7fd952f01', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ca7b9232-8c51-4280-8d06-43bdde598d1e', '53dfb90d-677a-47fe-8b9b-1d3fc56b337e', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('73942eaf-84d9-4fbe-8260-43d721bb63bb', '7e353bcf-5e4b-4139-9634-2b727ae120ba', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('152d9f6b-bf7a-41d7-be50-444b56475f8d', '57e33a38-5d5b-4d16-a8e9-6fbb0ad46491', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('26e3f856-76ac-46fd-bad0-45c0137a7a18', 'e8ab4430-3205-4998-bd9a-cf69491638b3', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('343c1ba9-a041-4e00-8e54-4a3f23376153', 'b19ef4f8-0dd0-479f-9f81-da396e394809', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('48aeabde-4ed1-4fbe-81bb-4a5c38619c6b', '2c5ade87-74f1-4e64-bc85-92520729fcf3', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a9f02183-c49b-42e7-9e59-4bf45b77a4ae', '2c5ade87-74f1-4e64-bc85-92520729fcf3', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('70543375-85bd-40cc-8699-4d7fd61c0b73', '64dcc499-6f8c-40d6-bbea-8467ee84c64d', N'CustomProperty3', N'Cột 3', N'', 0, 0, 98, 0, 0, 0, 52)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4182d17d-bc49-4e15-af21-4d88e70dd0c6', 'b3b0bccd-b1f3-42ed-b230-c9b62af7e8be', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0e59eca7-d42d-409b-9644-4dca7d68e4dc', '5ab21184-6f6b-4285-bc9c-fd6ddacc4d3b', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c13bf850-e8e4-4eda-a7f8-4e3078484e2e', 'e48c5be1-9ffe-496b-93bd-66524d43c14d', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c960aec1-7f62-4e9c-8046-4e33812a0181', '524cf43f-e155-4815-8545-00a59150be51', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9e0cc203-76fd-47d7-a826-4e7256124995', 'c4b31ce4-abd9-45f7-844a-140ef7396fee', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('91ae2d6f-85ce-43d5-ac03-4e9be1e299c5', 'ad5cac1d-5b8e-42e8-9bd9-8b64019a142a', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('928e8674-235a-4334-a968-4fd870ec0237', 'a7334d39-5a93-4b8c-b223-4ef6a9cfd895', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('468322bd-b0c4-4b01-8e37-503e0ca29400', '68b51815-0fb6-46a5-bbbd-e62957ce64ca', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6cbf97ae-bcd1-480f-a150-516bbce755b6', 'debede28-b5e8-4bee-946b-a62bc6cf02d8', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('98605c92-3ce9-4da8-92d7-53c22b6ae88c', '910ccbed-14f1-4884-b793-e18d3f4f42c4', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1744b927-39ac-440a-8990-559817b83b40', '8d126f53-e80b-4356-b06c-3eaba0c66c59', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3c8265ea-44d5-4599-b560-55d5db0a1379', '5ab21184-6f6b-4285-bc9c-fd6ddacc4d3b', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f5340233-3235-4321-b511-576892bc5498', '6ab6f01d-395e-4197-86e7-d71dca8184bc', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1c336f91-9d31-4f89-9254-58bd171c242d', '64dcc499-6f8c-40d6-bbea-8467ee84c64d', N'CustomProperty2', N'Cột 2', N'', 0, 0, 98, 0, 0, 0, 51)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ffaa7795-3b76-44d7-964f-5b7ba0e349bd', '0cf6960a-d945-47e1-aebd-ccad29e27d62', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('cda126f3-0cb9-4d3e-8a6d-5bc76acedcd8', '79d71976-5232-429b-a750-68beab405914', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('14bf8f67-6617-4122-9823-5c6718fff446', '61a3322d-63e8-48d0-aa47-ecd029de2257', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('672bc0a8-8152-45a2-8f46-5ce0db51ab2d', '6dcee98d-b159-44df-a3b0-ff96b7b7249e', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('cc1cd8d9-f07c-4fda-b64e-5d4f9065b9ec', '64dcc499-6f8c-40d6-bbea-8467ee84c64d', N'CustomProperty1', N'Cột 1', N'', 0, 0, 98, 0, 0, 0, 49)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6ce40e57-18d9-4e8e-8109-60569ff6c01c', '57e33a38-5d5b-4d16-a8e9-6fbb0ad46491', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bb718386-45ed-42f2-9ddb-60a378fccb61', 'b6f0d579-beb5-43fc-8968-c26fcdd4eb25', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b2c509fd-20f6-4bc6-8e4b-60fcfd26bf20', 'a70476c8-5dfe-44c1-b208-a08b58321329', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c0295430-93ec-4bdd-b7f0-613d08a90a33', 'a70476c8-5dfe-44c1-b208-a08b58321329', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1fe7e106-1a97-4201-b1e1-620ed54c4db9', 'a7334d39-5a93-4b8c-b223-4ef6a9cfd895', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('84f40b94-9606-4c41-8305-628b12f0c580', '307049c0-0dee-4e98-bb6b-611c20670b91', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8b2330a2-9032-48fd-9188-62aafe4514b8', '64e1f3db-6a76-4ee0-830e-6e1bb8a26386', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('18d1b442-7b92-4dce-9fcf-62ba9da5f360', '68b51815-0fb6-46a5-bbbd-e62957ce64ca', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e2bf7e4b-8852-4d28-8893-62de476cac4b', 'd28de887-cf9f-472c-bf80-e799d3a7036b', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d428824d-5101-4564-8bb1-6495d50623a5', '1307c619-ac47-491a-8ad4-21422beeceaf', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e3732073-9097-4313-ac31-6656b5aa4e97', '0cf6960a-d945-47e1-aebd-ccad29e27d62', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7ad3da8f-ee13-40e6-a6f3-668a28365c6c', '900479e1-85c5-40ec-afed-692663b05348', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8f2fa2b1-8b60-4256-aca1-66a06b6f085f', 'c3e16024-9e7f-4743-9dcf-be8d02900206', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('efd43b66-c2fa-4ca4-8c0a-6798147f6eca', '8ffcf5ee-cf71-42e0-a45b-545eeaa0533e', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0c81973b-6be0-41ea-9061-68d3dc1c80ac', '0cf6960a-d945-47e1-aebd-ccad29e27d62', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9dd32e41-13f8-4f2b-91c6-6940e43c5579', '091b4939-9eed-4917-a3f5-1c28887f6d27', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b4e9e4b2-a333-4538-946c-6a49de4fbcd2', '44e02da2-f5d6-4547-8b5b-4f8e56e291b0', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('40d4e882-e37c-47a6-8431-6c884ed02642', '7c7f8e36-b3a5-4e3c-987c-1619fbc8fb79', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5290e9bc-3fa1-4e18-bafc-6cb9d2faf2a2', '1307b135-0daa-4545-9854-23ca4c9da8d8', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6f1278a1-c44c-44d7-8565-6e46b874dc80', 'f8b946d8-0078-47fd-a4f5-562498692501', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('05969c5d-4510-4941-b3ba-6e530286e0e4', 'ad5cac1d-5b8e-42e8-9bd9-8b64019a142a', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b5e4faf6-c78b-4558-800e-6e92454ae7b3', '3b9f7c93-c839-4972-a45c-4ef2b2da5cf3', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b72884b9-5075-4a7e-a6d4-6ea6fe78397f', 'b3b0bccd-b1f3-42ed-b230-c9b62af7e8be', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b96f1767-d3c0-4f89-ac0e-6fcb82ca63c7', '524cf43f-e155-4815-8545-00a59150be51', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7bdb5f14-0133-4069-84f6-711051773492', '0d0f164f-d1b7-4036-b791-f41bcefb5f04', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4128bed0-9de7-4295-9020-7118c9111437', '1c42b76a-7e1d-4a14-87f3-8bc3e0269de7', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5e37dfaa-956d-4e03-b9f4-720fefe1eb7c', '091b4939-9eed-4917-a3f5-1c28887f6d27', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8f8e2503-91cd-4178-9e06-7373f0a001a6', '28b2480c-4875-491b-b3ee-b9b1a96494e7', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('57fc9ab3-947c-413b-81ca-747e8f83ce56', 'c93bc860-4663-4e8c-a0c9-abfed6e7fa81', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c3ecf3b3-4304-48ac-8891-75028acc23d1', 'c4b31ce4-abd9-45f7-844a-140ef7396fee', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e3205507-6987-4643-bf1f-75ffd240db3d', 'bf31964e-6ca8-4eef-a6dd-de108424ad11', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('47e9e981-d143-4d66-b721-763a209e4582', '95bc5426-66eb-48c7-ad2d-573b948f2f1d', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('be2a22dd-5d0e-43b5-9117-7790b541ff1c', '9e823114-3384-4996-8ee9-e942a3f0e930', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2676aa30-c028-4b8e-95f6-784b1fa78f14', 'e48c5be1-9ffe-496b-93bd-66524d43c14d', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6af58a63-6bea-4fde-b098-785182ca946a', '80ebf9fe-9489-48c9-8a89-e153d4519d40', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5493dda3-f900-42ff-8067-78a3ce783b3e', 'b6f0d579-beb5-43fc-8968-c26fcdd4eb25', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('340b2b51-9c3b-4349-9a41-7941a9b98dfe', 'c26f395f-4e5d-4fc6-8389-802dd75b5746', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('12ef6ed0-d3e0-4fbd-b68b-796bdd220511', 'c4b31ce4-abd9-45f7-844a-140ef7396fee', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2bd53789-8cb6-47a4-a8de-7dcc1f2a0482', '64e1f3db-6a76-4ee0-830e-6e1bb8a26386', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('92299a79-f271-417e-a258-7dff099b09ed', 'c26f395f-4e5d-4fc6-8389-802dd75b5746', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('cf740cf2-4397-4af7-9be2-7e32491b713d', 'a70476c8-5dfe-44c1-b208-a08b58321329', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f21e8911-53cc-4cc7-a41f-7eb39b5c8867', '64dcc499-6f8c-40d6-bbea-8467ee84c64d', N'CostAccount', N'TK giá vốn', N'Tài khoản giá vốn', 0, 0, 95, 0, 0, 1, 53)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('861086d5-b778-4c5c-b096-7ee67858e578', 'e8ab4430-3205-4998-bd9a-cf69491638b3', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('37888755-b4a7-43aa-bec1-7f79b7d30252', 'eab768db-a758-4b31-b168-ae956b220a49', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('00b52291-e4a9-4dc4-ba15-8247225d4bbd', 'e5f80603-9851-4268-8526-55b36b378df6', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('df37fe1b-d654-4e76-a2c5-82d54f8d4958', '6ab6f01d-395e-4197-86e7-d71dca8184bc', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a082fd45-97a7-4553-a6cb-8312b3bfe4ec', '742becbf-87e5-4b93-899f-ed6561ff9ce2', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5b218da1-f1c7-4f81-8d3d-8346aa3de0b2', 'eab768db-a758-4b31-b168-ae956b220a49', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('094a75cb-b454-43f7-b2b9-83508ff2984c', '321906dc-cf8b-4c96-8941-b48c84b4b357', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f136ffab-545a-4916-8212-84172bca3939', '85bf8ed9-70a9-409c-b63b-5553b2480888', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('69bb16a5-eead-4929-aaf3-865353473de3', 'ad648e0c-02b0-4dbd-a5a5-0df4c72ea399', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a74d55ce-274e-49c3-81fb-86cb373341c8', '127a2513-754b-4b4e-b667-5689faec1df6', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b2454890-99d7-4afa-afea-89119c2ce6ad', '7d1e859c-4b35-4a98-8fe9-29f7ac9d0ad6', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('56788be2-f078-4bd3-b9f0-8abbffe616f9', 'd28de887-cf9f-472c-bf80-e799d3a7036b', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('201f58c2-957a-439b-be33-8c6d1166361f', '4af728c9-c642-413d-9955-df1b7fcf90b5', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3ac18529-e64f-4a57-b915-8d97d12e0b45', '8b6efee0-76e3-4eaa-95f5-2edb055ad8e8', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('653a25eb-8b44-4387-b502-8db1a19746d6', 'b6f0d579-beb5-43fc-8968-c26fcdd4eb25', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('dfa3adda-eb61-4aed-b746-8f6acc5c3eba', 'b6f0d579-beb5-43fc-8968-c26fcdd4eb25', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7ef70a73-e39d-4ce3-8540-8fb758c51cca', '900479e1-85c5-40ec-afed-692663b05348', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fd8ab9b4-87e0-48bb-8c93-8fd1cf722132', '8ffcf5ee-cf71-42e0-a45b-545eeaa0533e', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('947c3d36-5ac5-4d04-868c-9100b324938e', '95812b6b-07aa-4140-8ec6-37f613a274c4', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f618c241-ddb4-4a8c-a335-926229cbacc5', '127a2513-754b-4b4e-b667-5689faec1df6', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('437c4035-a349-48f5-b308-932d91b1d1b7', '220dce55-01ee-4d0d-b78f-1439f0aafd58', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2cb8f5be-776f-4b87-8ec5-9607ce738d0d', '6e71c00c-9868-4e07-b5d6-92e08a0d3a24', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d6665fdb-0ac9-416f-892e-970d4b799c24', '72c53312-4ee4-45ca-afd2-910ced175c62', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a58903ea-3f78-40da-a252-97bb3394087c', 'a70476c8-5dfe-44c1-b208-a08b58321329', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9ed139e3-372b-4c72-bde4-984e227eeb69', '61a3322d-63e8-48d0-aa47-ecd029de2257', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a963c395-de89-4fc4-a035-9946770428c9', 'b2ec0598-91be-427d-99e6-b7aa3bde5fec', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2354a68c-fcda-4569-833c-9a2d23c2f29e', 'd28de887-cf9f-472c-bf80-e799d3a7036b', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9515e3af-c6e3-4733-9362-9ad41a9b9917', 'a7334d39-5a93-4b8c-b223-4ef6a9cfd895', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a44b60ee-5907-4b61-bcbe-9b2bf27a2e11', 'db65556d-7613-422a-8109-165ce9185b3e', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9cc960a6-9b62-431d-a15c-9c446e338ca4', 'fd6cf906-6c8b-4a0f-bec0-f374e4aa09a2', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('90833f78-13f5-476d-9fca-9ddf661746a5', 'c8813262-e966-4559-a991-55bd10eb176a', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c5e3172a-ef37-4e10-85fa-9fb30424dacf', 'e8ab4430-3205-4998-bd9a-cf69491638b3', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0d10bfcb-11af-4f4a-a4be-a05b3a98179b', '79d71976-5232-429b-a750-68beab405914', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6705ec72-2a3b-43e4-9499-a0da7b491785', '72c53312-4ee4-45ca-afd2-910ced175c62', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bff8980c-1840-40fe-adf8-a11e5ffc9f31', '60443339-1f83-4032-abae-202bfe984b75', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bbe64d0a-ed82-439e-bb32-a1db5e260f34', 'a34d8517-b960-45e5-9e14-d5bae0c0837b', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a352073a-f2d2-4aa3-b9b9-a239c8d13c7e', '95812b6b-07aa-4140-8ec6-37f613a274c4', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e3ff4c27-c017-446a-9edb-a2bb9fdef544', 'c8813262-e966-4559-a991-55bd10eb176a', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ebde1185-544d-478e-a3ff-a3336da7a2fa', '60f24c74-f4a1-46e9-a5ea-4659cd78ffe4', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('59e20ce3-6b84-449d-88eb-a38b90cb74d7', 'e48c5be1-9ffe-496b-93bd-66524d43c14d', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 0, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8dc19930-aec8-4d82-bd9e-a40e9941762f', '1930711d-1428-4d51-bd27-b0195da69932', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8bc1554c-a5b1-46b0-95aa-a57ee42bcdf5', '868fd85f-ca24-4409-a8f4-ccb7fd952f01', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f4bdafa8-0f10-45ed-b01d-a632791a9d9a', 'b19ef4f8-0dd0-479f-9f81-da396e394809', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8c599b78-abae-4c7e-ac4f-a6ec5c7a5803', 'c3e16024-9e7f-4743-9dcf-be8d02900206', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7156a7f6-813e-4e05-99c6-a72691b4c4c0', 'bf31964e-6ca8-4eef-a6dd-de108424ad11', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('da6412b0-3ae1-4d15-b09f-a88e99391291', '1930711d-1428-4d51-bd27-b0195da69932', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('587952c3-bef4-456b-9a8b-a8a6308099b7', 'a33105d3-ff8a-4c3f-b83b-4bbd27a73ecf', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c549a582-300d-463d-aba2-a97089e4f478', 'a7334d39-5a93-4b8c-b223-4ef6a9cfd895', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6458246d-e915-4d64-8b13-a9f274f70b74', '900479e1-85c5-40ec-afed-692663b05348', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('29341e09-5818-4642-9f21-aa36358da17b', '6e71c00c-9868-4e07-b5d6-92e08a0d3a24', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('034ce2c7-6cf4-4c1f-bdba-aa4eb6932983', 'f8b946d8-0078-47fd-a4f5-562498692501', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('75eccc95-bf09-4a20-9619-aa609ff3299c', 'd66b4f70-faab-401a-a2b3-c52bef49aaba', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('af2c636f-c76d-4b9d-a290-ab37d7f0be45', 'c93bc860-4663-4e8c-a0c9-abfed6e7fa81', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fe8138b5-4c19-4f58-8f41-ad3c48fae17b', '80ebf9fe-9489-48c9-8a89-e153d4519d40', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('cd544513-e708-4d86-b0f9-adbfbb0a5171', '524cf43f-e155-4815-8545-00a59150be51', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2f77347f-4690-491e-9e01-ade5b2c9f0ff', '8b6efee0-76e3-4eaa-95f5-2edb055ad8e8', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('32810956-3a66-4d42-839c-ae7948a689a6', '85bf8ed9-70a9-409c-b63b-5553b2480888', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ac56c4cb-2c26-453d-8f9d-aea1a9c5f80c', '4af728c9-c642-413d-9955-df1b7fcf90b5', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('dfab5d24-c26a-48c4-8111-af662c34555d', '79d71976-5232-429b-a750-68beab405914', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('569c9f4a-3458-4f7c-8ee8-af9cceab7230', '7c7f8e36-b3a5-4e3c-987c-1619fbc8fb79', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9967c4e3-9e40-4542-9f60-b039393e8ace', '64e1f3db-6a76-4ee0-830e-6e1bb8a26386', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8681027a-c9b0-46c6-9d23-b08b15411780', '0b9df75a-d40c-414e-8483-0e30961b4846', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bfccb938-76ee-4378-a664-b0a496aae551', '53dfb90d-677a-47fe-8b9b-1d3fc56b337e', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e8d021fc-19af-4ccf-8253-b0cd680f8112', '1c42b76a-7e1d-4a14-87f3-8bc3e0269de7', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8cfca8f8-c5cd-4e8f-831f-b278883b5900', '95812b6b-07aa-4140-8ec6-37f613a274c4', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b0e7a738-1e70-49a3-8648-b50383edd3e8', '1c42b76a-7e1d-4a14-87f3-8bc3e0269de7', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('82f2b208-662f-48d5-8831-b597d484ce0d', '3b9f7c93-c839-4972-a45c-4ef2b2da5cf3', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f8ad7bdd-9804-4328-babd-b6b8009c42c5', 'd2e41fbc-4e2f-426f-ae64-cb0da81062e2', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c3dd7314-940e-4e4a-8314-b8cfa6035f9d', '1307b135-0daa-4545-9854-23ca4c9da8d8', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d5c684d4-2407-4f1c-a4f9-b94aa42acebc', 'ad5cac1d-5b8e-42e8-9bd9-8b64019a142a', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7f7186a9-1e52-41ec-a587-b97380c04dad', 'c26f395f-4e5d-4fc6-8389-802dd75b5746', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1d625c16-2d3e-4b67-b991-ba1cb1532573', '6e71c00c-9868-4e07-b5d6-92e08a0d3a24', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('13fd80ef-3a05-4a6f-99fb-ba4fe7ce98aa', 'debede28-b5e8-4bee-946b-a62bc6cf02d8', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c8a4cfc9-6a00-4eab-a4fd-bad48c08226e', 'd526ef06-0afd-4579-b737-386bdd0941bd', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('38419242-ad66-41f1-8700-bc9cbdf89da7', '6ab4e507-cb5c-45dd-b9c4-8370881c9359', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('817971ea-eb69-43e7-91f1-bcec53c6c0b7', 'a67e0254-75ae-4710-bba6-2030945de07e', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fbfddac9-ae58-4e88-9dbe-bd5753ff866e', '321906dc-cf8b-4c96-8941-b48c84b4b357', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ccbf266c-7149-4a49-a67f-be0daa792908', 'fd6cf906-6c8b-4a0f-bec0-f374e4aa09a2', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('94b7ce2a-5c58-4b95-b948-bfd40e71e4b5', 'eab768db-a758-4b31-b168-ae956b220a49', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2032434a-6da2-4409-a402-c16260ae546f', 'db65556d-7613-422a-8109-165ce9185b3e', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9d5894e7-2c93-4e14-bf75-c28472a71fb6', 'a34d8517-b960-45e5-9e14-d5bae0c0837b', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c84c3c7b-c277-4162-8c79-c2aeaef465dd', 'ad648e0c-02b0-4dbd-a5a5-0df4c72ea399', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6dc8f7f9-75b1-40dd-b18c-c387bcec144d', '0cf6960a-d945-47e1-aebd-ccad29e27d62', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('74c5b3c9-3bb9-443c-88fa-c3c88ea70fbb', '8ffcf5ee-cf71-42e0-a45b-545eeaa0533e', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a9cbfef1-34e2-4876-b7cf-c5d3df42c323', '1307c619-ac47-491a-8ad4-21422beeceaf', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5d3ede53-d7e7-4d4a-85a5-c8de8e5e9d0c', 'd526ef06-0afd-4579-b737-386bdd0941bd', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c2c5c067-272c-450e-948a-ca3892d873ec', '6e71c00c-9868-4e07-b5d6-92e08a0d3a24', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('35e06c02-81bd-4800-bb8b-caecc3d70148', '28b2480c-4875-491b-b3ee-b9b1a96494e7', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b0da1d58-331b-4404-b255-cb901adb6ef2', '1307c619-ac47-491a-8ad4-21422beeceaf', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d36e6ae4-74ea-45f0-bb6f-cbb1733d46e3', 'f8b946d8-0078-47fd-a4f5-562498692501', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('dbcde511-1938-4b4b-9376-cc32bb3fd371', 'c93bc860-4663-4e8c-a0c9-abfed6e7fa81', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d7deb7eb-cbb4-415f-9a7f-cc7982409e01', 'debede28-b5e8-4bee-946b-a62bc6cf02d8', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ead081f7-5a6d-40a9-a086-cda9bb9a31ed', '95bc5426-66eb-48c7-ad2d-573b948f2f1d', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('59ac9de5-2fa7-480e-a9b7-cedd3ee53495', '7c7f8e36-b3a5-4e3c-987c-1619fbc8fb79', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9b58f952-ee32-4c6a-b84d-cf4db79e1486', '95812b6b-07aa-4140-8ec6-37f613a274c4', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('23108d18-fad9-4391-91d5-cf5b4b32a339', '659b432e-8980-4975-b501-999730677599', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('34ce3e0e-60d8-49fd-9d52-cfae93174dad', '60443339-1f83-4032-abae-202bfe984b75', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('191a8c3c-0f35-490b-86d1-d002bd685ded', '6a4d55aa-8b6d-44d3-abd1-f9c78bb71afa', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('dc5f2a05-bf88-421a-b9c7-d02b38233dd0', '7e353bcf-5e4b-4139-9634-2b727ae120ba', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4106dd3d-e3fd-447d-b7d1-d32654166902', 'db65556d-7613-422a-8109-165ce9185b3e', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('54b4429e-dcb5-4c86-a0ab-d36f2961b44e', '5ab21184-6f6b-4285-bc9c-fd6ddacc4d3b', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('faaa4258-05a2-42b1-ab3d-d3ba2cdba017', '091b4939-9eed-4917-a3f5-1c28887f6d27', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d0fba7d8-dfa5-4efb-883f-d63da4df4ede', '61a3322d-63e8-48d0-aa47-ecd029de2257', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('43abd25d-0db6-4f62-a4a5-d7e43b7ab106', 'a67e0254-75ae-4710-bba6-2030945de07e', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9a560359-131a-4bb8-84c2-d9a89f2a31d1', '7e353bcf-5e4b-4139-9634-2b727ae120ba', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('727888bb-6bf5-4061-b715-da1c78fa581b', 'b2ec0598-91be-427d-99e6-b7aa3bde5fec', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2d532023-956b-4364-8086-da2c3c6e1e9f', '5ab21184-6f6b-4285-bc9c-fd6ddacc4d3b', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7a23cb31-0305-4434-b4f4-da80c6d8fc0a', '0d0f164f-d1b7-4036-b791-f41bcefb5f04', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('df3e84c6-a886-481c-83b8-dbb6e9e11a90', '6ab4e507-cb5c-45dd-b9c4-8370881c9359', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7c25fc32-8fbb-4e14-b1b8-dbccd3ac65f2', '3b9f7c93-c839-4972-a45c-4ef2b2da5cf3', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3a59cbfe-bdc3-4afe-89ab-dbd4d6664c87', '7d1e859c-4b35-4a98-8fe9-29f7ac9d0ad6', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ee7f2ef3-f914-40aa-8e9e-dc0300bb2484', '0b9df75a-d40c-414e-8483-0e30961b4846', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2ea97e15-9253-4cf5-b41b-dcd4661c03f9', '742becbf-87e5-4b93-899f-ed6561ff9ce2', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6c7b9536-18ce-420c-a56c-dcff0fea7989', '2c5ade87-74f1-4e64-bc85-92520729fcf3', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e5f62741-89f8-4275-8a62-dd5d4bda238f', '6dcee98d-b159-44df-a3b0-ff96b7b7249e', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ad991f32-b460-49d4-a9de-de19c913bc90', '68b51815-0fb6-46a5-bbbd-e62957ce64ca', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7f19456b-0119-4f5e-bfe1-df8e2e9501bb', 'b2ec0598-91be-427d-99e6-b7aa3bde5fec', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b1338dcb-8615-49eb-a064-e0190098bfc4', '659b432e-8980-4975-b501-999730677599', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('791ca4be-5deb-4d90-94ce-e1089d209af5', '659b432e-8980-4975-b501-999730677599', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('111d185d-8277-421a-a005-e2f1b9d7dd87', '910ccbed-14f1-4884-b793-e18d3f4f42c4', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d82055cf-43b7-4222-a207-e3025257038f', '771f6f3e-f9df-44c3-96e0-453c4b849dbe', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('326f0ee6-508f-48bf-b81d-e326990c753c', '72c53312-4ee4-45ca-afd2-910ced175c62', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ff78fcda-f958-4872-a32a-e3aa7763d737', 'a67e0254-75ae-4710-bba6-2030945de07e', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1d5cca5c-e212-443a-8f2c-e434ffacc12e', '60f24c74-f4a1-46e9-a5ea-4659cd78ffe4', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('99d8a0d0-3bf9-4a55-ab2b-e43b2babace8', 'b3b0bccd-b1f3-42ed-b230-c9b62af7e8be', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6ec63054-3c20-420a-968e-e4722e2bddca', '220dce55-01ee-4d0d-b78f-1439f0aafd58', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('77c67bf9-5844-489a-ad49-e47a6d7d4199', 'bf31964e-6ca8-4eef-a6dd-de108424ad11', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('29a7fc03-0ea6-4c7a-9cbf-e60ab0f4173b', '1c42b76a-7e1d-4a14-87f3-8bc3e0269de7', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('da9f8717-5dd7-44ea-b1e6-e723ac10bd65', '1307c619-ac47-491a-8ad4-21422beeceaf', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b2706c9b-3aab-4fe0-b198-e72ab8c59816', '21bf5f6f-da51-47d1-9581-071ed46e2ff2', N'VATAccount', N'TK thuế GTGT', NULL, 0, 0, 100, 0, 0, 1, 16)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('7ac0cd3d-0591-4cab-b706-e7a6e75c1631', '307049c0-0dee-4e98-bb6b-611c20670b91', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a813f580-1ddc-455c-9ed3-e7afbb391370', '8d126f53-e80b-4356-b06c-3eaba0c66c59', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0b264373-1137-4fad-84ff-e880d3bfd452', '6a4d55aa-8b6d-44d3-abd1-f9c78bb71afa', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('301265d2-79ad-4b09-a4ba-e8a775e78ecf', 'fd6cf906-6c8b-4a0f-bec0-f374e4aa09a2', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5af260be-8e92-42eb-b787-e930fc0c1708', '1307b135-0daa-4545-9854-23ca4c9da8d8', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f6707baf-b557-4cb1-b2d2-e9e1e37a0e52', 'e5f80603-9851-4268-8526-55b36b378df6', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b768bfc3-9b9b-4ede-b28d-eb369721df65', 'b19ef4f8-0dd0-479f-9f81-da396e394809', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ac9667e8-d428-4de8-a9a1-ebe5ef164dfb', 'b2ec0598-91be-427d-99e6-b7aa3bde5fec', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0cb6e53a-cc2b-4df2-b471-ec511b0246e6', '1930711d-1428-4d51-bd27-b0195da69932', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1c1d1a15-8463-48c1-a1e4-ec73aa962dc9', '8ffcf5ee-cf71-42e0-a45b-545eeaa0533e', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('96e5033b-2833-4062-b8a8-eca0471f7227', '64dcc499-6f8c-40d6-bbea-8467ee84c64d', N'CostSetID', N'ĐT tập hợp CP', N'Đối tượng tập hợp chi phí', 0, 0, 150, 0, 0, 0, 46)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d12a546c-741b-40ed-b1d9-ed807dcecd84', '6dcee98d-b159-44df-a3b0-ff96b7b7249e', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('fa886121-f586-4ea7-bf52-ee004c4a3bca', '60443339-1f83-4032-abae-202bfe984b75', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('690e07c3-e742-4fc6-92c3-ee0a97dac65e', '127a2513-754b-4b4e-b667-5689faec1df6', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9054b638-9988-4c6a-ba14-ee13f128b66c', '28b2480c-4875-491b-b3ee-b9b1a96494e7', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('091c2a06-b258-4752-a1d4-ef4c25f0aba5', '771f6f3e-f9df-44c3-96e0-453c4b849dbe', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b21cf324-4925-49d7-9368-ef5303f7cf4b', '4af728c9-c642-413d-9955-df1b7fcf90b5', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('71fce5b1-d268-44b4-846d-f01c51486c48', '1307b135-0daa-4545-9854-23ca4c9da8d8', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('14906112-f347-4067-b0ad-f1047e855818', 'd66b4f70-faab-401a-a2b3-c52bef49aaba', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('65c1078d-ba8d-4c07-bc91-f10b46b8d671', 'b6c34904-d85b-45c0-aab9-fa9fdebe0d49', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('06513697-7a8f-409c-ac76-f186e0f2083d', '321906dc-cf8b-4c96-8941-b48c84b4b357', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('11def719-3f8a-431a-856d-f19a728c33a3', '910ccbed-14f1-4884-b793-e18d3f4f42c4', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('22958800-dc8a-4e49-9932-f1fecbff4112', 'c3e16024-9e7f-4743-9dcf-be8d02900206', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b3658b10-b3e7-40ce-ab90-f2a0576c6ce4', '3b9f7c93-c839-4972-a45c-4ef2b2da5cf3', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('323a695b-3905-4770-849f-f2b626a4f33b', '2c5ade87-74f1-4e64-bc85-92520729fcf3', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('35253116-ea9e-4846-af47-f33312d40a0e', '8b6efee0-76e3-4eaa-95f5-2edb055ad8e8', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f6020767-d0c4-40f3-8961-f3d0a5caa1f0', '6ab6f01d-395e-4197-86e7-d71dca8184bc', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('de7112c3-432e-41b5-9ae5-f4221f04b2a5', '01f16c6a-b7a3-4f56-b56b-7e03c6e1df68', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('64b79cdd-e0b6-41c7-ab85-f580cf557865', '7c7f8e36-b3a5-4e3c-987c-1619fbc8fb79', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('94691ee3-56d3-4b5d-a7ec-f5d8032a6173', 'c93bc860-4663-4e8c-a0c9-abfed6e7fa81', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ed39e420-9c5a-4ca9-ba95-f626eae5fc7d', 'b6c34904-d85b-45c0-aab9-fa9fdebe0d49', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8efc1948-7ee9-42c1-a1d9-f6feb9cd5021', '6ab4e507-cb5c-45dd-b9c4-8370881c9359', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0102f57a-6cc7-478e-a714-f709abb6e389', '64dcc499-6f8c-40d6-bbea-8467ee84c64d', N'RepositoryID', N'Kho', N'', 0, 0, 267, 0, 0, 0, 50)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4532bee6-b33c-46f3-8586-f789593238ec', '44e02da2-f5d6-4547-8b5b-4f8e56e291b0', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a5501f9e-0220-42f1-9b28-f7f2b0905e74', '44e02da2-f5d6-4547-8b5b-4f8e56e291b0', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e42504aa-1e8a-4c85-b02d-f81c217446fd', 'c3e16024-9e7f-4743-9dcf-be8d02900206', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('13e96dad-f5a0-41c9-8ac6-fa4bd88e8740', 'a67e0254-75ae-4710-bba6-2030945de07e', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('889eaea2-bd5a-4a85-8f29-fb24182196da', '0d0f164f-d1b7-4036-b791-f41bcefb5f04', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9ea02fc0-b919-4e27-83cb-fbbea59e6114', '771f6f3e-f9df-44c3-96e0-453c4b849dbe', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ed2b02ac-0b10-46a7-b851-fc83b34db51f', '6a4d55aa-8b6d-44d3-abd1-f9c78bb71afa', N'No', N'Số chứng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6f408c3c-4ccc-4e33-b928-fc8587adb46f', 'e5f80603-9851-4268-8526-55b36b378df6', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.TemplateDetail SET TabCaption = N'&3. Chứng từ tham chiếu' WHERE ID = '9ddc5656-3c46-4ff0-a4ce-028749f7dd06'
UPDATE dbo.TemplateDetail SET TabCaption = N'&1. Hạch toán' WHERE ID = 'b31e485e-d51b-4a7a-b9b6-66e691ecdab0'
UPDATE dbo.TemplateDetail SET TabCaption = N'&1. Chi tiết' WHERE ID = '3f57544c-38bd-460a-9c52-cf95a324ae91'
UPDATE dbo.TemplateDetail SET TabCaption = N'&2. Thống kê' WHERE ID = '572cd728-9e62-47c0-9b5c-fac093ce0115'

INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('524cf43f-e155-4815-8545-00a59150be51', 'a00603f1-6826-49ac-9b47-6539c56f091e', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('ad648e0c-02b0-4dbd-a5a5-0df4c72ea399', 'f5d9e415-7101-44b1-a431-fb6a8ee33896', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('0b9df75a-d40c-414e-8483-0e30961b4846', '81eed06e-67cf-4ea8-9699-32397d694084', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('c4b31ce4-abd9-45f7-844a-140ef7396fee', 'a5c5762b-246e-47fe-8f2c-bcb09f0dbd31', 1, 2, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('220dce55-01ee-4d0d-b78f-1439f0aafd58', 'c89c04ee-8ee8-4c2d-8f8e-2499d9197cc9', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('7c7f8e36-b3a5-4e3c-987c-1619fbc8fb79', '72aef97d-7513-4d14-b846-e353226b5ff0', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('db65556d-7613-422a-8109-165ce9185b3e', '335f1c3e-62ab-4403-99b6-4f2b1e9a5e38', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('091b4939-9eed-4917-a3f5-1c28887f6d27', '8b1d6b9c-31bb-4d77-b9c2-c3d93836689c', 2, 4, N'&5. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('53dfb90d-677a-47fe-8b9b-1d3fc56b337e', '8ddd15b9-8c3e-44db-9d0d-abebf31c7fb2', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('60443339-1f83-4032-abae-202bfe984b75', 'e1adad03-54cb-419c-8cd0-69c70198d793', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('a67e0254-75ae-4710-bba6-2030945de07e', 'ff079521-a8db-46b9-a6db-67d8dea4f608', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('1307c619-ac47-491a-8ad4-21422beeceaf', '5e07088d-d833-43c6-bde5-399a77112204', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('1307b135-0daa-4545-9854-23ca4c9da8d8', 'd55b8be3-1b7f-447c-9e88-8ac6abed9dde', 2, 1, N'&2. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('7d1e859c-4b35-4a98-8fe9-29f7ac9d0ad6', 'b5b01477-1001-40de-980e-616d5a6a2d70', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('7e353bcf-5e4b-4139-9634-2b727ae120ba', '0bcff3c0-ee42-412b-9a47-08db6db262bf', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('8b6efee0-76e3-4eaa-95f5-2edb055ad8e8', 'deee0050-e8fe-458e-9894-fe3abf34ce39', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('95812b6b-07aa-4140-8ec6-37f613a274c4', 'c3d4814f-118a-4b42-8bb6-36cc884955bc', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('d526ef06-0afd-4579-b737-386bdd0941bd', '446f2005-6346-4e68-9399-bd3a35308285', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('8d126f53-e80b-4356-b06c-3eaba0c66c59', '4911dbad-677b-4475-89cd-d79f2a7ed889', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('771f6f3e-f9df-44c3-96e0-453c4b849dbe', '2df8175e-428f-4285-a84d-815e7697318e', 2, 4, N'&5. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('60f24c74-f4a1-46e9-a5ea-4659cd78ffe4', '92fc8d27-4eab-458c-81c7-5c469d2c945a', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('a33105d3-ff8a-4c3f-b83b-4bbd27a73ecf', 'dcc9e96c-79c5-4b48-9d1e-4de58e976eed', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('3b9f7c93-c839-4972-a45c-4ef2b2da5cf3', 'd1f2d23e-792b-4697-b426-e4f816fd3935', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('a7334d39-5a93-4b8c-b223-4ef6a9cfd895', 'c99a31df-0a06-423b-8bb0-3caea036f13f', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('44e02da2-f5d6-4547-8b5b-4f8e56e291b0', '1b225244-a667-406d-9f09-5902f16a1ebd', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('8ffcf5ee-cf71-42e0-a45b-545eeaa0533e', 'c4001494-465c-4fff-bc03-4c9d42f126ea', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('85bf8ed9-70a9-409c-b63b-5553b2480888', '074ec166-c447-4aa2-8804-408b7f57530f', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('e5f80603-9851-4268-8526-55b36b378df6', '9726eff6-5d74-4554-bc83-269bf1a0d19c', 2, 1, N'&2. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('c8813262-e966-4559-a991-55bd10eb176a', 'abb83c5a-1d9e-4562-a54a-a4d4229827f8', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('f8b946d8-0078-47fd-a4f5-562498692501', '5361605c-fe60-487a-8f96-46c64e59db7f', 2, 4, N'&5. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('127a2513-754b-4b4e-b667-5689faec1df6', '9b37cc40-bb2b-4942-854b-c4d91a5bebb3', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('95bc5426-66eb-48c7-ad2d-573b948f2f1d', '41b04923-5d42-4a5a-8aef-9c300f184475', 2, 1, N'&2. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('307049c0-0dee-4e98-bb6b-611c20670b91', 'd6789f03-0660-44eb-a02e-1bf7d37cb703', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('e48c5be1-9ffe-496b-93bd-66524d43c14d', '27c4df34-0246-4067-b64a-dc1dab099f80', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('79d71976-5232-429b-a750-68beab405914', '768f630d-fb71-403c-8bf4-93b42af1adba', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('900479e1-85c5-40ec-afed-692663b05348', 'c0ed51a5-3b7a-4966-989e-a5b5887eac52', 2, 1, N'&2. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('64e1f3db-6a76-4ee0-830e-6e1bb8a26386', '31a94605-c906-474c-a491-43bd24201ab4', 2, 1, N'&2. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('57e33a38-5d5b-4d16-a8e9-6fbb0ad46491', 'c750918a-aa86-4643-a44f-2fd95cda4185', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('01f16c6a-b7a3-4f56-b56b-7e03c6e1df68', 'dd03d8a0-577a-42ba-8cf2-7021684659ff', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('c26f395f-4e5d-4fc6-8389-802dd75b5746', '5badd60b-9f88-4dc9-b630-df3d71bacc34', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('6ab4e507-cb5c-45dd-b9c4-8370881c9359', '7fd7ae11-8bf2-4fe2-853b-978c2bd402ae', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('ad5cac1d-5b8e-42e8-9bd9-8b64019a142a', '4a29c846-3f64-44a5-b8ef-7602e5a76192', 2, 1, N'&2. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('1c42b76a-7e1d-4a14-87f3-8bc3e0269de7', '11f38291-8778-4c22-a1d3-8ef328860d71', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('72c53312-4ee4-45ca-afd2-910ced175c62', '05c5fbf6-7236-424d-80f9-8abd56bf5e5f', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('2c5ade87-74f1-4e64-bc85-92520729fcf3', 'c939a06d-b632-4568-831d-c2cec630ba70', 2, 1, N'&2. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('6e71c00c-9868-4e07-b5d6-92e08a0d3a24', '544aa577-91ad-48bd-b0bd-ceeb9d56b9a5', 2, 4, N'&5. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('659b432e-8980-4975-b501-999730677599', 'a2665fd1-5cae-4fac-b19e-b564db3347a0', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('a70476c8-5dfe-44c1-b208-a08b58321329', '790850a0-c45b-47df-8f64-83cd6914c7ea', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('debede28-b5e8-4bee-946b-a62bc6cf02d8', '3dc2d2b0-2ef2-47f6-b842-49ef082ceaea', 2, 4, N'&5. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('c93bc860-4663-4e8c-a0c9-abfed6e7fa81', 'a8939c4b-33f4-4a2a-b190-4b370be6164f', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('eab768db-a758-4b31-b168-ae956b220a49', '62e26b96-043f-4780-95f8-f01e4cde9d46', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('1930711d-1428-4d51-bd27-b0195da69932', '8812242f-a2ad-444b-a1fc-fcbc15b1390f', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('321906dc-cf8b-4c96-8941-b48c84b4b357', '70453208-b426-40b2-bdec-4f28cd2eec87', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('b2ec0598-91be-427d-99e6-b7aa3bde5fec', '18b24d1b-70f2-4d59-899e-66c90c31177a', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('28b2480c-4875-491b-b3ee-b9b1a96494e7', '85dc5dee-81cf-4b54-a3a4-b6f57fb4fa96', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('c3e16024-9e7f-4743-9dcf-be8d02900206', '0c422e8f-c7ab-41f7-bfe7-947dac80e0c2', 2, 4, N'&5. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('b6f0d579-beb5-43fc-8968-c26fcdd4eb25', '68c2f673-ca71-46fa-b7c0-7b21d024388f', 2, 1, N'&2. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('d66b4f70-faab-401a-a2b3-c52bef49aaba', '42ffbab3-4898-4277-9f75-e3e5bafc2ea9', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('b3b0bccd-b1f3-42ed-b230-c9b62af7e8be', '3880b9d0-958c-4d86-906a-9523512953d1', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('d2e41fbc-4e2f-426f-ae64-cb0da81062e2', '3057e5b5-ff79-4761-9da5-a60efb532796', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('0cf6960a-d945-47e1-aebd-ccad29e27d62', 'e0089786-a334-4c44-8699-765552f9fcb3', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('868fd85f-ca24-4409-a8f4-ccb7fd952f01', '774c10b8-d918-454f-af43-d4d5805a0ef3', 1, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('e8ab4430-3205-4998-bd9a-cf69491638b3', 'b93eb44b-37f4-4041-a38b-2060f11ba49c', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('a34d8517-b960-45e5-9e14-d5bae0c0837b', '9c2f5580-8834-49ba-b8a8-58a6cf507e5a', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('6ab6f01d-395e-4197-86e7-d71dca8184bc', 'ec9ca46d-b8dc-4607-9ba0-2bebb8db4e9d', 2, 1, N'&2. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('b19ef4f8-0dd0-479f-9f81-da396e394809', '4af2d054-73b3-4cf5-8350-eb27f34e4b3f', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('bf31964e-6ca8-4eef-a6dd-de108424ad11', '2bd05e5c-17b8-42bf-a54a-9e419568b466', 2, 4, N'&5. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('4af728c9-c642-413d-9955-df1b7fcf90b5', '5dca0bdd-cda3-43d5-9cef-981cf1c298f6', 2, 4, N'&5. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('80ebf9fe-9489-48c9-8a89-e153d4519d40', 'fb3dc6c5-b3cb-422d-a0fe-26c72c2503e1', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('910ccbed-14f1-4884-b793-e18d3f4f42c4', '86f4cfdb-62d5-4339-9f7d-8683da05135c', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('68b51815-0fb6-46a5-bbbd-e62957ce64ca', '1114f347-363e-4697-a1c5-8f9010d254e1', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('d28de887-cf9f-472c-bf80-e799d3a7036b', '479c8788-3154-4fea-874a-c0b476f21766', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('9e823114-3384-4996-8ee9-e942a3f0e930', '5be46a14-7f9e-49b4-a196-fccf0bb3d8d1', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('61a3322d-63e8-48d0-aa47-ecd029de2257', 'd1f6d514-1911-428d-aa22-53e212ec5a5f', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('742becbf-87e5-4b93-899f-ed6561ff9ce2', '682b2fb0-05c5-4008-ad3b-741ea1f8fb2c', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('fd6cf906-6c8b-4a0f-bec0-f374e4aa09a2', '8e5bc949-73b3-42ea-b7c4-74d397b7eacb', 2, 1, N'&2. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('0d0f164f-d1b7-4036-b791-f41bcefb5f04', '2df9201b-273e-4fbe-9312-18491e9c19f7', 2, 3, N'&4. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('6a4d55aa-8b6d-44d3-abd1-f9c78bb71afa', 'aa090801-e3a4-4ad2-8847-3064d45c5e29', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('b6c34904-d85b-45c0-aab9-fa9fdebe0d49', '20859e95-1aac-4c57-83ed-970da9011c02', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('5ab21184-6f6b-4285-bc9c-fd6ddacc4d3b', 'f7fbbb08-56e7-4542-b107-0a2f7ad983a8', 2, 2, N'&3. Chứng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('6dcee98d-b159-44df-a3b0-ff96b7b7249e', '4ae2f337-44ef-46b8-a732-4ccd7f706238', 2, 1, N'&2. Chứng từ tham chiếu')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

SET IDENTITY_INSERT dbo.VoucherPatternsReport ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (272, 0, 0, N'GvoucherList-CTGSS02bDNN1', N'CT ghi sổ (Không cộng gộp HT giống nhau)', N'ChungTuGhiSoS02b-DNN.rst', 1, N'680')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (273, 0, 0, N'GvoucherList-CTGSS02bDNN2', N'CT ghi sổ (Cộng gộp HT giống nhau của cùng CT)', N'ChungTuGhiSoS02b-DNN.rst', 1, N'680')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (274, 0, 0, N'GvoucherList-CTGSS02bDNN3', N'CT ghi sổ (Cộng gộp HT giống nhau của toàn bộ CT)', N'ChungTuGhiSoS02b-DNN.rst', 1, N'680')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (278, 0, 0, N'BangLuongTheoBuoi_TPB', N'Bảng lương theo buổi (theo phòng ban)', N'BangLuong_TPB.rst', 0, N'830')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (279, 0, 0, N'BangLuongTheoGio_TPB', N'Bảng lương theo giờ (theo phòng ban)', N'BangLuong_TPB.rst', 0, N'831')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (280, 0, 0, N'BangLuongCoBanCoDinh_TPB', N'Bảng lương cơ bản cố định (theo phòng ban)', N'BangLuong_TPB.rst', 0, N'832')
GO
SET IDENTITY_INSERT dbo.VoucherPatternsReport OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DBCC CHECKIDENT('dbo.VoucherPatternsReport', RESEED, 280)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.GenCode WITH NOCHECK
  ADD CONSTRAINT FK_GenCode_TypeGroup FOREIGN KEY (TypeGroupID) REFERENCES dbo.TypeGroup(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Template WITH NOCHECK
  ADD CONSTRAINT FK_Template_Type FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateDetail WITH NOCHECK
  ADD CONSTRAINT FK_TemplateDetail_Template FOREIGN KEY (TemplateID) REFERENCES dbo.Template(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF