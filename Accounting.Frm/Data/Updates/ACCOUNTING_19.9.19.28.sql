SET CONCAT_NULL_YIELDS_NULL, ANSI_NULLS, ANSI_PADDING, QUOTED_IDENTIFIER, ANSI_WARNINGS, ARITHABORT, XACT_ABORT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS OFF
GO

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO

ALTER TABLE [dbo].[VoucherPatternsReport]
  ALTER
    COLUMN [VoucherPatternsCode] [nvarchar](50)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE PROCEDURE [dbo].[Proc_CountVoucher]
    @FromDate DATETIME,
    @ToDate DATETIME,
	@TypeCount int
AS
BEGIN          
	DECLARE @tbLuuTru TABLE (
		ID uniqueidentifier,
		Date datetime,
		No nvarchar(25),
		RefTable nvarchar(250)
	)

	INSERT INTO @tbLuuTru(ID,Date,No,RefTable)
		Select ID, Date, No, 'FAAdjustment' as RefTable From FAAdjustment
		UNION ALL
		Select ID, Date, No, 'FAAudit' as RefTable From FAAudit
		UNION ALL 
		Select ID, Date, No, 'FADecrement' as RefTable From FADecrement
		UNION ALL 
		Select ID, Date, No, 'FADepreciation' as RefTable From FADepreciation
		UNION ALL
		Select ID, Date, No, 'FAIncrement' as RefTable From FAIncrement
		UNION ALL
		Select ID, Date, No, 'FATransfer' as RefTable From FATransfer
		UNION ALL
		Select ID, Date, No, 'GOtherVoucher' as RefTable From GOtherVoucher
		UNION ALL
		Select ID, Date, No, 'GvoucherList' as RefTable From GvoucherList
		UNION ALL
		Select ID, Date, No, 'MBCreditCard' as RefTable From MBCreditCard
		UNION ALL
		Select ID, Date, No, 'MBDeposit' as RefTable From MBDeposit
		UNION ALL
		Select ID, Date, No, 'MBInternalTransfer' as RefTable From MBInternalTransfer
		UNION ALL
		Select ID, Date, No, 'MBTellerPaper' as RefTable From MBTellerPaper
		UNION ALL
		Select ID, Date, No, 'MCAudit' as RefTable From MCAudit
		UNION ALL
		Select ID, Date, No, 'MCPayment' as RefTable From MCPayment
		UNION ALL
		Select ID, Date, No, 'MCReceipt' as RefTable From MCReceipt
		UNION ALL
		Select ID, Date, No, 'PPDiscountReturn' as RefTable From PPDiscountReturn
		UNION ALL
		Select ID, Date, No, 'PPInvoice' as RefTable From PPInvoice
		UNION ALL
		Select ID, Date, No, 'PPOrder' as RefTable From PPOrder
		UNION ALL
		Select ID, Date, No, 'PPService' as RefTable From PPService
		UNION ALL
		Select ID, Date, No, 'RSAssemblyDismantlement_new' as RefTable From RSAssemblyDismantlement_new
		UNION ALL
		Select ID, Date, No, 'RSInwardOutward' as RefTable From RSInwardOutward
		UNION ALL 
		Select ID, Date, No, 'RSTransfer' as RefTable From RSTransfer
		UNION ALL
		Select ID, Date, No, 'SAInvoice' as RefTable From SAInvoice
		UNION ALL
		Select ID, Date, No, 'SAOrder' as RefTable From SAOrder
		UNION ALL
		Select ID, Date, No, 'SAQuote' as RefTable From SAQuote
		UNION ALL 
		Select ID, Date, No, 'SAReturn' as RefTable From SAReturn
		UNION ALL
		Select ID, Date, No, 'TIAdjustment' as RefTable From TIAdjustment
		UNION ALL
		Select ID, Date, No, 'TIAllocation' as RefTable From TIAllocation
		UNION ALL
		Select ID, Date, No, 'TIAudit' as RefTable From TIAudit
		UNION ALL
		Select ID, Date, No, 'TIDecrement' as RefTable From TIDecrement
		UNION ALL
		Select ID, Date, No, 'TIIncrement' as RefTable From TIIncrement

	IF (@TypeCount = 0) 
	BEGIN
		select count(No) from @tbLuuTru
	END
	ELSE
	BEGIN
		select count(No) from @tbLuuTru where Date between @FromDate and @ToDate
	END

END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[EMContractDetailMG]
  ADD [UnitPriceOriginal] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sp_refreshview '[dbo].[ViewVouchersCloseBook]'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO






CREATE VIEW [dbo].[ViewVouchersCloseBook1]
AS
/*Create by tnson 24/08/2011 - View này chỉ giành riêng cho việc kiểm tra các chứng từ chưa ghi sổ trước khi khóa sổ không được dùng cho việc khác */ SELECT dbo.MCPayment.ID, dbo.MCPaymentDetail.ID AS DetailID, dbo.MCPayment.TypeID, 
                         dbo.Type.TypeName, dbo.MCPayment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCPayment.Date, dbo.MCPayment.PostedDate, dbo.MCPaymentDetail.DebitAccount, dbo.MCPaymentDetail.CreditAccount, 
                         dbo.MCPayment.CurrencyID, dbo.MCPaymentDetail.AmountOriginal, dbo.MCPaymentDetail.Amount, NULL AS OrgPrice, dbo.MCPayment.Reason, dbo.MCPaymentDetail.Description, dbo.MCPaymentDetail.CostSetID, 
                         CostSet.CostSetCode, dbo.MCPaymentDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.MCPayment.EmployeeID, dbo.MCPayment.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.MCPayment.Recorded, dbo.MCPayment.TotalAmount, dbo.MCPayment.TotalAmountOriginal, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MCPayment' AS RefTable
/*case AccountingObject.IsEmployee when 1 then  AccountingObject.AccountingObjectName end AS EmployeeName*/ FROM dbo.MCPayment INNER JOIN
                         dbo.MCPaymentDetail ON dbo.MCPayment.ID = dbo.MCPaymentDetail.MCPaymentID INNER JOIN
                         dbo.Type ON MCPayment.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MCPaymentDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MCPaymentDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MCPaymentDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.MCPayment.ID, dbo.MCPaymentDetailTax.ID AS DetailID, dbo.MCPayment.TypeID, dbo.Type.TypeName, dbo.MCPayment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCPayment.Date, 
                         dbo.MCPayment.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MCPayment.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MCPayment.Reason, 
                         dbo.MCPaymentDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, dbo.MCPaymentDetailTax.AccountingObjectID , NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MCPayment.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MCPaymentDetailTax.VATAmountOriginal, dbo.MCPaymentDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.MCPayment.Recorded, dbo.MCPayment.TotalAmount, dbo.MCPayment.TotalAmountOriginal, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MCPayment' AS RefTable
FROM            dbo.MCPayment INNER JOIN
                         dbo.MCPaymentDetailTax ON dbo.MCPayment.ID = dbo.MCPaymentDetailTax.MCPaymentID INNER JOIN
                         dbo.Type ON MCPayment.TypeID = dbo.Type.ID
/*========================*/ UNION ALL
SELECT        dbo.MBDeposit.ID, dbo.MBDepositDetail.ID AS DetailID, dbo.MBDeposit.TypeID, dbo.Type.TypeName, dbo.MBDeposit.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBDeposit.Date, dbo.MBDeposit.PostedDate, 
                         dbo.MBDepositDetail.DebitAccount, dbo.MBDepositDetail.CreditAccount, dbo.MBDeposit.CurrencyID, dbo.MBDepositDetail.AmountOriginal, dbo.MBDepositDetail.Amount, NULL AS OrgPrice, dbo.MBDeposit.Reason, 
                         dbo.MBDepositDetail.Description, dbo.MBDepositDetail.CostSetID, CostSet.CostSetCode, dbo.MBDepositDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MBDeposit.EmployeeID, dbo.MBDeposit.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL 
                         AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBDeposit.Recorded, dbo.MBDeposit.TotalAmountOriginal, dbo.MBDeposit.TotalAmount, NULL AS DiscountAmountOriginal, NULL 
                         AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBDeposit' AS RefTable
FROM            dbo.MBDeposit INNER JOIN
                         dbo.MBDepositDetail ON dbo.MBDeposit.ID = dbo.MBDepositDetail.MBDepositID INNER JOIN
                         dbo.Type ON MBDeposit.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBDepositDetail.AccountingObjectID = dbo.AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBDepositDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBDepositDetail.ContractID
/*========================      */ UNION ALL
SELECT        dbo.MBDeposit.ID, dbo.MBDepositDetailTax.ID AS DetailID, dbo.MBDeposit.TypeID, dbo.Type.TypeName, dbo.MBDeposit.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBDeposit.Date, dbo.MBDeposit.PostedDate, NULL 
                         AS DebitAccount, NULL AS CreditAccount, dbo.MBDeposit.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBDeposit.Reason, dbo.MBDepositDetailTax.Description, NULL AS CostSetID, NULL 
                         AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, dbo.MBDepositDetailTax.AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBDeposit.BranchID, NULL 
                         AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL 
                         AS VATAccount, dbo.MBDepositDetailTax.VATAmountOriginal, dbo.MBDepositDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBDeposit.Recorded, dbo.MBDeposit.TotalAmountOriginal, 
                         dbo.MBDeposit.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBDeposit' AS RefTable
FROM            dbo.MBDeposit INNER JOIN
                         dbo.MBDepositDetailTax ON dbo.MBDeposit.ID = dbo.MBDepositDetailTax.MBDepositID INNER JOIN
                         dbo.Type ON MBDeposit.TypeID = dbo.Type.ID
/*Chuyển tiền nội bộ*/ UNION ALL
SELECT        dbo.MBInternalTransfer.ID, dbo.MBInternalTransferDetail.ID AS DetailID, dbo.MBInternalTransfer.TypeID, dbo.Type.TypeName, dbo.MBInternalTransfer.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBInternalTransfer.Date, 
                         dbo.MBInternalTransfer.PostedDate, dbo.MBInternalTransferDetail.DebitAccount, dbo.MBInternalTransferDetail.CreditAccount, dbo.MBInternalTransferDetail.CurrencyID, dbo.MBInternalTransferDetail.AmountOriginal, 
                         dbo.MBInternalTransferDetail.Amount, NULL AS OrgPrice, dbo.MBInternalTransfer.Reason, dbo.MBInternalTransfer.Reason AS Description, dbo.MBInternalTransferDetail.CostSetID, CostSet.CostSetCode, 
                         dbo.MBInternalTransferDetail.ContractID, EMContract.Code AS ContractCode, NULL AS AccountingObjectID, '' AS AccountingObjectCategoryName, '' AS AccountingObjectCode, dbo.MBInternalTransferDetail.EmployeeID, 
                         dbo.MBInternalTransfer.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBInternalTransfer.Recorded, dbo.MBInternalTransfer.TotalAmountOriginal, 
                         dbo.MBInternalTransfer.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBInternalTransfer' AS RefTable
FROM            dbo.MBInternalTransfer INNER JOIN
                         dbo.MBInternalTransferDetail ON dbo.MBInternalTransfer.ID = dbo.MBInternalTransferDetail.MBInternalTransferID INNER JOIN
                         dbo.Type ON dbo.MBInternalTransfer.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBInternalTransferDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBInternalTransferDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.MBTellerPaper.ID, dbo.MBTellerPaperDetail.ID AS DetailID, dbo.MBTellerPaper.TypeID, dbo.Type.TypeName, dbo.MBTellerPaper.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBTellerPaper.Date, 
                         dbo.MBTellerPaper.PostedDate, dbo.MBTellerPaperDetail.DebitAccount, dbo.MBTellerPaperDetail.CreditAccount, dbo.MBTellerPaper.CurrencyID, dbo.MBTellerPaperDetail.AmountOriginal, 
                         dbo.MBTellerPaperDetail.Amount, NULL AS OrgPrice, dbo.MBTellerPaper.Reason, dbo.MBTellerPaperDetail.Description, dbo.MBTellerPaperDetail.CostSetID, CostSet.CostSetCode, dbo.MBTellerPaperDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, 
                         dbo.MBTellerPaper.EmployeeID, dbo.MBTellerPaper.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBTellerPaper.Recorded, 
                         dbo.MBTellerPaper.TotalAmountOriginal, dbo.MBTellerPaper.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 
                         0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBTellerPaper' AS RefTable
FROM            dbo.MBTellerPaper INNER JOIN
                         dbo.MBTellerPaperDetail ON dbo.MBTellerPaper.ID = dbo.MBTellerPaperDetail.MBTellerPaperID INNER JOIN
                         dbo.Type ON dbo.MBTellerPaper.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBTellerPaperDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBTellerPaperDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBTellerPaperDetail.ContractID
/*===========================================*/ UNION ALL
SELECT        dbo.MBTellerPaper.ID, dbo.MBTellerPaperDetailTax.ID AS DetailID, dbo.MBTellerPaper.TypeID, dbo.Type.TypeName, dbo.MBTellerPaper.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBTellerPaper.Date, 
                         dbo.MBTellerPaper.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MBTellerPaper.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBTellerPaper.Reason, 
                         dbo.MBTellerPaperDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, dbo.MBTellerPaperDetailTax.AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBTellerPaper.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MBTellerPaperDetailTax.VATAmountOriginal, dbo.MBTellerPaperDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.MBTellerPaper.Recorded, dbo.MBTellerPaper.TotalAmountOriginal, dbo.MBTellerPaper.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MBTellerPaper' AS RefTable
FROM            dbo.MBTellerPaper INNER JOIN
                         dbo.MBTellerPaperDetailTax ON dbo.MBTellerPaper.ID = dbo.MBTellerPaperDetailTax.MBTellerPaperID INNER JOIN
                         dbo.Type ON dbo.MBTellerPaper.TypeID = dbo.Type.ID
/*========================*/ UNION ALL
SELECT        dbo.MBCreditCard.ID, dbo.MBCreditCardDetail.ID AS DetailID, dbo.MBCreditCard.TypeID, dbo.Type.TypeName, dbo.MBCreditCard.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBCreditCard.Date, 
                         dbo.MBCreditCard.PostedDate, dbo.MBCreditCardDetail.DebitAccount, dbo.MBCreditCardDetail.CreditAccount, dbo.MBCreditCard.CurrencyID, dbo.MBCreditCardDetail.AmountOriginal, dbo.MBCreditCardDetail.Amount, NULL 
                         AS OrgPrice, dbo.MBCreditCard.Reason, dbo.MBCreditCardDetail.Description, dbo.MBCreditCardDetail.CostSetID, CostSet.CostSetCode, dbo.MBCreditCardDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MBCreditCard.EmployeeID, 
                         dbo.MBCreditCard.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBCreditCard.Recorded, dbo.MBCreditCard.TotalAmountOriginal, 
                         dbo.MBCreditCard.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBCreditCard' AS RefTable
FROM            dbo.MBCreditCard INNER JOIN
                         dbo.MBCreditCardDetail ON dbo.MBCreditCard.ID = dbo.MBCreditCardDetail.MBCreditCardID INNER JOIN
                         dbo.Type ON dbo.MBCreditCard.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBCreditCardDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBCreditCardDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBCreditCardDetail.ContractID
/*=========================================*/ UNION ALL
SELECT        dbo.MBCreditCard.ID, dbo.MBCreditCardDetailTax.ID AS DetailID, dbo.MBCreditCard.TypeID, dbo.Type.TypeName, dbo.MBCreditCard.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBCreditCard.Date, 
                         dbo.MBCreditCard.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MBCreditCard.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBCreditCard.Reason, 
                         dbo.MBCreditCardDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, dbo.MBCreditCardDetailTax.AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBCreditCard.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MBCreditCardDetailTax.VATAmountOriginal, dbo.MBCreditCardDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.MBCreditCard.Recorded, dbo.MBCreditCard.TotalAmountOriginal, dbo.MBCreditCard.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MBCreditCard' AS RefTable
FROM            dbo.MBCreditCard INNER JOIN
                         dbo.MBCreditCardDetailTax ON dbo.MBCreditCard.ID = dbo.MBCreditCardDetailTax.MBCreditCardID INNER JOIN
                         dbo.Type ON dbo.MBCreditCard.TypeID = dbo.Type.ID
/* =======================================================================*/ UNION ALL
SELECT        dbo.MCReceipt.ID, dbo.MCReceiptDetail.ID AS DetailID, dbo.MCReceipt.TypeID, dbo.Type.TypeName, dbo.MCReceipt.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCReceipt.Date, dbo.MCReceipt.PostedDate, 
                         dbo.MCReceiptDetail.DebitAccount, dbo.MCReceiptDetail.CreditAccount, dbo.MCReceipt.CurrencyID, dbo.MCReceiptDetail.AmountOriginal, dbo.MCReceiptDetail.Amount, NULL AS OrgPrice, dbo.MCReceipt.Reason, 
                         dbo.MCReceiptDetail.Description, dbo.MCReceiptDetail.CostSetID, CostSet.CostSetCode, dbo.MCReceiptDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MCReceipt.EmployeeID, dbo.MCReceipt.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL 
                         AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MCReceipt.Recorded, dbo.MCReceipt.TotalAmountOriginal, dbo.MCReceipt.TotalAmount, NULL AS DiscountAmountOriginal, NULL 
                         AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MCReceipt' AS RefTable
FROM            dbo.MCReceipt INNER JOIN
                         dbo.MCReceiptDetail ON dbo.MCReceipt.ID = dbo.MCReceiptDetail.MCReceiptID INNER JOIN
                         dbo.Type ON MCReceipt.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MCReceiptDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MCReceiptDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MCReceiptDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.MCReceipt.ID, dbo.MCReceiptDetailTax.ID AS DetailID, dbo.MCReceipt.TypeID, dbo.Type.TypeName, dbo.MCReceipt.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCReceipt.Date, dbo.MCReceipt.PostedDate, NULL 
                         AS DebitAccount, NULL AS CreditAccount, dbo.MCReceipt.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MCReceipt.Reason, dbo.MCReceiptDetailTax.Description, NULL AS CostSetID, NULL 
                         AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, dbo.MCReceiptDetailTax.AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL AS AccountingObjectCode, NULL AS EmployeeID, dbo.MCReceipt.BranchID, NULL 
                         AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL 
                         AS VATAccount, dbo.MCReceiptDetailTax.VATAmountOriginal, dbo.MCReceiptDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MCReceipt.Recorded, dbo.MCReceipt.TotalAmountOriginal, 
                         dbo.MCReceipt.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MCReceipt' AS RefTable
FROM            dbo.MCReceipt INNER JOIN
                         dbo.MCReceiptDetailTax ON dbo.MCReceipt.ID = dbo.MCReceiptDetailTax.MCReceiptID INNER JOIN
                         dbo.Type ON MCReceipt.TypeID = dbo.Type.ID
/*========================*/ UNION ALL
SELECT        dbo.FAAdjustment.ID, dbo.FAAdjustmentDetail.ID AS DetailID, dbo.FAAdjustment.TypeID, dbo.Type.TypeName, dbo.FAAdjustment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FAAdjustment.Date, 
                         dbo.FAAdjustment.PostedDate, dbo.FAAdjustmentDetail.DebitAccount, dbo.FAAdjustmentDetail.CreditAccount, NULL AS CurrencyID, $ 0 AS AmountOriginal, dbo.FAAdjustmentDetail.Amount, $ 0 AS OrgPrice, 
                         dbo.FAAdjustment.Reason, dbo.FAAdjustmentDetail.Description, dbo.FAAdjustmentDetail.CostSetID, CostSet.CostSetCode, dbo.FAAdjustmentDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, NULL AS EmployeeID, 
                         dbo.FAAdjustment.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, $ 0 AS VATAmountOriginal, $ 0 AS VATAmount, $ 0 AS Quantity, $ 0 AS UnitPrice, dbo.FAAdjustment.Recorded, $ 0 AS TotalAmountOriginal, 
                         dbo.FAAdjustment.TotalAmount, $ 0 AS DiscountAmountOriginal, $ 0 AS DiscountAmount, NULL AS DiscountAccount, $ 0 AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FAAdjustment' AS RefTable
FROM            dbo.FAAdjustment INNER JOIN
                         dbo.FAAdjustmentDetail ON dbo.FAAdjustment.ID = dbo.FAAdjustmentDetail.FAAdjustmentID INNER JOIN
                         dbo.Type ON FAAdjustment.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FAAdjustmentDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FAAdjustment.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FAAdjustmentDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FAAdjustmentDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.FADepreciation.ID, dbo.FADepreciationDetail.ID AS DetailID, dbo.FADepreciation.TypeID, dbo.Type.TypeName, dbo.FADepreciation.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FADepreciation.Date, 
                         dbo.FADepreciation.PostedDate, dbo.FADepreciationDetail.DebitAccount, dbo.FADepreciationDetail.CreditAccount, FADepreciationDetail.CurrencyID, dbo.FADepreciationDetail.AmountOriginal, dbo.FADepreciationDetail.Amount, 
                         dbo.FADepreciationDetail.OrgPrice, dbo.FADepreciation.Reason, dbo.FADepreciationDetail.Description, dbo.FADepreciationDetail.CostSetID, CostSet.CostSetCode, dbo.FADepreciationDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, 
                         dbo.FADepreciationDetail.EmployeeID, dbo.FADepreciation.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, FADepreciationDetail.ObjectID, Department.DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.FADepreciation.Recorded, dbo.FADepreciation.TotalAmountOriginal, dbo.FADepreciation.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'FADepreciation' AS RefTable
FROM            dbo.FADepreciation INNER JOIN
                         dbo.FADepreciationDetail ON dbo.FADepreciation.ID = dbo.FADepreciationDetail.FADepreciationID INNER JOIN
                         dbo.Type ON FADepreciation.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FADepreciationDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FADepreciationDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FADepreciationDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FADepreciationDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FADepreciationDetail.ObjectID
/*========================*/ UNION ALL
SELECT        dbo.FAIncrement.ID, dbo.FAIncrementDetail.ID AS DetailID, dbo.FAIncrement.TypeID, dbo.Type.TypeName, dbo.FAIncrement.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FAIncrement.Date, dbo.FAIncrement.PostedDate, 
                         dbo.FAIncrementDetail.DebitAccount, dbo.FAIncrementDetail.CreditAccount, FAIncrement.CurrencyID, dbo.FAIncrementDetail.AmountOriginal, dbo.FAIncrementDetail.Amount, dbo.FAIncrementDetail.OrgPriceOriginal AS OrgPrice, 
                         dbo.FAIncrement.Reason, dbo.FAIncrementDetail.Description, dbo.FAIncrementDetail.CostSetID, CostSet.CostSetCode, dbo.FAIncrementDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.FAIncrement.EmployeeID, dbo.FAIncrement.BranchID, dbo.FixedAsset.ID AS FixedAssetID, 
                         dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, FAIncrementDetail.DepartmentID, dbo.Department.DepartmentName, 
                         dbo.FAIncrementDetail.VATAccount AS VATAccount, dbo.FAIncrementDetail.VATAmountOriginal AS VATAmountOriginal, dbo.FAIncrementDetail.VATAmount AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.FAIncrement.Recorded, dbo.FAIncrement.TotalAmountOriginal, dbo.FAIncrement.TotalAmount, dbo.FAIncrementDetail.DiscountAmountOriginal, dbo.FAIncrementDetail.DiscountAmount, NULL AS DiscountAccount, 
                         dbo.FAIncrementDetail.FreightAmountOriginal, dbo.FAIncrementDetail.FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FAIncrement' AS RefTable
FROM            dbo.FAIncrement INNER JOIN
                         dbo.FAIncrementDetail ON dbo.FAIncrement.ID = dbo.FAIncrementDetail.ID INNER JOIN
                         dbo.Type ON FAIncrement.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FAIncrementDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FAIncrementDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FAIncrementDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FAIncrementDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FAIncrementDetail.DepartmentID
/*========================*/ UNION ALL
SELECT        dbo.FADecrement.ID, dbo.FADecrementDetail.ID AS DetailID, dbo.FADecrement.TypeID, dbo.Type.TypeName, dbo.FADecrement.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FADecrement.Date, 
                         dbo.FADecrement.PostedDate, dbo.FADecrementDetail.DebitAccount, dbo.FADecrementDetail.CreditAccount, FADecrement.CurrencyID, dbo.FADecrementDetail.AmountOriginal, dbo.FADecrementDetail.Amount, NULL AS OrgPrice, 
                         dbo.FADecrement.Reason, dbo.FADecrementDetail.Description, dbo.FADecrementDetail.CostSetID, CostSet.CostSetCode, dbo.FADecrementDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.FADecrementDetail.EmployeeID, 
                         dbo.FADecrement.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, 
                         FADecrementDetail.DepartmentID, Department.DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.FADecrement.Recorded, 
                         dbo.FADecrement.TotalAmountOriginal, dbo.FADecrement.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 
                         0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FADecrement' AS RefTable
FROM            dbo.FADecrement INNER JOIN
                         dbo.FADecrementDetail ON dbo.FADecrement.ID = dbo.FADecrementDetail.FADecrementID INNER JOIN
                         dbo.Type ON FADecrement.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FADecrementDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FADecrementDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FADecrementDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FADecrementDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FADecrementDetail.DepartmentID
/*========================*/ UNION ALL
/*Chứng từ nghiệp vụ khác*/ SELECT dbo.GOtherVoucher.ID, dbo.GOtherVoucherDetail.ID AS DetailID, dbo.GOtherVoucher.TypeID, dbo.Type.TypeName, dbo.GOtherVoucher.No, NULL AS InwardNo, NULL AS OutwardNo, 
                         dbo.GOtherVoucher.Date, dbo.GOtherVoucher.PostedDate, dbo.GOtherVoucherDetail.DebitAccount, dbo.GOtherVoucherDetail.CreditAccount, GOtherVoucherDetail.CurrencyID, dbo.GOtherVoucherDetail.AmountOriginal, 
                         dbo.GOtherVoucherDetail.Amount, NULL AS OrgPrice, dbo.GOtherVoucher.Reason, dbo.GOtherVoucherDetail.Description, dbo.GOtherVoucherDetail.CostSetID, CostSet.CostSetCode, dbo.GOtherVoucherDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.GOtherVoucherDetail.DebitAccountingObjectID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode AS AccountingObjectCode, dbo.GOtherVoucherDetail.EmployeeID, dbo.GOtherVoucher.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL 
                         AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL 
                         AS UnitPrice, dbo.GOtherVoucher.Recorded, dbo.GOtherVoucher.TotalAmountOriginal, dbo.GOtherVoucher.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'GOtherVoucher' AS RefTable
FROM            dbo.GOtherVoucher INNER JOIN
                         dbo.GOtherVoucherDetail ON dbo.GOtherVoucher.ID = dbo.GOtherVoucherDetail.GOtherVoucherID INNER JOIN
                         dbo.Type ON GOtherVoucher.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON GOtherVoucherDetail.DebitAccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = GOtherVoucherDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = GOtherVoucherDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.GOtherVoucher.ID, dbo.GOtherVoucherDetailTax.ID AS DetailID, dbo.GOtherVoucher.TypeID, dbo.Type.TypeName, dbo.GOtherVoucher.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.GOtherVoucher.Date, 
                         dbo.GOtherVoucher.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, GOtherVoucherDetailTax.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.GOtherVoucher.Reason, 
                         dbo.GOtherVoucherDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AccountingObjectCode, NULL AS EmployeeID, dbo.GOtherVoucher.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.GOtherVoucherDetailTax.VATAmountOriginal, dbo.GOtherVoucherDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.GOtherVoucher.Recorded, dbo.GOtherVoucher.TotalAmountOriginal, dbo.GOtherVoucher.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'GOtherVoucher' AS RefTable
FROM            dbo.GOtherVoucher INNER JOIN
                         dbo.GOtherVoucherDetailTax ON dbo.GOtherVoucher.ID = dbo.GOtherVoucherDetailTax.GOtherVoucherID INNER JOIN
                         dbo.Type ON GOtherVoucher.TypeID = dbo.Type.ID
/*========================*/ UNION ALL
SELECT        dbo.RSInwardOutward.ID, dbo.RSInwardOutwardDetail.ID AS DetailID, dbo.RSInwardOutward.TypeID, dbo.Type.TypeName, dbo.RSInwardOutward.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.RSInwardOutward.Date, 
                         dbo.RSInwardOutward.PostedDate, dbo.RSInwardOutwardDetail.DebitAccount, dbo.RSInwardOutwardDetail.CreditAccount, RSInwardOutward.CurrencyID, dbo.RSInwardOutwardDetail.AmountOriginal, 
                         dbo.RSInwardOutwardDetail.Amount, NULL AS OrgPrice, dbo.RSInwardOutward.Reason, dbo.RSInwardOutwardDetail.Description, dbo.RSInwardOutwardDetail.CostSetID, CostSet.CostSetCode, 
                         dbo.RSInwardOutwardDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.RSInwardOutwardDetail.EmployeeID, dbo.RSInwardOutward.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, 
                         dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, 
                         RSInwardOutwardDetail.Quantity, RSInwardOutwardDetail.UnitPrice, dbo.RSInwardOutward.Recorded, dbo.RSInwardOutward.TotalAmountOriginal, dbo.RSInwardOutward.TotalAmount, NULL AS DiscountAmountOriginal, NULL 
                         AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'RSInwardOutward' AS RefTable
FROM            dbo.RSInwardOutward INNER JOIN
                         dbo.RSInwardOutwardDetail ON dbo.RSInwardOutward.ID = dbo.RSInwardOutwardDetail.RSInwardOutwardID INNER JOIN
                         dbo.Type ON RSInwardOutward.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON RSInwardOutwardDetail.EmployeeID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON RSInwardOutwardDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON RSInwardOutwardDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = RSInwardOutwardDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = RSInwardOutwardDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.RSTransfer.ID, dbo.RSTransferDetail.ID AS DetailID, dbo.RSTransfer.TypeID, dbo.Type.TypeName, dbo.RSTransfer.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.RSTransfer.Date, dbo.RSTransfer.PostedDate, 
                         dbo.RSTransferDetail.DebitAccount, dbo.RSTransferDetail.CreditAccount, RSTransfer.CurrencyID, dbo.RSTransferDetail.AmountOriginal, dbo.RSTransferDetail.Amount, NULL AS OrgPrice, dbo.RSTransfer.Reason, 
                         dbo.RSTransferDetail.Description, dbo.RSTransferDetail.CostSetID, CostSet.CostSetCode, dbo.RSTransferDetail.ContractID, EMContract.Code AS ContractCode, dbo.RSTransfer.AccountingObjectID, NULL 
                         AS AccountingObjectCategoryName, NULL AS AccountingObjectCode, dbo.RSTransferDetail.EmployeeID, dbo.RSTransfer.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, 
                         dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL 
                         AS VATAmount, RSTransferDetail.Quantity, RSTransferDetail.UnitPrice, dbo.RSTransfer.Recorded, dbo.RSTransfer.TotalAmountOriginal, dbo.RSTransfer.TotalAmount, NULL AS DiscountAmountOriginal, NULL 
                         AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'RSTransfer' AS RefTable
FROM            dbo.RSTransfer INNER JOIN
                         dbo.RSTransferDetail ON dbo.RSTransfer.ID = dbo.RSTransferDetail.RSTransferID INNER JOIN
                         dbo.Type ON RSTransfer.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.MaterialGoods ON RSTransferDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON RSTransferDetail.ToRepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = RSTransferDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = RSTransferDetail.ContractID
/*======================== */ UNION ALL
SELECT        dbo.PPInvoice.ID, dbo.PPInvoiceDetail.ID AS DetailID, dbo.PPInvoice.TypeID, dbo.Type.TypeName, dbo.PPInvoice.No, dbo.PPInvoice.InwardNo AS InwardNo, NULL AS OutwardNo, dbo.PPInvoice.Date, 
                         dbo.PPInvoice.PostedDate, dbo.PPInvoiceDetail.DebitAccount, dbo.PPInvoiceDetail.CreditAccount, PPInvoice.CurrencyID, dbo.PPInvoiceDetail.AmountOriginal, dbo.PPInvoiceDetail.Amount, NULL AS OrgPrice, 
                         dbo.PPInvoice.Reason, dbo.PPInvoiceDetail.Description, dbo.PPInvoiceDetail.CostSetID, CostSet.CostSetCode, dbo.PPInvoiceDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPInvoice.EmployeeID, 
                         dbo.PPInvoice.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, 
                         dbo.Repository.RepositoryName, dbo.PPInvoiceDetail.DepartmentID, NULL AS DepartmentName, PPInvoiceDetail.VATAccount, PPInvoiceDetail.VATAmountOriginal, PPInvoiceDetail.VATAmount, PPInvoiceDetail.Quantity, 
                         PPInvoiceDetail.UnitPrice, dbo.PPInvoice.Recorded, dbo.PPInvoice.TotalAmountOriginal, dbo.PPInvoice.TotalAmount, dbo.PPInvoiceDetail.DiscountAmountOriginal, dbo.PPInvoiceDetail.DiscountAmount, NULL 
                         AS DiscountAccount, dbo.PPInvoiceDetail.FreightAmountOriginal, dbo.PPInvoiceDetail.FreightAmount, 0 AS DetailTax, PPInvoiceDetail.InwardAmountOriginal, PPInvoiceDetail.InwardAmount, 
                         dbo.PPInvoice.StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPInvoice' AS RefTable
FROM            dbo.PPInvoice INNER JOIN
                         dbo.PPInvoiceDetail ON dbo.PPInvoice.ID = dbo.PPInvoiceDetail.PPInvoiceID INNER JOIN
                         dbo.Type ON PPInvoice.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPInvoiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPInvoiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON PPInvoiceDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPInvoiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPInvoiceDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.PPDiscountReturn.ID, dbo.PPDiscountReturnDetail.ID AS DetailID, dbo.PPDiscountReturn.TypeID, dbo.Type.TypeName, dbo.PPDiscountReturn.No, NULL AS InwardNo, dbo.PPDiscountReturn.OutwardNo AS OutwardNo, 
                         dbo.PPDiscountReturn.Date, dbo.PPDiscountReturn.PostedDate, dbo.PPDiscountReturnDetail.DebitAccount, dbo.PPDiscountReturnDetail.CreditAccount, PPDiscountReturn.CurrencyID, 
                         dbo.PPDiscountReturnDetail.AmountOriginal, dbo.PPDiscountReturnDetail.Amount, NULL AS OrgPrice, dbo.PPDiscountReturn.Reason, dbo.PPDiscountReturnDetail.Description, dbo.PPDiscountReturnDetail.CostSetID, 
                         CostSet.CostSetCode, dbo.PPDiscountReturnDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPDiscountReturn.EmployeeID, dbo.PPDiscountReturn.BrachID AS BranchID, NULL 
                         AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, PPDiscountReturnDetail.VATAccount, PPDiscountReturnDetail.VATAmountOriginal, PPDiscountReturnDetail.VATAmount, PPDiscountReturnDetail.Quantity, 
                         PPDiscountReturnDetail.UnitPrice, dbo.PPDiscountReturn.Recorded, dbo.PPDiscountReturn.TotalAmountOriginal, dbo.PPDiscountReturn.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL 
                         AS OutwardAmount, 'PPDiscountReturn' AS RefTable
FROM            dbo.PPDiscountReturn INNER JOIN
                         dbo.PPDiscountReturnDetail ON dbo.PPDiscountReturn.ID = dbo.PPDiscountReturnDetail.PPDiscountReturnID INNER JOIN
                         dbo.Type ON PPDiscountReturn.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPDiscountReturnDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPDiscountReturnDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON PPDiscountReturnDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPDiscountReturnDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPDiscountReturnDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.PPService.ID, dbo.PPServiceDetail.ID AS DetailID, dbo.PPService.TypeID, dbo.Type.TypeName, dbo.PPService.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.PPService.Date, dbo.PPService.PostedDate, 
                         dbo.PPServiceDetail.DebitAccount, dbo.PPServiceDetail.CreditAccount, PPService.CurrencyID, dbo.PPServiceDetail.AmountOriginal, dbo.PPServiceDetail.Amount, NULL AS OrgPrice, dbo.PPService.Reason, 
                         dbo.PPServiceDetail.Description, dbo.PPServiceDetail.CostSetID, CostSet.CostSetCode, dbo.PPServiceDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPService.EmployeeID, dbo.PPService.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, 
                         PPServiceDetail.VATAccount, PPServiceDetail.VATAmountOriginal, PPServiceDetail.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.PPService.Recorded, dbo.PPService.TotalAmountOriginal, dbo.PPService.TotalAmount, 
                         PPServiceDetail.DiscountAmountOriginal, PPServiceDetail.DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPService' AS RefTable
FROM            dbo.PPService INNER JOIN
                         dbo.PPServiceDetail ON dbo.PPService.ID = dbo.PPServiceDetail.PPServiceID INNER JOIN
                         dbo.Type ON PPService.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPServiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPServiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPServiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPServiceDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.PPOrder.ID, dbo.PPOrderDetail.ID AS DetailID, dbo.PPOrder.TypeID, dbo.Type.TypeName, dbo.PPOrder.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.PPOrder.Date, dbo.PPOrder.DeliverDate AS PostedDate, 
                         dbo.PPOrderDetail.DebitAccount, dbo.PPOrderDetail.CreditAccount, PPOrder.CurrencyID, dbo.PPOrderDetail.AmountOriginal, dbo.PPOrderDetail.Amount, NULL AS OrgPrice, dbo.PPOrder.Reason, dbo.PPOrderDetail.Description, 
                         dbo.PPOrderDetail.CostSetID, CostSet.CostSetCode, dbo.PPOrderDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPOrder.EmployeeID, dbo.PPOrder.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, 
                         PPOrderDetail.VATAccount, PPOrderDetail.VATAmountOriginal, PPOrderDetail.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.PPOrder.Exported AS Recorded, dbo.PPOrder.TotalAmountOriginal, dbo.PPOrder.TotalAmount, 
                         PPOrderDetail.DiscountAmountOriginal, PPOrderDetail.DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPOrder' AS RefTable
FROM            dbo.PPOrder INNER JOIN
                         dbo.PPOrderDetail ON dbo.PPOrder.ID = dbo.PPOrderDetail.PPOrderID INNER JOIN
                         dbo.Type ON PPOrder.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPOrderDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPOrderDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPOrderDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPOrderDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.SAInvoice.ID, dbo.SAInvoiceDetail.ID AS DetailID, dbo.SAInvoice.TypeID, dbo.Type.TypeName, dbo.SAInvoice.No, NULL AS InwardNo, dbo.SAInvoice.OutwardNo AS OutwardNo, dbo.SAInvoice.Date, 
                         dbo.SAInvoice.PostedDate, dbo.SAInvoiceDetail.DebitAccount, dbo.SAInvoiceDetail.CreditAccount, SAInvoice.CurrencyID, dbo.SAInvoiceDetail.AmountOriginal, dbo.SAInvoiceDetail.Amount, NULL AS OrgPrice, 
                         dbo.SAInvoice.Reason, dbo.SAInvoiceDetail.Description, dbo.SAInvoiceDetail.CostSetID, CostSet.CostSetCode, dbo.SAInvoiceDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAInvoice.EmployeeID, dbo.SAInvoice.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, SAInvoiceDetail.VATAccount, SAInvoiceDetail.VATAmountOriginal, SAInvoiceDetail.VATAmount, SAInvoiceDetail.Quantity, SAInvoiceDetail.UnitPrice, dbo.SAInvoice.Recorded, 
                         dbo.SAInvoice.TotalAmountOriginal, dbo.SAInvoice.TotalAmount, dbo.SAInvoiceDetail.DiscountAmountOriginal, dbo.SAInvoiceDetail.DiscountAmount, dbo.SAInvoiceDetail.DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, dbo.SAInvoiceDetail.OWAmountOriginal AS OutwardAmountOriginal, 
                         dbo.SAInvoiceDetail.OWAmount AS OutwardAmount, 'SAInvoice' AS RefTable
FROM            dbo.SAInvoice INNER JOIN
                         dbo.SAInvoiceDetail ON dbo.SAInvoice.ID = dbo.SAInvoiceDetail.SAInvoiceID INNER JOIN
                         dbo.Type ON dbo.SAInvoice.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAInvoiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAInvoiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAInvoiceDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAInvoiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAInvoiceDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.SAReturn.ID, dbo.SAReturnDetail.ID AS DetailID, dbo.SAReturn.TypeID, dbo.Type.TypeName, dbo.SAReturn.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.SAReturn.Date, dbo.SAReturn.PostedDate, NULL 
                         AS DebitAccount, NULL AS CreditAccount, SAReturn.CurrencyID, dbo.SAReturnDetail.AmountOriginal, dbo.SAReturnDetail.Amount, NULL AS OrgPrice, dbo.SAReturn.Reason, dbo.SAReturnDetail.Description, 
                         dbo.SAReturnDetail.CostSetID, CostSet.CostSetCode, dbo.SAReturnDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAReturn.EmployeeID, dbo.SAReturn.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, SAReturnDetail.VATAccount, SAReturnDetail.VATAmountOriginal, SAReturnDetail.VATAmount, SAReturnDetail.Quantity, SAReturnDetail.UnitPrice, SAReturn.Recorded AS Posted, 
                         dbo.SAReturn.TotalAmountOriginal, dbo.SAReturn.TotalAmount, dbo.SAReturnDetail.DiscountAmountOriginal, dbo.SAReturnDetail.DiscountAmount, dbo.SAReturnDetail.DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'SAReturn' AS RefTable
FROM            dbo.SAReturn INNER JOIN
                         dbo.SAReturnDetail ON dbo.SAReturn.ID = dbo.SAReturnDetail.SAReturnID INNER JOIN
                         dbo.Type ON dbo.SAReturn.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAReturnDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAReturnDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAReturnDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAReturnDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAReturnDetail.ContractID
/*========================*/ UNION ALL
SELECT        dbo.SAOrder.ID, dbo.SAOrderDetail.ID AS DetailID, dbo.SAOrder.TypeID, dbo.Type.TypeName, dbo.SAOrder.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.SAOrder.Date, dbo.SAOrder.DeliveDate AS PostedDate, NULL 
                         AS DebitAccount, NULL AS CreditAccount, SAOrder.CurrencyID, dbo.SAOrderDetail.AmountOriginal, dbo.SAOrderDetail.Amount, NULL AS OrgPrice, dbo.SAOrder.Reason, dbo.SAOrderDetail.Description, 
                         dbo.SAOrderDetail.CostSetID, CostSet.CostSetCode, dbo.SAOrderDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAOrder.EmployeeID, dbo.SAOrder.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, SAOrderDetail.VATAccount, SAOrderDetail.VATAmountOriginal, SAOrderDetail.VATAmount, SAOrderDetail.Quantity, SAOrderDetail.UnitPrice, SAOrder.Exported AS Recorded, dbo.SAOrder.TotalAmountOriginal, 
                         dbo.SAOrder.TotalAmount, dbo.SAOrderDetail.DiscountAmountOriginal, dbo.SAOrderDetail.DiscountAmount, dbo.SAOrderDetail.DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'SAOrder' AS RefTable
FROM            dbo.SAOrder INNER JOIN
                         dbo.SAOrderDetail ON dbo.SAOrder.ID = dbo.SAOrderDetail.SAOrderID INNER JOIN
                         dbo.Type ON dbo.SAOrder.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAOrderDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAOrderDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAOrderDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAOrderDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAOrderDetail.ContractID
						 	 /*============TrungNQ== ghi tang ccdc==========*/ UNION ALL
SELECT        dbo.TIIncrement.ID, dbo.TIIncrementDetail.ID AS DetailID, dbo.TIIncrement.TypeID, dbo.Type.TypeName, dbo.TIIncrement.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.TIIncrement.Date, null AS PostedDate, NULL 
                         AS DebitAccount, NULL AS CreditAccount, TIIncrement.CurrencyID, dbo.TIIncrementDetail.AmountOriginal, dbo.TIIncrementDetail.Amount, NULL AS OrgPrice, dbo.TIIncrement.Reason, dbo.TIIncrementDetail.Description, 
                         dbo.TIIncrementDetail.CostSetID, CostSet.CostSetCode, dbo.TIIncrementDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, TIIncrement.EmployeeID, dbo.TIIncrement.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, null AS MaterialGoodsID, null as MaterialGoodsName, null AS RepositoryID, null as RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, TIIncrementDetail.VATAccount, TIIncrementDetail.VATAmountOriginal, TIIncrementDetail.VATAmount, TIIncrementDetail.Quantity, TIIncrementDetail.UnitPrice, TIIncrement.Exported AS Recorded, dbo.TIIncrement.TotalAmountOriginal, 
                         dbo.TIIncrement.TotalAmount, dbo.TIIncrementDetail.DiscountAmountOriginal, dbo.TIIncrementDetail.DiscountAmount, null as DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'TIIncrement' AS RefTable
FROM            dbo.TIIncrement INNER JOIN
                         dbo.TIIncrementDetail ON dbo.TIIncrement.ID = dbo.TIIncrementDetail.TIIncrementID INNER JOIN
                         dbo.Type ON dbo.TIIncrement.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON TIIncrementDetail.AccountingObjectID = AccountingObject.ID  LEFT JOIN
                         dbo.CostSet ON CostSet.ID = TIIncrementDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = TIIncrementDetail.ContractID
						 				 	 /*============TrungNQ== hop dong ban ==========*/ UNION ALL
SELECT        dbo.EMContract.ID, null AS DetailID, dbo.EMContract.TypeID, dbo.Type.TypeName, null as No, NULL AS InwardNo, NULL AS OutwardNo, null as Date, null AS PostedDate, NULL 
                         AS DebitAccount, NULL AS CreditAccount, EMContract.CurrencyID, dbo.EMContract.AmountOriginal, dbo.EMContract.Amount, NULL AS OrgPrice, dbo.EMContract.Reason, dbo.EMContract.Description, 
                         dbo.EMContract.CostSetID, null as CostSetCode, dbo.EMContract.ID as ContractID, dbo.EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, null as EmployeeID, dbo.EMContract.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, null AS MaterialGoodsID, null as MaterialGoodsName, null AS RepositoryID, null as RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, null as VATAccount, null as VATAmountOriginal, null as VATAmount, null as Quantity, null as UnitPrice, null AS Recorded, null as TotalAmountOriginal, 
                         null as TotalAmount, null as DiscountAmountOriginal, null as DiscountAmount, null as DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'EMContract' AS RefTable
FROM            dbo.EMContract  INNER JOIN
                         dbo.Type ON dbo.EMContract.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON EMContract.AccountingObjectID = AccountingObject.ID  LEFT JOIN
                         dbo.CostSet ON CostSet.ID = EMContract.CostSetID 
	 /*============TrungNQ== ghi tang tscd==========*/ UNION ALL
SELECT        dbo.FAIncrement.ID, dbo.FAIncrementDetail.ID AS DetailID, dbo.FAIncrement.TypeID, dbo.Type.TypeName, dbo.FAIncrement.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FAIncrement.Date, null AS PostedDate, NULL 
                         AS DebitAccount, NULL AS CreditAccount, FAIncrement.CurrencyID, dbo.FAIncrementDetail.AmountOriginal, dbo.FAIncrementDetail.Amount, NULL AS OrgPrice, dbo.FAIncrement.Reason, dbo.FAIncrementDetail.Description, 
                         dbo.FAIncrementDetail.CostSetID, CostSet.CostSetCode, dbo.FAIncrementDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, FAIncrement.EmployeeID, dbo.FAIncrement.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, null AS MaterialGoodsID, null as MaterialGoodsName, null AS RepositoryID, null as RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, FAIncrementDetail.VATAccount, FAIncrementDetail.VATAmountOriginal, FAIncrementDetail.VATAmount, FAIncrementDetail.Quantity, null as UnitPrice, FAIncrement.Exported AS Recorded, dbo.FAIncrement.TotalAmountOriginal, 
                         dbo.FAIncrement.TotalAmount, dbo.FAIncrementDetail.DiscountAmountOriginal, dbo.FAIncrementDetail.DiscountAmount, null as DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FAIncrement' AS RefTable
FROM            dbo.FAIncrement INNER JOIN
                         dbo.FAIncrementDetail ON dbo.FAIncrement.ID = dbo.FAIncrementDetail.FAIncrementID INNER JOIN
                         dbo.Type ON dbo.FAIncrement.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FAIncrementDetail.AccountingObjectID = AccountingObject.ID  LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FAIncrementDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FAIncrementDetail.ContractID


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*
*/
ALTER FUNCTION [dbo].[Func_SoDu]
      ( @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT 
      )
RETURNS @Result TABLE
      (
        AccountID UNIQUEIDENTIFIER ,
        DetailByAccountObject BIT ,
        AccountObjectType INT ,
		AccountObjectID NVARCHAR(50) ,
        AccountNumber NVARCHAR(10) ,
        AccountName NVARCHAR(512) ,
        AccountNameEnglish NVARCHAR(512) ,
        AccountCategoryKind INT ,
        IsParent BIT ,
        ParentID UNIQUEIDENTIFIER ,
        Grade INT ,
        AccountKind INT ,
        SortOrder INT ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
      )
AS
    BEGIN
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        DECLARE @MainCurrency NVARCHAR(3)
        SET @MainCurrency = 'VND'	
        /*ngày bắt đầu chương trình*/
        DECLARE @StartDate DATETIME

        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'                
           BEGIN
					
                 DECLARE @GeneralLedger TABLE
                         (
                           AccountNumber NVARCHAR(30) ,
                           AccountObjectID UNIQUEIDENTIFIER ,
                           BranchID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(3) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4) ,
                           OpenningDebitAmountOC DECIMAL(25, 4) ,
                           DebitAmountOC DECIMAL(25, 4) ,
                           CreditAmountOC DECIMAL(25, 4) ,
                           DebitAmountOCAccum DECIMAL(25, 4) ,
                           CreditAmountOCAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedger
                        SELECT  G.*
                        FROM    (
                                  SELECT    Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                                     ELSE 0
                                                END) AS OpenningDebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal - GL.CreditAmountOriginal
                                                     ELSE 0
                                                END) AS OpenningDebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOCAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOCAccum
                                  FROM      @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                  WHERE    GL.PostedDate <= @ToDate
                                  GROUP BY  Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID
                                ) G
			
                 DECLARE @Data TABLE
                         (
                           AccountNumber NVARCHAR(50) ,
						   AccountObjectID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(20) ,
                           SortOrder INT ,
                           OpeningDebitAmount DECIMAL(25, 4) ,
                           OpeningCreditAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,/*Nợ lũy kế*/
                           CreditAmountAccum DECIMAL(25, 4) ,/*có lũy kế*/
                           ClosingDebitAmount DECIMAL(25, 4) ,
                           ClosingCreditAmount DECIMAL(25, 4)
                         )
	
                 INSERT @Data
                        SELECT  F.AccountNumber ,
						        F.AccountObjectID,
                                '' ,
                                0 ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount > 0
                                                 ) THEN F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount < 0
                                                 ) THEN -1 * F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount ,
                                SUM(F.DebitAmount) AS DebitAmount ,
                                SUM(F.CreditAmount) AS CreditAmount ,
                                SUM(F.DebitAmountAccum) AS DebitAmountAccum ,
                                SUM(F.CreditAmountAccum) AS CreditAmountAccum ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                                 ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                                 ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.AccountNumber ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            GL.AccountObjectID AccountObjectID ,
                                            A.AccountGroupKind AccountCategoryKind ,
                                            GL.BranchID BranchID
                                  FROM      @GeneralLedger AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber = A.AccountNumber
                                  WHERE     GL.AccountNumber NOT LIKE '0%'
                                            AND A.IsParentNode = 0
                                  GROUP BY  A.AccountNumber ,
                                            GL.AccountObjectID,
                                            A.AccountGroupKind ,
                                            GL.BranchID
                                  HAVING    SUM(OpenningDebitAmount) <> 0
                                            OR SUM(DebitAmount) <> 0
                                            OR SUM(CreditAmount) <> 0
                                ) AS F
                        GROUP BY F.AccountNumber ,
                                F.AccountCategoryKind,
								F.AccountObjectID
                 OPTION ( RECOMPILE )		         					    
                 INSERT @Result
                        SELECT  A.ID ,
						        null,
								null,
								D.AccountObjectID,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                0 AccountKind,
                                D.SortOrder ,
                                SUM(D.OpeningDebitAmount) ,
                                SUM(D.OpeningCreditAmount) ,
                                SUM(D.DebitAmount) ,
                                SUM(D.CreditAmount) ,
                                SUM(D.DebitAmountAccum) ,
                                SUM(D.CreditAmountAccum) ,
                                SUM(D.ClosingDebitAmount) ,
                                SUM(D.ClosingCreditAmount)
                        FROM    @Data D
                        INNER JOIN dbo.Account A ON D.AccountNumber LIKE A.AccountNumber + '%'
                        WHERE   A.Grade <= @MaxAccountGrade
                        GROUP BY A.ID ,
                                A.AccountNumber ,
								D.AccountObjectID,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                D.SortOrder
                        HAVING  SUM(D.OpeningDebitAmount) <> 0
                                OR SUM(D.OpeningCreditAmount) <> 0
                                OR SUM(D.DebitAmount) <> 0
                                OR SUM(D.CreditAmount) <> 0
                                OR SUM(D.ClosingDebitAmount) <> 0
                                OR SUM(D.ClosingCreditAmount) <> 0
                 OPTION ( RECOMPILE )
           END
        RETURN
    END



GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER FUNCTION [dbo].[Func_GetBalanceAccountF01_v1]
      (
        @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT ,
        @IsBalanceBothSide BIT
      )
RETURNS @Result TABLE
      (
        AccountID UNIQUEIDENTIFIER ,
        DetailByAccountObject BIT ,
        AccountObjectType INT ,
        AccountNumber NVARCHAR(50) ,
        AccountName NVARCHAR(512) ,
        AccountNameEnglish NVARCHAR(512) ,
        AccountCategoryKind INT ,
        IsParent BIT ,
        ParentID UNIQUEIDENTIFIER ,
        Grade INT ,
        AccountKind INT ,
        SortOrder INT ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
      )
AS
    BEGIN
	   DECLARE @MainCurrency NVARCHAR(3)
        SET @MainCurrency = 'VND'	
        /*ngày bắt đầu chương trình*/
        DECLARE @StartDate DATETIME
        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'
        IF @IsBalanceBothSide = 1
           BEGIN
                 DECLARE @GeneralLedger TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           AccountObjectID UNIQUEIDENTIFIER ,
                           BranchID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(3) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4) ,
                           OpenningDebitAmountOC DECIMAL(25, 4) ,
                           DebitAmountOC DECIMAL(25, 4) ,
                           CreditAmountOC DECIMAL(25, 4) ,
                           DebitAmountOCAccum DECIMAL(25, 4) ,
                           CreditAmountOCAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedger
                        SELECT  G.*
                        FROM    (
                                  SELECT    Account ,
                                            null as AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                                     ELSE 0
                                                END) AS OpenningDebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal - GL.CreditAmountOriginal
                                                     ELSE 0
                                                END) AS OpenningDebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOCAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOCAccum
                                  FROM      dbo.GeneralLedger AS GL
                                  WHERE    GL.PostedDate <= @ToDate
                                  GROUP BY  Account ,
                                            GL.BranchID ,
                                            GL.CurrencyID
                                ) G
			
                 DECLARE @Data TABLE
                         (
                           AccountNumber NVARCHAR(50) ,
						   AccountObjectID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(20) ,
                           SortOrder INT ,
                           OpeningDebitAmount DECIMAL(25, 4) ,
                           OpeningCreditAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,/*Nợ lũy kế*/
                           CreditAmountAccum DECIMAL(25, 4) ,/*có lũy kế*/
                           ClosingDebitAmount DECIMAL(25, 4) ,
                           ClosingCreditAmount DECIMAL(25, 4)
                         )
	
                 INSERT @Data
                        SELECT  F.AccountNumber ,
						        F.AccountObjectID,
                                '' ,
                                0 ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount > 0
                                                 ) THEN F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount < 0
                                                 ) THEN -1 * F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount ,
                                SUM(F.DebitAmount) AS DebitAmount ,
                                SUM(F.CreditAmount) AS CreditAmount ,
                                SUM(F.DebitAmountAccum) AS DebitAmountAccum ,
                                SUM(F.CreditAmountAccum) AS CreditAmountAccum ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                                 ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                                 ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.AccountNumber ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            GL.AccountObjectID AccountObjectID ,
                                            A.AccountGroupKind AccountCategoryKind ,
                                            GL.BranchID BranchID
                                  FROM      @GeneralLedger AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber = A.AccountNumber
                                  WHERE     GL.AccountNumber NOT LIKE '0%'
                                  GROUP BY  A.AccountNumber ,
                                            GL.AccountObjectID,
                                            A.AccountGroupKind ,
                                            GL.BranchID
                                  HAVING    SUM(OpenningDebitAmount) <> 0
                                            OR SUM(DebitAmount) <> 0
                                            OR SUM(CreditAmount) <> 0
                                ) AS F
                        GROUP BY F.AccountNumber ,
                                F.AccountCategoryKind,
								F.AccountObjectID
                 OPTION ( RECOMPILE )		         					    
                 INSERT @Result
                        SELECT  A.ID ,
						        null,
								null,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                0 AccountKind,
                                D.SortOrder ,
                                SUM(D.OpeningDebitAmount) ,
                                SUM(D.OpeningCreditAmount) ,
                                SUM(D.DebitAmount) ,
                                SUM(D.CreditAmount) ,
                                SUM(D.DebitAmountAccum) ,
                                SUM(D.CreditAmountAccum) ,
                                SUM(D.ClosingDebitAmount) ,
                                SUM(D.ClosingCreditAmount)
                        FROM    @Data D
                        INNER JOIN dbo.Account A ON D.AccountNumber LIKE A.AccountNumber + '%'
                        WHERE   A.Grade <= @MaxAccountGrade
                        GROUP BY A.ID ,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                D.SortOrder
                        HAVING  SUM(D.OpeningDebitAmount) <> 0
                                OR SUM(D.OpeningCreditAmount) <> 0
                                OR SUM(D.DebitAmount) <> 0
                                OR SUM(D.CreditAmount) <> 0
                                OR SUM(D.ClosingDebitAmount) <> 0
                                OR SUM(D.ClosingCreditAmount) <> 0
                 OPTION ( RECOMPILE )
           END
        ELSE
           BEGIN
                 DECLARE @GeneralLedgerP1 TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedgerP1
                        SELECT  Account ,
                                SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmountAccum ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmountAccum
                        FROM    dbo.GeneralLedger AS GL
                        WHERE   GL.PostedDate <= @ToDate
                        GROUP BY Account
                 INSERT @Result
                        SELECT  A.ID ,
                                null DetailByAccountObject ,
                                null AccountObjectType,
                                A.AccountNumber ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade AS Grade ,
                                F.AccountKind,
                                F.SortOrder ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount > 0
                                               ) THEN F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount < 0
                                               ) THEN -1 * F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount ,
                                ( F.DebitAmount ) AS DebitAmount ,
                                ( F.CreditAmount ) AS CreditAmount ,
                                ( F.DebitAmountAccum ) AS DebitAmountAccum ,
                                ( F.CreditAmountAccum ) AS CreditAmountAccum ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                               ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                               ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.ID ,
                                            null DetailByAccountObject,
                                            null AccountObjectType,
                                            A.AccountNumber ,
                                            '' AS CurrencyID ,
                                            0 AS SortOrder ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            0 AS AccountKind
                                  FROM      @GeneralLedgerP1 AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber LIKE A.AccountNumber + '%'
                                  WHERE     A.Grade <= @MaxAccountGrade
                                            AND (
                                                  OpenningDebitAmount <> 0
                                                  OR DebitAmount <> 0
                                                  OR CreditAmount <> 0
                                                )
                                  GROUP BY  A.ID ,
                                            A.AccountNumber      
                                ) AS F
                        INNER JOIN Account A ON F.AccountNumber = A.AccountNumber
                 OPTION ( RECOMPILE )      
           END
        RETURN
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER FUNCTION [dbo].[Func_GetBalanceAccountF01]
      (
        @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT ,
        @IsBalanceBothSide BIT
      )
RETURNS @Result TABLE
      (
        AccountID UNIQUEIDENTIFIER ,
        DetailByAccountObject BIT ,
        AccountObjectType INT ,
        AccountNumber NVARCHAR(50) ,
        AccountName NVARCHAR(512) ,
        AccountNameEnglish NVARCHAR(512) ,
        AccountCategoryKind INT ,
        IsParent BIT ,
        ParentID UNIQUEIDENTIFIER ,
        Grade INT ,
        AccountKind INT ,
        SortOrder INT ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
      )
AS
    BEGIN
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/
	   DECLARE @MainCurrency NVARCHAR(3)
        SET @MainCurrency = 'VND'	
        /*ngày bắt đầu chương trình*/
        DECLARE @StartDate DATETIME
        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'
        IF @IsBalanceBothSide = 1
           BEGIN
                 DECLARE @GeneralLedger TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           AccountObjectID UNIQUEIDENTIFIER ,
                           BranchID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(3) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4) ,
                           OpenningDebitAmountOC DECIMAL(25, 4) ,
                           DebitAmountOC DECIMAL(25, 4) ,
                           CreditAmountOC DECIMAL(25, 4) ,
                           DebitAmountOCAccum DECIMAL(25, 4) ,
                           CreditAmountOCAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedger
                        SELECT  G.*
                        FROM    (
                                  SELECT    Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                                     ELSE 0
                                                END) AS OpenningDebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal - GL.CreditAmountOriginal
                                                     ELSE 0
                                                END) AS OpenningDebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOCAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOCAccum
                                  FROM      @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                  WHERE    GL.PostedDate <= @ToDate
                                  GROUP BY  Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID
                                ) G
			
                 DECLARE @Data TABLE
                         (
                           AccountNumber NVARCHAR(50) ,
						   AccountObjectID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(20) ,
                           SortOrder INT ,
                           OpeningDebitAmount DECIMAL(25, 4) ,
                           OpeningCreditAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,/*Nợ lũy kế*/
                           CreditAmountAccum DECIMAL(25, 4) ,/*có lũy kế*/
                           ClosingDebitAmount DECIMAL(25, 4) ,
                           ClosingCreditAmount DECIMAL(25, 4)
                         )
	
                 INSERT @Data
                        SELECT  F.AccountNumber ,
						        null,
                                '' ,
                                0 ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount > 0
                                                 ) THEN F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount < 0
                                                 ) THEN -1 * F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount ,
                                SUM(F.DebitAmount) AS DebitAmount ,
                                SUM(F.CreditAmount) AS CreditAmount ,
                                SUM(F.DebitAmountAccum) AS DebitAmountAccum ,
                                SUM(F.CreditAmountAccum) AS CreditAmountAccum ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                                 ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                                 ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.AccountNumber ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            CASE A.DetailByAccountObject
                                                                  WHEN 0 THEN NULL
                                                                  ELSE GL.AccountObjectID
                                                                END AS AccountObjectID ,
                                            A.AccountGroupKind AccountCategoryKind ,
                                            GL.BranchID BranchID
                                  FROM      @GeneralLedger AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber = A.AccountNumber
                                  WHERE     GL.AccountNumber NOT LIKE '0%'
                                            AND A.IsParentNode = 0
                                  GROUP BY  A.AccountNumber ,
                                            A.AccountGroupKind ,
                                            GL.BranchID,
											CASE A.DetailByAccountObject
                                                                  WHEN 0 THEN NULL
                                                                  ELSE GL.AccountObjectID
                                                                END
                                  HAVING    SUM(OpenningDebitAmount) <> 0
                                            OR SUM(DebitAmount) <> 0
                                            OR SUM(CreditAmount) <> 0
                                ) AS F
                        GROUP BY F.AccountNumber ,
                                F.AccountCategoryKind,
								F.AccountObjectID
                 OPTION ( RECOMPILE )		         					    
                 INSERT @Result
                        SELECT  A.ID ,
						        null,
								null,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                0 AccountKind,
                                D.SortOrder ,
                                SUM(D.OpeningDebitAmount) ,
                                SUM(D.OpeningCreditAmount) ,
                                SUM(D.DebitAmount) ,
                                SUM(D.CreditAmount) ,
                                SUM(D.DebitAmountAccum) ,
                                SUM(D.CreditAmountAccum) ,
                                SUM(D.ClosingDebitAmount) ,
                                SUM(D.ClosingCreditAmount)
                        FROM    @Data D
                        INNER JOIN dbo.Account A ON D.AccountNumber LIKE A.AccountNumber + '%'
                        WHERE   A.Grade <= @MaxAccountGrade
                        GROUP BY A.ID ,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                D.SortOrder
                        HAVING  SUM(D.OpeningDebitAmount) <> 0
                                OR SUM(D.OpeningCreditAmount) <> 0
                                OR SUM(D.DebitAmount) <> 0
                                OR SUM(D.CreditAmount) <> 0
                                OR SUM(D.ClosingDebitAmount) <> 0
                                OR SUM(D.ClosingCreditAmount) <> 0
                 OPTION ( RECOMPILE )
           END
        ELSE
           BEGIN
                 DECLARE @GeneralLedgerP1 TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedgerP1
                        SELECT  Account ,
                                SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmountAccum ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmountAccum
                        FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                        WHERE   GL.PostedDate <= @ToDate
                        GROUP BY Account
                 INSERT @Result
                        SELECT  A.ID ,
                                null DetailByAccountObject ,
                                null AccountObjectType,
                                A.AccountNumber ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade AS Grade ,
                                F.AccountKind,
                                F.SortOrder ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount > 0
                                               ) THEN F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount < 0
                                               ) THEN -1 * F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount ,
                                ( F.DebitAmount ) AS DebitAmount ,
                                ( F.CreditAmount ) AS CreditAmount ,
                                ( F.DebitAmountAccum ) AS DebitAmountAccum ,
                                ( F.CreditAmountAccum ) AS CreditAmountAccum ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                               ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                               ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.ID ,
                                            null DetailByAccountObject,
                                            null AccountObjectType,
                                            A.AccountNumber ,
                                            '' AS CurrencyID ,
                                            0 AS SortOrder ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            0 AS AccountKind
                                  FROM      @GeneralLedgerP1 AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber LIKE A.AccountNumber + '%'
                                  WHERE     A.Grade <= @MaxAccountGrade
                                            AND (
                                                  OpenningDebitAmount <> 0
                                                  OR DebitAmount <> 0
                                                  OR CreditAmount <> 0
                                                )
                                  GROUP BY  A.ID ,
                                            A.AccountNumber      
                                ) AS F
                        INNER JOIN Account A ON F.AccountNumber = A.AccountNumber
                 OPTION ( RECOMPILE )      
           END
        RETURN
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*

*/
ALTER FUNCTION [dbo].[Func_GetB01_DN]
    (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT,
      @IsB01bDNN BIT ,
      @isPrintByYear BIT,
      @PrevFromDate DATETIME,
      @PrevToDate DATETIME
    )
RETURNS @Result TABLE
    (
      ItemID UNIQUEIDENTIFIER ,
      ItemCode NVARCHAR(25) ,
      ItemName NVARCHAR(512) ,
      ItemNameEnglish NVARCHAR(512) ,
      ItemIndex INT ,
      Description NVARCHAR(512) ,
      FormulaType INT ,
      FormulaFrontEnd NVARCHAR(MAX) ,
      Formula XML ,
      Hidden BIT ,
      IsBold BIT ,
      IsItalic BIT ,
      Category INT ,
      SortOrder INT
      ,
      Amount DECIMAL(25, 4) ,
      PrevAmount DECIMAL(25, 4)
    )

    BEGIN 
            DECLARE @AccountingSystem INT	
            SET @AccountingSystem = 48 
    
            DECLARE @SubAccountSystem INT
            SELECT  @SubAccountSystem = 133
	
            DECLARE @ReportID NVARCHAR(100)
            SET @ReportID = '1'

            IF @AccountingSystem = 48
                AND @SubAccountSystem = 133
                AND @IsB01bDNN = 0
                BEGIN

                    SET @ReportID = '7'	
                END

            DECLARE @ItemForeignCurrency UNIQUEIDENTIFIER
            DECLARE @ItemIndex INT
            DECLARE @MainCurrency NVARCHAR(3)
            IF @AccountingSystem = 48
                AND @SubAccountSystem <> 133
                SET @ItemForeignCurrency = '6D6BA7EB-E60A-46ED-A556-E674709F5466'
            ELSE
                SET @ItemForeignCurrency = '00000000-0000-0000-0000-000000000000'	
	
            SET @ItemIndex = ( SELECT TOP 1
                                        ItemIndex
                               FROM     FRTemplate
                               WHERE    ItemID = @ItemForeignCurrency
                             )
            SET @MainCurrency = 'VND'
            DECLARE @tblItem TABLE
                (
                  ItemID UNIQUEIDENTIFIER ,
                  ItemIndex INT ,
                  ItemName NVARCHAR(512) ,
                  OperationSign INT ,
                  OperandString NVARCHAR(512) ,
                  AccountNumberPercent NVARCHAR(25) ,
                  AccountNumber NVARCHAR(25) ,
                  CorrespondingAccountNumber VARCHAR(25) ,
                  IsDetailByAO INT ,
                  AccountKind INT
                )
	
            INSERT  @tblItem
                    SELECT  ItemID ,
                            ItemIndex ,
                            ItemName ,
                            x.r.value('@OperationSign', 'INT') ,
                            RTRIM(LTRIM(x.r.value('@OperandString',
                                                  'nvarchar(512)'))) ,
                            x.r.value('@AccountNumber', 'nvarchar(25)') + '%' ,
                            x.r.value('@AccountNumber', 'nvarchar(25)') ,
                            CASE WHEN x.r.value('@CorrespondingAccountNumber',
                                                'nvarchar(25)') <> ''
                                 THEN x.r.value('@CorrespondingAccountNumber',
                                                'nvarchar(25)') + '%'
                                 ELSE ''
                            END ,
                            CASE WHEN x.r.value('@OperandString',
                                                'nvarchar(512)') LIKE '%ChitietTheoTKvaDoituong'
                                 THEN 1
                                 ELSE CASE WHEN x.r.value('@OperandString',
                                                          'nvarchar(512)') LIKE '%ChitietTheoTK'
                                           THEN 2
                                           ELSE 0
                                      END
                            END ,
                            NULL
                    FROM    dbo.FRTemplate
                            CROSS APPLY Formula.nodes('/root/DetailFormula')
                            AS x ( r )
                    WHERE   AccountingSystem = @AccountingSystem
                            AND ReportID = @ReportID
                            AND FormulaType = 0
                            AND Formula IS NOT NULL
                            AND ItemID <> @ItemForeignCurrency
	
            UPDATE  @tblItem
            SET     AccountKind = A.AccountGroupKind
            FROM    dbo.Account A
            WHERE   A.AccountNumber = [@tblItem].AccountNumber
		
            DECLARE @AccountBalance TABLE
                (
                  AccountNumber NVARCHAR(20) ,
                  AccountCategoryKind INT ,
                  IsDetailByAO INT 
	            )
	 
	 /*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

            INSERT  @AccountBalance
                    SELECT 
		DISTINCT            A.AccountNumber ,
                            A.AccountGroupKind ,
                            B.IsDetailByAO
                    FROM    dbo.Account A
                            INNER JOIN @tblItem B ON A.AccountNumber LIKE B.AccountNumberPercent
	
            DECLARE @Balance TABLE
                (
                  AccountNumber NVARCHAR(20) ,
                  AccountCategoryKind INT ,
                  AccountObjectID UNIQUEIDENTIFIER ,
                  IsDetailByAO INT ,
                  OpeningDebit DECIMAL(25, 4) ,
                  OpeningCredit DECIMAL(25, 4) ,
                  ClosingDebit DECIMAL(25, 4) ,
                  ClosingCredit DECIMAL(25, 4) ,
                  BranchID UNIQUEIDENTIFIER
                )
            DECLARE @GeneralLedger TABLE
                (
                  AccountNumber NVARCHAR(20) ,
                  AccountObjectID UNIQUEIDENTIFIER ,
                  BranchID UNIQUEIDENTIFIER ,
                  IsOPN BIT ,
                  DebitAmount DECIMAL(25, 4) ,
                  CreditAmount DECIMAL(25, 4)
                )
	
            INSERT  INTO @GeneralLedger
                    ( AccountNumber ,
                      AccountObjectID ,
                      BranchID ,
                      IsOPN ,
                      DebitAmount ,
                      CreditAmount
	                )
                    SELECT  GL.Account ,
                            GL.AccountingObjectID ,
                            GL.BranchID ,
                            CASE WHEN GL.PostedDate < @FromDate THEN 1
                                 ELSE 0
                            END ,
                            SUM(DebitAmount) AS DebitAmount ,
                            SUM(CreditAmount) AS CreditAmount
                    FROM    @tbDataGL GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                    WHERE   PostedDate <= @ToDate
                    GROUP BY GL.Account ,
                            GL.AccountingObjectID ,
                            GL.BranchID ,
                            CASE WHEN GL.PostedDate < @FromDate THEN 1
                                 ELSE 0
                            END
	
	
            DECLARE @StartDate DATETIME
			SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'
	 
            DECLARE @GeneralLedger421 TABLE
                (
                  OperandString NVARCHAR(255) ,
                  ClosingAmount DECIMAL(25, 4) ,
                  OpeningAmount DECIMAL(25, 4)
                )
	
	
	
	
	/* 
	 

Đối với báo cáo kỳ khác năm:
*Cuối kỳ: (Dư Có – Dư Nợ) TK 4211 trên sổ cái tính đến Từ ngày -1 + (Dư Có – Dư Nợ) TK 4212 trên sổ cái tính đến Từ ngày -1 
* Đầu kỳ: 
(Dư Có – Dư Nợ) TK 4211 trên sổ cái tính đến từ ngày -1 của kỳ trước liền kề+ (Dư Có – Dư Nợ) TK 4212 trên sổ cái tính đến Từ ngày -1 của kỳ trước liền kề. 
→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, 
VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10) thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4211 – Dư Nợ TK 4211 trên Sổ cái tính đến Từ ngày -1 

	 */
	 /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
            INSERT  INTO @GeneralLedger421
                    ( OperandString ,
                      ClosingAmount ,
                      OpeningAmount
	                )
                    SELECT  N'Solieuchitieu421a_Ky_khac_nam' AS AccountNumber ,
                            ISNULL( SUM(CASE WHEN GL.Account LIKE '4211%'
                                          AND GL.PostedDate < @FromDate
                                     THEN CreditAmount - DebitAmount
                                     ELSE 0
                                END),0)
                         
                            + ISNULL( SUM(CASE WHEN GL.Account LIKE '4212%'
                                            AND GL.PostedDate < @FromDate
                                       THEN CreditAmount -DebitAmount
                                       ELSE 0
                                  END),0)
                           AS ClosingAmount ,
                                  
                            ISNULL( SUM(
									CASE WHEN DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0 ELSE 
										CASE WHEN GL.Account LIKE '4211%'
													  AND GL.PostedDate < @PrevFromDate
												 THEN CreditAmount -DebitAmount
												 ELSE 0
											END
									end		
                                ),0)
                         
                            + SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0
                                       ELSE CASE WHEN GL.Account LIKE '4212%'
                                                      AND GL.PostedDate < @PrevFromDate
                                                 THEN CreditAmount -DebitAmount
                                                 ELSE 0
                                            END
                                  END)
                     
                        +   ISNULL(SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate) 
									  AND GL.Account LIKE '4211%'
												 THEN CASE WHEN GL.PostedDate < @FromDate then CreditAmount - DebitAmount ELSE 0 end
									 ELSE 0
								END)
								,0) AS OpeningAmount                                  
                                  
                    FROM    @tbDataGL GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                    WHERE   PostedDate <= @ToDate
                            AND gl.Account LIKE '421%'
	
	/*
	

Đối với báo cáo kỳ khác năm:
* Cuối kỳ: PS Có TK 4212 trên sổ cái trong kỳ báo cáo (không bao gồm ĐU N4211/Có TK 4212) - PSN TK 4212 trên sổ cái trong kỳ báo cáo (không bao gồm ĐU N4212/Có TK 4211)
* Đầu kỳ: 
PS Có TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4211/Có TK 4212) – PS Nợ TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4212/Có TK 4211)
→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10) thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4212 – Dư Nợ TK 4212 trên Sổ cái tính đến Từ ngày -1 

	*/
	/*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
	
            INSERT  INTO @GeneralLedger421
                    ( OperandString ,
                      ClosingAmount ,
                      OpeningAmount
	                )
                    SELECT  N'Solieuchitieu421b_Ky_khac_nam' AS AccountNumber ,
                           ISNULL( SUM(CASE WHEN GL.Account LIKE '4212%'
                                          AND GL.PostedDate BETWEEN @fromdate AND @ToDate
                                     THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                 THEN 0
                                                 ELSE CreditAmount
                                            END )
                                     ELSE 0
                                END),0)
                            - ISNULL( SUM(CASE WHEN GL.Account LIKE '4212%'
                                            AND GL.PostedDate BETWEEN @Fromdate AND @ToDate
                                       THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                   THEN 0
                                                   ELSE DebitAmount
                                              END )
                                       ELSE 0
                                  END),0) AS ClosingAmount ,
		 /** Đầu kỳ: 
PS Có TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4211/Có TK 4212) – PS Nợ TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4212/Có TK 4211)
→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10)
 thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4212 – Dư Nợ TK 4212 trên Sổ cái tính đến Từ ngày -1 
*/
                            ISNULL( SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0
                                     ELSE ( CASE WHEN GL.Account LIKE '4212%'
                                                      AND GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                                 THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                             THEN 0
                                                             ELSE CreditAmount
                                                        END )
                                                 ELSE 0
                                            END )
                                END),0)
                            - ISNULL(  SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0
                                       ELSE CASE WHEN GL.Account LIKE '4212%'
                                                      AND GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                                 THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                             THEN 0
                                                             ELSE DebitAmount
                                                        END )
                                                 ELSE 0
                                            END
                                  END),0)
                            + /*nếu không  là kỳ đầu tiên
		→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, 
		VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10) thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4212 – Dư Nợ TK 4212 trên Sổ cái tính đến Từ ngày -1 
		*/
			ISNULL(SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate) 
                          AND GL.Account LIKE '4212%'
                     THEN CASE WHEN GL.PostedDate < @FromDate then CreditAmount - DebitAmount ELSE 0 end
                     ELSE 0
                END),0) AS OpeningAmount
                    FROM    @tbDataGL GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                    WHERE   PostedDate <= @ToDate
                            AND gl.Account LIKE '421%'
	
            INSERT  INTO @Balance
                    SELECT  GL.AccountNumber ,
                            A.AccountCategoryKind ,
                            CASE WHEN A.IsDetailByAO = 1
                                 THEN GL.AccountObjectID
                                 ELSE NULL
                            END AS AccountObjectID ,
                            A.IsDetailByAO ,
                            SUM(CASE WHEN GL.IsOPN = 1
                                     THEN DebitAmount - CreditAmount
                                     ELSE 0
                                END) OpeningDebit ,
                            0 ,
                            SUM(DebitAmount - CreditAmount) ClosingDebit ,
                            0
                            ,
                            CASE WHEN A.IsDetailByAO = 1
                                      AND @IsSimilarBranch = 0
                                 THEN GL.BranchID
                                 ELSE NULL
                            END AS BranchID
                    FROM    @GeneralLedger GL
                            INNER JOIN @AccountBalance A ON GL.AccountNumber = A.AccountNumber
                    GROUP BY GL.AccountNumber ,
                            A.AccountCategoryKind ,
                            CASE WHEN A.IsDetailByAO = 1
                                 THEN GL.AccountObjectID
                                 ELSE NULL
                            END ,
                            A.IsDetailByAO
                            ,
                            CASE WHEN A.IsDetailByAO = 1
                                      AND @IsSimilarBranch = 0
                                 THEN GL.BranchID
                                 ELSE NULL
                            END
            OPTION  ( RECOMPILE )
            UPDATE  @Balance
            SET     OpeningCredit = -OpeningDebit ,
                    OpeningDebit = 0
            WHERE   AccountCategoryKind = 1
                    OR ( AccountCategoryKind NOT IN ( 0, 1 )
                         AND OpeningDebit < 0
                       )
			
            UPDATE  @Balance
            SET     ClosingCredit = -ClosingDebit ,
                    ClosingDebit = 0
            WHERE   AccountCategoryKind = 1
                    OR ( AccountCategoryKind NOT IN ( 0, 1 )
                         AND ClosingDebit < 0
                       )	
	
            DECLARE @tblMasterDetail TABLE
                (
                  ItemID UNIQUEIDENTIFIER ,
                  DetailItemID UNIQUEIDENTIFIER ,
                  OperationSign INT ,
                  Grade INT ,
                  OpeningAmount DECIMAL(25, 4) ,
                  ClosingAmount DECIMAL(25, 4)
                )	
	
	
            INSERT  INTO @tblMasterDetail
                    SELECT  I.ItemID ,
                            NULL ,
                            1 ,
                            -1
                            ,
                            SUM(( CASE I.OperandString
                                    WHEN 'DUNO'
                                    THEN
                                         CASE I.AccountKind
                                           WHEN 1 THEN 0
                                           WHEN 0
                                           THEN I.OpeningDebit
                                                - I.OpeningCredit
                                           ELSE CASE WHEN I.OpeningDebit
                                                          - I.OpeningCredit > 0
                                                     THEN I.OpeningDebit
                                                          - I.OpeningCredit
                                                     ELSE 0
                                                END
                                         END
                                         /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUNO_ky_nam'
                                    THEN 
                                         CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.AccountKind
                                                     WHEN 1 THEN 0
                                                     WHEN 0
                                                     THEN I.OpeningDebit
                                                          - I.OpeningCredit
                                                     ELSE CASE
                                                              WHEN I.OpeningDebit
                                                              - I.OpeningCredit > 0
                                                              THEN I.OpeningDebit
                                                              - I.OpeningCredit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'DUCO'
                                    THEN CASE I.AccountKind
                                           WHEN 0 THEN 0
                                           WHEN 1
                                           THEN I.OpeningCredit
                                                - I.OpeningDebit
                                           ELSE CASE WHEN I.OpeningCredit
                                                          - I.OpeningDebit > 0
                                                     THEN I.OpeningCredit
                                                          - I.OpeningDebit
                                                     ELSE 0
                                                END
                                         END
                                         /* Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUCO_ky_nam'
                                    THEN CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.AccountKind
                                                     WHEN 0 THEN 0
                                                     WHEN 1
                                                     THEN I.OpeningCredit
                                                          - I.OpeningDebit
                                                     ELSE CASE
                                                              WHEN I.OpeningCredit
                                                              - I.OpeningDebit > 0
                                                              THEN I.OpeningCredit
                                                              - I.OpeningDebit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421a_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.OpeningAmount
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421b_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.OpeningAmount
                                              ELSE 0
                                         END
                                         /* - Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    ELSE CASE WHEN LEFT(I.OperandString, 4) = 'DUNO'
                                              THEN I.OpeningDebit
                                              ELSE I.OpeningCredit
                                         END
                                  END ) * I.OperationSign) AS OpeningAmount ,
                            SUM(( CASE I.OperandString
                                    WHEN 'DUNO'
                                    THEN CASE I.Accountkind
                                           WHEN 1 THEN 0
                                           WHEN 0
                                           THEN I.ClosingDebit
                                                - I.ClosingCredit
                                           ELSE CASE WHEN I.ClosingDebit
                                                          - I.ClosingCredit > 0
                                                     THEN I.ClosingDebit
                                                          - I.ClosingCredit
                                                     ELSE 0
                                                END
                                         END
                                         /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUNO_ky_nam'
                                    THEN CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.Accountkind
                                                     WHEN 1 THEN 0
                                                     WHEN 0
                                                     THEN I.ClosingDebit
                                                          - I.ClosingCredit
                                                     ELSE CASE
                                                              WHEN I.ClosingDebit
                                                              - I.ClosingCredit > 0
                                                              THEN I.ClosingDebit
                                                              - I.ClosingCredit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'DUCO'
                                    THEN CASE I.AccountKind
                                           WHEN 0 THEN 0
                                           WHEN 1
                                           THEN I.ClosingCredit
                                                - I.ClosingDebit
                                           ELSE CASE WHEN I.ClosingCredit
                                                          - I.ClosingDebit > 0
                                                     THEN I.ClosingCredit
                                                          - I.ClosingDebit
                                                     ELSE 0
                                                END
                                         END
                                         /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUCO_ky_nam'
                                    THEN CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.AccountKind
                                                     WHEN 0 THEN 0
                                                     WHEN 1
                                                     THEN I.ClosingCredit
                                                          - I.ClosingDebit
                                                     ELSE CASE
                                                              WHEN I.ClosingCredit
                                                              - I.ClosingDebit > 0
                                                              THEN I.ClosingCredit
                                                              - I.ClosingDebit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421a_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.ClosingAmount
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421b_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.ClosingAmount
                                              ELSE 0
                                         END
                                         /* - Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    ELSE CASE WHEN LEFT(I.OperandString, 4) = 'DUNO'
                                              THEN I.ClosingDebit
                                              ELSE I.ClosingCredit
                                         END
                                  END ) * I.OperationSign) AS ClosingAmount
                    FROM    ( SELECT    I.ItemID ,
                                        I.OperandString ,
                                        I.OperationSign ,
                                        I.AccountNumber ,
                                        I.AccountKind ,
                                        SUM(B.OpeningDebit) AS OpeningDebit ,
                                        SUM(B.OpeningCredit) AS OpeningCredit ,
                                        SUM(B.ClosingDebit) AS ClosingDebit ,
                                        SUM(B.ClosingCredit) AS ClosingCredit
                              FROM      @tblItem I
                                        INNER JOIN @Balance B ON B.AccountNumber LIKE I.AccountNumberPercent
                                                              AND B.IsDetailByAO = I.IsDetailByAO
                              GROUP BY  I.ItemID ,
                                        I.OperandString ,
                                        I.OperationSign ,
                                        I.AccountNumber ,
                                        I.AccountKind
                            ) I
                            /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                            LEFT JOIN @GeneralLedger421 C ON I.OperandString = C.OperandString
                    GROUP BY I.ItemID			
	
            INSERT  @tblMasterDetail
                    SELECT  ItemID ,
                            x.r.value('@ItemID', 'NVARCHAR(100)') ,
                            x.r.value('@OperationSign', 'INT') ,
                            0 ,
                            0.0 ,
                            0.0
                    FROM    dbo.FRTemplate
                            CROSS APPLY Formula.nodes('/root/MasterFormula')
                            AS x ( r )
                    WHERE   AccountingSystem = @AccountingSystem
                            AND ReportID = @ReportID
                            AND FormulaType = 1
                            AND Formula IS NOT NULL 
	
	;
            WITH    V ( ItemID, DetailItemID, OpeningAmount, ClosingAmount, OperationSign )
                      AS ( SELECT   ItemID ,
                                    DetailItemID ,
                                    OpeningAmount ,
                                    ClosingAmount ,
                                    OperationSign
                           FROM     @tblMasterDetail
                           WHERE    Grade = -1
                           UNION ALL
                           SELECT   B.ItemID ,
                                    B.DetailItemID ,
                                    V.OpeningAmount ,
                                    V.ClosingAmount ,
                                    B.OperationSign * V.OperationSign AS OperationSign
                           FROM     @tblMasterDetail B ,
                                    V
                           WHERE    B.DetailItemID = V.ItemID
                         )
	INSERT  @Result
            SELECT  FR.ItemID ,
                    FR.ItemCode ,
                    FR.ItemName ,
                    FR.ItemNameEnglish ,
                    FR.ItemIndex ,
                    FR.Description ,
                    FR.FormulaType ,
                    CASE WHEN FR.FormulaType = 1 THEN FR.FormulaFrontEnd
                         ELSE ''
                    END AS FormulaFrontEnd ,
                    CASE WHEN FR.FormulaType = 1 THEN FR.Formula
                         ELSE NULL
                    END AS Formula ,
                    FR.Hidden ,
                    FR.IsBold ,
                    FR.IsItalic ,
                    FR.Category ,
                    -1 AS SortOrder ,
                    ISNULL(X.Amount, 0) AS Amount ,
                    ISNULL(X.PrevAmount, 0) AS PrevAmount
            FROM    ( SELECT    ItemID ,
                                SUM(V.OperationSign * V.OpeningAmount) AS PrevAmount ,
                                SUM(V.OperationSign * V.ClosingAmount) AS Amount
                      FROM      V
                      GROUP BY  ItemID
                    ) AS X
                    RIGHT JOIN FRTemplate FR ON FR.ItemID = X.ItemID
            WHERE   AccountingSystem = @AccountingSystem
                    AND ReportID = @ReportID
	
            RETURN 
        END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GetHealthFinanceMoney]
      ( @FromDate DATETIME ,
        @ToDate DATETIME
		)
AS
BEGIN
	DECLARE @tbDataReturn TABLE(
		tySoNo decimal(18,2),
		tyTaiTro decimal(18,2),
		vonLuanChuyen decimal(18,2),
		hsTTNganHan decimal(18,2),
		hsTTNhanh decimal(18,2),
		hsTTTucThoi decimal(18,2),
		hsTTChung decimal(18,2),
		hsQVHangTonKho decimal(18,2),
		hsLNTrenVonKD decimal(18,2),
		hsLNTrenDTThuan decimal(18,2),
		hsLNTrenVonCSH decimal(18,2),
		tienMat money,
		tienGui money,
		doanhThu money,
		chiPhi money,
		loiNhuanTruocThue money,
		phaiThu money,
		phaiTra money,
		hangTonKho money
	)
	
	INSERT INTO @tbDataReturn(tySoNo,tyTaiTro,vonLuanChuyen,hsTTNganHan,hsTTNhanh,hsTTTucThoi,hsTTChung,hsQVHangTonKho,hsLNTrenVonKD,hsLNTrenDTThuan,
	hsLNTrenVonCSH,tienMat,tienGui,doanhThu,chiPhi,loiNhuanTruocThue,phaiThu,phaiTra,hangTonKho)
	Values(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null)
	
	DECLARE @tbDataGetB01DN TABLE(
	  ItemID UNIQUEIDENTIFIER ,
      ItemCode NVARCHAR(25) ,
      ItemName NVARCHAR(512) ,
      ItemNameEnglish NVARCHAR(512) ,
      ItemIndex INT ,
      Description NVARCHAR(512) ,
      FormulaType INT ,
      FormulaFrontEnd NVARCHAR(MAX) ,
      Formula XML ,
      Hidden BIT ,
      IsBold BIT ,
      IsItalic BIT ,
      Category INT ,
      SortOrder INT,
      Amount DECIMAL(25, 4) ,
      PrevAmount DECIMAL(25, 4)
	)
	
	INSERT INTO @tbDataGetB01DN
	SELECT * FROM [dbo].[Func_GetB01_DN] (null,null,@FromDate,@ToDate,0,1,0,@FromDate,@ToDate)
	
	
	DECLARE @tbDataGetBalanceAccountF01 TABLE(
		AccountID UNIQUEIDENTIFIER ,
        AccountCategoryKind INT ,
        AccountNumber NVARCHAR(50) ,
        AccountName NVARCHAR(512) ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
	)
	
	INSERT INTO @tbDataGetBalanceAccountF01
	SELECT t.AccountID ,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName,
			   sum(t.OpeningDebitAmount) OpeningDebitAmount,
			   sum(t.OpeningCreditAmount) OpeningCreditAmount,
			   sum(t.DebitAmount) DebitAmount,
			   sum(t.CreditAmount) CreditAmount,
			   sum(t.DebitAmountAccum) DebitAmountAccum,
			   sum(t.CreditAmountAccum) CreditAmountAccum,
			   sum(t.ClosingDebitAmount) ClosingDebitAmount,
			   sum(t.ClosingCreditAmount) ClosingCreditAmount
			   FROM [dbo].[Func_GetBalanceAccountF01] (@FromDate,@ToDate,1,0) t
	GROUP BY t.AccountID,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName
	ORDER BY t.AccountNumber
	
	
	Declare @ct400 decimal(25, 4)
	Set @ct400=(select Amount from @tbDataGetB01DN where ItemCode='400')
	
	Declare @ct600 decimal(25, 4)
	Set @ct600=(select Amount from @tbDataGetB01DN where ItemCode='600')
	
	if(ISNULL(@ct600,0)=0)
	begin
		UPDATE @tbDataReturn SET tySoNo = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET tySoNo = (ISNULL(@ct400,0)/ISNULL(@ct600,0))*100
	end
	
	Declare @ct500 decimal(25, 4)
	Set @ct500=(select Amount from @tbDataGetB01DN where ItemCode='500')
	
	if(ISNULL(@ct600,0)=0)
	begin
		UPDATE @tbDataReturn SET tyTaiTro = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET tyTaiTro = (ISNULL(@ct500,0)/ISNULL(@ct600,0))*100
	end
	
	Declare @ct100 decimal(25, 4)
	Set @ct100=(select Amount from @tbDataGetB01DN where ItemCode='100')
	
	Declare @ct410 decimal(25, 4)
	Set @ct410=(select Amount from @tbDataGetB01DN where ItemCode='410')
	
	UPDATE @tbDataReturn SET vonLuanChuyen = (ISNULL(@ct100,0)-ISNULL(@ct410,0))
	
	if(ISNULL(@ct410,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTNganHan = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTNganHan = (ISNULL(@ct100,0)/ISNULL(@ct410,0))
	end
	
	Declare @ct140 decimal(25, 4)
	Set @ct140=(select Amount from @tbDataGetB01DN where ItemCode='140')
	
	if(ISNULL(@ct410,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTNhanh = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTNhanh = (ISNULL(@ct100,0)-ISNULL(@ct140,0))/ISNULL(@ct410,0)
	end
	
	Declare @ct110 decimal(25, 4)
	Set @ct110=(select Amount from @tbDataGetB01DN where ItemCode='110')
	
	Declare @ct120 decimal(25, 4)
	Set @ct120=(select Amount from @tbDataGetB01DN where ItemCode='120')
	
	if(ISNULL(@ct410,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTTucThoi = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTTucThoi = (ISNULL(@ct110,0)+ISNULL(@ct120,0))/ISNULL(@ct410,0)
	end
	
	Declare @ct300 decimal(25, 4)
	Set @ct300=(select Amount from @tbDataGetB01DN where ItemCode='300')
	
	if(ISNULL(@ct400,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTChung = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTChung = (ISNULL(@ct300,0)/ISNULL(@ct400,0))
	end
	
	Declare @vonHH decimal(25, 4)
	Set @vonHH = (select SUM(DebitAmount) from GeneralLedger where Account='632' AND AccountCorresponding='911' AND (PostedDate between @FromDate and @ToDate))
	
	Declare @hangHoaTon decimal(25,4)
	set @hangHoaTon = (select SUM(ISNULL(OpeningDebitAmount,0)+ISNULL(ClosingDebitAmount,0)) from @tbDataGetBalanceAccountF01 
						where AccountNumber in ('152','153','154','155','156','157'))/2
	
	if(ISNULL(@hangHoaTon,0)=0)
	begin
		UPDATE @tbDataReturn SET hsQVHangTonKho = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsQVHangTonKho=(ISNULL(@vonHH,0)/ISNULL(@hangHoaTon,0))*100
	end
	
	Declare @LNsauThue decimal(25,4)
	set @LNsauThue = (select SUM(CreditAmount) from GeneralLedger where Account='4212' AND AccountCorresponding='911' AND (PostedDate between @FromDate and @ToDate))
	
	Declare @ct600namTruoc decimal(25, 4)
	Set @ct600namTruoc=(select PrevAmount from @tbDataGetB01DN where ItemCode='600')
	
	Declare @vonKdBq decimal(25,4)
	set @vonKdBq = (ISNULL(@ct600,0)+ISNULL(@ct600namTruoc,0))/2
	
	if(ISNULL(@vonKdBq,0)=0)
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonKD = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonKD = (ISNULL(@LNsauThue,0)/ISNULL(@vonKdBq,0))
	end
	
	
	Declare @dtThuan decimal(25,4)
	set @dtThuan = (select SUM(CreditAmount) from GeneralLedger where Account like'511%' AND AccountCorresponding='911' AND (PostedDate between @FromDate and @ToDate))
	
	if(ISNULL(@dtThuan,0)=0)
	begin
		UPDATE @tbDataReturn SET hsLNTrenDTThuan = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsLNTrenDTThuan = (ISNULL(@LNsauThue,0)/ISNULL(@dtThuan,0))
	end
	
	Declare @ct500namtruoc decimal(25, 4)
	Set @ct500namtruoc=(select PrevAmount from @tbDataGetB01DN where ItemCode='500')
	
	Declare @voncsh decimal(25,4)
	set @voncsh = (ISNULL(@ct500,0)+ISNULL(@ct500namtruoc,0))/2
	
	if(ISNULL(@voncsh,0)=0)
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonCSH = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonCSH = (ISNULL(@LNsauThue,0)/ISNULL(@voncsh,0))
	end
	
	Declare @duNoCuoiKy111 money
	set @duNoCuoiKy111 = (select ClosingDebitAmount from @tbDataGetBalanceAccountF01 where AccountNumber='111')
	
	UPDATE @tbDataReturn SET tienMat = ISNULL(@duNoCuoiKy111,0)
	
	Declare @duNoCuoiKy112 money
	set @duNoCuoiKy112 = (select ClosingDebitAmount from @tbDataGetBalanceAccountF01 where AccountNumber='112')
	
	UPDATE @tbDataReturn SET tienGui = ISNULL(@duNoCuoiKy112,0)
	
	Declare @doanhThu money
	set @doanhThu = (select SUM(CreditAmount - DebitAmount) from GeneralLedger 
						where ((Account like '511%') or (Account like '515%') or (Account like '711%'))
								and (AccountCorresponding<>'911') and (PostedDate between @FromDate and @ToDate))
	
	UPDATE @tbDataReturn SET doanhThu = ISNULL(@doanhThu,0)
	
	Declare @chiPhi money
	set @chiPhi = (select SUM(DebitAmount - CreditAmount) from GeneralLedger
						where ((Account like '632%') or (Account like '642%') or (Account like '635%') or (Account like '811%') or (Account like '821%'))
						and (AccountCorresponding<>'911') and (PostedDate between @FromDate and @ToDate))
	
	UPDATE @tbDataReturn SET chiPhi = ISNULL(@chiPhi,0)
	
	Declare @chiPhiko821 money
	set @chiPhiko821 = (select SUM(DebitAmount - CreditAmount) from GeneralLedger
						where ((Account like '632%') or (Account like '642%') or (Account like '635%') or (Account like '811%'))
						and (AccountCorresponding<>'911') and (PostedDate between @FromDate and @ToDate))
	
	UPDATE @tbDataReturn SET loiNhuanTruocThue = (ISNULL(@doanhThu,0) - ISNULL(@chiPhiko821,0))
	
	Declare @phaiThu money
	set @phaiThu = (select SUM(ClosingDebitAmount) from @tbDataGetBalanceAccountF01 where AccountNumber='131')
	
	UPDATE @tbDataReturn SET phaiThu = (ISNULL(@phaiThu,0))
	
	Declare @phaiTra money
	set @phaiTra = (select SUM(ClosingCreditAmount) from @tbDataGetBalanceAccountF01 where AccountNumber='331')
	
	UPDATE @tbDataReturn SET phaiTra = (ISNULL(@phaiTra,0))
	
	Declare @hangTonKho money
	set @hangTonKho = (select SUM(ClosingDebitAmount) from @tbDataGetBalanceAccountF01 
						where AccountNumber in ('152','153','154','155','156','157'))
	
	UPDATE @tbDataReturn SET hangTonKho = (ISNULL(@hangTonKho,0))
	
	SELECT * FROM @tbDataReturn
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoTheoDoiLaiLoTheoHoaDon]
    @FromDate DATETIME,
    @ToDate DATETIME
AS
BEGIN          
    
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
		/*end add by cuongpv*/
	
	DECLARE @tbluutru TABLE (
		SAInvoiceID UNIQUEIDENTIFIER,
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		Reason NVARCHAR(512),
		GiaTriHHDV money,
		ChietKhauBan money,
		ChietKhau_TLGG money,
		TraLai money,
		GiaVon money,
		LaiLo money
    )
    INSERT INTO @tbluutru(SAInvoiceID, DetailID, InvoiceDate,InvoiceNo,AccountingObjectID,Reason)
		SELECT DISTINCT SA.ID, GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
		FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		WHERE (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is null
		UNION ALL
		SELECT DISTINCT SA.ID, GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason
		FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		WHERE (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is not null
	

	DECLARE @tbluutruGiaTriHHDV TABLE (
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		Reason NVARCHAR(512),
		GiaTriHHDV money
    )

	INSERT INTO @tbluutruGiaTriHHDV
	SELECT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.CreditAmount) as GiaTriHHDV
	FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	WHERE (GL.Account like '511%') AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is null
	GROUP BY GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
	UNION ALL
	SELECT GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.CreditAmount) as GiaTriHHDV
	FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
	WHERE (GL.Account like '511%') AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is not null
	GROUP BY GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason

    UPDATE @tbluutru SET GiaTriHHDV = a.GiaTriHHDV
	FROM (
		Select DetailID as IvID, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, Reason as Re, GiaTriHHDV
		From @tbluutruGiaTriHHDV
	) a
	WHERE DetailID = a.IvID
	

	DECLARE @tbluutruChietKhauBan TABLE (
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		Reason NVARCHAR(512),
		ChietKhauBan money
    )

	INSERT INTO @tbluutruChietKhauBan
	SELECT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.DebitAmount) as ChietKhauBan
	FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	WHERE (GL.Account like '511%') AND (GL.TypeID not in (330,340)) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is null
	GROUP BY GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
	UNION ALL
	SELECT GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.DebitAmount) as ChietKhauBan
	FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
	WHERE (GL.Account like '511%') AND (GL.TypeID not in (330,340)) AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is not null
	GROUP BY GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason


	UPDATE @tbluutru SET ChietKhauBan = b.ChietKhauBan
	FROM (
		Select DetailID as IvID, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, Reason as Re, ChietKhauBan
		From @tbluutruChietKhauBan
	) b
	WHERE DetailID = b.IvID
	
	DECLARE @tbluutruChietKhau_TLGG TABLE (
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		Reason NVARCHAR(512),
		ChietKhau_TLGG money
    )
	INSERT INTO @tbluutruChietKhau_TLGG
	SELECT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.CreditAmount) as ChietKhau_TLGG
	FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	WHERE (GL.Account like '511%') AND (GL.TypeID in (330,340)) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail))
	GROUP BY GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason


	UPDATE @tbluutru SET ChietKhau_TLGG = c.ChietKhau_TLGG
	FROM (
		Select DetailID as IvID, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, Reason as Re, ChietKhau_TLGG
		From @tbluutruChietKhau_TLGG
	) c
	WHERE DetailID = c.IvID
	

	DECLARE @tbDataTraLai TABLE(
		SAInvoiceDetailID UNIQUEIDENTIFIER,
		TraLai money
	)
	INSERT INTO @tbDataTraLai(SAInvoiceDetailID,TraLai)
		SELECT SAR.SAInvoiceDetailID, SUM(GL.DebitAmount - GL.CreditAmount) as TraLai
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON GL.DetailID = SAR.ID
		WHERE (GL.Account like '511%') AND (GL.TypeID=330) AND (SAR.SAInvoiceDetailID in (select DetailID from @tbluutru))
		AND (GL.PostedDate between @FromDate and @ToDate)
		GROUP BY SAR.SAInvoiceDetailID
	

	UPDATE @tbluutru SET TraLai = TL.TraLai
	FROM (select SAInvoiceDetailID, TraLai from @tbDataTraLai) TL
	WHERE DetailID = TL.SAInvoiceDetailID
	
	DECLARE @tbluutruGiaVon TABLE (
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		Reason NVARCHAR(512),
		GiaVonHangBan money,
		GiaVonTraLai money,
		GiaVon money
    )

	INSERT INTO @tbluutruGiaVon(DetailID, InvoiceDate, InvoiceNo, AccountingObjectID, Reason, GiaVonHangBan)
	SELECT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.DebitAmount - GL.CreditAmount) as GiaVonHangBan
	FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	WHERE (GL.Account like '632%') AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND SA.BillRefID is null
	GROUP BY GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
	UNION ALL
	SELECT GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.DebitAmount - GL.CreditAmount) as GiaVonHangBan
	FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
	WHERE (GL.Account like '632%') AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND SA.BillRefID is not null
	GROUP BY GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason

	UPDATE @tbluutruGiaVon SET GiaVonTraLai = Gvtl.GiaVonTraLai
	FROM (
		Select SAR.SAInvoiceDetailID, SUM(GL.DebitAmount - GL.CreditAmount) as GiaVonTraLai
		From @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON GL.DetailID = SAR.ID
		Where (GL.Account like '632%') AND (SAR.SAInvoiceDetailID in (select DetailID from @tbluutruGiaVon))
		AND (GL.PostedDate between @FromDate and @ToDate)
		Group By SAR.SAInvoiceDetailID
	) Gvtl
	WHERE DetailID = SAInvoiceDetailID

	UPDATE @tbluutruGiaVon SET GiaVon = ISNULL(GiaVonHangBan,0) + ISNULL(GiaVonTraLai,0)


	UPDATE @tbluutru SET GiaVon = d.GiaVon
	FROM (
		Select DetailID as IvID, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, Reason as Re, GiaVon
		From @tbluutruGiaVon
	) d
	WHERE DetailID = d.IvID AND InvoiceDate = d.IvDate AND InvoiceNo = d.IvNo AND AccountingObjectID = d.AOID AND Reason = d.Re
		
	
	DECLARE @tbDataReturn TABLE (
		SAInvoiceID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		AccountingObjectName NVARCHAR(512),
		Reason NVARCHAR(512),
		GiaTriHHDV money,
		ChietKhauBan money,
		ChietKhau_TLGG money,
		ChietKhau money,
		GiamGia money,
		TraLai money,
		GiaVon money,
		LaiLo money
    )
    
    INSERT INTO @tbDataReturn(SAInvoiceID,InvoiceDate,InvoiceNo,AccountingObjectID,Reason,GiaTriHHDV,ChietKhauBan,ChietKhau_TLGG,TraLai,GiaVon)
    SELECT SAInvoiceID, InvoiceDate,InvoiceNo,AccountingObjectID,Reason,SUM(GiaTriHHDV) as GiaTriHHDV, SUM(ChietKhauBan) as ChietKhauBan,
    SUM(ChietKhau_TLGG) as ChietKhau_TLGG, SUM(TraLai) as TraLai, SUM(GiaVon) as GiaVon
    FROM @tbluutru
    GROUP BY SAInvoiceID, InvoiceDate,InvoiceNo,AccountingObjectID,Reason
    

	INSERT INTO @tbDataReturn(SAInvoiceID,InvoiceDate,InvoiceNo,AccountingObjectID,Reason,GiamGia)
		SELECT SA.ID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.DebitAmount - GL.CreditAmount) as GiamGia
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		WHERE (GL.Account like '511%') AND (GL.TypeID=340) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAReturnDetail))
		GROUP BY SA.ID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
    
    UPDATE @tbDataReturn SET AccountingObjectName = AJ.AccountingObjectName
	FROM ( select ID,AccountingObjectName from AccountingObject) AJ
	where AccountingObjectID = AJ.ID
    
    UPDATE @tbDataReturn SET ChietKhau = (ISNULL(ChietKhauBan,0) - ISNULL(ChietKhau_TLGG,0))
    
    UPDATE @tbDataReturn SET LaiLo = (ISNULL(GiaTriHHDV,0) - (ISNULL(ChietKhau,0)+ISNULL(GiamGia,0)+ISNULL(TraLai,0)+ISNULL(GiaVon,0)))
    
    SELECT InvoiceDate,InvoiceNo,AccountingObjectName,Reason,GiaTriHHDV,ChietKhau,GiamGia,TraLai,GiaVon,LaiLo FROM @tbDataReturn
    WHERE ((ISNULL(GiaTriHHDV,0)>0) OR (ISNULL(ChietKhau,0)>0) OR (ISNULL(GiamGia,0)>0)
			OR (ISNULL(TraLai,0)>0) OR (ISNULL(GiaVon,0)>0) OR (ISNULL(LaiLo,0)>0))
	ORDER BY InvoiceDate,InvoiceNo
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

ALTER PROCEDURE [dbo].[Proc_GLR_GetS03a2_DN]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @CurrencyID NVARCHAR(10) ,
    @AccountNumber NVARCHAR(25) ,
    @BankAccountID UNIQUEIDENTIFIER ,
    @IsSimilarSum BIT
AS
    BEGIN
        SET NOCOUNT ON;     
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate BETWEEN @FromDate AND @ToDate
		/*end add by cuongpv*/
		
		CREATE TABLE #Result
            (
              BranchName NVARCHAR(512)  ,
              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefNo NVARCHAR(22) ,
              RefDate DATETIME ,
              [Description] NVARCHAR(512) ,
              AccountNumber NVARCHAR(20) ,
              CorrespondingAccountNumber NVARCHAR(20) ,
              Amount MONEY ,
              Col2 MONEY DEFAULT 0 ,
              Col3 MONEY DEFAULT 0 ,
              Col4 MONEY DEFAULT 0 ,
              Col5 MONEY DEFAULT 0 ,
              Col6 MONEY DEFAULT 0 ,
              Col7 MONEY DEFAULT 0 ,
              ColOtherAccount NVARCHAR(20) ,
              AccountNumberList NVARCHAR(120) ,
              /*Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
              SortOrder INT,
              DetailPostOrder int    
            )
                     
         /*tài khoản ngân hàng khác - lấy các phát sinh không chọn tài khoản ngân hàng*/		
        DECLARE @BankAccountOther UNIQUEIDENTIFIER
        SET @BankAccountOther = '12345678-2222-48B8-AE4B-5CF7FA7FB3F5'
		IF(@CurrencyID = 'VND')
		BEGIN
		    INSERT  #Result
                ( BranchName ,
                  RefType ,
                  RefID ,
                  PostedDate ,
                  RefNo ,
                  RefDate ,
                  Description ,
                  AccountNumber ,
                  CorrespondingAccountNumber ,
                  Amount,
                    SortOrder,
                  DetailPostOrder
                )
                SELECT  null as BranchName ,
                        GL.TypeID ,
                        GL.ReferenceID ,
                        GL.PostedDate ,
                        GL.RefNo ,
                        GL.RefDate ,
                        CASE WHEN @IsSimilarSum = 1 THEN GL.Reason
                             ELSE ISNULL(GL.[Description], GL.Reason)
                        END ,
                        @AccountNumber AS AccountNumber ,
                        GL.AccountCorresponding ,
                        GL.CreditAmount AS Amount,
                          /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        GL.OrderPriority,
                        null
                FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                        INNER JOIN dbo.Account AS A ON GL.Account = A.AccountNumber
                WHERE   PostedDate BETWEEN @FromDate AND @ToDate
                        AND GL.Account LIKE @AccountNumber + '%'
                        AND GL.CreditAmount <> 0
                        AND ( @BankAccountID IS NULL
                              OR ( (GL.BankAccountDetailID = @BankAccountID
                                   AND @BankAccountID <> @BankAccountOther )
                                 )
                         
                              OR ( (GL.BankAccountDetailID IS  NULL
                                   AND @BankAccountID = @BankAccountOther )
                                 )
                            )
		END
        ELSE
		BEGIN
        INSERT  #Result
                ( BranchName ,
                  RefType ,
                  RefID ,
                  PostedDate ,
                  RefNo ,
                  RefDate ,
                  Description ,
                  AccountNumber ,
                  CorrespondingAccountNumber ,
                  Amount,
                    SortOrder,
                  DetailPostOrder
                )
                SELECT  null as BranchName ,
                        GL.TypeID ,
                        GL.ReferenceID ,
                        GL.PostedDate ,
                        GL.RefNo ,
                        GL.RefDate ,
                        CASE WHEN @IsSimilarSum = 1 THEN GL.Reason
                             ELSE ISNULL(GL.[Description], GL.Reason)
                        END ,
                        @AccountNumber AS AccountNumber ,
                        GL.AccountCorresponding ,
                        GL.CreditAmountOriginal AS Amount,
                          /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        GL.OrderPriority,
                        null
                FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                        INNER JOIN dbo.Account AS A ON GL.Account = A.AccountNumber
                WHERE   PostedDate BETWEEN @FromDate AND @ToDate
                        AND GL.Account LIKE @AccountNumber + '%'
                        AND GL.CreditAmount <> 0
                        AND ( @CurrencyID IS NULL
                              OR GL.CurrencyID = @CurrencyID
                            )
                        AND ( @BankAccountID IS NULL
                              OR ( (GL.BankAccountDetailID = @BankAccountID
                                   AND @BankAccountID <> @BankAccountOther )
                                 )
                         
                              OR ( (GL.BankAccountDetailID IS  NULL
                                   AND @BankAccountID = @BankAccountOther )
                                 )
                            )
							END
        IF EXISTS ( SELECT TOP 1
                            *
                    FROM    #Result )
            BEGIN
                DECLARE @AccountNumberAdded NVARCHAR(50)
                DECLARE @AccountNumberList NVARCHAR(MAX)
                DECLARE @Updatesql1 NVARCHAR(MAX)
                DECLARE @Updatesql2 NVARCHAR(MAX)
                DECLARE @tblName NVARCHAR(20)
                DECLARE @colNumber INT
                
                DECLARE @CoresAccountNumberList NVARCHAR(MAX)
        
                SET @tblName = '#Result'
                SET @Updatesql1 = ''
                SET @Updatesql2 = ''
                SET @colNumber = 2
                SET @AccountNumberList = @AccountNumber + ','
                SET @CoresAccountNumberList = ''
		
                DECLARE curAcc CURSOR FAST_FORWARD READ_ONLY
                FOR
                    SELECT DISTINCT TOP 4
                            R.CorrespondingAccountNumber
                    FROM    #Result AS R
                    WHERE   ISNULL(R.CorrespondingAccountNumber, '') <> ''
                    ORDER BY R.CorrespondingAccountNumber
                OPEN curAcc

                FETCH NEXT FROM curAcc INTO @AccountNumberAdded

                WHILE @@FETCH_STATUS = 0
                    BEGIN
                        IF @Updatesql1 <> ''
                            SET @Updatesql1 = @Updatesql1 + ', '
                        SET @Updatesql1 = @Updatesql1 + '[col'
                            + CONVERT(NVARCHAR(10), @colNumber) + ']'
                            + ' = (CASE WHEN ISNULL(CorrespondingAccountNumber,'''') = '''
                            + @AccountNumberAdded
                            + ''' THEN Amount Else 0 END)'                
                        IF @Updatesql2 <> ''
                            SET @Updatesql2 = @Updatesql2 + ' AND '
                        SET @Updatesql2 = @Updatesql2
                            + 'ISNULL(CorrespondingAccountNumber,'''') <> '''
                            + @AccountNumberAdded + ''' '
                        SET @AccountNumberList = @AccountNumberList
                            + @AccountNumberAdded + ','
                            
                            
                        SET @CoresAccountNumberList = @CoresAccountNumberList
                            + ',''' + @AccountNumberAdded + ''''
                        SET @colNumber = @colNumber + 1                          
                        FETCH NEXT FROM curAcc INTO @AccountNumberAdded
                
                    END
                CLOSE curAcc
                DEALLOCATE curAcc
                
			
                SET @Updatesql1 = 'UPDATE #Result SET ' + @Updatesql1
                    +
                    ', [col7] = (CASE WHEN ' + @Updatesql2
                    + ' THEN Amount ELSE 0 END)'
                    +
                    ', [ColOtherAccount] = (CASE WHEN ' + @Updatesql2
                    + ' THEN CorrespondingAccountNumber END)'
                 
       
                EXEC (@Updatesql1)
                UPDATE  #Result
                SET     AccountNumberList = @AccountNumberList          
            END
        
        IF @IsSimilarSum = 1
            BEGIN
                SELECT  BranchName ,
                        RefType ,
                        RefID ,
                        PostedDate ,
                        RefNo ,
                        RefDate ,
                        [Description] ,
                        AccountNumber ,
                        SUM(Amount) AS Amount ,
                        SUM(Col2) AS Col2 ,
                        SUM(Col3) AS Col3 ,
                        SUM(Col4) AS Col4 ,
                        SUM(Col5) AS Col5 ,
                        SUM(Col6) AS Col6 ,
                        SUM(Col7) AS Col7 ,
                        /*ISNULL(ColOtherAccount,
                               Temp1.CorrespondingAccountNumber) AS ColOtherAccount , comment by cuongpv*/
						ColOtherAccount, /*add by cuongpv*/
                        AccountNumberList
                FROM    #Result AS R
                        /*OUTER APPLY ( SELECT TOP 1
                                                CorrespondingAccountNumber
                                      FROM      #Result AS R1
                                      WHERE     R1.RefID = R.RefID
                                                AND ColOtherAccount NOT IN (
                                                STUFF(@CoresAccountNumberList,
                                                      1, 1, '') )
                                      ORDER BY  CorrespondingAccountNumber
                                    ) AS Temp1 comment by cuongpv*/
                GROUP BY BranchName ,
                        RefType ,
                        RefID ,
                        PostedDate ,
                        RefNo ,
                        RefDate ,
                        [Description] ,
                        AccountNumber ,
                        /*ISNULL(ColOtherAccount,
                               Temp1.CorrespondingAccountNumber) , comment by cuongpv*/
						ColOtherAccount, /*add by cuongpv*/
						SortOrder,
                        AccountNumberList
                  /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */       
                ORDER BY 
						R.PostedDate ,
						R.SortOrder,
                        R.RefDate ,
                        R.RefNo      
            END
        ELSE
            BEGIN
            
                SELECT  *
                FROM    #Result AS R
                ORDER BY R.PostedDate ,
                        R.RefDate ,
                        R.RefNo ,
                          /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        R.SortOrder,
                        R.DetailPostOrder
            END

        DROP TABLE #Result
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

ALTER PROCEDURE [dbo].[Proc_GLR_GetS03a1_DN]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @CurrencyID NVARCHAR(10) ,
    @AccountNumber NVARCHAR(25) ,
    @BankAccountID UNIQUEIDENTIFIER ,
    @IsSimilarSum BIT
AS
    BEGIN
        SET NOCOUNT ON;
		
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate BETWEEN @FromDate AND @ToDate
		/*end add by cuongpv*/

        CREATE TABLE #Result
            (
			
              BranchName NVARCHAR(512)  ,
              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefNo NVARCHAR(22) ,
              RefDate DATETIME ,
              [Description] NVARCHAR(512) ,
              AccountNumber NVARCHAR(20) ,
              CorrespondingAccountNumber NVARCHAR(20) ,
              Amount MONEY ,
              Col2 MONEY DEFAULT 0 ,
              Col3 MONEY DEFAULT 0 ,
              Col4 MONEY DEFAULT 0 ,
              Col5 MONEY DEFAULT 0 ,
              Col6 MONEY DEFAULT 0 ,
              Col7 MONEY DEFAULT 0 ,
              ColOtherAccount NVARCHAR(20) ,
              AccountNumberList NVARCHAR(120) ,
                  /*sửa lỗi 139606 -Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
              SortOrder INT,
              DetailPostOrder int,
			  OrderPriority int,
              
            )
                     
         /*tài khoản ngân hàng khác - lấy các phát sinh không chọn tài khoản ngân hàng*/		
        DECLARE @BankAccountOther UNIQUEIDENTIFIER
        SET @BankAccountOther = '12345678-2222-48B8-AE4B-5CF7FA7FB3F5'
        
		IF(@CurrencyID = 'VND')
		BEGIN
		       INSERT  #Result
                ( BranchName ,
                  RefType ,
                  RefID ,
                  PostedDate ,
                  RefNo ,
                  RefDate ,
                  Description ,
                  AccountNumber ,
                  CorrespondingAccountNumber ,
                  Amount,
                  SortOrder,
                  DetailPostOrder
                )
                SELECT  null as BranchName ,
                        GL.TypeID ,
                        GL.ReferenceID ,
                        GL.PostedDate ,
                        GL.RefNo ,
                        GL.RefDate ,
                        CASE WHEN @IsSimilarSum = 1 THEN GL.Reason
                             ELSE ISNULL(GL.[Description], GL.Reason)
                        END ,
                        @AccountNumber AS AccountNumber ,
                        GL.AccountCorresponding ,
                        GL.DebitAmount AS Amount,
                         /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        GL.OrderPriority,
                        null
                FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                        INNER JOIN dbo.Account AS A ON GL.Account = A.AccountNumber
                WHERE   PostedDate BETWEEN @FromDate AND @ToDate
                        AND GL.Account LIKE @AccountNumber + '%'
                        AND GL.DebitAmount <> 0
                        AND ( @BankAccountID IS NULL
                              OR ( (GL.BankAccountDetailID = @BankAccountID
                                   AND @BankAccountID <> @BankAccountOther )
                                 )
                         
                              OR ( (GL.BankAccountDetailID IS  NULL
                                   AND @BankAccountID = @BankAccountOther )
                                 )
                            )
		END
		ELSE
		BEGIN
        INSERT  #Result
                ( BranchName ,
                  RefType ,
                  RefID ,
                  PostedDate ,
                  RefNo ,
                  RefDate ,
                  Description ,
                  AccountNumber ,
                  CorrespondingAccountNumber ,
                  Amount,
                  SortOrder,
                  DetailPostOrder
                )
                SELECT  null as BranchName ,
                        GL.TypeID ,
                        GL.ReferenceID ,
                        GL.PostedDate ,
                        GL.RefNo ,
                        GL.RefDate ,
                        CASE WHEN @IsSimilarSum = 1 THEN GL.Reason
                             ELSE ISNULL(GL.[Description], GL.Reason)
                        END ,
                        @AccountNumber AS AccountNumber ,
                        GL.AccountCorresponding ,
                        GL.DebitAmountOriginal AS Amount,
                         /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        GL.OrderPriority,
                        null
                FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                        INNER JOIN dbo.Account AS A ON GL.Account = A.AccountNumber
                WHERE   PostedDate BETWEEN @FromDate AND @ToDate
                        AND GL.Account LIKE @AccountNumber + '%'
                        AND GL.DebitAmount <> 0
                        AND ( @CurrencyID IS NULL
                              OR GL.CurrencyID = @CurrencyID
                            )
                        AND ( @BankAccountID IS NULL
                              OR ( (GL.BankAccountDetailID = @BankAccountID
                                   AND @BankAccountID <> @BankAccountOther )
                                 )
                         
                              OR ( (GL.BankAccountDetailID IS  NULL
                                   AND @BankAccountID = @BankAccountOther )
                                 )
                            )
	END
        IF EXISTS ( SELECT TOP 1
                            *
                    FROM    #Result )
            BEGIN
                DECLARE @AccountNumberAdded NVARCHAR(50)
                DECLARE @AccountNumberList NVARCHAR(MAX)
                DECLARE @Updatesql1 NVARCHAR(MAX)
                DECLARE @Updatesql2 NVARCHAR(MAX)
                DECLARE @tblName NVARCHAR(20)
                DECLARE @colNumber INT
                
                DECLARE @CoresAccountNumberList NVARCHAR(MAX)
        
                SET @tblName = '#Result'
                SET @Updatesql1 = ''
                SET @Updatesql2 = ''
                SET @colNumber = 2
                SET @AccountNumberList = @AccountNumber + ','
                SET @CoresAccountNumberList = ''
		
                DECLARE curAcc CURSOR FAST_FORWARD READ_ONLY
                FOR
                    SELECT DISTINCT TOP 4
                            R.CorrespondingAccountNumber
                    FROM    #Result AS R
                    WHERE   ISNULL(R.CorrespondingAccountNumber, '') <> ''
                    ORDER BY R.CorrespondingAccountNumber
                OPEN curAcc

                FETCH NEXT FROM curAcc INTO @AccountNumberAdded

                WHILE @@FETCH_STATUS = 0
                    BEGIN
                        IF @Updatesql1 <> ''
                            SET @Updatesql1 = @Updatesql1 + ', '
                        SET @Updatesql1 = @Updatesql1 + '[col'
                            + CONVERT(NVARCHAR(10), @colNumber) + ']'
                            + ' = (CASE WHEN ISNULL(CorrespondingAccountNumber,'''') = '''
                            + @AccountNumberAdded
                            + ''' THEN Amount Else 0 END)'                
                        IF @Updatesql2 <> ''
                            SET @Updatesql2 = @Updatesql2 + ' AND '
                        SET @Updatesql2 = @Updatesql2
                            + 'ISNULL(CorrespondingAccountNumber,'''') <> '''
                            + @AccountNumberAdded + ''' '
                        SET @AccountNumberList = @AccountNumberList
                            + @AccountNumberAdded + ','
                            
                            
                        SET @CoresAccountNumberList = @CoresAccountNumberList
                            + ',''' + @AccountNumberAdded + ''''
                        SET @colNumber = @colNumber + 1                          
                        FETCH NEXT FROM curAcc INTO @AccountNumberAdded
                
                    END
                CLOSE curAcc
                DEALLOCATE curAcc
                
			
                SET @Updatesql1 = 'UPDATE #Result SET ' + @Updatesql1
                    +
                    ', [col7] = (CASE WHEN ' + @Updatesql2
                    + ' THEN Amount ELSE 0 END)'
                    +
                    ', [ColOtherAccount] = (CASE WHEN ' + @Updatesql2
                    + ' THEN CorrespondingAccountNumber END)'
                 
        
                EXEC (@Updatesql1)
                UPDATE  #Result
                SET     AccountNumberList = @AccountNumberList          
            END
        
        IF @IsSimilarSum = 1
            BEGIN
                SELECT  BranchName ,
                        RefType ,
                        RefID ,
                        PostedDate ,
                        RefNo ,
                        RefDate ,
                        [Description] ,
                        AccountNumber ,
                        SUM(Amount) AS Amount ,
                        SUM(Col2) AS Col2 ,
                        SUM(Col3) AS Col3 ,
                        SUM(Col4) AS Col4 ,
                        SUM(Col5) AS Col5 ,
                        SUM(Col6) AS Col6 ,
                        SUM(Col7) AS Col7 ,
                        /*ISNULL(ColOtherAccount,
                               Temp1.CorrespondingAccountNumber) AS ColOtherAccount , comment by cuongpv*/
						ColOtherAccount, /*add by cuongpv*/
                        AccountNumberList
                FROM    #Result AS R
                        /*OUTER APPLY ( SELECT TOP 1
                                                CorrespondingAccountNumber
                                      FROM      #Result AS R1
                                      WHERE     R1.RefID = R.RefID
                                                AND ColOtherAccount NOT IN (
                                                STUFF(@CoresAccountNumberList,
                                                      1, 1, '') )
                                      ORDER BY  CorrespondingAccountNumber
                                    ) AS Temp1 comment by cuongpv*/
                GROUP BY BranchName ,
                        RefType ,
                        RefID ,
                        PostedDate ,
                        RefNo ,
                        RefDate ,
                        [Description] ,
                        AccountNumber ,
                        /*ISNULL(ColOtherAccount,
                               Temp1.CorrespondingAccountNumber) , commnet by cuongpv*/
						ColOtherAccount,
						OrderPriority, /*add by cuongpv*/
                        AccountNumberList
                     /* -Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */       
                ORDER BY 
						R.PostedDate ,
						R.OrderPriority,
                        R.RefDate ,
                        R.RefNo             
                       
                                    
            
            END
        ELSE
            BEGIN
            
            
                SELECT  *
                FROM    #Result AS R                                                
                ORDER BY R.PostedDate ,
                        R.RefDate ,
                        R.RefNo ,
                        /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        R.SortOrder,
                        R.DetailPostOrder
            END

        DROP TABLE #Result
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

ALTER PROCEDURE [dbo].[Proc_GetCACashBookInCABook]
    (
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @CurrencyID NVARCHAR(3) ,
      @AccountNumber NVARCHAR(25)
    )
AS
    BEGIN

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
              RefType INT ,
              RefDate DATETIME ,
              PostedDate DATETIME ,
              ReceiptRefNo NVARCHAR(20)  ,
              PaymentRefNo NVARCHAR(20)  ,
              CashBookPostedDate DATETIME ,
              AccountObjectName NVARCHAR(512)
                 ,
              JournalMemo NVARCHAR(512)  ,
              CurrencyID NVARCHAR(3)  ,
              TotalReceiptFBCurrencyID MONEY ,
              TotalPaymentFBCurrencyID MONEY ,
              ClosingFBCurrencyID MONEY ,
              BranchID UNIQUEIDENTIFIER ,
              BranchName NVARCHAR(512)  ,
              RefTypeName NVARCHAR(100)  ,
              Note NVARCHAR(255)  ,
              CAType INT ,
              IsBold BIT ,
               /* - bổ sung mã nhân viên , tên nhân viên */
              EmployeeCode NVARCHAR(25)  ,
              EmployeeName NVARCHAR(512)  ,
			  OrderPriority int
            )       
       
	   IF(@CurrencyID = 'VND')
	   BEGIN
			/*Add by cuongpv de sua cach lam tron*/
			DECLARE @tbDataGL TABLE(
				ID uniqueidentifier,
				BranchID uniqueidentifier,
				ReferenceID uniqueidentifier,
				TypeID int,
				Date datetime,
				PostedDate datetime,
				No nvarchar(25),
				InvoiceDate datetime,
				InvoiceNo nvarchar(25),
				Account nvarchar(25),
				AccountCorresponding nvarchar(25),
				BankAccountDetailID uniqueidentifier,
				CurrencyID nvarchar(3),
				ExchangeRate decimal(25, 10),
				DebitAmount decimal(25,0),
				DebitAmountOriginal decimal(25,0),
				CreditAmount decimal(25,0),
				CreditAmountOriginal decimal(25,0),
				Reason nvarchar(512),
				Description nvarchar(512),
				VATDescription nvarchar(512),
				AccountingObjectID uniqueidentifier,
				EmployeeID uniqueidentifier,
				BudgetItemID uniqueidentifier,
				CostSetID uniqueidentifier,
				ContractID uniqueidentifier,
				StatisticsCodeID uniqueidentifier,
				InvoiceSeries nvarchar(25),
				ContactName nvarchar(512),
				DetailID uniqueidentifier,
				RefNo nvarchar(25),
				RefDate datetime,
				DepartmentID uniqueidentifier,
				ExpenseItemID uniqueidentifier,
				OrderPriority int,
				IsIrrationalCost bit
			)

			INSERT INTO @tbDataGL
			SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
			/*end add by cuongpv*/

			INSERT  #Result
                ( RefID ,
                  RefType ,
                  RefDate ,
                  PostedDate ,
                  ReceiptRefNo ,
                  PaymentRefNo ,
                  CashBookPostedDate ,
                  AccountObjectName ,
                  JournalMemo ,
                  CurrencyID ,
                  TotalReceiptFBCurrencyID ,
                  TotalPaymentFBCurrencyID ,
                  ClosingFBCurrencyID ,
                  BranchID ,
                  BranchName ,
                  RefTypeName ,
                  CAType ,
                  IsBold ,
                  EmployeeCode ,
                  EmployeeName,
				  OrderPriority
                )
                SELECT  RefID ,
                        RefType ,
                        RefDate ,
                        R.PostedDate ,
                        CASE WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        CASE WHEN TotalPaymentFBCurrencyID
                                  - TotalReceiptFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        R.PostedDate ,
                        ContactName ,
                        JournalMemo ,
                        @CurrencyID ,
                        SUM(CASE WHEN TotalReceiptFBCurrencyID
                                      - TotalPaymentFBCurrencyID > 0
                                 THEN TotalReceiptFBCurrencyID
                                      - TotalPaymentFBCurrencyID
                                 ELSE 0
                            END) AS TotalReceiptFBCurrencyID ,
                        SUM(CASE WHEN TotalPaymentFBCurrencyID
                                      - TotalReceiptFBCurrencyID > 0
                                 THEN TotalPaymentFBCurrencyID
                                      - TotalReceiptFBCurrencyID
                                 ELSE 0
                            END) AS TotalPaymentFBCurrencyID ,
                        SUM(ClosingFBCurrencyID) ,
                        BranchID ,
                        BranchName ,
                        RefTypeName ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END AS CAType ,
                        IsBold ,
                        EmployeeCode ,
                        EmployeeName,
						OrderPriority
                FROM    ( SELECT    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ReferenceID
                                    END AS RefID ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.TypeID
                                    END AS RefType ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefDate
                                    END AS RefDate ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END AS PostedDate ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefNo
                                    END AS RefNo ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ContactName
                                    END AS ContactName ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN N'Số dư đầu kỳ'
                                         ELSE GL.Description
                                    END AS JournalMemo ,
                                    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal > 0
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) AS TotalReceiptFBCurrencyID ,
                                    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.CreditAmountOriginal
                                                  - GL.DebitAmountOriginal > 0
                                             THEN GL.CreditAmountOriginal
                                                  - GL.DebitAmountOriginal
                                             ELSE 0
                                        END) AS TotalPaymentFBCurrencyID ,
                                    SUM(CASE WHEN GL.PostedDate < @FromDate
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) AS ClosingFBCurrencyID ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.BranchID
                                    END AS BranchID ,
                                    null AS BranchName ,
                                    null RefTypeName ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE 0
                                    END AS CAType ,
                                    CASE WHEN GL.PostedDate < @FromDate THEN 1
                                         ELSE 0
                                    END AS IsBold ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectCode
                                    END AS EmployeeCode ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectName
                                    END AS EmployeeName,
									CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE OrderPriority
                                    END AS OrderPriority
                          FROM      @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                    LEFT JOIN dbo.AccountingObject AS AO ON AO.ID = GL.EmployeeID
                          WHERE     ( GL.PostedDate <= @ToDate )
                                    AND ( @CurrencyID IS NULL
                                          OR GL.CurrencyID = @CurrencyID
                                        )
                                    AND GL.Account LIKE @AccountNumber
                                    + '%'
                          GROUP BY  CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ReferenceID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.TypeID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefNo
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ContactName
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN N'Số dư đầu kỳ'
                                         ELSE GL.Description
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.BranchID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE 0
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate THEN 1
                                         ELSE 0
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectCode
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectName
                                    END,
									 CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE OrderPriority
                                    END
                          HAVING    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal > 0
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) <> SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                              AND GL.CreditAmountOriginal
                                                              - GL.DebitAmountOriginal > 0
                                                         THEN GL.CreditAmountOriginal
                                                              - GL.DebitAmountOriginal
                                                         ELSE 0
                                                    END)
                                    OR SUM(CASE WHEN GL.PostedDate < @FromDate
                                                THEN GL.DebitAmountOriginal
                                                     - GL.CreditAmountOriginal
                                                ELSE 0
                                           END) <> 0
                        ) AS R
                GROUP BY R.RefID ,
                        R.RefType ,
                        R.RefDate ,
                        R.PostedDate ,
                        CASE WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        CASE WHEN TotalPaymentFBCurrencyID
                                  - TotalReceiptFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        R.RefNo ,
                        R.ContactName ,
                        R.JournalMemo ,
                        R.BranchID ,
                        R.BranchName ,
                        R.RefTypeName ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END ,
                        R.IsBold ,
                        R.EmployeeCode ,
                        R.EmployeeName,
						R.OrderPriority
                ORDER BY R.PostedDate,R.OrderPriority,
                        R.RefDate ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END ,
                        R.RefNo	
	   END
	   ELSE
	   BEGIN
			INSERT  #Result
                ( RefID ,
                  RefType ,
                  RefDate ,
                  PostedDate ,
                  ReceiptRefNo ,
                  PaymentRefNo ,
                  CashBookPostedDate ,
                  AccountObjectName ,
                  JournalMemo ,
                  CurrencyID ,
                  TotalReceiptFBCurrencyID ,
                  TotalPaymentFBCurrencyID ,
                  ClosingFBCurrencyID ,
                  BranchID ,
                  BranchName ,
                  RefTypeName ,
                  CAType ,
                  IsBold ,
                  EmployeeCode ,
                  EmployeeName,
				  OrderPriority
                )
                SELECT  RefID ,
                        RefType ,
                        RefDate ,
                        R.PostedDate ,
                        CASE WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        CASE WHEN TotalPaymentFBCurrencyID
                                  - TotalReceiptFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        R.PostedDate ,
                        ContactName ,
                        JournalMemo ,
                        @CurrencyID ,
                        SUM(CASE WHEN TotalReceiptFBCurrencyID
                                      - TotalPaymentFBCurrencyID > 0
                                 THEN TotalReceiptFBCurrencyID
                                      - TotalPaymentFBCurrencyID
                                 ELSE 0
                            END) AS TotalReceiptFBCurrencyID ,
                        SUM(CASE WHEN TotalPaymentFBCurrencyID
                                      - TotalReceiptFBCurrencyID > 0
                                 THEN TotalPaymentFBCurrencyID
                                      - TotalReceiptFBCurrencyID
                                 ELSE 0
                            END) AS TotalPaymentFBCurrencyID ,
                        SUM(ClosingFBCurrencyID) ,
                        BranchID ,
                        BranchName ,
                        RefTypeName ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END AS CAType ,
                        IsBold ,
                        EmployeeCode ,
                        EmployeeName,
						OrderPriority
                FROM    ( SELECT    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ReferenceID
                                    END AS RefID ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.TypeID
                                    END AS RefType ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefDate
                                    END AS RefDate ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END AS PostedDate ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefNo
                                    END AS RefNo ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ContactName
                                    END AS ContactName ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN N'Số dư đầu kỳ'
                                         ELSE GL.Description
                                    END AS JournalMemo ,
                                    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal > 0
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) AS TotalReceiptFBCurrencyID ,
                                    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.CreditAmountOriginal
                                                  - GL.DebitAmountOriginal > 0
                                             THEN GL.CreditAmountOriginal
                                                  - GL.DebitAmountOriginal
                                             ELSE 0
                                        END) AS TotalPaymentFBCurrencyID ,
                                    SUM(CASE WHEN GL.PostedDate < @FromDate
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) AS ClosingFBCurrencyID ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.BranchID
                                    END AS BranchID ,
                                    null AS BranchName ,
                                    null RefTypeName ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE 0
                                    END AS CAType ,
                                    CASE WHEN GL.PostedDate < @FromDate THEN 1
                                         ELSE 0
                                    END AS IsBold ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectCode
                                    END AS EmployeeCode ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectName
                                    END AS EmployeeName,
									CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE OrderPriority
                                    END AS OrderPriority
                          FROM      dbo.GeneralLedger AS GL
                                    LEFT JOIN dbo.AccountingObject AS AO ON AO.ID = GL.EmployeeID
                          WHERE     ( GL.PostedDate <= @ToDate )
                                    AND ( @CurrencyID IS NULL
                                          OR GL.CurrencyID = @CurrencyID
                                        )
                                    AND GL.Account LIKE @AccountNumber
                                    + '%'
                          GROUP BY  CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ReferenceID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.TypeID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefNo
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ContactName
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN N'Số dư đầu kỳ'
                                         ELSE GL.Description
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.BranchID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE 0
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate THEN 1
                                         ELSE 0
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectCode
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectName
                                    END,
									 CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE OrderPriority
                                    END
                          HAVING    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal > 0
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) <> SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                              AND GL.CreditAmountOriginal
                                                              - GL.DebitAmountOriginal > 0
                                                         THEN GL.CreditAmountOriginal
                                                              - GL.DebitAmountOriginal
                                                         ELSE 0
                                                    END)
                                    OR SUM(CASE WHEN GL.PostedDate < @FromDate
                                                THEN GL.DebitAmountOriginal
                                                     - GL.CreditAmountOriginal
                                                ELSE 0
                                           END) <> 0
                        ) AS R
                GROUP BY R.RefID ,
                        R.RefType ,
                        R.RefDate ,
                        R.PostedDate ,
                        CASE WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        CASE WHEN TotalPaymentFBCurrencyID
                                  - TotalReceiptFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        R.RefNo ,
                        R.ContactName ,
                        R.JournalMemo ,
                        R.BranchID ,
                        R.BranchName ,
                        R.RefTypeName ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END ,
                        R.IsBold ,
                        R.EmployeeCode ,
                        R.EmployeeName,
						R.OrderPriority
                ORDER BY R.PostedDate,R.OrderPriority ,
                        R.RefDate ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END ,
                        R.RefNo	
	   END

        DECLARE @ClosingAmount AS DECIMAL(22, 8)
        SET @ClosingAmount = 0
        SELECT  @ClosingAmount = ClosingFBCurrencyID
        FROM    #Result
        WHERE   RefID IS NULL
        UPDATE  #Result
        SET     @ClosingAmount = @ClosingAmount
                + ISNULL(TotalReceiptFBCurrencyID, 0)
                - ISNULL(TotalPaymentFBCurrencyID, 0) ,
                ClosingFBCurrencyID = @ClosingAmount	
    
        SELECT  *
        FROM    #Result R       
    
        DROP TABLE #Result
        
    END


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO



ALTER PROCEDURE [dbo].[Proc_BA_GetBookDepositListDetail]
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @CurrencyID NVARCHAR(3) ,
    @AccountNumber NVARCHAR(25) ,
    @BankAccountID UNIQUEIDENTIFIER ,
    @IsSimilarSum BIT = 0 ,
    @IsSoftOrderVoucher BIT = 0
AS
    BEGIN
        SET NOCOUNT ON
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        CREATE TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
              BankAccount NVARCHAR(500)  ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25)  ,
              RefType INT ,
              JournalMemo NVARCHAR(512)  ,
              CorrespondingAccountNumber NVARCHAR(20)
                 ,
              ExchangeRate DECIMAL ,
              DebitAmountOC DECIMAL(18,4) ,
              DebitAmount DECIMAL ,
              CreditAmountOC DECIMAL(18,4) ,
              CreditAmount DECIMAL ,
              ClosingAmountOC DECIMAL(18,4) ,
              ClosingAmount DECIMAL ,
              AccountObjectCode NVARCHAR(255)
                 ,
              AccountObjectName NVARCHAR(512)
                 ,
              EmployeeCode NVARCHAR(255)  ,
              EmployeeName NVARCHAR(255)  ,
              ProjectWorkCode NVARCHAR(20)
                 ,
              ProjectWorkName NVARCHAR(128)
                 ,
              

              ExpenseItemCode NVARCHAR(20)
                 ,
              ExpenseItemName NVARCHAR(128)
                 ,
              ListItemCode NVARCHAR(20)  ,
              ListItemName NVARCHAR(128)  ,
              ContractCode NVARCHAR(50)  ,

              BranchID UNIQUEIDENTIFIER ,
              BranchCode NVARCHAR(25)  ,
              BranchName NVARCHAR(255)  ,
              IsBold BIT ,
              PaymentType INT ,
              OrderType INT ,
              RefOrder INT,
			  OrderPriority INT
            )	
		
        CREATE TABLE #Result1
            (
              RowNum INT PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
              BankAccount NVARCHAR(500)  ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(512),
              CorrespondingAccountNumber NVARCHAR(20) ,
              ExchangeRate DECIMAL ,
              DebitAmountOC DECIMAL(18,4),
              DebitAmount DECIMAL ,
              CreditAmountOC DECIMAL(18,4) ,
              CreditAmount DECIMAL ,
              ClosingAmountOC DECIMAL(18,4) ,
              ClosingAmount DECIMAL ,
              AccountObjectCode NVARCHAR(255)
                 ,
              AccountObjectName NVARCHAR(512)
                 ,
              EmployeeCode NVARCHAR(255)  ,
              EmployeeName NVARCHAR(255)  ,

              ProjectWorkCode NVARCHAR(20) ,
              ProjectWorkName NVARCHAR(128) ,
              

              ExpenseItemCode NVARCHAR(20) ,
              ExpenseItemName NVARCHAR(128) ,
              ListItemCode NVARCHAR(20) ,
              ListItemName NVARCHAR(128)  ,
              ContractCode NVARCHAR(50) ,

              BranchID UNIQUEIDENTIFIER ,
              BranchCode NVARCHAR(25)  ,
              BranchName NVARCHAR(255)  ,
              IsBold BIT ,
              PaymentType INT ,
              OrderType INT ,
              RefOrder INT,
			  OrderPriority INT
            )	
		
        DECLARE @BankAccountAll UNIQUEIDENTIFIER/*tất cả tài khoản ngân hàng*/
        SET @BankAccountAll = 'A0624CFA-D105-422f-BF20-11F246704DC3'
		
        DECLARE @AccountNumberPercent NVARCHAR(25)
        SET @AccountNumberPercent = @AccountNumber + '%'
    
        DECLARE @ClosingAmountOC DECIMAL(29, 4)
        DECLARE @ClosingAmount DECIMAL(29, 4)
   	    DECLARE @tblListBankAccountDetail TABLE
            (
			  ID UNIQUEIDENTIFIER,
              BankAccount NVARCHAR(Max) ,
              BankAccountName NVARCHAR(MAX) 
            ) 
        if(@BankAccountID is null)     
        INSERT  INTO @tblListBankAccountDetail
                SELECT  BAD.ID,
				        BAD.BankAccount,
				        BAD.BankName
                FROM    BankAccountDetail BAD,
				        Bank BA
                WHERE BAD.BankID = BA.ID    
		else 	
			INSERT  INTO @tblListBankAccountDetail
                SELECT  BAD.ID,
				        BAD.BankAccount,
				        BAD.BankName
                FROM    BankAccountDetail BAD,
				        Bank BA
                WHERE BAD.BankID = BA.ID  
				and BAD.ID =  @BankAccountID 
	

        IF @ClosingAmount IS NULL
            SET @ClosingAmount = 0
        IF @ClosingAmountOC IS NULL
            SET @ClosingAmountOC = 0
            DECLARE @BankAccount NVARCHAR(500)
        DECLARE @CloseAmountOC MONEY
        DECLARE @CloseAmount MONEY
	  IF(@CurrencyID = 'VND')
	  BEGIN
	   INSERT  #Result
                ( BankAccount ,
                  JournalMemo ,
                  DebitAmount ,
                  DebitAmountOC ,
                  CreditAmount ,
                  CreditAmountOC ,
                  ClosingAmountOC ,
                  ClosingAmount ,
                  IsBold ,
                  OrderType   
                )
                SELECT  
		                BA.BankAccount + ' - ' + BA.BankAccountName,
                        N'Số dư đầu kỳ' AS JournalMemo ,
                        $0 AS DebitAmountOC ,
                        $0 AS DebitAmount ,
                        $0 AS CreditAmountOC ,
                        $0 AS CreditAmount ,
                        ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) ,
                        ISNULL(SUM(GL.DebitAmount - GL.CreditAmount), 0) ,
                        1 ,
                        0
                FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
				        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID
                WHERE   ( GL.PostedDate < @FromDate )
                        AND GL.Account LIKE @AccountNumberPercent
                GROUP BY BA.BankAccount , BA.BankAccountName
                HAVING  ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) <> 0
                        OR ISNULL(SUM(GL.DebitAmount - GL.CreditAmount), 0) <> 0

       INSERT  #Result
	   (
              RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder,
			  OrderPriority )
                    SELECT  GL.ReferenceID ,
	                        BA.BankAccount + ' - ' + BA.BankAccountName,
                            GL.PostedDate ,
                            GL.RefDate ,
                            GL.RefNo AS RefNoFinance ,

                            GL.Description,
                            GL.AccountCorresponding ,
                            GL.ExchangeRate ,
                            ISNULL(GL.DebitAmountOriginal, 0) AS DebitAmountOC ,
                            ISNULL(GL.DebitAmount, 0) AS DebitAmount ,
                            ISNULL(GL.CreditAmountOriginal, 0) AS CreditAmountOC ,
                            ISNULL(GL.CreditAmount, 0) AS CreditAmount ,
                            $0 AS ClosingAmountOC ,
                            $0 AS ClosingAmount ,
                            AO.AccountingObjectCode ,
                            AO.AccountingObjectName AS AccountObjectName ,
                            EI.ExpenseItemCode ,
                            EI.ExpenseItemName ,
                            GL.BranchID ,
                            0 ,
                            CASE WHEN GL.DebitAmount <> 0 THEN 0
                                 ELSE 1
                            END AS PaymentType ,
                            1 ,
                            0,
							null
                    FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
					        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID


                            LEFT JOIN dbo.AccountingObject AO ON GL.AccountingObjectID = AO.ID
                            LEFT JOIN dbo.ExpenseItem EI ON EI.ID = GL.ExpenseItemID
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND GL.Account LIKE @AccountNumberPercent
                           
                            AND ( GL.DebitAmountOriginal <> 0
                                  OR GL.CreditAmountOriginal <> 0
                                  OR GL.DebitAmount <> 0
                                  OR GL.CreditAmount <> 0
                                )
                    ORDER BY BA.BankAccount ,
                            PostedDate ,OrderPriority,
                            RefDate ,
                            PaymentType ,
                            RefNo
                    
  
        SET @BankAccount = NULL
        INSERT  INTO #Result1
		(     RowNum,
		      RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder,
			  OrderPriority)
                SELECT  ROW_NUMBER() OVER ( ORDER BY BankAccount , OrderType, PostedDate , CASE
                                                              WHEN @IsSoftOrderVoucher = 1
                                                              THEN RefOrder
                                                              ELSE 0
                                                              END, RefDate , PaymentType , RefNo ) ,
                        RefID ,
                        BankAccount ,
                        PostedDate ,
                        RefDate ,
                        RefNo ,
                        JournalMemo ,
                        CorrespondingAccountNumber ,
                        ExchangeRate ,
                        DebitAmountOC ,
                        DebitAmount ,
                        CreditAmountOC ,
                        CreditAmount ,
                        ClosingAmountOC ,
                        ClosingAmount ,
                        AccountObjectCode ,
                        AccountObjectName ,

                        ExpenseItemCode ,
                        ExpenseItemName ,
                        BranchID ,
						IsBold  ,
					    PaymentType  ,
						OrderType  ,
						RefOrder,
						OrderPriority
                FROM    #Result
                ORDER BY BankAccount ,
                        OrderType ,
                        PostedDate ,OrderPriority,
                        CASE WHEN @IsSoftOrderVoucher = 1 THEN RefOrder
                             ELSE 0
                        END ,
                        RefDate ,
                        PaymentType ,
                        RefNo
	  END
	  ELSE
	  BEGIN
        INSERT  #Result
                ( BankAccount ,
                  JournalMemo ,
                  DebitAmount ,
                  DebitAmountOC ,
                  CreditAmount ,
                  CreditAmountOC ,
                  ClosingAmountOC ,
                  ClosingAmount ,
                  IsBold ,
                  OrderType   
                )
                SELECT  
		                BA.BankAccount + ' - ' + BA.BankAccountName,
                        N'Số dư đầu kỳ' AS JournalMemo ,
                        $0 AS DebitAmountOC ,
                        $0 AS DebitAmount ,
                        $0 AS CreditAmountOC ,
                        $0 AS CreditAmount ,
                        ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) ,
                        ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) ,
                        1 ,
                        0
                FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
				        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID
                WHERE   ( GL.PostedDate < @FromDate )
                        AND GL.Account LIKE @AccountNumberPercent
                        AND ( @CurrencyID IS NULL
                              OR GL.CurrencyID = @CurrencyID
                            )
                GROUP BY BA.BankAccount , BA.BankAccountName
                HAVING  ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) <> 0
                        OR ISNULL(SUM(GL.DebitAmount - GL.CreditAmount), 0) <> 0

       INSERT  #Result
	   (
              RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder,
			  OrderPriority )
                    SELECT  GL.ReferenceID ,
	                        BA.BankAccount + ' - ' + BA.BankAccountName,
                            GL.PostedDate ,
                            GL.RefDate ,
                            GL.RefNo AS RefNoFinance ,

                            GL.Description,
                            GL.AccountCorresponding ,
                            GL.ExchangeRate ,
                            ISNULL(GL.DebitAmountOriginal, 0) AS DebitAmountOC ,
                            ISNULL(GL.DebitAmountOriginal, 0) AS DebitAmount ,
                            ISNULL(GL.CreditAmountOriginal, 0) AS CreditAmountOC ,
                            ISNULL(GL.CreditAmountOriginal, 0) AS CreditAmount ,
                            $0 AS ClosingAmountOC ,
                            $0 AS ClosingAmount ,
                            AO.AccountingObjectCode ,
                            AO.AccountingObjectName AS AccountObjectName ,
                            EI.ExpenseItemCode ,
                            EI.ExpenseItemName ,
                            GL.BranchID ,
                            0 ,
                            CASE WHEN GL.DebitAmount <> 0 THEN 0
                                 ELSE 1
                            END AS PaymentType ,
                            1 ,
                            0,
							null
                    FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
					        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID


                            LEFT JOIN dbo.AccountingObject AO ON GL.AccountingObjectID = AO.ID
                            LEFT JOIN dbo.ExpenseItem EI ON EI.ID = GL.ExpenseItemID
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND GL.Account LIKE @AccountNumberPercent
                            AND ( @CurrencyID IS NULL
                                  OR GL.CurrencyID = @CurrencyID
                                )
                            AND ( GL.DebitAmountOriginal <> 0
                                  OR GL.CreditAmountOriginal <> 0
                                  OR GL.DebitAmount <> 0
                                  OR GL.CreditAmount <> 0
                                )
                    ORDER BY BA.BankAccount ,
                            PostedDate ,OrderPriority,
                            RefDate ,
                            PaymentType ,
                            RefNo
                    
        SET @BankAccount = NULL
        INSERT  INTO #Result1
		(     RowNum,
		      RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder,
			  OrderPriority)
                SELECT  ROW_NUMBER() OVER ( ORDER BY BankAccount , OrderType, PostedDate , CASE
                                                              WHEN @IsSoftOrderVoucher = 1
                                                              THEN RefOrder
                                                              ELSE 0
                                                              END, RefDate , PaymentType , RefNo ) ,
                        RefID ,
                        BankAccount ,
                        PostedDate ,
                        RefDate ,
                        RefNo ,
                        JournalMemo ,
                        CorrespondingAccountNumber ,
                        ExchangeRate ,
                        DebitAmountOC ,
                        DebitAmount ,
                        CreditAmountOC ,
                        CreditAmount ,
                        ClosingAmountOC ,
                        ClosingAmount ,
                        AccountObjectCode ,
                        AccountObjectName ,

                        ExpenseItemCode ,
                        ExpenseItemName ,
                        BranchID ,
						IsBold  ,
					    PaymentType  ,
						OrderType  ,
						RefOrder,
						OrderPriority
                FROM    #Result
                ORDER BY BankAccount ,
                        OrderType ,
                        PostedDate ,OrderPriority,
                        CASE WHEN @IsSoftOrderVoucher = 1 THEN RefOrder
                             ELSE 0
                        END ,
                        RefDate ,
                        PaymentType ,
                        RefNo
        END
      
        SET @CloseAmount = 0
        SET @CloseAmountOC = 0
        UPDATE  #Result1
        SET

                @CloseAmount = ( CASE WHEN OrderType = 0
                                      THEN ( CASE WHEN ClosingAmount = 0
                                                  THEN 0
                                                  ELSE ClosingAmount
                                             END )
                                      WHEN @BankAccount <> BankAccount
                                      THEN DebitAmount - CreditAmount
                                      ELSE @CloseAmount + DebitAmount
                                           - CreditAmount
                                 END ) ,
                @CloseAmountOC = ( CASE WHEN OrderType = 0
                                        THEN ( CASE WHEN ClosingAmountOC = 0
                                                    THEN 0
                                                    ELSE ClosingAmountOC
                                               END )
                                        WHEN @BankAccount <> BankAccount
                                        THEN DebitAmountOC - CreditAmountOC
                                        ELSE @CloseAmountOC + DebitAmountOC
                                             - CreditAmountOC
                                   END ) ,
                ClosingAmount = @CloseAmount ,
                ClosingAmountOC = @CloseAmountOC ,
                @BankAccount = BankAccount
        
        SELECT  *
        FROM    #Result1
        ORDER BY BankAccount ,
                OrderType ,
                PostedDate ,OrderPriority,
                CASE WHEN @IsSoftOrderVoucher = 1 THEN RefOrder
                     ELSE 0
                END ,
                RefDate ,
                PaymentType ,
                RefNo
     
        DROP TABLE #Result
        DROP TABLE #Result1
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER FUNCTION [dbo].[Func_GetB03_DN]
(
	@BranchID UNIQUEIDENTIFIER
	,@IncludeDependentBranch BIT
	,@FromDate DATETIME
	,@ToDate DATETIME
	,@PrevFromDate DATETIME
	,@PrevToDate DATETIME	
)
RETURNS 
@Result TABLE 
(	
		ItemID UNIQUEIDENTIFIER
      ,ItemCode NVARCHAR(25)
      ,ItemName NVARCHAR(512)
      ,ItemNameEnglish NVARCHAR(512)
      ,ItemIndex INT
      ,Description NVARCHAR(512)
      ,FormulaType INT
      ,FormulaFrontEnd NVARCHAR(MAX)
      ,Formula XML
      ,Hidden BIT
      ,IsBold BIT
      ,IsItalic BIT
      ,Amount DECIMAL(25,4)
      ,PrevAmount  DECIMAL(25,4)    
)
AS
BEGIN

	DECLARE @AccountingSystem INT	
    SET @AccountingSystem = 48
    
    set @PrevToDate = DATEADD(DD,-1,@FromDate)
	declare @CalPrevDate nvarchar(20)
	declare @CalPrevMonth nvarchar(20)
	declare @CalPrevDay nvarchar(20)
	set @CalPrevDate = CAST(day(@FromDate) AS varchar) + CAST(month(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevMonth = CAST(month(@FromDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevDay = CAST(day(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar)
	if @CalPrevDate = '113112'
	 set @PrevFromDate = convert(DATETIME,dbo.ctod('D',dbo.godtos('M', -12, @FromDate)),103)
	else if (@CalPrevMonth in (55,77,1010,1212) and @CalPrevDay in (131) )
	 set @PrevFromDate = DATEADD(DD,-30,@FromDate)
	else if (@CalPrevMonth in (11,22,88,44,66,99,1111) and @CalPrevDay in (131,130) )
	 set @PrevFromDate = DATEADD(DD,-31,@FromDate)
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 = 0)
	 set @PrevFromDate = DATEADD(DD,-29,@FromDate)
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 <> 0)
	 set @PrevFromDate = DATEADD(DD,-28,@FromDate)
	else if ( (@CalPrevMonth in (13,1012) and @CalPrevDay = '131') or (@CalPrevMonth in (46,79) and @CalPrevDay = '130') )
	 set @PrevFromDate = DATEADD(QQ,-1,@FromDate)
	else set @PrevFromDate = DATEADD(DD,-1-DATEDIFF(day, @FromDate, @ToDate),@FromDate)

	DECLARE @ReportID NVARCHAR(100)
	SET @ReportID = '3'
	
	DECLARE @ItemBeginingCash UNIQUEIDENTIFIER	

	SET @ItemBeginingCash = (Select ItemID From FRTemplate Where ItemCode = '60' AND ReportID = 3 AND AccountingSystem = @AccountingSystem)

	DECLARE @tblItem TABLE
	(
		ItemID				UNIQUEIDENTIFIER
		,ItemIndex			INT
		,ItemName			NVARCHAR(512)	
		,OperationSign		INT
		,OperandString		NVARCHAR(512)	
		,AccountNumber		NVARCHAR(25)
		,AccountNumberPercent NVARCHAR(25) 
		,CorrespondingAccountNumber	VARCHAR(25)
	)
	
	INSERT @tblItem
	SELECT ItemID 
		, ItemIndex
		, ItemName
		, x.r.value('@OperationSign','INT')
		, x.r.value('@OperandString','nvarchar(512)')
		, x.r.value('@AccountNumber','nvarchar(25)') 
		, x.r.value('@AccountNumber','nvarchar(25)') + '%'
		, CASE WHEN x.r.value('@CorrespondingAccountNumber','nvarchar(25)') <>'' 
			THEN x.r.value('@CorrespondingAccountNumber','nvarchar(25)') + '%'
			ELSE ''
		 END
	FROM dbo.FRTemplate
	CROSS APPLY Formula.nodes('/root/DetailFormula') AS x(r)
	WHERE AccountingSystem = @AccountingSystem
		  AND ReportID = @ReportID
		  AND FormulaType = 0 AND Formula IS NOT NULL 
	
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/
		  
	DECLARE @Balance TABLE 
		(
			AccountNumber NVARCHAR(25)	
			,CorrespondingAccountNumber NVARCHAR(25)		
			,PrevBusinessAmount DECIMAL(25,4)
			,BusinessAmount DECIMAL(25,4)							
			,PrevInvestmentAmount DECIMAL(25,4)
			,InvestmentAmount DECIMAL(25,4)
			,PrevFinancialAmount DECIMAL(25,4)
			,FinancialAmount DECIMAL(25,4)
		)			
		
	INSERT INTO @Balance			
	SELECT 
		GL.Account
		,GL.AccountCorresponding		
		,SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND(AC.ActivityID IS NULL OR AC.ActivityID = 0) 
				THEN DebitAmount  ELSE 0 END) AS PrevBusinessAmount
		,SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND (AC.ActivityID IS NULL OR AC.ActivityID = 0) 
				THEN DebitAmount ELSE 0 END) AS BusinessAmount		
		,SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND  AC.ActivityID = 1 
				THEN DebitAmount  ELSE 0 END) AS PrevInvestmentAmount
		,SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 1 
				THEN DebitAmount ELSE 0 END) AS InvestmentAmount		
		,SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND AC.ActivityID = 2 
				THEN DebitAmount  ELSE 0 END) AS PrevFinancialAmount
		,SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 2 
				THEN DebitAmount ELSE 0 END) AS FinancialAmount		
	FROM @tbDataGL GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
		INNER JOIN dbo.Account A ON GL.Account = A.AccountNumber
		LEFT JOIN [dbo].[FRB03ReportDetailActivity] AC   ON GL.DetailID = Ac.RefDetailID
													/*Khi JOIN thêm điều kiện sổ*/
	WHERE PostedDate BETWEEN @PrevFromDate AND @ToDate	
	and GL.No <> 'OPN'
	GROUP BY
		GL.Account
		,GL.AccountCorresponding
	HAVING
		SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND(AC.ActivityID IS NULL OR AC.ActivityID = 0) 
			THEN DebitAmount  ELSE 0 END)<>0
		OR SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND (AC.ActivityID IS NULL OR AC.ActivityID = 0) 
			THEN DebitAmount ELSE 0 END) <>0
		OR SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND  AC.ActivityID = 1 
			THEN DebitAmount  ELSE 0 END) <>0
		OR SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 1 
			THEN DebitAmount ELSE 0 END)<>0
		OR SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND AC.ActivityID = 2 
			THEN DebitAmount  ELSE 0 END) <>0
		OR SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 2 
			THEN DebitAmount ELSE 0 END) <>0
	
	DECLARE @DebitBalance TABLE
	(	
		ItemID				UNIQUEIDENTIFIER
		,OperationSign		INT
		,AccountNumber NVARCHAR(25)
		,AccountKind	INT
		,PrevDebitAmount Decimal(25,4)
		,DebitAmount Decimal(25,4)
	)
	
	INSERT @DebitBalance
	SELECT
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
		,SUM(CASE WHEN  GL.PostedDate < @PrevFromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0 END)	
		,SUM(GL.DebitAmount - GL.CreditAmount)		
	FROM  @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
	INNER JOIN @tblItem I ON GL.Account LIKE I.AccountNumberPercent	
	LEFT JOIN dbo.Account A ON A.AccountNumber = I.AccountNumber
	WHERE GL.PostedDate < @FromDate	
	    AND I.OperandString = 'DUNO'
	    AND I.ItemID = @ItemBeginingCash
	Group By
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
	
	UNION ALL	
	SELECT
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
		,SUM(CASE WHEN  GL.PostedDate <= @PrevToDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0 END)	
		,SUM(GL.DebitAmount - GL.CreditAmount)		
	FROM  @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
	INNER JOIN @tblItem I ON GL.Account LIKE I.AccountNumberPercent	
	LEFT JOIN dbo.Account A ON A.AccountNumber = I.AccountNumber
	WHERE GL.PostedDate <= @ToDate
	    AND I.OperandString = 'DUNO'
	    AND I.ItemID <> @ItemBeginingCash
	Group By
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
		
		   
	UPDATE @DebitBalance
	SET PrevDebitAmount = 0 WHERE AccountKind = 1 OR (AccountKind NOT IN (0,1)  AND PrevDebitAmount < 0) 
	
	UPDATE @DebitBalance
	SET DebitAmount = 0 WHERE AccountKind = 1 OR (AccountKind NOT IN (0,1)  AND DebitAmount < 0) 
	
	DECLARE @tblMasterDetail Table
		(
			ItemID UNIQUEIDENTIFIER
			,DetailItemID UNIQUEIDENTIFIER
			,OperationSign INT
			,Grade INT
			,PrevAmount DECIMAL(25,4)
			,Amount DECIMAL(25,4)		
		)	
	
	
	INSERT INTO @tblMasterDetail
	SELECT I.ItemID
			,NULL
			,1
			,-1
			,SUM(I.PrevAmount)
			,SUM(I.Amount)
	FROM	
		(SELECT			
			I.ItemID
			,SUM(CASE				
					WHEN I.OperandString = 'PhatsinhDU' THEN b.PrevBusinessAmount + b.PrevInvestmentAmount + b.PrevFinancialAmount
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_DAUTU' THEN b.PrevInvestmentAmount
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_TAICHINH' THEN b.PrevFinancialAmount
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_SXKD' THEN b.PrevBusinessAmount
				 END * I.OperationSign) AS  PrevAmount			
			,SUM(CASE 
					WHEN I.OperandString = 'PhatsinhDU' THEN (b.BusinessAmount + b.InvestmentAmount + b.FinancialAmount)
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_DAUTU' THEN (b.InvestmentAmount)
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_TAICHINH' THEN (b.FinancialAmount)
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_SXKD' THEN (b.BusinessAmount)
			    END * I.OperationSign) AS Amount
			FROM @tblItem I 			
				 INNER JOIN @Balance B 
					ON (B.AccountNumber like I.AccountNumberPercent)
					AND (B.CorrespondingAccountNumber like I.CorrespondingAccountNumber)	
				WHERE I.OperandString IN ('PhatsinhDU','PhatsinhDUChiTietTheoHD_DAUTU','PhatsinhDUChiTietTheoHD_TAICHINH','PhatsinhDUChiTietTheoHD_SXKD')					
			GROUP BY I.ItemID
		UNION ALL
			SELECT
		    I.ItemID
			,SUM((b.PrevBusinessAmount + b.PrevInvestmentAmount + b.PrevFinancialAmount)* I.OperationSign) AS  PrevAmount			
			,SUM((b.BusinessAmount + b.InvestmentAmount + b.FinancialAmount)* I.OperationSign) AS Amount
			FROM @tblItem I 
				 INNER JOIN @Balance B 
					ON (B.AccountNumber like I.AccountNumberPercent)
			WHERE I.OperandString = 'PhatsinhNO'				
			GROUP BY I.ItemID
		UNION ALL
			SELECT
		    I.ItemID
			,SUM((b.PrevBusinessAmount + b.PrevInvestmentAmount + b.PrevFinancialAmount)* I.OperationSign) AS  PrevAmount			
			,SUM((b.BusinessAmount + b.InvestmentAmount + b.FinancialAmount)* I.OperationSign) AS Amount
			FROM @tblItem I 
				 INNER JOIN @Balance B 
					ON (B.CorrespondingAccountNumber like I.AccountNumberPercent)
			WHERE I.OperandString = 'PhatsinhCO'		
					
			GROUP BY I.ItemID
			
		UNION ALL
			SELECT I.ItemID
				,I.PrevDebitAmount * I.OperationSign
				,I.DebitAmount * I.OperationSign
			FROM @DebitBalance I
		 ) AS I
	Group By I.ITemID
		
	
		
	
	       
	    
	INSERT @tblMasterDetail	
	SELECT ItemID 
	, x.r.value('@ItemID','NVARCHAR(100)')
	, x.r.value('@OperationSign','INT')	
	, 0
	, 0.0	
	, 0.0
	FROM dbo.FRTemplate 
	CROSS APPLY Formula.nodes('/root/MasterFormula') AS x(r)
	WHERE AccountingSystem = @AccountingSystem
		  AND ReportID = @ReportID
		  AND FormulaType = 1 AND Formula IS NOT NULL 
	
	;
	WITH V(ItemID,DetailItemID,PrevAmount,Amount,OperationSign)
		AS 
		(
			SELECT ItemID,DetailItemID,PrevAmount, Amount, OperationSign
			FROM @tblMasterDetail WHERE Grade = -1 
			UNION ALL
			SELECT B.ItemID
				, B.DetailItemID				
				, V.PrevAmount
				, V.Amount
				, B.OperationSign * V.OperationSign AS OperationSign
			FROM @tblMasterDetail B, V 
			WHERE B.DetailItemID = V.ItemID	
		)
		
	INSERT @Result      
	SELECT 
		FR.ItemID
		, FR.ItemCode
		, FR.ItemName
		, FR.ItemNameEnglish
		, FR.ItemIndex
		, FR.Description		
		, FR.FormulaType
		, CASE WHEN FR.FormulaType = 1 THEN FR.FormulaFrontEnd	ELSE ''	END AS FormulaFrontEnd
		, CASE WHEN FR.FormulaType = 1 THEN FR.Formula ELSE NULL END AS Formula
		, FR.Hidden
		, FR.IsBold
		, FR.IsItalic
		,CASE WHEN FR.Formula IS NOT NULL THEN ISNULL(X.Amount,0)
			ELSE X.Amount
		 END AS Amount
		,CASE WHEN FR.Formula IS NOT NULL THEN ISNULL(X.PrevAmount,0) 
			ELSE X.PrevAmount 
		END AS PrevAmount
		FROM		
		(
			SELECT V.ItemID
				,SUM(V.OperationSign * V.Amount)  AS Amount
				,SUM(V.OperationSign * V.PrevAmount) AS PrevAmount
			FROM V
			GROUP BY ItemID
		) AS X		
		RIGHT JOIN dbo.FRTemplate FR ON FR.ItemID = X.ItemID	
	WHERE AccountingSystem = @AccountingSystem
		  AND ReportID = @ReportID
		  and Hidden = 0
	Order by ItemIndex	
	RETURN 
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER FUNCTION [dbo].[Func_GetB02_DN]
    (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @PrevFromDate DATETIME ,
      @PrevToDate DATETIME
    )
RETURNS @Result TABLE
    (
      ItemID UNIQUEIDENTIFIER ,
      ItemCode NVARCHAR(25) ,
      ItemName NVARCHAR(512) ,
      ItemNameEnglish NVARCHAR(512) ,
      ItemIndex INT ,
      Description NVARCHAR(512) ,
      FormulaType INT ,
      FormulaFrontEnd NVARCHAR(MAX) ,
      Formula XML ,
      Hidden BIT ,
      IsBold BIT ,
      IsItalic BIT
      ,
      Amount DECIMAL(25, 4) ,
      PrevAmount DECIMAL(25, 4)
    )
AS 
    BEGIN
	DECLARE @AccountingSystem INT	
            SET @AccountingSystem = 48
        DECLARE @ReportID NVARCHAR(100)
        SET @ReportID = '2'/*báo cáo kết quả HĐ kinh doanh*/
    set @PrevToDate = DATEADD(DD,-1,@FromDate)
	declare @CalPrevDate nvarchar(20)
	declare @CalPrevMonth nvarchar(20)
	declare @CalPrevDay nvarchar(20)
	set @CalPrevDate = CAST(day(@FromDate) AS varchar) + CAST(month(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevMonth = CAST(month(@FromDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevDay = CAST(day(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar)
	if @CalPrevDate = '113112'
	 set @PrevFromDate = convert(DATETIME,dbo.ctod('D',dbo.godtos('M', -12, @FromDate)),103)
	else if (@CalPrevMonth in (55,77,1010,1212) and @CalPrevDay in (131) )
	 set @PrevFromDate = DATEADD(DD,-30,@FromDate)
	else if (@CalPrevMonth in (11,22,88,44,66,99,1111) and @CalPrevDay in (131,130) )
	 set @PrevFromDate = DATEADD(DD,-31,@FromDate)
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 = 0)
	 set @PrevFromDate = DATEADD(DD,-29,@FromDate)
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 <> 0)
	 set @PrevFromDate = DATEADD(DD,-28,@FromDate)
	else if ( (@CalPrevMonth in (13,1012) and @CalPrevDay = '131') or (@CalPrevMonth in (46,79) and @CalPrevDay = '130') )
	 set @PrevFromDate = DATEADD(QQ,-1,@FromDate)
	else set @PrevFromDate = DATEADD(DD,-1-DATEDIFF(day, @FromDate, @ToDate),@FromDate)
	
        DECLARE @tblItem TABLE
            (
              ItemID UNIQUEIDENTIFIER ,
              ItemName NVARCHAR(255) ,
              OperationSign INT ,
              OperandString NVARCHAR(512) ,
              AccountNumber VARCHAR(25) ,
              CorrespondingAccountNumber VARCHAR(25) ,
              IsDetailGreaterThanZero BIT
	      )
	
        INSERT  @tblItem
                SELECT  ItemID ,
                        ItemName ,
                        x.r.value('@OperationSign', 'INT') ,
                        x.r.value('@OperandString', 'nvarchar(512)') ,
                        x.r.value('@AccountNumber', 'nvarchar(25)') + '%' ,
                        CASE WHEN x.r.value('@CorrespondingAccountNumber',
                                            'nvarchar(25)') <> ''
                             THEN x.r.value('@CorrespondingAccountNumber',
                                            'nvarchar(25)') + '%'
                             ELSE ''
                        END ,
                        CASE WHEN FormulaType = 0 THEN CAST(0 AS BIT)
                             ELSE CAST(1 AS BIT)
                        END
                FROM    FRTemplate
                        CROSS APPLY Formula.nodes('/root/DetailFormula') AS x ( r )
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
                        AND ( FormulaType = 0/*chỉ tiêu chi tiết*/
                              OR FormulaType = 2/*chỉ tiêu chi tiết chỉ được lấy số liệu khi kết quả>0*/
                            )
                        AND Formula IS NOT NULL
		
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate BETWEEN @PrevFromDate AND @ToDate
		/*end add by cuongpv*/

        DECLARE @Balance TABLE
            (
              AccountNumber NVARCHAR(25) ,
              CorrespondingAccountNumber NVARCHAR(25) ,
              PostedDate DATETIME ,
              CreditAmount DECIMAL(25, 4) ,
              DebitAmount DECIMAL(25, 4) ,
              CreditAmountDetailBy DECIMAL(25, 4) ,
              DebitAmountDetailBy DECIMAL(25, 4) ,
              IsDetailBy BIT,
              /*add by hoant 16.09.2016 theo cr 116741*/
                CreditAmountByBussinessType0 DECIMAL(25, 4) ,/*Có Chi tiết theo loại  0-Chiết khấu thương mai*/
               CreditAmountByBussinessType1 DECIMAL(25, 4) ,/*Có Chi tiết theo loại  1 - Giảm giá hàng bán*/
               CreditAmountByBussinessType2 DECIMAL(25, 4) ,/*Có Chi tiết theo loại   2- trả lại hàng bán*/
              DebitAmountByBussinessType0 DECIMAL(25, 4) ,/*Nợ Chi tiết theo loại  0-Chiết khấu thương mai*/
              DebitAmountByBussinessType1 DECIMAL(25, 4) ,/*Nợ Chi tiết theo loại  1 - Giảm giá hàng bán*/
              DebitAmountByBussinessType2 DECIMAL(25, 4) /*Nợ Chi tiết theo loại   2- trả lại hàng bán*/
            )			
/*
1. Doanh thu bán hàng và cung cấp dịch vụ

PS Có TK 511 (không kể PSĐƯ N911/C511, không kể các nghiệp vụ: Chiết khấu thương mại (bán hàng),

 Giảm giá hàng bán, Trả lại hàng bán)
  – PS Nợ TK 511 (không kể PSĐƯ N511/911, không kể các nghiệp vụ: Chiết khấu thương mại (bán hàng), 
  Giảm giá hàng bán, Trả lại hàng bán)
*/
		
        INSERT  INTO @Balance
                SELECT  GL.Account ,
                        GL.AccountCorresponding ,
                        GL.PostedDate ,
                        SUM(ISNULL(CreditAmount, 0)) AS CreditAmount ,
                        SUM(ISNULL(DebitAmount, 0)) AS DebitAmount ,
                        0 AS CreditAmountDetailBy ,
                        0 AS DebitAmountDetailBy ,
                        0,
                        SUM(CASE WHEN (GL.TypeID in ('320','321','322','323','324','325','350','360') and gl.Account like '511%')
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType0 ,
                        SUM(CASE WHEN GL.TypeID =340
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType1 
                            ,
                        SUM(CASE WHEN GL.TypeID =330
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType2 ,
                        SUM(CASE WHEN (GL.TypeID in ('320','321','322','323','324','325','350','360') and gl.Account like '511%')
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType0
                            ,
                        SUM(CASE WHEN GL.TypeID =340
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType1
                            ,
                        SUM(CASE WHEN GL.TypeID =330
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType2
                FROM    @tbDataGL GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                WHERE   PostedDate BETWEEN @PrevFromDate AND @ToDate
                GROUP BY GL.Account ,
                        GL.AccountCorresponding,
                        GL.PostedDate                     
        OPTION  ( RECOMPILE )


        DECLARE @tblMasterDetail TABLE
            (
              ItemID UNIQUEIDENTIFIER ,
              DetailItemID UNIQUEIDENTIFIER ,
              OperationSign INT ,
              Grade INT ,
              DebitAmount DECIMAL(25, 4) ,
              PrevDebitAmount DECIMAL(25, 4)
            )	
	
	
	/*
	PhatsinhCO(511) - PhatsinhDU(911/511) + PhatsinhCO_ChitietChietKhauThuongmai(511)
	 + PhatsinhCO_ChitietGiamgiaHangBan(511) + PhatsinhCO_ChitietTralaiHangBan(511) +
	  PhatsinhNO(511) - PhatsinhDU(511/911) - PhatsinhNO_ChitietChietKhauThuongmai(511) - PhatsinhNO_ChitietGiamgiaHangBan(511) - PhatsinhNO_ChitietTralaiHangBan(511)
	*/
        INSERT  INTO @tblMasterDetail
                SELECT  I.ItemID ,
                        NULL ,
                        1 ,
                        -1
                        ,
                        CASE WHEN I.IsDetailGreaterThanZero = 0
                                  OR ( SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                                THEN ( CASE WHEN I.OperandString = 'PhatsinhCO'
                                                            THEN GL.CreditAmount
                                                            WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                            THEN GL.DebitAmount
                                                        
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                        
                                                            WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
															 THEN GL.CreditAmount
															 WHEN I.OperandString IN (
																  'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
															 THEN GL.DebitAmount
															 ELSE 0
														
                                                       END ) * I.OperationSign
                                                ELSE 0
                                           END) ) > 0
                             THEN ( SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                             THEN ( CASE WHEN I.OperandString = 'PhatsinhCO'
                                                         THEN GL.CreditAmount
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                         THEN GL.DebitAmount
                                                    
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                     
                                                              
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.CreditAmount
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.DebitAmount
                                                         ELSE 0
                                                    END ) * I.OperationSign
                                             ELSE 0
                                        END) )
                             ELSE 0
                        END AS DebitAmount ,
                        CASE WHEN I.IsDetailGreaterThanZero = 0
                                  OR ( SUM(CASE WHEN GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                                THEN CASE WHEN I.OperandString = 'PhatsinhCO'
                                                          THEN GL.CreditAmount
                                                          WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                          THEN GL.DebitAmount
            
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                         
                                                          WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.CreditAmount
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.DebitAmount
                                                         ELSE 0    
                                                     END * I.OperationSign
                                                ELSE 0
                                           END) ) > 0
                             THEN SUM(CASE WHEN GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                           THEN CASE WHEN I.OperandString = 'PhatsinhCO'
                                                     THEN GL.CreditAmount
                                                     WHEN I.OperandString IN (
                                                          'PhatsinhNO',
                                                          'PhatsinhDU' )
                                                     THEN GL.DebitAmount
                                                     
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                              
                                                              
                                                              
                                                     WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.CreditAmount
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.DebitAmount
                                                         ELSE 0
                                                END * I.OperationSign
                                           ELSE 0
                                      END)
                             ELSE 0
                        END AS DebitAmount
                FROM    @tblItem I
                        INNER JOIN @Balance AS GL ON ( GL.AccountNumber LIKE I.AccountNumber )
                                                     AND ( I.OperandString <> 'PhatsinhDU'
                                                           OR ( I.OperandString = 'PhatsinhDU'
                                                              AND GL.CorrespondingAccountNumber LIKE I.CorrespondingAccountNumber
                                                              )
                                                         )
                GROUP BY I.ItemID ,
                        I.IsDetailGreaterThanZero
        OPTION  ( RECOMPILE )
			
        INSERT  @tblMasterDetail
                SELECT  ItemID ,
                        x.r.value('@ItemID', 'UNIQUEIDENTIFIER') ,
                        x.r.value('@OperationSign', 'INT') ,
                        0 ,
                        0.0 ,
                        0.0
                FROM    FRTemplate
                        CROSS APPLY Formula.nodes('/root/MasterFormula') AS x ( r )
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
                        AND FormulaType = 1
                        AND Formula IS NOT NULL 
	
	

	;
        WITH    V ( ItemID, DetailItemID, DebitAmount, PrevDebitAmount, OperationSign )
                  AS ( SELECT   ItemID ,
                                DetailItemID ,
                                DebitAmount ,
                                PrevDebitAmount ,
                                OperationSign
                       FROM     @tblMasterDetail
                       WHERE    Grade = -1
                       UNION ALL
                       SELECT   B.ItemID ,
                                B.DetailItemID ,
                                V.DebitAmount ,
                                V.PrevDebitAmount ,
                                B.OperationSign * V.OperationSign AS OperationSign
                       FROM     @tblMasterDetail B ,
                                V
                       WHERE    B.DetailItemID = V.ItemID
                     )
	INSERT    @Result
                SELECT  FR.ItemID ,
                        FR.ItemCode ,
                        FR.ItemName ,
                        FR.ItemNameEnglish ,
                        FR.ItemIndex ,
                        FR.Description ,
                        FR.FormulaType ,
                        CASE WHEN FR.FormulaType = 1 THEN FR.FormulaFrontEnd
                             ELSE ''
                        END AS FormulaFrontEnd ,
                        CASE WHEN FR.FormulaType = 1 THEN FR.Formula
                             ELSE NULL
                        END AS Formula ,
                        FR.Hidden ,
                        FR.IsBold ,
                        FR.IsItalic ,
                        ISNULL(X.Amount, 0) AS Amount ,
                        ISNULL(X.PrevAmount, 0) AS PrevAmount
                FROM    ( SELECT    V.ItemID ,
                                    SUM(V.OperationSign * V.DebitAmount) AS Amount ,
                                    SUM(V.OperationSign * V.PrevDebitAmount) AS PrevAmount
                          FROM      V
                          GROUP BY  ItemID
                        ) AS X
                        RIGHT JOIN dbo.FRTemplate FR ON FR.ItemID = X.ItemID
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
                ORDER BY ItemIndex
	
        RETURN 
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_S02C1DNN]
    @BranchID UNIQUEIDENTIFIER,
    @IncludeDependentBranch BIT,
    @StartDate DATETIME,
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountNumber NVARCHAR(MAX),
    @IsSimilarSum BIT
AS
    BEGIN
        SET NOCOUNT ON
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)
		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		CREATE TABLE #Result
            (
             RefID UNIQUEIDENTIFIER,
             RefType INT,
             PostedDate DATETIME,
             RefNo NVARCHAR(25),
             RefDate DATETIME,
             InvDate DATETIME,
             InvNo NVARCHAR(MAX),
             JournalMemo NVARCHAR(512),
             AccountNumber NVARCHAR(25),
			 DetailAccountNumber NVARCHAR(25) ,
             AccountCategoryKind INT,
             AccountName NVARCHAR(512),
             CorrespondingAccountNumber NVARCHAR(25),
             DebitAmount MONEY,
             CreditAmount MONEY,
             OrderType INT,
             IsBold BIT,
             OrderNumber int
            )
        CREATE TABLE #tblAccountNumber
            (
             AccountNumber NVARCHAR(25)    PRIMARY KEY,
             AccountName NVARCHAR(512)  ,
             AccountNumberPercent NVARCHAR(25)  ,
             AccountCategoryKind INT,
			 IsParent BIT
            )
        INSERT  #tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountName,
                        A1.AccountNumber + '%',
                        A1.AccountGroupKind,
						A1.IsParentNode
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        IF @IsSimilarSum = 0 /*khong cong gop but ke toan*/
		Begin
            INSERT  #Result
                    (
                     RefID,
                     RefType,
                     PostedDate,
                     RefNo,
                     RefDate,
                     InvDate,
                     InvNo,
                     JournalMemo,
                     AccountNumber,
					 DetailAccountNumber,
                     AccountName,
                     CorrespondingAccountNumber,
                     AccountCategoryKind,
                     DebitAmount,
                     CreditAmount,
                     ORDERType,
                     IsBold,
                     OrderNumber
                    )
                    SELECT
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Description AS JournalMemo,
                            AC.AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind,
                            DebitAmount,
                            CreditAmount,
                            1 AS ORDERType,
                            0,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%')
											AND GL.DebitAmount <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND (GL.DebitAmount <> 0
                                 OR GL.CreditAmount <> 0
                                 or GL.DebitAmountOriginal <> 0
                                 OR GL.CreditAmountOriginal <> 0
                                )
              End
			  Else
			  Begin /*cong gop but toan thue*/
				INSERT  #Result(RefID, RefType, PostedDate, RefNo, RefDate, InvDate, InvNo, JournalMemo, AccountNumber, DetailAccountNumber, AccountName, CorrespondingAccountNumber,
                     AccountCategoryKind, DebitAmount, CreditAmount, ORDERType, IsBold, OrderNumber)
                SELECT RefID, RefType, PostedDate, RefNo, RefDate, InvDate, InvNo, JournalMemo, AccountNumber, DetailAccountNumber, AccountName, CorrespondingAccountNumber,
                     AccountCategoryKind, DebitAmount, CreditAmount, ORDERType, IsBold, OrderNumber
				FROM
				    (SELECT
                            GL.ReferenceID as RefID,
                            GL.TypeID as RefType,
                            GL.PostedDate as PostedDate,
                            RefNo,
                            GL.RefDate as RefDate,
                            Gl.InvoiceDate as InvDate,
                            GL.InvoiceNo as InvNo,
                            GL.Reason AS JournalMemo,
                            AC.AccountNumber as AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName as AccountName,
                            GL.AccountCorresponding as CorrespondingAccountNumber,
                            AC.AccountCategoryKind as AccountCategoryKind,
                            SUM(GL.DebitAmount) as DebitAmount,
                            0 as CreditAmount,
                            1 AS ORDERType,
                            0 as IsBold,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%')
											AND SUM(GL.DebitAmount) <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
					GROUP BY
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Reason,
                            AC.AccountNumber,
							GL.Account,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind
					HAVING SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0
				UNION ALL
				SELECT
                            GL.ReferenceID as RefID,
                            GL.TypeID as RefType,
                            GL.PostedDate as PostedDate,
                            RefNo,
                            GL.RefDate as RefDate,
                            Gl.InvoiceDate as InvDate,
                            GL.InvoiceNo as InvNo,
                            GL.Reason AS JournalMemo,
                            AC.AccountNumber as AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName as AccountName,
                            GL.AccountCorresponding as CorrespondingAccountNumber,
                            AC.AccountCategoryKind as AccountCategoryKind,
                            0 as DebitAmount,
                            SUM(GL.CreditAmount) as CreditAmount,
                            1 AS ORDERType,
                            0 as IsBold,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%')
											AND SUM(GL.CreditAmount) <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
					GROUP BY
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Reason,
                            AC.AccountNumber,
							GL.Account,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind
					HAVING SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0
				) T
				where T.DebitAmount > 0 OR T.CreditAmount>0
			  End
        SELECT  AccountNumber,
                AccountName,
				IsParent,
                SUM(OpenningDebitAmount) AS OpenningDebitAmount,
                SUM(OpenningCreditAmount) AS OpenningCreditAmount,
                SUM(ClosingDebitAmount) AS ClosingDebitAmount,
                SUM(ClosingCreditAmount) AS ClosingCreditAmount,
                SUM(AccumDebitAmount) AS AccumDebitAmount,
                SUM(AccumCreditAmount) AS AccumCreditAmount,
				AccountCategoryKind
        FROM    (
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS OpenningDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate < @FromDate
                 GROUP BY GL.BranchID,
                        AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS ClosingDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate <= @ToDate
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        SUM(GL.DebitAmount) AS AccumDebitAmount,
                        SUM(GL.CreditAmount) AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate BETWEEN @StartDate AND @ToDate
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                ) AS A
        GROUP BY AccountNumber,
                AccountName,
				IsParent ,
				AccountCategoryKind
        HAVING
				SUM(OpenningDebitAmount) <>0 OR
                SUM(OpenningCreditAmount)  <>0 OR
                SUM(ClosingDebitAmount)  <>0 OR
                SUM(ClosingCreditAmount) <>0 OR
                SUM(AccumDebitAmount) <>0 OR
                SUM(AccumCreditAmount) <>0
        ORDER BY AccountNumber
		SELECT  ROW_NUMBER() OVER (ORDER BY AccountNumber,DetailAccountNumber, OrderType, PostedDate, RefDate,OrderNumber, RefNo) AS RowNum,
				RefID,
				RefType,
				PostedDate,
				RefDate,
				RefNo,
				InvDate,
				InvNo,
				JournalMemo,
				AccountNumber,
				DetailAccountNumber,
				CorrespondingAccountNumber,
				DebitAmount,
				CreditAmount,
				IsBold,
				AccountCategoryKind
		FROM    #Result
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GL_GetGLAccountLedgerDiaryBook]
    @BranchID UNIQUEIDENTIFIER,
    @IncludeDependentBranch BIT,
    @StartDate DATETIME,
    @FromDate DATETIME,
    @ToDate DATETIME,    
    @AccountNumber NVARCHAR(MAX),
    @IsSimilarSum BIT
AS
    BEGIN    
        SET NOCOUNT ON       
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

		CREATE TABLE #Result
            (
             RefID UNIQUEIDENTIFIER,
             RefType INT,
             PostedDate DATETIME,
             RefNo NVARCHAR(25),
             RefDate DATETIME,
             InvDate DATETIME,
             InvNo NVARCHAR(MAX), 
             JournalMemo NVARCHAR(512),
             AccountNumber NVARCHAR(25),
			 DetailAccountNumber NVARCHAR(25) ,
             AccountCategoryKind INT,
             AccountName NVARCHAR(512),
             CorrespondingAccountNumber NVARCHAR(25),
             DebitAmount MONEY,
             CreditAmount MONEY,
             OrderType INT,
             IsBold BIT,
             OrderNumber int,
			 OrderPriority int,
            )
        CREATE TABLE #tblAccountNumber
            (
             AccountNumber NVARCHAR(25)    PRIMARY KEY,
             AccountName NVARCHAR(512)  ,
             AccountNumberPercent NVARCHAR(25)  ,
             AccountCategoryKind INT,
			 IsParent BIT
            )
			
       
	
        INSERT  #tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountName,
                        A1.AccountNumber + '%',
                        A1.AccountGroupKind,
						A1.IsParentNode
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
	            					
               
        IF @IsSimilarSum = 0 /*khong cong gop but ke toan*/
		Begin
            INSERT  #Result
                    (
                     RefID,
                     RefType,
                     PostedDate,
                     RefNo,
                     RefDate,
                     InvDate,
                     InvNo,
                     JournalMemo,
                     AccountNumber,
					 DetailAccountNumber,
                     AccountName,
                     CorrespondingAccountNumber,
                     AccountCategoryKind,
                     DebitAmount,
                     CreditAmount,
                     ORDERType,
                     IsBold,
                     OrderNumber,
					 OrderPriority
					
                    )
                    SELECT 
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Description AS JournalMemo,
                            AC.AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind,
                            DebitAmount,
                            CreditAmount,
                            1 AS ORDERType,
                            0,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%') 
											AND GL.DebitAmount <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber,
										OrderPriority
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND (GL.DebitAmount <> 0
                                 OR GL.CreditAmount <> 0
                                 or GL.DebitAmountOriginal <> 0
                                 OR GL.CreditAmountOriginal <> 0
                                )  
					ORDER BY OrderPriority                         
              End
			  Else
			  Begin /*cong gop but toan thue*/
				INSERT  #Result(RefID, RefType, PostedDate, RefNo, RefDate, InvDate, InvNo, JournalMemo, AccountNumber, DetailAccountNumber, AccountName, CorrespondingAccountNumber,
                     AccountCategoryKind, DebitAmount, CreditAmount, ORDERType, IsBold, OrderNumber,OrderPriority)
                SELECT RefID, RefType, PostedDate, RefNo, RefDate, InvDate, InvNo, JournalMemo, AccountNumber, DetailAccountNumber, AccountName, CorrespondingAccountNumber,
                     AccountCategoryKind, DebitAmount, CreditAmount, ORDERType, IsBold, OrderNumber,OrderPriority
				FROM
				    (SELECT 
                            GL.ReferenceID as RefID,
                            GL.TypeID as RefType,
                            GL.PostedDate as PostedDate,
                            RefNo,
                            GL.RefDate as RefDate,
                            Gl.InvoiceDate as InvDate,
                            GL.InvoiceNo as InvNo,
                            GL.Reason AS JournalMemo,
                            AC.AccountNumber as AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName as AccountName,
                            GL.AccountCorresponding as CorrespondingAccountNumber,
                            AC.AccountCategoryKind as AccountCategoryKind,
                            SUM(GL.DebitAmount) as DebitAmount,
                            0 as CreditAmount,
                            1 AS ORDERType,
                            0 as IsBold,

                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%') 
											AND SUM(GL.DebitAmount) <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber,
										OrderPriority
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
					GROUP BY
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Reason,
                            AC.AccountNumber,
							GL.Account,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind,
							OrderPriority
					HAVING SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0 
				
				UNION ALL

				SELECT 
                            GL.ReferenceID as RefID,
                            GL.TypeID as RefType,
                            GL.PostedDate as PostedDate,
                            RefNo,
                            GL.RefDate as RefDate,
                            Gl.InvoiceDate as InvDate,
                            GL.InvoiceNo as InvNo,
                            GL.Reason AS JournalMemo,
                            AC.AccountNumber as AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName as AccountName,
                            GL.AccountCorresponding as CorrespondingAccountNumber,
                            AC.AccountCategoryKind as AccountCategoryKind,
                            0 as DebitAmount,
                            SUM(GL.CreditAmount) as CreditAmount,
                            1 AS ORDERType,
                            0 as IsBold,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%') 
											AND SUM(GL.CreditAmount) <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber,
										OrderPriority
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
					GROUP BY
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Reason,
                            AC.AccountNumber,
							GL.Account,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind,
							OrderPriority
					HAVING SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0 
				) T
				where T.DebitAmount > 0 OR T.CreditAmount>0
			  End      
					
       
        SELECT  AccountNumber,
                AccountName,
				IsParent,
                SUM(OpenningDebitAmount) AS OpenningDebitAmount,
                SUM(OpenningCreditAmount) AS OpenningCreditAmount,
                SUM(ClosingDebitAmount) AS ClosingDebitAmount,
                SUM(ClosingCreditAmount) AS ClosingCreditAmount,
                SUM(AccumDebitAmount) AS AccumDebitAmount,
                SUM(AccumCreditAmount) AS AccumCreditAmount,
				AccountCategoryKind
        FROM    (
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS OpenningDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate < @FromDate                       
                 GROUP BY GL.BranchID,
                        AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS ClosingDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate <= @ToDate                        
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        SUM(GL.DebitAmount) AS AccumDebitAmount,
                        SUM(GL.CreditAmount) AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate BETWEEN @StartDate AND @ToDate                        
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                ) AS A
        GROUP BY AccountNumber,
                AccountName,
				IsParent ,
				AccountCategoryKind         
        HAVING
				SUM(OpenningDebitAmount) <>0 OR
                SUM(OpenningCreditAmount)  <>0 OR
                SUM(ClosingDebitAmount)  <>0 OR
                SUM(ClosingCreditAmount) <>0 OR
                SUM(AccumDebitAmount) <>0 OR
                SUM(AccumCreditAmount) <>0 
        ORDER BY AccountNumber
        		
		SELECT  ROW_NUMBER() OVER (ORDER BY AccountNumber,DetailAccountNumber, OrderType, PostedDate, RefDate,OrderNumber, RefNo) AS RowNum,
				RefID,
				RefType,
				PostedDate,
				RefDate,
				RefNo,
				InvDate,
				InvNo,
				JournalMemo,
				AccountNumber,
				DetailAccountNumber,
				CorrespondingAccountNumber,
				DebitAmount,
				CreditAmount,
				IsBold,
				AccountCategoryKind
		FROM    #Result
		ORDER BY OrderPriority
			
              
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GL_GetBookDetailPaymentByAccountNumber]
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(MAX) ,
	/*@CurrencyId NVARCHAR(10) , comment by cuongpv*/
    @GroupTheSameItem BIT
AS
    BEGIN

		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1) PRIMARY KEY,
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              InvDate DATETIME ,
              InvNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(512) ,
              AccountNumber NVARCHAR(25) ,
              CorrespondingAccountNumber NVARCHAR(25) ,
              DebitAmount DECIMAL(22,4) ,
              CreditAmount DECIMAL(22,4) ,
              ClosingDebitAmount DECIMAL(22,4) ,
              ClosingCreditAmount DECIMAL(22,4) ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(512) ,
              OrderType INT , 
              IsBold BIT ,
              BranchName NVARCHAR(512),
			  AccountGroupKind NVARCHAR(25) ,
			  OrderPriority int

            )      
       
        
        DECLARE @tblAccountNumber TABLE
            (              
			  AccountNumber  NVARCHAR(25) ,  
              AccountNumberPercent NVARCHAR(25)   , 
			  AccountGroupKind NVARCHAR(25) 
            )
        INSERT  @tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountNumber + '%',
						A1.AccountGroupKind
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        IF @GroupTheSameItem = 1
            BEGIN
                INSERT  #Result					
				SELECT
					RefID ,
                    PostedDate ,
                    RefDate ,
                    RefNo ,
                    InvDate ,
                    InvNo ,
                    RefType ,
                    JournalMemo ,
                    AccountNumber,
                    CorrespondingAccountNumber ,
                    ISNULL(DebitAmount,0) ,
                    ISNULL(CreditAmount,0) ,
                    ISNULL(ClosingDebitAmount,0),
                    ISNULL(ClosingCreditAmount,0),
                    AccountingObjectCode,
                    AccountObjectName,
                    OrderType ,
                    IsBold ,
                    BranchName,
					AccountGroupKind,
					OrderPriority	

                 FROM
					(SELECT  
								NULL as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư đầu kỳ' as JournalMemo ,
                                A.AccountNumber,
                                NULL as CorrespondingAccountNumber ,
								$0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
									 THEN SUM(GL.DebitAmount - GL.CreditAmount)
									 ELSE 0
								END AS ClosingDebitAmount ,
								CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
									 THEN SUM(GL.CreditAmount - GL.DebitAmount)
									 ELSE 0
								END AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                0 AS OrderType ,
                                1 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
								,A.AccountGroupKind,
								0 as OrderPriority /*edit by cuongpv: GL.OrderPriority -> 0 as OrderPriority*/

                            FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
							INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
							WHERE   ( GL.PostedDate < @FromDate )  
							/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
							GROUP BY 
								A.AccountNumber,A.AccountGroupKind /*comment by cuongpv: ,GL.OrderPriority*/
							HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv Description-> Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                SUM(GL.DebitAmount) AS DebitAmount ,
                                null AS CreditAmount ,/*edit by cuongpv SUM(GL.CreditAmount) -> null*/
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 As isBold,
                                '' as BranchName
								,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
								,A.AccountGroupKind,
								GL.OrderPriority
                        FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )    
						/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
						and GL.AccountCorresponding <> ''      
                        GROUP BY GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv GL.Description -> GL.Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding,
								A.AccountGroupKind,
								GL.OrderPriority
                        HAVING  SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0
						/*add by cuongpv de sua cong gop but toan*/
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv Description-> Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                null AS DebitAmount ,
                                SUM(GL.CreditAmount) AS CreditAmount ,
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 As isBold,
                                '' as BranchName
								,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
								,A.AccountGroupKind,
								GL.OrderPriority
                        FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )    
						/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
						and GL.AccountCorresponding <> ''      
                        GROUP BY GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv GL.Description -> GL.Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding,
								A.AccountGroupKind,
								GL.OrderPriority
                        HAVING  SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0 
						/*end add by cuongpv*/                       
                      )T
                      ORDER BY 
						AccountNumber,
                        OrderType ,
                        postedDate ,
						OrderPriority,
                        RefDate ,
                        RefNo      
                        ,SortOrder,DetailPostOrder,EntryType            
            END
            
        ELSE
            BEGIN             
                
                INSERT  #Result					
				SELECT
					RefID ,
                    PostedDate ,
                    RefDate ,
                    RefNo ,
                    InvDate ,
                    InvNo ,
                    RefType ,
                    JournalMemo ,
                    AccountNumber,
                    CorrespondingAccountNumber ,
                    DebitAmount ,
                    CreditAmount ,
                    ClosingDebitAmount ,
                    ClosingCreditAmount ,
                    AccountingObjectCode,
                    AccountObjectName,
                    OrderType ,
                    IsBold ,
                    BranchName,
					AccountGroupKind,
					OrderPriority
                 FROM
					(SELECT  
								null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư đầu kỳ' as JournalMemo ,
                                A.AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
									 THEN SUM(GL.DebitAmount - GL.CreditAmount)
									 ELSE 0
								END AS ClosingDebitAmount ,
								CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
									 THEN SUM(GL.CreditAmount - GL.DebitAmount)
									 ELSE 0
								END AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                0 AS OrderType ,
                                1 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType
								,A.AccountGroupKind,
								0 as OrderPriority /*edit by cuongpv: GL.OrderPriority -> OrderPriority*/
                            FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
							INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
							WHERE   ( GL.PostedDate < @FromDate )   
							/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
							GROUP BY 
								A.AccountNumber,A.AccountGroupKind/*comment by cuongpv: ,GL.OrderPriority*/
							HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                CASE WHEN GL.DESCRIPTION IS NULL
                                          OR LTRIM(GL.Description) = ''
                                     THEN GL.Reason
                                     ELSE GL.Description
                                END AS JournalMemo ,
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                GL.DebitAmount ,
                                GL.CreditAmount ,
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 ,
                                null BranchName
								,null
								,null
								,null
								,A.AccountGroupKind,
								GL.OrderPriority
                        FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
                        WHERE   (GL.PostedDate BETWEEN @FromDate AND @ToDate )                              
                                AND ( GL.DebitAmount <> 0
                                      OR GL.CreditAmount <> 0
                                    )   	
									/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
									and GL.AccountCorresponding <> ''                     
                      )T
                      ORDER BY
						AccountNumber,
                        OrderType ,
                        postedDate ,
						OrderPriority,
                        RefDate ,
                        RefNo,SortOrder,DetailPostOrder,EntryType                                                          
               
            END         

			 

		DECLARE @AccountNumber1 NVARCHAR(25)

		DECLARE C1 CURSOR FOR  
		SELECT  F.Value
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
	    order by F.Value
		OPEN c1;  
		FETCH NEXT FROM c1 INTO @AccountNumber1  ;  
		WHILE @@FETCH_STATUS = 0  
		   BEGIN  
		      PRINT @AccountNumber1
			  DECLARE @AccountObjectCode NVARCHAR(25)
			 ,@ClosingDebitAmount DECIMAL(22,4)
			 
		      SELECT @ClosingDebitAmount = 0
			  UPDATE  #Result
			  SET     @ClosingDebitAmount = @ClosingDebitAmount + ISNULL(ClosingDebitAmount,0) - 
		                              ISNULL(ClosingCreditAmount,0) + ISNULL(DebitAmount,0) - ISNULL(CreditAmount,0),
		
                ClosingDebitAmount = CASE WHEN @ClosingDebitAmount > 0
                                          THEN @ClosingDebitAmount
                                          ELSE 0
                                     END ,
                ClosingCreditAmount = CASE WHEN @ClosingDebitAmount < 0
                                           THEN - @ClosingDebitAmount
                                           ELSE 0
                                      END 
               where AccountNumber = @AccountNumber1
			   FETCH NEXT FROM c1 INTO @AccountNumber1  ; 
			   
		   END;  
		CLOSE c1;  
		DEALLOCATE c1;  
		
        DECLARE @DebitAmount DECIMAL(22,4) 
		DECLARE @CreditAmount DECIMAL(22,4)  
		DECLARE @ClosingDeditAmount_SDDK DECIMAL(22,4)  
		DECLARE @ClosingCreditAmount_SDDK DECIMAL(22,4)  
	    select @DebitAmount = sum(DebitAmount),
		       @CreditAmount = sum(CreditAmount)
		from #Result Res
		INNER JOIN @tblAccountNumber A ON Res.AccountNumber LIKE A.AccountNumberPercent
		where OrderType = 1
		select @ClosingDeditAmount_SDDK = sum(ClosingDebitAmount),
		       @ClosingCreditAmount_SDDK = sum(ClosingCreditAmount)
		from #Result where OrderType = 0
        SELECT  RowNum ,
                RefID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountNumber,
                PostedDate ,
                RefDate ,
                RefNo ,
                InvDate ,
                InvNo ,
                RefType ,
                JournalMemo ,
                CorrespondingAccountNumber ,
                DebitAmount ,
                CreditAmount ,
                ClosingDebitAmount ,
                ClosingCreditAmount ,               
                OrderType AS InvoiceOrder ,
                IsBold ,
                BranchName,
				AccountGroupKind
        FROM    #Result
		WHERE (DebitAmount > 0 OR CreditAmount > 0 AND OrderType = 1) OR OrderType = 0 /*add by cuongpv*/
        ORDER BY RowNum
		
		
		select 
		null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư cuối kỳ' as JournalMemo ,
                                AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 DebitAmount ,
								$0 CreditAmount ,
								case 
								when A1.AccountGroupKind=0 or (A1.AccountGroupKind=2 and (A1.AccountNumber like '1%' or A1.AccountNumber like '2%' or A1.AccountNumber like '6%' 
								or A1.AccountNumber like '8%')) then
								sum(ClosingDebitAmount) + sum(sumDebitAmount) - sum(sumCreditAmount) else $0 
								end as ClosingDebitAmount,
								case 
								when A1.AccountGroupKind=1 or (A1.AccountGroupKind=2 and (A1.AccountNumber like '3%' or A1.AccountNumber like '4%' or A1.AccountNumber like '5%' 
								or A1.AccountNumber like '7%')) then
								sum(ClosingCreditAmount) + sum(sumCreditAmount) - sum(sumDebitAmount) else $0 
								end AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                2 AS OrderType ,
                                2 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType,
								A1.AccountGroupKind
        from (SELECT  
								null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư cuối kỳ' as JournalMemo ,
                                A.AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN GL.OrderType = 0 and gl.IsBold = 1 THEN sum(ClosingDebitAmount)
									 ELSE 0 
								END
								AS ClosingDebitAmount,
								sum(DebitAmount) sumDebitAmount,
								sum(CreditAmount) sumCreditAmount,
								0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                2 AS OrderType ,
                                2 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType,
								A.AccountGroupKind
                            FROM    #Result AS GL
							INNER JOIN @tblAccountNumber A ON GL.AccountNumber = A.AccountNumber
							GROUP BY OrderType,IsBold,
								A.AccountNumber,A.AccountGroupKind) as A1
        group by AccountNumber,AccountGroupKind
        DROP TABLE #Result 
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_FU_GetCashDetailBook]
    (
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @AccountNumber NVARCHAR(20) ,
      @BranchID UNIQUEIDENTIFIER ,
      @CurrencyID NVARCHAR(3)
    )
AS 
    BEGIN
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,4),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,4),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
			  AccountNumber NVARCHAR(15),
              PostedDate DATETIME ,
              RefDate DATETIME ,
              ReceiptRefNo NVARCHAR(25) ,
              PaymentRefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(512) ,
              CorrespondingAccountNumber NVARCHAR(20) ,
              ExchangeRate Numeric ,
              DebitAmount Numeric ,
              DebitAmountOC Numeric(25,4),
              CreditAmount Numeric ,
              CreditAmountOC Numeric(25,4),
              ClosingAmount Numeric ,
              ClosingAmountOC Numeric(25,4),
              ContactName NVARCHAR(512) ,
			  AccountObjectId NVARCHAR(255),
              AccountObjectCode NVARCHAR(255) ,
              AccountObjectName NVARCHAR(512) , 
              EmployeeCode NVARCHAR(255) ,
              EmployeeName NVARCHAR(255) , 
              InvoiceOrder INT , 
              BranchID UNIQUEIDENTIFIER ,
              BranchName NVARCHAR(512) ,
              IsBold BIT
            )
         DECLARE @tblAccountNumber TABLE
            (              
			  AccountNumber  NVARCHAR(25),  
              AccountNumberPercent NVARCHAR(25)              
            )
        INSERT  @tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountNumber + '%'
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        DECLARE @AccountNumberPercent NVARCHAR(25)
        SET @AccountNumberPercent = @AccountNumber + '%'
        
        DECLARE @DiscountNumber NVARCHAR(25)       
        SET @DiscountNumber = '5211%'
   
        DECLARE @ClosingAmountOC MONEY
        DECLARE @ClosingAmount MONEY
   IF(@CurrencyID = 'VND')
   BEGIN
 INSERT  #Result
	          (RefID ,
			  AccountNumber,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  CorrespondingAccountNumber)
			  select ReferenceID ,
			  Account,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  AccountCorresponding from 
			            (SELECT null as ReferenceID,
						    Acc.AccountNumber as Account,
							null as PostedDate,
							null as RefDate,
							null as ReceiptRefNo,
							null as PaymentRefNo,
							null as RefType,
					        N'Số tồn đầu kỳ' AS JournalMemo ,
                            $0 AS ExchangeRate ,
                            $0 AS DebitAmount ,
                            $0 AS DebitAmountOC ,
                            $0 AS CreditAmount ,
                            $0 AS CreditAmountOC ,
							SUM(GL.DebitAmount - GL.CreditAmount) as ClosingAmount,
							SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) as ClosingAmountOC,
							null as ContactName,
							null as AccountObjectId,
                            0 AS InvoiceOrder ,
                            null BranchID ,
                            0 as BranchName,
                            null AccountCorresponding
							 from @tbDataGL gl /*edit by cuongpv GeneralLedger -> @tbDataGL*/
							 inner join @tblAccountNumber Acc on GL.Account = Acc.AccountNumber
							 where ( GL.PostedDate < @FromDate )
							
						    group by Acc.AccountNumber
				        union all
                        SELECT  GL.ReferenceID,
						        GL.Account,
                                GL.PostedDate ,
                                GL.RefDate ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS ReceiptRefNo ,
                                CASE WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS PaymentRefNo ,
                                GL.TypeID ,
								case when GL.Reason is null then
                                GL.Description 
								else GL.Reason 
								end as Description, 
                                GL.ExchangeRate ,
                                GL.DebitAmount ,
                                GL.DebitAmountOriginal ,
                                GL.CreditAmount ,
                                GL.CreditAmountOriginal ,
                                $0 AS ClosingAmount ,
                                $0 AS ClosingAmountOC ,
                                GL.ContactName ,
                                GL.AccountingObjectID ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0 THEN 1
                                     WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0 THEN 2
                                     ELSE 3
                                END AS InvoiceOrder ,
                                GL.BranchID ,
                                0,
                               GL.AccountCorresponding
                        FROM    @tbDataGL AS GL ,/*edit by cuongpv GeneralLedger -> @tbDataGL*/
						        @tblAccountNumber Acc
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )
                                AND GL.Account = Acc.AccountNumber
                                
                                AND ( GL.CreditAmount <> 0
                                      OR GL.DebitAmount <> 0
                                      OR GL.CreditAmountOriginal <> 0
                                      OR GL.DebitAmountOriginal <> 0
                                    )) T
                        ORDER BY Account,
						        PostedDate ,
                                RefDate ,
                                InvoiceOrder, 
								ReceiptRefNo,
								PaymentRefNo
		END
		ELSE
		   BEGIN
 INSERT  #Result
	          (RefID ,
			  AccountNumber,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  CorrespondingAccountNumber)
			  select ReferenceID ,
			  Account,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  AccountCorresponding from 
			            (SELECT null as ReferenceID,
						    Acc.AccountNumber as Account,
							null as PostedDate,
							null as RefDate,
							null as ReceiptRefNo,
							null as PaymentRefNo,
							null as RefType,
					        N'Số tồn đầu kỳ' AS JournalMemo ,
                            $0 AS ExchangeRate ,
                            $0 AS DebitAmount ,
                            $0 AS DebitAmountOC ,
                            $0 AS CreditAmount ,
                            $0 AS CreditAmountOC ,
							SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) as ClosingAmount,
							SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) as ClosingAmountOC,
							null as ContactName,
							null as AccountObjectId,
                            0 AS InvoiceOrder ,
                            null BranchID ,
                            0 as BranchName,
                            null AccountCorresponding
							 from @tbDataGL gl /*edit by cuongpv GeneralLedger -> @tbDataGL*/
							 inner join @tblAccountNumber Acc on GL.Account = Acc.AccountNumber
							 where ( GL.PostedDate < @FromDate )
								AND ( @CurrencyID IS NULL
								OR GL.CurrencyID = @CurrencyID)
						    group by Acc.AccountNumber
				        union all
                        SELECT  GL.ReferenceID,
						        GL.Account,
                                GL.PostedDate ,
                                GL.RefDate ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS ReceiptRefNo ,
                                CASE WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS PaymentRefNo ,
                                GL.TypeID ,
								case when GL.Reason is null then
                                GL.Description 
								else GL.Reason 
								end as Description, 
                                GL.ExchangeRate ,
                                GL.DebitAmountOriginal ,
                                GL.DebitAmountOriginal ,
                                GL.CreditAmountOriginal ,
                                GL.CreditAmountOriginal ,
                                $0 AS ClosingAmount ,
                                $0 AS ClosingAmountOC ,
                                GL.ContactName ,
                                GL.AccountingObjectID ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0 THEN 1
                                     WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0 THEN 2
                                     ELSE 3
                                END AS InvoiceOrder ,
                                GL.BranchID ,
                                0,
                               GL.AccountCorresponding
                        FROM    @tbDataGL AS GL ,/*edit by cuongpv GeneralLedger -> @tbDataGL*/
						        @tblAccountNumber Acc
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )
                                AND GL.Account = Acc.AccountNumber
                                AND ( @CurrencyID IS NULL
                                      OR GL.CurrencyID = @CurrencyID
                                    )
                                AND ( GL.CreditAmount <> 0
                                      OR GL.DebitAmount <> 0
                                      OR GL.CreditAmountOriginal <> 0
                                      OR GL.DebitAmountOriginal <> 0
                                    )) T
                        ORDER BY Account,
						        PostedDate ,
                                RefDate ,
                                InvoiceOrder, 
								ReceiptRefNo,
								PaymentRefNo
		END
        SET @ClosingAmount = 0
        SET @ClosingAmountOC = 0
        UPDATE  #Result
        SET     @ClosingAmount = @ClosingAmount  + ClosingAmount +ISNULL(DebitAmount, 0)
                - ISNULL(CreditAmount, 0) ,
                ClosingAmount = @ClosingAmount ,
                @ClosingAmountOC = @ClosingAmountOC + ClosingAmountOC + ISNULL(DebitAmountOC, 0)
                - ISNULL(CreditAmountOC, 0) ,
                ClosingAmountOC = @ClosingAmountOC 
        SELECT  *
        FROM    #Result R   
		order by rownum
        DROP TABLE #Result 
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE PROCEDURE [dbo].[Proc_SoChiTietPhaiThuKhachHangTheoMatHang]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountNumber NVARCHAR(25),
    @AccountObjectID AS NVARCHAR(MAX),
    @CurrencyID NVARCHAR(3),
    @AccountObjectGroupID AS NVARCHAR(MAX),
	@MaterialsGoodID as NVARCHAR(MAX)

AS
    BEGIN          
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

		DECLARE @tblListAccountObjectID TABLE
            (
             AccountObjectID UNIQUEIDENTIFIER ,
             AccountObjectGroupList NVARCHAR(MAX)           
            ) 
		     
        INSERT  INTO @tblListAccountObjectID
        SELECT  
			AO.ID
			,AO.AccountObjectGroupID
        FROM AccountingObject AS AO 
        LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') AS TLAO 
			ON AO.ID = TLAO.Value
        WHERE 
		(@AccountObjectID IS NULL OR TLAO.Value IS NOT NULL)


		DECLARE @tblListMaterialsGoodID TABLE
            (
             MaterialsGoodID UNIQUEIDENTIFIER        
            ) 
             
   INSERT  INTO @tblListMaterialsGoodID
         SELECT  TG.id
         FROM    MaterialGoods AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@MaterialsGoodID,',') AS MaterialsGoodID ON TG.ID = MaterialsGoodID.Value
         WHERE  MaterialsGoodID.Value IS NOT NULL

	

			DECLARE @tblResult TABLE
            (
			 AccountingObjectID uniqueidentifier,
			 AccountingObjectCode nvarchar(25),
			 AccountingObjectName nvarchar(512),
			 SAInvoiceDetail uniqueidentifier,
			 InvoiceNo nvarchar(25),
			 PostedDateOrder Datetime,
             PostedDate Datetime,
			 Date Datetime,
			 No nvarchar(25),
			 NoOrder nvarchar(25),
			 Reason nvarchar(512),
			 Description nvarchar(512),
			 Account nvarchar(25),
			 Unit nvarchar(25),
			 Quantity decimal(25,10),
			 UnitPrice decimal(25,3),
			 SoTien money,
			 ChietKhau decimal(25,3),
			 ThueGTGT decimal(25,3),
			 TraLaiGiamGia money,
			 SoDaThu money,
			 SoConPhaiThu money,
			 Type int,
			 OrderPriority int,
			 DetailID uniqueidentifier,
			 ReferenceID uniqueidentifier,
			 CurrencyID nvarchar(3)
            ) 

			DECLARE @accnumber nvarchar(128)

			SET @accnumber = @AccountNumber + '%'

			
			INSERT INTO @tblResult 
			SELECT DISTINCT  d.AccountingObjectID,null,null,b.SAInvoiceID,d.InvoiceNo,d.PostedDate,d.PostedDate,d.Date,d.No,d.No,d.Description,d.Reason,d.Account,b.Unit,b.Quantity,b.UnitPrice,0,0,0,0,0,0,1,2,d.DetailID,d.ReferenceID,d.CurrencyID
			FROM  @tbDataGL d 
			INNER JOIN SAInvoiceDetail b ON d.DetailID = b.ID 
			WHERE d.PostedDate >=@FromDate 
			AND d.PostedDate <@ToDate AND d.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			AND d.TypeID NOT IN ('330','340') and MaterialGoodsID IN (SELECT * from @tblListMaterialsGoodID)
			AND AccountCorresponding NOT LIKE '111%' AND AccountCorresponding NOT LIKE '112%'
			 AND Account LIKE @accnumber AND CurrencyID = @CurrencyID
			
			UPDATE @tblResult SET ChietKhau = a.CA
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D, DetailID DID, CurrencyID CID
		From @tbDataGL
		WHERE  AccountingObjectID IN (SELECT AccountingObjectID from @tblListAccountObjectID) 
		AND TypeID NOT IN ('330','340') AND AccountCorresponding NOT LIKE '111%' AND AccountCorresponding NOT LIKE '112%'
		and AccountCorresponding not like '3331%' and CreditAmount >0
		AND Account LIKE @accnumber and CurrencyID = @CurrencyID
	) a
	WHERE  Account like @accnumber  AND Reason = a.D and DetailID = a.DID and CurrencyID = a.CID


	UPDATE @tblResult SET ThueGTGT = a.DA
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D,DetailID  DID, CurrencyID CID
		From @tbDataGL
		WHERE AccountCorresponding LIKE '3331%' AND AccountingObjectID IN (SELECT AccountingObjectID from @tblListAccountObjectID) 
		AND TypeID NOT IN ('330','340')  and DebitAmount >0 AND CurrencyID = @CurrencyID
	) a
	WHERE  Account = @AccountNumber  AND Reason = a.D and DetailID =  a.DID and Type = 1 and CurrencyID = a.CID
		
		UPDATE @tblResult SET SoTien = a.DA
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D,DetailID  DID, CurrencyID CID
		From @tbDataGL
		WHERE   TypeID NOT IN ('330','340') AND AccountCorresponding NOT LIKE '111%' AND AccountCorresponding NOT LIKE '112%'
		and AccountCorresponding NOT LIKE '3331%'
		AND Account LIKE '131%' AND CurrencyID = @CurrencyID  AND DebitAmount >0
	) a
	WHERE  Account = @AccountNumber  AND Reason = a.D and DetailID =  a.DID and Type = 1 and CurrencyID = a.CID


			
			UPDATE @tblResult SET SoConPhaiThu = (SoTien - ChietKhau + ThueGTGT -TraLaiGiamGia - SoDaThu ) WHERE Type = 1

			INSERT INTO @tblResult 
			SELECT AccountingObjectID,null,null,SAInvoiceDetail,InvoiceNo,PostedDateOrder,PostedDate, Date, No,NoOrder, Description,Description,null,null, 0,0,0,0,0,0,0,0,1,1,null,ReferenceID,CurrencyID
			From @tblResult
			WHERE Type = 1
			group by PostedDate,Date,No,Description,AccountingObjectID,SAInvoiceDetail,PostedDateOrder,InvoiceNo,NoOrder,ReferenceID,CurrencyID

			INSERT INTO @tblResult 
			SELECT DISTINCT  d.AccountingObjectID,null,null,b.SAInvoiceID,d.InvoiceNo,d.PostedDate,d.PostedDate,d.Date,d.No,d.No,d.Description,d.Reason,d.Account,b.Unit,b.Quantity,b.UnitPrice,0,0,0,0,0,0,2,2,d.DetailID,d.ReferenceID,d.CurrencyID
			FROM  @tbDataGL d 
			INNER JOIN SAReturnDetail b ON d.DetailID = b.ID 
			WHERE d.PostedDate >=@FromDate 
			AND d.PostedDate <@ToDate AND d.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			AND d.TypeID ='330' and MaterialGoodsID IN (SELECT * from @tblListMaterialsGoodID)
			 AND Account LIKE @accnumber AND CurrencyID = @CurrencyID
			
			UPDATE @tblResult SET ChietKhau = a.DA
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D, DetailID DID, CurrencyID CID
		From @tbDataGL
		WHERE  AccountingObjectID IN (SELECT AccountingObjectID from @tblListAccountObjectID) 
		AND TypeID ='330'
		and AccountCorresponding not like '3331%' and DebitAmount >0
		AND Account LIKE @accnumber  AND CurrencyID = @CurrencyID
	) a
	WHERE  Account like @accnumber  AND Reason = a.D and DetailID = a.DID and CurrencyID = a.CID


	UPDATE @tblResult SET ThueGTGT = -(a.CA)
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D,DetailID  DID, CurrencyID CID
		From @tbDataGL
		WHERE AccountCorresponding LIKE '3331%' AND AccountingObjectID IN (SELECT AccountingObjectID from @tblListAccountObjectID) 
		AND TypeID ='330'  and CreditAmount >0 AND CurrencyID = @CurrencyID
	) a
	WHERE  Account = @AccountNumber  AND Reason = a.D and DetailID =  a.DID and Type = 2 and CurrencyID = a.CID
		
			UPDATE @tblResult SET TralaiGiamGia = -(a.CA)
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D,DetailID  DID, CurrencyID CID
		From @tbDataGL
		WHERE   AccountingObjectID IN (SELECT AccountingObjectID from @tblListAccountObjectID) 
		AND TypeID ='330'
		and AccountCorresponding not like '3331%'
		AND Account LIKE @accnumber
		AND CreditAmount >0
		AND CurrencyID = @CurrencyID
	) a
	WHERE  Account = @AccountNumber  AND Reason = a.D and DetailID =  a.DID and Type = 2 and CurrencyID = a.CID


			UPDATE @tblResult SET SoConPhaiThu = (SoTien + ChietKhau + ThueGTGT +TraLaiGiamGia - SoDaThu ) WHERE Type = 2

			INSERT INTO @tblResult 
			SELECT AccountingObjectID,null,null,SAInvoiceDetail,InvoiceNo,PostedDateOrder,PostedDate, Date, No,NoOrder, Description,Description,null,null, 0,0,0,0,0,0,0,0,2,1,null,ReferenceID,CurrencyID
			From @tblResult
			WHERE Type = 2
			group by PostedDate,Date,No,Description,AccountingObjectID,SAInvoiceDetail,PostedDateOrder,InvoiceNo,NoOrder,ReferenceID, CurrencyID


			INSERT INTO @tblResult 
			SELECT DISTINCT  d.AccountingObjectID,null,null,b.SAInvoiceID,d.InvoiceNo,d.PostedDate,d.PostedDate,d.Date,d.No,d.No,d.Description,d.Reason,d.Account,b.Unit,b.Quantity,b.UnitPrice,0,0,0,0,0,0,3,2,d.DetailID,d.ReferenceID,d.CurrencyID
			FROM  @tbDataGL d 
			INNER JOIN SAReturnDetail b ON d.DetailID = b.ID 
			WHERE d.PostedDate >=@FromDate 
			AND d.PostedDate <@ToDate AND d.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			AND d.TypeID ='340' and MaterialGoodsID IN (SELECT * from @tblListMaterialsGoodID)
			 AND Account LIKE @accnumber AND CurrencyID = @CurrencyID
			
			UPDATE @tblResult SET ChietKhau = a.DA
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D, DetailID DID, CurrencyID CID
		From @tbDataGL
		WHERE  AccountingObjectID IN (SELECT AccountingObjectID from @tblListAccountObjectID) 
		AND TypeID ='340'
		and AccountCorresponding not like '3331%' and DebitAmount >0
		AND Account LIKE @accnumber 
		AND CurrencyID = @CurrencyID
	) a
	WHERE  Account like @accnumber  AND Reason = a.D and DetailID = a.DID and CurrencyID = a.CID


	UPDATE @tblResult SET ThueGTGT = -(a.CA)
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D,DetailID  DID, CurrencyID CID
		From @tbDataGL
		WHERE AccountCorresponding LIKE '3331%' AND AccountingObjectID IN (SELECT AccountingObjectID from @tblListAccountObjectID) 
		AND TypeID ='340'  and CreditAmount >0 AND CurrencyID = @CurrencyID
	) a
	WHERE  Account = @AccountNumber  AND Reason = a.D and DetailID =  a.DID and Type = 3 and CurrencyID = a.CID
		
		UPDATE @tblResult SET TralaiGiamGia = -(a.CA)
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D,DetailID  DID, CurrencyID CID
		From @tbDataGL
		WHERE   AccountingObjectID IN (SELECT AccountingObjectID from @tblListAccountObjectID) 
		AND TypeID ='340'
		and AccountCorresponding not like '3331%' and CreditAmount >0
		AND Account LIKE @accnumber  AND CurrencyID = @CurrencyID
	) a
	WHERE  Account = @AccountNumber  AND Reason = a.D and DetailID =  a.DID and Type = 3 and CurrencyID = a.CID


			
			UPDATE @tblResult SET SoConPhaiThu = (SoTien + ChietKhau + ThueGTGT + TraLaiGiamGia - SoDaThu ) WHERE Type = 3

			INSERT INTO @tblResult 
			SELECT AccountingObjectID,null,null,SAInvoiceDetail,InvoiceNo,PostedDateOrder,PostedDate, Date, No,NoOrder, Description,Description,null,null, 0,0,0,0,0,0,0,0,3,1,null,ReferenceID,CurrencyID
			From @tblResult
			WHERE Type = 3
			group by PostedDate,Date,No,Description,AccountingObjectID,SAInvoiceDetail,PostedDateOrder,InvoiceNo,NoOrder,ReferenceID,CurrencyID


			INSERT INTO @tblResult 
			SELECT DISTINCT  d.AccountingObjectID,null,null,null,d.InvoiceNo,d.PostedDate,d.PostedDate,d.Date,d.No,d.No,d.Description,d.Reason,d.Account,null,null,null,0,0,0,0,0,0,4,2,d.DetailID,d.ReferenceID,d.CurrencyID
			FROM  @tbDataGL d 
			
			WHERE d.PostedDate >=@FromDate 
			AND d.PostedDate <@ToDate AND d.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			AND (AccountCorresponding LIKE '111%' OR AccountCorresponding LIKE '112%')
			 AND Account LIKE @accnumber AND CurrencyID = @CurrencyID
			
			

		
		UPDATE @tblResult SET SoDaThu = a.CA
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D,DetailID  DID,CurrencyID CID
		From @tbDataGL
		WHERE   AccountingObjectID IN (SELECT AccountingObjectID from @tblListAccountObjectID) 
		and CreditAmount >0
		AND (AccountCorresponding LIKE '111%' OR AccountCorresponding LIKE '112%' )
		AND Account LIKE @accnumber  AND CurrencyID = @CurrencyID
	) a
	WHERE  Account = @AccountNumber  AND Reason = a.D and DetailID =  a.DID and Type = 4 and CurrencyID = a.CID

			
			UPDATE @tblResult SET SoConPhaiThu = (SoTien + ChietKhau + ThueGTGT +TraLaiGiamGia - SoDaThu ) WHERE Type = 4

			INSERT INTO @tblResult 
			SELECT AccountingObjectID,null,null,SAInvoiceDetail,InvoiceNo,PostedDateOrder,PostedDate, Date, No,NoOrder, Description,Description,null,null, 0,0,0,0,0,0,0,0,4,1,null,ReferenceID,CurrencyID
			From @tblResult
			WHERE Type = 4
			group by PostedDate,Date,No,Description,AccountingObjectID,SAInvoiceDetail,PostedDateOrder,InvoiceNo,NoOrder,ReferenceID,CurrencyID

			 

			  UPDATE @tblResult SET AccountingObjectCode =a.AOC, AccountingObjectName = a.AON
			FROM (
			Select ID as AOI, AccountingObjectCode as AOC, AccountingObjectName AON
			From AccountingObject
			) a
			WHERE AccountingObjectID = a.AOI


				UPDATE @tblResult SET PostedDate = null,Date = null, No = null where OrderPriority = 2
		SELECT * from @tblResult
		order by PostedDateOrder, NoOrder,Type,OrderPriority
			
		
		       			
    END

	
	
	
	 
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE PROCEDURE [dbo].[Proc_SoChiTietBanHangTheoNhanVien]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountObjectID AS NVARCHAR(MAX),
	@MaterialsGoodID as NVARCHAR(MAX),
	@EmployeeID as NVARCHAR(MAX)

AS
    BEGIN          
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

		DECLARE @tblListAccountObjectID TABLE
            (
             AccountObjectID UNIQUEIDENTIFIER ,
             AccountObjectGroupList NVARCHAR(MAX)           
            ) 

        INSERT  INTO @tblListAccountObjectID
        SELECT  
			AO.ID
			,AO.AccountObjectGroupID
        FROM AccountingObject AS AO 
        LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') AS TLAO 
			ON AO.ID = TLAO.Value
        WHERE 
		(@AccountObjectID IS NULL OR TLAO.Value IS NOT NULL)


		DECLARE @tblListMaterialsGoodID TABLE
            (
             MaterialsGoodID UNIQUEIDENTIFIER        
            ) 
             
   INSERT  INTO @tblListMaterialsGoodID
         SELECT  TG.id
         FROM    MaterialGoods AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@MaterialsGoodID,',') AS MaterialsGoodID ON TG.ID = MaterialsGoodID.Value
         WHERE  MaterialsGoodID.Value IS NOT NULL

		 DECLARE @tblListEmployeeID TABLE
            (
             EmployeeID UNIQUEIDENTIFIER        
            ) 
             
   INSERT  INTO @tblListEmployeeID
         SELECT  TG.id
         FROM    AccountingObject AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@EmployeeID,',') AS EmployeeID ON TG.ID = EmployeeID.Value
         WHERE  EmployeeID.Value IS NOT NULL

		 

		 DECLARE @tblResult TABLE
            (
			 AccountingObjectID uniqueidentifier,
			 AccountingObjectCode nvarchar(25),
			 AccountingObjectName nvarchar(512),
			 EmployeeID uniqueidentifier,
			 EmployeeCode nvarchar(25),
			 EmployeeName nvarchar(512),
			 SAInvoiceDetail uniqueidentifier,
			 Description nvarchar(512),
			 Date Datetime,
			 No nvarchar(25),
			 DetailID uniqueidentifier,
			 InvoiceNo nvarchar(25),
			 InvoiceDate Datetime,
			 MaterialGoodsID uniqueidentifier,
			 MaterialGoodsCode nvarchar(25),
			 MaterialGoodsName nvarchar(512),
			 Unit nvarchar(25),
			 Quantity decimal(25,10),
			 UnitPrice money,
			 DoanhSoBan money,
			 ChietKhau money,
			 SoLuongTraLai int,
			 GiaTriTraLai money,
			 GiamGia money,
			 Type int
            ) 

			BEGIN
			INSERT INTO @tblResult 
			SELECT DISTINCT GL.AccountingObjectID,null,null,GL.EmployeeID,null,null,SAD.SAInvoiceID,GL.Description,GL.Date,GL.No,GL.DetailID,GL.InvoiceNo,GL.InvoiceDate,SAD.MaterialGoodsID,null,null,SAD.Unit,SAD.Quantity,SAD.UnitPrice,0,0,0,0,0,1
			FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
			WHERE (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is null
			AND GL.PostedDate >=@FromDate 
			AND GL.PostedDate <@ToDate AND GL.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			AND SAD.MaterialGoodsID IN (SELECT * from @tblListMaterialsGoodID) AND GL.EmployeeID in (select * from @tblListEmployeeID)
			AND GL.Account LIKE '511%'
		
			UNION ALL
			SELECT DISTINCT GL.AccountingObjectID,null,null,GL.EmployeeID,null,null,SAD.SAInvoiceID,GL.Description,GL.Date,GL.No,GL.DetailID,SAB.InvoiceNo,SAB.InvoiceDate,SAD.MaterialGoodsID,null,null,SAD.Unit,SAD.Quantity,SAD.UnitPrice,0,0,0,0,0,1
			FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
			LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
			WHERE (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
			AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is not null
			AND GL.PostedDate >=@FromDate 
			AND GL.PostedDate <@ToDate AND GL.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			 and SAD.MaterialGoodsID IN (SELECT * from @tblListMaterialsGoodID) AND GL.EmployeeID in (select * from @tblListEmployeeID)
			 and GL.Account LIKE '511%'


			UPDATE @tblResult SET ChietKhau = a.DA
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description D, DetailID DID
		From @tbDataGL 
		WHERE Account LIKE '511%' and DebitAmount > 0
	) a
	WHERE Type = 1  AND Description = a.D and DetailID = DID
	UPDATE @tblResult SET 	DoanhSoBan = a.CA
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description D, DetailID DID
		From @tbDataGL 
		WHERE Account LIKE '511%' and CreditAmount > 0
	) a
	WHERE Type = 1  AND Description = a.D and DetailID = DID
	

			INSERT INTO @tblResult 
			SELECT DISTINCT GL.AccountingObjectID,null,null,GL.EmployeeID,null,null,SAR.SAInvoiceID,GL.Description,GL.Date,GL.No,GL.DetailID,GL.InvoiceNo,GL.InvoiceDate,SAR.MaterialGoodsID,null,null,SAR.Unit,0,SAR.UnitPrice,0,0,SAR.Quantity,0,0,2
			FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
			WHERE (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
			AND (GL.DetailID in (select ID from SAReturnDetail)) AND SA.BillRefID is null
			AND GL.PostedDate >=@FromDate 
			AND GL.PostedDate <@ToDate AND GL.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			AND GL.TypeID IN ('330') and SAR.MaterialGoodsID IN (SELECT * from @tblListMaterialsGoodID) AND GL.EmployeeID IN (SELECT * from @tblListEmployeeID)
			AND Account LIKE '511%'
			UNION ALL
			SELECT DISTINCT GL.AccountingObjectID,null,null,GL.EmployeeID,null,null,SAR.SAInvoiceID,GL.Description,GL.Date,GL.No,GL.DetailID,SAB.InvoiceNo,SAB.InvoiceDate,SAD.MaterialGoodsID,null,null,SAR.Unit,0,SAR.UnitPrice,0,0,SAR.Quantity,0,0,2
			FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
			LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
			WHERE (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
			AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAReturnDetail)) AND SA.BillRefID is not null
			AND GL.PostedDate >=@FromDate 
			AND GL.PostedDate <@ToDate AND GL.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			AND GL.TypeID IN ('330') and SAR.MaterialGoodsID IN (SELECT * from @tblListMaterialsGoodID) AND GL.EmployeeID IN (SELECT * from @tblListEmployeeID)
			AND Account LIKE '511%'
			
			
			UPDATE @tblResult SET ChietKhau = (0 - a.CA)
			FROM (
			Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description D, DetailID DID
			From @tbDataGL
			WHERE  Account LIKE '511%' and TypeID = '330' and CreditAmount >0
			) a
			 WHERE Type = 2 and Description = a.D and DetailID = a.DID
			
			

		UPDATE @tblResult SET GiaTriTraLai = a.DA
		FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount as DA, Account A, AccountCorresponding AC, Description D, DetailID DID
		From @tbDataGL 
		WHERE  Account LIKE '511%' and TypeID = '330' and DebitAmount >0
		) a
		WHERE Type = 2  and Description = a.D and DetailID = a.DID

			 			INSERT INTO @tblResult 
			SELECT DISTINCT GL.AccountingObjectID,null,null,GL.EmployeeID,null,null,SAR.SAInvoiceID,GL.Description,GL.Date,GL.No,GL.DetailID,SAB.InvoiceNo,SAB.InvoiceDate,SAR.MaterialGoodsID,null,null,SAR.Unit,0,SAR.UnitPrice,0,0,0,0,0,3
			FROM @tbDataGL GL 
			LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID 
			LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID 
			LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
			LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
			WHERE GL.PostedDate >=@FromDate 
			AND GL.PostedDate <@ToDate AND GL.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			AND GL.TypeID IN ('340') and SAR.MaterialGoodsID IN (SELECT * from @tblListMaterialsGoodID) AND GL.EmployeeID IN (SELECT * from @tblListEmployeeID)
			AND Account LIKE '511%'
			UNION ALL
			SELECT DISTINCT GL.AccountingObjectID,null,null,GL.EmployeeID,null,null,SAR.SAInvoiceID,GL.Description,GL.Date,GL.No,GL.DetailID,GL.InvoiceNo,GL.InvoiceDate,SAR.MaterialGoodsID,null,null,SAR.Unit,0,SAR.UnitPrice,0,0,0,0,0,3
			FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
			WHERE (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
			AND (GL.DetailID in (select ID from SAReturnDetail)) AND SA.BillRefID is null
			AND GL.PostedDate >=@FromDate 
			AND GL.PostedDate <@ToDate AND GL.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			AND GL.TypeID IN ('340') and SAR.MaterialGoodsID IN (SELECT * from @tblListMaterialsGoodID) AND GL.EmployeeID IN (SELECT * from @tblListEmployeeID)
			AND Account LIKE '511%'
			
			
			
			UPDATE @tblResult SET ChietKhau =(0 - a.CA)
			FROM (
			Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description D, DetailID DID
			From @tbDataGL
			WHERE Account LIKE '511%' AND TypeID IN ('340') AND CreditAmount >0
			) a
			 WHERE Type = 3  and Description = a.D and DetailID = a.DID and AccountingObjectID = a.AOI
		
		UPDATE @tblResult SET GiamGia = a.DA
			FROM (
			Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description D, DetailID DID
			From @tbDataGL
			WHERE Account LIKE '511%' AND TypeID IN ('340') AND DebitAmount >0
			) a
			 WHERE Type = 3  and Description = a.D and DetailID = a.DID and AccountingObjectID = a.AOI
			
			UPDATE @tblResult SET AccountingObjectCode = a.AOC, AccountingObjectName = a.AON
			FROM(
			SELECT ID as AOI, AccountingObjectCode as AOC, AccountingObjectName as AON
			from AccountingObject
			) a
			where AccountingObjectID = a.AOI

				UPDATE @tblResult SET MaterialGoodsCode = a.MGC, MaterialGoodsName = a.MGN
			FROM(
			SELECT ID as MGI, MaterialGoodsCode as MGC, MaterialGoodsName as MGN
			from MaterialGoods
			) a
			where MaterialGoodsID = a.MGI

			UPDATE @tblResult SET EmployeeName = a.AON, EmployeeCode = a.AOC
			FROM(
			SELECT ID as AOI, AccountingObjectName as AON, AccountingObjectCode AOC
			from AccountingObject
			) a
			where EmployeeID =a.AOI 
		
	SELECT * From @tblResult order by Date,Type
   END
   END


	
	
	 
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_SA_TongHopCongNoNhanVien]
	@FromDate DATETIME,
    @ToDate DATETIME,
    @Account NVARCHAR(25),
    @AccountObjectID AS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @tbluutru TABLE (
		AccountingObjectID UNIQUEIDENTIFIER,
		AccountingObjectCode NVARCHAR(25),
		AccountingObjectName NVARCHAR(512),
		TK nvarchar(25),
		NoDK decimal(19,0),
		CoDK decimal(19,0),
		NoPS decimal(19,0),
		CoPS decimal(19,0),
		NoCK decimal(19,0),
		CoCK decimal(19,0)
    )
	DECLARE @tbAccountObjectID TABLE(
			AccountingObjectID UNIQUEIDENTIFIER
		)
	INSERT @tbAccountObjectID
	SELECT tblAccOjectSelect.Value as AccountingObjectID
	FROM dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') tblAccOjectSelect
	WHERE tblAccOjectSelect.Value in (SELECT ID FROM AccountingObject)
    INSERT INTO @tbluutru(AccountingObjectID, AccountingObjectCode, AccountingObjectName)
		SELECT ID as AccountingObjectID, AccountingObjectCode, AccountingObjectName
		FROM AccountingObject
		WHERE ID in (select AccountingObjectID from @tbAccountObjectID )

	DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate

	if(@Account='all')
	begin
		UPDATE @tbluutru SET NoPS = d.NoPS , CoPS = d.CoPS, TK = d.TK
		FROM (select c.MaNV,c.TenNV,c.TK,sum(c.NoPS) as NoPS, SUM(c.CoPS) as CoPS
				from
					(SELECT b.AccountingObjectCode as MaNV, b.AccountingObjectName as TenNV, a.DebitAmount as NoPS, a.CreditAmount as CoPS, a.Account as TK
					FROM @tbDataGL a join @tbluutru b on a.AccountingObjectID = b.AccountingObjectID
					where Date between @FromDate and @ToDate
					and a.Account in (select AccountNumber from Account where DetailType = '2')
			) c
			group by c.MaNV, c.TenNV, c.TK) d
			where AccountingObjectCode = d.MaNV
		UPDATE @tbluutru SET NoDK = d.NoDK , CoDK = d.CoDK, TK = d.TK
		FROM (select c.MaNV,c.TenNV,c.TK,sum(c.NoDK) as NoDK, SUM(c.CoDK) as CoDK
				from
					(SELECT b.AccountingObjectCode as MaNV, b.AccountingObjectName as TenNV, a.DebitAmount as NoDK, a.CreditAmount as CoDK, a.Account as TK
					FROM @tbDataGL a join @tbluutru b on a.AccountingObjectID = b.AccountingObjectID
					where Date < @FromDate
					and a.Account in (select AccountNumber from Account where DetailType = '2')
			) c
			group by c.MaNV, c.TenNV, c.TK) d
			where AccountingObjectCode = d.MaNV
	end
	else
	begin
		UPDATE @tbluutru SET NoPS = d.NoPS , CoPS = d.CoPS, TK = d.TK
		FROM (select c.MaNV,c.TenNV,c.TK,sum(c.NoPS) as NoPS, SUM(c.CoPS) as CoPS
				from
					(SELECT b.AccountingObjectCode as MaNV, b.AccountingObjectName as TenNV, a.DebitAmount as NoPS, a.CreditAmount as CoPS, a.Account as TK
					FROM @tbDataGL a join @tbluutru b on a.AccountingObjectID = b.AccountingObjectID
					where Date between @FromDate and @ToDate
					and a.Account LIKE (@Account +'%')
			) c
			group by c.MaNV, c.TenNV, c.TK) d
			where AccountingObjectCode = d.MaNV
		UPDATE @tbluutru SET NoDK = d.NoDK , CoDK = d.CoDK, TK = d.TK
		FROM (select c.MaNV,c.TenNV,c.TK,sum(c.NoDK) as NoDK, SUM(c.CoDK) as CoDK
				from
					(SELECT b.AccountingObjectCode as MaNV, b.AccountingObjectName as TenNV, a.DebitAmount as NoDK, a.CreditAmount as CoDK, a.Account as TK
					FROM @tbDataGL a join @tbluutru b on a.AccountingObjectID = b.AccountingObjectID
					where Date < @FromDate
					and a.Account LIKE (@Account +'%')
			) c
			group by c.MaNV, c.TenNV, c.TK) d
			where AccountingObjectCode = d.MaNV
	end

	select * from @tbluutru
	where NoDK != 0 or CoDK != 0 or NoPS != 0 or CoPS != 0
	order by AccountingObjectCode

END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_ReceivableDetail]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3) ,
    @IsSimilarSum BIT 
AS
    BEGIN
		
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        DECLARE @ID UNIQUEIDENTIFIER
        SET @ID = '00000000-0000-0000-0000-000000000000'
               
        CREATE TABLE #tblListAccountObjectID  
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25)
                 ,
              AccountObjectName NVARCHAR(512)
                 ,
              AccountObjectAddress NVARCHAR(512)
                 ,
              AccountObjectGroupListCode NVARCHAR(MAX)
                
                NULL ,
              AccountObjectGroupListName NVARCHAR(MAX)
                
                NULL ,
              CompanyTaxCode NVARCHAR(50) 
                                          NULL
            ) 
        INSERT  INTO #tblListAccountObjectID
                SELECT  AO.ID ,
                        AccountingObjectCode ,
                        AccountingObjectName ,
                        [Address] ,
                        AOG.AccountingObjectGroupCode , 
                        AOG.AccountingObjectGroupName , 
                        CASE WHEN AO.ObjectType = 0 THEN TaxCode
                             ELSE ''
                        END AS CompanyTaxCode 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                           ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value 
                        left JOIN dbo.AccountingObjectGroup AOG ON AO.AccountObjectGroupID = AOG.ID 
          
        CREATE TABLE #tblResult
            (
              RowNum INT PRIMARY KEY
                         IDENTITY(1, 1) ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100)
                
                NULL ,
              AccountObjectName NVARCHAR(512)
                
                NULL ,
              AccountObjectGroupListCode NVARCHAR(MAX)
                
                NULL , 
              AccountObjectGroupListName NVARCHAR(MAX)
                
                NULL ,          
              AccountObjectAddress NVARCHAR(512)
                
                NULL , 
              CompanyTaxCode NVARCHAR(50) 
                                          NULL ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25) 
                                 NULL ,
              InvDate DATETIME ,
              InvNo NVARCHAR(MAX) 
                                  NULL ,
              
              RefType INT ,
              RefID UNIQUEIDENTIFIER ,                        
              RefDetailID NVARCHAR(50) 
                                       NULL ,
              JournalMemo NVARCHAR(512) 
                                        NULL ,
              AccountNumber NVARCHAR(20) 
                                         NULL ,
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20)
                
                NULL ,
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,
              DebitAmount MONEY , 
              CreditAmountOC MONEY ,
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY , 
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50)
                 , 
              InventoryItemName NVARCHAR(512)
                 ,
              UnitName NVARCHAR(20) 
                                    NULL ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(20, 6) ,            
              IsBold BIT , 
              OrderType INT ,
              BranchName NVARCHAR(512) 
                                       NULL ,
              GLJournalMemo NVARCHAR(512) 
                                          NULL 
              ,
              MasterCustomField1 NVARCHAR(255)
                
                NULL ,
              MasterCustomField2 NVARCHAR(512)
                
                NULL ,
              MasterCustomField3 NVARCHAR(512)
                
                NULL ,
              MasterCustomField4 NVARCHAR(512)
                
                NULL ,
              MasterCustomField5 NVARCHAR(512)
                
                NULL ,
              MasterCustomField6 NVARCHAR(512)
                
                NULL ,
              MasterCustomField7 NVARCHAR(512)
                
                NULL ,
              MasterCustomField8 NVARCHAR(512)
                
                NULL ,
              MasterCustomField9 NVARCHAR(512)
                
                NULL ,
              MasterCustomField10 NVARCHAR(512)
                
                NULL ,
              CustomField1 NVARCHAR(512) 
                                         NULL ,
              CustomField2 NVARCHAR(512) 
                                         NULL ,
              CustomField3 NVARCHAR(512) 
                                         NULL ,
              CustomField4 NVARCHAR(512) 
                                         NULL ,
              CustomField5 NVARCHAR(512) 
                                         NULL ,
              CustomField6 NVARCHAR(512) 
                                         NULL ,
              CustomField7 NVARCHAR(512) 
                                         NULL ,
              CustomField8 NVARCHAR(512) 
                                         NULL ,
              CustomField9 NVARCHAR(512) 
                                         NULL ,
              CustomField10 NVARCHAR(512) 
                                          NULL ,
              /*cột số lượng, đơn giá*/
              MainUnitName NVARCHAR(255) 
                                         NULL ,
              MainUnitPrice DECIMAL(20, 6) ,
              MainQuantity DECIMAL(22, 8) ,
              /*mã thống kê, và tên loại chứng từ*/
              ListItemCode NVARCHAR(20) ,
              ListItemName NVARCHAR(128) ,
              RefTypeName NVARCHAR(128),
			  OrderPriority int
            )
        
        CREATE TABLE #tblAccountNumber
            (
              AccountNumber NVARCHAR(20) 
                                         PRIMARY KEY ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL
            BEGIN
			INSERT  INTO #tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
            END
        ELSE
            BEGIN 
			   INSERT  INTO #tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   DetailType = '1'
                                AND Grade = 1
                                AND IsParentNode = 0
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END 

/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
		
		IF(@CurrencyID = 'VND')
		BEGIN
		IF @IsSimilarSum = 0 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,  
                          AccountObjectName ,	
                          AccountObjectGroupListCode ,
                          AccountObjectGroupListName ,        
                          AccountObjectAddress , 
                          CompanyTaxCode , 
                          PostedDate ,
                          RefDate , 
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,	
                          RefID ,  
                          RefDetailID ,
                          JournalMemo , 				  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,			  
                          ExchangeRate , 		  
                          DebitAmountOC ,
                          DebitAmount , 
                          CreditAmountOC , 
                          CreditAmount , 
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,	
                          ClosingCreditAmount ,               
                          IsBold , 
                          OrderType,
						  OrderPriority
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode , 
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,           
                                AccountObjectAddress ,
                                CompanyTaxCode , 
                                PostedDate ,
                                RefDate ,
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID ,
                                RefDetailID ,
                                JournalMemo , 				  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  
                                ExchangeRate ,  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) ,
                                SUM(CreditAmount) ,
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN SUM(ClosingDebitAmountOC)
                                            - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) ) > 0
                                                 THEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,         
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( SUM(ClosingDebitAmount)
                                              - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount)
                                                        - SUM(ClosingCreditAmount) ) > 0
                                                 THEN SUM(ClosingDebitAmount)
                                                      - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmountOC)
                                              - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) ) > 0
                                                 THEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmount)
                                              - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) ) > 0
                                                 THEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,
                                
                                IsBold , 
                                OrderType ,
								OrderPriority

                                
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,	
                                            LAOI.AccountObjectGroupListCode ,
                                            LAOI.AccountObjectGroupListName ,          
                                            LAOI.AccountObjectAddress , 
                                            LAOI.CompanyTaxCode , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,             
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefDate
                                            END AS RefDate , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefNo
                                            END AS RefNo , 
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN N'Số dư đầu kỳ'
                                                 ELSE ISNULL(AOL.DESCRIPTION,
                                                             AOL.Reason)
                                            END AS JournalMemo ,   
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber ,
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ExchangeRate
                                            END AS ExchangeRate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                               
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                     
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC ,           
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 1
                                                 ELSE 0
                                            END AS IsBold ,	
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType ,
											 CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN null
                                                 ELSE OrderPriority
                                            END AS OrderPriority 
                                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                            INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                            INNER JOIN #tblListAccountObjectID
                                            AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                  WHERE     AOL.PostedDate <= @ToDate
     
                                ) AS RSNS
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode , 
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,        
                                RSNS.AccountObjectAddress ,
                                CompanyTaxCode , 
                                RSNS.PostedDate ,
                                RSNS.RefDate ,
                                RSNS.RefNo , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,   
                                RSNS.RefDetailID ,
                                RSNS.JournalMemo , 				  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber , 		  
                                RSNS.ExchangeRate ,           
                                RSNS.IsBold ,
                                RSNS.OrderType,
								RSNS.OrderPriority
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC
                                       - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount
                                       - ClosingCreditAmount) <> 0
                        ORDER BY RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.OrderType ,
                                RSNS.PostedDate ,
								RSNS.OrderPriority,
                                RSNS.RefDate ,
                                RSNS.RefNo 
            END
        ELSE 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,   
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,            
                          AccountObjectAddress ,
                          CompanyTaxCode ,
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID ,   
                          RefDetailID ,
                          JournalMemo , 			  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,		  
                          ExchangeRate ,		  
                          DebitAmountOC ,
                          DebitAmount ,
                          CreditAmountOC , 
                          CreditAmount ,
                          ClosingDebitAmountOC ,
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount ,                                    
                          IsBold , 
                          OrderType,
						  OrderPriority
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,  
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,             
                                AccountObjectAddress , 
                                CompanyTaxCode ,
                                PostedDate ,	
                                RefDate , 
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID , 
                                RefDetailID ,
                                JournalMemo ,				  
                                AccountNumber ,
                                AccountCategoryKind ,
                                CorrespondingAccountNumber ,			  
                                ExchangeRate , 	  
                                DebitAmountOC ,
                                DebitAmount ,
                                CreditAmountOC ,
                                CreditAmount , 
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmountOC
                                              - ClosingCreditAmountOC )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmountOC
                                                        - ClosingCreditAmountOC ) > 0
                                                 THEN ClosingDebitAmountOC
                                                      - ClosingCreditAmountOC
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,  
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmount
                                              - ClosingCreditAmount )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmount
                                                        - ClosingCreditAmount ) > 0
                                                 THEN ClosingDebitAmount
                                                      - ClosingCreditAmount
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmountOC
                                              - ClosingDebitAmountOC )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC ) > 0
                                                 THEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmount
                                              - ClosingDebitAmount )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount ) > 0
                                                 THEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,                                     
                                IsBold , 
                                OrderType,
								OrderPriority
                        FROM    ( SELECT    AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode ,
                                            AD.AccountObjectGroupListName ,            
                                            AD.AccountObjectAddress ,
                                            CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType , 
                                            MAX(AD.RefDetailID) AS RefDetailID ,
                                            AD.JournalMemo ,   
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber , 
                                            AD.ExchangeRate , 
                                            SUM(AD.DebitAmountOC) AS DebitAmountOC ,                              
                                            SUM(AD.DebitAmount) AS DebitAmount ,                                        
                                            SUM(AD.CreditAmountOC) AS CreditAmountOC , 
                                            SUM(AD.CreditAmount) AS CreditAmount ,
                                            SUM(AD.ClosingDebitAmountOC) AS ClosingDebitAmountOC ,       
                                            SUM(AD.ClosingDebitAmount) AS ClosingDebitAmount ,
                                            SUM(AD.ClosingCreditAmountOC) AS ClosingCreditAmountOC , 
                                            SUM(AD.ClosingCreditAmount) AS ClosingCreditAmount ,
                                            AD.IsBold ,	
                                            AD.OrderType ,
											AD.OrderPriority
											
                                  FROM      ( SELECT    AOL.AccountingObjectID ,
                                                        LAOI.AccountObjectCode ,   
                                                        LAOI.AccountObjectName ,	
                                                        LAOI.AccountObjectGroupListCode ,
                                                        LAOI.AccountObjectGroupListName ,              
                                                        LAOI.AccountObjectAddress , 
                                                        LAOI.CompanyTaxCode , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.PostedDate
                                                        END AS PostedDate ,             
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefDate
                                                        END AS RefDate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefNo
                                                        END AS RefNo , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceDate
                                                        END AS InvDate ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceNo
                                                        END AS InvNo ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.ReferenceID
                                                        END AS RefID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.TypeID
                                                        END AS RefType ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE CAST(DetailID AS NVARCHAR(50))
                                                        END AS RefDetailID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN N'Số dư đầu kỳ'
                                                             ELSE AOL.Reason
                                                        END AS JournalMemo , 
                                                        TBAN.AccountNumber ,
                                                        TBAN.AccountCategoryKind ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.AccountCorresponding
                                                        END AS CorrespondingAccountNumber , 
                                                        
                                                        CASE WHEN AOL.PostedDate < @FromDate
															 THEN NULL
															 ELSE AOL.ExchangeRate
														END AS ExchangeRate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmountOriginal
                                                        END AS DebitAmountOC ,                         
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmount
                                                        END AS DebitAmount ,                           
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmountOriginal
                                                        END AS CreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmount
                                                        END AS CreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingDebitAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmount
                                                             ELSE $0
                                                        END AS ClosingDebitAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingCreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmount
                                                             ELSE $0
                                                        END AS ClosingCreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 1
                                                             ELSE 0
                                                        END AS IsBold ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 0
                                                             ELSE 1
                                                        END AS OrderType ,
                                                           CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 0
                                                             ELSE OrderPriority
                                                        END AS OrderPriority 
                                              FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                                        INNER JOIN #tblListAccountObjectID
                                                        AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                                        INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                              WHERE     AOL.PostedDate <= @ToDate
                                            ) AS AD
                                  GROUP BY  AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode , 
                                            AD.AccountObjectGroupListName ,     
                                            AD.AccountObjectAddress ,
                                            AD.CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType ,
                                            AD.JournalMemo ,
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber ,  
                                            AD.ExchangeRate ,
                                            AD.IsBold , 	
                                            AD.OrderType,
											AD.OrderPriority
                                ) AS RSS
                        WHERE   RSS.DebitAmountOC <> 0
                                OR RSS.CreditAmountOC <> 0
                                OR RSS.DebitAmount <> 0
                                OR RSS.CreditAmount <> 0
                                OR ClosingCreditAmountOC
                                - ClosingDebitAmountOC <> 0
                                OR ClosingCreditAmount - ClosingDebitAmount <> 0
                        ORDER BY RSS.AccountObjectCode ,
                                RSS.AccountNumber ,
                                RSS.OrderType ,
                                RSS.PostedDate ,
								RSS.OrderPriority,
                                RSS.RefDate ,
                                RSS.RefNo ,
                                RSS.InvDate ,
                                RSS.InvNo
            END
		END
		ELSE
		BEGIN
        IF @IsSimilarSum = 0 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,  
                          AccountObjectName ,	
                          AccountObjectGroupListCode ,
                          AccountObjectGroupListName ,        
                          AccountObjectAddress , 
                          CompanyTaxCode , 
                          PostedDate ,
                          RefDate , 
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,	
                          RefID ,  
                          RefDetailID ,
                          JournalMemo , 				  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,			  
                          ExchangeRate , 		  
                          DebitAmountOC ,
                          DebitAmount , 
                          CreditAmountOC , 
                          CreditAmount , 
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,	
                          ClosingCreditAmount ,               
                          IsBold , 
                          OrderType,
						  OrderPriority
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode , 
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,           
                                AccountObjectAddress ,
                                CompanyTaxCode , 
                                PostedDate ,
                                RefDate ,
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID ,
                                RefDetailID ,
                                JournalMemo , 				  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  
                                ExchangeRate ,  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) ,
                                SUM(CreditAmount) ,
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN SUM(ClosingDebitAmountOC)
                                            - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) ) > 0
                                                 THEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,         
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( SUM(ClosingDebitAmount)
                                              - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount)
                                                        - SUM(ClosingCreditAmount) ) > 0
                                                 THEN SUM(ClosingDebitAmount)
                                                      - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmountOC)
                                              - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) ) > 0
                                                 THEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmount)
                                              - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) ) > 0
                                                 THEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,
                                
                                IsBold , 
                                OrderType ,
								OrderPriority

                                
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,	
                                            LAOI.AccountObjectGroupListCode ,
                                            LAOI.AccountObjectGroupListName ,          
                                            LAOI.AccountObjectAddress , 
                                            LAOI.CompanyTaxCode , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,             
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefDate
                                            END AS RefDate , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefNo
                                            END AS RefNo , 
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN N'Số dư đầu kỳ'
                                                 ELSE ISNULL(AOL.DESCRIPTION,
                                                             AOL.Reason)
                                            END AS JournalMemo ,   
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber ,
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ExchangeRate
                                            END AS ExchangeRate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                               
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmount ,                                     
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC ,           
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 1
                                                 ELSE 0
                                            END AS IsBold ,	
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType ,
											 CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN null
                                                 ELSE OrderPriority
                                            END AS OrderPriority 
                                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                            INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                            INNER JOIN #tblListAccountObjectID
                                            AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )

                                ) AS RSNS
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode , 
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,        
                                RSNS.AccountObjectAddress ,
                                CompanyTaxCode , 
                                RSNS.PostedDate ,
                                RSNS.RefDate ,
                                RSNS.RefNo , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,   
                                RSNS.RefDetailID ,
                                RSNS.JournalMemo , 				  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber , 		  
                                RSNS.ExchangeRate ,           
                                RSNS.IsBold ,
                                RSNS.OrderType,
								RSNS.OrderPriority
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC
                                       - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount
                                       - ClosingCreditAmount) <> 0
                        ORDER BY RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.OrderType ,
                                RSNS.PostedDate ,
								RSNS.OrderPriority,
                                RSNS.RefDate ,
                                RSNS.RefNo 
            END
        ELSE 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,   
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,            
                          AccountObjectAddress ,
                          CompanyTaxCode ,
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID ,   
                          RefDetailID ,
                          JournalMemo , 			  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,		  
                          ExchangeRate ,		  
                          DebitAmountOC ,
                          DebitAmount ,
                          CreditAmountOC , 
                          CreditAmount ,
                          ClosingDebitAmountOC ,
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount ,                                    
                          IsBold , 
                          OrderType,
						  OrderPriority
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,  
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,             
                                AccountObjectAddress , 
                                CompanyTaxCode ,
                                PostedDate ,	
                                RefDate , 
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID , 
                                RefDetailID ,
                                JournalMemo ,				  
                                AccountNumber ,
                                AccountCategoryKind ,
                                CorrespondingAccountNumber ,			  
                                ExchangeRate , 	  
                                DebitAmountOC ,
                                DebitAmount ,
                                CreditAmountOC ,
                                CreditAmount , 
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmountOC
                                              - ClosingCreditAmountOC )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmountOC
                                                        - ClosingCreditAmountOC ) > 0
                                                 THEN ClosingDebitAmountOC
                                                      - ClosingCreditAmountOC
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,  
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmount
                                              - ClosingCreditAmount )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmount
                                                        - ClosingCreditAmount ) > 0
                                                 THEN ClosingDebitAmount
                                                      - ClosingCreditAmount
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmountOC
                                              - ClosingDebitAmountOC )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC ) > 0
                                                 THEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmount
                                              - ClosingDebitAmount )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount ) > 0
                                                 THEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,                                     
                                IsBold , 
                                OrderType,
								OrderPriority
                        FROM    ( SELECT    AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode ,
                                            AD.AccountObjectGroupListName ,            
                                            AD.AccountObjectAddress ,
                                            CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType , 
                                            MAX(AD.RefDetailID) AS RefDetailID ,
                                            AD.JournalMemo ,   
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber , 
                                            AD.ExchangeRate , 
                                            SUM(AD.DebitAmountOC) AS DebitAmountOC ,                              
                                            SUM(AD.DebitAmount) AS DebitAmount ,                                        
                                            SUM(AD.CreditAmountOC) AS CreditAmountOC , 
                                            SUM(AD.CreditAmount) AS CreditAmount ,
                                            SUM(AD.ClosingDebitAmountOC) AS ClosingDebitAmountOC ,       
                                            SUM(AD.ClosingDebitAmount) AS ClosingDebitAmount ,
                                            SUM(AD.ClosingCreditAmountOC) AS ClosingCreditAmountOC , 
                                            SUM(AD.ClosingCreditAmount) AS ClosingCreditAmount ,
                                            AD.IsBold ,	
                                            AD.OrderType ,
											AD.OrderPriority
											
                                  FROM      ( SELECT    AOL.AccountingObjectID ,
                                                        LAOI.AccountObjectCode ,   
                                                        LAOI.AccountObjectName ,	
                                                        LAOI.AccountObjectGroupListCode ,
                                                        LAOI.AccountObjectGroupListName ,              
                                                        LAOI.AccountObjectAddress , 
                                                        LAOI.CompanyTaxCode , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.PostedDate
                                                        END AS PostedDate ,             
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefDate
                                                        END AS RefDate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefNo
                                                        END AS RefNo , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceDate
                                                        END AS InvDate ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceNo
                                                        END AS InvNo ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.ReferenceID
                                                        END AS RefID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.TypeID
                                                        END AS RefType ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE CAST(DetailID AS NVARCHAR(50))
                                                        END AS RefDetailID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN N'Số dư đầu kỳ'
                                                             ELSE AOL.Reason
                                                        END AS JournalMemo , 
                                                        TBAN.AccountNumber ,
                                                        TBAN.AccountCategoryKind ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.AccountCorresponding
                                                        END AS CorrespondingAccountNumber , 
                                                        
                                                        CASE WHEN AOL.PostedDate < @FromDate
															 THEN NULL
															 ELSE AOL.ExchangeRate
														END AS ExchangeRate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmountOriginal
                                                        END AS DebitAmountOC ,                         
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmountOriginal
                                                        END AS DebitAmount ,                           
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmountOriginal
                                                        END AS CreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmountOriginal
                                                        END AS CreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingDebitAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingDebitAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingCreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingCreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 1
                                                             ELSE 0
                                                        END AS IsBold ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 0
                                                             ELSE 1
                                                        END AS OrderType ,
                                                           CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 0
                                                             ELSE OrderPriority
                                                        END AS OrderPriority 
                                              FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                                        INNER JOIN #tblListAccountObjectID
                                                        AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                                        INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                              WHERE     AOL.PostedDate <= @ToDate
                                                        AND ( @CurrencyID IS NULL
                                                              OR AOL.CurrencyID = @CurrencyID
                                                            )
                                            ) AS AD
                                  GROUP BY  AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode , 
                                            AD.AccountObjectGroupListName ,     
                                            AD.AccountObjectAddress ,
                                            AD.CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType ,
                                            AD.JournalMemo ,
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber ,  
                                            AD.ExchangeRate ,
                                            AD.IsBold , 	
                                            AD.OrderType,
											AD.OrderPriority
                                ) AS RSS
                        WHERE   RSS.DebitAmountOC <> 0
                                OR RSS.CreditAmountOC <> 0
                                OR RSS.DebitAmount <> 0
                                OR RSS.CreditAmount <> 0
                                OR ClosingCreditAmountOC
                                - ClosingDebitAmountOC <> 0
                                OR ClosingCreditAmount - ClosingDebitAmount <> 0
                        ORDER BY RSS.AccountObjectCode ,
                                RSS.AccountNumber ,
                                RSS.OrderType ,
                                RSS.PostedDate ,
								RSS.OrderPriority,
                                RSS.RefDate ,
                                RSS.RefNo ,
                                RSS.InvDate ,
                                RSS.InvNo
            END
			END
/* Tính số tồn */
        DECLARE @CloseAmountOC AS DECIMAL(22, 8) ,
            @CloseAmount AS DECIMAL(22, 8) ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0
                                        THEN ( CASE WHEN ClosingDebitAmountOC = 0
                                                    THEN ClosingCreditAmountOC
                                                    ELSE -1
                                                         * ClosingDebitAmountOC
                                               END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber
                                        THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC
                                             - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0
                                              THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1
                                              THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0
                                                        THEN -1
                                                             * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1
                                               THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0
                                               THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0
                                                         THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0
                                      THEN ( CASE WHEN ClosingDebitAmount = 0
                                                  THEN ClosingCreditAmount
                                                  ELSE -1 * ClosingDebitAmount
                                             END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber
                                      THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount
                                           - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0
                                            THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1
                                            THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0
                                                      THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1
                                             THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0
                                             THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0
                                                       THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
        SELECT  RS.*
        FROM    #tblResult RS
        ORDER BY RowNum
        
        DROP TABLE #tblResult
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*Edit by: cuongpv
*Edit Date: 07/05/2019
*Description: lay them du lieu tu cac bang PPService, PPDiscountReturn, SAReturn
*/
ALTER procedure [dbo].[Proc_SA_GetDetailPayS12] 
(@fromdate DATETIME,
 @todate DATETIME,
 @account nvarchar(255),
 @currency nvarchar(3),
 @AccountObjectID AS NVARCHAR(MAX)
)
as
begin
DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(15),
			  AccountGroupKind int
            )
INSERT  INTO @tblAccountNumber
                SELECT  AO.AccountNumber,
				        AO.AccountGroupKind
                FROM    Account AS AO
                        inner JOIN dbo.Func_ConvertStringIntoTable_Nvarchar(@account,
                                                              ',') AS TLAO ON AO.AccountNumber = TLAO.Value
DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
			  AccountObjectCode NVARCHAR(100),
              AccountObjectGroupListCode NVARCHAR(MAX) ,
              AccountObjectCategoryName NVARCHAR(MAX)
            ) 
             
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID ,
				        AO.AccountingObjectCode,
                        AO.AccountObjectGroupID ,
						null
                FROM    AccountingObject AS AO
                        inner JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                              ',') AS TLAO ON AO.ID = TLAO.Value
                        

		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @todate
		/*end add by cuongpv*/

DECLARE @tblResult TABLE
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              AccountNumber NVARCHAR(20)  ,
			  AccountGroupKind int, 
              AccountObjectID UNIQUEIDENTIFIER ,
			  AccountObjectCode NVARCHAR(100)  ,
			  RefDate DATETIME , 
			  RefNo NVARCHAR(25)  ,
			  PostedDate DATETIME ,
			  JournalMemo NVARCHAR(512)  ,
			  CorrespondingAccountNumber NVARCHAR(20)  , 
			  DueDate DATETIME,
			  DebitAmount MONEY , 
              CreditAmount MONEY , 
			  ClosingDebitAmount MONEY , 
              ClosingCreditAmount MONEY,
			  OrderType int,
			  OrderPriority int
            )

if @account = '331'
Begin
	/*add by cuongpv tao bang du lieu DueDate tu cac bang*/
	DECLARE @tbDataDueDate TABLE(
		ID UNIQUEIDENTIFIER,
		IDDetail UNIQUEIDENTIFIER,
		DueDate DATETIME
	)
	/*end add by cuongpv*/
	/*add by cuongpv lay du lieu DueDate tu cac bang PPinvoice, PPservice, PPDiscountReturn*/
	INSERT INTO @tbDataDueDate
	SELECT Piv.ID, Pivdt.ID, Piv.DueDate FROM PPInvoiceDetail Pivdt LEFT JOIN PPInvoice Piv ON Piv.ID = Pivdt.PPInvoiceID
	WHERE Pivdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)
	
	UNION ALL

	SELECT Psv.ID, Psvdt.ID, Psv.DueDate FROM PPServiceDetail Psvdt LEFT JOIN PPService Psv ON Psv.ID = Psvdt.PPServiceID
	WHERE Psvdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)

	UNION ALL

	SELECT Pdcrt.ID, Pdcrtdt.ID, Pdcrt.DueDate FROM PPDiscountReturnDetail Pdcrtdt LEFT JOIN PPDiscountReturn Pdcrt ON Pdcrt.ID = Pdcrtdt.PPDiscountReturnID
	WHERE Pdcrtdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)
	/*end add by cuongpv*/
	insert into @tblResult
	SELECT  a.AccountNumber,
			A.AccountGroupKind,
			LAOI.AccountObjectID,
			LAOI.AccountObjectCode,
			null as NgayThangGS,
			null as Sohieu, 
			null as ngayCTu,
			N'Số dư đầu kỳ' as Diengiai ,
			null as TKDoiUng, 
			null as ThoiHanDuocCKhau, 
			$0 AS PSNO ,
			$0 AS PSCO ,
			CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
					THEN SUM(GL.DebitAmount - GL.CreditAmount)
					ELSE 0
			END AS DuNo ,
			CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
					THEN SUM(GL.CreditAmount - GL.DebitAmount)
					ELSE 0
			END AS DuCo,
			0 AS OrderType,
			OrderPriority
		FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
		INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber 
		INNER JOIN @tblListAccountObjectID AS LAOI ON GL.AccountingObjectID = LAOI.AccountObjectID
		WHERE   ( GL.PostedDate < @FromDate )  
		and GL.CurrencyID = @currency                    
		GROUP BY 
			A.AccountNumber,LAOI.AccountObjectID,A.AccountGroupKind,LAOI.AccountObjectCode,OrderPriority
		HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
	UNION ALL
	select  an.AccountNumber,
			An.AccountGroupKind,
			LAOI.AccountObjectID,
			LAOI.AccountObjectCode,
			a.PostedDate as NgayThangGS, 
			a.No as Sohieu, 
			a.Date as ngayCTu,
			a.Description as Diengiai, 
			a.AccountCorresponding as TKDoiUng, 
			DueDate as ThoiHanDuocCKhau, 
			sum(a.DebitAmount) as PSNO, 
			sum(a.CreditAmount) as PSCO,
			sum(fnsd.DebitAmount) DuNo,
			sum(fnsd.CreditAmount) DuCo,
			1 AS OrderType,
			OrderPriority
	 from @tbDataGL a /*edit by cuongpv GeneralLedger -> @tbDataGL*/		
	LEFT JOIN @tbDataDueDate tbDuedate ON tbDuedate.IDDetail = a.DetailID
	inner join @tblAccountNumber as an on a.Account = an.AccountNumber
	INNER JOIN @tblListAccountObjectID AS LAOI ON a.AccountingObjectID = LAOI.AccountObjectID
	INNER JOIN Func_SoDu (
	   @fromdate
	  ,@todate
	  ,1) AS fnsd ON LAOI.AccountObjectID = fnsd.AccountObjectID and an.AccountNumber = fnsd.AccountNumber			
	where a.PostedDate between @fromdate and @todate
	and  a.CurrencyID = @currency  
	group by an.AccountNumber,
			LAOI.AccountObjectID,
			a.PostedDate,
			a.No , 
			a.Date ,
			a.Description, 
			a.AccountCorresponding, 
			DueDate,
			An.AccountGroupKind,
			LAOI.AccountObjectCode,
			OrderPriority
	order by AccountNumber,NgayThangGS,OrderPriority
End
else
Begin
	/*add by cuongpv tao bang du lieu DueDate tu cac bang*/
	DECLARE @tbDataDueDate1 TABLE(
		ID UNIQUEIDENTIFIER,
		IDDetail UNIQUEIDENTIFIER,
		DueDate DATETIME
	)
	/*end add by cuongpv*/
	/*add by cuongpv lay du lieu DueDate tu cac bang SAInvoice, SAReturn*/
	INSERT INTO @tbDataDueDate1
	SELECT SAiv.ID, SAivdt.ID, SAiv.DueDate FROM SAInvoiceDetail SAivdt LEFT JOIN SAInvoice SAiv ON SAiv.ID = SAivdt.SAInvoiceID
	WHERE SAivdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)
	
	UNION ALL

	SELECT SArt.ID, SArtdt.ID, SArt.DueDate FROM SAReturnDetail SArtdt LEFT JOIN SAReturn SArt ON SArt.ID = SArtdt.SAReturnID
	WHERE SArtdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)
	/*end add by cuongpv*/
	insert into @tblResult
		SELECT  a.AccountNumber,
			A.AccountGroupKind,
			LAOI.AccountObjectID,
			LAOI.AccountObjectCode,
			null as NgayThangGS,
			null as Sohieu, 
			null as ngayCTu,
			N'Số dư đầu kỳ' as Diengiai ,
			null as TKDoiUng, 
			null as ThoiHanDuocCKhau, 
			$0 AS PSNO ,
			$0 AS PSCO ,
			CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
					THEN SUM(GL.DebitAmount - GL.CreditAmount)
					ELSE 0
			END AS DuNo ,
			CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
					THEN SUM(GL.CreditAmount - GL.DebitAmount)
					ELSE 0
			END AS DuCo,
			0 AS OrderType,
			OrderPriority
		FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
		INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber 
		INNER JOIN @tblListAccountObjectID AS LAOI ON GL.AccountingObjectID = LAOI.AccountObjectID
		WHERE   ( GL.PostedDate < @FromDate )  
		and GL.CurrencyID = @currency                    
		GROUP BY 
			A.AccountNumber,LAOI.AccountObjectID,A.AccountGroupKind,LAOI.AccountObjectCode,OrderPriority
		HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
	UNION ALL
		select  an.AccountNumber,
				An.AccountGroupKind,
				LAOI.AccountObjectID,
				LAOI.AccountObjectCode,
				a.PostedDate as NgayThangGS, 
				a.No as Sohieu, 
				a.Date as ngayCTu,
				a.Description as Diengiai, 
				a.AccountCorresponding as TKDoiUng, 
				DueDate as ThoiHanDuocCKhau, 
				sum(a.DebitAmount) as PSNO, 
				sum(a.CreditAmount) as PSCO,
				sum(fnsd.DebitAmount) DuNo,
				sum(fnsd.CreditAmount) DuCo,
				1 AS OrderType,
				OrderPriority
		 from @tbDataGL a	/*edit by cuongpv GeneralLedger -> @tbDataGL*/
		LEFT JOIN @tbDataDueDate1 tbDuedate ON tbDuedate.IDDetail = a.DetailID
		inner join @tblAccountNumber as an on a.Account = an.AccountNumber
		INNER JOIN @tblListAccountObjectID AS LAOI ON a.AccountingObjectID = LAOI.AccountObjectID
		INNER JOIN Func_SoDu (
		   @fromdate
		  ,@todate
		  ,1) AS fnsd ON LAOI.AccountObjectID = fnsd.AccountObjectID	and an.AccountNumber = fnsd.AccountNumber			
		where a.PostedDate between @fromdate and @todate
		and  a.CurrencyID = @currency  
		group by an.AccountNumber,
				LAOI.AccountObjectID,
				a.PostedDate,
				a.No , 
				a.Date ,
				a.Description, 
				a.AccountCorresponding, 
				DueDate,An.AccountGroupKind,LAOI.AccountObjectCode,OrderPriority
		order by AccountNumber,AccountObjectID,NgayThangGS,OrderPriority
End
DECLARE @CloseAmount AS Money ,
        @AccountObjectCode_tmp NVARCHAR(100) ,
        @AccountNumber_tmp NVARCHAR(20)
SELECT  @CloseAmount = 0 ,
        @AccountObjectCode_tmp = N'' ,
        @AccountNumber_tmp = N''
UPDATE  @tblResult
        SET     
                 @CloseAmount = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmount = 0 THEN ClosingCreditAmount
                                                                     ELSE -1 * ClosingDebitAmount
                                                                END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode or @AccountNumber_tmp <> AccountNumber THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountGroupKind = 0 THEN -1 * @CloseAmount
                                            WHEN AccountGroupKind = 1 THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0 THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountGroupKind = 1 THEN @CloseAmount
                                             WHEN AccountGroupKind = 0 THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0 THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
				@AccountNumber_tmp = AccountNumber							
select AccountNumber,
			AccountObjectID,
			RefDate as NgayThangGS, 
			RefNo as Sohieu, 
			PostedDate as ngayCTu,
			JournalMemo as Diengiai, 
			CorrespondingAccountNumber as TKDoiUng, 
			DueDate as ThoiHanDuocCKhau, 
			DebitAmount as PSNO, 
			CreditAmount as PSCO,
			ClosingDebitAmount as DuNo,
			ClosingCreditAmount as DuCo
from @tblResult 
order by RowNum,OrderPriority
end				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_PO_SummaryPayable]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS
    BEGIN
	    
		/*Add by cuongpv de sua cach lam tron*/
	DECLARE @tbDataGL TABLE(
		ID uniqueidentifier,
		BranchID uniqueidentifier,
		ReferenceID uniqueidentifier,
		TypeID int,
		Date datetime,
		PostedDate datetime,
		No nvarchar(25),
		InvoiceDate datetime,
		InvoiceNo nvarchar(25),
		Account nvarchar(25),
		AccountCorresponding nvarchar(25),
		BankAccountDetailID uniqueidentifier,
		CurrencyID nvarchar(3),
		ExchangeRate decimal(25, 10),
		DebitAmount decimal(25,0),
		DebitAmountOriginal decimal(25,2),
		CreditAmount decimal(25,0),
		CreditAmountOriginal decimal(25,2),
		Reason nvarchar(512),
		Description nvarchar(512),
		VATDescription nvarchar(512),
		AccountingObjectID uniqueidentifier,
		EmployeeID uniqueidentifier,
		BudgetItemID uniqueidentifier,
		CostSetID uniqueidentifier,
		ContractID uniqueidentifier,
		StatisticsCodeID uniqueidentifier,
		InvoiceSeries nvarchar(25),
		ContactName nvarchar(512),
		DetailID uniqueidentifier,
		RefNo nvarchar(25),
		RefDate datetime,
		DepartmentID uniqueidentifier,
		ExpenseItemID uniqueidentifier,
		OrderPriority int,
		IsIrrationalCost bit
	)

	INSERT INTO @tbDataGL
	SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
	/*end add by cuongpv*/
		
		DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(512) ,
              AccountObjectAddress NVARCHAR(512) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        null AccountingObjectGroupCode ,
                        null AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
      
                 
		
		DECLARE @StartDateOfPeriod DATETIME
		SET @StartDateOfPeriod = '1/1/'+CAST(Year(@FromDate) AS NVARCHAR(5))
                         
           
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(512) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
	    IF(@CurrencyID = 'VND')
		BEGIN
		 SELECT  ROW_NUMBER() OVER ( ORDER BY AccountObjectCode ) AS RowNum ,
                AccountingObjectID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountObjectAddress ,
                AccountObjectTaxCode ,
                AccountNumber ,
                AccountCategoryKind ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC)
                            - SUM(OpenningCreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) ) > 0
                                 THEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN ( SUM(OpenningDebitAmount - OpenningCreditAmount) )
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmount
                                            - OpenningCreditAmount) ) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount)
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmountOC
                                  - OpenningDebitAmountOC) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) ) > 0
                                 THEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmount - OpenningDebitAmount) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) ) > 0
                                 THEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmount ,
                SUM(DebitAmountOC) AS DebitAmountOC ,
                SUM(DebitAmount) AS DebitAmount ,
                SUM(CreditAmountOC) AS CreditAmountOC ,
                SUM(CreditAmount) AS CreditAmount ,
                SUM(AccumDebitAmountOC) AS AccumDebitAmountOC ,
                SUM(AccumDebitAmount) AS AccumDebitAmount ,
                SUM(AccumCreditAmountOC) AS AccumCreditAmountOC ,
                SUM(AccumCreditAmount) AS AccumCreditAmount ,
                /* Số dư cuối kỳ = Dư Có đầu kỳ - Dư Nợ đầu kỳ + Phát sinh Có – Phát sinh Nợ
				Nếu Số dư cuối kỳ >0 thì hiển bên cột Dư Có cuối kỳ 
				Nếu số dư cuối kỳ <0 thì hiển thị bên cột Dư Nợ cuối kỳ */
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC
                                + DebitAmountOC - CreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC) > 0
                                 THEN $0
                                 ELSE SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC)
                            END
                  END ) AS CloseDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC
                                + CreditAmountOC - DebitAmountOC)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC
                                            + CreditAmountOC - DebitAmountOC) ) > 0
                                 THEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                                + DebitAmount - CreditAmount)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount) > 0 THEN $0
                                 ELSE SUM(OpenningDebitAmount
                                          - OpenningCreditAmount + DebitAmount
                                          - CreditAmount)
                            END
                  END ) AS ClosingDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                                + CreditAmount - DebitAmount)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount
                                            + CreditAmount - DebitAmount) ) > 0
                                 THEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)
                                 ELSE $0
                            END
                  END ) AS ClosingCreditAmount ,
                AccountObjectGroupListCode ,
                AccountObjectGroupListName
        FROM    ( SELECT    AOL.AccountingObjectID ,
                            LAOI.AccountObjectCode ,
                            LAOI.AccountObjectName ,
                            LAOI.AccountObjectAddress,
                            LAOI.AccountObjectTaxCode ,
                            TBAN.AccountNumber ,
                            TBAN.AccountCategoryKind ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmount
                                 ELSE $0
                            END AS OpenningDebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmount
                                 ELSE $0
                            END AS OpenningCreditAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmountOriginal
                            END AS DebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmount
                            END AS DebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmountOriginal
                            END AS CreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmount
                            END AS CreditAmount ,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmountOriginal ELSE 0 END AS AccumDebitAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmount ELSE 0 END AS AccumDebitAmount,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmountOriginal ELSE 0 END AS AccumCreditAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmount ELSE 0 END AS AccumCreditAmount,
                            LAOI.AccountObjectGroupListCode ,
                            LAOI.AccountObjectGroupListName
                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                  WHERE     AOL.PostedDate <= @ToDate
                           
                ) AS RSNS
        GROUP BY RSNS.AccountingObjectID ,
                RSNS.AccountObjectCode ,
                RSNS.AccountObjectName ,
                RSNS.AccountObjectAddress ,
                RSNS.AccountObjectTaxCode ,
                RSNS.AccountNumber ,
                RSNS.AccountCategoryKind ,
                RSNS.AccountObjectGroupListCode ,
                RSNS.AccountObjectGroupListName 
        HAVING 
				SUM(DebitAmountOC) <> 0
                OR SUM(DebitAmount) <> 0
                OR SUM(CreditAmountOC) <> 0
                OR SUM(CreditAmount) <> 0
                OR SUM(OpenningDebitAmount - OpenningCreditAmount) <> 0 
                OR SUM(OpenningDebitAmountOC - OpenningCreditAmountOC) <> 0 
				OR ( 
					(SUM(AccumDebitAmountOC) <> 0
					OR SUM(AccumDebitAmount) <> 0
					OR SUM(AccumCreditAmountOC) <> 0
					OR SUM(AccumCreditAmount) <> 0) )
        ORDER BY RSNS.AccountObjectCode
        OPTION (RECOMPILE)
		END
		ELSE
		BEGIN
        SELECT  ROW_NUMBER() OVER ( ORDER BY AccountObjectCode ) AS RowNum ,
                AccountingObjectID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountObjectAddress ,
                AccountObjectTaxCode ,
                AccountNumber ,
                AccountCategoryKind ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC)
                            - SUM(OpenningCreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) ) > 0
                                 THEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN ( SUM(OpenningDebitAmount - OpenningCreditAmount) )
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmount
                                            - OpenningCreditAmount) ) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount)
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmountOC
                                  - OpenningDebitAmountOC) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) ) > 0
                                 THEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmount - OpenningDebitAmount) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) ) > 0
                                 THEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmount ,
                SUM(DebitAmountOC) AS DebitAmountOC ,
                SUM(DebitAmount) AS DebitAmount ,
                SUM(CreditAmountOC) AS CreditAmountOC ,
                SUM(CreditAmount) AS CreditAmount ,
                SUM(AccumDebitAmountOC) AS AccumDebitAmountOC ,
                SUM(AccumDebitAmount) AS AccumDebitAmount ,
                SUM(AccumCreditAmountOC) AS AccumCreditAmountOC ,
                SUM(AccumCreditAmount) AS AccumCreditAmount ,
                /* Số dư cuối kỳ = Dư Có đầu kỳ - Dư Nợ đầu kỳ + Phát sinh Có – Phát sinh Nợ
				Nếu Số dư cuối kỳ >0 thì hiển bên cột Dư Có cuối kỳ 
				Nếu số dư cuối kỳ <0 thì hiển thị bên cột Dư Nợ cuối kỳ */
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC
                                + DebitAmountOC - CreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC) > 0
                                 THEN $0
                                 ELSE SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC)
                            END
                  END ) AS CloseDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC
                                + CreditAmountOC - DebitAmountOC)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC
                                            + CreditAmountOC - DebitAmountOC) ) > 0
                                 THEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                                + DebitAmount - CreditAmount)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount) > 0 THEN $0
                                 ELSE SUM(OpenningDebitAmount
                                          - OpenningCreditAmount + DebitAmount
                                          - CreditAmount)
                            END
                  END ) AS ClosingDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                                + CreditAmount - DebitAmount)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount
                                            + CreditAmount - DebitAmount) ) > 0
                                 THEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)
                                 ELSE $0
                            END
                  END ) AS ClosingCreditAmount ,
                AccountObjectGroupListCode ,
                AccountObjectGroupListName
        FROM    ( SELECT    AOL.AccountingObjectID ,
                            LAOI.AccountObjectCode ,
                            LAOI.AccountObjectName ,
                            LAOI.AccountObjectAddress,
                            LAOI.AccountObjectTaxCode ,
                            TBAN.AccountNumber ,
                            TBAN.AccountCategoryKind ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmountOriginal
                            END AS DebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmountOriginal
                            END AS DebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmountOriginal
                            END AS CreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmountOriginal
                            END AS CreditAmount ,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmountOriginal ELSE 0 END AS AccumDebitAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmount ELSE 0 END AS AccumDebitAmount,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmountOriginal ELSE 0 END AS AccumCreditAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmount ELSE 0 END AS AccumCreditAmount,
                            LAOI.AccountObjectGroupListCode ,
                            LAOI.AccountObjectGroupListName
                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                  WHERE     AOL.PostedDate <= @ToDate
                            AND ( @CurrencyID IS NULL
                                  OR AOL.CurrencyID = @CurrencyID
                                )
                ) AS RSNS
        GROUP BY RSNS.AccountingObjectID ,
                RSNS.AccountObjectCode ,
                RSNS.AccountObjectName ,
                RSNS.AccountObjectAddress ,
                RSNS.AccountObjectTaxCode ,
                RSNS.AccountNumber ,
                RSNS.AccountCategoryKind ,
                RSNS.AccountObjectGroupListCode ,
                RSNS.AccountObjectGroupListName 
        HAVING 
				SUM(DebitAmountOC) <> 0
                OR SUM(DebitAmount) <> 0
                OR SUM(CreditAmountOC) <> 0
                OR SUM(CreditAmount) <> 0
                OR SUM(OpenningDebitAmount - OpenningCreditAmount) <> 0 
                OR SUM(OpenningDebitAmountOC - OpenningCreditAmountOC) <> 0 
				OR ( 
					(SUM(AccumDebitAmountOC) <> 0
					OR SUM(AccumDebitAmount) <> 0
					OR SUM(AccumCreditAmountOC) <> 0
					OR SUM(AccumCreditAmount) <> 0) )
        ORDER BY RSNS.AccountObjectCode
        OPTION (RECOMPILE)
		END
		
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*
*/
ALTER PROCEDURE [dbo].[Proc_PO_PayDetailNCC]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS 
    BEGIN     
        DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(512) ,
              AccountObjectAddress NVARCHAR(512) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        null AccountingObjectGroupCode ,
                        null AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
		
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/
		      
        CREATE TABLE #tblResult
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100)  ,
              AccountObjectName NVARCHAR(512)  ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL ,             
              AccountObjectAddress NVARCHAR(512)  ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25)  ,
              DueDate DATETIME , 
              InvDate DATETIME ,
              InvNo NVARCHAR(25)  ,

              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              JournalMemo NVARCHAR(512)  , 
              AccountNumber NVARCHAR(20)  , 
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20)  , 
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,	
              DebitAmount MONEY , 
              CreditAmountOC MONEY , 
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY ,
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,	
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50)  ,
              InventoryItemName NVARCHAR(512)  ,
              UnitName NVARCHAR(20)  ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(18, 4) , 
              BranchName NVARCHAR(128)  ,
              IsBold BIT ,	
              OrderType INT ,
              GLJournalMemo NVARCHAR(512)  ,
              DocumentIncluded NVARCHAR(512)  ,
              CustomField1 NVARCHAR(512)  ,
              CustomField2 NVARCHAR(512)  ,
              CustomField3 NVARCHAR(512)  ,
              CustomField4 NVARCHAR(512)  ,
              CustomField5 NVARCHAR(512)  ,
              CustomField6 NVARCHAR(512)  ,
              CustomField7 NVARCHAR(512)  ,
              CustomField8 NVARCHAR(512)  ,
              CustomField9 NVARCHAR(512)  ,
              CustomField10 NVARCHAR(512) ,
               MainUnitName NVARCHAR(20)  ,
              MainQuantity DECIMAL(22, 8) ,
              MainUnitPrice DECIMAL(18, 4),
			  OrderPriority int  
            )
        
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(512) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
			
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
            
                
/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
IF(@CurrencyID='VND')
BEGIN
INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode , 
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,      
                          AccountObjectAddress , 
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID , 
                          JournalMemo , 				  
                          AccountNumber , 
                          AccountCategoryKind ,
                          CorrespondingAccountNumber ,	  			  
                          DebitAmountOC ,	
                          DebitAmount , 
                          CreditAmountOC ,
                          CreditAmount ,
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount ,
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount,
						  OrderType,
						  /*OrderPriority - comment by cuongpv*/ 
						  OrderPriority
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,
                                AccountObjectName ,	
                                AccountObjectGroupListCode , 
                                AccountObjectGroupListName ,            
                                AccountObjectAddress , 
                                RSNS.PostedDate ,
                                NgayCtu ,
                                SoCtu ,
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RSNS.RefID , 
                                JournalMemo ,			  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  			  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) , 
                                SUM(CreditAmount) , 
                                ( CASE WHEN AccountCategoryKind = 0 THEN SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) ) > 0 THEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,      
                                ( CASE WHEN AccountCategoryKind = 0 THEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) ) > 0 THEN SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) ) > 0 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) ) > 0 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount,  
								  OrderType,
								  /*OrderPriority - comment by cuongpv*/
								  OrderPriority
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,
                                            AccountObjectGroupListCode ,
                                            AccountObjectGroupListName ,         
                                            LAOI.AccountObjectAddress ,
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.Date
                                            END AS NgayCtu ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.No
                                            END AS SoCtu ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType , 
                                            CASE WHEN AOL.PostedDate < @FromDate 
												      THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN N'Số dư đầu kỳ'
                                                 ELSE AOL.Description
                                            END AS JournalMemo , 
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                             
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                  
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount,
											CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType,
											/*OrderPriority - comment by cuongpv*/
											OrderPriority
                                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                                         
                                  WHERE     AOL.PostedDate <= @ToDate
                                            
                                ) AS RSNS                               
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode ,  
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,
                                RSNS.AccountObjectAddress ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu , 
                                RSNS.SoCtu , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,
                                RSNS.JournalMemo , 		  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber,
								OrderType,
								/*OrderPriority - comment by cuongpv*/	
								OrderPriority
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount - ClosingCreditAmount) <> 0
                        ORDER BY 
						        RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu ,
                                RSNS.SoCtu,
								OrderPriority
								/*OrderPriority - comment by cuongpv*/

                OPTION  ( RECOMPILE ) 
END
ELSE
BEGIN
INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode , 
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,      
                          AccountObjectAddress , 
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID , 
                          JournalMemo , 				  
                          AccountNumber , 
                          AccountCategoryKind ,
                          CorrespondingAccountNumber ,	  			  
                          DebitAmountOC ,	
                          DebitAmount , 
                          CreditAmountOC ,
                          CreditAmount ,
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount ,
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount,
						  OrderType,
						  /*OrderPriority - comment by cuongpv*/ 
						  OrderPriority
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,
                                AccountObjectName ,	
                                AccountObjectGroupListCode , 
                                AccountObjectGroupListName ,            
                                AccountObjectAddress , 
                                RSNS.PostedDate ,
                                NgayCtu ,
                                SoCtu ,
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RSNS.RefID , 
                                JournalMemo ,			  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  			  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) , 
                                SUM(CreditAmount) , 
                                ( CASE WHEN AccountCategoryKind = 0 THEN SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) ) > 0 THEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,      
                                ( CASE WHEN AccountCategoryKind = 0 THEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) ) > 0 THEN SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) ) > 0 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) ) > 0 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount,  
								  OrderType,
								  /*OrderPriority - comment by cuongpv*/
								  OrderPriority
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,
                                            AccountObjectGroupListCode ,
                                            AccountObjectGroupListName ,         
                                            LAOI.AccountObjectAddress ,
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.Date
                                            END AS NgayCtu ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.No
                                            END AS SoCtu ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType , 
                                            CASE WHEN AOL.PostedDate < @FromDate 
												      THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN N'Số dư đầu kỳ'
                                                 ELSE AOL.Description
                                            END AS JournalMemo , 
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                             
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                  
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount,
											CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType,
											/*OrderPriority - comment by cuongpv*/
											OrderPriority
                                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                                         
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )
											AND (AOL.DebitAmountOriginal <>0 OR AOL.CreditAmountOriginal <>0)
                                ) AS RSNS                               
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode ,  
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,
                                RSNS.AccountObjectAddress ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu , 
                                RSNS.SoCtu , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,
                                RSNS.JournalMemo , 		  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber,
								OrderType,
								/*OrderPriority - comment by cuongpv*/	
								OrderPriority
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount - ClosingCreditAmount) <> 0
                        ORDER BY 
						        RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu ,
                                RSNS.SoCtu,
								OrderPriority
								/*OrderPriority - comment by cuongpv*/

                OPTION  ( RECOMPILE )  
				END
/* Tính số tồn */ 
/* edit by namnh
 @CloseAmountOC,@CloseAmount Decimal(22,8) -> Money)*/
        DECLARE @CloseAmountOC AS Money ,
            @CloseAmount AS Money ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmountOC = 0 THEN ClosingCreditAmountOC
                                                                       ELSE -1 * ClosingDebitAmountOC
                                                                  END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1 THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0 THEN -1 * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0 THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0 THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmount = 0 THEN ClosingCreditAmount
                                                                     ELSE -1 * ClosingDebitAmount
                                                                END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1 THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0 THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0 THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0 THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
        SELECT  RS.PostedDate as Ngay_HT, 
		        RS.RefDate as ngayCtu,
				RS.RefNo as SoCtu,
				Rs.JournalMemo as DienGiai,
				/*'331' as TK_CONGNO, comment by cuongpv*/
				Rs.CorrespondingAccountNumber as TkDoiUng,
                RS.AccountObjectCode,
				RS.AccountObjectName,
				RS.AccountNumber as TK_CONGNO, /*edit by cuongpv RS.AccountNumber -> RS.AccountNumber as TK_CONGNO*/
				fn.OpeningDebitAmount as OpeningDebitAmount,
				fn.OpeningCreditAmount as OpeningCreditAmount,
				Rs.DebitAmount as DebitAmount,
				Rs.DebitAmountOC as DebitAmountOC,
				Rs.CreditAmount as CreditAmount,
				Rs.CreditAmountOC as CreditAmountOC,
				Rs.ClosingDebitAmount as ClosingDebitAmount,
				Rs.ClosingDebitAmountOC as ClosingDebitAmountOC,
				Rs.ClosingCreditAmount as ClosingCreditAmount,
				Rs.ClosingCreditAmountOC as ClosingCreditAmountOC
        FROM    #tblResult RS ,
		        [dbo].[Func_SoDu] (
   @FromDate
  ,@ToDate
  ,3) fn
  where rs.AccountNumber = fn.AccountNumber
  and rs.AccountObjectID = fn.AccountObjectID
  
  order by RS.AccountObjectCode ,
                                RS.AccountNumber ,
                                RS.PostedDate ,
                                RS.RefDate ,
                                RS.RefNo,
								OrderPriority
        DROP TABLE #tblResult
    END				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*

*/
ALTER PROCEDURE [dbo].[Proc_PO_PayableDetail]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS 
    BEGIN     
        DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              AccountObjectAddress NVARCHAR(512) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        null AccountingObjectGroupCode ,
                        null AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
      
        CREATE TABLE #tblResult
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100)  ,
              AccountObjectName NVARCHAR(512)  ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL ,             
              AccountObjectAddress NVARCHAR(512)  ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25)  ,
              DueDate DATETIME , 
              InvDate DATETIME ,
              InvNo NVARCHAR(25)  ,

              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              JournalMemo NVARCHAR(512)  , 
              AccountNumber NVARCHAR(20)  , 
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20)  , 
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,	
              DebitAmount MONEY , 
              CreditAmountOC MONEY , 
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY ,
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,	
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50)  ,
              InventoryItemName NVARCHAR(512)  ,
              UnitName NVARCHAR(20)  ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(18, 4) , 
              BranchName NVARCHAR(512)  ,
              IsBold BIT ,	
              OrderType INT ,
              GLJournalMemo NVARCHAR(512)  ,
              DocumentIncluded NVARCHAR(512)  ,
              CustomField1 NVARCHAR(512)  ,
              CustomField2 NVARCHAR(512)  ,
              CustomField3 NVARCHAR(512)  ,
              CustomField4 NVARCHAR(512)  ,
              CustomField5 NVARCHAR(512)  ,
              CustomField6 NVARCHAR(512)  ,
              CustomField7 NVARCHAR(512)  ,
              CustomField8 NVARCHAR(512)  ,
              CustomField9 NVARCHAR(512)  ,
              CustomField10 NVARCHAR(512) ,
               MainUnitName NVARCHAR(20)  ,
              MainQuantity DECIMAL(22, 8) ,
              MainUnitPrice DECIMAL(18, 4)  
              
            )
        
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(512) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
            
                
/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode , 
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,      
                          AccountObjectAddress , 
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID , 
                          JournalMemo , 				  
                          AccountNumber , 
                          AccountCategoryKind ,
                          CorrespondingAccountNumber ,	  			  
                          DebitAmountOC ,	
                          DebitAmount , 
                          CreditAmountOC ,
                          CreditAmount ,
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount ,
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount,
						  OrderType   
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,
                                AccountObjectName ,	
                                AccountObjectGroupListCode , 
                                AccountObjectGroupListName ,            
                                AccountObjectAddress , 
                                RSNS.PostedDate ,
                                RefDate ,
                                RefNo ,
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RSNS.RefID , 
                                JournalMemo ,			  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  			  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) , 
                                SUM(CreditAmount) , 
                                ( CASE WHEN AccountCategoryKind = 0 THEN SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) ) > 0 THEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,      
                                ( CASE WHEN AccountCategoryKind = 0 THEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) ) > 0 THEN SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) ) > 0 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) ) > 0 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount,  
								  OrderType
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,
                                            AccountObjectGroupListCode ,
                                            AccountObjectGroupListName ,         
                                            LAOI.AccountObjectAddress ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,                      
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE RefDate
                                            END AS RefDate , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.RefNo
                                            END AS RefNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType , 
                                            CASE WHEN AOL.PostedDate < @FromDate 
												      THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN N'Số dư đầu kỳ'
                                                 ELSE AOL.Description
                                            END AS JournalMemo , 
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                             
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                  
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount,
											CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType 
                                  FROM      dbo.GeneralLedger AS AOL
                                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                                         
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )
                                ) AS RSNS                               
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode ,  
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,
                                RSNS.AccountObjectAddress ,
                                RSNS.PostedDate ,
                                RSNS.RefDate , 
                                RSNS.RefNo , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,
                                RSNS.JournalMemo , 		  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber,
								OrderType 		   
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount - ClosingCreditAmount) <> 0
                        ORDER BY RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.PostedDate ,
                                RSNS.RefDate ,
                                RSNS.RefNo
                OPTION  ( RECOMPILE )    
                        
/* Tính số tồn */
        DECLARE @CloseAmountOC AS DECIMAL(22, 8) ,
            @CloseAmount AS DECIMAL(22, 8) ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmountOC = 0 THEN ClosingCreditAmountOC
                                                                       ELSE -1 * ClosingDebitAmountOC
                                                                  END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1 THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0 THEN -1 * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0 THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0 THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmount = 0 THEN ClosingCreditAmount
                                                                     ELSE -1 * ClosingDebitAmount
                                                                END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1 THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0 THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0 THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0 THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
        SELECT  
                RS.AccountObjectCode,
				RS.AccountObjectName,
				RS.AccountNumber,
				sum(fn.OpeningDebitAmount) OpeningDebitAmount,
				sum(fn.OpeningCreditAmount) OpeningCreditAmount,
				sum(Rs.DebitAmount) DebitAmount,
				sum(Rs.CreditAmount) CreditAmount,
				sum(Rs.ClosingDebitAmount) ClosingDebitAmount,
				sum(Rs.ClosingCreditAmount) ClosingCreditAmount
        FROM    #tblResult RS ,
		        [dbo].[Func_SoDu] (
   @FromDate
  ,@ToDate
  ,3) fn
  where rs.AccountNumber = fn.AccountNumber
  and rs.AccountObjectID = fn.AccountObjectID
  group by RS.AccountObjectCode,
				RS.AccountObjectName,
				RS.AccountNumber
        DROP TABLE #tblResult
    END				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER procedure [dbo].[Proc_PO_GetDetailBook]
@FromDate DATETIME ,
    @ToDate DATETIME,
    @AccountObjectID AS NVARCHAR(MAX),
	@MaterialGoods as NVARCHAR(MAX)
as
begin
    CREATE TABLE #tblListAccountObjectID  
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25)
                 ,
              AccountObjectName NVARCHAR(512)
                 
            ) 
    CREATE TABLE #tblListMaterialGoods  
            (
              MaterialGoodsID UNIQUEIDENTIFIER ,
              MaterialGoodsCode NVARCHAR(25)
                 ,
              MaterialGoodsName NVARCHAR(512)
                 ,
			  Unit NVARCHAR(25)  ,
			 Quantity decimal,
			 UnitPrice Money 
            ) 
        INSERT  INTO #tblListAccountObjectID
                SELECT  AO.ID ,
                        ao.AccountingObjectCode ,
                        ao.AccountingObjectName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                           ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value 
        INSERT  INTO #tblListMaterialGoods
                SELECT  MG.ID ,
                        MG.MaterialGoodsCode ,
                        MG.MaterialGoodsName,
						MG.Unit,
						MG.Quantity,
						MG.UnitPrice 
                FROM    dbo.Func_ConvertStringIntoTable(@MaterialGoods,
                                                           ',') tblAccountObjectSelected
                        INNER JOIN dbo.MaterialGoods MG ON MG.ID = tblAccountObjectSelected.Value 
	/*select * from #tblListMaterialGoods
	select * from #tblListAccountObjectID*/
	/*Tao bang tam luu du lieu ra bao cao*/
	DECLARE @tbDataResult TABLE(
		AccountObjectID UNIQUEIDENTIFIER,
		MaKH NVARCHAR(25),
		TenKH NVARCHAR(512),
		NgayHachToan DATE,
		NgayCTu DATE,
		SoCTu NVARCHAR(25),
		SoHoaDon NVARCHAR(25),
		NgayHoaDon DATE,
		MaterialGoodsID UNIQUEIDENTIFIER ,
        Mahang NVARCHAR(25),
		Tenhang NVARCHAR(512),
		DVT NVARCHAR(25),
		SoLuongMua DECIMAL(25,10),
		DonGia MONEY,
		GiaTriMua DECIMAL(25,0),
		ChietKhau DECIMAL(25,0),
		SoLuongTraLai DECIMAL(25,10),
		GiaTriTraLai DECIMAL(25,0),
		GiaTriGiamGia DECIMAL(25,0),
		TypeMG  NVARCHAR(1)
	)

	IF((@AccountObjectID is not null) AND (@MaterialGoods is not null))
	BEGIN
		/*lay gia tri PPInvoice vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, SoLuongMua, DonGia, GiaTriMua, ChietKhau, TypeMG)
		SELECT a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, b.InvoiceNo as SoHoaDon, b.InvoiceDate as NgayHoaDon, 
			   b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.Quantity as SoLuongMua, b.UnitPrice as DonGia, b.Amount as GiaTriMua, b.DiscountAmount as ChietKhau, 'I' as TypeMG   		 
		   FROM PPInvoiceDetail b LEFT JOIN PPInvoice a ON a.id = b.PPInvoiceID 
		   WHERE a.PostedDate between @FromDate and @ToDate
		   and a.Recorded = '1'
		   and ((a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID)) and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods)))


		/*Lay gia tri PPService vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, SoLuongMua, DonGia, GiaTriMua, ChietKhau, TypeMG)
		   SELECT  a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, b.InvoiceNo as SoHoaDon, b.InvoiceDate as NgayHoaDon, 
				   b.MaterialGoodsID as MaterialGoodsID, b.Quantity as SoLuongMua, b.UnitPrice as DonGia, b.Amount as GiaTriMua, b.DiscountAmount as ChietKhau, 'S' as TypeMG  		
		   FROM PPServiceDetail b LEFT JOIN PPService a ON a.ID = b.PPServiceID
		   WHERE a.PostedDate between @FromDate and @ToDate
		   and a.Recorded = '1' 
		   and ((a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID)) and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods)))

		/*Update DVT cua cac gia tri lay tu PPService*/
		UPDATE @tbDataResult SET DVT = MS.DVT
		FROM (
			SELECT ID as MSID, Unit as DVT FROM dbo.MaterialGoods
		) MS
		WHERE MaterialGoodsID = MS.MSID AND TypeMG = 'S'


		/*Lay gia tri Tra lai vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, DonGia, SoLuongTraLai, GiaTriTraLai, TypeMG)
		SELECT  a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, a.InvoiceNo as SoHoaDon, a.InvoiceDate as NgayHoaDon,
				  b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.UnitPrice as DonGia, b.Quantity as SoLuongTraLai, b.Amount as GiaTriTraLai, 'T' as TypeMG 		
			FROM PPDiscountReturnDetail b LEFT JOIN PPDiscountReturn a ON a.ID = b.PPDiscountReturnID
			WHERE a.PostedDate between @FromDate and @ToDate and a.Recorded = '1' and TypeID = '220'
			and ((a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID)) and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods)))



		/*lay gia tri giam gia vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, GiaTriGiamGia, TypeMG)
			select a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, InvoiceNo as SoHoaDon, InvoiceDate as NgayHoaDon, 
				   b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.Amount as GiaTriGiamGia, 'G' as TypeMG	   	
			from PPDiscountReturnDetail b LEFT JOIN PPDiscountReturn a ON a.ID = b.PPDiscountReturnID 
			where a.PostedDate between @FromDate and @ToDate and TypeID = '230'
			and ((a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID)) and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods)))
	END
	ELSE IF((@AccountObjectID is not null) AND (@MaterialGoods is null))
	BEGIN
		/*lay gia tri PPInvoice vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, SoLuongMua, DonGia, GiaTriMua, ChietKhau, TypeMG)
		SELECT a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, b.InvoiceNo as SoHoaDon, b.InvoiceDate as NgayHoaDon, 
			   b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.Quantity as SoLuongMua, b.UnitPrice as DonGia, b.Amount as GiaTriMua, b.DiscountAmount as ChietKhau, 'I' as TypeMG   		 
		   FROM PPInvoiceDetail b LEFT JOIN PPInvoice a ON a.id = b.PPInvoiceID 
		   WHERE a.PostedDate between @FromDate and @ToDate
		   and a.Recorded = '1'
		   and (a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID))


		/*Lay gia tri PPService vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, SoLuongMua, DonGia, GiaTriMua, ChietKhau, TypeMG)
		   SELECT  a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, b.InvoiceNo as SoHoaDon, b.InvoiceDate as NgayHoaDon, 
				   b.MaterialGoodsID as MaterialGoodsID, b.Quantity as SoLuongMua, b.UnitPrice as DonGia, b.Amount as GiaTriMua, b.DiscountAmount as ChietKhau, 'S' as TypeMG  		
		   FROM PPServiceDetail b LEFT JOIN PPService a ON a.ID = b.PPServiceID
		   WHERE a.PostedDate between @FromDate and @ToDate
		   and a.Recorded = '1' 
		   and (a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID))

		/*Update DVT cua cac gia tri lay tu PPService*/
		UPDATE @tbDataResult SET DVT = MS.DVT
		FROM (
			SELECT ID as MSID, Unit as DVT FROM dbo.MaterialGoods
		) MS
		WHERE MaterialGoodsID = MS.MSID AND TypeMG = 'S'


		/*Lay gia tri Tra lai vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, DonGia, SoLuongTraLai, GiaTriTraLai, TypeMG)
		SELECT  a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, a.InvoiceNo as SoHoaDon, a.InvoiceDate as NgayHoaDon,
				  b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.UnitPrice as DonGia, b.Quantity as SoLuongTraLai, b.Amount as GiaTriTraLai, 'T' as TypeMG 		
			FROM PPDiscountReturnDetail b LEFT JOIN PPDiscountReturn a ON a.ID = b.PPDiscountReturnID
			WHERE a.PostedDate between @FromDate and @ToDate and a.Recorded = '1' and TypeID = '220'
			and (a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID))



		/*lay gia tri giam gia vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, GiaTriGiamGia, TypeMG)
			select a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, InvoiceNo as SoHoaDon, InvoiceDate as NgayHoaDon, 
				   b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.Amount as GiaTriGiamGia, 'G' as TypeMG	   	
			from PPDiscountReturnDetail b LEFT JOIN PPDiscountReturn a ON a.ID = b.PPDiscountReturnID 
			where a.PostedDate between @FromDate and @ToDate and TypeID = '230'
			and (a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID))
	END
	ELSE
	BEGIN
		/*lay gia tri PPInvoice vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, SoLuongMua, DonGia, GiaTriMua, ChietKhau, TypeMG)
		SELECT a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, b.InvoiceNo as SoHoaDon, b.InvoiceDate as NgayHoaDon, 
			   b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.Quantity as SoLuongMua, b.UnitPrice as DonGia, b.Amount as GiaTriMua, b.DiscountAmount as ChietKhau, 'I' as TypeMG   		 
		   FROM PPInvoiceDetail b LEFT JOIN PPInvoice a ON a.id = b.PPInvoiceID 
		   WHERE a.PostedDate between @FromDate and @ToDate
		   and a.Recorded = '1'
		   and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods))


		/*Lay gia tri PPService vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, SoLuongMua, DonGia, GiaTriMua, ChietKhau, TypeMG)
		   SELECT  a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, b.InvoiceNo as SoHoaDon, b.InvoiceDate as NgayHoaDon, 
				   b.MaterialGoodsID as MaterialGoodsID, b.Quantity as SoLuongMua, b.UnitPrice as DonGia, b.Amount as GiaTriMua, b.DiscountAmount as ChietKhau, 'S' as TypeMG  		
		   FROM PPServiceDetail b LEFT JOIN PPService a ON a.ID = b.PPServiceID
		   WHERE a.PostedDate between @FromDate and @ToDate
		   and a.Recorded = '1' 
		   and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods))

		/*Update DVT cua cac gia tri lay tu PPService*/
		UPDATE @tbDataResult SET DVT = MS.DVT
		FROM (
			SELECT ID as MSID, Unit as DVT FROM dbo.MaterialGoods
		) MS
		WHERE MaterialGoodsID = MS.MSID AND TypeMG = 'S'


		/*Lay gia tri Tra lai vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, DonGia, SoLuongTraLai, GiaTriTraLai, TypeMG)
		SELECT  a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, a.InvoiceNo as SoHoaDon, a.InvoiceDate as NgayHoaDon,
				  b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.UnitPrice as DonGia, b.Quantity as SoLuongTraLai, b.Amount as GiaTriTraLai, 'T' as TypeMG 		
			FROM PPDiscountReturnDetail b LEFT JOIN PPDiscountReturn a ON a.ID = b.PPDiscountReturnID
			WHERE a.PostedDate between @FromDate and @ToDate and a.Recorded = '1' and TypeID = '220'
			and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods))



		/*lay gia tri giam gia vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, GiaTriGiamGia, TypeMG)
			select a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, InvoiceNo as SoHoaDon, InvoiceDate as NgayHoaDon, 
				   b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.Amount as GiaTriGiamGia, 'G' as TypeMG	   	
			from PPDiscountReturnDetail b LEFT JOIN PPDiscountReturn a ON a.ID = b.PPDiscountReturnID 
			where a.PostedDate between @FromDate and @ToDate and TypeID = '230'
			and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods))
	END

	/*Update makh, tenkh, mahang, ten hang vao @tbDataResult*/
	UPDATE @tbDataResult SET MaKH = AO.MaKH, TenKH = AO.TenKH
	FROM (
		SELECT ID as AOID, AccountingObjectCode as MaKH, AccountingObjectName as TenKH FROM dbo.AccountingObject
	) AO
	WHERE AccountObjectID = AO.AOID

	UPDATE @tbDataResult SET Mahang = M.Mahang, Tenhang = M.Tenhang
	FROM (
		SELECT ID as MID, MaterialGoodsCode as Mahang, MaterialGoodsName as Tenhang FROM dbo.MaterialGoods
	) M
	WHERE MaterialGoodsID = M.MID

	/*lay du lieu ra bao cao*/
	SELECT MaKH, TenKH, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, Mahang, Tenhang, DVT, SoLuongMua, DonGia, GiaTriMua, ChietKhau, SoLuongTraLai, GiaTriTraLai, GiaTriGiamGia
	FROM @tbDataResult
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_GetGLBookDetailMoneyBorrow]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountID NVARCHAR(MAX) ,
    @AccountObjectID NVARCHAR(MAX)
AS 
    BEGIN
	
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        CREATE  TABLE #Result
            (
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(512) ,
              AccountNumber NVARCHAR(25) ,
              AccountName NVARCHAR(512) ,
              CorrespondingAccountNumber NVARCHAR(25) ,
              DueDate DATETIME ,
              DebitAmount DECIMAL(22, 4) ,
              CreditAmount DECIMAL(22, 4) ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(512) ,
              OrderType INT ,
              IsBold BIT ,
              BranchName NVARCHAR(512) ,
              SortOrder INT ,
              DetailPostOrder INT
            )      

        DECLARE @tblAccountObject TABLE
            (
              AccountObjectID UNIQUEIDENTIFIER PRIMARY KEY,
			  AccountObjectCode NVARCHAR(25)
                 ,
              AccountObjectName NVARCHAR(512)
                
            )                       
        INSERT  INTO @tblAccountObject
                SELECT  f.value,A.AccountingObjectCode,A.AccountingObjectName
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                           ',') f
                 INNER JOIN dbo.AccountingObject A ON A.ID = F.Value 	
        CREATE TABLE #tblSelectedAccountNumber
            (
              AccountNumber NVARCHAR(20) 
                                         NOT NULL
                                         PRIMARY KEY ,
              AccountName NVARCHAR(255) 
                                        NULL
            )
        INSERT  INTO #tblSelectedAccountNumber
                SELECT DISTINCT  A.AccountNumber, A.AccountName
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountID, ',') F
                        INNER JOIN dbo.Account A ON A.AccountNumber like F.Value + '%'	/*edit by cuongpv (A.AccountNumber = F.Value) -> A.AccountNumber like F.Value + '%'*/		        
		/*select * from #tblSelectedAccountNumber*/
        BEGIN             
                
            INSERT  #Result
                    SELECT  RefID ,
                            PostedDate ,
                            RefDate ,
                            RefNo ,
                            RefType ,
                            JournalMemo ,
                            AccountNumber ,
                            AccountName ,
                            CorrespondingAccountNumber ,
                            DueDate ,
                            DebitAmount ,
                            CreditAmount ,
                            AccountObjectCode ,
                            AccountObjectName ,
                            OrderType ,
                            IsBold ,
                            BranchName ,
                            SortOrder ,
                            DetailPostOrder
                    FROM    ( SELECT    NULL AS RefID ,
                                        NULL AS PostedDate ,
                                        NULL AS RefDate ,
                                        NULL AS RefNo ,
                                        NULL AS RefType ,
                                        N'Số dư đầu kỳ' AS JournalMemo ,
                                        GL.Account AccountNumber,
                                        AC.AccountName ,
                                        NULL AS CorrespondingAccountNumber ,
                                        NULL DueDate ,
                                        CASE WHEN SUM(GL.DebitAmount
                                                      - GL.CreditAmount) > 0
                                             THEN SUM(GL.DebitAmount
                                                      - GL.CreditAmount)
                                             ELSE 0
                                        END AS DebitAmount ,
                                        CASE WHEN SUM(GL.CreditAmount
                                                      - GL.DebitAmount) > 0
                                             THEN SUM(GL.CreditAmount
                                                      - GL.DebitAmount)
                                             ELSE 0
                                        END AS CreditAmount ,
                                        AO.AccountObjectCode ,
                                        AO.AccountObjectName AS AccountObjectName ,
                                        0 AS OrderType ,
                                        1 AS isBold ,
                                        null  AS BranchName,
                                        0 AS sortOrder ,
                                        0 AS DetailPostOrder ,
                                        0 AS EntryType
                              FROM      @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                        inner JOIN #tblSelectedAccountNumber AC ON GL.Account = AC.AccountNumber /*edit by cuongpv like -> '='*/
                                                              /*+ '%' comment by cuongpv*/
                                        inner JOIN @tblAccountObject AO ON AO.AccountObjectID = GL.AccountingObjectID
                              WHERE     ( GL.PostedDate < @FromDate )
                              GROUP BY  GL.Account ,
                                        AC.AccountName ,
                                        AO.AccountObjectCode ,
                                        AO.AccountObjectName
                              HAVING    SUM(GL.DebitAmount - GL.CreditAmount) <> 0
                              UNION ALL
                              SELECT    GL.ReferenceID ,
                                        GL.PostedDate ,
                                        GL.RefDate ,
                                        GL.RefNo ,
                                        GL.TypeID ,
                                        CASE WHEN ( GL.Description IS NOT NULL
                                                    AND GL.Description <> ''
                                                  ) THEN GL.Description
                                             ELSE GL.Reason
                                        END AS JournalMemo ,
                                        GL.Account AccountNumber,
                                        AC.AccountName ,
                                        GL.AccountCorresponding,
                                        null as DueDate ,
                                        GL.DebitAmount ,
                                        GL.CreditAmount ,
                                        AO.AccountObjectCode ,
                                        AO.AccountObjectName AS AccountObjectName ,
                                        2 AS OrderType ,
                                        0 ,
                                        null AS BranchName,
                                        GL.OrderPriority,
                                        null,
                                        null
                              FROM      @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                        inner JOIN #tblSelectedAccountNumber AC ON GL.Account = AC.AccountNumber /*edit by cuongpv like -> '='*/
                                                              /*+ '%' comment by cuongpv*/
                                        inner JOIN @tblAccountObject AO ON AO.AccountObjectID = GL.AccountingObjectID
                              WHERE     ( GL.PostedDate BETWEEN @FromDate AND @ToDate )
                                        AND ( GL.DebitAmount <> 0
                                              OR GL.CreditAmount <> 0
                                            )
                            ) T
                    ORDER BY AccountObjectName ,
                            AccountObjectCode ,
                            OrderType ,
                            postedDate ,
                            RefDate ,
							SortOrder ,
                            RefNo ,
                            DetailPostOrder ,
                           
                            EntryType                                                          
               
        END         
       
	   /*add by cuongpv insert total PS*/
	   INSERT  #Result
                ( JournalMemo ,
                  AccountNumber ,
                  AccountName ,
                  DebitAmount ,
                  CreditAmount ,
                  AccountObjectCode ,
                  AccountObjectName ,
                  OrderType ,
                  IsBold ,
                  BranchName
		        )                                                
                SELECT  N'Cộng số phát sinh' JournalMemo ,
                        AccountNumber ,
                        AccountName ,
                        SUM(DebitAmount) AS DebitAmount ,
                        SUM(CreditAmount) AS CreditAmount ,
                        AccountObjectCode ,
                        AccountObjectName ,
                        3 AS OrderType ,
                        1 ,
                        null as BranchName
                FROM    #Result
                WHERE   OrderType = 2
                GROUP BY AccountNumber ,
                        AccountName ,
                        AccountObjectCode ,
                        AccountObjectName 
	   /*end add by cuongpv*/     
       
      /*Insert lable*/
        INSERT  #Result
                ( JournalMemo ,
                  AccountNumber ,
                  AccountName ,
                  DebitAmount ,
                  CreditAmount ,
                  AccountObjectCode ,
                  AccountObjectName ,
                  OrderType ,
                  IsBold ,
                  BranchName
		        )                                                
                SELECT  N'Số dư cuối kỳ' JournalMemo ,
                        AccountNumber ,
                        AccountName ,
                        CASE WHEN SUM(DebitAmount - CreditAmount) > 0
                             THEN SUM(DebitAmount - CreditAmount)
                             ELSE 0
                        END AS DebitAmount ,
                        CASE WHEN SUM(CreditAmount - DebitAmount) > 0
                             THEN SUM(CreditAmount - DebitAmount)
                             ELSE 0
                        END AS CreditAmount ,
                        AccountObjectCode ,
                        AccountObjectName ,
                        4 AS OrderType ,
                        1 ,
                        null as BranchName
                FROM    #Result
                WHERE   OrderType = 2
                        OR OrderType = 0
                GROUP BY AccountNumber ,
                        AccountName ,
                        AccountObjectCode ,
                        AccountObjectName
                HAVING  ( SUM(DebitAmount) <> 0
                          OR SUM(CreditAmount) <> 0
                        )                        
	     /*IF EXISTS ( SELECT  * FROM    #Result ) comment by cuongpv*/
			SELECT  * ,
                ROW_NUMBER() OVER ( ORDER BY AccountNumber,AccountObjectName,AccountObjectCode,  OrderType , PostedDate , RefNo , RefDate, SortOrder, DetailPostOrder ) AS RowNum
			FROM    #Result  
        
        /*ELSE 
           BEGIN
                INSERT  #Result
                        ( JournalMemo ,
                          OrderType ,
                          IsBold ,
                          BranchName
		                )                        
                        SELECT  N'' JournalMemo ,
                                2 ,
                                1 ,
                                ''                                                                     
                SELECT  * ,
                        ROW_NUMBER() OVER ( ORDER BY AccountObjectName, AccountObjectCode , OrderType , PostedDate , RefDate, RefNo, SortOrder, DetailPostOrder ) AS RowNum
                FROM    #Result         
            END  comment by cuongpv*/                      
        DELETE  #Result
        DELETE  #tblSelectedAccountNumber        
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.AutoPrinciple SET AutoPrincipleName = N'Chênh lệch lỗ tỷ giá xuất quỹ' WHERE ID = '799f8b26-9d0e-4353-acc3-0db8e886c6c0'

INSERT dbo.AutoPrinciple(ID, AutoPrincipleName, TypeID, DebitAccount, CreditAccount, Description, IsActive) VALUES ('000e6f4f-bcea-4032-b9f7-1cd76f7d3d95', N'Chênh lệch lãi tỷ giá xuất quỹ', 600, N'1112', N'515', N'', 1)
INSERT dbo.AutoPrinciple(ID, AutoPrincipleName, TypeID, DebitAccount, CreditAccount, Description, IsActive) VALUES ('f190ac3a-8ccb-4cc3-9d8d-21775cd7d2de', N'Tính thuế môn bài', 600, N'6422', N'33382', N'', 1)
INSERT dbo.AutoPrinciple(ID, AutoPrincipleName, TypeID, DebitAccount, CreditAccount, Description, IsActive) VALUES ('fbef58f4-c21c-4966-b0f3-43be53605402', N'Khấu trừ thuế GTGT mua vào của TSCĐ', 600, N'33311', N'1332', N'', 1)
INSERT dbo.AutoPrinciple(ID, AutoPrincipleName, TypeID, DebitAccount, CreditAccount, Description, IsActive) VALUES ('78e983e9-3d56-4ff6-9b7d-514cbcc274c8', N'Kết chuyển lãi đầu năm', 600, N'4212', N'4211', N'', 1)
INSERT dbo.AutoPrinciple(ID, AutoPrincipleName, TypeID, DebitAccount, CreditAccount, Description, IsActive) VALUES ('8a2350c4-31ff-4a6d-ba03-6b8ae5e4699d', N'Khấu trừ thuế GTGT mua vào của HHDV', 600, N'33311', N'1331', N'', 1)
INSERT dbo.AutoPrinciple(ID, AutoPrincipleName, TypeID, DebitAccount, CreditAccount, Description, IsActive) VALUES ('9e20f445-a258-43af-8f20-749c9cfbbe08', N'Kết chuyển lỗ đầu năm', 600, N'4211', N'4212', N'', 1)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.Sys_UserRole WHERE ID = 'efa6a6fa-5540-4168-946c-337d2e660197'

INSERT dbo.Sys_UserRole(ID, UserID, RoleID, BranchID) VALUES ('8da180c6-f48d-4154-b2c6-121e86842467', '5c2f6d39-e0c4-4d3e-8ab1-e00d51867ccc', 'b7e767cb-2731-434c-a513-61ed7497db6f', '00000000-0000-0000-0000-000000000000')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.SystemOption(ID, Code, Name, Type, Data, DefaultData, Note, IsSecurity) VALUES (136, N'EmailForgotPass', N'Email khi quên mật khẩu đăng nhập', 0, N'', N'', NULL, 0)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '75006b84-c909-47c1-b416-01fd3230ec2b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1b23b016-5471-482a-8f58-024d03b5734e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f212a3b6-6bad-4ec8-8096-04fdb32b10a7'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Mẫu số hóa đơn' WHERE ID = '46afa327-2ccd-4297-9469-0524f4789324'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '97c36895-7998-4d4f-bbc9-072092284211'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'de13e6db-5418-4fb6-b82d-07dca7f1e586'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c38851f3-b8a7-4d9f-9624-087133ed9ef4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9688fd89-ea37-4793-afce-092d4a11019a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4459854a-5656-49ab-a1a9-0a9331b93e84'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd808834a-a3a4-4b79-ac79-0b2056dc0f8a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c3ae478f-9ff1-48f4-8723-0b8718cb608b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ad154df9-508e-499f-9b97-0cb08e06d17f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b7ddda1a-7320-4322-91c4-0d6715af400c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c9c03931-3b05-4317-aa60-0dd67490b6d5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4b34447a-388e-4e54-9fff-0e5e4af9d8dc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f6b1f4fc-d00b-45d9-9514-0ee2b981dab2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0bf25e2c-2920-4447-90f7-118a14ab5dbe'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '98d3f1c8-d388-4f44-98a1-11ae5a6fe128'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '31def9fe-2ec0-4c15-8785-1211d9a34905'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'adc635cd-6271-46f7-b04f-12a175723206'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '756de066-15df-41b2-98f7-131e27d337e9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b6962615-a53b-4dbb-8166-1419797f5019'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9f9ea109-d7ea-4011-817d-1485f3f720a8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ba736e22-3961-4330-99e8-153ec7b5c1be'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '14e63169-2110-48db-9e71-166b114a9b2e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '77106d8b-a15f-4a62-b69c-190533a58f8c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '33ad8e37-dfe9-4827-8c36-1acb132a0f35'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '5098283f-187a-416e-be8b-1adfe7cf7f65'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c6f86e74-b970-4d20-a3ca-1b81403314c4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c34ab165-2ce5-4b68-9e66-1c38e67566bc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0e0bdfdc-3644-49e5-ace3-1c91de5b4e33'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b69a6dd3-de27-4665-8282-1d1f57706c1f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6e10306a-baf4-4315-b465-1f71faa770bb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '881da5c0-8bab-4c57-bb08-1fba1b8403f8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '330a61eb-cc59-4fba-b0dc-2145aa1a9f36'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '2189493d-b871-4019-8bc6-21a459e5a24b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b06964e8-2e0a-4c10-8b41-21fe81d9e810'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'cf91d344-3164-4aca-98eb-22b04d50e1ba'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'da9d3034-a294-431e-9ce9-24751e16bad2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9b0eb694-5b40-47f9-9266-24e73aab7621'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7cb91b13-0672-471c-a220-2b59c62e8258'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b824c3cd-8432-4459-8042-2df224358209'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7a1ae1c0-f79e-4720-9cf0-2e46446cd1f7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1df35117-f4b2-4604-9877-2ebbeb800e01'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c3c9a4d7-2192-4778-8098-2f0a02111f77'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c90a87a1-b329-414a-b3bc-30298fe0b0ee'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'aac54337-8091-4911-b1b1-31741dbd79ac'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b740aa6d-7101-40df-9d0e-31c92375fe67'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'aac770fb-c9ef-4f44-8262-321523c8c220'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd9da5c27-5da4-4e45-8eb5-347a3db8c495'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0d7ad216-c0e3-4dd7-9875-352e555abf9f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ac1424a4-0413-46fc-90cd-35c5c13a94ee'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4d9e924e-769b-4479-977c-372bba86b7f3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7dff304b-6b20-42d9-a403-392b02ba2e1f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '17c137d0-43a2-4565-afd0-396a7062148d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '40211460-d12a-461b-8d2e-3ae6021cc46b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e1880f33-a22c-4331-a07b-3c36adc919b2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd2555d88-2e1e-4207-a4aa-3d360d8c64ca'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'be6be530-785f-4e96-a830-3de7035b20f3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8e9eedd3-d9d3-4a63-ba64-3eb24a50bfae'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1478bb69-7adf-4ff4-b3f7-3edd661d4670'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fea28c98-ab92-4d53-8e97-3f3f75e6af43'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '73f93418-32c9-4052-9254-3fcb1828f521'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b771a2ad-784a-4cef-a2ea-40c848ae48c7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '96b26a8d-289a-4b0a-b056-4140c2640993'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4753bcbc-e3d1-46e1-b0bc-416266e04251'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8b2e11dc-ba56-4bea-b4dc-41889f764ab5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f20b61df-0f22-4660-9071-41a05fad62b9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'aacc7541-a1f0-4146-8e94-43e6f63f4349'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '33e972aa-5d88-412a-a6ba-44bff1241d82'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6a68f53d-984d-4d64-915c-45048b04d6dc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '71a25a25-1d71-4ee3-adc3-454efcb30a31'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '90c8bec3-48e4-4c90-9bce-48baacb359e0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ca5e70a6-494d-4b9e-80e6-48e706e7e90c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '702fee37-56fa-4619-a530-495dc36884f6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'af254aff-785d-44c3-bd6a-49d39563b1a6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '5f36c817-880c-4493-88cf-49f4e33aa596'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6f6ca8ef-539d-4370-8109-4a50a43f3efb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1d41b6be-d9ab-4d4d-9096-4acae4a61cf8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c14e5ca5-f2c8-4085-9f43-507bb27868ee'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ab0ad70b-a79d-4fef-b4c2-510111ce789f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6cc75c72-d85d-4e95-ac6e-51161983011a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd0e9aaa1-977b-4196-bbb6-515d8f6ea44b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e331913a-7d7b-45f4-87d5-51836c7aecc3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '234cf647-bdd6-4e80-b56b-52dbf7f37e2b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fe86671b-2637-4d3a-8b67-546490fecb76'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '00593073-23a2-416e-b768-548dde41a612'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'bb79087e-df26-45bb-9401-54c1b01dca53'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '2d2be932-db7c-4a11-9af3-55305556cd7d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8a3812ce-c293-44bf-8f77-55d7532eb0ed'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f0e9ec9b-19e5-4858-8d84-55ff367e5553'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0d6cb72b-aa6e-4c54-bb22-56f8f9986b01'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ee593f8d-b115-4f3a-9f0a-57f27df3640b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9a61d5fe-e5fa-4776-bb8b-5c1a37a29f97'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a59eef43-0ae3-4804-affc-5c425be6578c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd1a6cd6e-261c-4fac-9051-5cc98bd9530d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1a82ec8c-a407-465e-8971-5d7ac1b78fe5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fc21d323-aedb-4d0b-be8d-5eb4b40beb0a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9d83f687-081c-4cd5-b69c-5ef6d182caa5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '47d57fd5-f1ef-4ce2-9f33-60cf252530fc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '41bc9a80-6d77-4a56-a7c1-6274d938e188'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd3b5a749-78b8-4c04-ae90-628f3be67552'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b7cc92ee-19fe-4877-880b-64113d5037ca'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6b007071-d36d-4731-a8fb-6556821a8522'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0fcf4a1c-8c57-413f-a155-661809d77bea'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '80c5d536-47e8-4a7e-b399-68547bcc8d6b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6892ba01-9cab-4494-9654-694c303305b6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '31ab1347-e257-4e0e-b594-69f5d0fd6a8b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3402e535-205c-40fe-b4a2-6aee991cbfe9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd4789912-20a6-4c83-9d52-6b369d61b551'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '278193fc-419c-4804-b8b2-6b9fdd7d367b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f408a137-46d6-428e-bffe-6c5b7e57d31f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '87096530-ecdf-4b70-903f-6d2644f9aa8a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fa77d9cf-361d-4bb6-9d6f-703a5ff6f4e6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '63d6a310-7cc4-42a6-b0fc-704f06c71d7f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '349fbcdf-8caa-4a54-8219-70d1dccb0f2d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b0797ccb-b814-435c-9111-721fdba647b6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '004283dd-d9c3-4994-92da-744fb4edda2d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e3551320-3851-49f3-b172-773559505270'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b7a9cfe4-6ca6-4e65-8235-776cf0447d11'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a53c3912-adb3-47a4-97e8-777aa104c337'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '357118bf-79e9-4654-a2ba-7782defad07c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f310fd44-e5c7-4581-b98a-7808829dbb32'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e06e38f8-f2a9-47a1-b960-7817ae19a155'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '42cda470-f758-43d3-8706-786deee606f8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c0f0d9c7-c8d3-4138-aa93-78a5fbd266b1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6e12d6ae-0e3d-414e-a0ba-791cffcbc105'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '544f7372-36fd-4466-88a6-7a035fdf8b96'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8c0a115b-8c39-42a2-aac3-7b92ba8c2202'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a324ba75-2b7c-4291-832f-7cc8312eb778'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0b4cb582-0d00-448e-9bc5-7e2414004bb8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'dd1cbe9a-232b-4459-a84a-7f2d3575a315'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '55a2efdf-6869-4e8d-89f6-7f8fa6e79093'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '91a87b06-ffa1-4b96-a7b1-8047dd4e58e5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c46c6d05-c6d0-4662-a4b2-81900e8366f3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9836b586-5403-4ccc-ac46-824f18d86cda'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3defdb8e-08ee-410f-af2e-82ee6d5ff429'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '935d8881-2ece-4b7f-9848-86beec925a26'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '793390c6-d57e-430f-a55b-8945fd51b641'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '27003906-edc3-4d48-83d1-89c66f9787f8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fd06d5a1-7c3b-4a85-96e0-8aafefdfa3f5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '2c3b8dda-33da-4f67-a4e1-8c1d04794e64'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e804cf22-1c86-4a60-99ad-8c5f96827bc4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7a113dcb-3b5d-4812-8a3c-8d3d5a1a40f1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '00d841ba-0598-4c91-b868-8d43b60611fa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0ea1fe2c-f7cc-4418-b8bd-9041deab0452'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3224832a-fe7b-4503-8dff-91c1333d6b3f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7bfdd216-df44-445e-af8b-91ec40a63985'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '19ccc042-3729-4581-9256-93cea21e5c09'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '44e049cf-43cb-4a3b-9e37-93d1b9d76a0a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '176e3187-3715-43c6-8477-93d9e0ad4593'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '64a77163-74b3-4203-acd2-96083f3cafde'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a0b31b11-6661-4cf6-9056-96be53edd92d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fec0f6cf-66c7-4c4d-b957-97c4da71c024'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '05d1483f-41cc-4633-bb64-99a4eca90c5b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7f217da6-54bc-45b3-ab8b-9a2cc6435720'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'f14e8896-327b-4d65-a7aa-9a7d152f8bb8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'bf65958a-8bf4-46a7-889a-9ab6be7122ad'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7e8f15aa-36b8-4d00-b59e-9b427fefcb8c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8f97f64f-cb09-406f-918f-9c2011999cc1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3d892de8-5012-4f9b-aa8c-9cc96e416abc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0e64d3df-cb2b-421e-8cd6-9cfc6d45940c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '62c2d9ab-783b-4427-9e37-9e8110748089'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '92c80ecf-322b-4e93-95e0-a0976c2ba69f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd48c4c8c-1a6d-46b2-8127-a251925d0f8c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b30691e3-d167-493f-8e9a-a2b12ed7094a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd7e98032-f35a-49b1-92ab-a47db50a3fbd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '5835f914-0fa0-4390-b917-a6671c869aa6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0eeb9d84-fca8-4455-94ec-a77d145bf929'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '10d1dd07-2013-4a92-a9b8-a8226946b4d3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '63baf285-8420-4ad1-8b55-a8ce5ff47b2a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '286d8891-1182-4411-9df8-a9dbdff7a113'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '11eeb64d-47aa-4b47-8aa9-add89ffeac8c'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7aeafa7e-9105-40a1-9b90-b0296f5763b7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'bbdd7a4f-83f7-4c10-9594-b067fe745cd1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7a866ee5-8cd3-4b03-9fb5-b0ea5123715d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6c63ff18-491d-47e7-95d1-b1cc1fd7860d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e20c047a-d97f-499c-b500-b425d16d956d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4577fa42-dbb7-4447-8f5d-b4b2278633d5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8ded385f-3b96-4206-9927-b5cc3c7650bc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '69bc613c-99ca-4912-bfc6-b86e906f37a9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '66592843-e45f-471b-9ef5-b8cd02c992f9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e854a6b5-4850-4e5f-b772-b9b4f5478e57'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e76bd1dd-a622-450e-9a78-baa96c6d822a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c0357192-081e-4879-b044-baefdbdf55ef'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '621e9cdd-14e4-4f30-858a-bc631b54162d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8e83eb67-d85d-4617-af00-bd31482a59b4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0cfc4651-7529-4618-b3dd-bd91353666c4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8fb971b6-62bb-4b56-8724-c0690cfa818a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '06174800-e44e-4215-b60a-c1c82bb3ad4e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ffbffb76-a4ac-4232-89bf-c26e0e804511'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '2948d8f2-acdf-41c1-a6f2-c40fdff5b325'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '57b66c5c-bef5-463f-b3b0-c41b264d08a4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '62996861-65bd-49e3-b392-c44ba1151757'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ce5f807d-6525-4834-86b5-c56243028d39'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3d93a6b2-805f-4017-a40f-c5e49c80bb24'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0cdca876-e0ab-4354-af3b-c630b070f4cd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c5b72028-66e1-40bb-8c30-c66e9e780c5b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '51324b48-c884-442a-a532-c6c0c012c693'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '61f3694e-1ef9-44e7-a3da-c7bc526453c1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '940d4aa3-6ae8-48c5-8bf0-c9234c3924ec'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4f03d2e1-1cf0-49ff-ab1f-c9e26d8f388a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ce75579c-3790-459b-b0a7-ca10e82755ef'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '3069990e-6f6a-4797-8b3a-ca7126ab7f78'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b1da244e-b336-4172-ad5a-cc37c91d92fd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8e9b151c-cc89-4e62-80b2-ce8d4ab27b4a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '532b776b-4599-4242-a70d-cfb6184ce326'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '52433bf9-14f8-4533-b30d-cfe4e2052f21'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c9219768-9e3a-4ddd-8518-d18248593505'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e84746dd-9a3c-415d-aff1-d2472b7fdc65'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd52b9012-2244-4c21-ac70-d2cde48f3798'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'adf8e843-39d7-4c65-ab8d-d2dc85215da9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '24fcee18-f4c1-45bc-9161-d32f0e9824f0'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '449c27f8-fd16-4263-bffa-d7694421395f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '209fc03b-0667-45a0-8b88-d966a0f49fea'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0d42d44e-8713-4b53-801d-dca4b0619121'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c82231fd-2759-4e8b-8088-dcb4bccaeb89'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd6cbbab5-d176-40c6-a19d-debd8c245c4f'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '14efde4a-d1fa-472d-9b63-e09e05ce6cdf'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '5339cbfa-492b-4764-ba97-e2235470b37d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ae2b7e64-eaeb-435e-96b1-e377d07d191d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ce16ad5a-8ce2-4057-a414-e3ac7c140fb8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '0d68a09d-b350-4485-83ff-e51050b25c0b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'adb595bf-147c-4208-9a7c-e5a2c9a8e412'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'e44c5627-4342-4446-af86-e754746f59e5'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9b65308d-5ab3-43d5-ab13-e78968bc2ccb'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c2f7fd07-7ca2-4545-93b4-e78b6203fee3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '847d24c5-b5ff-4433-b0a2-e8e7ce21c594'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'fd4171ef-2945-47b8-a89a-e9df228c54c2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b8794276-5eb6-447f-b444-ecf721262c22'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'aaa37cde-d92f-4f4c-aa87-ed3ba79de042'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1c14b9dc-86b7-416c-bff7-ed8a914dc9fd'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '63e782a9-4424-475f-a595-eed32c5a430d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd260d87a-8da5-479f-b143-ef314f9886a9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '67aba6b6-8c2a-45c0-aef9-ef767abf0ab7'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd8047841-f0cc-4379-8f4f-f241269d4ac8'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '36a0db25-be70-4d21-9c19-f44a89bfe2d6'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7ed01c40-0925-4649-beb3-f4f8587d3b37'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Mẫu số hóa đơn' WHERE ID = '3bc99973-c6a4-4079-a89d-f5ea73d3bb59'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '49bf89b4-fbe9-427d-93ca-f60609e156f4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd7fb8e97-c632-482d-9405-f6472b180cc4'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'acc41787-114b-4d13-ba08-f8d26f79dcce'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'b6dbacb9-8065-4a45-817f-f926e63a02c3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '4d35217e-f58c-48df-891f-f92f577bfb31'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '03b8a9b7-0737-40d0-a8ac-f9bdfd409af1'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1bce3983-e5fd-4994-9c1d-fb52a3056f84'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '448c23ab-0203-480a-8dfe-fce426a9d05c'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'110,111,112,113,114,115,118' WHERE ID = 103
UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'110,111,112,113,114,115,118' WHERE ID = 194
UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'110,111,112,113,114,115,118' WHERE ID = 237

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
SET IDENTITY_INSERT dbo.VoucherPatternsReport ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (281, 0, 0, N'SAQuote-BH-PXKBH', N'Phiếu xuất kho bán hàng', N'SAQuote-BH-PXKBH.rst', 1, N'321,322,320,412,323,324,325')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (282, 0, 11, N'MCPayment-02-TT-Double-PPService', N'Phiếu chi: Mẫu 02-TT (2 liên)', N'MCPayment-02-TT-Double-PPService.rst', 1, N'116')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (283, 0, 10, N'MCReceipt-01-TT-Double-SAInvoice', N'Phiếu thu: Mẫu 01-TT (2 liên)', N'MCReceipt-01-TT-Double-SAInvoice.rst', 1, N'102')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (284, 0, 11, N'MCPayment-02-TT-Double-PPInvoice', N'Phiếu chi: Mẫu 02-TT (2 liên)', N'MCPayment-02-TT-Double-PPInvoice.rst', 1, N'117')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (285, 0, 11, N'MCPayment-02-TT-TSCD', N'Phiếu chi: 02-TT', N'MCPayment-02-TT-TSCD.rst', 1, N'119')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (286, 0, 11, N'MCPayment-02-TT-A5-TSCD', N'Phiếu chi: 02-TT (A5)', N'MCPayment-02-TT-A5-TSCD.rst', 1, N'119')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (287, 0, 11, N'MCPayment-02-TT-Double-TSCD', N'Phiếu chi: Mẫu 02-TT (2 liên)', N'MCPayment-02-TT-Double-TSCD.rst', 1, N'119')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (289, 0, 11, N'MCPayment-02-TT-Double-CCDC', N'Phiếu chi: Mẫu 02-TT (2 liên)', N'MCPayment-02-TT-Double-CCDC.rst', 1, N'902')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (290, 0, 11, N'MCPayment-02-TT-A5-CCDC', N'Phiếu chi: 02-TT (A5)', N'MCPayment-02-TT-A5-CCDC.rst', 1, N'902')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (291, 0, 11, N'MCPayment-02-TT-CCDC', N'Phiếu chi: 02-TT', N'MCPayment-02-TT-CCDC.rst', 1, N'902')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (292, 0, 0, N'MCReceipt-A5', N'Phiếu thu: Mẫu 01-TT (A5)', N'MCReceipt-A5.rst', 1, N'')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (293, 0, 0, N'MCPayment-A5', N'Phiếu chi: 02-TT (A5)', N'MCPayment-A5.rst', 1, N'')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (294, 0, 0, N'MCReceipt-A4', N'Phiếu thu: Mẫu 01-TT', N'MCReceipt-A4.rst', 1, N'')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (295, 0, 0, N'MCPayment-A4', N'Phiếu chi: 02-TT', N'MCPayment-A4.rst', 1, N'')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (296, 0, 0, N'MCReceipt-A4-Double', N'Phiếu thu: Mẫu 01-TT (2 liên)', N'MCReceipt-A4-Double.rst', 1, N'')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (297, 0, 0, N'MCPayment-A4-Double', N'Phiếu chi: 02-TT (2 liên)', N'MCPayment-A4-Double.rst', 1, N'')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (298, 0, 0, N'GOtherVoucher-ChungTu', N'Chứng từ kế toán', N'GOtherVoucher-ChungTu.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (299, 0, 0, N'RSInward', N'Phiếu nhập kho', N'RSInward.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (300, 0, 0, N'RSOutward', N'Phiếu xuất kho', N'RSOutward.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (301, 0, 0, N'RSOutward-SAInvoice', N'Phiếu xuất kho bán hàng', N'RSOutward-SAInvoice.rst', 1, NULL)
GO
SET IDENTITY_INSERT dbo.VoucherPatternsReport OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DBCC CHECKIDENT('dbo.VoucherPatternsReport', RESEED, 301)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

UPDATE [dbo].[EMContractDetailMG] SET [AmountOriginal] = [Amount], [VATAmountOriginal] = [VATAmount], [DiscountAmountOriginal] = [DiscountAmount], [TotalAmountOriginal] = [TotalAmount], [UnitPriceOriginal] = [UnitPrice] WHERE ID=ID

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF