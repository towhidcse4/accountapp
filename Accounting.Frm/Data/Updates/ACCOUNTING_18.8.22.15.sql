
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO

IF COL_LENGTH('dbo.SAInvoiceDetail', '[TaxExchangeRate]') IS NULL
BEGIN
    ALTER TABLE [dbo].[SAInvoiceDetail]
		ADD [TaxExchangeRate] [decimal](25, 10) NULL
END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

IF COL_LENGTH('dbo.SAInvoiceDetail', '[ExportTaxRate]') IS NULL
BEGIN
    ALTER TABLE [dbo].[SAInvoiceDetail]
		ADD [ExportTaxRate] [decimal](25, 10) NULL
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

IF COL_LENGTH('[dbo].[SAInvoiceDetail]', '[ExportTaxAmount]') IS NULL
BEGIN
    ALTER TABLE [dbo].[SAInvoiceDetail]
		ADD [ExportTaxAmount] [money] NULL
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

IF COL_LENGTH('[dbo].[SAInvoiceDetail]', '[ExportTaxAccount]') IS NULL
BEGIN
    ALTER TABLE [dbo].[SAInvoiceDetail]
		ADD [ExportTaxAccount] [nvarchar](25) NULL
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
IF COL_LENGTH('[dbo].[CPPeriod]', '[IsDelete]') IS NULL
BEGIN
	ALTER TABLE [dbo].[RSInwardOutwardDetail]
	  ADD [AccountingObjectID] [uniqueidentifier] NULL
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
IF COL_LENGTH('[dbo].[CPPeriod]', '[IsDelete]') IS NULL
BEGIN
	ALTER TABLE [dbo].[CPPeriod]
	  ADD [IsDelete] [bit] NULL
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
DROP VIEW [dbo].[ViewVouchersCloseBook]
GO
CREATE VIEW [dbo].[ViewVouchersCloseBook]
AS
/*Create by tnson 24/08/2011 - View này chỉ giành riêng cho việc kiểm tra các chứng từ chưa ghi sổ trước khi khóa sổ không được dùng cho việc khác */ 
SELECT dbo.MCPayment.ID, dbo.MCPaymentDetail.ID AS DetailID, 
                         dbo.MCPayment.TypeID, dbo.Type.TypeName, dbo.MCPayment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCPayment.Date, dbo.MCPayment.PostedDate, dbo.MCPaymentDetail.DebitAccount, 
                         dbo.MCPaymentDetail.CreditAccount, dbo.MCPayment.CurrencyID, dbo.MCPaymentDetail.AmountOriginal, dbo.MCPaymentDetail.Amount, NULL AS OrgPrice, dbo.MCPayment.Reason, 
                         dbo.MCPaymentDetail.Description, dbo.MCPaymentDetail.CostSetID, CostSet.CostSetCode, dbo.MCPaymentDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MCPayment.EmployeeID, dbo.MCPayment.BranchID, NULL 
                         AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MCPayment.Recorded, dbo.MCPayment.TotalAmount, 
                         dbo.MCPayment.TotalAmountOriginal, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MCPayment' AS RefTable
/*case AccountingObject.IsEmployee when 1 then  AccountingObject.AccountingObjectName end AS EmployeeName*/ 
FROM dbo.MCPayment INNER JOIN
                         dbo.MCPaymentDetail ON dbo.MCPayment.ID = dbo.MCPaymentDetail.MCPaymentID INNER JOIN
                         dbo.Type ON MCPayment.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MCPaymentDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MCPaymentDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MCPaymentDetail.ContractID

UNION ALL
SELECT        dbo.MCPayment.ID, dbo.MCPaymentDetailTax.ID AS DetailID, dbo.MCPayment.TypeID, dbo.Type.TypeName, dbo.MCPayment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCPayment.Date, 
                         dbo.MCPayment.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MCPayment.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MCPayment.Reason, 
                         dbo.MCPaymentDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MCPayment.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MCPaymentDetailTax.VATAmountOriginal, dbo.MCPaymentDetailTax.VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MCPayment.Recorded, dbo.MCPayment.TotalAmount, dbo.MCPayment.TotalAmountOriginal, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MCPayment' AS RefTable
FROM            dbo.MCPayment INNER JOIN
                         dbo.MCPaymentDetailTax ON dbo.MCPayment.ID = dbo.MCPaymentDetailTax.MCPaymentID INNER JOIN
                         dbo.Type ON MCPayment.TypeID = dbo.Type.ID

UNION ALL
SELECT        dbo.MBDeposit.ID, dbo.MBDepositDetail.ID AS DetailID, dbo.MBDeposit.TypeID, dbo.Type.TypeName, dbo.MBDeposit.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBDeposit.Date, 
                         dbo.MBDeposit.PostedDate, dbo.MBDepositDetail.DebitAccount, dbo.MBDepositDetail.CreditAccount, dbo.MBDeposit.CurrencyID, dbo.MBDepositDetail.AmountOriginal, dbo.MBDepositDetail.Amount, NULL 
                         AS OrgPrice, dbo.MBDeposit.Reason, dbo.MBDepositDetail.Description, dbo.MBDepositDetail.CostSetID, CostSet.CostSetCode, dbo.MBDepositDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MBDeposit.EmployeeID, 
                         dbo.MBDeposit.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBDeposit.Recorded, 
                         dbo.MBDeposit.TotalAmountOriginal, dbo.MBDeposit.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MBDeposit' AS RefTable
FROM            dbo.MBDeposit INNER JOIN
                         dbo.MBDepositDetail ON dbo.MBDeposit.ID = dbo.MBDepositDetail.MBDepositID INNER JOIN
                         dbo.Type ON MBDeposit.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBDepositDetail.AccountingObjectID = dbo.AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBDepositDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBDepositDetail.ContractID

UNION ALL
SELECT        dbo.MBDeposit.ID, dbo.MBDepositDetailTax.ID AS DetailID, dbo.MBDeposit.TypeID, dbo.Type.TypeName, dbo.MBDeposit.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBDeposit.Date, 
                         dbo.MBDeposit.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MBDeposit.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBDeposit.Reason, 
                         dbo.MBDepositDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBDeposit.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MBDepositDetailTax.VATAmountOriginal, dbo.MBDepositDetailTax.VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MBDeposit.Recorded, dbo.MBDeposit.TotalAmountOriginal, dbo.MBDeposit.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBDeposit' AS RefTable
FROM            dbo.MBDeposit INNER JOIN
                         dbo.MBDepositDetailTax ON dbo.MBDeposit.ID = dbo.MBDepositDetailTax.MBDepositID INNER JOIN
                         dbo.Type ON MBDeposit.TypeID = dbo.Type.ID

UNION ALL
SELECT        dbo.MBInternalTransfer.ID, dbo.MBInternalTransferDetail.ID AS DetailID, dbo.MBInternalTransfer.TypeID, dbo.Type.TypeName, dbo.MBInternalTransfer.No, NULL AS InwardNo, NULL AS OutwardNo, 
                         dbo.MBInternalTransfer.Date, dbo.MBInternalTransfer.PostedDate, dbo.MBInternalTransferDetail.DebitAccount, dbo.MBInternalTransferDetail.CreditAccount, dbo.MBInternalTransferDetail.CurrencyID, 
                         dbo.MBInternalTransferDetail.AmountOriginal, dbo.MBInternalTransferDetail.Amount, NULL AS OrgPrice, dbo.MBInternalTransfer.Reason, dbo.MBInternalTransfer.Reason AS Description, 
                         dbo.MBInternalTransferDetail.CostSetID, CostSet.CostSetCode, dbo.MBInternalTransferDetail.ContractID, EMContract.Code AS ContractCode, NULL AS AccountingObjectID, '' AS AccountingObjectCategoryName, 
                         '' AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBInternalTransfer.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.MBInternalTransfer.Recorded, dbo.MBInternalTransfer.TotalAmountOriginal, dbo.MBInternalTransfer.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBInternalTransfer' AS RefTable
FROM            dbo.MBInternalTransfer INNER JOIN
                         dbo.MBInternalTransferDetail ON dbo.MBInternalTransfer.ID = dbo.MBInternalTransferDetail.MBInternalTransferID INNER JOIN
                         dbo.Type ON dbo.MBInternalTransfer.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBInternalTransferDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBInternalTransferDetail.ContractID

UNION ALL
SELECT        dbo.MBTellerPaper.ID, dbo.MBTellerPaperDetail.ID AS DetailID, dbo.MBTellerPaper.TypeID, dbo.Type.TypeName, dbo.MBTellerPaper.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBTellerPaper.Date, 
                         dbo.MBTellerPaper.PostedDate, dbo.MBTellerPaperDetail.DebitAccount, dbo.MBTellerPaperDetail.CreditAccount, dbo.MBTellerPaper.CurrencyID, dbo.MBTellerPaperDetail.AmountOriginal, 
                         dbo.MBTellerPaperDetail.Amount, NULL AS OrgPrice, dbo.MBTellerPaper.Reason, dbo.MBTellerPaperDetail.Description, dbo.MBTellerPaperDetail.CostSetID, CostSet.CostSetCode, 
                         dbo.MBTellerPaperDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.MBTellerPaper.EmployeeID, dbo.MBTellerPaper.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL 
                         AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MBTellerPaper.Recorded, dbo.MBTellerPaper.TotalAmountOriginal, dbo.MBTellerPaper.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBTellerPaper' AS RefTable
FROM            dbo.MBTellerPaper INNER JOIN
                         dbo.MBTellerPaperDetail ON dbo.MBTellerPaper.ID = dbo.MBTellerPaperDetail.MBTellerPaperID INNER JOIN
                         dbo.Type ON dbo.MBTellerPaper.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBTellerPaperDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBTellerPaperDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBTellerPaperDetail.ContractID

UNION ALL
SELECT        dbo.MBTellerPaper.ID, dbo.MBTellerPaperDetailTax.ID AS DetailID, dbo.MBTellerPaper.TypeID, dbo.Type.TypeName, dbo.MBTellerPaper.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBTellerPaper.Date, 
                         dbo.MBTellerPaper.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MBTellerPaper.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBTellerPaper.Reason, 
                         dbo.MBTellerPaperDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBTellerPaper.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MBTellerPaperDetailTax.VATAmountOriginal, 
                         dbo.MBTellerPaperDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBTellerPaper.Recorded, dbo.MBTellerPaper.TotalAmountOriginal, dbo.MBTellerPaper.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBTellerPaper' AS RefTable
FROM            dbo.MBTellerPaper INNER JOIN
                         dbo.MBTellerPaperDetailTax ON dbo.MBTellerPaper.ID = dbo.MBTellerPaperDetailTax.MBTellerPaperID INNER JOIN
                         dbo.Type ON dbo.MBTellerPaper.TypeID = dbo.Type.ID

UNION ALL
SELECT        dbo.MBCreditCard.ID, dbo.MBCreditCardDetail.ID AS DetailID, dbo.MBCreditCard.TypeID, dbo.Type.TypeName, dbo.MBCreditCard.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBCreditCard.Date, 
                         dbo.MBCreditCard.PostedDate, dbo.MBCreditCardDetail.DebitAccount, dbo.MBCreditCardDetail.CreditAccount, dbo.MBCreditCard.CurrencyID, dbo.MBCreditCardDetail.AmountOriginal, 
                         dbo.MBCreditCardDetail.Amount, NULL AS OrgPrice, dbo.MBCreditCard.Reason, dbo.MBCreditCardDetail.Description, dbo.MBCreditCardDetail.CostSetID, CostSet.CostSetCode, dbo.MBCreditCardDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.MBCreditCard.EmployeeID, dbo.MBCreditCard.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL 
                         AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MBCreditCard.Recorded, dbo.MBCreditCard.TotalAmountOriginal, dbo.MBCreditCard.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBCreditCard' AS RefTable
FROM            dbo.MBCreditCard INNER JOIN
                         dbo.MBCreditCardDetail ON dbo.MBCreditCard.ID = dbo.MBCreditCardDetail.MBCreditCardID INNER JOIN
                         dbo.Type ON dbo.MBCreditCard.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MBCreditCardDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MBCreditCardDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MBCreditCardDetail.ContractID

UNION ALL
SELECT        dbo.MBCreditCard.ID, dbo.MBCreditCardDetailTax.ID AS DetailID, dbo.MBCreditCard.TypeID, dbo.Type.TypeName, dbo.MBCreditCard.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MBCreditCard.Date, 
                         dbo.MBCreditCard.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MBCreditCard.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MBCreditCard.Reason, 
                         dbo.MBCreditCardDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MBCreditCard.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MBCreditCardDetailTax.VATAmountOriginal, 
                         dbo.MBCreditCardDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MBCreditCard.Recorded, dbo.MBCreditCard.TotalAmountOriginal, dbo.MBCreditCard.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MBCreditCard' AS RefTable
FROM            dbo.MBCreditCard INNER JOIN
                         dbo.MBCreditCardDetailTax ON dbo.MBCreditCard.ID = dbo.MBCreditCardDetailTax.MBCreditCardID INNER JOIN
                         dbo.Type ON dbo.MBCreditCard.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        dbo.MCReceipt.ID, dbo.MCReceiptDetail.ID AS DetailID, dbo.MCReceipt.TypeID, dbo.Type.TypeName, dbo.MCReceipt.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCReceipt.Date, 
                         dbo.MCReceipt.PostedDate, dbo.MCReceiptDetail.DebitAccount, dbo.MCReceiptDetail.CreditAccount, dbo.MCReceipt.CurrencyID, dbo.MCReceiptDetail.AmountOriginal, dbo.MCReceiptDetail.Amount, NULL 
                         AS OrgPrice, dbo.MCReceipt.Reason, dbo.MCReceiptDetail.Description, dbo.MCReceiptDetail.CostSetID, CostSet.CostSetCode, dbo.MCReceiptDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.MCReceipt.EmployeeID, 
                         dbo.MCReceipt.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.MCReceipt.Recorded, 
                         dbo.MCReceipt.TotalAmountOriginal, dbo.MCReceipt.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'MCReceipt' AS RefTable
FROM            dbo.MCReceipt INNER JOIN
                         dbo.MCReceiptDetail ON dbo.MCReceipt.ID = dbo.MCReceiptDetail.MCReceiptID INNER JOIN
                         dbo.Type ON MCReceipt.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON MCReceiptDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = MCReceiptDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = MCReceiptDetail.ContractID

UNION ALL
SELECT        dbo.MCReceipt.ID, dbo.MCReceiptDetailTax.ID AS DetailID, dbo.MCReceipt.TypeID, dbo.Type.TypeName, dbo.MCReceipt.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.MCReceipt.Date, 
                         dbo.MCReceipt.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, dbo.MCReceipt.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.MCReceipt.Reason, 
                         dbo.MCReceiptDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AS AccountingObjectCode, NULL AS EmployeeID, dbo.MCReceipt.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.MCReceiptDetailTax.VATAmountOriginal, dbo.MCReceiptDetailTax.VATAmount, NULL 
                         AS Quantity, NULL AS UnitPrice, dbo.MCReceipt.Recorded, dbo.MCReceipt.TotalAmountOriginal, dbo.MCReceipt.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL 
                         AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'MCReceipt' AS RefTable
FROM            dbo.MCReceipt INNER JOIN
                         dbo.MCReceiptDetailTax ON dbo.MCReceipt.ID = dbo.MCReceiptDetailTax.MCReceiptID INNER JOIN
                         dbo.Type ON MCReceipt.TypeID = dbo.Type.ID

UNION ALL
SELECT        dbo.FAAdjustment.ID, dbo.FAAdjustmentDetail.ID AS DetailID, dbo.FAAdjustment.TypeID, dbo.Type.TypeName, dbo.FAAdjustment.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FAAdjustment.Date, 
                         dbo.FAAdjustment.PostedDate, dbo.FAAdjustmentDetail.DebitAccount, dbo.FAAdjustmentDetail.CreditAccount, NULL AS CurrencyID, $ 0 AS AmountOriginal, dbo.FAAdjustmentDetail.Amount, $ 0 AS OrgPrice, 
                         dbo.FAAdjustment.Reason, dbo.FAAdjustmentDetail.Description, dbo.FAAdjustmentDetail.CostSetID, CostSet.CostSetCode, dbo.FAAdjustmentDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, NULL AS EmployeeID, 
                         dbo.FAAdjustment.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, $ 0 AS VATAmountOriginal, $ 0 AS VATAmount, $ 0 AS Quantity, $ 0 AS UnitPrice, dbo.FAAdjustment.Recorded, 
                         $ 0 AS TotalAmountOriginal, dbo.FAAdjustment.TotalAmount, $ 0 AS DiscountAmountOriginal, $ 0 AS DiscountAmount, NULL AS DiscountAccount, $ 0 AS FreightAmountOriginal, NULL AS FreightAmount, 
                         0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FAAdjustment' AS RefTable
FROM            dbo.FAAdjustment INNER JOIN
                         dbo.FAAdjustmentDetail ON dbo.FAAdjustment.ID = dbo.FAAdjustmentDetail.FAAdjustmentID INNER JOIN
                         dbo.Type ON FAAdjustment.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FAAdjustmentDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FAAdjustment.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FAAdjustmentDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FAAdjustmentDetail.ContractID

UNION ALL
SELECT        dbo.FADepreciation.ID, dbo.FADepreciationDetail.ID AS DetailID, dbo.FADepreciation.TypeID, dbo.Type.TypeName, dbo.FADepreciation.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FADepreciation.Date, 
                         dbo.FADepreciation.PostedDate, dbo.FADepreciationDetail.DebitAccount, dbo.FADepreciationDetail.CreditAccount, FADepreciationDetail.CurrencyID, dbo.FADepreciationDetail.AmountOriginal, 
                         dbo.FADepreciationDetail.Amount, dbo.FADepreciationDetail.OrgPrice, dbo.FADepreciation.Reason, dbo.FADepreciationDetail.Description, dbo.FADepreciationDetail.CostSetID, CostSet.CostSetCode, 
                         dbo.FADepreciationDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.FADepreciationDetail.EmployeeID, dbo.FADepreciation.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL 
                         AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, FADepreciationDetail.DepartmentID, Department.DepartmentName, NULL AS VATAccount, NULL 
                         AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.FADepreciation.Recorded, dbo.FADepreciation.TotalAmountOriginal, dbo.FADepreciation.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FADepreciation' AS RefTable
FROM            dbo.FADepreciation INNER JOIN
                         dbo.FADepreciationDetail ON dbo.FADepreciation.ID = dbo.FADepreciationDetail.FADepreciationID INNER JOIN
                         dbo.Type ON FADepreciation.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FADepreciationDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FADepreciationDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FADepreciationDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FADepreciationDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FADepreciationDetail.DepartmentID

UNION ALL
SELECT        dbo.FAIncrement.ID, dbo.FAIncrementDetail.ID AS DetailID, dbo.FAIncrement.TypeID, dbo.Type.TypeName, dbo.FAIncrement.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FAIncrement.Date, 
                         dbo.FAIncrement.PostedDate, dbo.FAIncrementDetail.DebitAccount, dbo.FAIncrementDetail.CreditAccount, FAIncrement.CurrencyID, dbo.FAIncrementDetail.AmountOriginal, dbo.FAIncrementDetail.Amount, 
                         dbo.FAIncrementDetail.OrgPriceOriginal AS OrgPrice, dbo.FAIncrement.Reason, dbo.FAIncrementDetail.Description, dbo.FAIncrementDetail.CostSetID, CostSet.CostSetCode, dbo.FAIncrementDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.AccountingObject.ID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, 
                         dbo.FAIncrement.EmployeeID, dbo.FAIncrement.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, FAIncrementDetail.DepartmentID, dbo.Department.DepartmentName, dbo.FAIncrementDetail.VATAccount AS VATAccount, 
                         dbo.FAIncrementDetail.VATAmountOriginal AS VATAmountOriginal, dbo.FAIncrementDetail.VATAmount AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.FAIncrement.Recorded, 
                         dbo.FAIncrement.TotalAmountOriginal, dbo.FAIncrement.TotalAmount, dbo.FAIncrementDetail.DiscountAmountOriginal, dbo.FAIncrementDetail.DiscountAmount, NULL AS DiscountAccount, 
                         dbo.FAIncrementDetail.FreightAmountOriginal, dbo.FAIncrementDetail.FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FAIncrement' AS RefTable
FROM            dbo.FAIncrement INNER JOIN
                         dbo.FAIncrementDetail ON dbo.FAIncrement.ID = dbo.FAIncrementDetail.ID INNER JOIN
                         dbo.Type ON FAIncrement.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FAIncrementDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FAIncrementDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FAIncrementDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FAIncrementDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FAIncrementDetail.DepartmentID

UNION ALL
SELECT        dbo.FADecrement.ID, dbo.FADecrementDetail.ID AS DetailID, dbo.FADecrement.TypeID, dbo.Type.TypeName, dbo.FADecrement.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.FADecrement.Date, 
                         dbo.FADecrement.PostedDate, dbo.FADecrementDetail.DebitAccount, dbo.FADecrementDetail.CreditAccount, FADecrement.CurrencyID, dbo.FADecrementDetail.AmountOriginal, 
                         dbo.FADecrementDetail.Amount, NULL AS OrgPrice, dbo.FADecrement.Reason, dbo.FADecrementDetail.Description, dbo.FADecrementDetail.CostSetID, CostSet.CostSetCode, dbo.FADecrementDetail.ContractID, 
                         EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, 
                         dbo.AccountingObject.AccountingObjectCode, dbo.FADecrementDetail.EmployeeID, dbo.FADecrement.BranchID, dbo.FixedAsset.ID AS FixedAssetID, dbo.FixedAsset.FixedAssetName, NULL 
                         AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, FADecrementDetail.DepartmentID, Department.DepartmentName, NULL AS VATAccount, NULL 
                         AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.FADecrement.Recorded, dbo.FADecrement.TotalAmountOriginal, dbo.FADecrement.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'FADecrement' AS RefTable
FROM            dbo.FADecrement INNER JOIN
                         dbo.FADecrementDetail ON dbo.FADecrement.ID = dbo.FADecrementDetail.FADecrementID INNER JOIN
                         dbo.Type ON FADecrement.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON FADecrementDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.FixedAsset ON FADecrementDetail.FixedAssetID = dbo.FixedAsset.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = FADecrementDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = FADecrementDetail.ContractID LEFT JOIN
                         dbo.Department ON Department.ID = FADecrementDetail.DepartmentID

UNION ALL
/*Chứng từ nghiệp vụ khác*/ 
SELECT dbo.GOtherVoucher.ID, dbo.GOtherVoucherDetail.ID AS DetailID, dbo.GOtherVoucher.TypeID, dbo.Type.TypeName, dbo.GOtherVoucher.No, NULL AS InwardNo, NULL AS OutwardNo, 
                         dbo.GOtherVoucher.Date, dbo.GOtherVoucher.PostedDate, dbo.GOtherVoucherDetail.DebitAccount, dbo.GOtherVoucherDetail.CreditAccount, GOtherVoucherDetail.CurrencyID, 
                         dbo.GOtherVoucherDetail.AmountOriginal, dbo.GOtherVoucherDetail.Amount, NULL AS OrgPrice, dbo.GOtherVoucher.Reason, dbo.GOtherVoucherDetail.Description, dbo.GOtherVoucherDetail.CostSetID, 
                         CostSet.CostSetCode, dbo.GOtherVoucherDetail.ContractID, EMContract.Code AS ContractCode, dbo.GOtherVoucherDetail.DebitAccountingObjectID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode AS AccountingObjectCode, dbo.GOtherVoucherDetail.EmployeeID, 
                         dbo.GOtherVoucher.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.GOtherVoucher.Recorded, 
                         dbo.GOtherVoucher.TotalAmountOriginal, dbo.GOtherVoucher.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'GOtherVoucher' AS RefTable
FROM            dbo.GOtherVoucher INNER JOIN
                         dbo.GOtherVoucherDetail ON dbo.GOtherVoucher.ID = dbo.GOtherVoucherDetail.GOtherVoucherID INNER JOIN
                         dbo.Type ON GOtherVoucher.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON GOtherVoucherDetail.DebitAccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = GOtherVoucherDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = GOtherVoucherDetail.ContractID

UNION ALL
SELECT        dbo.GOtherVoucher.ID, dbo.GOtherVoucherDetailTax.ID AS DetailID, dbo.GOtherVoucher.TypeID, dbo.Type.TypeName, dbo.GOtherVoucher.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.GOtherVoucher.Date, 
                         dbo.GOtherVoucher.PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, GOtherVoucherDetailTax.CurrencyID, $ 0 AS AmountOriginal, $ 0 AS Amount, NULL AS OrgPrice, dbo.GOtherVoucher.Reason, 
                         dbo.GOtherVoucherDetailTax.Description, NULL AS CostSetID, NULL AS CostSetCode, NULL AS ContractID, NULL AS ContractCode, NULL AS AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL 
                         AccountingObjectCode, NULL AS EmployeeID, dbo.GOtherVoucher.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, NULL AS MaterialGoodsID, NULL AS MaterialGoodsName, NULL 
                         AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, dbo.GOtherVoucherDetailTax.VATAmountOriginal, 
                         dbo.GOtherVoucherDetailTax.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.GOtherVoucher.Recorded, dbo.GOtherVoucher.TotalAmountOriginal, dbo.GOtherVoucher.TotalAmount, NULL 
                         AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 1 AS DetailTax, NULL AS InwardAmountOriginal, NULL 
                         AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'GOtherVoucher' AS RefTable
FROM            dbo.GOtherVoucher INNER JOIN
                         dbo.GOtherVoucherDetailTax ON dbo.GOtherVoucher.ID = dbo.GOtherVoucherDetailTax.GOtherVoucherID INNER JOIN
                         dbo.Type ON GOtherVoucher.TypeID = dbo.Type.ID

UNION ALL
SELECT        dbo.RSInwardOutward.ID, dbo.RSInwardOutwardDetail.ID AS DetailID, dbo.RSInwardOutward.TypeID, dbo.Type.TypeName, dbo.RSInwardOutward.No, NULL AS InwardNo, NULL AS OutwardNo, 
                         dbo.RSInwardOutward.Date, dbo.RSInwardOutward.PostedDate, dbo.RSInwardOutwardDetail.DebitAccount, dbo.RSInwardOutwardDetail.CreditAccount, RSInwardOutward.CurrencyID, 
                         dbo.RSInwardOutwardDetail.AmountOriginal, dbo.RSInwardOutwardDetail.Amount, NULL AS OrgPrice, dbo.RSInwardOutward.Reason, dbo.RSInwardOutwardDetail.Description, 
                         dbo.RSInwardOutwardDetail.CostSetID, CostSet.CostSetCode, dbo.RSInwardOutwardDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, dbo.RSInwardOutwardDetail.EmployeeID, dbo.RSInwardOutward.BranchID, NULL 
                         AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID, dbo.Repository.RepositoryName, NULL 
                         AS DepartmentID, NULL AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, RSInwardOutwardDetail.Quantity, RSInwardOutwardDetail.UnitPrice, 
                         dbo.RSInwardOutward.Recorded, dbo.RSInwardOutward.TotalAmountOriginal, dbo.RSInwardOutward.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL 
                         AS OutwardAmount, 'RSInwardOutward' AS RefTable
FROM            dbo.RSInwardOutward INNER JOIN
                         dbo.RSInwardOutwardDetail ON dbo.RSInwardOutward.ID = dbo.RSInwardOutwardDetail.RSInwardOutwardID INNER JOIN
                         dbo.Type ON RSInwardOutward.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON RSInwardOutwardDetail.EmployeeID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON RSInwardOutwardDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON RSInwardOutwardDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = RSInwardOutwardDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = RSInwardOutwardDetail.ContractID

UNION ALL
SELECT        dbo.RSTransfer.ID, dbo.RSTransferDetail.ID AS DetailID, dbo.RSTransfer.TypeID, dbo.Type.TypeName, dbo.RSTransfer.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.RSTransfer.Date, 
                         dbo.RSTransfer.PostedDate, dbo.RSTransferDetail.DebitAccount, dbo.RSTransferDetail.CreditAccount, RSTransfer.CurrencyID, dbo.RSTransferDetail.AmountOriginal, dbo.RSTransferDetail.Amount, NULL 
                         AS OrgPrice, dbo.RSTransfer.Reason, dbo.RSTransferDetail.Description, dbo.RSTransferDetail.CostSetID, CostSet.CostSetCode, dbo.RSTransferDetail.ContractID, EMContract.Code AS ContractCode, NULL 
                         AccountingObjectID, NULL AS AccountingObjectCategoryName, NULL AS AccountingObjectCode, dbo.RSTransferDetail.EmployeeID, dbo.RSTransfer.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, 
                         dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, NULL AS VATAccount, NULL AS VATAmountOriginal, NULL AS VATAmount, RSTransferDetail.Quantity, RSTransferDetail.UnitPrice, dbo.RSTransfer.Recorded, 
                         dbo.RSTransfer.TotalAmountOriginal, dbo.RSTransfer.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL 
                         AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 
                         'RSTransfer' AS RefTable
FROM            dbo.RSTransfer INNER JOIN
                         dbo.RSTransferDetail ON dbo.RSTransfer.ID = dbo.RSTransferDetail.RSTransferID INNER JOIN
                         dbo.Type ON RSTransfer.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.MaterialGoods ON RSTransferDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON RSTransferDetail.ToRepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = RSTransferDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = RSTransferDetail.ContractID

UNION ALL
SELECT        dbo.PPInvoice.ID, dbo.PPInvoiceDetail.ID AS DetailID, dbo.PPInvoice.TypeID, dbo.Type.TypeName, dbo.PPInvoice.No, dbo.PPInvoice.InwardNo AS InwardNo, NULL AS OutwardNo, dbo.PPInvoice.Date, 
                         dbo.PPInvoice.PostedDate, dbo.PPInvoiceDetail.DebitAccount, dbo.PPInvoiceDetail.CreditAccount, PPInvoice.CurrencyID, dbo.PPInvoiceDetail.AmountOriginal, dbo.PPInvoiceDetail.Amount, NULL AS OrgPrice, 
                         dbo.PPInvoice.Reason, dbo.PPInvoiceDetail.Description, dbo.PPInvoiceDetail.CostSetID, CostSet.CostSetCode, dbo.PPInvoiceDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPInvoice.EmployeeID, 
                         dbo.PPInvoice.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, 
                         dbo.Repository.RepositoryName, dbo.PPInvoiceDetail.DepartmentID, NULL AS DepartmentName, PPInvoiceDetail.VATAccount, PPInvoiceDetail.VATAmountOriginal, PPInvoiceDetail.VATAmount, 
                         PPInvoiceDetail.Quantity, PPInvoiceDetail.UnitPrice, dbo.PPInvoice.Recorded, dbo.PPInvoice.TotalAmountOriginal, dbo.PPInvoice.TotalAmount, dbo.PPInvoiceDetail.DiscountAmountOriginal, 
                         dbo.PPInvoiceDetail.DiscountAmount, NULL AS DiscountAccount, dbo.PPInvoiceDetail.FreightAmountOriginal, dbo.PPInvoiceDetail.FreightAmount, 0 AS DetailTax, PPInvoiceDetail.InwardAmountOriginal, 
                         PPInvoiceDetail.InwardAmount, dbo.PPInvoice.StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPInvoice' AS RefTable
FROM            dbo.PPInvoice INNER JOIN
                         dbo.PPInvoiceDetail ON dbo.PPInvoice.ID = dbo.PPInvoiceDetail.PPInvoiceID INNER JOIN
                         dbo.Type ON PPInvoice.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPInvoiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPInvoiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON PPInvoiceDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPInvoiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPInvoiceDetail.ContractID

UNION ALL
SELECT        dbo.PPDiscountReturn.ID, dbo.PPDiscountReturnDetail.ID AS DetailID, dbo.PPDiscountReturn.TypeID, dbo.Type.TypeName, dbo.PPDiscountReturn.No, NULL AS InwardNo, 
                         dbo.PPDiscountReturn.OutwardNo AS OutwardNo, dbo.PPDiscountReturn.Date, dbo.PPDiscountReturn.PostedDate, dbo.PPDiscountReturnDetail.DebitAccount, dbo.PPDiscountReturnDetail.CreditAccount, 
                         PPDiscountReturn.CurrencyID, dbo.PPDiscountReturnDetail.AmountOriginal, dbo.PPDiscountReturnDetail.Amount, NULL AS OrgPrice, dbo.PPDiscountReturn.Reason, dbo.PPDiscountReturnDetail.Description, 
                         dbo.PPDiscountReturnDetail.CostSetID, CostSet.CostSetCode, dbo.PPDiscountReturnDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPDiscountReturn.EmployeeID, 
                         dbo.PPDiscountReturn.BrachID AS BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, 
                         dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, PPDiscountReturnDetail.VATAccount, PPDiscountReturnDetail.VATAmountOriginal, 
                         PPDiscountReturnDetail.VATAmount, PPDiscountReturnDetail.Quantity, PPDiscountReturnDetail.UnitPrice, dbo.PPDiscountReturn.Recorded, dbo.PPDiscountReturn.TotalAmountOriginal, 
                         dbo.PPDiscountReturn.TotalAmount, NULL AS DiscountAmountOriginal, NULL AS DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL 
                         AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPDiscountReturn' AS RefTable
FROM            dbo.PPDiscountReturn INNER JOIN
                         dbo.PPDiscountReturnDetail ON dbo.PPDiscountReturn.ID = dbo.PPDiscountReturnDetail.PPDiscountReturnID INNER JOIN
                         dbo.Type ON PPDiscountReturn.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPDiscountReturnDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPDiscountReturnDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON PPDiscountReturnDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPDiscountReturnDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPDiscountReturnDetail.ContractID

UNION ALL
SELECT        dbo.PPService.ID, dbo.PPServiceDetail.ID AS DetailID, dbo.PPService.TypeID, dbo.Type.TypeName, dbo.PPService.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.PPService.Date, dbo.PPService.PostedDate, 
                         dbo.PPServiceDetail.DebitAccount, dbo.PPServiceDetail.CreditAccount, PPService.CurrencyID, dbo.PPServiceDetail.AmountOriginal, dbo.PPServiceDetail.Amount, NULL AS OrgPrice, dbo.PPService.Reason, 
                         dbo.PPServiceDetail.Description, dbo.PPServiceDetail.CostSetID, CostSet.CostSetCode, dbo.PPServiceDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPService.EmployeeID, dbo.PPService.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, NULL AS RepositoryID, NULL AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, 
                         PPServiceDetail.VATAccount, PPServiceDetail.VATAmountOriginal, PPServiceDetail.VATAmount, NULL AS Quantity, NULL AS UnitPrice, dbo.PPService.Recorded, dbo.PPService.TotalAmountOriginal, 
                         dbo.PPService.TotalAmount, PPServiceDetail.DiscountAmountOriginal, PPServiceDetail.DiscountAmount, NULL AS DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL
                          AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL AS OutwardAmount, 'PPService' AS RefTable
FROM            dbo.PPService INNER JOIN
                         dbo.PPServiceDetail ON dbo.PPService.ID = dbo.PPServiceDetail.PPServiceID INNER JOIN
                         dbo.Type ON PPService.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPServiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPServiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPServiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPServiceDetail.ContractID

UNION ALL
SELECT        dbo.PPOrder.ID, dbo.PPOrderDetail.ID AS DetailID, dbo.PPOrder.TypeID, dbo.Type.TypeName, dbo.PPOrder.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.PPOrder.Date, 
                         dbo.PPOrder.DeliverDate AS PostedDate, dbo.PPOrderDetail.DebitAccount, dbo.PPOrderDetail.CreditAccount, PPOrder.CurrencyID, dbo.PPOrderDetail.AmountOriginal, dbo.PPOrderDetail.Amount, NULL 
                         AS OrgPrice, dbo.PPOrder.Reason, dbo.PPOrderDetail.Description, dbo.PPOrderDetail.CostSetID, CostSet.CostSetCode, dbo.PPOrderDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, PPOrder.EmployeeID, 
                         dbo.PPOrder.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, NULL AS RepositoryID, NULL 
                         AS RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, PPOrderDetail.VATAccount, PPOrderDetail.VATAmountOriginal, PPOrderDetail.VATAmount, NULL AS Quantity, NULL AS UnitPrice, 
                         dbo.PPOrder.Exported AS Recorded, dbo.PPOrder.TotalAmountOriginal, dbo.PPOrder.TotalAmount, PPOrderDetail.DiscountAmountOriginal, PPOrderDetail.DiscountAmount, NULL AS DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL 
                         AS OutwardAmount, 'PPOrder' AS RefTable
FROM            dbo.PPOrder INNER JOIN
                         dbo.PPOrderDetail ON dbo.PPOrder.ID = dbo.PPOrderDetail.PPOrderID INNER JOIN
                         dbo.Type ON PPOrder.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON PPOrderDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON PPOrderDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = PPOrderDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = PPOrderDetail.ContractID

UNION ALL
SELECT        dbo.SAInvoice.ID, dbo.SAInvoiceDetail.ID AS DetailID, dbo.SAInvoice.TypeID, dbo.Type.TypeName, dbo.SAInvoice.No, NULL AS InwardNo, dbo.SAInvoice.OutwardNo AS OutwardNo, dbo.SAInvoice.Date, 
                         dbo.SAInvoice.PostedDate, dbo.SAInvoiceDetail.DebitAccount, dbo.SAInvoiceDetail.CreditAccount, SAInvoice.CurrencyID, dbo.SAInvoiceDetail.AmountOriginal, dbo.SAInvoiceDetail.Amount, NULL AS OrgPrice, 
                         dbo.SAInvoice.Reason, dbo.SAInvoiceDetail.Description, dbo.SAInvoiceDetail.CostSetID, CostSet.CostSetCode, dbo.SAInvoiceDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAInvoice.EmployeeID, 
                         dbo.SAInvoice.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, 
                         dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, SAInvoiceDetail.VATAccount, SAInvoiceDetail.VATAmountOriginal, SAInvoiceDetail.VATAmount, SAInvoiceDetail.Quantity, 
                         SAInvoiceDetail.UnitPrice, dbo.SAInvoice.Recorded, dbo.SAInvoice.TotalAmountOriginal, dbo.SAInvoice.TotalAmount, dbo.SAInvoiceDetail.DiscountAmountOriginal, dbo.SAInvoiceDetail.DiscountAmount, 
                         dbo.SAInvoiceDetail.DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, 
                         dbo.SAInvoiceDetail.OWAmountOriginal AS OutwardAmountOriginal, dbo.SAInvoiceDetail.OWAmount AS OutwardAmount, 'SAInvoice' AS RefTable
FROM            dbo.SAInvoice INNER JOIN
                         dbo.SAInvoiceDetail ON dbo.SAInvoice.ID = dbo.SAInvoiceDetail.SAInvoiceID INNER JOIN
                         dbo.Type ON dbo.SAInvoice.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAInvoiceDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAInvoiceDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAInvoiceDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAInvoiceDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAInvoiceDetail.ContractID

UNION ALL
SELECT        dbo.SAReturn.ID, dbo.SAReturnDetail.ID AS DetailID, dbo.SAReturn.TypeID, dbo.Type.TypeName, dbo.SAReturn.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.SAReturn.Date, dbo.SAReturn.PostedDate, NULL 
                         AS DebitAccount, NULL AS CreditAccount, SAReturn.CurrencyID, dbo.SAReturnDetail.AmountOriginal, dbo.SAReturnDetail.Amount, NULL AS OrgPrice, dbo.SAReturn.Reason, dbo.SAReturnDetail.Description, 
                         dbo.SAReturnDetail.CostSetID, CostSet.CostSetCode, dbo.SAReturnDetail.ContractID, EMContract.Code AS ContractCode, dbo.AccountingObject.ID AS AccountingObjectID, 
                         dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAReturn.EmployeeID, dbo.SAReturn.BranchID, NULL AS FixedAssetID, NULL 
                         AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL 
                         AS DepartmentName, SAReturnDetail.VATAccount, SAReturnDetail.VATAmountOriginal, SAReturnDetail.VATAmount, SAReturnDetail.Quantity, SAReturnDetail.UnitPrice, SAReturn.Recorded AS Posted, 
                         dbo.SAReturn.TotalAmountOriginal, dbo.SAReturn.TotalAmount, dbo.SAReturnDetail.DiscountAmountOriginal, dbo.SAReturnDetail.DiscountAmount, dbo.SAReturnDetail.DiscountAccount, NULL 
                         AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL AS OutwardAmountOriginal, NULL 
                         AS OutwardAmount, 'SAReturn' AS RefTable
FROM            dbo.SAReturn INNER JOIN
                         dbo.SAReturnDetail ON dbo.SAReturn.ID = dbo.SAReturnDetail.SAReturnID INNER JOIN
                         dbo.Type ON dbo.SAReturn.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAReturnDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAReturnDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAReturnDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAReturnDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAReturnDetail.ContractID

UNION ALL
SELECT        dbo.SAOrder.ID, dbo.SAOrderDetail.ID AS DetailID, dbo.SAOrder.TypeID, dbo.Type.TypeName, dbo.SAOrder.No, NULL AS InwardNo, NULL AS OutwardNo, dbo.SAOrder.Date, 
                         dbo.SAOrder.DeliveDate AS PostedDate, NULL AS DebitAccount, NULL AS CreditAccount, SAOrder.CurrencyID, dbo.SAOrderDetail.AmountOriginal, dbo.SAOrderDetail.Amount, NULL AS OrgPrice, 
                         dbo.SAOrder.Reason, dbo.SAOrderDetail.Description, dbo.SAOrderDetail.CostSetID, CostSet.CostSetCode, dbo.SAOrderDetail.ContractID, EMContract.Code AS ContractCode, 
                         dbo.AccountingObject.ID AS AccountingObjectID, dbo.AccountingObject.AccountingObjectCategory AS AccountingObjectCategoryName, dbo.AccountingObject.AccountingObjectCode, SAOrder.EmployeeID, 
                         dbo.SAOrder.BranchID, NULL AS FixedAssetID, NULL AS FixedAssetName, dbo.MaterialGoods.ID AS MaterialGoodsID, dbo.MaterialGoods.MaterialGoodsName, dbo.Repository.ID AS RepositoryID, 
                         dbo.Repository.RepositoryName, NULL AS DepartmentID, NULL AS DepartmentName, SAOrderDetail.VATAccount, SAOrderDetail.VATAmountOriginal, SAOrderDetail.VATAmount, SAOrderDetail.Quantity, 
                         SAOrderDetail.UnitPrice, SAOrder.Exported AS Recorded, dbo.SAOrder.TotalAmountOriginal, dbo.SAOrder.TotalAmount, dbo.SAOrderDetail.DiscountAmountOriginal, dbo.SAOrderDetail.DiscountAmount, 
                         dbo.SAOrderDetail.DiscountAccount, NULL AS FreightAmountOriginal, NULL AS FreightAmount, 0 AS DetailTax, NULL AS InwardAmountOriginal, NULL AS InwardAmount, NULL AS StoredInRepository, NULL 
                         AS OutwardAmountOriginal, NULL AS OutwardAmount, 'SAOrder' AS RefTable
FROM            dbo.SAOrder INNER JOIN
                         dbo.SAOrderDetail ON dbo.SAOrder.ID = dbo.SAOrderDetail.SAOrderID INNER JOIN
                         dbo.Type ON dbo.SAOrder.TypeID = dbo.Type.ID LEFT JOIN
                         dbo.AccountingObject ON SAOrderDetail.AccountingObjectID = AccountingObject.ID LEFT JOIN
                         dbo.MaterialGoods ON SAOrderDetail.MaterialGoodsID = dbo.MaterialGoods.ID LEFT JOIN
                         dbo.Repository ON SAOrderDetail.RepositoryID = dbo.Repository.ID LEFT JOIN
                         dbo.CostSet ON CostSet.ID = SAOrderDetail.CostSetID LEFT JOIN
                         dbo.EMContract ON EMContract.ID = SAOrderDetail.ContractID
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'ViewVouchersCloseBook'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_DiagramPaneCount', 1, 'SCHEMA', N'dbo', 'VIEW', N'ViewVouchersCloseBook'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[CPUncompleteDetail]', N'tmp_devart_CPUncompleteDetail', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename table [dbo].[CPUncompleteDetail] to [dbo].[tmp_devart_CPUncompleteDetail]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[PK_CPUncompleteDetail]', N'tmp_devart_PK_CPUncompleteDetail', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename index [PK_CPUncompleteDetail] to [tmp_devart_PK_CPUncompleteDetail] on table [dbo].[CPUncompleteDetail]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[CPUncompleteDetail] (
  [ID] [uniqueidentifier] NOT NULL,
  [UncompleteType] [int] NULL,
  [MaterialGoodsID] [uniqueidentifier] NULL,
  [Quantity] [decimal](25, 10) NULL,
  [PercentComplete] [decimal](25, 10) NULL,
  [UnitPrice] [money] NULL,
  [CostSetID] [uniqueidentifier] NULL,
  [CPUncompleteID] [uniqueidentifier] NULL
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

INSERT [dbo].[CPUncompleteDetail](ID, CPUncompleteID, CostSetID, UnitPrice)
  SELECT ID, CPUncompleteID, CostSetID, GeneralExpensesAmount FROM [dbo].[tmp_devart_CPUncompleteDetail] WITH (NOLOCK)

if @@ERROR = 0
  DROP TABLE [dbo].[tmp_devart_CPUncompleteDetail]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[CPUncomplete]', N'tmp_devart_CPUncomplete', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename table [dbo].[CPUncomplete] to [dbo].[tmp_devart_CPUncomplete]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[PK_CPUncomplete]', N'tmp_devart_PK_CPUncomplete', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename index [PK_CPUncomplete] to [tmp_devart_PK_CPUncomplete] on table [dbo].[CPUncomplete]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[CPUncomplete] (
  [ID] [uniqueidentifier] NOT NULL,
  [CostSetID] [uniqueidentifier] NULL,
  [DirectMaterialAmount] [money] NULL,
  [DirectLaborAmount] [money] NULL,
  [GeneralExpensesAmount] [money] NULL,
  [TotalCostAmount] [money] NULL,
  [CPPeriodID] [uniqueidentifier] NULL
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

INSERT [dbo].[CPUncomplete](ID, CPPeriodID, CostSetID)
  SELECT ID, CPPeriodID, CostSetID FROM [dbo].[tmp_devart_CPUncomplete] WITH (NOLOCK)

if @@ERROR = 0
  DROP TABLE [dbo].[tmp_devart_CPUncomplete]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[CPUncomplete]
  ADD CONSTRAINT [PK_CPUncompleteDetail] PRIMARY KEY CLUSTERED ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[CPUncompleteDetail]
  ADD CONSTRAINT [PK_CPUncomplete] PRIMARY KEY CLUSTERED ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DISABLE TRIGGER ALL ON dbo.TemplateColumn
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

EXEC(N'INSERT dbo.Bank(ID, BankCode, BankName, BankNameRepresent, Address, Description, BankTransactionType, Icon, IsActive) VALUES (''a5bacac6-2844-4bfa-8050-7c0f51fa84c7'', N''MB'', N''Ngân hàng TMCP Quân đội '', N''Military Commercial Joint Stock Bank'', N''Dịch vọng'', N'''', 0, 0xFFD8FFE000104A46494600010101000000000000FFDB004300090607080706090807080A0A090B0D160F0D0C0C0D1B14151016201D2222201D1F1F2428342C242631271F1F2D3D2D3135373A3A3A232B3F443F384334393A37FFDB0043010A0A0A0D0C0D1A0F0F1A37251F253737373737373737373737373737373737373737373737373737373737373737373737373737373737373737373737373737FFC0001108005D007C03011100021101031101FFC4001C0000020203010100000000000000000000000604070203050108FFC4004610000103030104040A06060A03000000000102030400051106122131411351617107142232528191A1B1C115426272B2D12333637382C2165355839293A3C3D2E1243443FFC4001A010100030101010000000000000000000000010203040506FFC4003411000201030105060405050100000000000001020304112105122231511423415261711333A1F0324243B1C106248191D1E1FFDA000C03010002110311003F00BC680280F280280280F14A09495288000C924F0A0166E9E1074B5AFA4122EECB8A6CE1698E0BC527A8EC8383DF56DC973C1194274DF0C0B37342ADB6753D694FEB16EAB61F77B509E000EA571ECAD236F5251DEC1475629E065B6784FD39714AD4DFD20DA1B38716B84E14A0FDA29040F5D53E1B2FBC864B4DEAD7796CB96A9F1A5A071E85D0A23BC711557171E6826993F350485006680F680280280280280F2A00500BF7FD4C8B74916DB7C7370BB2D3B623215B29693E9BABE084FBCF206AD84A3BD2784356F08ADF555F586DD2D5F652AFF733BC5B23A8B5098FBC91BD5DEB249E49ACA956A95DEEDBC70BCCFF0082D384692CD57AF4122EF7376416DCBB3CDECB63FF001E1C76C36CB43EC36377F11DF5E9469C28ADEA8F2CE4739547882C2386BBABCA782D052948FA9C41EFAC9DD4DCB28BAA114B0752D7733E329936E96EC1B8246E5B4BC123A8FA43B0D6B254AE6387A328B7E8BCF3437C2D4906E2FA11A9E1B4C5C3706AE2C28B2A51EC7538524F62B23E15E5578DE59BCAE28F4677D295BD7D1F0B1E6DDA8AE7664E672DDBC5AD232B7C3604B8C9EB5A13B9D48F49201EB156B7B9A373A4749742B5684E8F3D5751E214C8F3E2B72A1BCDBD1DD48536E36ACA543AC1AD5A6B46666FA807A385480A00A00A00A03CA802B6B9D528B0C66E347911DA9F241287642B0DC66C79CEAFAC0E0071528802AF18E757C886FC0A9A66A07DC8AEC3B117E242717D249B848FFDA9ABE6B3E883CBA87002A5594AE25BF5BF0AE48ACAE5525887320582CB26F0762D080D43DA3D24F746524F3D9E6B57BBACD2EF69D0B38EE4398A16756E1EF4F90D7677ECBA7B5026C0CC471731E402E4E7549529C3B255BF7E40DC780C0DDD75F375DDC5D53F8F2969D0F569AA5467F09237B7AE2D0B891A59654861E9BE2854A481B1BB692B3F648C1EE3547615B79C73AA592EABC319C78E0872350D9F51066D722D4B7972482DA3C9053E5AD256143CDD9E8C9CF68EBAD216F5ADFBD53C63EF1FE4ABA94EAF038F33857ED3D2ECC971642AE16BE6E04E5D687DB48F387DA1EB15EE58ED88565B95B99E75CECF953E2A669B2DE27DA036EDBDD33608DE86B6FCB6FF0076AFE535A5DECB855EF68BC32B6F7F2A7C155650EDA73534585D2DDED4BDAB6956D5DA0253831C9E2FA11F571F5D23711E50ACE8549CFBAACB135F534AB08AE3A6F31FD8B55A710F34971A5A5685A4292A49C8503C08AB999B070A90140140140140276B0F0816ED3EB5C3889FA42E8067C59A5612D76B8AE091D9C7B2AD18393C22B2928F3290BD5D9774B93979BD3C87E539808284E10903825A49E43D23CF3D75D5150A6B2DE7EFC3FE983729BC225D9DDB0482991A82E4DF47C530509594FF0078AC7947B3877D7977D757753828C708EDB5B7A10E2A8F51D11AD74E3684A1AB8B6942461294B4B000EA1BABC176172DE5C4F515CD15E2702ED37495CAE2ECE7AF6FB6EB8809C34080084A9208F2339C295EDAECA30BBA50DC5058FBF5319CADE72DEDE3521CD12D48438CCE52194E0AA36CA8B6B210A464E539F356471E42ACFB735871D7A955D9B394CC21BBA420BD1DD8D78792F46090D2F0720052C91E66F076D40FAB98CD27DB269A70D1931ECF1C352184EB4B0FF68A7FCB5FE55C5D82E3CA7476AA3D456BCBFA79D75732C9736A2CA51CADA2DAFA177BC63C93DA2BD6B2AD796EF12594705CD2B6ACB29EA42B64F664CA44984F185736C1095A71E50E637EE5A4FA26BDECD3B8E7A48F27BCA3CB54589E0D759B7698C9B0EA471118070883270432507786CA8F9B839C6796072AE6AF4E517968E8A752335A16C2482010722B1343DA00A00A00A0287D6FA26E2D6B511A127A3B5DDA6254DB840286DC5ED17329E2BD9D85AB1C380E7578C9A8BE8B528E29BCF89CB95A7A2C4977F82ADA90E36D214D3EFE0AF05BCF2E1E503C3B072A9D9F515D5BBA925A99DE2742AC631E42BF8E3121A61AFA263B40ADBDA7909DE4050CF2E7DFCEAFF114925BB8E5A8DC69B7BC75B5278B5BAF8766D91E4A1D8E93D1A903092144646E3CB15BD7DDA753F0E7431A1BD3A7ACB1A995890C4D857B5'
+ N'78934C1534025B4A7CCF24E31BBAC66A6928CD4DE304567284A1A9CA62E51C30DA55A7E2BAA0800ACA079671C7CDE758C6AC70B80DA54DE5F19D1088FFD0B7E578AB6952E56D8484F9BFA4000F66EAD311ECEE58F1FE4CF2FB428E7C08B15F8D70B8428A2CD1A285484ED290800903794F9A37551494E718EE6352CD4A1072DECE86FBCC88B02FB3102D51A4A4846E5246107677E371E39156AB28C2ABE1C95A4A53A4B8B066C448AFE9876598ADA1E912C746529C16F2E04800F50DF55928AB7954C6A4A72770A19D0ECCED3F721B104BC9916D952588AA94B203AC25C584E543EB7564733BEB8AD368CAE69BA6D6A76DC584684D4D3D0FA061C66A1C56634740432CA036DA0704A40C01572A6EA00A00A00A01475702BD4FA6D3B584B699AF63AD419D91F8CD675E5BB6D51FA16A7AD48AF5106FC8E8B59BD9C624DBDB3EB4AD40FE2155FE9D9668CA267B623C719089D21469B5B7CD942938FB8AFFAAF5D3FEDFD8E4C77CBD4EDDE9CD9BC40741FD646713EF49AD5BEF63EA8C69AEEE4BD4F2CEADA9B771C4A996FE0A150BE64D7A09FE187B9C7B6DEE2B102334B717B4869292020F102B2A5734E30499B4E84DC9B252140E8C655FD64CDAFF5C9F9513EE57BFF00257F5DFB7F04A4A8BD7CB5A33B92E38B3EA6C8F89ADE7AD482335A53918A5ED8B95D9DCF191F850915107ACE5EA4B5C305E86D80DE6CDA763E705F96D2C93DEA73E55C3792DCB13AAD23BD7BEC385E36BE83BAABA5DED3087D253C94DBCDAC7C2BE7B64BFEE31D51EE6D0F949FA96E8DE2BD93CB3DA00A00A00A014758A548D45A65FCE10B764C5577B8C28A7DED8AA558EFDBD48FA13078A91621EBC1D05EED32F1B951A4367B48D9581EE35C3FD3D3C39236DAB0CC5083213B3F49C73C03CEFB0EFF009D7D1A5C138FB9E5FE68CBD8E85D5CDB8F63919FA8539EF403F2AB675A6FEF9148AFC68D9A7D79BACDFB51D1F157E7568FCD97B1153E5C7DC4940C240EA18AF24F446D51D8D276C4F5BE8FC4A35E847E4C17A9C3FAD2F63282EE6F6C9F4585FB54A4A7E75ACDF7ABD8AE3BB7EE4279F223DCDD27CE7DF20F66D102A89E294E5EE5F1C515EC325B51B577B1C54A368476DC5ECFDD6C27976AABCEDAF2DDB5513AF6647370E433DCD0A5DA27348484AE49622007ADD7DB4FC36ABC6D911CD772E88F57683EED2F52DC1BABD83CC3DA00A00A00A016BC204575ED38E4B88DEDCAB63ADCF6120649534ADA2077A7693EBABD37C587E243E42078470D488567991941719724746A1CDB75A563E22BC9D994DD0BE9D26755DC954B6522B57DE0B96EBA4E3A76DA7707B5001F78AFA4A7259967C71FB1E5B4F0BD3FE92E52B6B4D5A560FEADC4027B36549A8FD3A6CAAF9B35D4DDA71E06EEE9C8DF1C7B943F3AD62D3AAF1D0A555DDAF7155C4EC38B4FA2A22BCB7CD9DEB90CD315B1A72D033B8B8827FC2A35DEBE553F738D7CD99AADCB0AB8839E6CA463F7815FCB532966A7FA18E02021C1222211CE438063EF2EAB9EE7DDFF0025F189E7A0DDA59424DF7A5512422238E023F68F123DC9AF1F6D54EEA2BAFF00E9E86CB8779263DDB2278F5FACB071B496DD55D247621B050C83DEE2D4AFE0ACF665374E84AA3FCDA1ADF54DEA8A2BC0B3070AED390280280280280C558C60F3EBA8050FA86D32917BB8E9E1767936B82EB6EC3612DA08682BF48120E33E49240DFC2BA68DB42B54F8BCA4BC4E7AF712A51DCE699120E9CB7B0D912D099AE1C796F206E03901C857742DA09716A714EE26DF0E809D3D103894ADE75C868595A22280D804E79F12379DD4ECD1E4DE9D03B8963296BD4CDFB0C2DA4B903301D4820AE3A40DA07910460D4BB7873868C88D79FE6D4C99B0DA1A6928F1165C2918DB713952BB49EBA956F492E41D7AAFC4D2D69F8697121E71D9119BCF4519DC14233EAC9ECCD555BC34CBCA5E059D79E34587D4C2669D82EB88722154129E3D02400AF575EF3BEA256D06F31D0985C4D692D4D8E69FB518C196E325A580025E4001608E073D752EDA96EE122157ABBD96C8222C9D3915D9912E2E2B01B43882CA4ED212AC606787135C375B3E9CE199BCE0ECB6BD9C6788AC64BB3455B5E623C8BACF6BA29B7252565A3C63B291869AF52779FB4A5560D4629423C91D196DB93F119C70A8014014014014054FE182F96EB9259D36969F7A5C7B84776427A0516C3641CE55C382BE35A528EF4969A14A92C45EA21B53AC105A7988921A8DD28C28B24E73D638EFAEF53A104D45E0E170AD269B592287ED2BC9FA4AE8EF73CEE3DC2A99A5E67F534C54F2A334B96B1BC26ECEFADF34EEBD7EA462AFA7D0CD02D8ADE2DF705FDE43BF33564A9F95FD4ABF89E65F4362442E567947BD1F9AAAD88791919979919E237016573D686FE6AA9C47C9F7FEC8CCBCE6484B09E16609EF435FF2AB24BC9FB7FD235F31B92A6F95B4247DD6FF003AB2C797F62AD3F31B161A90CA98763FE89630A41C60FB0D59A52586884DC5E5337693BCC1D0F7B957116A7DC88EC30D1F1509F255B79C9DA50E58AF3EE68EEBCC56876D0AB95893D4BF927201AE43A4F680280280280A5F5ADBAEFF00D38BBBD16CF7094CBE1928723B0549386C03BF871AEAB7AD1A69E4E7AF49D4C60E5B361D4AA03A0D2B386786D7448F8AB756DDAE9AE48CBB34DF364C6F4A6B174793610D9FDACD687C09A8ED8BC113D93D42568ED6CCC753C8B5C370A7FF00937376967B81007BEA15EBCF227B2AEA71D116E18099F3ED76A7F8162E49799503D8549093EA3513B9AC96631CFB168DB537CDE0E835A62EEFA76D17DB1941FACD282FFD'
+ N'CAE396D1BA5FA4FE86EACA878CFF00734C8B1BD155B33B57595827827092A3DC90B24FB2A61797B2E54F1FE43B4B65F9B266C694BDDC425363972A5951DF21EB778AC748EBDA59DA57F0A4D74AAF552E3697D4C5D0A79E143331E0A6E6A653E31A9D21D23CA088092907B0950AA76AA9D49ECF4FA1EAFC145C824F47AA1B51E4176E18F72E9DAAA0ECF4FA102E3E0A750BB15C65ABCDB5E0B182171D6D1F682AF8544EE6738EEB26342317945C28DC903A85606C654014014014014014014014060EB4DBC82875095A0F14AD390680E63BA62C0F2F6DDB25B56AEB54441F955B7E5D48DD5D0930ED16D838F1281123E370E8584A71EC150DB7CC61136A090A00A00A00A00A00A03FFFD9, 1)')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

SET IDENTITY_INSERT dbo.ContractState ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.ContractState(ID, ContractStateCode, ContractStateName, Description, IsSecurity) VALUES (1, N'1', N'Chưa thực hiện', N'Chưa thực hiện', 0)
INSERT dbo.ContractState(ID, ContractStateCode, ContractStateName, Description, IsSecurity) VALUES (2, N'2', N'Đang thực hiện', N'Đang thực hiện', 0)
INSERT dbo.ContractState(ID, ContractStateCode, ContractStateName, Description, IsSecurity) VALUES (3, N'3', N'Tạm ngừng thực hiện', N'Tạm ngừng thực hiện', 0)
INSERT dbo.ContractState(ID, ContractStateCode, ContractStateName, Description, IsSecurity) VALUES (4, N'4', N'Đình chỉ', N'Đình chỉ', 0)
INSERT dbo.ContractState(ID, ContractStateCode, ContractStateName, Description, IsSecurity) VALUES (5, N'5', N'Hủy bỏ', N'Hủy bỏ', 0)
INSERT dbo.ContractState(ID, ContractStateCode, ContractStateName, Description, IsSecurity) VALUES (6, N'6', N'Hoàn thành chưa thanh lý', N'Hoàn thành chưa thanh lý', 0)
INSERT dbo.ContractState(ID, ContractStateCode, ContractStateName, Description, IsSecurity) VALUES (7, N'7', N'Thanh lý', N'Thanh lý', 0)
INSERT dbo.ContractState(ID, ContractStateCode, ContractStateName, Description, IsSecurity) VALUES (9, N'8', N'Kết thúc', N'Kết thúc', 0)
GO
SET IDENTITY_INSERT dbo.ContractState OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DBCC CHECKIDENT('dbo.ContractState', RESEED, 10)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.Sys_User SET FullName = N'ADMIN', Email = N'', WorkPhone = N'', HomePhone = N'', MobilePhone = N'', Fax = N'', BirthDay = NULL WHERE userid = '5c2f6d39-e0c4-4d3e-8ab1-e00d51867ccc'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.Sys_UserRole WHERE ID = '53aea629-7eef-49b5-aff0-addbd3593eef'

INSERT dbo.Sys_UserRole(ID, UserID, RoleID, BranchID) VALUES ('2c79598a-c285-4d9e-aaec-fd31d1947ceb', '5c2f6d39-e0c4-4d3e-8ab1-e00d51867ccc', 'b7e767cb-2731-434c-a513-61ed7497db6f', '00000000-0000-0000-0000-000000000000')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1207e407-4b08-4f18-b31b-4d29fbbb4427'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAllOriginal' WHERE ID = 'a1aeba99-8807-4d74-880a-6c5b829bfa4f'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAll' WHERE ID = '116c5b52-ea3a-423b-a4f0-72a051c9e194'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'a0b31b11-6661-4cf6-9056-96be53edd92d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 4 WHERE ID = '8d7385e9-07c6-4a2a-8327-bce7e512bd37'
UPDATE dbo.TemplateColumn SET VisiblePosition = 6 WHERE ID = '1c054e5a-7095-4810-8f97-d948f0bdfd20'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '7cd61a32-14dc-4a53-85c5-da69e7be11a5'
UPDATE dbo.TemplateColumn SET VisiblePosition = 3 WHERE ID = '0c9c0267-bbe0-4bf9-8d85-dcf9c7327899'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '36a0db25-be70-4d21-9c19-f44a89bfe2d6'
UPDATE dbo.TemplateColumn SET VisiblePosition = 5 WHERE ID = '3d5b9028-defc-43f6-a491-fae0884869de'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.TT153InvoiceTemplate(ID, InvoiceTempName, InvoiceTempPath, InvoiceType) VALUES ('28870782-2ab6-4ee0-a9a2-34d23a69a52a', N'Phiếu xuất kho hàng gửi bán đại lý', N'TT153-PXK-GBDL.rst', 0)
INSERT dbo.TT153InvoiceTemplate(ID, InvoiceTempName, InvoiceTempPath, InvoiceType) VALUES ('ca771f4e-5040-4000-a4fd-5b20e4b3c0fb', N'Hóa đơn GTGT (3 liên, A4, song ngữ)', N'TT153-HD-GTGT-English.rst', 0)
INSERT dbo.TT153InvoiceTemplate(ID, InvoiceTempName, InvoiceTempPath, InvoiceType) VALUES ('fe714ac6-4bc7-4bd5-9197-62b8ba2f0159', N'Hóa đơn bán hàng (3 liên, A4, song ngữ)', N'TT153-HD-BHTT-English.rst', 0)
INSERT dbo.TT153InvoiceTemplate(ID, InvoiceTempName, InvoiceTempPath, InvoiceType) VALUES ('606b721b-d13d-45af-9df3-81dad43920fb', N'Hóa đơn bán hàng (3 liên, A4)', N'TT153-HD-BHTT.rst', 0)
INSERT dbo.TT153InvoiceTemplate(ID, InvoiceTempName, InvoiceTempPath, InvoiceType) VALUES ('67625ecf-dcb9-4eec-ac1c-8c01851aa615', N'Phiếu xuất kho kiêm vận chuyển hàng nội bộ', N'TT153-PXK-VCNB.rst', 0)
INSERT dbo.TT153InvoiceTemplate(ID, InvoiceTempName, InvoiceTempPath, InvoiceType) VALUES ('5fc90deb-ee1e-4386-af3a-c2c157b1985b', N'Hóa đơn GTGT (3 liên, A4)', N'TT153-HD-GTGT.rst', 0)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.VoucherPatternsReport WHERE ID = 135

UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'' WHERE ID = 110
UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'340' WHERE ID = 151
UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'340' WHERE ID = 153
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

GO
ENABLE TRIGGER ALL ON dbo.TemplateColumn
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF
GO
