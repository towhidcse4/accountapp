
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO




BEGIN TRANSACTION




ALTER TABLE dbo.GenCode
  DROP CONSTRAINT FK_GenCode_TypeGroup
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END




ALTER TABLE dbo.Template
  DROP CONSTRAINT FK_Template_Type
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END




ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END




ALTER TABLE dbo.TemplateDetail
  DROP CONSTRAINT FK_TemplateDetail_Template
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = '86115ade-d36b-4acb-b0d3-00673b4299f3'
UPDATE dbo.TemplateColumn SET IsVisible = 0, VisiblePosition = 15 WHERE ID = '2432bef2-ad83-401f-b9e6-00ae5af45279'
UPDATE dbo.TemplateColumn SET VisiblePosition = 19 WHERE ID = 'a606471e-3c65-4229-94e5-0195790bb0b3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 34 WHERE ID = '75d97382-5968-4215-b52a-034e09ecc234'
UPDATE dbo.TemplateColumn SET VisiblePosition = 47 WHERE ID = '2b8adfd4-37b9-4113-b534-070f9118a85c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = 'de13e6db-5418-4fb6-b82d-07dca7f1e586'
UPDATE dbo.TemplateColumn SET VisiblePosition = 40 WHERE ID = '9fc72a41-111e-4db5-8cfb-0a529de8469b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = 'd3778e18-463a-4272-bec9-0fd44edc5760'
UPDATE dbo.TemplateColumn SET VisiblePosition = 30 WHERE ID = '9c5fc359-b343-44c8-9a51-17097ffc5f3f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 45 WHERE ID = '3751d19b-cc3a-45f6-bd4a-17b37b9caec2'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = 'aa48b05b-3710-4546-b505-19c89c47f3f2'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = '46de8e54-3469-4fdf-aaf0-1b47f553d5c8'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = '0e0bdfdc-3644-49e5-ace3-1c91de5b4e33'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '982550c7-8936-4a66-a4a4-21285e80d747'
UPDATE dbo.TemplateColumn SET VisiblePosition = 26 WHERE ID = 'b9851338-3bdc-4c49-9e20-230e26c88130'
UPDATE dbo.TemplateColumn SET VisiblePosition = 23 WHERE ID = '4a17a989-8866-4fc3-8b22-260851af4f5a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 49 WHERE ID = 'e2bf4ed2-1aa4-4761-ae0d-2e06627add3b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 47 WHERE ID = '6f2c4ca0-e07b-4777-b09a-2ebeb4b27f80'
UPDATE dbo.TemplateColumn SET ColumnWidth = 130, VisiblePosition = 6 WHERE ID = 'e7db317d-003e-4520-ae26-32b1ab8d1c39'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = 'f2dd105b-f4c1-407b-adfa-331c79bd8c1e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = 'd9da5c27-5da4-4e45-8eb5-347a3db8c495'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = '00d0dd8b-b970-4203-8d1f-34c3449abd98'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = '8802a23a-8dc9-438e-acb6-37050c2652d3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 35 WHERE ID = '9d50aa12-bfe6-4573-a7da-377e66b53dc4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 25 WHERE ID = '5599d37d-ca27-49f9-b2e1-37cb9bb82acc'
UPDATE dbo.TemplateColumn SET VisiblePosition = 9 WHERE ID = 'fb42e083-5cc0-464a-bed1-3dfa060b1bcc'
UPDATE dbo.TemplateColumn SET VisiblePosition = 42 WHERE ID = 'cec1d0d5-1d83-4917-9850-3e254cc2754f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 37 WHERE ID = 'bc4de4b7-2704-436f-af0b-3ec9f79cb33d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 27 WHERE ID = 'cfe456d5-7441-4593-9631-438654200b22'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = 'e7aceb82-a713-44a8-82b8-43fc2fb20956'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'a715c3f0-e422-42df-9bfd-45c0331e14d5'
UPDATE dbo.TemplateColumn SET VisiblePosition = 30 WHERE ID = '860cface-b2ac-4bf8-8e1f-479d37a14a92'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '431866d4-166a-44fb-91db-48a399cbee7b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 29 WHERE ID = '1af2e029-6360-48b8-879c-4c214eb09bc1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 25 WHERE ID = '4ccd2c13-2b3b-4327-9d29-4e705ef5ce6c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 32 WHERE ID = '9cff17cc-a201-479e-98a5-4e80adde96b6'
UPDATE dbo.TemplateColumn SET VisiblePosition = 27 WHERE ID = 'e3024b2e-3e6b-49ce-953c-4fa0aa3b5858'
UPDATE dbo.TemplateColumn SET VisiblePosition = 22 WHERE ID = '563508d7-2c01-456f-adb9-500f71603e3c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = 'ce7848a1-f383-4945-8fa3-520a3fad2230'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = 'cd8f7a3b-8ea5-4835-beca-538bee28a701'
UPDATE dbo.TemplateColumn SET VisiblePosition = 7 WHERE ID = '4f7e1291-4231-4212-b723-53f36b797da1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = 'f0e9ec9b-19e5-4858-8d84-55ff367e5553'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '388dfe6a-c613-40bd-aa46-58e2069aa902'
UPDATE dbo.TemplateColumn SET VisiblePosition = 40 WHERE ID = 'be69d000-1288-4a9b-98a0-58eb7648cc60'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'ae4cef7b-67ff-4afc-97d3-59be70c61456'
UPDATE dbo.TemplateColumn SET VisiblePosition = 41 WHERE ID = '915ee824-462d-4fd0-96d0-59e09c9649a7'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = 'b26c9182-e657-43ff-856f-5a251542f73e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = '954288d9-0820-4bb5-b653-5adf38c36ba1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 5 WHERE ID = 'be85bf54-ee9b-469b-9b10-5ae99dcf7184'
UPDATE dbo.TemplateColumn SET VisiblePosition = 47 WHERE ID = '2ac09628-c69a-4379-b646-5b435968b31b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 37 WHERE ID = '6feb7ce5-7d33-4a6c-bd82-5b7e5e887e08'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9a61d5fe-e5fa-4776-bb8b-5c1a37a29f97'
UPDATE dbo.TemplateColumn SET VisiblePosition = 15 WHERE ID = '67051ba1-f181-4d2f-a6c9-5d7216f60943'
UPDATE dbo.TemplateColumn SET VisiblePosition = 51 WHERE ID = '2e101d30-05bc-4b34-b16c-5e6eaab6f2f6'
UPDATE dbo.TemplateColumn SET VisiblePosition = 46 WHERE ID = 'ccc598ef-5fbb-47b8-9cd4-5e926b2e220a'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'b126320b-b684-41a8-98be-5f72e6c6aacc'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = '47d57fd5-f1ef-4ce2-9f33-60cf252530fc'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Hiệu lực đến' WHERE ID = '427a90df-5e3c-44e7-a742-628c29572448'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = '0df33157-3be3-4375-adea-67433ba2d7bb'
UPDATE dbo.TemplateColumn SET VisiblePosition = 9 WHERE ID = 'c46d5470-8839-4dff-ae9e-69ec02968295'
UPDATE dbo.TemplateColumn SET VisiblePosition = 1 WHERE ID = 'b0797ccb-b814-435c-9111-721fdba647b6'
UPDATE dbo.TemplateColumn SET VisiblePosition = 41 WHERE ID = '69edb07f-f077-4faa-9c10-724c9314fa18'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = 'ed534220-ca55-484a-bdc8-743a83f78f1c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 39 WHERE ID = '8dc03db4-733c-4ad1-bbcf-74c0210b0c53'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = 'd25e59e0-9712-4e8d-bfd5-754b225c97c6'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = 'a92478e1-575b-4a74-b03a-75aea7b1d55d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = 'd19df6b0-baaa-4ca4-b52a-7693b8749a74'
UPDATE dbo.TemplateColumn SET VisiblePosition = 15 WHERE ID = '7c464ede-c5c9-4ef3-85a8-7a325c359cbf'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = '9848654a-bc7e-443f-a0c7-7d2f98e6d67a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 38 WHERE ID = '507ee1b0-6b19-4808-b881-8316c57fa315'
UPDATE dbo.TemplateColumn SET ColumnWidth = 130, VisiblePosition = 7 WHERE ID = '8aa21c84-4677-4078-ae0f-8394e4976e6e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 31 WHERE ID = 'd3c7c1fa-1a21-4098-b336-844c383bdfd7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = '237fbd9e-94af-4b29-ac67-849bdb382f56'
UPDATE dbo.TemplateColumn SET IsVisible = 0, VisiblePosition = 5 WHERE ID = 'f9c7ef57-c5af-416d-90bf-85573934d452'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = 'ebcb8be0-5d06-4bc0-a33d-8705d5eb217d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 48 WHERE ID = 'c72a0cac-5ebe-466e-bce9-88296737846c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 50 WHERE ID = '634079f8-364d-4f78-b542-8bab22445d81'
UPDATE dbo.TemplateColumn SET VisiblePosition = 43 WHERE ID = 'dd67c842-a727-4d20-bb7f-8cf13c647d63'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = '20f751e0-5c7a-419a-b204-8e409b7764da'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'f96eca0c-5169-4c9c-8c5a-9017988f67d0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = '6acea1cf-4d87-4807-96a6-9454059226f9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 19 WHERE ID = '8d81d49f-733e-432c-b598-954815f3cb4d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 34 WHERE ID = '23277013-ff04-45bf-8297-96c8c01c57b0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 22 WHERE ID = '91af5295-ea44-47d1-bc65-96d99a92a98a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 35 WHERE ID = '33835079-6883-4d69-b4a7-9be6c4568812'
UPDATE dbo.TemplateColumn SET VisiblePosition = 20 WHERE ID = '2b2cf61e-4c27-40bf-9896-9c8e7e8fb63a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = '159d5eef-f7da-45c9-98b2-9d0699ded452'
UPDATE dbo.TemplateColumn SET ColumnName = N'TransportMethodName' WHERE ID = '08633f40-8f8e-4629-b55c-9d0e7e101ee5'
UPDATE dbo.TemplateColumn SET VisiblePosition = 44 WHERE ID = '453861e1-1644-4085-bbc5-9f0fd56a3f0d'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '278950b5-0e49-4a0e-9cac-a11c92659dd5'
UPDATE dbo.TemplateColumn SET VisiblePosition = 26 WHERE ID = '056a76ab-571a-48f6-accc-a270c373af8f'
UPDATE dbo.TemplateColumn SET IsVisible = 1, VisiblePosition = 18 WHERE ID = 'c38165d1-5165-48bb-b5c8-a3227e6e0bde'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = 'e90c83d6-e94a-4e12-bffb-a4783da4db26'
UPDATE dbo.TemplateColumn SET VisiblePosition = 42 WHERE ID = '6fc10602-d5bc-4218-8b11-a521e01b194e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 3 WHERE ID = 'd365d710-e869-4837-a7ec-a6a7d06ff253'
UPDATE dbo.TemplateColumn SET VisiblePosition = 24 WHERE ID = '16323890-d6ee-4cdf-9c47-a6f3743e8af7'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '71e613e3-fa43-4ba2-87ee-a74670100b52'
UPDATE dbo.TemplateColumn SET VisiblePosition = 52 WHERE ID = 'ceb17871-1712-481e-8fbd-aa7db412b124'
UPDATE dbo.TemplateColumn SET VisiblePosition = 44 WHERE ID = 'e7932497-edda-4c2c-bed9-aca05236c92d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 33 WHERE ID = '193ed4dd-16a8-4c49-961e-acb98a518bcb'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Số kỳ phân bổ còn lại trước ĐC (tháng)', ColumnToolTip = N'Số kỳ phân bổ còn lại trước ĐC (tháng)', VisiblePosition = 9 WHERE ID = '35e3e013-fef3-4ad6-8946-aedbd408a16a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 6 WHERE ID = '60a0a73e-690e-4db1-9cdf-aee29456478f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 29 WHERE ID = '07b88b8f-3b2a-4f50-85d7-b0d3c5aef24c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 28 WHERE ID = 'c2ed29a8-e5b6-403c-84b6-b70387172bee'
UPDATE dbo.TemplateColumn SET VisiblePosition = 54 WHERE ID = '3911838c-856d-419c-8305-b8a20615ec45'
UPDATE dbo.TemplateColumn SET VisiblePosition = 8 WHERE ID = '4d78bf75-a0af-4b9f-8af0-b9ae36062259'
UPDATE dbo.TemplateColumn SET IsVisible = 1, VisiblePosition = 18 WHERE ID = 'de06f3c1-cf9f-4b0d-b0e3-ba410d70a0a4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 19 WHERE ID = 'beb33586-b4fe-453d-8496-ba7e7e35225c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = 'e42c68f7-bfd6-4716-9579-babc9ed1706f'
UPDATE dbo.TemplateColumn SET ColumnWidth = 120, VisiblePosition = 0 WHERE ID = 'c0357192-081e-4879-b044-baefdbdf55ef'
UPDATE dbo.TemplateColumn SET VisiblePosition = 43 WHERE ID = '0c211c73-74b0-4e6b-9dcb-bb4d8e43ebd4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = '63f5fe2f-2cab-4dc8-ae1e-c0a2aa431b39'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '94838bf1-01ea-4ebb-b5e0-c188ed268048'
UPDATE dbo.TemplateColumn SET VisiblePosition = 49 WHERE ID = '90077261-f9cf-4e97-abae-c2f9c7394d18'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = 'c7bfecce-6124-4fde-92d7-c4011ee40e01'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'TK chờ PB', ColumnToolTip = N'Tài khoản chờ phân bố', VisiblePosition = 4 WHERE ID = 'c7b78338-e4af-41e0-825c-c4dcf76ed31a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 49 WHERE ID = 'b974fc94-9fb8-4e83-a4a9-c502960a79f4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 48 WHERE ID = 'd04edc6d-2987-452c-85c5-c6a36687068e'
UPDATE dbo.TemplateColumn SET ColumnWidth = 150, VisiblePosition = 5 WHERE ID = '0fe6d7da-47f3-4681-829c-ca002dc2529f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 38 WHERE ID = '2500f696-2f1e-4894-bebc-cb475645a5f4'
UPDATE dbo.TemplateColumn SET ColumnToolTip = N'Tài khoản hao mòn' WHERE ID = '9dae84ab-912c-4c79-88cf-d1edd79de737'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '50ae044f-f65a-4127-8e28-d41b2b192723'
UPDATE dbo.TemplateColumn SET VisiblePosition = 23 WHERE ID = '8c8d05fd-b0b1-4d69-857d-d6291b96dcc2'
UPDATE dbo.TemplateColumn SET VisiblePosition = 53 WHERE ID = '4793c06b-e117-46c9-8343-d756e457354a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 54 WHERE ID = 'f9be9e03-7f3a-446e-b2f4-d7a6ed508b12'
UPDATE dbo.TemplateColumn SET VisiblePosition = 53 WHERE ID = 'b54e5578-663a-4bfc-bd09-d7d60a1851a9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 28 WHERE ID = '504a6e8f-abe8-4cf8-b139-d82510c6ad15'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 0, ColumnWidth = 100, VisiblePosition = 8 WHERE ID = '00cc78b3-d41b-493a-9d38-d8372b6dbad0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = '97d8d877-e4e1-49f0-b11b-d9ba086d8629'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = '380828d9-429d-48b0-9a31-da1fc2058977'
UPDATE dbo.TemplateColumn SET VisiblePosition = 15 WHERE ID = 'ce7539b4-0b95-46f5-ba20-db1aaab7c601'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAllAmount' WHERE ID = '46d31c52-66b7-4c42-ad9f-de71fa1e3281'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '2fee27be-1f8c-497d-b49c-e0f9c171b720'
UPDATE dbo.TemplateColumn SET VisiblePosition = 50 WHERE ID = '12ac3de1-e56e-474e-8665-e2dee7aa4f05'
UPDATE dbo.TemplateColumn SET VisiblePosition = 0 WHERE ID = '62288350-8dba-42ae-93fa-e334c90c53e5'
UPDATE dbo.TemplateColumn SET VisiblePosition = 2 WHERE ID = '1ed5f7a2-b544-4b4e-8cbf-e465dd05373a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 24 WHERE ID = '22402633-369c-4d5e-94fb-e4ff1195989b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Số tiền còn lại sau ĐC', ColumnToolTip = N'Số tiền còn lại sau điều chỉnh', VisiblePosition = 7 WHERE ID = '30e29beb-490a-4df4-8235-e5182cdb8b21'
UPDATE dbo.TemplateColumn SET VisiblePosition = 4 WHERE ID = 'e694f3e3-b588-43ad-a134-e54472bddfb4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 48 WHERE ID = '972e547e-cb1a-461f-9b9d-e839bca07fb3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 39 WHERE ID = 'f6b8ff00-7255-4e5a-be28-eabb4939e3b7'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = '3248be20-6947-4e6d-bd06-ebf16a4f920a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 32 WHERE ID = 'f383d439-5750-4e2b-9c86-ed3c3389a127'
UPDATE dbo.TemplateColumn SET VisiblePosition = 33 WHERE ID = '27d7acf9-80e5-4566-a375-ee98081e547b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = 'f34b1627-f2d4-4d2f-8e35-eee544db9e9b'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '523b533d-f2e7-48e0-ad7e-efba63694198'
UPDATE dbo.TemplateColumn SET VisiblePosition = 52 WHERE ID = 'ced4c3fa-bfc7-485d-8442-f0e6c735d7dc'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '853e6ab5-d80f-4fd4-a181-f151c29dcd57'
UPDATE dbo.TemplateColumn SET VisiblePosition = 31 WHERE ID = '9ed8c767-3c0e-4ff8-98db-f1bfaf238d8a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 51 WHERE ID = '81be4430-1f6b-4a9f-b6ae-f1d507e83f6e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = '8b5322f9-f2a4-494f-8376-f6c62ad84012'
UPDATE dbo.TemplateColumn SET VisiblePosition = 46 WHERE ID = '36b96f8d-d16f-48c7-9b17-f80bd983e934'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '3b6799a9-a955-42a7-836e-f875b0aacf7c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 3 WHERE ID = '39c5157e-27a6-4d97-ae10-f9be60906cd2'
UPDATE dbo.TemplateColumn SET VisiblePosition = 45 WHERE ID = 'ef734e78-a839-409c-a6f7-fdccc9f6cf97'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = '7cad8edd-9565-4915-90fe-fe24106d8078'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAllAmount' WHERE ID = '548cb06c-2462-494f-bfda-ff63fe5494f7'

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO



DBCC CHECKIDENT('dbo.Type', RESEED, 131)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
 

ALTER TABLE dbo.GenCode WITH NOCHECK
  ADD CONSTRAINT FK_GenCode_TypeGroup FOREIGN KEY (TypeGroupID) REFERENCES dbo.TypeGroup(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END




ALTER TABLE dbo.Template WITH NOCHECK
  ADD CONSTRAINT FK_Template_Type FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END




ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END




ALTER TABLE dbo.TemplateDetail WITH NOCHECK
  ADD CONSTRAINT FK_TemplateDetail_Template FOREIGN KEY (TemplateID) REFERENCES dbo.Template(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO
