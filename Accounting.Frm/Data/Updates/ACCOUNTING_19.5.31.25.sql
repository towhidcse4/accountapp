SET CONCAT_NULL_YIELDS_NULL, ANSI_NULLS, ANSI_PADDING, QUOTED_IDENTIFIER, ANSI_WARNINGS, ARITHABORT, XACT_ABORT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS OFF
GO

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO

CREATE TABLE [dbo].[TM04GTGTDetail] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM04GTGTID] [uniqueidentifier] NULL,
  [Code] [nvarchar](512) NULL,
  [Name] [nvarchar](512) NULL,
  [Data] [nvarchar](512) NULL,
  [OrderPriority] [int] NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM04GTGTAdjust] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM04GTGTID] [uniqueidentifier] NOT NULL,
  [Code] [nvarchar](512) NULL,
  [Name] [nvarchar](512) NULL,
  [DeclaredAmount] [money] NULL,
  [AdjustAmount] [money] NULL,
  [DifferAmount] [money] NULL,
  [LateDays] [int] NULL,
  [LateAmount] [money] NULL,
  [ExplainAmount] [money] NULL,
  [CommandNo] [nvarchar](512) NULL,
  [CommandDate] [datetime] NULL,
  [TaxCompanyName] [nvarchar](512) NULL,
  [TaxCompanyDecisionName] [nvarchar](512) NULL,
  [ReceiveDays] [int] NULL,
  [ExplainLateAmount] [money] NULL,
  [DifferReason] [nvarchar](2048) NULL,
  [OrderPriority] [int] NULL,
  [Type] [int] NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TM04GTGTAdjust', 'COLUMN', N'Type'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM04GTGT] (
  [ID] [uniqueidentifier] NOT NULL,
  [BranchID] [uniqueidentifier] NULL,
  [TypeID] [int] NOT NULL,
  [DeclarationName] [nvarchar](512) NULL,
  [DeclarationTerm] [nvarchar](512) NULL,
  [FromDate] [datetime] NULL,
  [ToDate] [datetime] NULL,
  [IsFirstDeclaration] [bit] NULL,
  [AdditionTime] [int] NULL,
  [AdditionDate] [datetime] NULL,
  [CompanyName] [nvarchar](512) NULL,
  [CompanyTaxCode] [nvarchar](50) NULL,
  [TaxAgencyTaxCode] [nvarchar](50) NULL,
  [TaxAgencyName] [nvarchar](512) NULL,
  [TaxAgencyEmployeeName] [nvarchar](512) NULL,
  [CertificationNo] [nvarchar](512) NULL,
  [SignName] [nvarchar](512) NULL,
  [SignDate] [datetime] NULL,
  [IsMonth] [bit] NULL,
  [IsQuarter] [bit] NULL,
  [IsArising] [bit] NULL,
  [Day] [int] NULL,
  [Month] [int] NULL DEFAULT (NULL),
  [Year] [int] NULL DEFAULT (NULL),
  [AdditionTerm] [nvarchar](512) NULL DEFAULT (NULL),
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TM04GTGT', 'COLUMN', N'IsMonth'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TM04GTGT', 'COLUMN', N'IsQuarter'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TM04GTGT', 'COLUMN', N'IsArising'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TM04GTGT', 'COLUMN', N'Day'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TM04GTGT', 'COLUMN', N'Month'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TM04GTGT', 'COLUMN', N'Year'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TM04GTGT', 'COLUMN', N'AdditionTerm'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[CareerGroup] (
  [ID] [uniqueidentifier] NOT NULL,
  [CareerGroupCode] [nvarchar](25) NOT NULL,
  [CareerGroupName] [nvarchar](512) NOT NULL,
  CONSTRAINT [PK_CareerGroup] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturnDetail]
  ADD [CareerGroupID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoiceDetail]
  ADD [CareerGroupID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoiceDetail]
  ADD [RSInwardDetailID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[RSTransferDetail]
  ADD [RSInwardDetailID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[RSInwardOutwardDetail]
  ADD [RSInwardDetailID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MaterialGoods]
  ADD [CareerGroupID ] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[AccountingObject]
  ADD [IsUnofficialStaff] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_TAX_ListTaxSubmit]
    @inputDate DATETIME
AS
BEGIN          
    
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @inputDate
		/*end add by cuongpv*/
	
	DECLARE @tbluutru TABLE (
		icheck BIT,
		tkThue NVARCHAR(25),
		dienGiai NVARCHAR(512),
		soTienPhaiNop money,
		soTienNopLanNay money,
		soTienDaNop money
    )
    
    DECLARE @icheck BIT
    SET @icheck = 0

	DECLARE @checkPPT nvarchar(500)
    SET @checkPPT = (select top 1 a.Data from dbo.SystemOption a where a.Code = 'PPTTGTGT')
	DECLARE @ThueGTGTDauRa money
	DECLARE @ThueGTGTHangNK money
	DECLARE @TongPhaiNop money
	DECLARE @TongDaNop money
	if(@checkPPT = N'Phương pháp khấu trừ')
	begin
    set @ThueGTGTDauRa = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '33311%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'33311', N'Thuế GTGT đầu ra', @ThueGTGTDauRa)
    
    set @ThueGTGTHangNK = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '33312%' 
    and (GL.AccountCorresponding in ('1331', '1332')) and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'33312', N'Thuế GTGT hàng nhập khẩu', @ThueGTGTHangNK)
	end
	else
	begin
    set @TongPhaiNop = (select SUM(Cast(b.Data as money)) from TM04GTGT a join TM04GTGTDetail b
	on a.ID = b.TM04GTGTID
	where a.ToDate <= @inputDate and a.IsFirstDeclaration = 1 and b.Code like 'Item33%')


	set @TongDaNop = (select SUM(GL.DebitAmount) from @tbDataGL GL where GL.Account like '33311%' and (GL.AccountCorresponding like '111%' or GL.AccountCorresponding like '112%') and GL.PostedDate <= @inputDate)
	
	set @ThueGTGTDauRa = ISNULL(@TongPhaiNop,0) - ISNULL(@TongDaNop,0)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'33311', N'Thuế GTGT đầu ra', @ThueGTGTDauRa)
    
    set @ThueGTGTHangNK = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '33312%' 
    and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'33312', N'Thuế GTGT hàng nhập khẩu', @ThueGTGTHangNK)
	end
    DECLARE @ThueGTGTHangNKDaNop money
    set @ThueGTGTHangNKDaNop = (select SUM(GL.DebitAmount) from @tbDataGL GL where GL.Account like '33312%' 
    and ((GL.AccountCorresponding like '111%') OR  (GL.AccountCorresponding like '112%')) and GL.PostedDate <= @inputDate)
    
    UPDATE @tbluutru SET soTienPhaiNop = ISNULL(soTienPhaiNop,0) - ISNULL(@ThueGTGTHangNKDaNop,0) WHERE tkThue like '33312%'
    
    DECLARE @ThueTieuThuDB money
    set @ThueTieuThuDB = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '3332%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3332', N'Thuế tiêu thụ đặc biệt', @ThueTieuThuDB)
    
    DECLARE @ThueXuatNhapKhau money
    set @ThueXuatNhapKhau = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '3333%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3333', N'Thuế xuất nhập khẩu', @ThueXuatNhapKhau)
    
    DECLARE @ThueThuNhapDN money
    set @ThueThuNhapDN = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '3334%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3334', N'Thuế thu nhập doanh nghiệp', @ThueThuNhapDN)
    
    DECLARE @ThueThuNhapCN money
    set @ThueThuNhapCN = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '3335%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3335', N'Thuế thu nhập cá nhân', @ThueThuNhapCN)
    
    DECLARE @ThueTaiNguyen money
    set @ThueTaiNguyen = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '3336%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3336', N'Thuế tài nguyên', @ThueTaiNguyen)
    
    DECLARE @ThueNhaDat money
    set @ThueNhaDat = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '3337%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3337', N'Thuế nhà đất, tiền thuê dất', @ThueNhaDat)
    
    DECLARE @ThueBaoVeMT money
    set @ThueBaoVeMT = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '33381%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'33381', N'Thuế bảo vệ môi trường', @ThueBaoVeMT)
    
    DECLARE @ThueKhac money
    set @ThueKhac = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '33382%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'33382', N'Các loại thuế khác', @ThueKhac)
    
    DECLARE @PhiNopKhac money
    set @PhiNopKhac = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '3339%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3339', N'Phí, lệ phí và các khoản nộp khác', @PhiNopKhac)
    
    UPDATE @tbluutru SET soTienNopLanNay = soTienPhaiNop
    
    SELECT icheck, tkThue, dienGiai, soTienPhaiNop, soTienNopLanNay from @tbluutru where soTienPhaiNop>0
    
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*=================================================================================================
 * Created By:		thaivh	
 * Created Date:	15/5/2018
 * Description:		Lay du lieu cho hop dong << hợp đồng >>
===================================================================================================== */
ALTER PROCEDURE [dbo].[Proc_EM_GetContract]
  
    @FromDate DATETIME ,
    @ToDate DATETIME,
	@IsContractSale bit
AS
BEGIN
	/*add by cuongpv*/
	DECLARE @tbDataEMContract TABLE(
	ID uniqueidentifier,
	AccountingObjectID uniqueidentifier,
	MaterialGoodsID uniqueidentifier,
	SignedDate datetime,
	TypeID int,
	Unit nvarchar(25),
	Quantity decimal(25,10),
	QuantityReceipt decimal(25,10),
	TotalAmount decimal(25,0),
	DiscountAmount decimal(25,0),
	VATAmount decimal(25,0)
	)

	INSERT INTO @tbDataEMContract
	SELECT a.ID, a.AccountingObjectID, b.MaterialGoodsID, a.SignedDate, a.TypeID, b.Unit, b.Quantity, b.QuantityReceipt, b.TotalAmount, b.DiscountAmount, b.VATAmount
	FROM EMContract a LEFT JOIN EMContractDetailMG b ON a.ID = b.ContractID
	WHERE a.SignedDate BETWEEN @FromDate and @ToDate

	DECLARE @tbluutru TABLE(
		ContractID UNIQUEIDENTIFIER,
		SignedDate DATE,
		Code NVARCHAR(50),
		AccountingObjectName NVARCHAR(512),
		AccountingObjectID UNIQUEIDENTIFIER,
		MaterialGoodsName NVARCHAR(512),
		MaterialGoodsID UNIQUEIDENTIFIER,
		Unit NVARCHAR(25),
		Quantity DECIMAL(25,10),
		QuantityReceipt DECIMAL(25,10),
		Amount MONEY,
		DiscountAmount MONEY,
		VATAmount MONEY,
		ContractAmount MONEY,
		DSBanHang MONEY,
		TraLai MONEY,
		GiamGia MONEY,
		ActionAmount MONEY,
		RevenueType BIT,
		AccountObjectGroupID UNIQUEIDENTIFIER,
		MaterialGoodsCategoryID UNIQUEIDENTIFIER,
		DepartmentID UNIQUEIDENTIFIER,
		ContractEmployeeID UNIQUEIDENTIFIER
	)
	/*end add by cuongpv*/
    SET NOCOUNT ON;
	
    if @IsContractSale = 1
		Begin
			
			/*Add by cuongpv de sua cach lam tron*/
			DECLARE @tbDataGL TABLE(
				ID uniqueidentifier,
				BranchID uniqueidentifier,
				ReferenceID uniqueidentifier,
				TypeID int,
				Date datetime,
				PostedDate datetime,
				No nvarchar(25),
				InvoiceDate datetime,
				InvoiceNo nvarchar(25),
				Account nvarchar(25),
				AccountCorresponding nvarchar(25),
				BankAccountDetailID uniqueidentifier,
				CurrencyID nvarchar(3),
				ExchangeRate decimal(25, 10),
				DebitAmount decimal(25,0),
				DebitAmountOriginal decimal(25,2),
				CreditAmount decimal(25,0),
				CreditAmountOriginal decimal(25,2),
				Reason nvarchar(512),
				Description nvarchar(512),
				VATDescription nvarchar(512),
				AccountingObjectID uniqueidentifier,
				EmployeeID uniqueidentifier,
				BudgetItemID uniqueidentifier,
				CostSetID uniqueidentifier,
				ContractID uniqueidentifier,
				StatisticsCodeID uniqueidentifier,
				InvoiceSeries nvarchar(25),
				ContactName nvarchar(512),
				DetailID uniqueidentifier,
				RefNo nvarchar(25),
				RefDate datetime,
				DepartmentID uniqueidentifier,
				ExpenseItemID uniqueidentifier,
				OrderPriority int,
				IsIrrationalCost bit
			)

			INSERT INTO @tbDataGL
			SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate BETWEEN @FromDate and @ToDate

			/*end add by cuongpv*/

			/*add by cuongpv*/
			INSERT INTO @tbluutru(ContractID, AccountingObjectID, MaterialGoodsID, Unit, Quantity, QuantityReceipt, Amount, 
			DiscountAmount, VATAmount)
			/*SELECT a.ID as ContractID, a.AccountingObjectID, b.MaterialGoodsID, MAX(b.Unit) as Unit, SUM(b.Quantity) as Quantity, SUM(b.QuantityReceipt) as QuantityReceipt,
			SUM(b.TotalAmount) as Amount, SUM(b.DiscountAmount) as DiscountAmount, SUM(b.VATAmount) as VATAmount
			FROM EMContract a LEFT JOIN EMContractDetailMG b ON a.ID = b.ContractID
			WHERE (a.SignedDate BETWEEN @FromDate and @ToDate) AND a.TypeID = 860 comment by cuongpv*/
			SELECT ID as ContractID, AccountingObjectID, MaterialGoodsID, MAX(Unit) as Unit, SUM(Quantity) as Quantity, SUM(QuantityReceipt) as QuantityReceipt,
			SUM(TotalAmount) as Amount, SUM(DiscountAmount) as DiscountAmount, SUM(VATAmount) as VATAmount
			FROM @tbDataEMContract
			WHERE (SignedDate BETWEEN @FromDate and @ToDate) AND TypeID = 860
			GROUP BY ID, AccountingObjectID, MaterialGoodsID
			
			DECLARE @tbDSBanHang TABLE(
				ContractID UNIQUEIDENTIFIER,
				AccountingObjectID UNIQUEIDENTIFIER,
				MaterialGoodsID UNIQUEIDENTIFIER,
				DSBanHang decimal(25,10)
			)
			INSERT INTO @tbDSBanHang
			SELECT c.ContractID, c.AccountingObjectID, a.MaterialGoodsID, SUM(c.CreditAmount - c.DebitAmount) as DSBanHang 
				FROM SAInvoiceDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID /*edit by cuongpv GeneralLedger -> @tbDataGL*/
				WHERE c.ContractID in (select ContractID from @tbluutru) AND c.Account like '511%'
				 AND (c.PostedDate between @FromDate and @ToDate)
				GROUP BY c.ContractID, c.AccountingObjectID, a.MaterialGoodsID
			
			UPDATE @tbluutru SET DSBanHang = f.DSBanHang
			FROM (select ContractID as Eid, AccountingObjectID as AOID, MaterialGoodsID as Mid, DSBanHang from @tbDSBanHang) f
			WHERE ContractID = f.Eid AND AccountingObjectID = f.AOID AND MaterialGoodsID = f.Mid
			
			DECLARE @tbTraLai TABLE(
				ContractID UNIQUEIDENTIFIER,
				AccountingObjectID UNIQUEIDENTIFIER,
				MaterialGoodsID UNIQUEIDENTIFIER,
				TraLai decimal(25,10)
			)
			INSERT INTO @tbTraLai
			SELECT c.ContractID, c.AccountingObjectID, a.MaterialGoodsID, SUM(c.DebitAmount - c.CreditAmount) as TraLai 
				FROM SAReturnDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID /*edit by cuongpv GeneralLedger -> @tbDataGL*/
				WHERE c.ContractID in (select ContractID from @tbluutru) AND c.Account like '511%' AND c.TypeID=330
				 AND (c.PostedDate between @FromDate and @ToDate)
				GROUP BY c.ContractID, c.AccountingObjectID, a.MaterialGoodsID
			
			UPDATE @tbluutru SET TraLai = tr.TraLai
			FROM (select ContractID as Eid, AccountingObjectID as AOID, MaterialGoodsID as Mid, TraLai from @tbTraLai) tr
			WHERE ContractID = tr.Eid AND AccountingObjectID = tr.AOID AND MaterialGoodsID = tr.Mid
			
			
			
			UPDATE @tbluutru SET ActionAmount = ISNULL(DSBanHang,0) - ISNULL(TraLai,0)
			
			UPDATE @tbluutru SET ContractAmount = ISNULL(Amount,0)
			
			/*end add by cuongpv*/
		End
	else
		Begin
			INSERT INTO @tbluutru(ContractID, AccountingObjectID, MaterialGoodsID, Unit, Quantity, QuantityReceipt, Amount, 
			DiscountAmount, VATAmount)
			/*SELECT a.ID as ContractID, a.AccountingObjectID, b.MaterialGoodsID, MAX(b.Unit) as Unit, SUM(b.Quantity) as Quantity, SUM(b.QuantityReceipt) as QuantityReceipt,
			SUM(b.TotalAmount) as Amount, SUM(b.DiscountAmount) as DiscountAmount, SUM(b.VATAmount) as VATAmount
			FROM EMContract a LEFT JOIN EMContractDetailMG b ON a.ID = b.ContractID
			WHERE (a.SignedDate BETWEEN @FromDate and @ToDate) AND a.TypeID = 850
			GROUP BY a.ID, a.AccountingObjectID, b.MaterialGoodsID Comment by cuongpv*/
			SELECT ID as ContractID, AccountingObjectID, MaterialGoodsID, MAX(Unit) as Unit, SUM(Quantity) as Quantity, SUM(QuantityReceipt) as QuantityReceipt,
			SUM(TotalAmount) as Amount, SUM(DiscountAmount) as DiscountAmount, SUM(VATAmount) as VATAmount
			FROM @tbDataEMContract
			WHERE (SignedDate BETWEEN @FromDate and @ToDate) AND TypeID = 850
			GROUP BY ID, AccountingObjectID, MaterialGoodsID
			
			/*add by cuongpv lay du lieu tho lam tron tung dong PPInvoiceDetail, PPServiceDetail*/
			DECLARE @tbDataPPinvoiceDetail TABLE(
				ContractID UNIQUEIDENTIFIER,
				AccountingObjectID UNIQUEIDENTIFIER,
				MaterialGoodsID UNIQUEIDENTIFIER,
				Amount decimal(25,0),
				DiscountAmount decimal(25,0),
				VATAmount decimal(25,0)
			)
			INSERT INTO @tbDataPPinvoiceDetail
			SELECT ContractID, AccountingObjectID, MaterialGoodsID, Amount, DiscountAmount, VATAmount 
			FROM PPInvoiceDetail 
			WHERE ContractID in (select ContractID from @tbluutru)

			DECLARE @tbDataPPServiceDetail TABLE(
				ContractID UNIQUEIDENTIFIER,
				AccountingObjectID UNIQUEIDENTIFIER,
				MaterialGoodsID UNIQUEIDENTIFIER,
				Amount decimal(25,0),
				DiscountAmount decimal(25,0),
				VATAmount decimal(25,0)
			)
			INSERT INTO @tbDataPPServiceDetail
			SELECT ContractID, AccountingObjectID, MaterialGoodsID, Amount, DiscountAmount, VATAmount 
			FROM PPServiceDetail 
			WHERE ContractID in (select ContractID from @tbluutru)
			/*end add by cuongpv*/

			DECLARE @tbDSDSMuaHang TABLE(
				ContractID UNIQUEIDENTIFIER,
				AccountingObjectID UNIQUEIDENTIFIER,
				MaterialGoodsID UNIQUEIDENTIFIER,
				DSMuaHang decimal(25,10)
			)
			INSERT INTO @tbDSDSMuaHang
			SELECT a.ContractID, a.AccountingObjectID, a.MaterialGoodsID, SUM(a.Amount) as DSMuaHang
				FROM @tbDataPPinvoiceDetail a 
				/*WHERE a.ContractID in (select ContractID from @tbluutru) comment by cuongpv*/
				GROUP BY a.ContractID, a.AccountingObjectID, a.MaterialGoodsID
			UNION ALL
			SELECT a.ContractID, a.AccountingObjectID, a.MaterialGoodsID, SUM(a.Amount) as DSMuaHang
				FROM @tbDataPPServiceDetail a 
				/*WHERE a.ContractID in (select ContractID from @tbluutru) comment by cuongpv*/
				GROUP BY a.ContractID, a.AccountingObjectID, a.MaterialGoodsID
			
			UPDATE @tbluutru SET DSBanHang = mh.DSMuaHang
			FROM (select ContractID as Eid, AccountingObjectID as AOId, MaterialGoodsID as Mid, DSMuaHang from @tbDSDSMuaHang) mh
			WHERE ContractID = mh.Eid AND AccountingObjectID = mh.AOId AND MaterialGoodsID = mh.Mid
			
			/*add by cuongpv lay du lieu lam tron tung dong PPDiscountReturnDetail*/
			DECLARE @tbDataPPDiscountReturnDetail TABLE(
				ContractID UNIQUEIDENTIFIER,
				AccountingObjectID UNIQUEIDENTIFIER,
				MaterialGoodsID UNIQUEIDENTIFIER,
				Amount decimal(25,0),
				VATAmount decimal(25,0)
			)
			INSERT INTO @tbDataPPDiscountReturnDetail
			SELECT a.ContractID, a.AccountingObjectID, a.MaterialGoodsID, a.Amount, a.VATAmount
			FROM PPDiscountReturnDetail a left join PPDiscountReturn b on b.ID = a.PPDiscountReturnID
			WHERE b.TypeID=220 and a.ContractID in (select ContractID from @tbluutru)
			/*end add by cuongpv*/

			DECLARE @tbTraLaiGiamGia TABLE(
				ContractID UNIQUEIDENTIFIER,
				AccountingObjectID UNIQUEIDENTIFIER,
				MaterialGoodsID UNIQUEIDENTIFIER,
				TraLaiGiamGia decimal(25,10)
			)
			INSERT INTO @tbTraLaiGiamGia
			SELECT a.ContractID, a.AccountingObjectID, a.MaterialGoodsID, SUM(a.Amount) as TraLaiGiamGia
				FROM @tbDataPPDiscountReturnDetail a
				/*WHERE a.ContractID in (select ContractID from @tbluutru) comment by cuongpv*/
				GROUP BY a.ContractID, a.AccountingObjectID, a.MaterialGoodsID
			
			UPDATE @tbluutru SET TraLai = trg.TraLaiGiamGia
			FROM (select ContractID as Eid, AccountingObjectID as AOId, MaterialGoodsID as Mid, TraLaiGiamGia from @tbTraLaiGiamGia) trg
			WHERE ContractID = trg.Eid AND AccountingObjectID = trg.AOId AND MaterialGoodsID = trg.Mid
			
			UPDATE @tbluutru SET ActionAmount = ISNULL(DSBanHang,0) - ISNULL(TraLai,0)
			
			UPDATE @tbluutru SET ContractAmount = ISNULL(Amount,0)
		End
		
			UPDATE @tbluutru SET SignedDate = hd.SignedDate, Code = hd.Code, AccountingObjectName = hd.AccountingObjectName, RevenueType = hd.RevenueType,
			DepartmentID = hd.DepartmentID, ContractEmployeeID = hd.ContractEmployeeID
			FROM (select ID as Eid, SignedDate, Code, AccountingObjectName, RevenueType, DepartmentID, ContractEmployeeID from EMContract) hd
			WHERE ContractID = hd.Eid
			
			UPDATE @tbluutru SET MaterialGoodsCategoryID = M.MaterialGoodsCategoryID, MaterialGoodsName = M.MaterialGoodsName
			FROM (select ID as Mid, MaterialGoodsCategoryID, MaterialGoodsName from MaterialGoods) M
			WHERE MaterialGoodsID = M.Mid
			
			UPDATE @tbluutru SET AccountObjectGroupID = AO.AccountObjectGroupID
			FROM (select ID as AOid, AccountObjectGroupID from AccountingObject) AO
			WHERE AccountingObjectID = AO.AOID
		
		SELECT ContractID, SignedDate, Code, AccountingObjectName, AccountingObjectID, MaterialGoodsName, MaterialGoodsID, Unit, Quantity, QuantityReceipt, Amount,
		DiscountAmount, VATAmount, ContractAmount, ActionAmount, RevenueType, AccountObjectGroupID, MaterialGoodsCategoryID, DepartmentID, ContractEmployeeID
		FROM @tbluutru
		ORDER BY Code
	/*end add by cuongpv*/
END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[RefVoucher] (
  [ID] [uniqueidentifier] NOT NULL,
  [RefID1] [uniqueidentifier] NULL,
  [RefID2] [uniqueidentifier] NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE dbo.Template
  DROP CONSTRAINT FK_Template_Type
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateDetail
  DROP CONSTRAINT FK_TemplateDetail_Template
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.AccountDefault SET FilterAccount = N'141;331;3388;711' WHERE ID = '128fd6e6-b779-41b6-96ac-38bd1999bddc'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.CareerGroup(ID, CareerGroupCode, CareerGroupName) VALUES ('ff99230b-805e-45f3-83a0-22feeef159da', N'5', N'Hoạt động kinh doanh khác áp dụng thuế suất 2%')
INSERT dbo.CareerGroup(ID, CareerGroupCode, CareerGroupName) VALUES ('8cc59044-8f45-45d5-a886-62d3efdb3517', N'3', N'Dịch vụ, xây dựng không bao thầu nguyên vật liệu áp dụng thuế suất 5%')
INSERT dbo.CareerGroup(ID, CareerGroupCode, CareerGroupName) VALUES ('bd586600-1b06-49c8-ab11-c3a61294a6df', N'4', N'Sản xuất, vận tải, dịch vụ có gắn với hàng hóa, xây dựng có bao thầu nguyên vật liệu áp dụng thuế suất 3%')
INSERT dbo.CareerGroup(ID, CareerGroupCode, CareerGroupName) VALUES ('f63a3b76-8f45-472b-9a50-dc3f90de9056', N'2', N'Phân phối, cung cấp hàng hóa áp dụng thuế suất 1%')
INSERT dbo.CareerGroup(ID, CareerGroupCode, CareerGroupName) VALUES ('58a72acd-13e1-43d1-88e9-f8c1fe583f44', N'1', N'Hàng hóa dịch vụ không chịu thuế GTGT hoặc hàng hóa dịch vụ áp dụng thuế suất 0%')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.SupplierService(ID, SupplierServiceCode, SupplierServiceName, PathAccess) VALUES ('e424067d-cbe9-4949-96c8-f4086f5b6384', N'VNPT', N'Công ty công nghệ thông tin VNPT', N'https://0107489961-democadmin.vnpt-invoice.com.vn/publishservice.asmx')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.SystemOption(ID, Code, Name, Type, Data, DefaultData, Note, IsSecurity) VALUES (131, N'VTHH_NhomNNMD', N'Nhóm ngành nghề mặc định', 7, N'1', N'1', N'Lấy từ bảng CareerGroup', 0)
INSERT dbo.SystemOption(ID, Code, Name, Type, Data, DefaultData, Note, IsSecurity) VALUES (132, N'PPTTGTGT', N'Phương pháp tính thuế giá trị gia tăng', 5, N'Phương pháp khấu trừ', N'Phương pháp khấu trừ', N'Phương pháp khấu trừ
Phương pháp trực tiếp trên doanh thu', 0)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.Template SET TemplateName = N'Mẫu thuế', IsDefault = 0, IsActive = 0, IsSecurity = 1 WHERE ID = '335f1c3e-62ab-4403-99b6-4f2b1e9a5e38'
UPDATE dbo.Template SET TemplateName = N'Mẫu thuế', IsDefault = 0, IsActive = 0, IsSecurity = 1 WHERE ID = '85dc5dee-81cf-4b54-a3a4-b6f57fb4fa96'

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
SET IDENTITY_INSERT dbo.Template ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.Template(ID, TemplateName, TypeID, IsDefault, IsActive, IsSecurity, OrderPriority) VALUES ('774c10b8-d918-454f-af43-d4d5805a0ef3', N'Mẫu thuế', 300, 0, 0, 1, 1)
GO
SET IDENTITY_INSERT dbo.Template OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.TemplateColumn WHERE ID = '5f9c6ab5-0a29-488d-a255-03aaaa1029fd'
DELETE dbo.TemplateColumn WHERE ID = 'ac83e94d-8e58-4373-99fe-03f75c913301'
DELETE dbo.TemplateColumn WHERE ID = 'a5dcd8d2-45be-49f9-83a8-087ce8a251b9'
DELETE dbo.TemplateColumn WHERE ID = '56514978-b576-4e28-8db3-0a21e5adb3f7'
DELETE dbo.TemplateColumn WHERE ID = 'f6920bcf-a206-4295-bb04-102664e2e386'
DELETE dbo.TemplateColumn WHERE ID = '437da69c-0b3b-4a3f-ae38-16000b386227'
DELETE dbo.TemplateColumn WHERE ID = '43c0c9a6-e70e-414d-84b2-1634b667a058'
DELETE dbo.TemplateColumn WHERE ID = 'ffd956c4-e8ff-4004-8e3c-19effb366dc2'
DELETE dbo.TemplateColumn WHERE ID = 'adeda0d1-2ffe-49ad-b1a7-1e7f7fe9f88c'
DELETE dbo.TemplateColumn WHERE ID = 'f3996409-5963-4ab4-8761-21eb69f34684'
DELETE dbo.TemplateColumn WHERE ID = '6c107150-91b5-4dbf-98d8-23ed50e6a91d'
DELETE dbo.TemplateColumn WHERE ID = 'd6adde08-d0d5-42fb-8f24-2446ca27f414'
DELETE dbo.TemplateColumn WHERE ID = '85b1b07e-b3ea-4748-ac3c-25a706bcee57'
DELETE dbo.TemplateColumn WHERE ID = '9e9e02f1-d3cd-4bf3-92fd-283c7242c359'
DELETE dbo.TemplateColumn WHERE ID = '4ea59bfc-f8f9-4820-8934-2a3cef5e70c8'
DELETE dbo.TemplateColumn WHERE ID = '3e7df795-5bb6-4cf7-a90d-30c9cb900940'
DELETE dbo.TemplateColumn WHERE ID = 'f9bc256f-66ea-4480-8c24-35b8815093a4'
DELETE dbo.TemplateColumn WHERE ID = '90fe2c02-25fa-43cc-8bfd-3ca6a243931a'
DELETE dbo.TemplateColumn WHERE ID = '13d91551-4764-4e45-80f4-3cad3824275b'
DELETE dbo.TemplateColumn WHERE ID = '2411c695-e49a-4224-b03c-44455e6838a0'
DELETE dbo.TemplateColumn WHERE ID = '8aa2df95-ca0b-428b-a031-463cdb35e803'
DELETE dbo.TemplateColumn WHERE ID = '064d270c-b29e-4f6a-a494-4ca5901cfd7f'
DELETE dbo.TemplateColumn WHERE ID = '4b8a9380-b3e2-49c8-b1cf-4f70c75eeff8'
DELETE dbo.TemplateColumn WHERE ID = '10f3bb97-08f7-4431-95c6-515791388bba'
DELETE dbo.TemplateColumn WHERE ID = '07b97bb3-1f8d-4d33-a718-566b0766f694'
DELETE dbo.TemplateColumn WHERE ID = 'c0bef7cf-8db2-489a-9a73-569524f23a6c'
DELETE dbo.TemplateColumn WHERE ID = 'fddc1a53-a6fc-431c-9ab0-595ecb993bf7'
DELETE dbo.TemplateColumn WHERE ID = '38122559-9b3c-446f-97f0-5c33d50e4039'
DELETE dbo.TemplateColumn WHERE ID = '42b6cea0-3461-4fd5-aa66-5e914bd70b6f'
DELETE dbo.TemplateColumn WHERE ID = 'c2a73b39-2978-4296-aa2e-62ac35550ba5'
DELETE dbo.TemplateColumn WHERE ID = 'a4fc2b1a-6346-436a-b11d-644b264e6623'
DELETE dbo.TemplateColumn WHERE ID = 'b556bc87-ad8b-45e1-ab1f-6d743036fbae'
DELETE dbo.TemplateColumn WHERE ID = '38972881-7772-49ec-997e-72305b48806d'
DELETE dbo.TemplateColumn WHERE ID = '3c5bd54a-7f3e-43e3-af6b-7547d866e855'
DELETE dbo.TemplateColumn WHERE ID = '46a1b142-5f82-46b3-a63f-763172a4af8a'
DELETE dbo.TemplateColumn WHERE ID = 'b26bde64-11b4-4b48-9c38-79a8aa2bd463'
DELETE dbo.TemplateColumn WHERE ID = 'd2953d60-4a19-4640-8312-79c247e189b9'
DELETE dbo.TemplateColumn WHERE ID = '5864bf96-990f-4878-b918-79df9f96ac4d'
DELETE dbo.TemplateColumn WHERE ID = '03e06ff3-12e5-4c5e-b8dc-7b7256ad2218'
DELETE dbo.TemplateColumn WHERE ID = '05ef9dce-6acf-419b-ba1e-7c58beb9c298'
DELETE dbo.TemplateColumn WHERE ID = '87521372-0791-4fab-9335-7d64e8487019'
DELETE dbo.TemplateColumn WHERE ID = '0c61cdde-5b93-4390-b412-8223b4bbd74e'
DELETE dbo.TemplateColumn WHERE ID = '4e4127c6-6fd3-4ab5-960e-83a0558fa2dc'
DELETE dbo.TemplateColumn WHERE ID = 'acd99994-1292-48d9-9c0a-83b9c6965bc2'
DELETE dbo.TemplateColumn WHERE ID = '418ef7c3-72a8-4f7a-bbe4-84123e544fe7'
DELETE dbo.TemplateColumn WHERE ID = 'f284f348-cea2-4d2d-a54a-850014e65ea8'
DELETE dbo.TemplateColumn WHERE ID = 'c99fa51f-15fe-4e8e-9117-8524f73bda1c'
DELETE dbo.TemplateColumn WHERE ID = '58261f8b-dd43-4e75-863c-869e5f21e83a'
DELETE dbo.TemplateColumn WHERE ID = '7d5fdf6c-815a-4a00-96e0-895189581399'
DELETE dbo.TemplateColumn WHERE ID = '3e38cfd7-bda5-471d-8792-896f9e611132'
DELETE dbo.TemplateColumn WHERE ID = '2f785c9b-bff7-492d-9199-8b209d93eae8'
DELETE dbo.TemplateColumn WHERE ID = 'd89a27a0-85b7-45ed-b1c8-8fa3a7187740'
DELETE dbo.TemplateColumn WHERE ID = 'c90814e2-9638-4070-9efb-95a758c2e348'
DELETE dbo.TemplateColumn WHERE ID = '615852e6-b31b-4d57-aa82-965ac49b0b40'
DELETE dbo.TemplateColumn WHERE ID = '2d337ddf-8c01-402e-8f60-97095994d8e8'
DELETE dbo.TemplateColumn WHERE ID = '389ee745-14b8-4536-b816-974199d1e280'
DELETE dbo.TemplateColumn WHERE ID = 'f5875ea4-4664-4dcb-82b2-9902c2d341d4'
DELETE dbo.TemplateColumn WHERE ID = '1e90b5d8-05fd-4fad-972a-9972c5b1fefc'
DELETE dbo.TemplateColumn WHERE ID = '043c6a2c-0c53-4cf9-b944-99d2c6d0ef9c'
DELETE dbo.TemplateColumn WHERE ID = 'b8137991-9c82-4109-a8cb-9a75fb3dac25'
DELETE dbo.TemplateColumn WHERE ID = 'b71c7ff6-a222-4fe9-9bfe-9d8a7174a2d3'
DELETE dbo.TemplateColumn WHERE ID = '0acdfd01-6ad7-4c11-947b-9daf493866a3'
DELETE dbo.TemplateColumn WHERE ID = '7b99c4d9-f5b6-4cb7-ba7f-9def97dce5e8'
DELETE dbo.TemplateColumn WHERE ID = '8b437e26-89c5-484a-921d-9e1ff9fc3d51'
DELETE dbo.TemplateColumn WHERE ID = '8dd58b08-f8c8-4e69-8354-a1b3a6953ad3'
DELETE dbo.TemplateColumn WHERE ID = 'a50a7689-b7f4-4156-91aa-a3b2d1327fa3'
DELETE dbo.TemplateColumn WHERE ID = '924e40e2-020f-4e58-aa5e-a55672496001'
DELETE dbo.TemplateColumn WHERE ID = 'd9a0b352-1414-476e-b607-a7cae0ccb1f3'
DELETE dbo.TemplateColumn WHERE ID = 'c8c8b380-eb0a-4a47-b7c0-aed404b3aee5'
DELETE dbo.TemplateColumn WHERE ID = '370c22c0-ab6b-475e-8256-b09d2f73d744'
DELETE dbo.TemplateColumn WHERE ID = '43f09223-6fb9-4c27-a1f5-b719f8c0ceb8'
DELETE dbo.TemplateColumn WHERE ID = '9c076000-2100-426b-9fe2-b7d7a7e561d4'
DELETE dbo.TemplateColumn WHERE ID = 'acbc1456-d658-4912-8fce-bade9d0bfc3d'
DELETE dbo.TemplateColumn WHERE ID = '358f71ba-49ec-4eaf-a97f-bb74897c1a27'
DELETE dbo.TemplateColumn WHERE ID = 'f5b67691-cfc6-44a4-a664-bb9c4c00a871'
DELETE dbo.TemplateColumn WHERE ID = '593550e3-8cc0-4687-b7ac-bcc3dad1b64d'
DELETE dbo.TemplateColumn WHERE ID = 'dbd26275-cad8-487f-91f5-bf36795171e3'
DELETE dbo.TemplateColumn WHERE ID = '211283a1-64fc-4154-af37-c3d27cceb58e'
DELETE dbo.TemplateColumn WHERE ID = 'be86c8d5-9398-4ee8-b61d-c4b28dff311b'
DELETE dbo.TemplateColumn WHERE ID = '0f2ec4d8-45c1-4286-bb48-cb3ec742089d'
DELETE dbo.TemplateColumn WHERE ID = '946b8032-96f9-4966-b157-ccab86a49661'
DELETE dbo.TemplateColumn WHERE ID = '54bb00de-b76c-407f-9dfa-ce199f7b101c'
DELETE dbo.TemplateColumn WHERE ID = '61b1e945-a1bd-4552-b24a-d21b6a145cbd'
DELETE dbo.TemplateColumn WHERE ID = '50ae044f-f65a-4127-8e28-d41b2b192723'
DELETE dbo.TemplateColumn WHERE ID = 'e5fdbe24-2c01-4716-9801-d4f8249c6ef9'
DELETE dbo.TemplateColumn WHERE ID = '92734fbf-04af-47ae-a4df-d58510e4adbf'
DELETE dbo.TemplateColumn WHERE ID = '0e7d2ee4-fdcc-48b5-b419-d8d362c4b213'
DELETE dbo.TemplateColumn WHERE ID = '64520a06-3768-4b9c-acda-da8e191ab976'
DELETE dbo.TemplateColumn WHERE ID = 'd8ee16fb-ea56-475e-9a38-de66ee9c54b2'
DELETE dbo.TemplateColumn WHERE ID = '93da0f2c-17d6-498e-a596-e00450cd6c6e'
DELETE dbo.TemplateColumn WHERE ID = 'dbba0cd3-663a-4610-b6f2-e3498360a2d7'
DELETE dbo.TemplateColumn WHERE ID = '2f6e93bf-d24e-42b0-94d4-e5907996199a'
DELETE dbo.TemplateColumn WHERE ID = 'b3c1bba6-773d-43bf-88e8-e969fa6cd838'
DELETE dbo.TemplateColumn WHERE ID = 'be08d80b-2be8-4edc-b0a9-ebc775f7754a'
DELETE dbo.TemplateColumn WHERE ID = '260b7ddd-dab4-473f-8716-ec99a18f943a'
DELETE dbo.TemplateColumn WHERE ID = 'f3a7cf15-04a2-4f29-9fb7-edc2199b8f48'
DELETE dbo.TemplateColumn WHERE ID = 'ec45fcd4-7714-4e43-a638-f08ab360879c'
DELETE dbo.TemplateColumn WHERE ID = '0ab22a22-5993-433f-97a2-f505a574db14'
DELETE dbo.TemplateColumn WHERE ID = '84f6bbe0-9205-48e9-8df3-f62330cf04d8'
DELETE dbo.TemplateColumn WHERE ID = 'd1b266d9-2df2-46e1-b969-f6f4f82df37f'
DELETE dbo.TemplateColumn WHERE ID = 'd9e24279-fe46-4d8e-ac97-f8b33771e1ca'
DELETE dbo.TemplateColumn WHERE ID = '796532a2-f500-448f-947b-fe3b3bc8a703'
DELETE dbo.TemplateColumn WHERE ID = '8cce7d81-67f4-4cee-816f-fea3072d6362'
DELETE dbo.TemplateColumn WHERE ID = '566e04f8-8121-4f6f-99fa-fedc661397aa'
DELETE dbo.TemplateColumn WHERE ID = '670c46fd-fbf8-4be9-8d74-ffd5db3fb87c'

UPDATE dbo.TemplateColumn SET ColumnName = N'CareerGroupID', ColumnCaption = N'Nhóm ngành nghề tính thuế GTGT', ColumnToolTip = N'Nhóm ngành nghề tính thuế GTGT' WHERE ID = '980026e9-fab3-41c1-b6c1-05f49018cd1c'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceSeries', ColumnCaption = N'Ký hiệu HĐ', ColumnToolTip = N'Ký hiệu hóa đơn', ColumnWidth = 75, IsVisible = 0 WHERE ID = '0a247384-a86f-4d1a-9626-08da97a44f2a'
UPDATE dbo.TemplateColumn SET ColumnName = N'CareerGroupID', ColumnCaption = N'Nhóm ngành nghề tính thuế GTGT', ColumnToolTip = N'Nhóm ngành nghề tính thuế GTGT' WHERE ID = 'b28b4676-d5d2-4ef5-aa8d-0afc9b3827e2'
UPDATE dbo.TemplateColumn SET ColumnName = N'Unit', ColumnCaption = N'ĐVT', ColumnToolTip = N'Đơn vị tính', ColumnWidth = 75 WHERE ID = '4d35426c-9f7f-44dd-b388-0be5ada9c18e'
UPDATE dbo.TemplateColumn SET ColumnName = N'LotNo', ColumnCaption = N'Số lô', ColumnWidth = 98, IsVisible = 1 WHERE ID = '0b5cdd69-dfee-4a0e-99b7-13c72a3a2178'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceForm', ColumnCaption = N'Hình thức hóa đơn', ColumnToolTip = NULL, ColumnWidth = 110, IsVisible = 0 WHERE ID = 'e2c38b81-8c47-465a-8692-1a0d130a8b89'
UPDATE dbo.TemplateColumn SET ColumnName = N'Date' WHERE ID = 'ad6e80ef-27d0-43e5-b301-20d836948242'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '4a65ce08-7259-47cf-9e39-21d71f75461b'
UPDATE dbo.TemplateColumn SET ColumnName = N'BankAccountDetailID', ColumnCaption = N'TK Ngân hàng', ColumnToolTip = N'Tài khoản Ngân hàng', ColumnWidth = 95, VisiblePosition = 47 WHERE ID = '9caae92e-ae50-4ca8-979f-24fae7094689'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Ten hàng' WHERE ID = '5a9609ae-16c6-453b-a49f-266e2ed62d36'
UPDATE dbo.TemplateColumn SET ColumnName = N'DueDate', ColumnCaption = N'Hạn thanh toán', ColumnToolTip = N'Hạn thanh toán', ColumnWidth = 90, IsVisible = 1, VisiblePosition = 11 WHERE ID = 'c8c1fd72-a7d8-4451-ad75-2b26bdfde429'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '365a350b-006f-42de-a7be-3577ed2ae354'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'ec8fcfec-1bfd-4306-b906-415fb8bfe0d1'
UPDATE dbo.TemplateColumn SET ColumnName = N'CareerGroupID', ColumnCaption = N'Nhóm ngành nghề tính thuế GTGT', ColumnToolTip = N'Nhóm ngành nghề tính thuế GTGT' WHERE ID = 'a6dd6095-5571-4c0c-bdd6-41c2bf1b7034'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '642c871b-1816-4f71-b27f-454a64e491b2'
UPDATE dbo.TemplateColumn SET ColumnName = N'CareerGroupID', ColumnCaption = N'Nhóm ngành nghề tính thuế GTGT', ColumnToolTip = N'Nhóm ngành nghề tính thuế GTGT' WHERE ID = '6a3a6787-e547-42bf-acee-45b489fe3b62'
UPDATE dbo.TemplateColumn SET ColumnName = N'CreditAccount', ColumnCaption = N'TK Có', ColumnToolTip = N'Tài khoản Có' WHERE ID = '6e7ebb4c-ecd7-433e-b40a-4c99b41621a3'
UPDATE dbo.TemplateColumn SET ColumnName = N'Date' WHERE ID = 'f9337bb0-6ea6-4943-b54e-4d828718f7e1'
UPDATE dbo.TemplateColumn SET ColumnName = N'PaymentClauseID', ColumnCaption = N'Điều khoản TT', ColumnToolTip = N'Điều khoản thanh toán', ColumnWidth = 95, IsVisible = 0, VisiblePosition = 10 WHERE ID = '1a070805-2dd2-4273-a534-5333cae7a6d9'
UPDATE dbo.TemplateColumn SET ColumnName = N'CustomProperty3', ColumnCaption = N'Cột 3', ColumnToolTip = N'', ColumnWidth = 105, VisiblePosition = 15 WHERE ID = '8d7d81dc-99e2-473a-9673-54e979e01a8f'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTypeID', IsVisible = 0 WHERE ID = '98fa63e9-bdbd-47b0-a961-58d790b71e80'
UPDATE dbo.TemplateColumn SET ColumnName = N'CostSetID', ColumnCaption = N'ĐT tập hợp CP', ColumnToolTip = N'Đối tượng tập hợp chi phí', ColumnWidth = 150, IsVisible = 0, VisiblePosition = 50 WHERE ID = 'ae4cef7b-67ff-4afc-97d3-59be70c61456'
UPDATE dbo.TemplateColumn SET ColumnName = N'QuantityConvert', ColumnCaption = N'SL chuyển đổi', ColumnToolTip = N'Số lượng chuyển đổi', ColumnWidth = 130 WHERE ID = '5baecb6b-baf2-4ec8-9b86-5d1f956ca6cb'
UPDATE dbo.TemplateColumn SET ColumnName = N'CareerGroupID', ColumnCaption = N'Nhóm ngành nghề tính thuế GTGT', ColumnToolTip = N'Nhóm ngành nghề tính thuế GTGT' WHERE ID = '5f9f7e20-255d-4ebd-a756-62f0d197b32d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 5 WHERE ID = '356f399d-ad7d-4a34-97d5-667326be0198'
UPDATE dbo.TemplateColumn SET ColumnName = N'ConfrontInvDate', ColumnCaption = N'Ngày hóa đơn', IsReadOnly = 1, ColumnWidth = 110, IsVisible = 1 WHERE ID = '0f590eee-45a4-426f-a9dd-6b0bbb03c896'
UPDATE dbo.TemplateColumn SET ColumnName = N'ExpiryDate', ColumnCaption = N'Hạn dùng', ColumnWidth = 95, IsVisible = 1 WHERE ID = '8dd73a9b-80fa-4eb5-ac9a-6d810135bddf'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '2a4f02a9-8ad5-4fad-8fc3-6e0f5304eff9'
UPDATE dbo.TemplateColumn SET ColumnName = N'EmployeeID', ColumnCaption = N'Nhân viên bán hàng', ColumnWidth = 115, IsVisible = 1, VisiblePosition = 16 WHERE ID = '7f1030da-f6ff-445e-84ed-6ea043a78c5d'
UPDATE dbo.TemplateColumn SET ColumnName = N'ConvertRate', ColumnCaption = N'Tỷ lệ chuyển đổi', ColumnToolTip = N'', ColumnWidth = 115 WHERE ID = '2da79752-467a-4b32-ad4a-726da233f047'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = '7c464ede-c5c9-4ef3-85a8-7a325c359cbf'
UPDATE dbo.TemplateColumn SET ColumnName = N'CareerGroupID', ColumnCaption = N'Nhóm ngành nghề tính thuế GTGT', ColumnToolTip = N'Nhóm ngành nghề tính thuế GTGT' WHERE ID = '03804e15-59dc-47a7-9e6f-7bc75cd81e26'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceDate', ColumnCaption = N'Ngày hóa đơn', ColumnToolTip = N'Ngày hóa đơn', ColumnWidth = 85, IsVisible = 0, VisiblePosition = 6 WHERE ID = '8a9aa42e-54ef-42cc-a38f-8117b3bc3bf8'
UPDATE dbo.TemplateColumn SET ColumnName = N'RepositoryID', ColumnCaption = N'Kho', ColumnWidth = 105 WHERE ID = 'c21d0981-43a6-40d2-be81-8201e8d820d8'
UPDATE dbo.TemplateColumn SET VisiblePosition = 4 WHERE ID = '4700636c-b301-41c9-9ee8-8318a90bf186'
UPDATE dbo.TemplateColumn SET ColumnName = N'MaterialGoodsID' WHERE ID = 'e743fd07-feaf-454c-a0c8-863d6cfffb93'
UPDATE dbo.TemplateColumn SET ColumnName = N'CareerGroupID', ColumnCaption = N'Nhóm ngành nghề tính thuế GTGT', ColumnToolTip = N'Nhóm ngành nghề tính thuế GTGT', VisiblePosition = 13 WHERE ID = 'ebcb8be0-5d06-4bc0-a33d-8705d5eb217d'
UPDATE dbo.TemplateColumn SET ColumnName = N'CurrencyID', ColumnCaption = N'Loại tiền', ColumnToolTip = N'', ColumnWidth = 57, VisiblePosition = 8 WHERE ID = 'fa45819e-0c3a-42bb-8b14-88f3c30e0a2d'
UPDATE dbo.TemplateColumn SET ColumnName = N'BudgetItemID', ColumnCaption = N'Mục thu/chi', ColumnToolTip = N'', ColumnWidth = 105, IsVisible = 0 WHERE ID = 'da0d1e31-b0f8-441a-b7e5-8aad76029bd8'
UPDATE dbo.TemplateColumn SET ColumnName = N'CareerGroupID', ColumnCaption = N'Nhóm ngành nghề tính thuế GTGT', ColumnToolTip = N'Nhóm ngành nghề tính thuế GTGT' WHERE ID = '038471e2-1a25-4afe-84f0-91c70bdd759f'
UPDATE dbo.TemplateColumn SET ColumnName = N'UnitPriceConvert' WHERE ID = '5b373c7e-90f0-4048-8595-99528b54b5e7'
UPDATE dbo.TemplateColumn SET ColumnName = N'RepositoryAccount', ColumnCaption = N'TK kho', ColumnWidth = 118, VisiblePosition = 49 WHERE ID = '1b1d6139-3684-481d-addc-9f4b29a8d140'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '7c92c1be-16d8-4b7d-94cf-9ff5b8040d98'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '2070be10-cf42-44ce-9ad2-a366e48d3556'
UPDATE dbo.TemplateColumn SET ColumnName = N'CustomProperty1', ColumnCaption = N'Cột 1', ColumnToolTip = N'', ColumnWidth = 105, VisiblePosition = 13 WHERE ID = '96857a3e-ac43-4554-8768-ae655ddfd1cc'
UPDATE dbo.TemplateColumn SET ColumnName = N'ExchangeRate', ColumnCaption = N'Tỷ giá', ColumnToolTip = N'', ColumnWidth = 63, IsVisible = 0, VisiblePosition = 9 WHERE ID = '23d07985-517c-427f-9620-b9f87137739a'
UPDATE dbo.TemplateColumn SET ColumnName = N'ConfrontInvNo', ColumnCaption = N'Số hóa đơn', ColumnToolTip = N'', IsReadOnly = 1, ColumnWidth = 110, IsVisible = 1 WHERE ID = '74647d3c-2e41-4cda-b207-bc2686c5ac1d'
UPDATE dbo.TemplateColumn SET ColumnName = N'CareerGroupID', ColumnCaption = N'Nhóm ngành nghề tính thuế GTGT', ColumnToolTip = N'Nhóm ngành nghề tính thuế GTGT' WHERE ID = '5f9f2738-2a63-4145-bb0b-bfd9ce74db00'
UPDATE dbo.TemplateColumn SET ColumnName = N'CareerGroupID', ColumnCaption = N'Nhóm ngành nghề tính thuế GTGT', ColumnToolTip = N'Nhóm ngành nghề tính thuế GTGT' WHERE ID = '70ce110e-b02e-4847-a8af-c0aeec2684f4'
UPDATE dbo.TemplateColumn SET ColumnName = N'CostAccount', ColumnCaption = N'TK giá vốn', ColumnToolTip = N'Tài khoản giá vốn', ColumnWidth = 120, VisiblePosition = 48 WHERE ID = '4cfd14a4-ba46-4b2d-9ad4-c4b93948b901'
UPDATE dbo.TemplateColumn SET ColumnName = N'CustomProperty2', ColumnCaption = N'Cột 2', ColumnToolTip = N'', ColumnWidth = 105, IsVisible = 0, VisiblePosition = 14 WHERE ID = 'a7f6e268-0437-4c09-83e8-caa5922737bb'
UPDATE dbo.TemplateColumn SET VisiblePosition = 4 WHERE ID = '8f7d18a2-37a3-4e14-83fb-cb9396f80c8d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 5 WHERE ID = 'ff7c1006-744d-46d2-b202-cd0f5841f7e4'
UPDATE dbo.TemplateColumn SET ColumnName = N'No' WHERE ID = '948caedd-0c38-4c48-a7a4-d12df21e29cf'
UPDATE dbo.TemplateColumn SET ColumnName = N'CareerGroupID', ColumnCaption = N'Nhóm ngành nghề tính thuế GTGT', ColumnToolTip = N'Nhóm ngành nghề tính thuế GTGT', VisiblePosition = 6 WHERE ID = 'faf25057-1119-47c8-a08e-d1f3d4d973e1'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = 'b5162489-ed5c-41d5-8204-d4bbf5ae5561'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '12b2ce67-f657-4249-ab78-d5cad2cd091b'
UPDATE dbo.TemplateColumn SET ColumnName = N'No' WHERE ID = 'f2d7c2d1-b662-41d0-bd6a-d8b48161e834'
UPDATE dbo.TemplateColumn SET ColumnName = N'CareerGroupID', ColumnCaption = N'Nhóm ngành nghề tính thuế GTGT', ColumnToolTip = N'Nhóm ngành nghề tính thuế GTGT' WHERE ID = 'f5eb9cc3-886e-4c64-a318-d9b3a54409cc'
UPDATE dbo.TemplateColumn SET ColumnName = N'UnitPriceConvert' WHERE ID = 'bc67cd7b-32fc-4a98-af2d-e0958596d237'
UPDATE dbo.TemplateColumn SET ColumnName = N'MaterialGoodsID' WHERE ID = '8506c781-e988-4fa6-b9dc-e0f8e6ea4f99'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '0ea77373-fe1e-455d-bdb2-e52fc9a42325'
UPDATE dbo.TemplateColumn SET ColumnName = N'DebitAccount', ColumnCaption = N'TK Nợ', ColumnToolTip = N'Tài khoản Nợ', ColumnWidth = 95 WHERE ID = '4413eab8-bcb3-469b-8c34-f0b40cd97896'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '48d25187-fb06-45b2-bfa6-f47a7045508b'
UPDATE dbo.TemplateColumn SET ColumnName = N'InvoiceTemplate', ColumnCaption = N'Mấu số hóa đơn', ColumnWidth = 115, IsVisible = 0 WHERE ID = '3bc99973-c6a4-4079-a89d-f5ea73d3bb59'
UPDATE dbo.TemplateColumn SET ColumnName = N'StatisticsCodeID', ColumnCaption = N'Mã thống kê', ColumnWidth = 105, IsVisible = 0, VisiblePosition = 51 WHERE ID = '3b6799a9-a955-42a7-836e-f875b0aacf7c'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên hàng' WHERE ID = '2ed4e537-e810-4576-9790-fb364869de49'
UPDATE dbo.TemplateColumn SET ColumnName = N'CareerGroupID', ColumnCaption = N'Nhóm ngành nghề tính thuế GTGT', ColumnToolTip = N'Nhóm ngành nghề tính thuế GTGT', VisiblePosition = 6 WHERE ID = '55eebe29-10c8-4bdf-87ea-fb5d170d250b'
UPDATE dbo.TemplateColumn SET ColumnName = N'RepositoryAccount', ColumnCaption = N'TK kho', ColumnToolTip = N'', ColumnWidth = 118, VisiblePosition = 49 WHERE ID = '685d73a2-ee4e-4c84-adf0-fcbc87efb000'

INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d7ec9d5e-f675-436b-b9b1-0065e325557d', '8dd3bd58-8293-4ae7-80a3-d23dd7774e16', N'ContractID', N'Hợp đồng', N'', 0, 0, 155, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('86773e57-f5a3-482d-b0a9-0222477139a8', '8dd3bd58-8293-4ae7-80a3-d23dd7774e16', N'AccountingObjectID', N'Đối tượng', N'', 0, 0, 155, 0, 0, 1, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2fa6fa0c-0a58-468a-b075-022b1fb6cddb', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'UnitPriceAfterTaxOriginal', N'Đơn giá sau thuế', N'', 0, 0, 120, 0, 0, 0, 17)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('15b7d740-f097-4b3e-a095-03ec9caf6c79', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'DiscountAmount', N'Tiền CK QĐ', N'Tiền chiết khấu quy đổi', 0, 1, 110, 0, 0, 0, 32)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('04f87340-a69b-4835-90d8-06eff3b8f77a', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'UnitPriceAfterTax', N'Đơn giá sau thuế QĐ', N'Đơn giá sau thuế quy đổi', 0, 0, 130, 0, 0, 0, 21)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f2a06705-51a3-4a56-a136-08b43bc4484a', '8dd3bd58-8293-4ae7-80a3-d23dd7774e16', N'MaterialGoodsID', N'Mã hàng', N'', 0, 0, 155, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bd56a142-70bf-43a2-898d-08d44d4bf56e', '8dd3bd58-8293-4ae7-80a3-d23dd7774e16', N'ConvertRate', N'Tỷ lệ chuyển đổi', N'', 0, 0, 115, 0, 0, 0, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a086c8f1-1216-4fba-b69e-0a6ecf98c0e4', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'VATAmount', N'Tiền thuế GTGT', N'Tiền thuế quy đổi', 0, 0, 88, 0, 0, 0, 40)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('689ac31a-fdd4-49d4-8feb-1dfeba793f79', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'LotNo', N'Số lô', N'', 0, 0, 95, 0, 0, 0, 27)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1d6ec30f-df92-4e43-bc15-2013414fb569', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'StatisticsCodeID', N'Mã thống kê', N'', 0, 0, 105, 0, 0, 0, 37)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('51a336cb-3535-45aa-a44f-27cfa50cccfc', '9ddc5656-3c46-4ff0-a4ce-028749f7dd06', N'No', N'Số chưng từ', NULL, 0, 0, 110, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('42d2707d-a0f8-4062-b5a9-2a9bc27233f0', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'DiscountAmountOriginal', N'Tiền CK', N'Tiền chiết khấu', 0, 0, 120, 0, 0, 1, 31)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2d44478d-a606-488c-a75d-2aa326e1e9db', '9ddc5656-3c46-4ff0-a4ce-028749f7dd06', N'Reason', N'Diễn giải', NULL, 0, 0, 250, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e43ff0e7-f0bb-4c4c-8a27-2e3258e59ed2', '8dd3bd58-8293-4ae7-80a3-d23dd7774e16', N'RepositoryID', N'Kho', N'', 0, 0, 105, 0, 0, 0, 30)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b16dbed8-949a-4b32-9b5d-30ce96b2836d', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'PanelWidth', N'Chiều rộng', N'', 0, 0, 110, 0, 0, 0, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bd98193a-07b7-471f-892b-30fe4da4f1f8', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'ExpenseItemID', N'Khoản mục CP', N'Khoản mục chi phí', 0, 0, 130, 0, 0, 0, 15)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('09de217f-f2f4-4db5-8efb-3b4fb8c8c38e', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'DiscountAmountAfterTaxOriginal', N'Tiền CK sau thuế', N'Tiền chiết khấu sau thuế', 0, 0, 110, 0, 0, 0, 33)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('61b937f6-ae0d-4469-bea1-403c44841023', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'ExpiryDate', N'Hạn dùng', N'', 0, 0, 95, 0, 0, 0, 26)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('abd15395-a3f1-435c-8ca0-41d7bccf05bd', '8dd3bd58-8293-4ae7-80a3-d23dd7774e16', N'DepartmentID', N'Phòng ban', N'', 0, 0, 150, 0, 0, 1, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5edc8fe4-7f37-4548-8a46-460cf928a30d', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'AmountAfterTaxOriginal', N'Thành tiền sau thuế', N'', 0, 0, 130, 0, 0, 0, 24)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('23bdeb16-93c4-4cfb-957c-4649e303c38a', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'ConvertRate', N'Tỷ lệ chuyển đổi', N'', 0, 0, 115, 0, 0, 0, 5)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('499ef960-7384-4687-9a19-4716fc7d7deb', '8dd3bd58-8293-4ae7-80a3-d23dd7774e16', N'CustomProperty2', N'Cột 2', NULL, 0, 0, 105, 0, 0, 0, 42)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2c646a2d-99ac-4087-b255-4b59eb2bbd26', '8dd3bd58-8293-4ae7-80a3-d23dd7774e16', N'AmountOriginal', N'Thành tiền', N'', 0, 0, 130, 0, 0, 0, 40)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8212f092-4817-4d11-a37c-4ca1bfd98dd6', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'ContractID', N'Hợp đồng', N'', 0, 0, 105, 0, 0, 0, 30)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1fda95e2-cbf5-47e1-a473-4dafed319d4f', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'RepositoryID', N'Kho', N'', 1, 0, 105, 0, 0, 0, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b66cff99-fa98-4d28-8fc4-5106b08cf54e', 'a8a8b086-24f3-459d-9631-9ffb82418e4f', N'FinalDate', N'Hiệu lực (ngày)', N'', 1, 0, 92, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('5f86a9d5-2ad6-4b3d-8b03-552afa5940e7', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'UnitPriceConvert', N'Đơn giá chuyển đổi QĐ', N'', 0, 0, 140, 0, 0, 0, 19)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e93a4b09-a5b6-4f0d-af66-573ae954b4f6', 'a8a8b086-24f3-459d-9631-9ffb82418e4f', N'CustomProperty2', N'Cột 2', NULL, 0, 0, 105, 0, 0, 0, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d3414aae-2116-4b74-a74d-5abc49a3d3e3', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'PanelQuantity', N'SL tấm', N'Số lượng tấm', 0, 0, 110, 0, 0, 0, 10)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('012dac4c-124e-47de-aa7f-5df6bd0a1885', '9ddc5656-3c46-4ff0-a4ce-028749f7dd06', N'Date', N'Ngày chứng từ', NULL, 0, 0, 110, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('456e7e0a-e9cf-445b-bc0f-672fd6d64a81', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'UnitPrice', N'Đơn giá QĐ', N'', 0, 0, 140, 0, 0, 0, 14)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('89f3407a-9557-4723-ac58-6b3f06cfe4ef', '74e58ffb-76d7-4504-93cd-9ac421d4c9f8', N'CashOutExchangeRate', N'Tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 52)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0c780500-c603-4a14-98e2-6df8cc560c33', '8dd3bd58-8293-4ae7-80a3-d23dd7774e16', N'CustomProperty3', N'Cột 3', NULL, 0, 0, 105, 0, 0, 0, 43)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6459aa60-132e-4811-8fdc-73bd5408c795', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'Quantity', N'Số lượng', N'', 0, 0, 110, 0, 0, 1, 11)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('1cc60142-b29d-42b3-ac01-73c1270ece38', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'CustomProperty3', N'Cột 3', NULL, 0, 0, 105, 0, 0, 0, 43)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d2187176-d67a-4cae-893e-7b6d39d53a9c', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'AmountAfterTax', N'Thành tiền sau thuế QĐ', N'Thành tiền sau thuế quy đổi', 0, 0, 140, 0, 0, 0, 25)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4b59a989-39dd-4f8b-abda-7e00c1604361', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'Unit', N'ĐVT', N'Đơn vị tính', 1, 0, 75, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('72756e4c-da6c-4800-8008-7fd4c0b16b65', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'AmountOriginal', N'Thành tiền', N'', 0, 0, 130, 0, 0, 1, 22)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e39cbe78-583d-448a-9393-804d2e3ee0a2', 'a8a8b086-24f3-459d-9631-9ffb82418e4f', N'CustomProperty1', N'Cột 1', NULL, 0, 0, 105, 0, 0, 0, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c05c2a2c-009a-464e-8bfa-8214fd763e17', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'CustomProperty1', N'Cột 1', NULL, 0, 0, 105, 0, 0, 0, 41)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('255f2bd2-653d-494c-a2f3-8c859232cce9', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'AccountingObjectID', N'Đối tượng', N'', 0, 0, 105, 0, 0, 0, 35)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('60a8fc6f-f673-47b5-bed4-8c94d20fc1cf', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'VATAccount', N'TK Thuế', N'Tài khoản thuế', 0, 0, 95, 0, 0, 0, 38)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9ac1098e-92dd-44ce-8ee8-8f1e95255f6c', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'PanelLength', N'Chiều dài', N'', 0, 0, 110, 0, 0, 0, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4d1d797e-f88e-40cb-9192-9794378e95e7', 'a8a8b086-24f3-459d-9631-9ffb82418e4f', N'CurrencyID', N'Loại tiền', N'', 1, 0, 74, 0, 0, 1, 0)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bf9d5713-ba00-4c16-948a-9abd4f45f15b', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'UnitPriceConvertOriginal', N'Đơn giá chuyển đổi', N'', 0, 0, 140, 0, 0, 0, 20)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9d4445db-2515-4d90-b1a8-9ade9161e4a0', '8dd3bd58-8293-4ae7-80a3-d23dd7774e16', N'Description', N'Diễn giải', N'', 0, 0, 455, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('03c5b280-5610-4fc7-ba33-9fde21aa48af', '8dd3bd58-8293-4ae7-80a3-d23dd7774e16', N'ExpenseItemID', N'Khoản mục CP', N'Khoản mục chi phí', 0, 0, 130, 0, 0, 0, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bfc4307d-5367-48d4-afde-a0682c2189b1', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'UnitPriceOriginal', N'Đơn giá', N'', 0, 0, 110, 0, 0, 1, 13)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('79d6e371-aeca-4753-99f4-a0e1073de0c4', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'PanelHeight', N'Chiều cao', N'', 0, 0, 110, 0, 0, 0, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('eb454c39-2898-4ca8-9c18-a189149f3865', '74e58ffb-76d7-4504-93cd-9ac421d4c9f8', N'CashOutDifferAccount', N'TK xử lý chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 55)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ac71d443-cce6-479d-8cfc-a2e3aa4fe83f', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'DiscountAmountAfterTax', N'Tiền CK sau thuế QĐ', N'Tiền chiết khấu sau thuế quy đổi', 0, 0, 130, 0, 0, 0, 34)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0a959401-4667-4e7d-9250-a452e3a5d447', '8dd3bd58-8293-4ae7-80a3-d23dd7774e16', N'CostSetID', N'ĐT tập hợp CP', N'Đối tượng tập hợp chi phí', 0, 0, 150, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ac9ce1fc-7437-496b-ba56-aa3e501fe7f8', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'CustomProperty2', N'Cột 2', NULL, 0, 0, 105, 0, 0, 0, 42)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c25f964d-dc82-47bf-850c-aeb8086950b7', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'DepartmentID', N'Phòng ban', N'', 0, 0, 150, 0, 0, 0, 18)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ef1e8782-28d1-4c1e-9f0e-b4196849caa7', 'a8a8b086-24f3-459d-9631-9ffb82418e4f', N'EmployeeID', N'Nhân viên bán hàng', N'', 0, 0, 115, 0, 0, 1, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6a09d9f2-2426-4a2b-a2c5-b6b8cc634afe', 'a8a8b086-24f3-459d-9631-9ffb82418e4f', N'CustomProperty3', N'Cột 3', NULL, 0, 0, 105, 0, 0, 0, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('80ba0a09-e01a-416f-b020-b7e6fcf324d3', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'DiscountRate', N'Tỷ lệ CK', N'Tỷ lệ chiết khấu', 0, 0, 95, 0, 0, 1, 29)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3b70f615-cf43-405f-b804-baca19d21e34', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'Description', N'Tên hàng', N'', 1, 0, 455, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('ce427051-8a18-4d58-9e66-bb7fc8b5a75a', 'a8a8b086-24f3-459d-9631-9ffb82418e4f', N'PaymentClauseID', N'Điều khoản TT', N'Điều khoản thanh toán', 0, 0, 120, 0, 0, 1, 2)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('134dba43-2564-44b4-af07-bc067c0bbcb4', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'DiscountAccount', N'TK Chiết khấu', N'Tài khoản chiết khấu', 0, 0, 110, 0, 0, 0, 28)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('98c761f9-4355-4a66-a351-be32c191bae7', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'VATAmountOriginal', N'Tiền thuế', N'', 0, 0, 105, 0, 0, 0, 39)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e487ecf2-ebae-4f3b-a342-c11121120114', '8dd3bd58-8293-4ae7-80a3-d23dd7774e16', N'Amount', N'Quy đổi', N'', 0, 1, 79, 0, 0, 0, 26)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e5821730-beaa-41ce-b32e-c39af08cf5b3', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'VATRate', N'Thuế suất', N'', 0, 0, 100, 0, 0, 0, 36)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('803eac4e-9649-4fd6-bab3-c5cd26752461', '8dd3bd58-8293-4ae7-80a3-d23dd7774e16', N'StatisticsCodeID', N'Mã thống kê', N'', 0, 0, 155, 0, 0, 1, 10)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('4815bbeb-135a-47ed-9352-c764495e447d', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'MaterialGoodsID', N'Mã hàng', N'', 1, 0, 155, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('60e870d1-7abb-489c-8542-c7e85a94ffd3', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'CostSetID', N'ĐT tập hợp CP', N'Đối tượng tập hợp chi phí', 0, 0, 150, 0, 0, 0, 45)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bfeb8481-962b-415d-af04-cfd282ebf02f', '9ddc5656-3c46-4ff0-a4ce-028749f7dd06', N'PostedDate', N'Ngày hạch toán', NULL, 0, 0, 110, 0, 0, 1, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2e25f6ec-584e-4d89-bbab-d994f24c5195', '8dd3bd58-8293-4ae7-80a3-d23dd7774e16', N'BudgetItemID', N'Mục thu/chi', N'', 0, 0, 105, 0, 0, 0, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('c619a8bc-b92c-42e9-934b-e2f86ed95d08', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'UnitConvert', N'Đơn vị chuyển đổi', NULL, 0, 0, 106, 0, 0, 0, 6)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8d8e865f-1282-47e3-a389-e9c2be171ac8', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'QuantityConvert', N'SL chuyển đổi', N'Số lượng chuyển đổi', 0, 0, 130, 0, 0, 0, 12)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bd0d1bea-b471-4073-b235-ea753b7eb9d4', '74e58ffb-76d7-4504-93cd-9ac421d4c9f8', N'CashOutAmount', N'QĐ theo tỷ giá xuất quỹ', NULL, 0, 1, 105, 0, 0, 0, 53)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f23ec6c0-6d05-44f6-b435-ef1738ebf119', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'Amount', N'Quy đổi', N'', 0, 1, 79, 0, 0, 0, 23)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('f6750496-a643-434e-bdc4-efa0529b999f', 'a8a8b086-24f3-459d-9631-9ffb82418e4f', N'ExchangeRate', N'Tỷ giá', N'', 1, 0, 75, 0, 0, 0, 1)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('df923857-d4f8-468e-bec1-f2817e5c6fe7', 'cfc107f7-aa2f-49ec-8533-fae83a2599ba', N'BudgetItemID', N'Mục thu/chi', N'', 0, 0, 105, 0, 0, 0, 16)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9d57c0d1-d3b8-459b-883a-f4e3ad15975b', '8dd3bd58-8293-4ae7-80a3-d23dd7774e16', N'CustomProperty1', N'Cột 1', NULL, 0, 0, 105, 0, 0, 0, 41)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bf9961d8-cd89-4b7b-8995-fdcee626ca91', '74e58ffb-76d7-4504-93cd-9ac421d4c9f8', N'CashOutDifferAmount', N'Chênh lệch', NULL, 0, 1, 105, 0, 0, 0, 54)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e0e44d71-feca-4c83-b69f-ffcd4cbfd453', '96a56301-3762-4310-82cd-39f2fae07538', N'InvoiceNo', N'Số hóa đơn', N'Số hóa đơn', 0, 0, 81, 0, 0, 0, 5)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.TemplateDetail WHERE ID = '819eab40-ad3c-452a-aeae-2ef865b03713'
DELETE dbo.TemplateDetail WHERE ID = '07f0a968-e35c-4b05-84ac-caed2a29d092'

UPDATE dbo.TemplateDetail SET TabIndex = 1, TabCaption = N'&2. Thống kê' WHERE ID = '00431e02-303b-4d21-af69-1c2b8ad546bc'
UPDATE dbo.TemplateDetail SET TabIndex = 1, TabCaption = N'&2.Thống kê' WHERE ID = '720e529e-b41b-491c-95ec-bb301ab83355'

INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('9ddc5656-3c46-4ff0-a4ce-028749f7dd06', '2d5a2a11-06e7-440d-ba5b-b9b2a67d951c', 2, 2, N'&3. Chưng từ tham chiếu')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('a8a8b086-24f3-459d-9631-9ffb82418e4f', '774c10b8-d918-454f-af43-d4d5805a0ef3', 0, 100, NULL)
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('8dd3bd58-8293-4ae7-80a3-d23dd7774e16', '774c10b8-d918-454f-af43-d4d5805a0ef3', 1, 1, N'&2. Thống kê')
INSERT dbo.TemplateDetail(ID, TemplateID, GridType, TabIndex, TabCaption) VALUES ('cfc107f7-aa2f-49ec-8533-fae83a2599ba', '774c10b8-d918-454f-af43-d4d5805a0ef3', 1, 0, N'&1. Hàng tiền')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

SET IDENTITY_INSERT dbo.VoucherPatternsReport ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (260, 0, 0, N'BangChamCong', N'Bảng chấm công', N'BangChamCong.rst', 1, N'821,820')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (261, 0, 0, N'04-GTGT', N'Tờ khai thuế GTGT 04', N'ToKhaiThueGiaTriGiaTangTM04.rst', 1, NULL)
GO
SET IDENTITY_INSERT dbo.VoucherPatternsReport OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DBCC CHECKIDENT('dbo.VoucherPatternsReport', RESEED, 261)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Template WITH NOCHECK
  ADD CONSTRAINT FK_Template_Type FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateDetail WITH NOCHECK
  ADD CONSTRAINT FK_TemplateDetail_Template FOREIGN KEY (TemplateID) REFERENCES dbo.Template(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF