SET CONCAT_NULL_YIELDS_NULL, ANSI_NULLS, ANSI_PADDING, QUOTED_IDENTIFIER, ANSI_WARNINGS, ARITHABORT, XACT_ABORT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS OFF
GO

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO

DISABLE TRIGGER [dbo].[trgInsteadOfOperation] ON [dbo].[TemplateColumn]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SABillDetail]
  ADD [LotNo] [nvarchar](50) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SABillDetail]
  ADD [ExpiryDate] [datetime] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER FUNCTION [dbo].[Func_GetBalanceAccountF01]
      (
        @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT ,
        @IsBalanceBothSide BIT
      )
RETURNS @Result TABLE
      (
        AccountID UNIQUEIDENTIFIER ,
        DetailByAccountObject BIT ,
        AccountObjectType INT ,
        AccountNumber NVARCHAR(50) ,
        AccountName NVARCHAR(255) ,
        AccountNameEnglish NVARCHAR(255) ,
        AccountCategoryKind INT ,
        IsParent BIT ,
        ParentID UNIQUEIDENTIFIER ,
        Grade INT ,
        AccountKind INT ,
        SortOrder INT ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
      )
AS
    BEGIN
	   DECLARE @MainCurrency NVARCHAR(3)
        SET @MainCurrency = 'VND'	
        /*ngày bắt đầu chương trình*/
        DECLARE @StartDate DATETIME
        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'
        IF @IsBalanceBothSide = 1
           BEGIN
                 DECLARE @GeneralLedger TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           AccountObjectID UNIQUEIDENTIFIER ,
                           BranchID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(3) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4) ,
                           OpenningDebitAmountOC DECIMAL(25, 4) ,
                           DebitAmountOC DECIMAL(25, 4) ,
                           CreditAmountOC DECIMAL(25, 4) ,
                           DebitAmountOCAccum DECIMAL(25, 4) ,
                           CreditAmountOCAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedger
                        SELECT  G.*
                        FROM    (
                                  SELECT    Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                                     ELSE 0
                                                END) AS OpenningDebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal - GL.CreditAmountOriginal
                                                     ELSE 0
                                                END) AS OpenningDebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOCAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOCAccum
                                  FROM      dbo.GeneralLedger AS GL
                                  WHERE    GL.PostedDate <= @ToDate
                                  GROUP BY  Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID
                                ) G
			
                 DECLARE @Data TABLE
                         (
                           AccountNumber NVARCHAR(50) ,
						   AccountObjectID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(20) ,
                           SortOrder INT ,
                           OpeningDebitAmount DECIMAL(25, 4) ,
                           OpeningCreditAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,/*Nợ lũy kế*/
                           CreditAmountAccum DECIMAL(25, 4) ,/*có lũy kế*/
                           ClosingDebitAmount DECIMAL(25, 4) ,
                           ClosingCreditAmount DECIMAL(25, 4)
                         )
	
                 INSERT @Data
                        SELECT  F.AccountNumber ,
						        null,
                                '' ,
                                0 ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount > 0
                                                 ) THEN F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount < 0
                                                 ) THEN -1 * F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount ,
                                SUM(F.DebitAmount) AS DebitAmount ,
                                SUM(F.CreditAmount) AS CreditAmount ,
                                SUM(F.DebitAmountAccum) AS DebitAmountAccum ,
                                SUM(F.CreditAmountAccum) AS CreditAmountAccum ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                                 ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                                 ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.AccountNumber ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            CASE A.DetailByAccountObject
                                                                  WHEN 0 THEN NULL
                                                                  ELSE GL.AccountObjectID
                                                                END AS AccountObjectID ,
                                            A.AccountGroupKind AccountCategoryKind ,
                                            GL.BranchID BranchID
                                  FROM      @GeneralLedger AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber = A.AccountNumber
                                  WHERE     GL.AccountNumber NOT LIKE '0%'
                                            AND A.IsParentNode = 0
                                  GROUP BY  A.AccountNumber ,
                                            A.AccountGroupKind ,
                                            GL.BranchID,
											CASE A.DetailByAccountObject
                                                                  WHEN 0 THEN NULL
                                                                  ELSE GL.AccountObjectID
                                                                END
                                  HAVING    SUM(OpenningDebitAmount) <> 0
                                            OR SUM(DebitAmount) <> 0
                                            OR SUM(CreditAmount) <> 0
                                ) AS F
                        GROUP BY F.AccountNumber ,
                                F.AccountCategoryKind,
								F.AccountObjectID
                 OPTION ( RECOMPILE )		         					    
                 INSERT @Result
                        SELECT  A.ID ,
						        null,
								null,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                0 AccountKind,
                                D.SortOrder ,
                                SUM(D.OpeningDebitAmount) ,
                                SUM(D.OpeningCreditAmount) ,
                                SUM(D.DebitAmount) ,
                                SUM(D.CreditAmount) ,
                                SUM(D.DebitAmountAccum) ,
                                SUM(D.CreditAmountAccum) ,
                                SUM(D.ClosingDebitAmount) ,
                                SUM(D.ClosingCreditAmount)
                        FROM    @Data D
                        INNER JOIN dbo.Account A ON D.AccountNumber LIKE A.AccountNumber + '%'
                        WHERE   A.Grade <= @MaxAccountGrade
                        GROUP BY A.ID ,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                D.SortOrder
                        HAVING  SUM(D.OpeningDebitAmount) <> 0
                                OR SUM(D.OpeningCreditAmount) <> 0
                                OR SUM(D.DebitAmount) <> 0
                                OR SUM(D.CreditAmount) <> 0
                                OR SUM(D.ClosingDebitAmount) <> 0
                                OR SUM(D.ClosingCreditAmount) <> 0
                 OPTION ( RECOMPILE )
           END
        ELSE
           BEGIN
                 DECLARE @GeneralLedgerP1 TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedgerP1
                        SELECT  Account ,
                                SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmountAccum ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmountAccum
                        FROM    dbo.GeneralLedger AS GL
                        WHERE   GL.PostedDate <= @ToDate
                        GROUP BY Account
                 INSERT @Result
                        SELECT  A.ID ,
                                null DetailByAccountObject ,
                                null AccountObjectType,
                                A.AccountNumber ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade AS Grade ,
                                F.AccountKind,
                                F.SortOrder ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount > 0
                                               ) THEN F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount < 0
                                               ) THEN -1 * F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount ,
                                ( F.DebitAmount ) AS DebitAmount ,
                                ( F.CreditAmount ) AS CreditAmount ,
                                ( F.DebitAmountAccum ) AS DebitAmountAccum ,
                                ( F.CreditAmountAccum ) AS CreditAmountAccum ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                               ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                               ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.ID ,
                                            null DetailByAccountObject,
                                            null AccountObjectType,
                                            A.AccountNumber ,
                                            '' AS CurrencyID ,
                                            0 AS SortOrder ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            0 AS AccountKind
                                  FROM      @GeneralLedgerP1 AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber LIKE A.AccountNumber + '%'
                                  WHERE     A.Grade <= @MaxAccountGrade
                                            AND (
                                                  OpenningDebitAmount <> 0
                                                  OR DebitAmount <> 0
                                                  OR CreditAmount <> 0
                                                )
                                  GROUP BY  A.ID ,
                                            A.AccountNumber      
                                ) AS F
                        INNER JOIN Account A ON F.AccountNumber = A.AccountNumber
                 OPTION ( RECOMPILE )      
           END
        RETURN
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER FUNCTION [dbo].[Func_GetB03_DN]
(
	@BranchID UNIQUEIDENTIFIER
	,@IncludeDependentBranch BIT
	,@FromDate DATETIME
	,@ToDate DATETIME
	,@PrevFromDate DATETIME
	,@PrevToDate DATETIME	
)
RETURNS 
@Result TABLE 
(	
		ItemID UNIQUEIDENTIFIER
      ,ItemCode NVARCHAR(25)
      ,ItemName NVARCHAR(255)
      ,ItemNameEnglish NVARCHAR(255)
      ,ItemIndex INT
      ,Description NVARCHAR(255)
      ,FormulaType INT
      ,FormulaFrontEnd NVARCHAR(MAX)
      ,Formula XML
      ,Hidden BIT
      ,IsBold BIT
      ,IsItalic BIT
      ,Amount DECIMAL(25,4)
      ,PrevAmount  DECIMAL(25,4)    
)
AS
BEGIN

	DECLARE @AccountingSystem INT	
    SET @AccountingSystem = 48
    
    set @PrevToDate = DATEADD(DD,-1,@FromDate)
	declare @CalPrevDate nvarchar(20)
	declare @CalPrevMonth nvarchar(20)
	declare @CalPrevDay nvarchar(20)
	set @CalPrevDate = CAST(day(@FromDate) AS varchar) + CAST(month(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevMonth = CAST(month(@FromDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevDay = CAST(day(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar)
	if @CalPrevDate = '113112'
	 set @PrevFromDate = convert(DATETIME,dbo.ctod('D',dbo.godtos('M', -12, @FromDate)),103)
	else if (@CalPrevMonth in (55,77,1010,1212) and @CalPrevDay in (131) )
	 set @PrevFromDate = DATEADD(DD,-30,@FromDate)
	else if (@CalPrevMonth in (11,22,88,44,66,99,1111) and @CalPrevDay in (131,130) )
	 set @PrevFromDate = DATEADD(DD,-31,@FromDate)
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 = 0)
	 set @PrevFromDate = DATEADD(DD,-29,@FromDate)
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 <> 0)
	 set @PrevFromDate = DATEADD(DD,-28,@FromDate)
	else if ( (@CalPrevMonth in (13,1012) and @CalPrevDay = '131') or (@CalPrevMonth in (46,79) and @CalPrevDay = '130') )
	 set @PrevFromDate = DATEADD(QQ,-1,@FromDate)
	else set @PrevFromDate = DATEADD(DD,-1-DATEDIFF(day, @FromDate, @ToDate),@FromDate)

	DECLARE @ReportID NVARCHAR(100)
	SET @ReportID = '3'
	
	DECLARE @ItemBeginingCash UNIQUEIDENTIFIER	

	SET @ItemBeginingCash = (Select ItemID From FRTemplate Where ItemCode = '60' AND ReportID = 3 AND AccountingSystem = @AccountingSystem)

	DECLARE @tblItem TABLE
	(
		ItemID				UNIQUEIDENTIFIER
		,ItemIndex			INT
		,ItemName			NVARCHAR(255)	
		,OperationSign		INT
		,OperandString		NVARCHAR(255)	
		,AccountNumber		NVARCHAR(25)
		,AccountNumberPercent NVARCHAR(25) 
		,CorrespondingAccountNumber	VARCHAR(25)
	)
	
	INSERT @tblItem
	SELECT ItemID 
		, ItemIndex
		, ItemName
		, x.r.value('@OperationSign','INT')
		, x.r.value('@OperandString','nvarchar(255)')
		, x.r.value('@AccountNumber','nvarchar(25)') 
		, x.r.value('@AccountNumber','nvarchar(25)') + '%'
		, CASE WHEN x.r.value('@CorrespondingAccountNumber','nvarchar(25)') <>'' 
			THEN x.r.value('@CorrespondingAccountNumber','nvarchar(25)') + '%'
			ELSE ''
		 END
	FROM dbo.FRTemplate
	CROSS APPLY Formula.nodes('/root/DetailFormula') AS x(r)
	WHERE AccountingSystem = @AccountingSystem
		  AND ReportID = @ReportID
		  AND FormulaType = 0 AND Formula IS NOT NULL 
		  
	DECLARE @Balance TABLE 
		(
			AccountNumber NVARCHAR(25)	
			,CorrespondingAccountNumber NVARCHAR(25)		
			,PrevBusinessAmount DECIMAL(25,4)
			,BusinessAmount DECIMAL(25,4)							
			,PrevInvestmentAmount DECIMAL(25,4)
			,InvestmentAmount DECIMAL(25,4)
			,PrevFinancialAmount DECIMAL(25,4)
			,FinancialAmount DECIMAL(25,4)
		)			
		
	INSERT INTO @Balance			
	SELECT 
		GL.Account
		,GL.AccountCorresponding		
		,SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND(AC.ActivityID IS NULL OR AC.ActivityID = 0) 
				THEN DebitAmount  ELSE 0 END) AS PrevBusinessAmount
		,SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND (AC.ActivityID IS NULL OR AC.ActivityID = 0) 
				THEN DebitAmount ELSE 0 END) AS BusinessAmount		
		,SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND  AC.ActivityID = 1 
				THEN DebitAmount  ELSE 0 END) AS PrevInvestmentAmount
		,SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 1 
				THEN DebitAmount ELSE 0 END) AS InvestmentAmount		
		,SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND AC.ActivityID = 2 
				THEN DebitAmount  ELSE 0 END) AS PrevFinancialAmount
		,SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 2 
				THEN DebitAmount ELSE 0 END) AS FinancialAmount		
	FROM dbo.GeneralLedger GL
		INNER JOIN dbo.Account A ON GL.Account = A.AccountNumber
		LEFT JOIN [dbo].[FRB03ReportDetailActivity] AC   ON GL.DetailID = Ac.RefDetailID
													/*Khi JOIN thêm điều kiện sổ*/
	WHERE PostedDate BETWEEN @PrevFromDate AND @ToDate	
	and GL.No <> 'OPN'
	GROUP BY
		GL.Account
		,GL.AccountCorresponding
	HAVING
		SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND(AC.ActivityID IS NULL OR AC.ActivityID = 0) 
			THEN DebitAmount  ELSE 0 END)<>0
		OR SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND (AC.ActivityID IS NULL OR AC.ActivityID = 0) 
			THEN DebitAmount ELSE 0 END) <>0
		OR SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND  AC.ActivityID = 1 
			THEN DebitAmount  ELSE 0 END) <>0
		OR SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 1 
			THEN DebitAmount ELSE 0 END)<>0
		OR SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND AC.ActivityID = 2 
			THEN DebitAmount  ELSE 0 END) <>0
		OR SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 2 
			THEN DebitAmount ELSE 0 END) <>0
	
	DECLARE @DebitBalance TABLE
	(	
		ItemID				UNIQUEIDENTIFIER
		,OperationSign		INT
		,AccountNumber NVARCHAR(25)
		,AccountKind	INT
		,PrevDebitAmount Decimal(25,4)
		,DebitAmount Decimal(25,4)
	)
	
	INSERT @DebitBalance
	SELECT
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
		,SUM(CASE WHEN  GL.PostedDate < @PrevFromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0 END)	
		,SUM(GL.DebitAmount - GL.CreditAmount)		
	FROM  dbo.GeneralLedger AS GL
	INNER JOIN @tblItem I ON GL.Account LIKE I.AccountNumberPercent	
	LEFT JOIN dbo.Account A ON A.AccountNumber = I.AccountNumber
	WHERE GL.PostedDate < @FromDate	
	    AND I.OperandString = 'DUNO'
	    AND I.ItemID = @ItemBeginingCash
	Group By
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
	
	UNION ALL	
	SELECT
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
		,SUM(CASE WHEN  GL.PostedDate <= @PrevToDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0 END)	
		,SUM(GL.DebitAmount - GL.CreditAmount)		
	FROM  dbo.GeneralLedger AS GL
	INNER JOIN @tblItem I ON GL.Account LIKE I.AccountNumberPercent	
	LEFT JOIN dbo.Account A ON A.AccountNumber = I.AccountNumber
	WHERE GL.PostedDate <= @ToDate
	    AND I.OperandString = 'DUNO'
	    AND I.ItemID <> @ItemBeginingCash
	Group By
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
		
		   
	UPDATE @DebitBalance
	SET PrevDebitAmount = 0 WHERE AccountKind = 1 OR (AccountKind NOT IN (0,1)  AND PrevDebitAmount < 0) 
	
	UPDATE @DebitBalance
	SET DebitAmount = 0 WHERE AccountKind = 1 OR (AccountKind NOT IN (0,1)  AND DebitAmount < 0) 
	
	DECLARE @tblMasterDetail Table
		(
			ItemID UNIQUEIDENTIFIER
			,DetailItemID UNIQUEIDENTIFIER
			,OperationSign INT
			,Grade INT
			,PrevAmount DECIMAL(25,4)
			,Amount DECIMAL(25,4)		
		)	
	
	
	INSERT INTO @tblMasterDetail
	SELECT I.ItemID
			,NULL
			,1
			,-1
			,SUM(I.PrevAmount)
			,SUM(I.Amount)
	FROM	
		(SELECT			
			I.ItemID
			,SUM(CASE				
					WHEN I.OperandString = 'PhatsinhDU' THEN b.PrevBusinessAmount + b.PrevInvestmentAmount + b.PrevFinancialAmount
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_DAUTU' THEN b.PrevInvestmentAmount
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_TAICHINH' THEN b.PrevFinancialAmount
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_SXKD' THEN b.PrevBusinessAmount
				 END * I.OperationSign) AS  PrevAmount			
			,SUM(CASE 
					WHEN I.OperandString = 'PhatsinhDU' THEN (b.BusinessAmount + b.InvestmentAmount + b.FinancialAmount)
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_DAUTU' THEN (b.InvestmentAmount)
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_TAICHINH' THEN (b.FinancialAmount)
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_SXKD' THEN (b.BusinessAmount)
			    END * I.OperationSign) AS Amount
			FROM @tblItem I 			
				 INNER JOIN @Balance B 
					ON (B.AccountNumber like I.AccountNumberPercent)
					AND (B.CorrespondingAccountNumber like I.CorrespondingAccountNumber)	
				WHERE I.OperandString IN ('PhatsinhDU','PhatsinhDUChiTietTheoHD_DAUTU','PhatsinhDUChiTietTheoHD_TAICHINH','PhatsinhDUChiTietTheoHD_SXKD')					
			GROUP BY I.ItemID
		UNION ALL
			SELECT
		    I.ItemID
			,SUM((b.PrevBusinessAmount + b.PrevInvestmentAmount + b.PrevFinancialAmount)* I.OperationSign) AS  PrevAmount			
			,SUM((b.BusinessAmount + b.InvestmentAmount + b.FinancialAmount)* I.OperationSign) AS Amount
			FROM @tblItem I 
				 INNER JOIN @Balance B 
					ON (B.AccountNumber like I.AccountNumberPercent)
			WHERE I.OperandString = 'PhatsinhNO'				
			GROUP BY I.ItemID
		UNION ALL
			SELECT
		    I.ItemID
			,SUM((b.PrevBusinessAmount + b.PrevInvestmentAmount + b.PrevFinancialAmount)* I.OperationSign) AS  PrevAmount			
			,SUM((b.BusinessAmount + b.InvestmentAmount + b.FinancialAmount)* I.OperationSign) AS Amount
			FROM @tblItem I 
				 INNER JOIN @Balance B 
					ON (B.CorrespondingAccountNumber like I.AccountNumberPercent)
			WHERE I.OperandString = 'PhatsinhCO'		
					
			GROUP BY I.ItemID
			
		UNION ALL
			SELECT I.ItemID
				,I.PrevDebitAmount * I.OperationSign
				,I.DebitAmount * I.OperationSign
			FROM @DebitBalance I
		 ) AS I
	Group By I.ITemID
		
	
		
	
	       
	    
	INSERT @tblMasterDetail	
	SELECT ItemID 
	, x.r.value('@ItemID','NVARCHAR(100)')
	, x.r.value('@OperationSign','INT')	
	, 0
	, 0.0	
	, 0.0
	FROM dbo.FRTemplate 
	CROSS APPLY Formula.nodes('/root/MasterFormula') AS x(r)
	WHERE AccountingSystem = @AccountingSystem
		  AND ReportID = @ReportID
		  AND FormulaType = 1 AND Formula IS NOT NULL 
	
	;
	WITH V(ItemID,DetailItemID,PrevAmount,Amount,OperationSign)
		AS 
		(
			SELECT ItemID,DetailItemID,PrevAmount, Amount, OperationSign
			FROM @tblMasterDetail WHERE Grade = -1 
			UNION ALL
			SELECT B.ItemID
				, B.DetailItemID				
				, V.PrevAmount
				, V.Amount
				, B.OperationSign * V.OperationSign AS OperationSign
			FROM @tblMasterDetail B, V 
			WHERE B.DetailItemID = V.ItemID	
		)
		
	INSERT @Result      
	SELECT 
		FR.ItemID
		, FR.ItemCode
		, FR.ItemName
		, FR.ItemNameEnglish
		, FR.ItemIndex
		, FR.Description		
		, FR.FormulaType
		, CASE WHEN FR.FormulaType = 1 THEN FR.FormulaFrontEnd	ELSE ''	END AS FormulaFrontEnd
		, CASE WHEN FR.FormulaType = 1 THEN FR.Formula ELSE NULL END AS Formula
		, FR.Hidden
		, FR.IsBold
		, FR.IsItalic
		,CASE WHEN FR.Formula IS NOT NULL THEN ISNULL(X.Amount,0)
			ELSE X.Amount
		 END AS Amount
		,CASE WHEN FR.Formula IS NOT NULL THEN ISNULL(X.PrevAmount,0) 
			ELSE X.PrevAmount 
		END AS PrevAmount
		FROM		
		(
			SELECT V.ItemID
				,SUM(V.OperationSign * V.Amount)  AS Amount
				,SUM(V.OperationSign * V.PrevAmount) AS PrevAmount
			FROM V
			GROUP BY ItemID
		) AS X		
		RIGHT JOIN dbo.FRTemplate FR ON FR.ItemID = X.ItemID	
	WHERE AccountingSystem = @AccountingSystem
		  AND ReportID = @ReportID
		  and Hidden = 0
	Order by ItemIndex	
	RETURN 
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER FUNCTION [dbo].[Func_GetB02_DN]
    (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @PrevFromDate DATETIME ,
      @PrevToDate DATETIME
    )
RETURNS @Result TABLE
    (
      ItemID UNIQUEIDENTIFIER ,
      ItemCode NVARCHAR(25) ,
      ItemName NVARCHAR(255) ,
      ItemNameEnglish NVARCHAR(255) ,
      ItemIndex INT ,
      Description NVARCHAR(255) ,
      FormulaType INT ,
      FormulaFrontEnd NVARCHAR(MAX) ,
      Formula XML ,
      Hidden BIT ,
      IsBold BIT ,
      IsItalic BIT
      ,
      Amount DECIMAL(25, 4) ,
      PrevAmount DECIMAL(25, 4)
    )
AS 
    BEGIN
	DECLARE @AccountingSystem INT	
            SET @AccountingSystem = 48
        DECLARE @ReportID NVARCHAR(100)
        SET @ReportID = '2'/*báo cáo kết quả HĐ kinh doanh*/
    set @PrevToDate = DATEADD(DD,-1,@FromDate)
	declare @CalPrevDate nvarchar(20)
	declare @CalPrevMonth nvarchar(20)
	declare @CalPrevDay nvarchar(20)
	set @CalPrevDate = CAST(day(@FromDate) AS varchar) + CAST(month(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevMonth = CAST(month(@FromDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevDay = CAST(day(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar)
	if @CalPrevDate = '113112'
	 set @PrevFromDate = convert(DATETIME,dbo.ctod('D',dbo.godtos('M', -12, @FromDate)),103)
	else if (@CalPrevMonth in (55,77,1010,1212) and @CalPrevDay in (131) )
	 set @PrevFromDate = DATEADD(DD,-30,@FromDate)
	else if (@CalPrevMonth in (11,22,88,44,66,99,1111) and @CalPrevDay in (131,130) )
	 set @PrevFromDate = DATEADD(DD,-31,@FromDate)
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 = 0)
	 set @PrevFromDate = DATEADD(DD,-29,@FromDate)
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 <> 0)
	 set @PrevFromDate = DATEADD(DD,-28,@FromDate)
	else if ( (@CalPrevMonth in (13,1012) and @CalPrevDay = '131') or (@CalPrevMonth in (46,79) and @CalPrevDay = '130') )
	 set @PrevFromDate = DATEADD(QQ,-1,@FromDate)
	else set @PrevFromDate = DATEADD(DD,-1-DATEDIFF(day, @FromDate, @ToDate),@FromDate)
	
        DECLARE @tblItem TABLE
            (
              ItemID UNIQUEIDENTIFIER ,
              ItemName NVARCHAR(255) ,
              OperationSign INT ,
              OperandString NVARCHAR(255) ,
              AccountNumber VARCHAR(25) ,
              CorrespondingAccountNumber VARCHAR(25) ,
              IsDetailGreaterThanZero BIT
	      )
	
        INSERT  @tblItem
                SELECT  ItemID ,
                        ItemName ,
                        x.r.value('@OperationSign', 'INT') ,
                        x.r.value('@OperandString', 'nvarchar(255)') ,
                        x.r.value('@AccountNumber', 'nvarchar(25)') + '%' ,
                        CASE WHEN x.r.value('@CorrespondingAccountNumber',
                                            'nvarchar(25)') <> ''
                             THEN x.r.value('@CorrespondingAccountNumber',
                                            'nvarchar(25)') + '%'
                             ELSE ''
                        END ,
                        CASE WHEN FormulaType = 0 THEN CAST(0 AS BIT)
                             ELSE CAST(1 AS BIT)
                        END
                FROM    FRTemplate
                        CROSS APPLY Formula.nodes('/root/DetailFormula') AS x ( r )
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
                        AND ( FormulaType = 0/*chỉ tiêu chi tiết*/
                              OR FormulaType = 2/*chỉ tiêu chi tiết chỉ được lấy số liệu khi kết quả>0*/
                            )
                        AND Formula IS NOT NULL
	
        DECLARE @Balance TABLE
            (
              AccountNumber NVARCHAR(25) ,
              CorrespondingAccountNumber NVARCHAR(25) ,
              PostedDate DATETIME ,
              CreditAmount DECIMAL(25, 4) ,
              DebitAmount DECIMAL(25, 4) ,
              CreditAmountDetailBy DECIMAL(25, 4) ,
              DebitAmountDetailBy DECIMAL(25, 4) ,
              IsDetailBy BIT,
              /*add by hoant 16.09.2016 theo cr 116741*/
                CreditAmountByBussinessType0 DECIMAL(25, 4) ,/*Có Chi tiết theo loại  0-Chiết khấu thương mai*/
               CreditAmountByBussinessType1 DECIMAL(25, 4) ,/*Có Chi tiết theo loại  1 - Giảm giá hàng bán*/
               CreditAmountByBussinessType2 DECIMAL(25, 4) ,/*Có Chi tiết theo loại   2- trả lại hàng bán*/
              DebitAmountByBussinessType0 DECIMAL(25, 4) ,/*Nợ Chi tiết theo loại  0-Chiết khấu thương mai*/
              DebitAmountByBussinessType1 DECIMAL(25, 4) ,/*Nợ Chi tiết theo loại  1 - Giảm giá hàng bán*/
              DebitAmountByBussinessType2 DECIMAL(25, 4) /*Nợ Chi tiết theo loại   2- trả lại hàng bán*/
            )			
/*
1. Doanh thu bán hàng và cung cấp dịch vụ

PS Có TK 511 (không kể PSĐƯ N911/C511, không kể các nghiệp vụ: Chiết khấu thương mại (bán hàng),

 Giảm giá hàng bán, Trả lại hàng bán)
  – PS Nợ TK 511 (không kể PSĐƯ N511/911, không kể các nghiệp vụ: Chiết khấu thương mại (bán hàng), 
  Giảm giá hàng bán, Trả lại hàng bán)
*/
		
        INSERT  INTO @Balance
                SELECT  GL.Account ,
                        GL.AccountCorresponding ,
                        GL.PostedDate ,
                        SUM(ISNULL(CreditAmount, 0)) AS CreditAmount ,
                        SUM(ISNULL(DebitAmount, 0)) AS DebitAmount ,
                        0 AS CreditAmountDetailBy ,
                        0 AS DebitAmountDetailBy ,
                        0,
                        SUM(CASE WHEN (GL.TypeID in ('320','321','322','323','324','325','350','360') and gl.Account like '511%')
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType0 ,
                        SUM(CASE WHEN GL.TypeID =340
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType1 
                            ,
                        SUM(CASE WHEN GL.TypeID =330
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType2 ,
                        SUM(CASE WHEN (GL.TypeID in ('320','321','322','323','324','325','350','360') and gl.Account like '511%')
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType0
                            ,
                        SUM(CASE WHEN GL.TypeID =340
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType1
                            ,
                        SUM(CASE WHEN GL.TypeID =330
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType2
                FROM    dbo.GeneralLedger GL
                WHERE   PostedDate BETWEEN @PrevFromDate AND @ToDate
                GROUP BY GL.Account ,
                        GL.AccountCorresponding,
                        GL.PostedDate                     
        OPTION  ( RECOMPILE )


        DECLARE @tblMasterDetail TABLE
            (
              ItemID UNIQUEIDENTIFIER ,
              DetailItemID UNIQUEIDENTIFIER ,
              OperationSign INT ,
              Grade INT ,
              DebitAmount DECIMAL(25, 4) ,
              PrevDebitAmount DECIMAL(25, 4)
            )	
	
	
	/*
	PhatsinhCO(511) - PhatsinhDU(911/511) + PhatsinhCO_ChitietChietKhauThuongmai(511)
	 + PhatsinhCO_ChitietGiamgiaHangBan(511) + PhatsinhCO_ChitietTralaiHangBan(511) +
	  PhatsinhNO(511) - PhatsinhDU(511/911) - PhatsinhNO_ChitietChietKhauThuongmai(511) - PhatsinhNO_ChitietGiamgiaHangBan(511) - PhatsinhNO_ChitietTralaiHangBan(511)
	*/
        INSERT  INTO @tblMasterDetail
                SELECT  I.ItemID ,
                        NULL ,
                        1 ,
                        -1
                        ,
                        CASE WHEN I.IsDetailGreaterThanZero = 0
                                  OR ( SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                                THEN ( CASE WHEN I.OperandString = 'PhatsinhCO'
                                                            THEN GL.CreditAmount
                                                            WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                            THEN GL.DebitAmount
                                                        
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                        
                                                            WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
															 THEN GL.CreditAmount
															 WHEN I.OperandString IN (
																  'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
															 THEN GL.DebitAmount
															 ELSE 0
														
                                                       END ) * I.OperationSign
                                                ELSE 0
                                           END) ) > 0
                             THEN ( SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                             THEN ( CASE WHEN I.OperandString = 'PhatsinhCO'
                                                         THEN GL.CreditAmount
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                         THEN GL.DebitAmount
                                                    
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                     
                                                              
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.CreditAmount
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.DebitAmount
                                                         ELSE 0
                                                    END ) * I.OperationSign
                                             ELSE 0
                                        END) )
                             ELSE 0
                        END AS DebitAmount ,
                        CASE WHEN I.IsDetailGreaterThanZero = 0
                                  OR ( SUM(CASE WHEN GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                                THEN CASE WHEN I.OperandString = 'PhatsinhCO'
                                                          THEN GL.CreditAmount
                                                          WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                          THEN GL.DebitAmount
            
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                         
                                                          WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.CreditAmount
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.DebitAmount
                                                         ELSE 0    
                                                     END * I.OperationSign
                                                ELSE 0
                                           END) ) > 0
                             THEN SUM(CASE WHEN GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                           THEN CASE WHEN I.OperandString = 'PhatsinhCO'
                                                     THEN GL.CreditAmount
                                                     WHEN I.OperandString IN (
                                                          'PhatsinhNO',
                                                          'PhatsinhDU' )
                                                     THEN GL.DebitAmount
                                                     
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                              
                                                              
                                                              
                                                     WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.CreditAmount
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.DebitAmount
                                                         ELSE 0
                                                END * I.OperationSign
                                           ELSE 0
                                      END)
                             ELSE 0
                        END AS DebitAmount
                FROM    @tblItem I
                        INNER JOIN @Balance AS GL ON ( GL.AccountNumber LIKE I.AccountNumber )
                                                     AND ( I.OperandString <> 'PhatsinhDU'
                                                           OR ( I.OperandString = 'PhatsinhDU'
                                                              AND GL.CorrespondingAccountNumber LIKE I.CorrespondingAccountNumber
                                                              )
                                                         )
                GROUP BY I.ItemID ,
                        I.IsDetailGreaterThanZero
        OPTION  ( RECOMPILE )
			
        INSERT  @tblMasterDetail
                SELECT  ItemID ,
                        x.r.value('@ItemID', 'UNIQUEIDENTIFIER') ,
                        x.r.value('@OperationSign', 'INT') ,
                        0 ,
                        0.0 ,
                        0.0
                FROM    FRTemplate
                        CROSS APPLY Formula.nodes('/root/MasterFormula') AS x ( r )
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
                        AND FormulaType = 1
                        AND Formula IS NOT NULL 
	
	

	;
        WITH    V ( ItemID, DetailItemID, DebitAmount, PrevDebitAmount, OperationSign )
                  AS ( SELECT   ItemID ,
                                DetailItemID ,
                                DebitAmount ,
                                PrevDebitAmount ,
                                OperationSign
                       FROM     @tblMasterDetail
                       WHERE    Grade = -1
                       UNION ALL
                       SELECT   B.ItemID ,
                                B.DetailItemID ,
                                V.DebitAmount ,
                                V.PrevDebitAmount ,
                                B.OperationSign * V.OperationSign AS OperationSign
                       FROM     @tblMasterDetail B ,
                                V
                       WHERE    B.DetailItemID = V.ItemID
                     )
	INSERT    @Result
                SELECT  FR.ItemID ,
                        FR.ItemCode ,
                        FR.ItemName ,
                        FR.ItemNameEnglish ,
                        FR.ItemIndex ,
                        FR.Description ,
                        FR.FormulaType ,
                        CASE WHEN FR.FormulaType = 1 THEN FR.FormulaFrontEnd
                             ELSE ''
                        END AS FormulaFrontEnd ,
                        CASE WHEN FR.FormulaType = 1 THEN FR.Formula
                             ELSE NULL
                        END AS Formula ,
                        FR.Hidden ,
                        FR.IsBold ,
                        FR.IsItalic ,
                        ISNULL(X.Amount, 0) AS Amount ,
                        ISNULL(X.PrevAmount, 0) AS PrevAmount
                FROM    ( SELECT    V.ItemID ,
                                    SUM(V.OperationSign * V.DebitAmount) AS Amount ,
                                    SUM(V.OperationSign * V.PrevDebitAmount) AS PrevAmount
                          FROM      V
                          GROUP BY  ItemID
                        ) AS X
                        RIGHT JOIN dbo.FRTemplate FR ON FR.ItemID = X.ItemID
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
                ORDER BY ItemIndex
	
        RETURN 
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE FUNCTION [dbo].[Func_SplitString] 
( 
    @string NVARCHAR(MAX), 
    @delimiter CHAR(1) 
) 
RETURNS @output TABLE(splitdata NVARCHAR(MAX) 
) 
BEGIN 
    DECLARE @start INT, @end INT 
    SELECT @start = 1, @end = CHARINDEX(@delimiter, @string) 
    WHILE @start < LEN(@string) + 1 BEGIN 
        IF @end = 0  
            SET @end = LEN(@string) + 1
       
        INSERT INTO @output (splitdata)  
        VALUES(SUBSTRING(@string, @start, @end - @start)) 
        SET @start = @end + 1 
        SET @end = CHARINDEX(@delimiter, @string, @start)
        
    END 
    RETURN 
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
Create View [dbo].[ViewVoucherPatternsReportType] AS
select vpr.*, ty.ID RefTypeID
from dbo.VoucherPatternsReport vpr, dbo.Type ty
where ty.ID IN (
	select * from dbo.Func_SplitString(vpr.ListTypeID, ',' )
)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*=================================================================================================
 * Created By:		haipl
 * Created Date:	25/12/2017
 * Description:		Lay du lieu cho bao cao << Sổ chi tiết bán hàng >>
===================================================================================================== */
ALTER PROCEDURE [dbo].[Proc_SA_GetSalesBookDetail]
  
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @InventoryItemID NVARCHAR(MAX) , 
    @OrganizationUnitID UNIQUEIDENTIFIER ,
    @AccountObjectID NVARCHAR(MAX) ,
	@SumGiaVon decimal output
AS

    BEGIN
        SET NOCOUNT ON;
        declare @v1 DECIMAL(29,4)                            
        CREATE TABLE #tblListInventoryItemID
            (
              InventoryItemID UNIQUEIDENTIFIER PRIMARY KEY
            ) 
        INSERT  INTO #tblListInventoryItemID
                    SELECT  T.*
                    FROM    dbo.Func_ConvertStringIntoTable(@InventoryItemID,
                                                              ',')	
                                                              AS T  
        select a.Date as ngay_CT, 
		a.PostedDate as ngay_HT, 
		a.No as So_Hieu,
		a.InvoiceDate as Ngay_HD, 
		a.InvoiceNo as SO_HD, 
		Sreason as Dien_giai, 
		accountCorresponding as TK_DOIUNG,
		c.Unit as DVT, 
		b.Quantity as SL, 
		UnitPriceOriginal as DON_GIA, 
		sum(debitAmount) as KHAC, 
		sum(CreditAmount) as TT, 
		c.ID as  InventoryItemID,
		c.MaterialGoodsName
		from GeneralLedger a join SAInvoiceDetail b on a.DetailID = b.ID 
		join MaterialGoods c on b.MaterialGoodsID = c.ID
		join SAInvoice d on a.ReferenceID = d.ID
		where a.PostedDate between  @FromDate and @ToDate and c.ID in (select InventoryItemID from #tblListInventoryItemID)
		and Account like '511%'
		group by a.Date,  a.PostedDate, a.No ,a.InvoiceDate , a.InvoiceNo , Sreason , accountCorresponding , 
		c.Unit , b.Quantity , UnitPriceOriginal, c.ID,c.MaterialGoodsName ;


		 select isnull(sum(OWAmount),0) Sum_Gia_Goc, b.MaterialGoodsCode ,b.ID InventoryItemID from SAInvoiceDetail a, MaterialGoods b 
		 where exists (select InventoryItemID from #tblListInventoryItemID where InventoryItemID = a.MaterialGoodsID)
		 and exists ( select ReferenceID from GeneralLedger where  account = '632' and PostedDate between @FromDate and @ToDate and ReferenceID=a.SAinvoiceID )
		 and a.MaterialGoodsID=b.ID group by b.ID,b.MaterialGoodsCode


    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO


ALTER TABLE dbo.GenCode
  DROP CONSTRAINT FK_GenCode_TypeGroup
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '7a608ddb-5aae-4190-b6d8-024505b0e1c5'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '0e9fae7b-5c0a-4a5c-b2dc-0441d3872c78'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '35fa305f-8d57-4b62-8117-3a62e683d535'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'ba093059-301e-4a7b-b488-45b9a14e484e'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '295a8e8b-b0c3-4445-8206-5e83fcd76d69'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '5a41d700-542d-4c91-a92a-b33659541575'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'dcdb70d3-55f9-4262-bb61-dae8a095321b'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.FRTemplate WHERE ItemID = 'ae03237d-2f3c-4058-990c-691c020e2420'
DELETE dbo.FRTemplate WHERE ItemID = '5a64196c-ec0e-415a-8170-a12f69b6d379'
DELETE dbo.FRTemplate WHERE ItemID = '9313a644-96bb-49ea-84d6-ab05348a0fcf'
DELETE dbo.FRTemplate WHERE ItemID = '7d79aae2-ff30-4da4-a937-beae0aa5ce4e'

UPDATE dbo.FRTemplate SET Formula = CONVERT(xml, N'<root><MasterFormula ItemID="495F0F08-808B-48B6-A323-5095E1BEC348" ItemCode="31" ItemName="10. Thu nh?p khác" OperationSign="1" /><MasterFormula ItemID="41634E80-E270-4085-B0BA-B1F2D7DDDD13" ItemCode="32" ItemName="11. Chi phí khác" OperationSign="-1" /></root>', 1) WHERE ItemID = 'a919b0a3-8b4d-4142-81b8-39d7032e292a'
UPDATE dbo.FRTemplate SET ItemCode = N'31', ItemName = N'10. Thu nhập khác', ItemNameEnglish = N'10. Other income', FormulaFrontEnd = N'PhatsinhDU(711/911)', Formula = CONVERT(xml, N'<root><DetailFormula OperationSign="1" OperandString="PhatsinhDU" Description="Phát sinh d?i ?ng" AccountNumber="711" CorrespondingAccountNumber="911" /></root>', 1) WHERE ItemID = '495f0f08-808b-48b6-a323-5095e1bec348'
UPDATE dbo.FRTemplate SET ItemCode = N'32', ItemName = N'11. Chi phí khác', FormulaFrontEnd = N'PhatsinhDU(911/811)', Formula = CONVERT(xml, N'<root><DetailFormula OperationSign="1" OperandString="PhatsinhDU" Description="Phát sinh d?i ?ng" AccountNumber="911" CorrespondingAccountNumber="811" /></root>', 1) WHERE ItemID = '41634e80-e270-4085-b0ba-b1f2d7dddd13'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.GenCode SET Prefix = N'TTS' WHERE ID = '329f7223-401f-4c82-bde9-3a2a36532dfe'
UPDATE dbo.GenCode SET TypeGroupName = N'Điều chuyển CCDC' WHERE ID = '277490db-e977-412e-849f-50d6711ea9b3'
UPDATE dbo.GenCode SET Prefix = N'STM' WHERE ID = '8e0699e1-9fca-4c28-a506-d3933261b0ab'
UPDATE dbo.GenCode SET Prefix = N'DMH' WHERE ID = 'e5cac91a-b0b5-4d7e-a1f0-dca2b5b5f562'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.SystemOption SET Data = N'02/01/2019', DefaultData = N'02/01/2019' WHERE ID = 103
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '45495f61-6d9b-4e88-b9ad-1988cfb3ec26'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '8809ba32-5817-4acc-bb78-2ff696d896b0'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '18693c83-b5be-4a4b-8d77-40664f968732'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'dacbce9c-82d8-4b4d-8489-422341193983'
UPDATE dbo.TemplateColumn SET VisiblePosition = 28 WHERE ID = 'b087074b-04a6-427a-b9a2-43511ff7dd02'
UPDATE dbo.TemplateColumn SET IsVisible = 1, VisiblePosition = 27 WHERE ID = '734a906a-5ec3-483a-a10a-438a3978d34e'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '209e81ec-78f3-4dca-97f5-4c861aa6d89c'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'b93bf459-b0f7-4c5a-84bc-58066b9c87b4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 27 WHERE ID = 'bbe4f3fa-1c1a-4526-ac1e-5b0df593998c'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '3f884677-3a17-429a-aae1-891917a06d38'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '59524648-7cac-432d-9dc2-c8b4c203d0b4'
UPDATE dbo.TemplateColumn SET IsVisible = 1, VisiblePosition = 26 WHERE ID = 'e35ff46b-cca9-49ce-b881-e77e4efb97c0'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '6062458b-fd80-4b26-a72a-ea6050a62bb7'

INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('577600a0-23a8-47fa-9015-5a41fa738d8e', '21bf5f6f-da51-47d1-9581-071ed46e2ff2', N'ExpiryDate', N'Hạn dùng', NULL, 0, 0, 95, 0, 0, 1, 16)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('2e1db53b-5400-4ce6-b420-966b50ed17a9', '21bf5f6f-da51-47d1-9581-071ed46e2ff2', N'LotNo', N'Số lô', N'', 0, 0, 98, 0, 0, 1, 15)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.TT153InvoiceTemplate(ID, InvoiceTempName, InvoiceTempPath, InvoiceType) VALUES ('28870782-2ab6-4ee0-a9a2-34d23a69a52a', N'Phiếu xuất kho hàng gửi bán đại lý', N'TT153-PXK-GBDL.rst', 0)
INSERT dbo.TT153InvoiceTemplate(ID, InvoiceTempName, InvoiceTempPath, InvoiceType) VALUES ('ca771f4e-5040-4000-a4fd-5b20e4b3c0fb', N'Hóa đơn GTGT (3 liên, A4, song ngữ)', N'TT153-HD-GTGT-English.rst', 0)
INSERT dbo.TT153InvoiceTemplate(ID, InvoiceTempName, InvoiceTempPath, InvoiceType) VALUES ('fe714ac6-4bc7-4bd5-9197-62b8ba2f0159', N'Hóa đơn bán hàng (3 liên, A4, song ngữ)', N'TT153-HD-BHTT-English.rst', 0)
INSERT dbo.TT153InvoiceTemplate(ID, InvoiceTempName, InvoiceTempPath, InvoiceType) VALUES ('606b721b-d13d-45af-9df3-81dad43920fb', N'Hóa đơn bán hàng (3 liên, A4)', N'TT153-HD-BHTT.rst', 0)
INSERT dbo.TT153InvoiceTemplate(ID, InvoiceTempName, InvoiceTempPath, InvoiceType) VALUES ('67625ecf-dcb9-4eec-ac1c-8c01851aa615', N'Phiếu xuất kho kiêm vận chuyển hàng nội bộ', N'TT153-PXK-VCNB.rst', 0)
INSERT dbo.TT153InvoiceTemplate(ID, InvoiceTempName, InvoiceTempPath, InvoiceType) VALUES ('5fc90deb-ee1e-4386-af3a-c2c157b1985b', N'Hóa đơn GTGT (3 liên, A4)', N'TT153-HD-GTGT.rst', 0)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.GenCode WITH NOCHECK
  ADD CONSTRAINT FK_GenCode_TypeGroup FOREIGN KEY (TypeGroupID) REFERENCES dbo.TypeGroup(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF
GO
