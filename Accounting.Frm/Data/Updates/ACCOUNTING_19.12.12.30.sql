SET CONCAT_NULL_YIELDS_NULL, ANSI_NULLS, ANSI_PADDING, QUOTED_IDENTIFIER, ANSI_WARNINGS, ARITHABORT, XACT_ABORT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS OFF
GO

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO


Update [dbo].[TIAllocationAllocated] SET OrderPriority = NULL

ALTER TABLE [dbo].[TIAllocationAllocated]
  DROP COLUMN [OrderPriority];
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIAllocationAllocated]
  ADD [OrderPriority] [int] IDENTITY
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIAllocationDetail]
  ADD [OrderPriority] [int] IDENTITY
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO


Update [dbo].[TIAllocationPost] SET OrderPriority = NULL

ALTER TABLE [dbo].[TIAllocationPost]
  DROP COLUMN [OrderPriority];
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIAllocationPost]
  ADD [OrderPriority] [int] IDENTITY
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_GetBkeInv_Sale]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT
    )
AS
BEGIN 
/*
VATRate type :
-1: Không chịu thuế
0: thuê 0%
5: 5%
10: 10%
*/
/*add by cuongpv de thong nhat cach lam tron so lieu*/
	DECLARE @tbDataLocal TABLE(
		VATRate DECIMAL(25,10),
		so_hd NVARCHAR(25),
		ngay_hd DATETIME,
		Nguoi_mua NVARCHAR(512),
		MST NVARCHAR(50),
		Mat_hang NVARCHAR(512),
		Amount DECIMAL(25,0),
		DiscountAmount DECIMAL(25,0),
		Thue_GTGT DECIMAL(25,0),
		TKThue NVARCHAR(25),
		flag int,
		RefID UNIQUEIDENTIFIER,
		RefType int,
		StatusInvoice int
	)
/*end add by cuongpv*/
	if @IsSimilarBranch = 0
	Begin
		/*add by cuongpv*/
		INSERT INTO @tbDataLocal
		/*end add by cuongpv*/
		 select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
		  Description as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag
		  ,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
		    /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
		   from SAinvoice a join SAInvoiceDetail b on a.id = b.SAInvoiceID where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1 
		union all
		select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST, 
		Description as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
		,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
		 from  SABill a join SABillDetail b on a.ID = b.SABillID  where cast(InvoiceDate As Date) between @FromDate and @ToDate
		union all
		select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
		  Description as Mat_hang,  Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,-1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
		,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
		 from SAReturn a join SAReturnDetail b on a.id = b.SAReturnID
		where cast(InvoiceDate As Date) between @FromDate and @ToDate and recorded = 1 

		update @tbDataLocal set Amount = 0, DiscountAmount = 0, Thue_GTGT = 0 where StatusInvoice = 5 or StatusInvoice = 3 or RefID In (Select SAInvoiceID from TT153DeletedInvoice)
		

		select VATRate, so_hd, ngay_hd, Nguoi_mua, MST, Mat_hang, (Amount - DiscountAmount) as Doanh_so, Thue_GTGT, TKThue, flag,RefID,RefType
		from @tbDataLocal order by VATRate, ngay_hd, so_hd, Nguoi_mua
	End
	else
	Begin
		/*add by cuongpv*/
		INSERT INTO @tbDataLocal
		/*end add by cuongpv*/
			select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
			  a.Reason as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
			   ,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
			   from SAinvoice a join SAInvoiceDetail b on a.id = b.SAInvoiceID where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1 
			union all
			select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST, 
			a.Reason as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
			 ,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
			 from  SABill a join SABillDetail b on a.ID = b.SABillID  where cast(InvoiceDate As Date) between @FromDate and @ToDate
			union all
			select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
			  a.Reason as Mat_hang,  Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,-1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
			 ,a.ID as RefID, a.TypeID as RefType, a.StatusInvoice
			 from SAReturn a join SAReturnDetail b on a.id = b.SAReturnID
			where cast(InvoiceDate As Date) between @FromDate and @ToDate and recorded = 1

		update @tbDataLocal set Amount = 0, DiscountAmount = 0, Thue_GTGT = 0 where StatusInvoice = 5 or StatusInvoice = 3 or RefID In (Select SAInvoiceID from TT153DeletedInvoice)

		select VATRate, 
			   so_hd, 
			   ngay_hd, 
			   Nguoi_mua, 
			   MST,
			   Mat_hang, 
			   sum(Doanh_so) Doanh_so, 
			   sum(Thue_GTGT) Thue_GTGT, 
			   TKThue,
			   flag
		from
			( select VATRate, so_hd, ngay_hd, Nguoi_mua, MST, Mat_hang, (Amount - DiscountAmount) as Doanh_so, Thue_GTGT, TKThue, flag 
			  from @tbDataLocal
			) aa
		group by VATRate, 
			   so_hd, 
			   ngay_hd, 
			   Nguoi_mua, 
			   MST,
			   Mat_hang, 
			   TKThue ,
			   flag
		order by VATRate, ngay_hd, so_hd,Nguoi_mua
	End
end;
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_GetBkeInv_Buy_Tax]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT,
	  @KhauTru Smallint
    )
AS
BEGIN
/*add by cuongpv de xu ly cach thong nhat lam tron*/
	DECLARE @tbDataLocal TABLE(
		VoucherID UNIQUEIDENTIFIER,
		GoodsServicePurchaseCode NVARCHAR(25),
		GoodsServicePurchaseName NVARCHAR(512),
		So_HD NVARCHAR(25),
		Ngay_HD DATETIME,
		ten_NBan NVARCHAR(512),
		MST NVARCHAR(50),
		Mat_hang NVARCHAR(512),
		GT_chuathue decimal(25,0),
		Thue_suat decimal(25,10),
		Thue_GTGT decimal(25,0),
		TK_Thue NVARCHAR(25),
		flag int,
		AccountingObjectID UNIQUEIDENTIFIER 
	)
/*end add by cuongpv*/
	IF (@KhauTru = 0)
	BEGIN
		if @IsSimilarBranch = 0
		Begin    
			/*add by cuongpv*/
			INSERT INTO @tbDataLocal
			/*end add by cuongpv*/
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD ,AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 b.Description as Mat_hang, InwardAmount  as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID                  
			 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) <= @ToDate and Recorded = 1
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all         
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 b.Description as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag, b.AccountingObjectID as AccountingObjectID   
			from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
			join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) <= @ToDate and Recorded = 1
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=a.InvoiceNo) = 0)
			union all    
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
			 b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID         
			 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) <= @ToDate and Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID																	
			from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and Recorded = 1 and a.TypeID not in (510)
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag, b.AccountingObjectID as AccountingObjectID																
			from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and Recorded = 1 and a.TypeID not in (907)
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and a.Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and a.Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and a.Recorded = 1
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
		
			/*edit by Hautv */
			UPDATE @tbDataLocal SET ten_NBan = d.AccountingObjectName
			FROM (select * from AccountingObject) d
				where AccountingObjectID = d.ID;

			/*edit by cuongpv order by c.GoodsServicePurchaseCode, InvoiceDate, InvoiceNo -> SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;*/
		
			SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;
		
		End
		else
		Begin
			/*add by cuongpv*/
			INSERT INTO @tbDataLocal
			/*end add by cuongpv*/
			 select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 a.Reason as Mat_hang, InwardAmount  as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID         
			 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) <= @ToDate and Recorded = 1
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all         
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 a.Reason as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag, b.AccountingObjectID as AccountingObjectID         
			from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
			join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) <= @ToDate and Recorded = 1
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=a.InvoiceNo) = 0)
			union all    
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
			 a.Reason as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue ,1 flag, b.AccountingObjectID as AccountingObjectID        
			 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) <= @ToDate and Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID																	
			from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and Recorded = 1 and a.TypeID not in (510)
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag, b.AccountingObjectID as AccountingObjectID																
			from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and Recorded = 1 and a.TypeID not in (907)
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and a.Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and a.Recorded = 1 
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate <= @ToDate and a.Recorded = 1
			and ((select Count(*) From [dbo].[TM012GTGT] where VoucherID=a.ID and InvoiceNo=b.InvoiceNo) = 0)

			/*edit by Hautv */
			UPDATE @tbDataLocal SET ten_NBan = d.AccountingObjectName
			FROM (select * from AccountingObject) d
				where AccountingObjectID = d.ID;
		select aa.VoucherID,
			   aa.GoodsServicePurchaseCode,
			   aa.GoodsServicePurchaseName,
			   aa.So_HD,
			   aa.Ngay_HD,
			   aa.ten_NBan,
			   aa.MST,
			   aa.Mat_hang,
			   sum(aa.GT_chuathue) GT_chuathue,
			   aa.Thue_suat ,
			   sum(aa.Thue_GTGT) Thue_GTGT,
			   aa.TK_Thue,
			   flag
		from (
			 select * from @tbDataLocal
			) aa
		group by VoucherID, GoodsServicePurchaseCode,
			   GoodsServicePurchaseName,
			   So_HD,
			   Ngay_HD,
			   ten_NBan,
			   MST,
			   Mat_hang,
			   TK_Thue,
			   flag,
			   aa.Thue_suat
		order by aa.GoodsServicePurchaseCode, aa.Ngay_HD, aa.So_HD
		End
	END
	ELSE/*tinh thue khau tru*/
	BEGIN
		if @IsSimilarBranch = 0
		Begin    
			/*add by cuongpv*/
			INSERT INTO @tbDataLocal
			/*end add by cuongpv*/
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD ,AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 b.Description as Mat_hang, InwardAmount  as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID                  
			 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) between @fromDate and @ToDate and Recorded = 1
			union all         
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 b.Description as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag, b.AccountingObjectID as AccountingObjectID   
			from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
			join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) between @fromDate and @ToDate and Recorded = 1
			union all    
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
			 b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID         
			 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) between @fromDate and @ToDate and Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID																	
			from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and Recorded = 1
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag, b.AccountingObjectID as AccountingObjectID																
			from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and Recorded = 1
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and a.Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and a.Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and a.Recorded = 1
		
			/*edit by Hautv */
			UPDATE @tbDataLocal SET ten_NBan = d.AccountingObjectName
			FROM (select * from AccountingObject) d
				where AccountingObjectID = d.ID;

			/*edit by cuongpv order by c.GoodsServicePurchaseCode, InvoiceDate, InvoiceNo -> SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;*/
		
			SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;
		
		End
		else
		Begin
			/*add by cuongpv*/
			INSERT INTO @tbDataLocal
			/*end add by cuongpv*/
			 select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 a.Reason as Mat_hang, InwardAmount  as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID         
			 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) between @fromDate and @ToDate and Recorded = 1
			union all         
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
			 a.Reason as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag, b.AccountingObjectID as AccountingObjectID         
			from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
			join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) between @fromDate and @ToDate and Recorded = 1
			union all    
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
			 a.Reason as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue ,1 flag, b.AccountingObjectID as AccountingObjectID        
			 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
			where cast(InvoiceDate As Date) between @fromDate and @ToDate and Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID																	
			from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and Recorded = 1
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
			b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag, b.AccountingObjectID as AccountingObjectID																
			from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and Recorded = 1
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and a.Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
			 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and a.Recorded = 1 
			union all
			select a.ID as VoucherID, c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
			 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
			 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
			join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
			where InvoiceDate between @fromDate and @ToDate and a.Recorded = 1

			/*edit by Hautv */
			UPDATE @tbDataLocal SET ten_NBan = d.AccountingObjectName
			FROM (select * from AccountingObject) d
				where AccountingObjectID = d.ID;
		select aa.VoucherID,
			   aa.GoodsServicePurchaseCode,
			   aa.GoodsServicePurchaseName,
			   aa.So_HD,
			   aa.Ngay_HD,
			   aa.ten_NBan,
			   aa.MST,
			   aa.Mat_hang,
			   sum(aa.GT_chuathue) GT_chuathue,
			   aa.Thue_suat ,
			   sum(aa.Thue_GTGT) Thue_GTGT,
			   aa.TK_Thue,
			   flag
		from (
			 select * from @tbDataLocal
			) aa
		group by VoucherID, GoodsServicePurchaseCode,
			   GoodsServicePurchaseName,
			   So_HD,
			   Ngay_HD,
			   ten_NBan,
			   MST,
			   Mat_hang,
			   TK_Thue,
			   flag,
			   aa.Thue_suat
		order by aa.GoodsServicePurchaseCode, aa.Ngay_HD, aa.So_HD
		End
	END
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_GetBkeInv_Buy]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT
    )
AS
BEGIN
/*add by cuongpv de xu ly cach thong nhat lam tron*/
	DECLARE @tbDataLocal TABLE(
		GoodsServicePurchaseCode NVARCHAR(25),
		GoodsServicePurchaseName NVARCHAR(512),
		So_HD NVARCHAR(25),
		Ngay_HD DATETIME,
		ten_NBan NVARCHAR(512),
		MST NVARCHAR(50),
		Mat_hang NVARCHAR(512),
		GT_chuathue decimal(25,0),
		Thue_suat decimal(25,10),
		Thue_GTGT decimal(25,0),
		TK_Thue NVARCHAR(25),
		flag int,
		AccountingObjectID UNIQUEIDENTIFIER ,
		RefID UNIQUEIDENTIFIER,
		TypeID int
	)
/*end add by cuongpv*/
	if @IsSimilarBranch = 0
	Begin    
		/*add by cuongpv*/
		INSERT INTO @tbDataLocal
		/*end add by cuongpv*/
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD ,AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
		 b.Description as Mat_hang, InwardAmount  as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID                  
		, a.ID as RefID, a.TypeID as TypeID
		 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1
		union all         
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
		 b.Description as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag, b.AccountingObjectID as AccountingObjectID   
		, a.ID as RefID, a.TypeID as TypeID
		from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
		join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1
		union all    
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
		 b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID         
		, a.ID as RefID, a.TypeID as TypeID
		 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
		b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID																	
		, a.ID as RefID, a.TypeID as TypeID
		from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 and a.TypeID not in (510)
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
		b.Description as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag, b.AccountingObjectID as AccountingObjectID																
		, a.ID as RefID, a.TypeID as TypeID
		from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 and a.TypeID not in (907)
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
		 , a.ID as RefID, a.TypeID as TypeID
		 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
		, a.ID as RefID, a.TypeID as TypeID
		 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and a.Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
		 , a.ID as RefID, a.TypeID as TypeID
		 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and a.Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
		, a.ID as RefID, a.TypeID as TypeID
		 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and a.Recorded = 1;
		
		/*edit by Hautv */
		UPDATE @tbDataLocal SET ten_NBan = d.AccountingObjectName
		FROM (select * from AccountingObject) d
			where AccountingObjectID = d.ID;

		/*edit by cuongpv order by c.GoodsServicePurchaseCode, InvoiceDate, InvoiceNo -> SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;*/
		
		SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;
		
	End
	else
	Begin
		/*add by cuongpv*/
		INSERT INTO @tbDataLocal
		/*end add by cuongpv*/
		 select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
		 a.Reason as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID
		, a.ID as RefID, a.TypeID as TypeID
		 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1
		union all         
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
		 a.Reason as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag, b.AccountingObjectID as AccountingObjectID         
		, a.ID as RefID, a.TypeID as TypeID
		from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
		join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1
		union all    
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
		 a.Reason as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue ,1 flag, b.AccountingObjectID as AccountingObjectID        
		 , a.ID as RefID, a.TypeID as TypeID
		 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
		a.Reason as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag, b.AccountingObjectID as AccountingObjectID																	
		, a.ID as RefID, a.TypeID as TypeID
		from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 and a.TypeID not in (510)
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
		a.Reason as Mat_hang, (Amount - DiscountAmount) as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag, b.AccountingObjectID as AccountingObjectID																
		, a.ID as RefID, a.TypeID as TypeID
		from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 and a.TypeID not in (907)
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 a.Reason as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
		 , a.ID as RefID, a.TypeID as TypeID
		 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 a.Reason as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
		, a.ID as RefID, a.TypeID as TypeID
		 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and a.Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		a.Reason as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag,NULL as AccountingObjectID																		
		 , a.ID as RefID, a.TypeID as TypeID
		 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and a.Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 a.Reason as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag,NULL as AccountingObjectID																	
		 , a.ID as RefID, a.TypeID as TypeID
		 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where a.PostedDate between @FromDate and @ToDate and a.Recorded = 1

		/*edit by Hautv */
		UPDATE @tbDataLocal SET ten_NBan = d.AccountingObjectName
		FROM (select * from AccountingObject) d
			where AccountingObjectID = d.ID;
	select aa.GoodsServicePurchaseCode,
		   aa.GoodsServicePurchaseName,
		   aa.So_HD,
		   aa.Ngay_HD,
		   aa.ten_NBan,
		   aa.MST,
		   aa.Mat_hang,
		   sum(aa.GT_chuathue) GT_chuathue,
		   aa.Thue_suat ,
		   sum(aa.Thue_GTGT) Thue_GTGT,
		   aa.TK_Thue,
		   flag
	from (
		 select * from @tbDataLocal
		) aa
	group by GoodsServicePurchaseCode,
		   GoodsServicePurchaseName,
		   So_HD,
		   Ngay_HD,
		   ten_NBan,
		   MST,
		   Mat_hang,
		   TK_Thue,
		   flag,
		   aa.Thue_suat
	order by aa.GoodsServicePurchaseCode, aa.Ngay_HD, aa.So_HD
	End
	
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
Proc_GetBalanceAccountF01 '2019-01-01','2019-12-31',3,0
*/
ALTER PROCEDURE [dbo].[Proc_GetBalanceAccountF01]
      ( @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT,
		@IsBalanceBothSide BIT
		)
AS
BEGIN
 
SELECT t.AccountID ,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName,
			   sum(t.OpeningDebitAmount) OpeningDebitAmount,
			   sum(t.OpeningCreditAmount) OpeningCreditAmount,
			   sum(t.DebitAmount) DebitAmount,
			   sum(t.CreditAmount) CreditAmount,
			   sum(t.DebitAmountAccum) DebitAmountAccum,
			   sum(t.CreditAmountAccum) CreditAmountAccum,
			   sum(t.ClosingDebitAmount) ClosingDebitAmount,
			   sum(t.ClosingCreditAmount) ClosingCreditAmount
			   FROM [dbo].[Func_GetBalanceAccountF01] (
   @FromDate
  ,@ToDate
  ,@MaxAccountGrade
  ,@IsBalanceBothSide) t
  group by t.AccountID,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName
  order by t.AccountNumber
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GetHealthFinanceMoney]
      ( @FromDate DATETIME ,
        @ToDate DATETIME
		)
AS
BEGIN
	DECLARE @tbDataReturn TABLE(
		tySoNo decimal(18,2),
		tyTaiTro decimal(18,2),
		vonLuanChuyen decimal(18,2),
		hsTTNganHan decimal(18,2),
		hsTTNhanh decimal(18,2),
		hsTTTucThoi decimal(18,2),
		hsTTChung decimal(18,2),
		hsQVHangTonKho decimal(18,2),
		hsLNTrenVonKD decimal(18,2),
		hsLNTrenDTThuan decimal(18,2),
		hsLNTrenVonCSH decimal(18,2),
		tienMat money,
		tienGui money,
		doanhThu money,
		chiPhi money,
		loiNhuanTruocThue money,
		phaiThu money,
		phaiTra money,
		hangTonKho money
	)
	
	INSERT INTO @tbDataReturn(tySoNo,tyTaiTro,vonLuanChuyen,hsTTNganHan,hsTTNhanh,hsTTTucThoi,hsTTChung,hsQVHangTonKho,hsLNTrenVonKD,hsLNTrenDTThuan,
	hsLNTrenVonCSH,tienMat,tienGui,doanhThu,chiPhi,loiNhuanTruocThue,phaiThu,phaiTra,hangTonKho)
	Values(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null)
	
	DECLARE @tbDataGetB01DN TABLE(
	  ItemID UNIQUEIDENTIFIER ,
      ItemCode NVARCHAR(25) ,
      ItemName NVARCHAR(512) ,
      ItemNameEnglish NVARCHAR(512) ,
      ItemIndex INT ,
      Description NVARCHAR(512) ,
      FormulaType INT ,
      FormulaFrontEnd NVARCHAR(MAX) ,
      Formula XML ,
      Hidden BIT ,
      IsBold BIT ,
      IsItalic BIT ,
      Category INT ,
      SortOrder INT,
      Amount DECIMAL(25, 4) ,
      PrevAmount DECIMAL(25, 4)
	)
	
	INSERT INTO @tbDataGetB01DN
	SELECT * FROM [dbo].[Func_GetB01_DN] (null,null,@FromDate,@ToDate,0,1,0,@FromDate,@ToDate)
	
	
	DECLARE @tbDataGetBalanceAccountF01 TABLE(
		AccountID UNIQUEIDENTIFIER ,
        AccountCategoryKind INT ,
        AccountNumber NVARCHAR(50) ,
        AccountName NVARCHAR(512) ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
	)
	
	INSERT INTO @tbDataGetBalanceAccountF01
	SELECT t.AccountID ,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName,
			   sum(t.OpeningDebitAmount) OpeningDebitAmount,
			   sum(t.OpeningCreditAmount) OpeningCreditAmount,
			   sum(t.DebitAmount) DebitAmount,
			   sum(t.CreditAmount) CreditAmount,
			   sum(t.DebitAmountAccum) DebitAmountAccum,
			   sum(t.CreditAmountAccum) CreditAmountAccum,
			   sum(t.ClosingDebitAmount) ClosingDebitAmount,
			   sum(t.ClosingCreditAmount) ClosingCreditAmount
			   FROM [dbo].[Func_GetBalanceAccountF01] (@FromDate,@ToDate,1,0) t
	GROUP BY t.AccountID,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName
	ORDER BY t.AccountNumber
	
	
	Declare @ct400 decimal(25, 4)
	Set @ct400=(select Amount from @tbDataGetB01DN where ItemCode='400')
	
	Declare @ct600 decimal(25, 4)
	Set @ct600=(select Amount from @tbDataGetB01DN where ItemCode='600')
	
	if(ISNULL(@ct600,0)=0)
	begin
		UPDATE @tbDataReturn SET tySoNo = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET tySoNo = (ISNULL(@ct400,0)/ISNULL(@ct600,0))*100
	end
	
	Declare @ct500 decimal(25, 4)
	Set @ct500=(select Amount from @tbDataGetB01DN where ItemCode='500')
	
	if(ISNULL(@ct600,0)=0)
	begin
		UPDATE @tbDataReturn SET tyTaiTro = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET tyTaiTro = (ISNULL(@ct500,0)/ISNULL(@ct600,0))*100
	end
	
	Declare @ct100 decimal(25, 4)
	Set @ct100=(select Amount from @tbDataGetB01DN where ItemCode='100')
	
	Declare @ct410 decimal(25, 4)
	Set @ct410=(select Amount from @tbDataGetB01DN where ItemCode='410')
	
	UPDATE @tbDataReturn SET vonLuanChuyen = (ISNULL(@ct100,0)-ISNULL(@ct410,0))
	
	if(ISNULL(@ct410,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTNganHan = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTNganHan = (ISNULL(@ct100,0)/ISNULL(@ct410,0))
	end
	
	Declare @ct140 decimal(25, 4)
	Set @ct140=(select Amount from @tbDataGetB01DN where ItemCode='140')
	
	if(ISNULL(@ct410,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTNhanh = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTNhanh = (ISNULL(@ct100,0)-ISNULL(@ct140,0))/ISNULL(@ct410,0)
	end
	
	Declare @ct110 decimal(25, 4)
	Set @ct110=(select Amount from @tbDataGetB01DN where ItemCode='110')
	
	Declare @ct120 decimal(25, 4)
	Set @ct120=(select Amount from @tbDataGetB01DN where ItemCode='120')
	
	if(ISNULL(@ct410,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTTucThoi = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTTucThoi = (ISNULL(@ct110,0)+ISNULL(@ct120,0))/ISNULL(@ct410,0)
	end
	
	Declare @ct300 decimal(25, 4)
	Set @ct300=(select Amount from @tbDataGetB01DN where ItemCode='300')
	
	if(ISNULL(@ct400,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTChung = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTChung = (ISNULL(@ct300,0)/ISNULL(@ct400,0))
	end
	
	Declare @vonHH decimal(25, 4)
	Set @vonHH = (select SUM(DebitAmount) from GeneralLedger where Account='632' AND AccountCorresponding='911' AND (PostedDate between @FromDate and @ToDate))
	
	Declare @hangHoaTon decimal(25,4)
	set @hangHoaTon = (select SUM(ISNULL(OpeningDebitAmount,0)+ISNULL(ClosingDebitAmount,0)) from @tbDataGetBalanceAccountF01 
						where AccountNumber in ('152','153','154','155','156','157'))/2
	
	if(ISNULL(@hangHoaTon,0)=0)
	begin
		UPDATE @tbDataReturn SET hsQVHangTonKho = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsQVHangTonKho=(ISNULL(@vonHH,0)/ISNULL(@hangHoaTon,0))*100
	end
	
	Declare @LNsauThue decimal(25,4)
	set @LNsauThue = (select SUM(CreditAmount) from GeneralLedger where Account='4212' AND AccountCorresponding='911' AND (PostedDate between @FromDate and @ToDate))
	
	Declare @ct600namTruoc decimal(25, 4)
	Set @ct600namTruoc=(select PrevAmount from @tbDataGetB01DN where ItemCode='600')
	
	Declare @vonKdBq decimal(25,4)
	set @vonKdBq = (ISNULL(@ct600,0)+ISNULL(@ct600namTruoc,0))/2
	
	if(ISNULL(@vonKdBq,0)=0)
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonKD = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonKD = (ISNULL(@LNsauThue,0)/ISNULL(@vonKdBq,0))
	end
	
	
	Declare @dtThuan decimal(25,4)
	set @dtThuan = (select SUM(CreditAmount) from GeneralLedger where Account like'511%' AND AccountCorresponding='911' AND (PostedDate between @FromDate and @ToDate))
	
	if(ISNULL(@dtThuan,0)=0)
	begin
		UPDATE @tbDataReturn SET hsLNTrenDTThuan = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsLNTrenDTThuan = (ISNULL(@LNsauThue,0)/ISNULL(@dtThuan,0))
	end
	
	Declare @ct500namtruoc decimal(25, 4)
	Set @ct500namtruoc=(select PrevAmount from @tbDataGetB01DN where ItemCode='500')
	
	Declare @voncsh decimal(25,4)
	set @voncsh = (ISNULL(@ct500,0)+ISNULL(@ct500namtruoc,0))/2
	
	if(ISNULL(@voncsh,0)=0)
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonCSH = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonCSH = (ISNULL(@LNsauThue,0)/ISNULL(@voncsh,0))
	end
	
	Declare @duNoCuoiKy111 money
	set @duNoCuoiKy111 = (select ClosingDebitAmount from @tbDataGetBalanceAccountF01 where AccountNumber='111')
	
	UPDATE @tbDataReturn SET tienMat = ISNULL(@duNoCuoiKy111,0)
	
	Declare @duNoCuoiKy112 money
	set @duNoCuoiKy112 = (select ClosingDebitAmount from @tbDataGetBalanceAccountF01 where AccountNumber='112')
	
	UPDATE @tbDataReturn SET tienGui = ISNULL(@duNoCuoiKy112,0)
	
	Declare @doanhThu money
	set @doanhThu = (select SUM(CreditAmount - DebitAmount) from GeneralLedger 
						where ((Account like '511%') or (Account like '515%') or (Account like '711%'))
								and (AccountCorresponding<>'911') and (PostedDate between @FromDate and @ToDate))
	
	UPDATE @tbDataReturn SET doanhThu = ISNULL(@doanhThu,0)
	
	Declare @chiPhi money
	set @chiPhi = (select SUM(DebitAmount - CreditAmount) from GeneralLedger
						where ((Account like '632%') or (Account like '642%') or (Account like '635%') or (Account like '811%') or (Account like '821%'))
						and (AccountCorresponding<>'911') and (PostedDate between @FromDate and @ToDate))
	
	UPDATE @tbDataReturn SET chiPhi = ISNULL(@chiPhi,0)
	
	Declare @chiPhiko821 money
	set @chiPhiko821 = (select SUM(DebitAmount - CreditAmount) from GeneralLedger
						where ((Account like '632%') or (Account like '642%') or (Account like '635%') or (Account like '811%'))
						and (AccountCorresponding<>'911') and (PostedDate between @FromDate and @ToDate))
	
	UPDATE @tbDataReturn SET loiNhuanTruocThue = (ISNULL(@doanhThu,0) - ISNULL(@chiPhiko821,0))
	
	Declare @phaiThu money
	set @phaiThu = (select SUM(ClosingDebitAmount) from @tbDataGetBalanceAccountF01 where AccountNumber='131')
	
	UPDATE @tbDataReturn SET phaiThu = (ISNULL(@phaiThu,0))
	
	Declare @phaiTra money
	set @phaiTra = (select SUM(ClosingCreditAmount) from @tbDataGetBalanceAccountF01 where AccountNumber='331')
	
	UPDATE @tbDataReturn SET phaiTra = (ISNULL(@phaiTra,0))
	
	Declare @hangTonKho money
	set @hangTonKho = (select SUM(ClosingDebitAmount) from @tbDataGetBalanceAccountF01 
						where AccountNumber in ('152','153','154','155','156','157'))
	
	UPDATE @tbDataReturn SET hangTonKho = (ISNULL(@hangTonKho,0))
	
	SELECT * FROM @tbDataReturn
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE PROCEDURE [dbo].[Proc_GetPPPayVendorBill]
	@AccountingObjectID UNIQUEIDENTIFIER,
	@FromDate DATETIME,
    @ToDate DATETIME
AS
	BEGIN
		DECLARE @Account NVARCHAR(MAX) = (SELECT TOP 1 AccountNumber FROM Account WHERE DetailType = '0')
		DECLARE @tbDataGL TABLE(
				ID uniqueidentifier,
				BranchID uniqueidentifier,
				ReferenceID uniqueidentifier,
				TypeID int,
				Date datetime,
				PostedDate datetime,
				No nvarchar(25),
				InvoiceDate datetime,
				InvoiceNo nvarchar(25),
				Account nvarchar(25),
				AccountCorresponding nvarchar(25),
				BankAccountDetailID uniqueidentifier,
				CurrencyID nvarchar(3),
				ExchangeRate decimal(25, 10),
				DebitAmount decimal(25,0),
				DebitAmountOriginal decimal(25,2),
				CreditAmount decimal(25,0),
				CreditAmountOriginal decimal(25,2),
				Reason nvarchar(512),
				Description nvarchar(512),
				VATDescription nvarchar(512),
				AccountingObjectID uniqueidentifier,
				EmployeeID uniqueidentifier,
				BudgetItemID uniqueidentifier,
				CostSetID uniqueidentifier,
				ContractID uniqueidentifier,
				StatisticsCodeID uniqueidentifier,
				InvoiceSeries nvarchar(25),
				ContactName nvarchar(512),
				DetailID uniqueidentifier,
				RefNo nvarchar(25),
				RefDate datetime,
				DepartmentID uniqueidentifier,
				ExpenseItemID uniqueidentifier,
				OrderPriority int,
				IsIrrationalCost bit
			)

			INSERT INTO @tbDataGL
			SELECT GL.* FROM dbo.GeneralLedger GL 
			LEFT JOIN dbo.AccountingObject AO ON GL.AccountingObjectID = AO.ID
			WHERE GL.AccountingObjectID = @AccountingObjectID AND GL.Account LIKE '331%'
			AND  GL.TypeID IN (210, 230, 240, 430, 500, 701, 601) AND AO.ObjectType IN (1, 2) AND AO.IsActive = 1 AND GL.Date <= @ToDate

			(SELECT g.AccountingObjectID, a.AccountingObjectCode, a.AccountingObjectName, g.No,
			g.ReferenceID, g.PostedDate AS Date, g.InvoiceNo, g.EmployeeID, g.CurrencyID, g.ExchangeRate AS RefVoucherExchangeRate,
			TotalDebit = (SELECT ISNULL(SUM(CreditAmount - DebitAmount),0)
					FROM @tbDataGL WHERE PostedDate > @FromDate AND ReferenceID = g.ReferenceID),
			TotalDebitOriginal = (SELECT ISNULL(SUM(CreditAmountOriginal - DebitAmountOriginal),0)
			FROM @tbDataGL WHERE PostedDate > @FromDate AND ReferenceID = g.ReferenceID),
			DebitAmount = ((SELECT ISNULL(SUM(CreditAmount - DebitAmount),0)
					FROM @tbDataGL WHERE PostedDate > @FromDate AND ReferenceID = g.ReferenceID) - 
					(SELECT ISNULL(SUM(Amount),0)
					FROM MCPaymentDetailVendor mcp WHERE mcp.AccountingObjectID = @AccountingObjectID AND mcp.PPInvoiceID = g.ReferenceID) -
					(SELECT ISNULL(SUM(Amount),0)
					FROM MBTellerPaperDetailVendor mbt WHERE mbt.AccountingObjectID = @AccountingObjectID AND mbt.PPInvoiceID = g.ReferenceID) - 
					(SELECT ISNULL(SUM(Amount),0)
					FROM MBCreditCardDetailVendor mbc WHERE mbc.AccountingObjectID = @AccountingObjectID AND mbc.PPInvoiceID = g.ReferenceID) - 
					(SELECT ISNULL(SUM(Amount),0)
					FROM ExceptVoucher exv WHERE exv.AccountingObjectID = @AccountingObjectID AND exv.DebitAccount = @Account AND exv.GLVoucherID = g.ReferenceID)),
			DebitAmountOriginal = ((SELECT ISNULL(SUM(CreditAmountOriginal - DebitAmountOriginal),0)
					FROM @tbDataGL WHERE PostedDate > @FromDate AND ReferenceID = g.ReferenceID) - 
					(SELECT ISNULL(SUM(AmountOriginal),0)
					FROM MCPaymentDetailVendor mcp WHERE mcp.AccountingObjectID = @AccountingObjectID AND mcp.PPInvoiceID = g.ReferenceID) -
					(SELECT ISNULL(SUM(AmountOriginal),0)
					FROM MBTellerPaperDetailVendor mbt WHERE mbt.AccountingObjectID = @AccountingObjectID AND mbt.PPInvoiceID = g.ReferenceID) - 
					(SELECT ISNULL(SUM(AmountOriginal),0)
					FROM MBCreditCardDetailVendor mbc WHERE mbc.AccountingObjectID = @AccountingObjectID AND mbc.PPInvoiceID = g.ReferenceID) - 
					(SELECT ISNULL(SUM(AmountOriginal),0)
					FROM ExceptVoucher exv WHERE exv.AccountingObjectID = @AccountingObjectID AND exv.DebitAccount = @Account AND exv.GLVoucherID = g.ReferenceID)),
			Account = @Account
		FROM @tbDataGL g
		LEFT JOIN AccountingObject a on g.AccountingObjectID = a.ID
		GROUP BY g.AccountingObjectID, a.AccountingObjectCode, a.AccountingObjectName, g.ReferenceID, g.No, g.InvoiceNo, g.PostedDate, g.TypeID, g.EmployeeID, g.ExchangeRate, g.CurrencyID) ORDER BY g.No
	END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE PROCEDURE [dbo].[Proc_GetPPPayVendor]
	@FromDate DATETIME,
    @ToDate DATETIME
AS
BEGIN
	DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL 
		LEFT JOIN dbo.AccountingObject AO ON GL.AccountingObjectID = AO.ID
		WHERE GL.AccountingObjectID IS NOT NULL AND GL.Account LIKE '331%'
		AND  GL.TypeID IN (110, 111, 112, 113, 114, 115, 116, 117, 119, 118, 128, 134, 144, 174
		, 210, 230, 240, 430, 500, 701, 601) AND AO.ObjectType IN (1, 2) AND AO.IsActive = 1 AND GL.Date <= @ToDate

		(SELECT g.AccountingObjectID, a.AccountingObjectCode, a.AccountingObjectName,
		SoDuDauNam = ((SELECT ISNULL(SUM(CreditAmount - DebitAmount),0)
                FROM GeneralLedger WHERE AccountingObjectID = g.AccountingObjectID AND TypeID = 701
                AND PostedDate < @FromDate)
				+ (SELECT ISNULL(SUM(CreditAmount),0)
                FROM @tbDataGL WHERE AccountingObjectID = g.AccountingObjectID AND TypeID != 701
                AND PostedDate < @FromDate)),
		SoDaTra = ((SELECT ISNULL(SUM(DebitAmount),0)
                FROM @tbDataGL WHERE AccountingObjectID = g.AccountingObjectID AND TypeID IN
				(110, 111, 112, 113, 114, 115, 116, 117, 119, 118, 128, 134, 144, 174))
				+ (SELECT ISNULL(SUM(Amount),0)
                FROM ExceptVoucher WHERE AccountingObjectID = g.AccountingObjectID)),
		SoPhatSinh = (SELECT ISNULL(SUM(CreditAmount - DebitAmount),0)
				FROM @tbDataGL WHERE AccountingObjectID = g.AccountingObjectID AND TypeID IN
				(210, 230, 240, 430, 500, 701, 601) AND PostedDate >= @FromDate),
		SoConPhaiTra = (((SELECT ISNULL(SUM(CreditAmount - DebitAmount),0)
                FROM GeneralLedger WHERE AccountingObjectID = g.AccountingObjectID AND TypeID = 701
                AND PostedDate < @FromDate)
				+ (SELECT ISNULL(SUM(CreditAmount),0)
                FROM @tbDataGL WHERE AccountingObjectID = g.AccountingObjectID AND TypeID != 701
                AND PostedDate < @FromDate)) + 
				(SELECT ISNULL(SUM(CreditAmount - DebitAmount),0)
				FROM @tbDataGL WHERE AccountingObjectID = g.AccountingObjectID AND TypeID IN
				(210, 230, 240, 430, 500, 701, 601) AND PostedDate >= @FromDate) -
				((SELECT ISNULL(SUM(DebitAmount),0)
                FROM @tbDataGL WHERE AccountingObjectID = g.AccountingObjectID AND TypeID IN
				(110, 111, 112, 113, 114, 115, 116, 117, 119, 118, 128, 134, 144, 174))
				+ (SELECT ISNULL(SUM(Amount),0)
                FROM ExceptVoucher WHERE AccountingObjectID = g.AccountingObjectID)))
		FROM @tbDataGL g
		LEFT JOIN AccountingObject a on g.AccountingObjectID = a.ID
		GROUP BY g.AccountingObjectID, a.AccountingObjectCode, a.AccountingObjectName) ORDER BY a.AccountingObjectCode
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoTheoDoiLaiLoTheoHoaDon]
    @FromDate DATETIME,
    @ToDate DATETIME
AS
BEGIN          
    
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
		/*end add by cuongpv*/
	
	DECLARE @tbluutru TABLE (
		SAInvoiceID UNIQUEIDENTIFIER,
		RefType int,
		RefID UNIQUEIDENTIFIER,
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		Reason NVARCHAR(512),
		GiaTriHHDV money,
		ChietKhauBan money,
		ChietKhau_TLGG money,
		TraLai money,
		GiaVon money,
		LaiLo money
    )
    INSERT INTO @tbluutru(SAInvoiceID, RefType, RefID,DetailID, InvoiceDate,InvoiceNo,AccountingObjectID,Reason)
		SELECT DISTINCT SA.ID, GL.TypeID as RefType, GL.ReferenceID as RefID, GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
		FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		WHERE (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is null
		UNION ALL
		SELECT DISTINCT SA.ID, GL.TypeID as RefType, GL.ReferenceID as RefID, GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason
		FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		WHERE (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is not null
	

	DECLARE @tbluutruGiaTriHHDV TABLE (
		DetailID UNIQUEIDENTIFIER,		
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		Reason NVARCHAR(512),
		GiaTriHHDV money,
		RefID UNIQUEIDENTIFIER,
		RefType int
    )

	INSERT INTO @tbluutruGiaTriHHDV
	SELECT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.CreditAmount) as GiaTriHHDV, GL.ReferenceID as RefID, GL.TypeID as RefType
	FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	WHERE (GL.Account like '511%') AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is null
	GROUP BY GL.DetailID, GL.ReferenceID,GL.TypeID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
	UNION ALL
	SELECT GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.CreditAmount) as GiaTriHHDV, GL.ReferenceID as RefID, GL.TypeID as RefType
	FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
	WHERE (GL.Account like '511%') AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is not null
	GROUP BY GL.DetailID, GL.ReferenceID,GL.TypeID,SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason

    UPDATE @tbluutru SET GiaTriHHDV = a.GiaTriHHDV
	FROM (
		Select DetailID as IvID, RefID as refID, RefType, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, Reason as Re, GiaTriHHDV
		From @tbluutruGiaTriHHDV
	) a
	WHERE DetailID = a.IvID
	

	DECLARE @tbluutruChietKhauBan TABLE (
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		Reason NVARCHAR(512),
		ChietKhauBan money,
		RefID UNIQUEIDENTIFIER,
		RefType int
    )

	INSERT INTO @tbluutruChietKhauBan
	SELECT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.DebitAmount) as ChietKhauBan, GL.ReferenceID as RefID, GL.TypeID as RefType
	FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	WHERE (GL.Account like '511%') AND (GL.TypeID not in (330,340)) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is null
	GROUP BY GL.DetailID, GL.ReferenceID,GL.TypeID,GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
	UNION ALL
	SELECT GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.DebitAmount) as ChietKhauBan, GL.ReferenceID as RefID, GL.TypeID as RefType
	FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
	WHERE (GL.Account like '511%') AND (GL.TypeID not in (330,340)) AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is not null
	GROUP BY GL.DetailID, GL.ReferenceID,GL.TypeID,SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason


	UPDATE @tbluutru SET ChietKhauBan = b.ChietKhauBan
	FROM (
		Select DetailID as IvID, RefID, RefType, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, Reason as Re, ChietKhauBan
		From @tbluutruChietKhauBan
	) b
	WHERE DetailID = b.IvID
	
	DECLARE @tbluutruChietKhau_TLGG TABLE (
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		Reason NVARCHAR(512),
		ChietKhau_TLGG money,
		RefID UNIQUEIDENTIFIER,
		RefType int
    )
	INSERT INTO @tbluutruChietKhau_TLGG
	SELECT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.CreditAmount) as ChietKhau_TLGG, GL.ReferenceID as RefID, GL.TypeID as RefType
	FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	WHERE (GL.Account like '511%') AND (GL.TypeID in (330,340)) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail))
	GROUP BY GL.DetailID, GL.ReferenceID,GL.TypeID,GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason


	UPDATE @tbluutru SET ChietKhau_TLGG = c.ChietKhau_TLGG
	FROM (
		Select DetailID as IvID, RefID, RefType,InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, Reason as Re, ChietKhau_TLGG
		From @tbluutruChietKhau_TLGG
	) c
	WHERE DetailID = c.IvID
	

	DECLARE @tbDataTraLai TABLE(
		SAInvoiceDetailID UNIQUEIDENTIFIER,
		TraLai money,
		RefID UNIQUEIDENTIFIER,
		RefType int
	)
	INSERT INTO @tbDataTraLai(SAInvoiceDetailID,TraLai, RefID,RefType)
		SELECT SAR.SAInvoiceDetailID, SUM(GL.DebitAmount - GL.CreditAmount) as TraLai, GL.ReferenceID as RefID, GL.TypeID as RefType
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON GL.DetailID = SAR.ID
		WHERE (GL.Account like '511%') AND (GL.TypeID=330) AND (SAR.SAInvoiceDetailID in (select DetailID from @tbluutru))
		AND (GL.PostedDate between @FromDate and @ToDate)
		GROUP BY SAR.SAInvoiceDetailID,GL.ReferenceID,GL.TypeID
	

	UPDATE @tbluutru SET TraLai = TL.TraLai
	FROM (select SAInvoiceDetailID, TraLai from @tbDataTraLai) TL
	WHERE DetailID = TL.SAInvoiceDetailID
	
	DECLARE @tbluutruGiaVon TABLE (
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		Reason NVARCHAR(512),
		GiaVonHangBan money,
		GiaVonTraLai money,
		GiaVon money,
		RefID UNIQUEIDENTIFIER,
		RefType int
    )

	INSERT INTO @tbluutruGiaVon(DetailID, RefID, RefType,InvoiceDate, InvoiceNo, AccountingObjectID, Reason, GiaVonHangBan)
	SELECT GL.DetailID, GL.ReferenceID as RefID, GL.TypeID as RefType, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.DebitAmount - GL.CreditAmount) as GiaVonHangBan
	FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	WHERE (GL.Account like '632%') AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND SA.BillRefID is null
	GROUP BY GL.DetailID, GL.ReferenceID,GL.TypeID,GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
	UNION ALL
	SELECT GL.DetailID, GL.ReferenceID as RefID, GL.TypeID as RefType,SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.DebitAmount - GL.CreditAmount) as GiaVonHangBan
	FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
	LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
	WHERE (GL.Account like '632%') AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
	AND (GL.PostedDate between @FromDate and @ToDate) AND SA.BillRefID is not null
	GROUP BY GL.DetailID, GL.ReferenceID,GL.TypeID,SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, GL.Reason

	UPDATE @tbluutruGiaVon SET GiaVonTraLai = Gvtl.GiaVonTraLai
	FROM (
		Select SAR.SAInvoiceDetailID, SUM(GL.DebitAmount - GL.CreditAmount) as GiaVonTraLai, GL.ReferenceID as RefID, GL.TypeID as RefType
		From @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON GL.DetailID = SAR.ID
		Where (GL.Account like '632%') AND (SAR.SAInvoiceDetailID in (select DetailID from @tbluutruGiaVon))
		AND (GL.PostedDate between @FromDate and @ToDate)
		Group By SAR.SAInvoiceDetailID, GL.ReferenceID,GL.TypeID
	) Gvtl
	WHERE DetailID = SAInvoiceDetailID 

	UPDATE @tbluutruGiaVon SET GiaVon = ISNULL(GiaVonHangBan,0) + ISNULL(GiaVonTraLai,0)


	UPDATE @tbluutru SET GiaVon = d.GiaVon
	FROM (
		Select DetailID as IvID, RefID, RefType, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, Reason as Re, GiaVon
		From @tbluutruGiaVon
	) d
	WHERE DetailID = d.IvID

	
	DECLARE @tbDataReturn TABLE (
		SAInvoiceID UNIQUEIDENTIFIER,
		RefType int,
		RefID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		AccountingObjectName NVARCHAR(512),
		Reason NVARCHAR(512),
		GiaTriHHDV money,
		ChietKhauBan money,
		ChietKhau_TLGG money,
		ChietKhau money,
		GiamGia money,
		TraLai money,
		GiaVon money,
		LaiLo money
    )
    
    INSERT INTO @tbDataReturn(SAInvoiceID,RefType,RefID,InvoiceDate,InvoiceNo,AccountingObjectID,Reason,GiaTriHHDV,ChietKhauBan,ChietKhau_TLGG,TraLai,GiaVon)
    SELECT SAInvoiceID, RefType,RefID,InvoiceDate,InvoiceNo,AccountingObjectID,Reason,SUM(GiaTriHHDV) as GiaTriHHDV, SUM(ChietKhauBan) as ChietKhauBan,
    SUM(ChietKhau_TLGG) as ChietKhau_TLGG, SUM(TraLai) as TraLai, SUM(GiaVon) as GiaVon
    FROM @tbluutru
    GROUP BY SAInvoiceID,RefID,RefType, InvoiceDate,InvoiceNo,AccountingObjectID,Reason
    

	INSERT INTO @tbDataReturn(SAInvoiceID,RefType,RefID,InvoiceDate,InvoiceNo,AccountingObjectID,Reason,GiamGia)
		SELECT SA.ID, GL.TypeID as RefType,GL.ReferenceID as RefID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.DebitAmount - GL.CreditAmount) as GiamGia
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		WHERE (GL.Account like '511%') AND (GL.TypeID=340) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAReturnDetail))
		GROUP BY SA.ID, GL.ReferenceID,GL.TypeID,GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
    
    UPDATE @tbDataReturn SET AccountingObjectName = AJ.AccountingObjectName
	FROM ( select ID,AccountingObjectName from AccountingObject) AJ
	where AccountingObjectID = AJ.ID
    
    UPDATE @tbDataReturn SET ChietKhau = (ISNULL(ChietKhauBan,0) - ISNULL(ChietKhau_TLGG,0))
    
    UPDATE @tbDataReturn SET LaiLo = (ISNULL(GiaTriHHDV,0) - (ISNULL(ChietKhau,0)+ISNULL(GiamGia,0)+ISNULL(TraLai,0)+ISNULL(GiaVon,0)))
    
    SELECT SAInvoiceID,RefType,RefID,InvoiceDate,InvoiceNo,AccountingObjectName,Reason,GiaTriHHDV,ChietKhau,GiamGia,TraLai,GiaVon,LaiLo FROM @tbDataReturn
    WHERE ((ISNULL(GiaTriHHDV,0)>0) OR (ISNULL(ChietKhau,0)>0) OR (ISNULL(GiamGia,0)>0)
			OR (ISNULL(TraLai,0)>0) OR (ISNULL(GiaVon,0)>0) OR (ISNULL(LaiLo,0)>0)) AND RefType in (220, 320, 321, 322, 323, 324, 325, 326, 340)
	ORDER BY InvoiceDate,InvoiceNo
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_GetSalesDiaryBook]
    @FromDate DATETIME ,
    @ToDate DATETIME,
    @IsDisplayNotReceiptOnly BIT
AS 
    BEGIN 
		
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between  @FromDate and @ToDate
		/*end add by cuongpv*/

        DECLARE @listRefType NVARCHAR(MAX)
        IF @IsDisplayNotReceiptOnly = 1 
            SET @listRefType = '3530,3532,3534,3536'  
        ELSE 
            SET @listRefType = '3530,3531,3532,3534,3535,3536,3537,3538,3540,3541,3542,3543,3544,3545,3550,3551,3552,3553,3554,3555'
		
		/*add by cuongpv*/
		DECLARE @tbResults TABLE (
			RefID uniqueidentifier,
			TypeID int,
			[Date] datetime,
			PostedDate datetime,
			[No] nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			[Description] nvarchar(512),
			TurnOverAmountInv decimal(25,0),
			TurnOverAmountFinishedInv decimal(25,0),
			TurnOverAmountServiceInv decimal(25,0),
			TurnOverAmountOther decimal(25,0),
			DiscountAmount decimal(25,0),
			ReturnAmount decimal(25,0),
			ReduceAmount decimal(25,0),
			CustomerID uniqueidentifier,
			CustomerCode nvarchar(25),
			CustomerName nvarchar(512),
			TypeData Char(4)
		)
		if(@IsDisplayNotReceiptOnly = 0 )
		BEGIN
		INSERT INTO @tbResults([Date], PostedDate, [No], InvoiceDate, InvoiceNo, [Description], TurnOverAmountInv, TurnOverAmountFinishedInv, TurnOverAmountServiceInv, TurnOverAmountOther
		,DiscountAmount, ReturnAmount, ReduceAmount, CustomerID, TypeData, RefID, TypeID)
		/*lay du lieu SAInvoice: BillRefID is null*/
		SELECT  GL.Date as [Date], GL.PostedDate as PostedDate, GL.No as [No], GL.InvoiceDate as InvoiceDate, GL.InvoiceNo as InvoiceNo, GL.Reason as [Description],
				SUM(CASE WHEN GL.Account like '5111%' THEN GL.CreditAmount ELSE $0 END) as TurnOverAmountInv,
				SUM(CASE WHEN GL.Account like '5112%' THEN GL.CreditAmount ELSE $0 END) as TurnOverAmountFinishedInv,
				SUM(CASE WHEN GL.Account like '5113%' THEN GL.CreditAmount ELSE $0 END) as TurnOverAmountServiceInv,
				SUM(CASE WHEN GL.Account like '5118%' THEn GL.CreditAmount ELSE $0 END) as TurnOverAmountOther,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID not in (330,340)) THEN GL.DebitAmount ELSE $0 END) as DiscountAmount,
				0 as ReturnAmount, 0 as ReduceAmount, GL.AccountingObjectID as CustomerID, NULL as TypeData, GL.ReferenceID as RefID, GL.TypeID
		FROM @tbDataGL GL INNER JOIN SAInvoiceDetail SAD ON SAD.ID = GL.DetailID
			LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		WHERE (GL.PostedDate between @FromDate and @ToDate) AND SA.BillRefID is null	
		GROUP BY GL.Date, GL.PostedDate, GL.No, GL.InvoiceDate, GL.InvoiceNo, GL.Reason, GL.AccountingObjectID, GL.ReferenceID, GL.TypeID
		/*lay du lieu SAInvoice: BillRefID is not null*/
		UNION ALL
		SELECT GL.Date as [Date], GL.PostedDate as PostedDate, GL.No as [No], SAB.InvoiceDate as InvoiceDate, SAB.InvoiceNo as InvoiceNo, GL.Reason as [Description],
				SUM(CASE WHEN GL.Account like '5111%' THEN GL.CreditAmount ELSE $0 END) as TurnOverAmountInv,
				SUM(CASE WHEN GL.Account like '5112%' THEN GL.CreditAmount ELSE $0 END) as TurnOverAmountFinishedInv,
				SUM(CASE WHEN GL.Account like '5113%' THEN GL.CreditAmount ELSE $0 END) as TurnOverAmountServiceInv,
				SUM(CASE WHEN GL.Account like '5118%' THEn GL.CreditAmount ELSE $0 END) as TurnOverAmountOther,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID not in (330,340)) THEN GL.DebitAmount ELSE $0 END) as DiscountAmount,
				0 as ReturnAmount, 0 as ReduceAmount, GL.AccountingObjectID as CustomerID, NULL as TypeData, GL.ReferenceID as RefID, GL.TypeID
		FROM @tbDataGL GL INNER JOIN SAInvoiceDetail SAD ON SAD.ID = GL.DetailID
			LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		WHERE (GL.PostedDate between @FromDate and @ToDate) AND SA.BillRefID is not null 
		GROUP BY GL.Date, GL.PostedDate, GL.No, SAB.InvoiceDate, SAB.InvoiceNo, GL.Reason, GL.AccountingObjectID, GL.ReferenceID, GL.TypeID
		/*lay gia tri Tra lai giam gia SAInvoice: BillRefID is null*/
		UNION ALL
		SELECT GL.Date as [Date], GL.PostedDate as PostedDate, GL.No as [No], GL.InvoiceDate as InvoiceDate, GL.InvoiceNo as InvoiceNo, GL.Reason as [Description],
				0 as TurnOverAmountInv,
				0 as TurnOverAmountFinishedInv,
				0 as TurnOverAmountServiceInv,
				0 as TurnOverAmountOther,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID in (330,340)) THEN GL.CreditAmount ELSE $0 END) as DiscountAmount,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID = 330) THEN GL.DebitAmount ELSE $0 END) as ReturnAmount,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID = 340) THEN GL.DebitAmount ELSE $0 END) as ReduceAmount, GL.AccountingObjectID as CustomerID, 'TLGG' as TypeData, GL.ReferenceID as RefID, GL.TypeID
		FROM @tbDataGL GL INNER JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID = SAR.SAInvoiceDetailID
			LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		WHERE (GL.PostedDate between @FromDate and @ToDate) AND SA.BillRefID is null
		GROUP BY GL.Date, GL.PostedDate, GL.No, GL.InvoiceDate, GL.InvoiceNo, GL.Reason, GL.AccountingObjectID, GL.ReferenceID, GL.TypeID
		/*lay gia tri Tra lai giam gia SAInvoice: BillRefID is not null*/
		UNION ALL
		SELECT GL.Date as [Date], GL.PostedDate as PostedDate, GL.No as [No], SAB.InvoiceDate as InvoiceDate, SAB.InvoiceNo as InvoiceNo, GL.Reason as [Description],
				0 as TurnOverAmountInv,
				0 as TurnOverAmountFinishedInv,
				0 as TurnOverAmountServiceInv,
				0 as TurnOverAmountOther,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID in (330,340)) THEN GL.CreditAmount ELSE $0 END) as DiscountAmount,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID = 330) THEN GL.DebitAmount ELSE $0 END) as ReturnAmount,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID = 340) THEN GL.DebitAmount ELSE $0 END) as ReduceAmount, GL.AccountingObjectID as CustomerID, 'TLGG' as TypeData, GL.ReferenceID as RefID, GL.TypeID
		FROM @tbDataGL GL INNER JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID = SAR.SAInvoiceDetailID
			LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		WHERE (GL.PostedDate between @FromDate and @ToDate) AND SA.BillRefID is not null 
		GROUP BY GL.Date, GL.PostedDate, GL.No, SAB.InvoiceDate, SAB.InvoiceNo, GL.Reason, GL.AccountingObjectID, GL.ReferenceID, GL.TypeID

		/*update ten doi tuong vao */
		UPDATE @tbResults SET CustomerCode = a.CustomerCode, CustomerName = a.CustomerName
		FROM(
			select ID as cusID, AccountingObjectCode as CustomerCode, AccountingObjectName as CustomerName from AccountingObject
		) a
		WHERE CustomerID = a.cusID

		

       select
				RefID,
				TypeID,
				Date, 
				PostedDate, 
				No,
				InvoiceDate, 
				InvoiceNo, 
				Description,
				TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther as SumTurnOver,
				TurnOverAmountInv ,
				TurnOverAmountFinishedInv ,
                TurnOverAmountServiceInv ,
                TurnOverAmountOther,
                (CASE WHEN TypeData='TLGG' THEN -DiscountAmount ELSE DiscountAmount END) as DiscountAmount ,
				ReturnAmount,
                ReduceAmount,
				(CASE WHEN TypeData='TLGG' THEN ((TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther) - (-DiscountAmount + ReturnAmount + ReduceAmount)) 
				ELSE ((TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther) - (DiscountAmount + ReturnAmount + ReduceAmount)) END) as TurnOverPure,
				CustomerCode, 
				CustomerName 
   from @tbResults 
   where ((TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv + TurnOverAmountOther) <> 0) OR ((DiscountAmount + ReturnAmount + ReduceAmount) <> 0)
   ORDER BY Date, No 
    
    END
	else
	BEGIN
	INSERT INTO @tbResults([Date], PostedDate, [No], InvoiceDate, InvoiceNo, [Description], TurnOverAmountInv, TurnOverAmountFinishedInv, TurnOverAmountServiceInv, TurnOverAmountOther
		,DiscountAmount, ReturnAmount, ReduceAmount, CustomerID, TypeData, RefID, TypeID)
		/*lay du lieu SAInvoice: BillRefID is null*/
		SELECT  GL.Date as [Date], GL.PostedDate as PostedDate, GL.No as [No], GL.InvoiceDate as InvoiceDate, GL.InvoiceNo as InvoiceNo, GL.Reason as [Description],
				SUM(CASE WHEN GL.Account like '5111%' THEN GL.CreditAmount ELSE $0 END) as TurnOverAmountInv,
				SUM(CASE WHEN GL.Account like '5112%' THEN GL.CreditAmount ELSE $0 END) as TurnOverAmountFinishedInv,
				SUM(CASE WHEN GL.Account like '5113%' THEN GL.CreditAmount ELSE $0 END) as TurnOverAmountServiceInv,
				SUM(CASE WHEN GL.Account like '5118%' THEn GL.CreditAmount ELSE $0 END) as TurnOverAmountOther,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID not in (330,340)) THEN GL.DebitAmount ELSE $0 END) as DiscountAmount,
				0 as ReturnAmount, 0 as ReduceAmount, GL.AccountingObjectID as CustomerID, NULL as TypeData, GL.ReferenceID as RefID, GL.TypeID
		FROM @tbDataGL GL INNER JOIN SAInvoiceDetail SAD ON SAD.ID = GL.DetailID
			LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		WHERE (GL.PostedDate between @FromDate and @ToDate) AND SA.BillRefID is null AND GL.TypeID in (320, 323)
		GROUP BY GL.Date, GL.PostedDate, GL.No, GL.InvoiceDate, GL.InvoiceNo, GL.Reason, GL.AccountingObjectID, GL.ReferenceID, GL.TypeID
		/*lay du lieu SAInvoice: BillRefID is not null*/
		UNION ALL
		SELECT GL.Date as [Date], GL.PostedDate as PostedDate, GL.No as [No], SAB.InvoiceDate as InvoiceDate, SAB.InvoiceNo as InvoiceNo, GL.Reason as [Description],
				SUM(CASE WHEN GL.Account like '5111%' THEN GL.CreditAmount ELSE $0 END) as TurnOverAmountInv,
				SUM(CASE WHEN GL.Account like '5112%' THEN GL.CreditAmount ELSE $0 END) as TurnOverAmountFinishedInv,
				SUM(CASE WHEN GL.Account like '5113%' THEN GL.CreditAmount ELSE $0 END) as TurnOverAmountServiceInv,
				SUM(CASE WHEN GL.Account like '5118%' THEn GL.CreditAmount ELSE $0 END) as TurnOverAmountOther,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID not in (330,340)) THEN GL.DebitAmount ELSE $0 END) as DiscountAmount,
				0 as ReturnAmount, 0 as ReduceAmount, GL.AccountingObjectID as CustomerID, NULL as TypeData, GL.ReferenceID as RefID, GL.TypeID
		FROM @tbDataGL GL INNER JOIN SAInvoiceDetail SAD ON SAD.ID = GL.DetailID
			LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		WHERE (GL.PostedDate between @FromDate and @ToDate) AND SA.BillRefID is not null AND GL.TypeID in (320, 323)
		GROUP BY GL.Date, GL.PostedDate, GL.No, SAB.InvoiceDate, SAB.InvoiceNo, GL.Reason, GL.AccountingObjectID, GL.ReferenceID, GL.TypeID
		/*lay gia tri Tra lai giam gia SAInvoice: BillRefID is null*/
		UNION ALL
		SELECT GL.Date as [Date], GL.PostedDate as PostedDate, GL.No as [No], GL.InvoiceDate as InvoiceDate, GL.InvoiceNo as InvoiceNo, GL.Reason as [Description],
				0 as TurnOverAmountInv,
				0 as TurnOverAmountFinishedInv,
				0 as TurnOverAmountServiceInv,
				0 as TurnOverAmountOther,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID in (330,340)) THEN GL.CreditAmount ELSE $0 END) as DiscountAmount,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID = 330) THEN GL.DebitAmount ELSE $0 END) as ReturnAmount,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID = 340) THEN GL.DebitAmount ELSE $0 END) as ReduceAmount, GL.AccountingObjectID as CustomerID, 'TLGG' as TypeData, GL.ReferenceID as RefID, GL.TypeID
		FROM @tbDataGL GL INNER JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID = SAR.SAInvoiceDetailID
			LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		WHERE (GL.PostedDate between @FromDate and @ToDate) AND SA.BillRefID is null AND GL.TypeID in (320, 323)
		GROUP BY GL.Date, GL.PostedDate, GL.No, GL.InvoiceDate, GL.InvoiceNo, GL.Reason, GL.AccountingObjectID, GL.ReferenceID, GL.TypeID
		/*lay gia tri Tra lai giam gia SAInvoice: BillRefID is not null*/
		UNION ALL
		SELECT GL.Date as [Date], GL.PostedDate as PostedDate, GL.No as [No], SAB.InvoiceDate as InvoiceDate, SAB.InvoiceNo as InvoiceNo, GL.Reason as [Description],
				0 as TurnOverAmountInv,
				0 as TurnOverAmountFinishedInv,
				0 as TurnOverAmountServiceInv,
				0 as TurnOverAmountOther,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID in (330,340)) THEN GL.CreditAmount ELSE $0 END) as DiscountAmount,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID = 330) THEN GL.DebitAmount ELSE $0 END) as ReturnAmount,
				SUM(CASE WHEN (GL.Account like '5111%' and GL.typeID = 340) THEN GL.DebitAmount ELSE $0 END) as ReduceAmount, GL.AccountingObjectID as CustomerID, 'TLGG' as TypeData, GL.ReferenceID as RefID, GL.TypeID
		FROM @tbDataGL GL INNER JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID = SAR.SAInvoiceDetailID
			LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		WHERE (GL.PostedDate between @FromDate and @ToDate) AND SA.BillRefID is not null AND GL.TypeID in (320, 323)
		GROUP BY GL.Date, GL.PostedDate, GL.No, SAB.InvoiceDate, SAB.InvoiceNo, GL.Reason, GL.AccountingObjectID, GL.ReferenceID, GL.TypeID

		/*update ten doi tuong vao */
		UPDATE @tbResults SET CustomerCode = a.CustomerCode, CustomerName = a.CustomerName
		FROM(
			select ID as cusID, AccountingObjectCode as CustomerCode, AccountingObjectName as CustomerName from AccountingObject
		) a
		WHERE CustomerID = a.cusID

		

       select
				RefID,
				TypeID,
				Date, 
				PostedDate, 
				No,
				InvoiceDate, 
				InvoiceNo, 
				Description,
				TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther as SumTurnOver,
				TurnOverAmountInv ,
				TurnOverAmountFinishedInv ,
                TurnOverAmountServiceInv ,
                TurnOverAmountOther,
                (CASE WHEN TypeData='TLGG' THEN -DiscountAmount ELSE DiscountAmount END) as DiscountAmount ,
				ReturnAmount,
                ReduceAmount,
				(CASE WHEN TypeData='TLGG' THEN ((TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther) - (-DiscountAmount + ReturnAmount + ReduceAmount)) 
				ELSE ((TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther) - (DiscountAmount + ReturnAmount + ReduceAmount)) END) as TurnOverPure,
				CustomerCode, 
				CustomerName 
   from @tbResults 
   where ((TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv + TurnOverAmountOther) <> 0) OR ((DiscountAmount + ReturnAmount + ReduceAmount) <> 0)
   ORDER BY Date, No 
	END
	END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_PO_DiaryBook]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER = NULL ,
    @IncludeDependentBranch BIT = 0 ,
    @IsNotPaid BIT = 0
AS 
    BEGIN
        
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)


if(@IsNotPaid = 0)
Begin
		/*Add by cuongpv de sua cach lam tron*/
		
		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
		/*end add by cuongpv*/
		
        select
		 DetailID,
		 TypeID,
		 PostedDate,
		 RefDate,
		 RefNo,
		 InvoiceDate,
		 InvoiceNo,
		 Description,
		 GoodsAmount,
		 RefID,
		 EquipmentAmount,
		 AnotherAccount,
		 AnotherAmount,
		 sum(GoodsAmount) + sum(EquipmentAmount) + sum(AnotherAmount) as PaymentableAmount
		
		from
		(select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description , sum(DebitAmount) as GoodsAmount, a.ReferenceID as RefID,
		0 as EquipmentAmount, null as AnotherAccount, 0 as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join PPInvoiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '156' and AccountCorresponding not in ('3333', '3332') 
		group by  a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID,a.OrderPriority, a.ReferenceID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description , sum(DebitAmount) as GoodsAmount,a.ReferenceID as RefID,
		0 as EquipmentAmount, null as AnotherAccount, 0 as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join PPServiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '156' and AccountCorresponding not in ('3333', '3332')
		group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID,a.OrderPriority, a.ReferenceID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description, sum(DebitAmount) as GoodsAmount,a.ReferenceID as RefID,
		0 as EquipmentAmount, null as AnotherAccount, 0 as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join TIIncrementDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '156' and AccountCorresponding not in ('3333', '3332')
		group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID,a.OrderPriority, a.ReferenceID

		union all

		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount, a.ReferenceID as RefID, sum(DebitAmount) as EquipmentAmount, 
		null as AnotherAccount, 0 as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join PPInvoiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '152' and AccountCorresponding not in ('3333', '3332')
		group by  a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID,a.OrderPriority, a.ReferenceID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount, a.ReferenceID as RefID, sum(DebitAmount) as EquipmentAmount,
		null as AnotherAccount, 0 as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join PPServiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '152' and AccountCorresponding not in ('3333', '3332')
		group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID,a.OrderPriority, a.ReferenceID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description,0 as GoodsAmount , a.ReferenceID as RefID, sum(DebitAmount) as EquipmentAmount,
		null as AnotherAccount, 0 as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join TIIncrementDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '152' and AccountCorresponding not in ('3333', '3332') 
		group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID,a.OrderPriority, a.ReferenceID
		union all

		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description,0 as GoodsAmount, a.ReferenceID as RefID, 0 as EquipmentAmount , a.Account as AnotherAccount, sum(DebitAmount) as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join PPInvoiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account in ('153','154','242') and AccountCorresponding not in ('3333', '3332') and AccountCorresponding in ('331', '111','112') 
		group by  a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , a.Account, b.Description, a.DetailID,a.TypeID,a.OrderPriority, a.ReferenceID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount, a.ReferenceID as RefID, 0 as EquipmentAmount, a.Account as AnotherAccount, sum(DebitAmount) as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join PPServiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate  /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		  and a.Account in ('153','154','242') and AccountCorresponding not in ('3333', '3332') and AccountCorresponding in ('331', '111','112') 
		group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , a.Account, b.Description, a.DetailID,a.TypeID,a.OrderPriority, a.ReferenceID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount, a.ReferenceID as RefID, 0 as EquipmentAmount, a.Account as AnotherAccount,sum(DebitAmount) as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join TIIncrementDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account in ('153','154','242') and AccountCorresponding not in ('3333', '3332') and AccountCorresponding in ('331', '111','112') 
		group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , a.Account, b.Description, a.DetailID,a.TypeID,a.OrderPriority, a.ReferenceID) t
group by 
         DetailID,
		 TypeID,
		 PostedDate,
		 OrderPriority,
		 RefDate,
		 RefNo,
		 InvoiceDate,
		 InvoiceNo,
		 Description,
		 GoodsAmount,
		 RefID,
		 EquipmentAmount,
		 AnotherAccount,
		 AnotherAmount
		 
ORDER BY PostedDate,OrderPriority ,
               RefDate ,RefNo           
   
   END
   else
   Begin
		/*Add by cuongpv de sua cach lam tron*/
		
		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
		/*end add by cuongpv*/
		
        select
		 DetailID,
		 TypeID,
		 PostedDate,
		 RefDate,
		 RefNo,
		 InvoiceDate,
		 InvoiceNo,
		 Description,
		 GoodsAmount,
		 RefID,
		 EquipmentAmount,
		 AnotherAccount,
		 AnotherAmount,
		 sum(GoodsAmount) + sum(EquipmentAmount) + sum(AnotherAmount) as PaymentableAmount
		
		from
		(select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description , sum(DebitAmount) as GoodsAmount, a.ReferenceID as RefID,
		0 as EquipmentAmount, null as AnotherAccount, 0 as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join PPInvoiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '156' and AccountCorresponding not in ('3333', '3332') and a.TypeID in (210, 240, 430, 500)
		group by  a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID,a.OrderPriority, a.ReferenceID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description , sum(DebitAmount) as GoodsAmount,a.ReferenceID as RefID,
		0 as EquipmentAmount, null as AnotherAccount, 0 as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join PPServiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '156' and AccountCorresponding not in ('3333', '3332') and a.TypeID in (210, 240, 430, 500)
		group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID,a.OrderPriority, a.ReferenceID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description, sum(DebitAmount) as GoodsAmount,a.ReferenceID as RefID,
		0 as EquipmentAmount, null as AnotherAccount, 0 as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join TIIncrementDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '156' and AccountCorresponding not in ('3333', '3332') and a.TypeID in (210, 240, 430, 500)
		group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID,a.OrderPriority, a.ReferenceID

		union all

		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount, a.ReferenceID as RefID, sum(DebitAmount) as EquipmentAmount, 
		null as AnotherAccount, 0 as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join PPInvoiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '152' and AccountCorresponding not in ('3333', '3332') and a.TypeID in (210, 240, 430, 500)
		group by  a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID,a.OrderPriority, a.ReferenceID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount, a.ReferenceID as RefID, sum(DebitAmount) as EquipmentAmount,
		null as AnotherAccount, 0 as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join PPServiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '152' and AccountCorresponding not in ('3333', '3332') and a.TypeID in (210, 240, 430, 500)
		group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID,a.OrderPriority, a.ReferenceID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description,0 as GoodsAmount , a.ReferenceID as RefID, sum(DebitAmount) as EquipmentAmount,
		null as AnotherAccount, 0 as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join TIIncrementDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '152' and AccountCorresponding not in ('3333', '3332') and a.TypeID in (210, 240, 430, 500)
		group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID,a.OrderPriority, a.ReferenceID
		union all

		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description,0 as GoodsAmount, a.ReferenceID as RefID, 0 as EquipmentAmount , a.Account as AnotherAccount, sum(DebitAmount) as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join PPInvoiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account in ('153','154','242') and AccountCorresponding not in ('3333', '3332') and AccountCorresponding in ('331', '111','112') and a.TypeID in (210, 240, 430, 500)
		group by  a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , a.Account, b.Description, a.DetailID,a.TypeID,a.OrderPriority, a.ReferenceID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount, a.ReferenceID as RefID, 0 as EquipmentAmount, a.Account as AnotherAccount, sum(DebitAmount) as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join PPServiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate  /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		  and a.Account in ('153','154','242') and AccountCorresponding not in ('3333', '3332') and AccountCorresponding in ('331', '111','112') and a.TypeID in (210, 240, 430, 500)
		group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , a.Account, b.Description, a.DetailID,a.TypeID,a.OrderPriority, a.ReferenceID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount, a.ReferenceID as RefID, 0 as EquipmentAmount, a.Account as AnotherAccount,sum(DebitAmount) as AnotherAmount,a.TypeID,a.OrderPriority
		  from @tbDataGL a join TIIncrementDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account in ('153','154','242') and AccountCorresponding not in ('3333', '3332') and AccountCorresponding in ('331', '111','112') and a.TypeID in (210, 240, 430, 500)
		group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , a.Account, b.Description, a.DetailID,a.TypeID,a.OrderPriority, a.ReferenceID) t
group by 
         DetailID,
		 TypeID,
		 PostedDate,
		 OrderPriority,
		 RefDate,
		 RefNo,
		 InvoiceDate,
		 InvoiceNo,
		 Description,
		 GoodsAmount,
		 RefID,
		 EquipmentAmount,
		 AnotherAccount,
		 AnotherAmount
		 
ORDER BY PostedDate,OrderPriority ,
               RefDate ,RefNo           
   
   END
  
END
    


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

ALTER PROCEDURE [dbo].[Proc_GL_GeneralDiaryBook_S03a]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @GroupTheSameItem BIT ,/*cộng gộp các bút toán giống nhau*/
    @IsShowAccumAmount BIT
AS 
    BEGIN
    
       DECLARE @PrevFromDate AS DATETIME
       SET @PrevFromDate = DATEADD(MILLISECOND, -10, @FromDate)
       DECLARE @StartDate DATETIME
        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'

		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        IF @GroupTheSameItem = 1 
            BEGIN
			select * from
            (

        SELECT  ROW_NUMBER() OVER ( ORDER BY gl.PostedDate, gl.RefDate
						 		,CASE WHEN GL.TypeID = 4012 THEN 1 ELSE 0 END
								, GL.RefNo , GL.TypeID ) AS RowNum ,
                        CAST (1 AS BIT) AS IsSummaryRow ,
						CAST(0 AS BIT) AS IsBold ,
                        GL.[ReferenceID] ,
                        GL.[TypeID] ,
                        GL.[PostedDate] ,
                        GL.[RefDate] ,
                        GL.No AS [RefNo],
                        GL.No AS RefNo1,
                        NULL AS RefOrder ,
                        GL.[InvoiceDate] ,
                        GL.[InvoiceNo] ,
                        CASE WHEN (Account like '133%' OR AccountCorresponding like '3331%') OR ( AccountCorresponding like '133%' OR Account like '3331%')
						THEN (CASE WHEN (GL.[VATDescription] IS NULL OR GL.[VATDescription] ='' ) THEN GL.[Reason] ELSE GL.[VATDescription] END)
						ELSE GL.[Reason]
						END AS JournalMemo,/*edit by cuongpv Description -> Reason*/

                        /*bổ sung trường diễn giải thông tin chung cr 137228*/              
						GL.Reason AS JournalMemoMaster ,/*edit by cuongpv Description -> Reason*/
                        GL.[Account] as AccountNumber ,
                        GL.[AccountCorresponding] as CorrespondingAccountNumber ,
						'' AS [RefTypeName],
                        '' AS AccountObjectCode ,
                        '' AS AccountObjectName ,
                        '' AS EmployeeCode ,
                        '' AS EmployeeName ,
                        '' AS ExpenseItemCode ,
                        '' AS ExpenseItemName ,
                        '' AS JobCode ,
                        '' AS JobName ,
                        '' AS ProjectWorkCode ,
                        '' AS ProjectWorkName ,
                        '' AS OrderNo ,
                        '' AS [PUContractCode] ,
                        '' AS [ContractCode] ,
                        '' AS ListItemCode ,
                        '' AS ListItemName ,
                        SUM(GL.[DebitAmount]) AS DebitAmount ,
                        null AS CreditAmount ,
                        '' as BranchName,
                        '' AS OrganizationUnitCode ,
                        '' AS OrganizationUnitName ,
                        '' AS MasterCustomField1 ,
                        '' AS MasterCustomField2 ,
                        '' AS MasterCustomField3 ,
                        '' AS MasterCustomField4 ,
                        '' AS MasterCustomField5 ,
                        '' AS MasterCustomField6 ,
                        '' AS MasterCustomField7 ,
                        '' AS MasterCustomField8 ,
                        '' AS MasterCustomField9 ,
                        '' AS MasterCustomField10 ,
                        '' AS CustomField1 ,
                        '' AS CustomField2 ,
                        '' AS CustomField3 ,
                        '' AS CustomField4 ,
                        '' AS CustomField5 ,
                        '' AS CustomField6 ,
                        '' AS CustomField7 ,
                        '' AS CustomField8 ,
                        '' AS CustomField9 ,
                        '' AS CustomField10 ,
                        '' AS UnResonableCost ,
                        CASE WHEN SUM(DebitAmount) <> 0
                                THEN Gl.Account
                                    + AccountCorresponding
                                WHEN SUM(CreditAmount) <> 0
                                THEN gl.AccountCorresponding
                                    + GL.Account
                        END AS Sort ,
                        GL.ReferenceID
                        AS RefIDSort,
                        0 AS SortOrder,
                        0 AS DetailPostOrder,
						MAX(GL.OrderPriority) as OrderPriority,
                        SUM(DebitAmount) as OrderTotal
                FROM      @tbDataGL GL /*edit by cuongpv [dbo].[GeneralLedger] -> @tbDataGL*/
                        LEFT JOIN dbo.AccountingObject ao ON ao.ID = GL.AccountingObjectID
                WHERE     gl.PostedDate BETWEEN @FromDate AND @ToDate
                        AND ISNULL(gl.AccountCorresponding,'') <> ''
                GROUP BY  GL.[ReferenceID] ,
                        GL.[TypeID] ,
                        GL.[PostedDate] ,
                        GL.[RefDate] ,
                        GL.[RefNo] ,
                        GL.No ,
                        GL.[InvoiceDate] ,
                        GL.[InvoiceNo] ,
                        GL.[VATDescription],
						GL.[Reason] ,
                        GL.[Account] ,
                        GL.[AccountCorresponding]
                                            
                HAVING    SUM(GL.[DebitAmount]) <> 0 /*edit by cuongpv bo (OR SUM(GL.[CreditAmount]) <> 0)*/
			UNION ALL 
			/*add by cuongpv*/
			SELECT  ROW_NUMBER() OVER ( ORDER BY gl.PostedDate, gl.RefDate
						 		,CASE WHEN GL.TypeID = 4012 THEN 1 ELSE 0 END
								, GL.RefNo , GL.TypeID ) AS RowNum ,
                        CAST (1 AS BIT) AS IsSummaryRow ,
						CAST(0 AS BIT) AS IsBold ,
                        GL.[ReferenceID] ,
                        GL.[TypeID] ,
                        GL.[PostedDate] ,
                        GL.[RefDate] ,
                        GL.No,
                        GL.No ,
                        NULL AS RefOrder ,
                        GL.[InvoiceDate] ,
                        GL.[InvoiceNo] ,
                        CASE WHEN (Account like '133%' OR AccountCorresponding like '3331%') OR ( AccountCorresponding like '133%' OR Account like '3331%')
						THEN (CASE WHEN (GL.[VATDescription] IS NULL OR GL.[VATDescription] ='') THEN GL.[Reason] ELSE GL.[VATDescription] END)
						ELSE GL.[Reason]
						END AS JournalMemo,/*edit by cuongpv Description -> Reason*/
                        /*bổ sung trường diễn giải thông tin chung cr 137228*/              
						GL.Reason AS JournalMemoMaster ,/*edit by cuongpv Description -> Reason*/
                        GL.[Account] as AccountNumber ,
                        GL.[AccountCorresponding] as CorrespondingAccountNumber ,
						'' AS [RefTypeName],
                        '' AS AccountObjectCode ,
                        '' AS AccountObjectName ,
                        '' AS EmployeeCode ,
                        '' AS EmployeeName ,
                        '' AS ExpenseItemCode ,
                        '' AS ExpenseItemName ,
                        '' AS JobCode ,
                        '' AS JobName ,
                        '' AS ProjectWorkCode ,
                        '' AS ProjectWorkName ,
                        '' AS OrderNo ,
                        '' AS [PUContractCode] ,
                        '' AS [ContractCode] ,
                        '' AS ListItemCode ,
                        '' AS ListItemName ,
                        null AS DebitAmount ,
                        SUM(GL.[CreditAmount]) AS CreditAmount ,
                        '' as BranchName,
                        '' AS OrganizationUnitCode ,
                        '' AS OrganizationUnitName ,
                        '' AS MasterCustomField1 ,
                        '' AS MasterCustomField2 ,
                        '' AS MasterCustomField3 ,
                        '' AS MasterCustomField4 ,
                        '' AS MasterCustomField5 ,
                        '' AS MasterCustomField6 ,
                        '' AS MasterCustomField7 ,
                        '' AS MasterCustomField8 ,
                        '' AS MasterCustomField9 ,
                        '' AS MasterCustomField10 ,
                        '' AS CustomField1 ,
                        '' AS CustomField2 ,
                        '' AS CustomField3 ,
                        '' AS CustomField4 ,
                        '' AS CustomField5 ,
                        '' AS CustomField6 ,
                        '' AS CustomField7 ,
                        '' AS CustomField8 ,
                        '' AS CustomField9 ,
                        '' AS CustomField10 ,
                        '' AS UnResonableCost ,
                        CASE WHEN SUM(DebitAmount) <> 0
                                THEN Gl.Account
                                    + AccountCorresponding
                                WHEN SUM(CreditAmount) <> 0
                                THEN gl.AccountCorresponding
                                    + GL.Account
                        END AS Sort ,
                        GL.ReferenceID
                        AS RefIDSort,
                        1 AS SortOrder,
                        0 AS DetailPostOrder,
						MAX(GL.OrderPriority) as OrderPriority,
                        SUM(CreditAmount) as OrderTotal                    
                FROM      @tbDataGL GL /*edit by cuongpv [dbo].[GeneralLedger] -> @tbDataGL*/
                        LEFT JOIN dbo.AccountingObject ao ON ao.ID = GL.AccountingObjectID
                WHERE     gl.PostedDate BETWEEN @FromDate AND @ToDate
                        AND ISNULL(gl.AccountCorresponding,'') <> ''
                GROUP BY  GL.[ReferenceID] ,
                        GL.[TypeID] ,
                        GL.[PostedDate] ,
                        GL.[RefDate] ,
                        GL.[RefNo] ,
                        GL.No ,
                        GL.[InvoiceDate] ,
                        GL.[InvoiceNo] ,
                        GL.[VATDescription],
						GL.[Reason] ,
                        GL.[Account] ,
                        GL.[AccountCorresponding]
                                            
                HAVING    SUM(GL.[CreditAmount]) <> 0
			/*end add by cuongpv*/
            ) t
			order by PostedDate,RefNo,OrderPriority,SortOrder
			END
        ELSE 
            BEGIN
			select * from
				(

			SELECT  
								ROW_NUMBER() OVER ( ORDER BY gl.PostedDate, gl.RefDate
						 			,CASE WHEN GL.TypeID = 4012 THEN 1 ELSE 0 END
									, GL.RefNo , GL.TypeID ) AS RowNum ,							
								CAST (1 AS BIT) AS IsSummaryRow ,
								CAST(0 AS BIT) AS IsBold ,
								GL.[ReferenceID] ,
								GL.[TypeID] ,
								GL.[PostedDate] as PostedDate,
								GL.[RefDate] ,
								GL.No AS [RefNo],
								GL.No AS RefNo1,
								'' AS RefOrder ,
								GL.[InvoiceDate] ,
								GL.[InvoiceNo] ,
								CASE WHEN (Account like '133%' OR AccountCorresponding like '3331%') OR ( AccountCorresponding like '133%' OR Account like '3331%')
								THEN (CASE WHEN (GL.[VATDescription] IS NULL OR GL.[VATDescription] ='') THEN GL.[Reason] ELSE GL.[VATDescription] END)
								ELSE GL.[Reason]
								END AS JournalMemo,/*edit by cuongpv Description -> Reason*/
								/* bổ sung trường diễn giải thông tin chung cr 137228*/              
								GL.Reason AS JournalMemoMaster ,
								GL.[Account] as AccountNumber ,
								GL.[AccountCorresponding] as CorrespondingAccountNumber ,
								'' as RefTypeName,
								ao.AccountingObjectCode ,
								ao.AccountingObjectName ,
								'' as EmployeeCode,
								'' as EmployeeName,
								'' as ExpenseItemCode ,
								'' as ExpenseItemName ,
								'' as JobCode,
								'' as JobName,
								'' as ProjectWorkCode ,
								'' as ProjectWorkName ,
								'' as OrderNo,
								'' as PUContractCode,
								'' as ContractCode ,
								'' as ListItemCode ,
								'' as ListItemName ,
								GL.[DebitAmount] ,
								GL.[CreditAmount] ,
								'' as BranchName,
								'' as OrganizationUnitCode,
								'' as OrganizationUnitName,
								'' as MasterCustomField1,
								'' as MasterCustomField2,
								'' as MasterCustomField3,
								'' as MasterCustomField4,
								'' as MasterCustomField5,
								'' as MasterCustomField6,
								'' as MasterCustomField7,
								'' as MasterCustomField8,
								'' as MasterCustomField9,
								'' as MasterCustomField10,
								'' as CustomField1,
								'' as CustomField2,
								'' as CustomField3,
								'' as CustomField4,
								'' as CustomField5,
								'' as CustomField6,
								'' as CustomField7,
								'' as CustomField8,
								'' as CustomField9,
								'' as CustomField10,
								N'Chi phí hợp lý' AS UnResonableCost ,
								CASE WHEN DebitAmount <> 0
										THEN Gl.Account
											+ AccountCorresponding
											+ CAST(DebitAmount AS NVARCHAR(22))
										WHEN CreditAmount <> 0
										THEN gl.AccountCorresponding
											+ GL.Account
											+ CAST(CreditAmount AS NVARCHAR(22))
								END AS Sort ,
								GL.ReferenceID
								AS RefIDSort,
								0 as SortOrder,
								0 as DetailPostOrder,
								GL.OrderPriority as OrderPriority
						FROM      @tbDataGL GL /*edit by cuongpv [dbo].[GeneralLedger] -> @tbDataGL*/
								LEFT JOIN dbo.AccountingObject ao ON ao.ID = GL.AccountingObjectID
						WHERE     gl.PostedDate BETWEEN @FromDate AND @ToDate
								AND ISNULL(gl.AccountCorresponding,
											'') <> ''
								AND ( GL.[DebitAmount] <> 0
										OR GL.[CreditAmount] <> 0
									)
				 ) t
				order by t.PostedDate, t.OrderPriority
            END 
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GetHealthFinanceChart]
      ( @FromDate DATETIME ,
        @ToDate DATETIME
		)
AS
BEGIN
	DECLARE @tbTemp TABLE(
		PostedDate datetime,
		PostedDay int,
		PostedMonth int,
		PostedYear int,
		Account nvarchar(25),
		AccountCorresponding nvarchar(25),
		DebitAmount decimal(25,0),
		CreditAmount decimal(25,0)
	)
	
	INSERT INTO @tbTemp
	SELECT PostedDate, DAY(PostedDate) as Ngay, MONTH(PostedDate) as Thang, YEAR(PostedDate) as Nam, Account, AccountCorresponding, 
	DebitAmount, CreditAmount
	FROM GeneralLedger
	WHERE PostedDate between @FromDate AND @ToDate
	
	Declare @tbDataTempChart TABLE(
		PostedMonth int,
		PostedYear int,
		TurnoverTotal money,
		CostsTotal money
	)
	
	INSERT INTO @tbDataTempChart(PostedMonth,PostedYear,TurnoverTotal)
	SELECT PostedMonth, PostedYear, SUM(CreditAmount-DebitAmount) as TurnoverTotal
	FROM @tbTemp
	WHERE ((Account like '511%') OR (Account like '515%') OR (Account like '711%')) AND AccountCorresponding <> '911'
	GROUP BY PostedMonth, PostedYear
	
	INSERT INTO @tbDataTempChart(PostedMonth,PostedYear,CostsTotal)
	SELECT PostedMonth, PostedYear, SUM(DebitAmount-CreditAmount) as CostsTotal
	FROM @tbTemp
	WHERE ((Account like '632%') OR (Account like '642%') OR (Account like '635%') OR (Account like '811%') OR (Account like '821%')) 
	AND AccountCorresponding <> '911'
	GROUP BY PostedMonth, PostedYear
	
	Declare @tbDataChart TABLE(
		PostedMonthYear nvarchar(15),
		PostedMonth int,
		PostedYear int,
		TurnoverTotal money,
		CostsTotal money
	)
	
	INSERT INTO @tbDataChart(PostedMonth,PostedYear,TurnoverTotal,CostsTotal)
	SELECT PostedMonth,PostedYear,SUM(TurnoverTotal) as TurnoverTotal, SUM(CostsTotal) as CostsTotal
	FROM @tbDataTempChart
	GROUP BY PostedMonth,PostedYear
	
	UPDATE @tbDataChart SET TurnoverTotal=0 WHERE  TurnoverTotal<0
	UPDATE @tbDataChart SET CostsTotal=0 WHERE  CostsTotal<0
	
	UPDATE @tbDataChart SET PostedMonthYear=CONVERT(nvarchar(2),PostedMonth)+'/'+CONVERT(nvarchar(4),PostedYear)
	
	SELECT PostedMonthYear,TurnoverTotal,CostsTotal FROM @tbDataChart
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_GetCACashBookInCABook]
    (
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @CurrencyID NVARCHAR(3) ,
      @AccountNumber NVARCHAR(25)
    )
AS
    BEGIN

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
              RefType INT ,
              RefDate DATETIME ,
              PostedDate DATETIME ,
              ReceiptRefNo NVARCHAR(20)  ,
              PaymentRefNo NVARCHAR(20)  ,
              CashBookPostedDate DATETIME ,
              AccountObjectName NVARCHAR(512)
                 ,
              JournalMemo NVARCHAR(512)  ,
              CurrencyID NVARCHAR(3)  ,
              TotalReceiptFBCurrencyID MONEY ,
              TotalPaymentFBCurrencyID MONEY ,
              ClosingFBCurrencyID MONEY ,
              BranchID UNIQUEIDENTIFIER ,
              BranchName NVARCHAR(512)  ,
              RefTypeName NVARCHAR(100)  ,
              Note NVARCHAR(255)  ,
              CAType INT ,
              IsBold BIT ,
               /* - bổ sung mã nhân viên , tên nhân viên */
              EmployeeCode NVARCHAR(25)  ,
              EmployeeName NVARCHAR(512)  ,
			  OrderPriority int
            )       
       
	   IF(@CurrencyID = 'VND')
	   BEGIN
			/*Add by cuongpv de sua cach lam tron*/
			DECLARE @tbDataGL TABLE(
				ID uniqueidentifier,
				BranchID uniqueidentifier,
				ReferenceID uniqueidentifier,
				TypeID int,
				Date datetime,
				PostedDate datetime,
				No nvarchar(25),
				InvoiceDate datetime,
				InvoiceNo nvarchar(25),
				Account nvarchar(25),
				AccountCorresponding nvarchar(25),
				BankAccountDetailID uniqueidentifier,
				CurrencyID nvarchar(3),
				ExchangeRate decimal(25, 10),
				DebitAmount decimal(25,0),
				DebitAmountOriginal decimal(25,0),
				CreditAmount decimal(25,0),
				CreditAmountOriginal decimal(25,0),
				Reason nvarchar(512),
				Description nvarchar(512),
				VATDescription nvarchar(512),
				AccountingObjectID uniqueidentifier,
				EmployeeID uniqueidentifier,
				BudgetItemID uniqueidentifier,
				CostSetID uniqueidentifier,
				ContractID uniqueidentifier,
				StatisticsCodeID uniqueidentifier,
				InvoiceSeries nvarchar(25),
				ContactName nvarchar(512),
				DetailID uniqueidentifier,
				RefNo nvarchar(25),
				RefDate datetime,
				DepartmentID uniqueidentifier,
				ExpenseItemID uniqueidentifier,
				OrderPriority int,
				IsIrrationalCost bit
			)

			INSERT INTO @tbDataGL
			SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
			/*end add by cuongpv*/

			INSERT  #Result
                ( RefID ,
                  RefType ,
                  RefDate ,
                  PostedDate ,
                  ReceiptRefNo ,
                  PaymentRefNo ,
                  CashBookPostedDate ,
                  AccountObjectName ,
                  JournalMemo ,
                  CurrencyID ,
                  TotalReceiptFBCurrencyID ,
                  TotalPaymentFBCurrencyID ,
                  ClosingFBCurrencyID ,
                  BranchID ,
                  BranchName ,
                  RefTypeName ,
                  CAType ,
                  IsBold ,
                  EmployeeCode ,
                  EmployeeName,
				  OrderPriority
                )
                SELECT  RefID ,
                        RefType ,
                        RefDate ,
                        R.PostedDate ,
                        CASE WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        CASE WHEN TotalPaymentFBCurrencyID
                                  - TotalReceiptFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        R.PostedDate ,
                        ContactName ,
                        JournalMemo ,
                        @CurrencyID ,
                        SUM(CASE WHEN TotalReceiptFBCurrencyID
                                      - TotalPaymentFBCurrencyID > 0
                                 THEN TotalReceiptFBCurrencyID
                                      - TotalPaymentFBCurrencyID
                                 ELSE 0
                            END) AS TotalReceiptFBCurrencyID ,
                        SUM(CASE WHEN TotalPaymentFBCurrencyID
                                      - TotalReceiptFBCurrencyID > 0
                                 THEN TotalPaymentFBCurrencyID
                                      - TotalReceiptFBCurrencyID
                                 ELSE 0
                            END) AS TotalPaymentFBCurrencyID ,
                        SUM(ClosingFBCurrencyID) ,
                        BranchID ,
                        BranchName ,
                        RefTypeName ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END AS CAType ,
                        IsBold ,
                        EmployeeCode ,
                        EmployeeName,
						OrderPriority
                FROM    ( SELECT    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ReferenceID
                                    END AS RefID ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.TypeID
                                    END AS RefType ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefDate
                                    END AS RefDate ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END AS PostedDate ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefNo
                                    END AS RefNo ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ContactName
                                    END AS ContactName ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN N'Số dư đầu kỳ'
                                         ELSE GL.Description
                                    END AS JournalMemo ,
                                    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.DebitAmount
                                                  - GL.CreditAmount > 0
                                             THEN GL.DebitAmount
                                                  - GL.CreditAmount
                                             ELSE 0
                                        END) AS TotalReceiptFBCurrencyID ,
                                    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.CreditAmount
                                                  - GL.DebitAmount > 0
                                             THEN GL.CreditAmount
                                                  - GL.DebitAmount
                                             ELSE 0
                                        END) AS TotalPaymentFBCurrencyID ,
                                    SUM(CASE WHEN GL.PostedDate < @FromDate
                                             THEN GL.DebitAmount
                                                  - GL.CreditAmount
                                             ELSE 0
                                        END) AS ClosingFBCurrencyID ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.BranchID
                                    END AS BranchID ,
                                    null AS BranchName ,
                                    null RefTypeName ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE 0
                                    END AS CAType ,
                                    CASE WHEN GL.PostedDate < @FromDate THEN 1
                                         ELSE 0
                                    END AS IsBold ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectCode
                                    END AS EmployeeCode ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectName
                                    END AS EmployeeName,
									CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE OrderPriority
                                    END AS OrderPriority
                          FROM      @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                    LEFT JOIN dbo.AccountingObject AS AO ON AO.ID = GL.EmployeeID
                          WHERE     ( GL.PostedDate <= @ToDate )
                                    AND GL.Account LIKE @AccountNumber
                          GROUP BY  CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ReferenceID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.TypeID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefNo
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ContactName
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN N'Số dư đầu kỳ'
                                         ELSE GL.Description
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.BranchID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE 0
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate THEN 1
                                         ELSE 0
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectCode
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectName
                                    END,
									 CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE OrderPriority
                                    END
                          HAVING    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.DebitAmount
                                                  - GL.CreditAmount > 0
                                             THEN GL.DebitAmount
                                                  - GL.CreditAmount
                                             ELSE 0
                                        END) <> SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                              AND GL.CreditAmount
                                                              - GL.DebitAmount > 0
                                                         THEN GL.CreditAmount
                                                              - GL.DebitAmount
                                                         ELSE 0
                                                    END)
                                    OR SUM(CASE WHEN GL.PostedDate < @FromDate
                                                THEN GL.DebitAmount
                                                     - GL.CreditAmount
                                                ELSE 0
                                           END) <> 0
                        ) AS R
                GROUP BY R.RefID ,
                        R.RefType ,
                        R.RefDate ,
                        R.PostedDate ,
                        CASE WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        CASE WHEN TotalPaymentFBCurrencyID
                                  - TotalReceiptFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        R.RefNo ,
                        R.ContactName ,
                        R.JournalMemo ,
                        R.BranchID ,
                        R.BranchName ,
                        R.RefTypeName ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END ,
                        R.IsBold ,
                        R.EmployeeCode ,
                        R.EmployeeName,
						R.OrderPriority
                ORDER BY R.PostedDate,R.OrderPriority,
                        R.RefDate ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END ,
                        R.RefNo	
	   END
	   ELSE
	   BEGIN
			INSERT  #Result
                ( RefID ,
                  RefType ,
                  RefDate ,
                  PostedDate ,
                  ReceiptRefNo ,
                  PaymentRefNo ,
                  CashBookPostedDate ,
                  AccountObjectName ,
                  JournalMemo ,
                  CurrencyID ,
                  TotalReceiptFBCurrencyID ,
                  TotalPaymentFBCurrencyID ,
                  ClosingFBCurrencyID ,
                  BranchID ,
                  BranchName ,
                  RefTypeName ,
                  CAType ,
                  IsBold ,
                  EmployeeCode ,
                  EmployeeName,
				  OrderPriority
                )
                SELECT  RefID ,
                        RefType ,
                        RefDate ,
                        R.PostedDate ,
                        CASE WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        CASE WHEN TotalPaymentFBCurrencyID
                                  - TotalReceiptFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        R.PostedDate ,
                        ContactName ,
                        JournalMemo ,
                        @CurrencyID ,
                        SUM(CASE WHEN TotalReceiptFBCurrencyID
                                      - TotalPaymentFBCurrencyID > 0
                                 THEN TotalReceiptFBCurrencyID
                                      - TotalPaymentFBCurrencyID
                                 ELSE 0
                            END) AS TotalReceiptFBCurrencyID ,
                        SUM(CASE WHEN TotalPaymentFBCurrencyID
                                      - TotalReceiptFBCurrencyID > 0
                                 THEN TotalPaymentFBCurrencyID
                                      - TotalReceiptFBCurrencyID
                                 ELSE 0
                            END) AS TotalPaymentFBCurrencyID ,
                        SUM(ClosingFBCurrencyID) ,
                        BranchID ,
                        BranchName ,
                        RefTypeName ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END AS CAType ,
                        IsBold ,
                        EmployeeCode ,
                        EmployeeName,
						OrderPriority
                FROM    ( SELECT    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ReferenceID
                                    END AS RefID ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.TypeID
                                    END AS RefType ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefDate
                                    END AS RefDate ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END AS PostedDate ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefNo
                                    END AS RefNo ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ContactName
                                    END AS ContactName ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN N'Số dư đầu kỳ'
                                         ELSE GL.Description
                                    END AS JournalMemo ,
                                    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal > 0
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) AS TotalReceiptFBCurrencyID ,
                                    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.CreditAmountOriginal
                                                  - GL.DebitAmountOriginal > 0
                                             THEN GL.CreditAmountOriginal
                                                  - GL.DebitAmountOriginal
                                             ELSE 0
                                        END) AS TotalPaymentFBCurrencyID ,
                                    SUM(CASE WHEN GL.PostedDate < @FromDate
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) AS ClosingFBCurrencyID ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.BranchID
                                    END AS BranchID ,
                                    null AS BranchName ,
                                    null RefTypeName ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE 0
                                    END AS CAType ,
                                    CASE WHEN GL.PostedDate < @FromDate THEN 1
                                         ELSE 0
                                    END AS IsBold ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectCode
                                    END AS EmployeeCode ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectName
                                    END AS EmployeeName,
									CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE OrderPriority
                                    END AS OrderPriority
                          FROM      dbo.GeneralLedger AS GL
                                    LEFT JOIN dbo.AccountingObject AS AO ON AO.ID = GL.EmployeeID
                          WHERE     ( GL.PostedDate <= @ToDate )
                                    AND ( @CurrencyID IS NULL  
                                          OR GL.CurrencyID = @CurrencyID 
                                        )
                                    AND GL.Account LIKE @AccountNumber
                                    + '%'
                          GROUP BY  CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ReferenceID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.TypeID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefNo
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ContactName
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN N'Số dư đầu kỳ'
                                         ELSE GL.Description
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.BranchID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE 0
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate THEN 1
                                         ELSE 0
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectCode
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectName
                                    END,
									 CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE OrderPriority
                                    END
                          HAVING    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal > 0
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) <> SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                              AND GL.CreditAmountOriginal
                                                              - GL.DebitAmountOriginal > 0
                                                         THEN GL.CreditAmountOriginal
                                                              - GL.DebitAmountOriginal
                                                         ELSE 0
                                                    END)
                                    OR SUM(CASE WHEN GL.PostedDate < @FromDate
                                                THEN GL.DebitAmountOriginal
                                                     - GL.CreditAmountOriginal
                                                ELSE 0
                                           END) <> 0
                        ) AS R
                GROUP BY R.RefID ,
                        R.RefType ,
                        R.RefDate ,
                        R.PostedDate ,
                        CASE WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        CASE WHEN TotalPaymentFBCurrencyID
                                  - TotalReceiptFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        R.RefNo ,
                        R.ContactName ,
                        R.JournalMemo ,
                        R.BranchID ,
                        R.BranchName ,
                        R.RefTypeName ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END ,
                        R.IsBold ,
                        R.EmployeeCode ,
                        R.EmployeeName,
						R.OrderPriority
                ORDER BY R.PostedDate,R.OrderPriority ,
                        R.RefDate ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END ,
                        R.RefNo	
	   END

        DECLARE @ClosingAmount AS DECIMAL(22, 8)
        SET @ClosingAmount = 0
        SELECT  @ClosingAmount = ClosingFBCurrencyID
        FROM    #Result
        WHERE   RefID IS NULL
        UPDATE  #Result
        SET     @ClosingAmount = @ClosingAmount
                + ISNULL(TotalReceiptFBCurrencyID, 0)
                - ISNULL(TotalPaymentFBCurrencyID, 0) ,
                ClosingFBCurrencyID = @ClosingAmount	
    
        SELECT  *
        FROM    #Result R       
    
        DROP TABLE #Result
        
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*

*/
ALTER PROCEDURE [dbo].[Proc_BA_GetOverBalanceBook]
    @BranchID UNIQUEIDENTIFIER = NULL ,
    @AccountNumber NVARCHAR(25) = NULL ,
    @BankAccountID UNIQUEIDENTIFIER = NULL ,
    @CurrencyID NVARCHAR(3) = NULL ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @IsWorkingWithManagementBook BIT ,
    @IncludeDependentBranch BIT
AS 
    SET NOCOUNT ON		
	/*add by cuongpv xu ly cach lam tron*/
	DECLARE @tbResult TABLE(
		RowNum BIGINT,
		ID UNIQUEIDENTIFIER,
		BankAccount NVARCHAR(50),
		BankName NVARCHAR(512),
		BankBranchName NVARCHAR(512),
		OpenAmount MONEY,
		DebitAmount MONEY,
		CreditAmount MONEY,
		CloseAmount MONEY
	)
	/*end add by cuongpv*/
    DECLARE @AccountNumberPercent NVARCHAR(26)
    SET @AccountNumberPercent = @AccountNumber + '%'

	IF(@CurrencyID is NULL OR @CurrencyID = 'VND')
	BEGIN
		/*Add by cuongpv de sua cach lam tron*/
			DECLARE @tbDataGL TABLE(
				ID uniqueidentifier,
				BranchID uniqueidentifier,
				ReferenceID uniqueidentifier,
				TypeID int,
				Date datetime,
				PostedDate datetime,
				No nvarchar(25),
				InvoiceDate datetime,
				InvoiceNo nvarchar(25),
				Account nvarchar(25),
				AccountCorresponding nvarchar(25),
				BankAccountDetailID uniqueidentifier,
				CurrencyID nvarchar(3),
				ExchangeRate decimal(25, 10),
				DebitAmount decimal(25,0),
				DebitAmountOriginal decimal(25,0),
				CreditAmount decimal(25,0),
				CreditAmountOriginal decimal(25,0),
				Reason nvarchar(512),
				Description nvarchar(512),
				VATDescription nvarchar(512),
				AccountingObjectID uniqueidentifier,
				EmployeeID uniqueidentifier,
				BudgetItemID uniqueidentifier,
				CostSetID uniqueidentifier,
				ContractID uniqueidentifier,
				StatisticsCodeID uniqueidentifier,
				InvoiceSeries nvarchar(25),
				ContactName nvarchar(512),
				DetailID uniqueidentifier,
				RefNo nvarchar(25),
				RefDate datetime,
				DepartmentID uniqueidentifier,
				ExpenseItemID uniqueidentifier,
				OrderPriority int,
				IsIrrationalCost bit
			)

			INSERT INTO @tbDataGL
			SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
			/*end add by cuongpv*/

			SELECT  ROW_NUMBER() OVER ( ORDER BY B.BankName, BA.BankAccount ) AS RowNum ,
					BA.ID,
					BA.BankAccount,
					B.BankName,
					BA.BankBranchName ,
					SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0.0 END) AS OpenAmount ,
					SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN  GL.DebitAmount ELSE 0.0 END) AS DebitAmount ,
					SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmount ELSE 0.0 END) AS CreditAmount ,
					SUM(GL.DebitAmount - GL.CreditAmount) AS CloseAmount
			FROM    @tbDataGL GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
					INNER JOIN dbo.BankAccountDetail BA ON GL.BankAccountDetailID = BA.ID          
					INNER JOIN dbo.Bank B ON BA.BankID =  B.ID
			WHERE   GL.PostedDate <= @ToDate
					AND ( GL.Account LIKE @AccountNumberPercent )
					AND ( GL.BankAccountDetailID = @BankAccountID
						  OR @BankAccountID IS NULL
						)
            
			GROUP BY BA.ID, 
				BA.BankAccount ,
				B.BankName,BA.BankBranchName
			HAVING
				SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0.0 END)<>0 OR 
				SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN  GL.DebitAmount ELSE 0.0 END)<>0 OR 
				SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmount ELSE 0.0 END)<>0 OR 
				SUM(GL.DebitAmount - GL.CreditAmount)<>0
	END
	ELSE
	BEGIN
		/*add by cuongpv*/
		SELECT  ROW_NUMBER() OVER ( ORDER BY B.BankName, BA.BankAccount ) AS RowNum ,
					BA.ID,
					BA.BankAccount,
					B.BankName,
					BA.BankBranchName ,
					SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal- GL.CreditAmountOriginal ELSE 0.0 END) AS OpenAmount ,
					SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.DebitAmountOriginal ELSE 0.0 END) AS DebitAmount ,
					SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmountOriginal ELSE 0.0 END) AS CreditAmount ,
					SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) AS CloseAmount
			FROM    dbo.GeneralLedger GL
					INNER JOIN dbo.BankAccountDetail BA ON GL.BankAccountDetailID = BA.ID          
					INNER JOIN dbo.Bank B ON BA.BankID =  B.ID
			WHERE   GL.PostedDate <= @ToDate
					AND ( GL.Account LIKE @AccountNumberPercent )
					AND ( GL.BankAccountDetailID = @BankAccountID
						  OR @BankAccountID IS NULL
						)
						AND GL.CurrencyID = @CurrencyID
            
			GROUP BY BA.ID, 
				BA.BankAccount ,
				B.BankName,BA.BankBranchName
			HAVING
				SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal- GL.CreditAmountOriginal ELSE 0.0 END)<>0 OR 
				SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.DebitAmountOriginal ELSE 0.0 END)<>0 OR  
				SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmountOriginal ELSE 0.0 END)<>0 OR 
				SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal)<>0 
	END
	/*end add by cuongpv*/
            
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO



ALTER PROCEDURE [dbo].[Proc_BA_GetBookDepositListDetail]
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @CurrencyID NVARCHAR(3) ,
    @AccountNumber NVARCHAR(25) ,
    @BankAccountID UNIQUEIDENTIFIER ,
    @IsSimilarSum BIT = 0 ,
    @IsSoftOrderVoucher BIT = 0
AS
    BEGIN
        SET NOCOUNT ON
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        CREATE TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
              BankAccount NVARCHAR(500)  ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25)  ,
              RefType INT ,
              JournalMemo NVARCHAR(512)  ,
              CorrespondingAccountNumber NVARCHAR(20)
                 ,
              ExchangeRate DECIMAL ,
              DebitAmountOC DECIMAL(18,4) ,
              DebitAmount DECIMAL ,
              CreditAmountOC DECIMAL(18,4) ,
              CreditAmount DECIMAL ,
              ClosingAmountOC DECIMAL(18,4) ,
              ClosingAmount DECIMAL ,
              AccountObjectCode NVARCHAR(255)
                 ,
              AccountObjectName NVARCHAR(512)
                 ,
              EmployeeCode NVARCHAR(255)  ,
              EmployeeName NVARCHAR(255)  ,
              ProjectWorkCode NVARCHAR(20)
                 ,
              ProjectWorkName NVARCHAR(128)
                 ,
              

              ExpenseItemCode NVARCHAR(20)
                 ,
              ExpenseItemName NVARCHAR(128)
                 ,
              ListItemCode NVARCHAR(20)  ,
              ListItemName NVARCHAR(128)  ,
              ContractCode NVARCHAR(50)  ,

              BranchID UNIQUEIDENTIFIER ,
              BranchCode NVARCHAR(25)  ,
              BranchName NVARCHAR(255)  ,
              IsBold BIT ,
              PaymentType INT ,
              OrderType INT ,
              RefOrder INT,
			  OrderPriority INT
            )	
		
        CREATE TABLE #Result1
            (
              RowNum INT PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
              BankAccount NVARCHAR(500)  ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(512),
              CorrespondingAccountNumber NVARCHAR(20) ,
              ExchangeRate DECIMAL ,
              DebitAmountOC DECIMAL(18,4),
              DebitAmount DECIMAL ,
              CreditAmountOC DECIMAL(18,4) ,
              CreditAmount DECIMAL ,
              ClosingAmountOC DECIMAL(18,4) ,
              ClosingAmount DECIMAL ,
              AccountObjectCode NVARCHAR(255)
                 ,
              AccountObjectName NVARCHAR(512)
                 ,
              EmployeeCode NVARCHAR(255)  ,
              EmployeeName NVARCHAR(255)  ,

              ProjectWorkCode NVARCHAR(20) ,
              ProjectWorkName NVARCHAR(128) ,
              

              ExpenseItemCode NVARCHAR(20) ,
              ExpenseItemName NVARCHAR(128) ,
              ListItemCode NVARCHAR(20) ,
              ListItemName NVARCHAR(128)  ,
              ContractCode NVARCHAR(50) ,

              BranchID UNIQUEIDENTIFIER ,
              BranchCode NVARCHAR(25)  ,
              BranchName NVARCHAR(255)  ,
              IsBold BIT ,
              PaymentType INT ,
              OrderType INT ,
              RefOrder INT,
			  OrderPriority INT
            )	
		
        DECLARE @BankAccountAll UNIQUEIDENTIFIER/*tất cả tài khoản ngân hàng*/
        SET @BankAccountAll = 'A0624CFA-D105-422f-BF20-11F246704DC3'
		
        DECLARE @AccountNumberPercent NVARCHAR(25)
        SET @AccountNumberPercent = @AccountNumber + '%'
    
        DECLARE @ClosingAmountOC DECIMAL(29, 4)
        DECLARE @ClosingAmount DECIMAL(29, 4)
   	    DECLARE @tblListBankAccountDetail TABLE
            (
			  ID UNIQUEIDENTIFIER,
              BankAccount NVARCHAR(Max) ,
              BankAccountName NVARCHAR(MAX) 
            ) 
        if(@BankAccountID is null)     
        INSERT  INTO @tblListBankAccountDetail
                SELECT  BAD.ID,
				        BAD.BankAccount,
				        BAD.BankName
                FROM    BankAccountDetail BAD,
				        Bank BA
                WHERE BAD.BankID = BA.ID    
		else 	
			INSERT  INTO @tblListBankAccountDetail
                SELECT  BAD.ID,
				        BAD.BankAccount,
				        BAD.BankName
                FROM    BankAccountDetail BAD,
				        Bank BA
                WHERE BAD.BankID = BA.ID  
				and BAD.ID =  @BankAccountID 
	

        IF @ClosingAmount IS NULL
            SET @ClosingAmount = 0
        IF @ClosingAmountOC IS NULL
            SET @ClosingAmountOC = 0
            DECLARE @BankAccount NVARCHAR(500)
        DECLARE @CloseAmountOC MONEY
        DECLARE @CloseAmount MONEY
	  IF(@CurrencyID = 'VND')
	  BEGIN
	   INSERT  #Result
                ( BankAccount ,
                  JournalMemo ,
                  DebitAmount ,
                  DebitAmountOC ,
                  CreditAmount ,
                  CreditAmountOC ,
                  ClosingAmountOC ,
                  ClosingAmount ,
                  IsBold ,
                  OrderType   
                )
                SELECT  
		                BA.BankAccount + ' - ' + BA.BankAccountName,
                        N'Số dư đầu kỳ' AS JournalMemo ,
                        $0 AS DebitAmountOC ,
                        $0 AS DebitAmount ,
                        $0 AS CreditAmountOC ,
                        $0 AS CreditAmount ,
                        ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) ,
                        ISNULL(SUM(GL.DebitAmount - GL.CreditAmount), 0) ,
                        1 ,
                        0
                FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
				        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID
                WHERE   ( GL.PostedDate < @FromDate )
                        AND GL.Account LIKE @AccountNumberPercent
                GROUP BY BA.BankAccount , BA.BankAccountName
                HAVING  ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) <> 0
                        OR ISNULL(SUM(GL.DebitAmount - GL.CreditAmount), 0) <> 0


       INSERT  #Result
	   (
              RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
			  RefType,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder,
			  OrderPriority )
                    SELECT  GL.ReferenceID ,
	                        BA.BankAccount + ' - ' + BA.BankAccountName,
                            GL.PostedDate ,
                            GL.RefDate ,
                            GL.RefNo AS RefNoFinance ,
							GL.TypeID,
                            GL.Description,
                            GL.AccountCorresponding ,
                            GL.ExchangeRate ,
                            ISNULL(GL.DebitAmountOriginal, 0) AS DebitAmountOC ,
                            ISNULL(GL.DebitAmount, 0) AS DebitAmount ,
                            ISNULL(GL.CreditAmountOriginal, 0) AS CreditAmountOC ,
                            ISNULL(GL.CreditAmount, 0) AS CreditAmount ,
                            $0 AS ClosingAmountOC ,
                            $0 AS ClosingAmount ,
                            AO.AccountingObjectCode ,
                            AO.AccountingObjectName AS AccountObjectName ,
                            EI.ExpenseItemCode ,
                            EI.ExpenseItemName ,
                            GL.BranchID ,
                            0 ,
                            CASE WHEN GL.DebitAmount <> 0 THEN 0
                                 ELSE 1
                            END AS PaymentType ,
                            1 ,
                            0,
							null
                    FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
					        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID


                            LEFT JOIN dbo.AccountingObject AO ON GL.AccountingObjectID = AO.ID
                            LEFT JOIN dbo.ExpenseItem EI ON EI.ID = GL.ExpenseItemID
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND GL.Account LIKE @AccountNumberPercent
                           
                            AND ( GL.DebitAmountOriginal <> 0
                                  OR GL.CreditAmountOriginal <> 0
                                  OR GL.DebitAmount <> 0
                                  OR GL.CreditAmount <> 0
                                )
                    ORDER BY BA.BankAccount ,
                            PostedDate ,OrderPriority,
                            RefDate ,
                            PaymentType ,
                            RefNo
                    
  
        SET @BankAccount = NULL
        INSERT  INTO #Result1
		(     RowNum,
		      RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
			  RefType,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder,
			  OrderPriority)
                SELECT  ROW_NUMBER() OVER ( ORDER BY BankAccount , OrderType, PostedDate , CASE
                                                              WHEN @IsSoftOrderVoucher = 1
                                                              THEN RefOrder
                                                              ELSE 0
                                                              END, RefDate , PaymentType , RefNo ) ,
                        RefID ,
                        BankAccount ,
                        PostedDate ,
                        RefDate ,
                        RefNo ,
						RefType,
                        JournalMemo ,
                        CorrespondingAccountNumber ,
                        ExchangeRate ,
                        DebitAmountOC ,
                        DebitAmount ,
                        CreditAmountOC ,
                        CreditAmount ,
                        ClosingAmountOC ,
                        ClosingAmount ,
                        AccountObjectCode ,
                        AccountObjectName ,

                        ExpenseItemCode ,
                        ExpenseItemName ,
                        BranchID ,
						IsBold  ,
					    PaymentType  ,
						OrderType  ,
						RefOrder,
						OrderPriority
                FROM    #Result
                ORDER BY BankAccount ,
                        OrderType ,
                        PostedDate ,OrderPriority,
                        CASE WHEN @IsSoftOrderVoucher = 1 THEN RefOrder
                             ELSE 0
                        END ,
                        RefDate ,
                        PaymentType ,
                        RefNo
	  END
	  ELSE
	  BEGIN
        INSERT  #Result
                ( BankAccount ,
                  JournalMemo ,
                  DebitAmount ,
                  DebitAmountOC ,
                  CreditAmount ,
                  CreditAmountOC ,
                  ClosingAmountOC ,
                  ClosingAmount ,
                  IsBold ,
                  OrderType   
                )
                SELECT  
		                BA.BankAccount + ' - ' + BA.BankAccountName,
                        N'Số dư đầu kỳ' AS JournalMemo ,
                        $0 AS DebitAmountOC ,
                        $0 AS DebitAmount ,
                        $0 AS CreditAmountOC ,
                        $0 AS CreditAmount ,
                        ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) ,
                        ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) ,
                        1 ,
                        0
                FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
				        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID
                WHERE   ( GL.PostedDate < @FromDate )
                        AND GL.Account LIKE @AccountNumberPercent
                        AND ( @CurrencyID IS NULL
                              OR GL.CurrencyID = @CurrencyID
                            )
                GROUP BY BA.BankAccount , BA.BankAccountName
                HAVING  ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) <> 0
                        OR ISNULL(SUM(GL.DebitAmount - GL.CreditAmount), 0) <> 0

       INSERT  #Result
	   (
              RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
			  RefType,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder,
			  OrderPriority )
                    SELECT  GL.ReferenceID ,
	                        BA.BankAccount + ' - ' + BA.BankAccountName,
                            GL.PostedDate ,
                            GL.RefDate ,
                            GL.RefNo AS RefNoFinance ,
							GL.TypeID,
                            GL.Description,
                            GL.AccountCorresponding ,
                            GL.ExchangeRate ,
                            ISNULL(GL.DebitAmountOriginal, 0) AS DebitAmountOC ,
                            ISNULL(GL.DebitAmountOriginal, 0) AS DebitAmount ,
                            ISNULL(GL.CreditAmountOriginal, 0) AS CreditAmountOC ,
                            ISNULL(GL.CreditAmountOriginal, 0) AS CreditAmount ,
                            $0 AS ClosingAmountOC ,
                            $0 AS ClosingAmount ,
                            AO.AccountingObjectCode ,
                            AO.AccountingObjectName AS AccountObjectName ,
                            EI.ExpenseItemCode ,
                            EI.ExpenseItemName ,
                            GL.BranchID ,
                            0 ,
                            CASE WHEN GL.DebitAmount <> 0 THEN 0
                                 ELSE 1
                            END AS PaymentType ,
                            1 ,
                            0,
							null
                    FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
					        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID


                            LEFT JOIN dbo.AccountingObject AO ON GL.AccountingObjectID = AO.ID
                            LEFT JOIN dbo.ExpenseItem EI ON EI.ID = GL.ExpenseItemID
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND GL.Account LIKE @AccountNumberPercent
                            AND ( @CurrencyID IS NULL
                                  OR GL.CurrencyID = @CurrencyID
                                )
                            AND ( GL.DebitAmountOriginal <> 0
                                  OR GL.CreditAmountOriginal <> 0
                                  OR GL.DebitAmount <> 0
                                  OR GL.CreditAmount <> 0
                                )
                    ORDER BY BA.BankAccount ,
                            PostedDate ,OrderPriority,
                            RefDate ,
                            PaymentType ,
                            RefNo
                  
        SET @BankAccount = NULL
        INSERT  INTO #Result1
		(     RowNum,
		      RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
			  RefType,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder,
			  OrderPriority)
                SELECT  ROW_NUMBER() OVER ( ORDER BY BankAccount , OrderType, PostedDate , CASE
                                                              WHEN @IsSoftOrderVoucher = 1
                                                              THEN RefOrder
                                                              ELSE 0
                                                              END, RefDate , PaymentType , RefNo ) ,
                        RefID ,
                        BankAccount ,
                        PostedDate ,
                        RefDate ,
                        RefNo ,
						RefType,
                        JournalMemo ,
                        CorrespondingAccountNumber ,
                        ExchangeRate ,
                        DebitAmountOC ,
                        DebitAmount ,
                        CreditAmountOC ,
                        CreditAmount ,
                        ClosingAmountOC ,
                        ClosingAmount ,
                        AccountObjectCode ,
                        AccountObjectName ,

                        ExpenseItemCode ,
                        ExpenseItemName ,
                        BranchID ,
						IsBold  ,
					    PaymentType  ,
						OrderType  ,
						RefOrder,
						OrderPriority
                FROM    #Result
                ORDER BY BankAccount ,
                        OrderType ,
                        PostedDate ,OrderPriority,
                        CASE WHEN @IsSoftOrderVoucher = 1 THEN RefOrder
                             ELSE 0
                        END ,
                        RefDate ,
                        PaymentType ,
                        RefNo
        END
	  
        SET @CloseAmount = 0
        SET @CloseAmountOC = 0
		
        UPDATE  #Result1
        SET

                @CloseAmount = ( CASE WHEN OrderType = 0
                                      THEN ( CASE WHEN ClosingAmount = 0
                                                  THEN 0
                                                  ELSE ClosingAmount
                                             END )
                                      WHEN @BankAccount <> BankAccount
                                      THEN DebitAmount - CreditAmount
                                      ELSE @CloseAmount + DebitAmount
                                           - CreditAmount
                                 END ) ,
                @CloseAmountOC = ( CASE WHEN OrderType = 0
                                        THEN ( CASE WHEN ClosingAmountOC = 0
                                                    THEN 0
                                                    ELSE ClosingAmountOC
                                               END )
                                        WHEN @BankAccount <> BankAccount
                                        THEN DebitAmountOC - CreditAmountOC
                                        ELSE @CloseAmountOC + DebitAmountOC
                                             - CreditAmountOC
                                   END ) ,
                ClosingAmount = @CloseAmount ,
                ClosingAmountOC = @CloseAmountOC ,
                @BankAccount = BankAccount

        SELECT  RefID,RefType,BankAccount,PostedDate,RefDate,RefNo,JournalMemo,CorrespondingAccountNumber,DebitAmountOC,DebitAmount,CreditAmountOC,CreditAmount,ClosingAmountOC,ClosingAmount,AccountObjectCode,AccountObjectName,2 as ordercode 
		into #tg1
        FROM    #Result1
        ORDER BY BankAccount ,
                OrderType ,
                PostedDate ,OrderPriority,
                CASE WHEN @IsSoftOrderVoucher = 1 THEN RefOrder
                     ELSE 0
                END ,
                RefDate ,
                PaymentType ,
                RefNo
			INSERT INTO #tg1 (RefID,RefType,BankAccount,PostedDate,RefDate,RefNo,JournalMemo,CorrespondingAccountNumber,DebitAmountOC,DebitAmount,CreditAmountOC,CreditAmount,ClosingAmountOC,ClosingAmount,AccountObjectCode,AccountObjectName,ordercode)  
			SELECT  null,null,BankAccount,null,null,null,N'Tài khoản ngân hàng: ' + BankAccount,null,null,null,null,null,null,null,null,null,1
			FROM #Result1
			group by BankAccount
				
			SELECT TOP 0 *
			into #tg2
			FROM #tg1

			INSERT INTO #tg2 (RefID,RefType,BankAccount,PostedDate,RefDate,RefNo,JournalMemo,CorrespondingAccountNumber,DebitAmountOC,DebitAmount,CreditAmountOC,CreditAmount,ClosingAmountOC,ClosingAmount,AccountObjectCode,AccountObjectName,ordercode)  
			SELECT  null,null,BankAccount,null,null,null,N'Cộng nhóm',null,Sum(DebitAmountOC),Sum(DebitAmount),Sum(CreditAmountOC),Sum(CreditAmount),0,0,null,null,3
			FROM #Result1
			group by BankAccount
			
			
			SELECT  BankAccount into #bankAccount from #tg1

			DECLARE @BankAcc nvarchar(512)
			DECLARE @COUNT INT
			DECLARE @SODUDAUKY Decimal(25,0)
			DECLARE @SODUDAUKYOC Decimal(25,4)
			SET @COUNT = (SELECT COUNT(*) From #bankAccount)
			WHILE(@COUNT > 0)
			BEGIN
			SET @BankAcc = (SELECT TOP 1 BankAccount FROM #bankAccount)
			SET @SODUDAUKY = ISNULL((select Sum(ClosingAmount) from #tg1 where JournalMemo like N'%Số dư đầu kỳ%' AND  BankAccount = @BankAcc ),0)
			SET @SODUDAUKYOC = ISNULL((select Sum(ClosingAmountOC) from #tg1 where JournalMemo like N'%Số dư đầu kỳ%' AND  BankAccount = @BankAcc ),0)
			UPDATE #tg2
			SET ClosingAmount  =  @SODUDAUKY
			+ (SELECT SUM(DebitAmount) FROM #tg1 WHERE BankAccount = @BankAcc)
			- (SELECT SUM(CreditAmount) FROM #tg1 WHERE BankAccount = @BankAcc),
 
			ClosingAmountOC  =  @SODUDAUKYOC
			+ (SELECT SUM(DebitAmountOC) FROM #tg1 WHERE BankAccount = @BankAcc)
			- (SELECT SUM(CreditAmountOC) FROM #tg1 WHERE BankAccount = @BankAcc) 

			WHERE BankAccount = @BankAcc
			SET @COUNT = @COUNT -1;
			DELETE #bankAccount where BankAccount = @BankAcc
			END
		
		INSERT INTO #tg1 SELECT * from #tg2

			
			SELECT * from #tg1 order by BankAccount,ordercode
        DROP TABLE #Result
        DROP TABLE #Result1
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER FUNCTION [dbo].[Func_GetB03_DN]
(
	@BranchID UNIQUEIDENTIFIER
	,@IncludeDependentBranch BIT
	,@FromDate DATETIME
	,@ToDate DATETIME
	,@PrevFromDate DATETIME
	,@PrevToDate DATETIME	
)
RETURNS 
@Result TABLE 
(	
		ItemID UNIQUEIDENTIFIER
      ,ItemCode NVARCHAR(25)
      ,ItemName NVARCHAR(512)
      ,ItemNameEnglish NVARCHAR(512)
      ,ItemIndex INT
      ,Description NVARCHAR(512)
      ,FormulaType INT
      ,FormulaFrontEnd NVARCHAR(MAX)
      ,Formula XML
      ,Hidden BIT
      ,IsBold BIT
      ,IsItalic BIT
      ,Amount DECIMAL(25,4)
      ,PrevAmount  DECIMAL(25,4)    
)
AS
BEGIN

	DECLARE @AccountingSystem INT	
    SET @AccountingSystem = 48
    
    set @PrevToDate = DATEADD(DD,-1,@FromDate)
	/*add by cuongpv*/
	set @PrevToDate = DATEADD(hh,23,@PrevToDate)
	set @PrevToDate = DATEADD(mi,59,@PrevToDate)
	set @PrevToDate = DATEADD(ss,59,@PrevToDate)
	/*end add by cuongpv*/
	declare @CalPrevDate nvarchar(20)
	declare @CalPrevMonth nvarchar(20)
	declare @CalPrevDay nvarchar(20)
	set @CalPrevDate = CAST(day(@FromDate) AS varchar) + CAST(month(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevMonth = CAST(month(@FromDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevDay = CAST(day(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar)
	if @CalPrevDate = '113112'
	 set @PrevFromDate = convert(DATETIME,dbo.ctod('D',dbo.godtos('M', -12, @FromDate)),103)
	else if (@CalPrevMonth in (55,77,1010,1212) and @CalPrevDay in (131) )
	 set @PrevFromDate = DATEADD(DD,-30,@FromDate)
	else if (@CalPrevMonth in (11,88,44,66,99,1111) and @CalPrevDay in (131,130) )/*edit by cuongpv: @CalPrevMonth in (11,22,88,44,66,99,1111) -> @CalPrevMonth in (11,88,44,66,99,1111)*/
	 set @PrevFromDate = DATEADD(DD,-31,@FromDate)
	else if (@CalPrevMonth in (22) and @CalPrevDay in (128, 129)) /*add by cuongpv*/
	 set @PrevFromDate = DATEADD(DD,-31,@FromDate) /*add by cuongpv*/
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 = 0)
	 set @PrevFromDate = DATEADD(DD,-29,@FromDate)
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 <> 0)
	 set @PrevFromDate = DATEADD(DD,-28,@FromDate)
	else if ( (@CalPrevMonth in (13,1012) and @CalPrevDay = '131') or (@CalPrevMonth in (46,79) and @CalPrevDay = '130') )
	 set @PrevFromDate = DATEADD(QQ,-1,@FromDate)
	else set @PrevFromDate = DATEADD(DD,-1-DATEDIFF(day, @FromDate, @ToDate),@FromDate)

	DECLARE @ReportID NVARCHAR(100)
	SET @ReportID = '3'
	
	DECLARE @ItemBeginingCash UNIQUEIDENTIFIER	

	SET @ItemBeginingCash = (Select ItemID From FRTemplate Where ItemCode = '60' AND ReportID = 3 AND AccountingSystem = @AccountingSystem)

	DECLARE @tblItem TABLE
	(
		ItemID				UNIQUEIDENTIFIER
		,ItemIndex			INT
		,ItemName			NVARCHAR(512)	
		,OperationSign		INT
		,OperandString		NVARCHAR(512)	
		,AccountNumber		NVARCHAR(25)
		,AccountNumberPercent NVARCHAR(25) 
		,CorrespondingAccountNumber	VARCHAR(25)
	)
	
	INSERT @tblItem
	SELECT ItemID 
		, ItemIndex
		, ItemName
		, x.r.value('@OperationSign','INT')
		, x.r.value('@OperandString','nvarchar(512)')
		, x.r.value('@AccountNumber','nvarchar(25)') 
		, x.r.value('@AccountNumber','nvarchar(25)') + '%'
		, CASE WHEN x.r.value('@CorrespondingAccountNumber','nvarchar(25)') <>'' 
			THEN x.r.value('@CorrespondingAccountNumber','nvarchar(25)') + '%'
			ELSE ''
		 END
	FROM dbo.FRTemplate
	CROSS APPLY Formula.nodes('/root/DetailFormula') AS x(r)
	WHERE AccountingSystem = @AccountingSystem
		  AND ReportID = @ReportID
		  AND FormulaType = 0 AND Formula IS NOT NULL 
	
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/
		  
	DECLARE @Balance TABLE 
		(
			AccountNumber NVARCHAR(25)	
			,CorrespondingAccountNumber NVARCHAR(25)		
			,PrevBusinessAmount DECIMAL(25,4)
			,BusinessAmount DECIMAL(25,4)							
			,PrevInvestmentAmount DECIMAL(25,4)
			,InvestmentAmount DECIMAL(25,4)
			,PrevFinancialAmount DECIMAL(25,4)
			,FinancialAmount DECIMAL(25,4)
		)			
		
	INSERT INTO @Balance			
	SELECT 
		GL.Account
		,GL.AccountCorresponding		
		,SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND(AC.ActivityID IS NULL OR AC.ActivityID = 0) 
				THEN DebitAmount  ELSE 0 END) AS PrevBusinessAmount
		,SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND (AC.ActivityID IS NULL OR AC.ActivityID = 0) 
				THEN DebitAmount ELSE 0 END) AS BusinessAmount		
		,SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND  AC.ActivityID = 1 
				THEN DebitAmount  ELSE 0 END) AS PrevInvestmentAmount
		,SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 1 
				THEN DebitAmount ELSE 0 END) AS InvestmentAmount		
		,SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND AC.ActivityID = 2 
				THEN DebitAmount  ELSE 0 END) AS PrevFinancialAmount
		,SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 2 
				THEN DebitAmount ELSE 0 END) AS FinancialAmount		
	FROM @tbDataGL GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
		INNER JOIN dbo.Account A ON GL.Account = A.AccountNumber
		LEFT JOIN [dbo].[FRB03ReportDetailActivity] AC   ON GL.DetailID = Ac.RefDetailID
													/*Khi JOIN thêm điều kiện sổ*/
	WHERE PostedDate BETWEEN @PrevFromDate AND @ToDate	
	and GL.No <> 'OPN'
	GROUP BY
		GL.Account
		,GL.AccountCorresponding
	HAVING
		SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND(AC.ActivityID IS NULL OR AC.ActivityID = 0) 
			THEN DebitAmount  ELSE 0 END)<>0
		OR SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND (AC.ActivityID IS NULL OR AC.ActivityID = 0) 
			THEN DebitAmount ELSE 0 END) <>0
		OR SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND  AC.ActivityID = 1 
			THEN DebitAmount  ELSE 0 END) <>0
		OR SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 1 
			THEN DebitAmount ELSE 0 END)<>0
		OR SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND AC.ActivityID = 2 
			THEN DebitAmount  ELSE 0 END) <>0
		OR SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 2 
			THEN DebitAmount ELSE 0 END) <>0
	
	DECLARE @DebitBalance TABLE
	(	
		ItemID				UNIQUEIDENTIFIER
		,OperationSign		INT
		,AccountNumber NVARCHAR(25)
		,AccountKind	INT
		,PrevDebitAmount Decimal(25,4)
		,DebitAmount Decimal(25,4)
	)
	
	INSERT @DebitBalance
	SELECT
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
		,SUM(CASE WHEN  GL.PostedDate < @PrevFromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0 END)	
		,SUM(GL.DebitAmount - GL.CreditAmount)		
	FROM  @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
	INNER JOIN @tblItem I ON GL.Account LIKE I.AccountNumberPercent	
	LEFT JOIN dbo.Account A ON A.AccountNumber = I.AccountNumber
	WHERE GL.PostedDate < @FromDate	
	    AND I.OperandString = 'DUNO'
	    AND I.ItemID = @ItemBeginingCash
	Group By
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
	
	UNION ALL	
	SELECT
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
		,SUM(CASE WHEN  GL.PostedDate <= @PrevToDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0 END)	
		,SUM(GL.DebitAmount - GL.CreditAmount)		
	FROM  @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
	INNER JOIN @tblItem I ON GL.Account LIKE I.AccountNumberPercent	
	LEFT JOIN dbo.Account A ON A.AccountNumber = I.AccountNumber
	WHERE GL.PostedDate <= @ToDate
	    AND I.OperandString = 'DUNO'
	    AND I.ItemID <> @ItemBeginingCash
	Group By
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
		
		   
	UPDATE @DebitBalance
	SET PrevDebitAmount = 0 WHERE AccountKind = 1 OR (AccountKind NOT IN (0,1)  AND PrevDebitAmount < 0) 
	
	UPDATE @DebitBalance
	SET DebitAmount = 0 WHERE AccountKind = 1 OR (AccountKind NOT IN (0,1)  AND DebitAmount < 0) 
	
	DECLARE @tblMasterDetail Table
		(
			ItemID UNIQUEIDENTIFIER
			,DetailItemID UNIQUEIDENTIFIER
			,OperationSign INT
			,Grade INT
			,PrevAmount DECIMAL(25,4)
			,Amount DECIMAL(25,4)		
		)	
	
	
	INSERT INTO @tblMasterDetail
	SELECT I.ItemID
			,NULL
			,1
			,-1
			,SUM(I.PrevAmount)
			,SUM(I.Amount)
	FROM	
		(SELECT			
			I.ItemID
			,SUM(CASE				
					WHEN I.OperandString = 'PhatsinhDU' THEN b.PrevBusinessAmount + b.PrevInvestmentAmount + b.PrevFinancialAmount
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_DAUTU' THEN b.PrevInvestmentAmount
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_TAICHINH' THEN b.PrevFinancialAmount
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_SXKD' THEN b.PrevBusinessAmount
				 END * I.OperationSign) AS  PrevAmount			
			,SUM(CASE 
					WHEN I.OperandString = 'PhatsinhDU' THEN (b.BusinessAmount + b.InvestmentAmount + b.FinancialAmount)
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_DAUTU' THEN (b.InvestmentAmount)
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_TAICHINH' THEN (b.FinancialAmount)
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_SXKD' THEN (b.BusinessAmount)
			    END * I.OperationSign) AS Amount
			FROM @tblItem I 			
				 INNER JOIN @Balance B 
					ON (B.AccountNumber like I.AccountNumberPercent)
					AND (B.CorrespondingAccountNumber like I.CorrespondingAccountNumber)	
				WHERE I.OperandString IN ('PhatsinhDU','PhatsinhDUChiTietTheoHD_DAUTU','PhatsinhDUChiTietTheoHD_TAICHINH','PhatsinhDUChiTietTheoHD_SXKD')					
			GROUP BY I.ItemID
		UNION ALL
			SELECT
		    I.ItemID
			,SUM((b.PrevBusinessAmount + b.PrevInvestmentAmount + b.PrevFinancialAmount)* I.OperationSign) AS  PrevAmount			
			,SUM((b.BusinessAmount + b.InvestmentAmount + b.FinancialAmount)* I.OperationSign) AS Amount
			FROM @tblItem I 
				 INNER JOIN @Balance B 
					ON (B.AccountNumber like I.AccountNumberPercent)
			WHERE I.OperandString = 'PhatsinhNO'				
			GROUP BY I.ItemID
		UNION ALL
			SELECT
		    I.ItemID
			,SUM((b.PrevBusinessAmount + b.PrevInvestmentAmount + b.PrevFinancialAmount)* I.OperationSign) AS  PrevAmount			
			,SUM((b.BusinessAmount + b.InvestmentAmount + b.FinancialAmount)* I.OperationSign) AS Amount
			FROM @tblItem I 
				 INNER JOIN @Balance B 
					ON (B.CorrespondingAccountNumber like I.AccountNumberPercent)
			WHERE I.OperandString = 'PhatsinhCO'		
					
			GROUP BY I.ItemID
			
		UNION ALL
			SELECT I.ItemID
				,I.PrevDebitAmount * I.OperationSign
				,I.DebitAmount * I.OperationSign
			FROM @DebitBalance I
		 ) AS I
	Group By I.ITemID
		
	
		
	
	       
	    
	INSERT @tblMasterDetail	
	SELECT ItemID 
	, x.r.value('@ItemID','NVARCHAR(100)')
	, x.r.value('@OperationSign','INT')	
	, 0
	, 0.0	
	, 0.0
	FROM dbo.FRTemplate 
	CROSS APPLY Formula.nodes('/root/MasterFormula') AS x(r)
	WHERE AccountingSystem = @AccountingSystem
		  AND ReportID = @ReportID
		  AND FormulaType = 1 AND Formula IS NOT NULL 
	
	;
	WITH V(ItemID,DetailItemID,PrevAmount,Amount,OperationSign)
		AS 
		(
			SELECT ItemID,DetailItemID,PrevAmount, Amount, OperationSign
			FROM @tblMasterDetail WHERE Grade = -1 
			UNION ALL
			SELECT B.ItemID
				, B.DetailItemID				
				, V.PrevAmount
				, V.Amount
				, B.OperationSign * V.OperationSign AS OperationSign
			FROM @tblMasterDetail B, V 
			WHERE B.DetailItemID = V.ItemID	
		)
		
	INSERT @Result      
	SELECT 
		FR.ItemID
		, FR.ItemCode
		, FR.ItemName
		, FR.ItemNameEnglish
		, FR.ItemIndex
		, FR.Description		
		, FR.FormulaType
		, CASE WHEN FR.FormulaType = 1 THEN FR.FormulaFrontEnd	ELSE ''	END AS FormulaFrontEnd
		, CASE WHEN FR.FormulaType = 1 THEN FR.Formula ELSE NULL END AS Formula
		, FR.Hidden
		, FR.IsBold
		, FR.IsItalic
		,CASE WHEN FR.Formula IS NOT NULL THEN ISNULL(X.Amount,0)
			ELSE X.Amount
		 END AS Amount
		,CASE WHEN FR.Formula IS NOT NULL THEN ISNULL(X.PrevAmount,0) 
			ELSE X.PrevAmount 
		END AS PrevAmount
		FROM		
		(
			SELECT V.ItemID
				,SUM(V.OperationSign * V.Amount)  AS Amount
				,SUM(V.OperationSign * V.PrevAmount) AS PrevAmount
			FROM V
			GROUP BY ItemID
		) AS X		
		RIGHT JOIN dbo.FRTemplate FR ON FR.ItemID = X.ItemID	
	WHERE AccountingSystem = @AccountingSystem
		  AND ReportID = @ReportID
		  and Hidden = 0
	Order by ItemIndex	
	RETURN 
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE PROCEDURE [dbo].[Proc_GL_GetBookDetailPaymentByAccountNumber1]
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(MAX) ,
	/*@CurrencyId NVARCHAR(10) , comment by cuongpv*/
    @GroupTheSameItem BIT
AS
    BEGIN

		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1) PRIMARY KEY,
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              InvDate DATETIME ,
              InvNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(512) ,
              AccountNumber NVARCHAR(25) ,
              CorrespondingAccountNumber NVARCHAR(25) ,
              DebitAmount DECIMAL(22,4) ,
              CreditAmount DECIMAL(22,4) ,
              ClosingDebitAmount DECIMAL(22,4) ,
              ClosingCreditAmount DECIMAL(22,4) ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(512) ,
              OrderType INT , 
              IsBold BIT ,
              BranchName NVARCHAR(512),
			  AccountGroupKind NVARCHAR(25) ,
			  OrderPriority int

            )      
       
        
        DECLARE @tblAccountNumber TABLE
            (              
			  AccountNumber  NVARCHAR(25) ,  
              AccountNumberPercent NVARCHAR(25)   , 
			  AccountGroupKind NVARCHAR(25) 
            )
        INSERT  @tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountNumber + '%',
						A1.AccountGroupKind
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        IF @GroupTheSameItem = 1
            BEGIN
                INSERT  #Result					
				SELECT
					RefID ,
                    PostedDate ,
                    RefDate ,
                    RefNo ,
                    InvDate ,
                    InvNo ,
                    RefType ,
                    JournalMemo ,
                    AccountNumber,
                    CorrespondingAccountNumber ,
                    ISNULL(DebitAmount,0) ,
                    ISNULL(CreditAmount,0) ,
                    ISNULL(ClosingDebitAmount,0),
                    ISNULL(ClosingCreditAmount,0),
                    AccountingObjectCode,
                    AccountObjectName,
                    OrderType ,
                    IsBold ,
                    BranchName,
					AccountGroupKind,
					OrderPriority	

                 FROM
					(SELECT  
								NULL as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư đầu kỳ' as JournalMemo ,
                                A.AccountNumber,
                                NULL as CorrespondingAccountNumber ,
								$0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
									 THEN SUM(GL.DebitAmount - GL.CreditAmount)
									 ELSE 0
								END AS ClosingDebitAmount ,
								CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
									 THEN SUM(GL.CreditAmount - GL.DebitAmount)
									 ELSE 0
								END AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                0 AS OrderType ,
                                1 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
								,A.AccountGroupKind,
								0 as OrderPriority /*edit by cuongpv: GL.OrderPriority -> 0 as OrderPriority*/

                            FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
							INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
							WHERE   ( GL.PostedDate < @FromDate )  
							/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
							GROUP BY 
								A.AccountNumber,A.AccountGroupKind /*comment by cuongpv: ,GL.OrderPriority*/
							HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv Description-> Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                SUM(GL.DebitAmount) AS DebitAmount ,
                                null AS CreditAmount ,/*edit by cuongpv SUM(GL.CreditAmount) -> null*/
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 As isBold,
                                '' as BranchName
								,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
								,A.AccountGroupKind,
								GL.OrderPriority
                        FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )    
						/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
						and GL.AccountCorresponding <> ''      
                        GROUP BY GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv GL.Description -> GL.Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding,
								A.AccountGroupKind,
								GL.OrderPriority
                        HAVING  SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0
						/*add by cuongpv de sua cong gop but toan*/
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv Description-> Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                null AS DebitAmount ,
                                SUM(GL.CreditAmount) AS CreditAmount ,
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 As isBold,
                                '' as BranchName
								,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
								,A.AccountGroupKind,
								GL.OrderPriority
                        FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )    
						/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
						and GL.AccountCorresponding <> ''      
                        GROUP BY GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv GL.Description -> GL.Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding,
								A.AccountGroupKind,
								GL.OrderPriority
                        HAVING  SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0 
						/*end add by cuongpv*/                       
                      )T
                      ORDER BY 
						AccountNumber,
                        OrderType ,
                        postedDate ,
						OrderPriority,
                        RefDate ,
                        RefNo      
                        ,SortOrder,DetailPostOrder,EntryType            
            END
            
        ELSE
            BEGIN             
                
                INSERT  #Result					
				SELECT
					RefID ,
                    PostedDate ,
                    RefDate ,
                    RefNo ,
                    InvDate ,
                    InvNo ,
                    RefType ,
                    JournalMemo ,
                    AccountNumber,
                    CorrespondingAccountNumber ,
                    DebitAmount ,
                    CreditAmount ,
                    ClosingDebitAmount ,
                    ClosingCreditAmount ,
                    AccountingObjectCode,
                    AccountObjectName,
                    OrderType ,
                    IsBold ,
                    BranchName,
					AccountGroupKind,
					OrderPriority
                 FROM
					(SELECT  
								null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư đầu kỳ' as JournalMemo ,
                                A.AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
									 THEN SUM(GL.DebitAmount - GL.CreditAmount)
									 ELSE 0
								END AS ClosingDebitAmount ,
								CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
									 THEN SUM(GL.CreditAmount - GL.DebitAmount)
									 ELSE 0
								END AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                0 AS OrderType ,
                                1 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType
								,A.AccountGroupKind,
								0 as OrderPriority /*edit by cuongpv: GL.OrderPriority -> OrderPriority*/
                            FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
							INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
							WHERE   ( GL.PostedDate < @FromDate )   
							/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
							GROUP BY 
								A.AccountNumber,A.AccountGroupKind/*comment by cuongpv: ,GL.OrderPriority*/
							HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                CASE WHEN GL.DESCRIPTION IS NULL
                                          OR LTRIM(GL.Description) = ''
                                     THEN GL.Reason
                                     ELSE GL.Description
                                END AS JournalMemo ,
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                GL.DebitAmount ,
                                GL.CreditAmount ,
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 ,
                                null BranchName
								,null
								,null
								,null
								,A.AccountGroupKind,
								GL.OrderPriority
                        FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
                        WHERE   (GL.PostedDate BETWEEN @FromDate AND @ToDate )                              
                                AND ( GL.DebitAmount <> 0
                                      OR GL.CreditAmount <> 0
                                    )   	
									/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
									and GL.AccountCorresponding <> ''                     
                      )T
                      ORDER BY
						AccountNumber,
                        OrderType ,
                        postedDate ,
						OrderPriority,
                        RefDate ,
                        RefNo,SortOrder,DetailPostOrder,EntryType                                                          
               
            END         

			 

		DECLARE @AccountNumber1 NVARCHAR(25)

		DECLARE C1 CURSOR FOR  
		SELECT  F.Value
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
	    order by F.Value
		OPEN c1;  
		FETCH NEXT FROM c1 INTO @AccountNumber1  ;  
		WHILE @@FETCH_STATUS = 0  
		   BEGIN  
		      PRINT @AccountNumber1
			  DECLARE @AccountObjectCode NVARCHAR(25)
			 ,@ClosingDebitAmount DECIMAL(22,4)
			 
		      SELECT @ClosingDebitAmount = 0
			  UPDATE  #Result
			  SET     @ClosingDebitAmount = @ClosingDebitAmount + ISNULL(ClosingDebitAmount,0) - 
		                              ISNULL(ClosingCreditAmount,0) + ISNULL(DebitAmount,0) - ISNULL(CreditAmount,0),
		
                ClosingDebitAmount = CASE WHEN @ClosingDebitAmount > 0
                                          THEN @ClosingDebitAmount
                                          ELSE 0
                                     END ,
                ClosingCreditAmount = CASE WHEN @ClosingDebitAmount < 0
                                           THEN - @ClosingDebitAmount
                                           ELSE 0
                                      END 
               where AccountNumber = @AccountNumber1
			   FETCH NEXT FROM c1 INTO @AccountNumber1  ; 
			   
		   END;  
		CLOSE c1;  
		DEALLOCATE c1;  
		
        DECLARE @DebitAmount DECIMAL(22,4) 
		DECLARE @CreditAmount DECIMAL(22,4)  
		DECLARE @ClosingDeditAmount_SDDK DECIMAL(22,4)  
		DECLARE @ClosingCreditAmount_SDDK DECIMAL(22,4)  
	    select @DebitAmount = sum(DebitAmount),
		       @CreditAmount = sum(CreditAmount)
		from #Result Res
		INNER JOIN @tblAccountNumber A ON Res.AccountNumber LIKE A.AccountNumberPercent
		where OrderType = 1
		select @ClosingDeditAmount_SDDK = sum(ClosingDebitAmount),
		       @ClosingCreditAmount_SDDK = sum(ClosingCreditAmount)
		from #Result where OrderType = 0
		 INSERT INTO #Result 
	SELECT null,null,null,null,null,null,null,N'Cộng',AccountNumber,null,Sum(DebitAmount) as DebitAmount,Sum(CreditAmount) as CreditAmount,null,null,null,null,2,null,null,null,null
	FROM #Result
	GROUP BY AccountNumber
	 INSERT INTO #Result 
	SELECT null,null,null,null,null,null,null,N'Số dư cuối kỳ',AccountNumber,null,null,null,null,null,null,null,3,null,null,null,null
	FROM #Result
	GROUP BY AccountNumber
        SELECT  RowNum ,
                RefID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountNumber,
                PostedDate ,
                RefDate ,
                RefNo ,
                InvDate ,
                InvNo ,
                RefType ,
                JournalMemo ,
                CorrespondingAccountNumber ,
                DebitAmount ,
                CreditAmount ,
                ClosingDebitAmount ,
                ClosingCreditAmount ,               
                OrderType AS InvoiceOrder ,
                IsBold ,
                BranchName,
				AccountGroupKind
        FROM    #Result
		WHERE (DebitAmount > 0 OR CreditAmount > 0 AND OrderType = 1) OR OrderType = 0 or OrderType = 2 or OrderType = 3/*add by cuongpv*/
        ORDER BY AccountNumber,InvoiceOrder
		
		
		select 
		null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư cuối kỳ' as JournalMemo ,
                                AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 DebitAmount ,
								$0 CreditAmount ,
								case 
								when A1.AccountGroupKind=0 or (A1.AccountGroupKind=2 and (A1.AccountNumber like '1%' or A1.AccountNumber like '2%' or A1.AccountNumber like '6%' 
								or A1.AccountNumber like '8%')) then
								sum(ClosingDebitAmount) + sum(sumDebitAmount) - sum(sumCreditAmount) else $0 
								end as ClosingDebitAmount,
								case 
								when A1.AccountGroupKind=1 or (A1.AccountGroupKind=2 and (A1.AccountNumber like '3%' or A1.AccountNumber like '4%' or A1.AccountNumber like '5%' 
								or A1.AccountNumber like '7%')) then
								sum(ClosingCreditAmount) + sum(sumCreditAmount) - sum(sumDebitAmount) else $0 
								end AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                2 AS OrderType ,
                                2 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType,
								A1.AccountGroupKind
        from (SELECT  
								null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư cuối kỳ' as JournalMemo ,
                                A.AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN GL.OrderType = 0 and gl.IsBold = 1 THEN sum(ClosingDebitAmount)
									 ELSE 0 
								END
								AS ClosingDebitAmount,
								sum(DebitAmount) sumDebitAmount,
								sum(CreditAmount) sumCreditAmount,
								0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                2 AS OrderType ,
                                2 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType,
								A.AccountGroupKind
                            FROM    #Result AS GL
							INNER JOIN @tblAccountNumber A ON GL.AccountNumber = A.AccountNumber
							GROUP BY OrderType,IsBold,
								A.AccountNumber,A.AccountGroupKind) as A1
        group by AccountNumber,AccountGroupKind
	  
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_S02C1DNN]
    @BranchID UNIQUEIDENTIFIER,
    @IncludeDependentBranch BIT,
    @StartDate DATETIME,
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountNumber NVARCHAR(MAX),
    @IsSimilarSum BIT
AS
    BEGIN
        SET NOCOUNT ON
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)
		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		CREATE TABLE #Result
            (
             RefID UNIQUEIDENTIFIER,
             RefType INT,
             PostedDate DATETIME,
             RefNo NVARCHAR(25),
             RefDate DATETIME,
             InvDate DATETIME,
             InvNo NVARCHAR(MAX),
             JournalMemo NVARCHAR(512),
             AccountNumber NVARCHAR(25),
			 DetailAccountNumber NVARCHAR(25) ,
             AccountCategoryKind INT,
             AccountName NVARCHAR(512),
             CorrespondingAccountNumber NVARCHAR(25),
             DebitAmount MONEY,
             CreditAmount MONEY,
             OrderType INT,
             IsBold BIT,
             OrderNumber int
            )
        CREATE TABLE #tblAccountNumber
            (
             AccountNumber NVARCHAR(25)    PRIMARY KEY,
             AccountName NVARCHAR(512)  ,
             AccountNumberPercent NVARCHAR(25)  ,
             AccountCategoryKind INT,
			 IsParent BIT
            )
        INSERT  #tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountName,
                        A1.AccountNumber + '%',
                        A1.AccountGroupKind,
						A1.IsParentNode
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        IF @IsSimilarSum = 0 /*khong cong gop but ke toan*/
		Begin
            INSERT  #Result
                    (
                     RefID,
                     RefType,
                     PostedDate,
                     RefNo,
                     RefDate,
                     InvDate,
                     InvNo,
                     JournalMemo,
                     AccountNumber,
					 DetailAccountNumber,
                     AccountName,
                     CorrespondingAccountNumber,
                     AccountCategoryKind,
                     DebitAmount,
                     CreditAmount,
                     ORDERType,
                     IsBold,
                     OrderNumber
                    )
                    SELECT
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Description AS JournalMemo,
                            AC.AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind,
                            DebitAmount,
                            CreditAmount,
                            1 AS ORDERType,
                            0,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%')
											AND GL.DebitAmount <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND (GL.DebitAmount <> 0
                                 OR GL.CreditAmount <> 0
                                 or GL.DebitAmountOriginal <> 0
                                 OR GL.CreditAmountOriginal <> 0
                                )
              End
			  Else
			  Begin /*cong gop but toan thue*/
				INSERT  #Result(RefID, RefType, PostedDate, RefNo, RefDate, InvDate, InvNo, JournalMemo, AccountNumber, DetailAccountNumber, AccountName, CorrespondingAccountNumber,
                     AccountCategoryKind, DebitAmount, CreditAmount, ORDERType, IsBold, OrderNumber)
                SELECT RefID, RefType, PostedDate, RefNo, RefDate, InvDate, InvNo, JournalMemo, AccountNumber, DetailAccountNumber, AccountName, CorrespondingAccountNumber,
                     AccountCategoryKind, DebitAmount, CreditAmount, ORDERType, IsBold, OrderNumber
				FROM
				    (SELECT
                            GL.ReferenceID as RefID,
                            GL.TypeID as RefType,
                            GL.PostedDate as PostedDate,
                            RefNo,
                            GL.RefDate as RefDate,
                            Gl.InvoiceDate as InvDate,
                            GL.InvoiceNo as InvNo,
                            GL.Reason AS JournalMemo,
                            AC.AccountNumber as AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName as AccountName,
                            GL.AccountCorresponding as CorrespondingAccountNumber,
                            AC.AccountCategoryKind as AccountCategoryKind,
                            SUM(GL.DebitAmount) as DebitAmount,
                            0 as CreditAmount,
                            1 AS ORDERType,
                            0 as IsBold,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%')
											AND SUM(GL.DebitAmount) <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
					GROUP BY
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Reason,
                            AC.AccountNumber,
							GL.Account,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind
					HAVING SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0
				UNION ALL
				SELECT
                            GL.ReferenceID as RefID,
                            GL.TypeID as RefType,
                            GL.PostedDate as PostedDate,
                            RefNo,
                            GL.RefDate as RefDate,
                            Gl.InvoiceDate as InvDate,
                            GL.InvoiceNo as InvNo,
                            GL.Reason AS JournalMemo,
                            AC.AccountNumber as AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName as AccountName,
                            GL.AccountCorresponding as CorrespondingAccountNumber,
                            AC.AccountCategoryKind as AccountCategoryKind,
                            0 as DebitAmount,
                            SUM(GL.CreditAmount) as CreditAmount,
                            1 AS ORDERType,
                            0 as IsBold,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%')
											AND SUM(GL.CreditAmount) <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
					GROUP BY
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Reason,
                            AC.AccountNumber,
							GL.Account,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind
					HAVING SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0
				) T
				where T.DebitAmount > 0 OR T.CreditAmount>0
			  End
        SELECT  AccountNumber,
                AccountName,
				IsParent,
                SUM(OpenningDebitAmount) AS OpenningDebitAmount,
                SUM(OpenningCreditAmount) AS OpenningCreditAmount,
                SUM(ClosingDebitAmount) AS ClosingDebitAmount,
                SUM(ClosingCreditAmount) AS ClosingCreditAmount,
                SUM(AccumDebitAmount) AS AccumDebitAmount,
                SUM(AccumCreditAmount) AS AccumCreditAmount,
				AccountCategoryKind
        FROM    (
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS OpenningDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate < @FromDate
                 GROUP BY GL.BranchID,
                        AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS ClosingDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate <= @ToDate
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        SUM(GL.DebitAmount) AS AccumDebitAmount,
                        SUM(GL.CreditAmount) AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate BETWEEN @StartDate AND @ToDate
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                ) AS A
        GROUP BY AccountNumber,
                AccountName,
				IsParent ,
				AccountCategoryKind
        HAVING
				SUM(OpenningDebitAmount) <>0 OR
                SUM(OpenningCreditAmount)  <>0 OR
                SUM(ClosingDebitAmount)  <>0 OR
                SUM(ClosingCreditAmount) <>0 OR
                SUM(AccumDebitAmount) <>0 OR
                SUM(AccumCreditAmount) <>0
        ORDER BY AccountNumber
		SELECT  ROW_NUMBER() OVER (ORDER BY AccountNumber,DetailAccountNumber, OrderType, PostedDate, RefDate,OrderNumber, RefNo) AS RowNum,
				RefID,
				RefType,
				PostedDate,
				RefDate,
				RefNo,
				InvDate,
				InvNo,
				JournalMemo,
				AccountNumber,
				DetailAccountNumber,
				CorrespondingAccountNumber,
				DebitAmount,
				CreditAmount,
				IsBold,
				AccountCategoryKind
		FROM    #Result
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_FU_GetCashDetailBook]
    (
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @AccountNumber NVARCHAR(20) ,
      @BranchID UNIQUEIDENTIFIER ,
      @CurrencyID NVARCHAR(3)
    )
AS 
    BEGIN
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,4),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,4),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
			  AccountNumber NVARCHAR(15),
              PostedDate DATETIME ,
              RefDate DATETIME ,
              ReceiptRefNo NVARCHAR(25) ,
              PaymentRefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(512) ,
              CorrespondingAccountNumber NVARCHAR(20) ,
              ExchangeRate Numeric ,
              DebitAmount Numeric ,
              DebitAmountOC Numeric(25,4),
              CreditAmount Numeric ,
              CreditAmountOC Numeric(25,4),
              ClosingAmount Numeric ,
              ClosingAmountOC Numeric(25,4),
              ContactName NVARCHAR(512) ,
			  AccountObjectId NVARCHAR(255),
              AccountObjectCode NVARCHAR(255) ,
              AccountObjectName NVARCHAR(512) , 
              EmployeeCode NVARCHAR(255) ,
              EmployeeName NVARCHAR(255) , 
              InvoiceOrder INT , 
              BranchID UNIQUEIDENTIFIER ,
              BranchName NVARCHAR(512) ,
              IsBold BIT,
			  OrderPriority int,
            )
         DECLARE @tblAccountNumber TABLE
            (              
			  AccountNumber  NVARCHAR(25),  
              AccountNumberPercent NVARCHAR(25)              
            )
              
        INSERT  @tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountNumber + '%'
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value

        DECLARE @AccountNumberPercent NVARCHAR(25)
        SET @AccountNumberPercent = @AccountNumber + '%'
        
        DECLARE @DiscountNumber NVARCHAR(25)       
        SET @DiscountNumber = '5211%'
   
        DECLARE @ClosingAmountOC MONEY
        DECLARE @ClosingAmount MONEY
   IF(@CurrencyID = 'VND')
   BEGIN

 INSERT  #Result
	          (RefID ,
			  AccountNumber,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  CorrespondingAccountNumber,
			  OrderPriority)
			  select ReferenceID ,
			  Account,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  AccountCorresponding,
			  OrderPriority from 
			            (SELECT null as ReferenceID,
						    Acc.AccountNumber as Account,
							null as PostedDate,
							null as RefDate,
							null as ReceiptRefNo,
							null as PaymentRefNo,
							null as RefType,
					        N'Số tồn đầu kỳ' AS JournalMemo ,
                            $0 AS ExchangeRate ,
                            $0 AS DebitAmount ,
                            $0 AS DebitAmountOC ,
                            $0 AS CreditAmount ,
                            $0 AS CreditAmountOC ,
							SUM(GL.DebitAmount - GL.CreditAmount) as ClosingAmount,
							SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) as ClosingAmountOC,
							null as ContactName,
							null as AccountObjectId,
                            0 AS InvoiceOrder ,
                            null BranchID ,
                            0 as BranchName,
                            null AccountCorresponding,
							0 as OrderPriority
							 from @tbDataGL gl /*edit by cuongpv GeneralLedger -> @tbDataGL*/
							 inner join @tblAccountNumber Acc on GL.Account like Acc.AccountNumberPercent /*edit by cuongpv: "GL.Account = Acc.AccountNumber" -> "GL.Account like Acc.AccountNumberPercent"*/
							 where ( GL.PostedDate < @FromDate )
							
						    group by Acc.AccountNumber
				        union all
                        SELECT  GL.ReferenceID,
						        GL.Account,
                                GL.PostedDate ,
                                GL.RefDate ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS ReceiptRefNo ,
                                CASE WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS PaymentRefNo ,
                                GL.TypeID ,
								case when GL.Reason is null then
                                GL.Description 
								else GL.Reason 
								end as Description, 
                                GL.ExchangeRate ,
                                GL.DebitAmount ,
                                GL.DebitAmountOriginal ,
                                GL.CreditAmount ,
                                GL.CreditAmountOriginal ,
                                $0 AS ClosingAmount ,
                                $0 AS ClosingAmountOC ,
                                GL.ContactName ,
                                GL.AccountingObjectID ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0 THEN 1
                                     WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0 THEN 2
                                     ELSE 3
                                END AS InvoiceOrder ,
                                GL.BranchID ,
                                0,
                               GL.AccountCorresponding,
							   GL.OrderPriority
                        FROM    @tbDataGL AS GL ,/*edit by cuongpv GeneralLedger -> @tbDataGL*/
						        @tblAccountNumber Acc
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )
                                AND GL.Account LIKE Acc.AccountNumberPercent /*edit by cuongpv: "GL.Account = Acc.AccountNumber" -> "GL.Account like Acc.AccountNumberPercent"*/
                                
                                AND ( GL.CreditAmount <> 0
                                      OR GL.DebitAmount <> 0
                                      OR GL.CreditAmountOriginal <> 0
                                      OR GL.DebitAmountOriginal <> 0
                                    )) T
                        ORDER BY Account,
						        PostedDate ,
                                RefDate ,
                                InvoiceOrder, 
								ReceiptRefNo,
								PaymentRefNo,
								OrderPriority
		END
		ELSE
		   BEGIN
SET @AccountNumber = NULL
 INSERT  #Result
	          (RefID ,
			  AccountNumber,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  CorrespondingAccountNumber,
			  OrderPriority)
			  select ReferenceID ,
			  Account,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  AccountCorresponding,
			  OrderPriority from 
			            (SELECT null as ReferenceID,
						    Acc.AccountNumber as Account,
							null as PostedDate,
							null as RefDate,
							null as ReceiptRefNo,
							null as PaymentRefNo,
							null as RefType,
					        N'Số tồn đầu kỳ' AS JournalMemo ,
                            $0 AS ExchangeRate ,
                            $0 AS DebitAmount ,
                            $0 AS DebitAmountOC ,
                            $0 AS CreditAmount ,
                            $0 AS CreditAmountOC ,
							SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) as ClosingAmount,
							SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) as ClosingAmountOC,
							null as ContactName,
							null as AccountObjectId,
                            0 AS InvoiceOrder ,
                            null BranchID ,
                            0 as BranchName,
                            null AccountCorresponding,
							0 as OrderPriority
							 from @tbDataGL gl /*edit by cuongpv GeneralLedger -> @tbDataGL*/
							 inner join @tblAccountNumber Acc on GL.Account LIKE Acc.AccountNumberPercent /*edit by cuongpv: "GL.Account = Acc.AccountNumber" -> "GL.Account like Acc.AccountNumberPercent"*/
							 where ( GL.PostedDate < @FromDate )
								AND ( @CurrencyID IS NULL
								OR GL.CurrencyID = @CurrencyID)
						    group by Acc.AccountNumber
				        union all
                        SELECT  GL.ReferenceID,
						        GL.Account,
                                GL.PostedDate ,
                                GL.RefDate ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS ReceiptRefNo ,
                                CASE WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS PaymentRefNo ,
                                GL.TypeID ,
								case when GL.Reason is null then
                                GL.Description 
								else GL.Reason 
								end as Description, 
                                GL.ExchangeRate ,
                                GL.DebitAmountOriginal ,
                                GL.DebitAmountOriginal ,
                                GL.CreditAmountOriginal ,
                                GL.CreditAmountOriginal ,
                                $0 AS ClosingAmount ,
                                $0 AS ClosingAmountOC ,
                                GL.ContactName ,
                                GL.AccountingObjectID ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0 THEN 1
                                     WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0 THEN 2
                                     ELSE 3
                                END AS InvoiceOrder ,
                                GL.BranchID ,
                                0,
                               GL.AccountCorresponding,
							   GL.OrderPriority
                        FROM    @tbDataGL AS GL ,/*edit by cuongpv GeneralLedger -> @tbDataGL*/
						        @tblAccountNumber Acc
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )
                                AND GL.Account LIKE Acc.AccountNumberPercent /*edit by cuongpv: "GL.Account = Acc.AccountNumber" -> "GL.Account like Acc.AccountNumberPercent"*/
                                AND ( @CurrencyID IS NULL
                                      OR GL.CurrencyID = @CurrencyID
                                    )
                                AND ( GL.CreditAmount <> 0
                                      OR GL.DebitAmount <> 0
                                      OR GL.CreditAmountOriginal <> 0
                                      OR GL.DebitAmountOriginal <> 0
                                    )) T
                        ORDER BY Account,
						        PostedDate ,
                                RefDate ,
                                InvoiceOrder, 
								ReceiptRefNo,
								PaymentRefNo,
								OrderPriority
								
		END
        SET @ClosingAmount = 0
        SET @ClosingAmountOC = 0

        UPDATE  #Result
        SET      @ClosingAmount = (Case when JournalMemo like N'%Số tồn đầu kỳ%'
										then ClosingAmount
										when @AccountNumber <> AccountNumber
										then DebitAmount - CreditAmount
										else @ClosingAmount  + ClosingAmount +ISNULL(DebitAmount, 0)
												- ISNULL(CreditAmount, 0) 
										end),	
                ClosingAmount = @ClosingAmount ,

                @ClosingAmountOC = (Case when JournalMemo like N'%Số tồn đầu kỳ%'
										then ClosingAmountOC
										when @AccountNumber <> AccountNumber
										then DebitAmountOC - CreditAmountOC
										else @ClosingAmountOC  + ClosingAmountOC +ISNULL(DebitAmountOC, 0)
												- ISNULL(CreditAmountOC, 0) 
										end),	
                ClosingAmountOC = @ClosingAmountOC ,
				@AccountNumber = AccountNumber
		
	
        SELECT  RowNum,RefID,RefType,AccountNumber,PostedDate,RefDate,ReceiptRefNo,PaymentRefNo,JournalMemo,CorrespondingAccountNumber,DebitAmountOC,DebitAmount,CreditAmountOC,CreditAmount,ClosingAmountOC,ClosingAmount,ContactName,OrderPriority,0 as ordercode
        into #tg1
		FROM    #Result R   
		order by rownum

			INSERT INTO #tg1 (RefID,RefType,AccountNumber,PostedDate,RefDate,ReceiptRefNo,PaymentRefNo,JournalMemo,CorrespondingAccountNumber,DebitAmountOC,DebitAmount,CreditAmountOC,CreditAmount,ClosingAmountOC,ClosingAmount,ContactName,OrderPriority,ordercode)  
			SELECT  null,null,AccountNumber,null,null,null,null,N'Cộng nhóm',null,Sum(DebitAmountOC),Sum(DebitAmount),Sum(CreditAmountOC),Sum(CreditAmount),0,0,null,null,1
			FROM #Result
			group by AccountNumber
		
			SELECT  DISTINCT(AccountNumber) into #Account from #tg1
			DECLARE @row int
			DECLARE @Acc nvarchar(512)
			DECLARE @COUNT INT
			DECLARE @ClAmount decimal(25,0)
			DECLARE @ClAmountOC decimal(25,4)
			SET @COUNT = (SELECT COUNT(*) From #Account)
			WHILE(@COUNT > 0)
			BEGIN
			SET @Acc = (SELECT TOP 1 AccountNumber FROM #Account)
			SET @row = (select Max(RowNum)
			from  #Result where AccountNumber = @Acc)
			SET @ClAmount = (select (ClosingAmount) from #Result WHERE RowNum = @row AND  AccountNumber = @Acc )
			SET @ClAmountOC = (select (ClosingAmountOC) from #Result WHERE RowNum = @row AND  AccountNumber = @Acc )
			UPDATE #tg1
			SET ClosingAmount = @ClAmount,
			ClosingAmountOC = @ClAmountOC

			WHERE AccountNumber = @Acc AND ordercode = 1
			SET @COUNT = @COUNT -1;
			DELETE #Account where AccountNumber = @Acc 
			END

			SELECT * from #tg1 ORDER BY AccountNumber, ordercode, PostedDate, RefDate,rownum, OrderPriority
        DROP TABLE #Result 
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE PROCEDURE [dbo].[Proc_PO_PayDetailNCC1]
	 @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS 
    BEGIN     
        DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(512) ,
              AccountObjectAddress NVARCHAR(512) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        null AccountingObjectGroupCode ,
                        null AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
		
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/
		      
        CREATE TABLE #tblResult
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100)  ,
              AccountObjectName NVARCHAR(512)  ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL ,             
              AccountObjectAddress NVARCHAR(512)  ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25)  ,
              DueDate DATETIME , 
              InvDate DATETIME ,
              InvNo NVARCHAR(25)  ,

              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              JournalMemo NVARCHAR(512)  , 
              AccountNumber NVARCHAR(20)  , 
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20)  , 
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,	
              DebitAmount MONEY , 
              CreditAmountOC MONEY , 
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY ,
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,	
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50)  ,
              InventoryItemName NVARCHAR(512)  ,
              UnitName NVARCHAR(20)  ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(18, 4) , 
              BranchName NVARCHAR(128)  ,
              IsBold BIT ,	
              OrderType INT ,
              GLJournalMemo NVARCHAR(512)  ,
              DocumentIncluded NVARCHAR(512)  ,
              CustomField1 NVARCHAR(512)  ,
              CustomField2 NVARCHAR(512)  ,
              CustomField3 NVARCHAR(512)  ,
              CustomField4 NVARCHAR(512)  ,
              CustomField5 NVARCHAR(512)  ,
              CustomField6 NVARCHAR(512)  ,
              CustomField7 NVARCHAR(512)  ,
              CustomField8 NVARCHAR(512)  ,
              CustomField9 NVARCHAR(512)  ,
              CustomField10 NVARCHAR(512) ,
               MainUnitName NVARCHAR(20)  ,
              MainQuantity DECIMAL(22, 8) ,
              MainUnitPrice DECIMAL(18, 4),
			  OrderPriority int ,
			  ID UNIQUEIDENTIFIER,
			  TypeID int
            )
        
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(512) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
			
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
            
                
/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
IF(@CurrencyID='VND')
BEGIN
INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode , 
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,      
                          AccountObjectAddress , 
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID , 
                          JournalMemo , 				  
                          AccountNumber , 
                          AccountCategoryKind ,
                          CorrespondingAccountNumber ,	  			  
                          DebitAmountOC ,	
                          DebitAmount , 
                          CreditAmountOC ,
                          CreditAmount ,
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount ,
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount,
						  OrderType, 
						  ID,
						  TypeID,
						  OrderPriority
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,
                                AccountObjectName ,	
                                AccountObjectGroupListCode , 
                                AccountObjectGroupListName ,            
                                AccountObjectAddress , 
                                RSNS.PostedDate ,
                                NgayCtu ,
                                SoCtu ,
                                InvDate ,
                                InvNo ,								
                                RefType ,
                                RSNS.RefID , 
                                JournalMemo ,			  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  			  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) , 
                                SUM(CreditAmount) , 
                                ( CASE WHEN AccountCategoryKind = 0 THEN SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) ) > 0 THEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,      
                                ( CASE WHEN AccountCategoryKind = 0 THEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) ) > 0 THEN SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) ) > 0 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) ) > 0 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount,  
								  OrderType,
								   ID,
								   TypeID	,
								   OrderPriority						 
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,
                                            AccountObjectGroupListCode ,
                                            AccountObjectGroupListName ,         
                                            LAOI.AccountObjectAddress ,
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.Date
                                            END AS NgayCtu ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.No
                                            END AS SoCtu ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType , 
                                            CASE WHEN AOL.PostedDate < @FromDate 
												      THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN N'Số dư đầu kỳ'
                                                 ELSE AOL.Description
                                            END AS JournalMemo , 
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                             
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                  
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount,
											CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType, 
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS ID , 
								  CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS TypeID 	,	

											CASE WHEN AOL.PostedDate < @FromDate THEN 0
                                                 ELSE AOL.OrderPriority
											END AS OrderPriority		
											
                                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                                         
                                  WHERE     AOL.PostedDate <= @ToDate
                                            
                                ) AS RSNS                               
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode ,  
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,
                                RSNS.AccountObjectAddress ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu , 
                                RSNS.SoCtu , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,
                                RSNS.JournalMemo , 		  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber,
								OrderType,
								RSNS.ID,
								RSNS.TypeID,
								RSNS.OrderPriority
								/*OrderPriority - comment by cuongpv*/	
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount - ClosingCreditAmount) <> 0
                        ORDER BY 
						        RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu ,
                                RSNS.SoCtu,
								RSNS.OrderPriority
								/*OrderPriority - comment by cuongpv*/

                OPTION  ( RECOMPILE ) 
END
ELSE
BEGIN
INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode , 
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,      
                          AccountObjectAddress , 
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID , 
                          JournalMemo , 				  
                          AccountNumber , 
                          AccountCategoryKind ,
                          CorrespondingAccountNumber ,	  			  
                          DebitAmountOC ,	
                          DebitAmount , 
                          CreditAmountOC ,
                          CreditAmount ,
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount ,
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount,
						  OrderType,
						  ID,
						  TypeID,
						  OrderPriority
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,
                                AccountObjectName ,	
                                AccountObjectGroupListCode , 
                                AccountObjectGroupListName ,            
                                AccountObjectAddress , 
                                RSNS.PostedDate ,
                                NgayCtu ,
                                SoCtu ,
                                InvDate ,
                                InvNo ,
                                RefType ,								
                                RSNS.RefID , 
                                JournalMemo ,			  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  			  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) , 
                                SUM(CreditAmount) , 
                                ( CASE WHEN AccountCategoryKind = 0 THEN SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) ) > 0 THEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,      
                                ( CASE WHEN AccountCategoryKind = 0 THEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) ) > 0 THEN SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) ) > 0 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) ) > 0 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount,  
								  OrderType,
								  ID,
								  TypeID,
								  OrderPriority
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,
                                            AccountObjectGroupListCode ,
                                            AccountObjectGroupListName ,         
                                            LAOI.AccountObjectAddress ,
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.Date
                                            END AS NgayCtu ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.No
                                            END AS SoCtu ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType , 
                                            CASE WHEN AOL.PostedDate < @FromDate 
												      THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN N'Số dư đầu kỳ'
                                                 ELSE AOL.Description
                                            END AS JournalMemo , 
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                             
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                  
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount,
											CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType,
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS ID , 
								  CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS TypeID ,
											CASE WHEN AOL.PostedDate < @FromDate THEN 0
                                                 ELSE AOL.OrderPriority
											END AS OrderPriority	
                                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                                         
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )

											AND (AOL.DebitAmountOriginal <>0 OR AOL.CreditAmountOriginal <>0)
                                ) AS RSNS                               
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode ,  
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,
                                RSNS.AccountObjectAddress ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu , 
                                RSNS.SoCtu , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,
                                RSNS.JournalMemo , 		  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber,
								OrderType,
								RSNS.ID,
								RSNS.TypeID,
								RSNS.OrderPriority

                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount - ClosingCreditAmount) <> 0
                        ORDER BY 
						        RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu ,
                                RSNS.SoCtu,
								RSNS.OrderPriority
								/*OrderPriority - comment by cuongpv*/

                OPTION  ( RECOMPILE )  
				END
/* Tính số tồn */ 
/* edit by namnh
 @CloseAmountOC,@CloseAmount Decimal(22,8) -> Money)*/
        DECLARE @CloseAmountOC AS Money ,
            @CloseAmount AS Money ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmountOC = 0 THEN ClosingCreditAmountOC
                                                                       ELSE -1 * ClosingDebitAmountOC
                                                                  END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1 THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0 THEN -1 * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0 THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0 THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmount = 0 THEN ClosingCreditAmount
                                                                     ELSE -1 * ClosingDebitAmount
                                                                END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1 THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0 THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0 THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0 THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
        SELECT  RS.PostedDate as Ngay_HT, 
		        RS.RefDate as ngayCtu,
				RS.RefNo as SoCtu,
				Rs.JournalMemo as DienGiai,
				/*'331' as TK_CONGNO, comment by cuongpv*/
				Rs.CorrespondingAccountNumber as TkDoiUng,
                RS.AccountObjectCode,
				RS.AccountObjectName,
				
				RS.AccountNumber as TK_CONGNO, /*edit by cuongpv RS.AccountNumber -> RS.AccountNumber as TK_CONGNO*/
				fn.OpeningDebitAmount as OpeningDebitAmount,
				fn.OpeningCreditAmount as OpeningCreditAmount,
				Rs.DebitAmount as DebitAmount,
				Rs.DebitAmountOC as DebitAmountOC,
				Rs.CreditAmount as CreditAmount,
				Rs.CreditAmountOC as CreditAmountOC,
				Rs.ClosingDebitAmount as ClosingDebitAmount,
				Rs.ClosingDebitAmountOC as ClosingDebitAmountOC,
				Rs.ClosingCreditAmount as ClosingCreditAmount,
				Rs.ClosingCreditAmountOC as ClosingCreditAmountOC,
				0 as ordercode,
				Rs.TypeID as TypeID,
				Rs.ID as ID,
				RS.OrderPriority as OrderPriority
			
				into #tg1
        FROM    #tblResult RS ,
		        [dbo].[Func_SoDu] (
   @FromDate
  ,@ToDate
  ,3) fn
  where rs.AccountNumber = fn.AccountNumber
  and rs.AccountObjectID = fn.AccountObjectID
  
  order by RS.AccountObjectCode ,
                                RS.AccountNumber ,
                                RS.PostedDate ,
                                RS.RefDate ,
                                RS.RefNo,
								RS.OrderPriority

	INSERT INTO #tg1 
	SELECT null,null,N'Cộng nhóm',null,null,AccountObjectCode,AccountObjectName,null,null ,null,Sum(DebitAmount) as DebitAmount,Sum(DebitAmountOC) as DebitAmountOC,Sum(CreditAmount) as CreditAmount,Sum(CreditAmountOC) as CreditAmountOC,null,null,null,null,1,null,null,null
	FROM #tg1
	GROUP BY AccountObjectCode,AccountObjectName

		INSERT INTO #tg1 
	SELECT null,null,N'Tên nhà cung cấp :',null,null,AccountObjectCode,AccountObjectName,null,null ,null,null,null,null,null,null,null,null,null,-1,null,null,null
	FROM #tg1
	GROUP BY AccountObjectCode,AccountObjectName

	select * from #tg1 
	   order by AccountObjectCode,
	   ordercode,
	   TK_CONGNO,
                                
                                Ngay_HT ,
                                ngayCtu ,
                                SoCtu,
								OrderPriority

								
								

    END				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_SoTongHopDoanhThuTheoNhanVien]
	@FromDate DATETIME,
    @ToDate DATETIME,
    @AccountingObjectID NVARCHAR(MAX),
	@MaterialGoodsID NVARCHAR(MAX)
AS
BEGIN     
	DECLARE @tblSoTongHopDoanhThuTheoNV TABLE(
			AccountingObjectID uniqueidentifier,
			AccountingObjectCode  NVARCHAR(25),
			AccountingObjectName NVARCHAR(512),
			MaterialGoodsID uniqueidentifier,
			MaterialGoodsCode  NVARCHAR(25),
			MaterialGoodsName NVARCHAR(512),
			DoanhSo decimal(25,0),
			GiamTruDoanhThu decimal (25,0)
			)
	DECLARE @tbltemp TABLE(
			AccountingObjectID uniqueidentifier,
			MaterialGoodsID uniqueidentifier,
			DoanhSo decimal(25,0),
			GiamTruDoanhThu decimal (25,0)
			)
	DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate >= @FromDate and GL.PostedDate <= @ToDate


	DECLARE @tblListAccountingObjectID TABLE
	(
	AccountingObjectID uniqueidentifier
	)
	
		 INSERT  INTO @tblListAccountingObjectID
         SELECT  TG.ID
         FROM    AccountingObject AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountingObjectID,',') AS AccountingObjectID ON TG.ID = AccountingObjectID.Value
         WHERE  AccountingObjectID.Value IS NOT NULL


	DECLARE @tblListMaterialGoodsID TABLE
	(
	MaterialGoodsID uniqueidentifier
	)
	
		 INSERT  INTO @tblListMaterialGoodsID
         SELECT  TG.ID
         FROM    MaterialGoods AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@MaterialGoodsID,',') AS MaterialGoodsID ON TG.ID = MaterialGoodsID.Value
         WHERE  MaterialGoodsID.Value IS NOT NULL
		INSERT INTO @tbltemp (AccountingObjectID,MaterialGoodsID,DoanhSo,GiamTruDoanhThu)
		SELECT a.employeeid,c.MaterialGoodsID, a.CreditAmount,a.DebitAmount
		FROM @tbDataGL a
		join [SAInvoiceDetail] c ON a.DetailID = c.ID
		 where a.employeeid in (Select AccountingObjectID From @tblListAccountingObjectID)
		 and c.MaterialGoodsID in (Select MaterialGoodsID From @tblListMaterialGoodsID)
		 And Account like '511%'
		 and PostedDate >= @FromDate and PostedDate <= @ToDate
		 INSERT INTO @tbltemp (AccountingObjectID,MaterialGoodsID,DoanhSo,GiamTruDoanhThu)
		SELECT a.employeeid,c.MaterialGoodsID,a.CreditAmount, a.DebitAmount
		FROM @tbDataGL a 
		join [SAReturnDetail] c ON a.DetailID = c.ID
		 where a.employeeid in (Select AccountingObjectID From @tblListAccountingObjectID)
		 and c.MaterialGoodsID in (Select MaterialGoodsID From @tblListMaterialGoodsID)
		 And Account like '511%'
		 and PostedDate >= @FromDate and PostedDate <= @ToDate

	
	INSERT INTO @tblSoTongHopDoanhThuTheoNV(AccountingObjectID,MaterialGoodsID,DoanhSo,GiamTruDoanhThu)
		SELECT a.AccountingObjectID,a.MaterialGoodsID,SUM(a.DoanhSo), SUM(a.GiamTruDoanhThu)
		FROM @tbltemp a
		GROUP BY AccountingObjectID,a.MaterialGoodsID		

	Update @tblSoTongHopDoanhThuTheoNV 
	Set MaterialGoodsCode = k.MaterialGoodsCode, MaterialGoodsName = k.MaterialGoodsName
	From (Select ID,MaterialGoodsCode, MaterialGoodsName From MaterialGoods ) k
	where MaterialGoodsID=k.ID

	Update @tblSoTongHopDoanhThuTheoNV 
	Set AccountingObjectCode = k.AccountingObjectCode, AccountingObjectName = k.AccountingObjectName
	From (Select ID,AccountingObjectCode, AccountingObjectName From AccountingObject ) k
	where AccountingObjectID=k.ID

	Select AccountingObjectID,AccountingObjectCode,AccountingObjectName,MaterialGoodsID,MaterialGoodsCode,MaterialGoodsName,DoanhSo,GiamTruDoanhThu, 2 as ordercode
	into #tg1
	 From @tblSoTongHopDoanhThuTheoNV
	 Order By MaterialGoodsCode

		insert into #tg1 (AccountingObjectID,AccountingObjectCode,AccountingObjectName,MaterialGoodsID,MaterialGoodsCode,MaterialGoodsName,DoanhSo,GiamTruDoanhThu, ordercode)
		select null, AccountingObjectCode, AccountingObjectName, null, null, N'Mã nhân viên: ' + AccountingObjectCode + '  -  ' + N'Tên nhân viên: ' + AccountingObjectName, null, null, 1
		From @tblSoTongHopDoanhThuTheoNV
		group by AccountingObjectCode, AccountingObjectName

		SELECT TOP 0 *
			into #tg2
			FROM #tg1

		insert into #tg2 (AccountingObjectID,AccountingObjectCode,AccountingObjectName,MaterialGoodsID,MaterialGoodsCode,MaterialGoodsName,DoanhSo,GiamTruDoanhThu, ordercode)
		select null, AccountingObjectCode, AccountingObjectName, null, null, N'Cộng nhóm', SUM(DoanhSo), SUM(GiamTruDoanhThu), 3
		From @tblSoTongHopDoanhThuTheoNV
		group by AccountingObjectCode, AccountingObjectName

		INSERT INTO #tg1 SELECT * from #tg2
		SELECT * from #tg1 order by AccountingObjectCode,AccountingObjectName,ordercode
END


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_SoTheoDoiChiTietTheoDoiTuongTHCP]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @CostSetID  NVARCHAR(MAX),
	@Account NVARCHAR(MAX)
AS
BEGIN     
	DECLARE @tblSoTheoDoiTheoDT TABLE(
			CostSetID uniqueidentifier,
			RefID uniqueidentifier,
			TypeID int,
			CostSetCode  NVARCHAR(25),
			CostSetName NVARCHAR(512),
			NgayChungTu Date,
			 SoChungTu NCHAR(20),
			 DienGiai NVARCHAR(MAX),
			 TK NVARCHAR(50),
			 TKDoiUng NVARCHAR(50),
			 SoTienNo decimal(25,0),
			 SoTienCo decimal(25,0),
			 OrderPriority int)
	
	
	DECLARE @tblListCostSetID TABLE
	(
	CostSetID uniqueidentifier
	)
	DECLARE @tblListAccount TABLE
	(
	Account NVARCHAR(25)
	)


		 INSERT  INTO @tblListCostSetID
         SELECT  TG.id
         FROM    CostSet AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@CostSetID,',') AS CostSetID ON TG.ID = CostSetID.Value
         WHERE  CostSetID.Value IS NOT NULL

	
		INSERT  INTO @tblListAccount
         SELECT  TG.AccountNumber
         FROM    Account AS TG
                 LEFT JOIN dbo.Func_SplitString(@Account,',') AS Account ON TG.AccountNumber = Account.splitdata
         WHERE  Account.splitdata IS NOT NULL
		
                              			
		INSERT INTO @tblSoTheoDoiTheoDT
		SELECT a.CostSetID, a.ReferenceID as RefID, a.TypeID,b.CostSetCode, b.CostSetName ,Date as NgayChungTu , No as SoChungTu, Reason as DienGiai, Account as TK, AccountCorresponding as TKDoiUng, DebitAmount as SoTienNo, CreditAmount as SoTienCo, a.OrderPriority
		FROM [GeneralLedger] a 
		LEFT JOIN [CostSet] b ON a.CostSetID = b.ID
		where CostSetID in (select CostSetID from @tblListCostSetID) AND a.Account in (select Account from @tblListAccount)
		and Date > = @FromDate and Date < = @ToDate
		order by Date, a.OrderPriority

		Select CostSetID,RefID,TypeID,CostSetCode,CostSetName,NgayChungTu,SoChungTu,DienGiai,TK,TKDoiUng,SoTienNo,SoTienCo From @tblSoTheoDoiTheoDT order by NgayChungTu,SoChungTu,OrderPriority
END






GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SoChiTietPhaiThuKhachHangTheoMatHang]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountNumber NVARCHAR(25),
    @AccountObjectID AS NVARCHAR(MAX),
    @CurrencyID NVARCHAR(3),
    @AccountObjectGroupID AS NVARCHAR(MAX),
	@MaterialsGoodID as NVARCHAR(MAX)

AS
    BEGIN          
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

		DECLARE @tblListAccountObjectID TABLE
            (
             AccountObjectID UNIQUEIDENTIFIER ,
             AccountObjectGroupList NVARCHAR(MAX)           
            ) 
		     
        INSERT  INTO @tblListAccountObjectID
        SELECT  
			AO.ID
			,AO.AccountObjectGroupID
        FROM AccountingObject AS AO 
        LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') AS TLAO 
			ON AO.ID = TLAO.Value
        WHERE 
		(@AccountObjectID IS NULL OR TLAO.Value IS NOT NULL)


		DECLARE @tblListMaterialsGoodID TABLE
            (
             MaterialsGoodID UNIQUEIDENTIFIER        
            ) 
             
   INSERT  INTO @tblListMaterialsGoodID
         SELECT  TG.id
         FROM    MaterialGoods AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@MaterialsGoodID,',') AS MaterialsGoodID ON TG.ID = MaterialsGoodID.Value
         WHERE  MaterialsGoodID.Value IS NOT NULL

	

			DECLARE @tblResult TABLE
            (
			 AccountingObjectID uniqueidentifier,
			 AccountingObjectCode nvarchar(25),
			 AccountingObjectName nvarchar(512),
			 SAInvoiceDetail uniqueidentifier,
			 InvoiceNo nvarchar(25),
			 PostedDateOrder Datetime,
             PostedDate Datetime,
			 Date Datetime,
			 No nvarchar(25),
			 NoOrder nvarchar(25),
			 Reason nvarchar(512),
			 Description nvarchar(512),
			 Account nvarchar(25),
			 Unit nvarchar(25),
			 Quantity decimal(25,10),
			 UnitPrice decimal(25,3),
			 SoTien money,
			 ChietKhau decimal(25,3),
			 ThueGTGT decimal(25,3),
			 TraLaiGiamGia money,
			 SoDaThu money,
			 SoConPhaiThu money,
			 Type int,
			 OrderPriority int,
			 DetailID uniqueidentifier,
			 ReferenceID uniqueidentifier,
			 CurrencyID nvarchar(3),
			 RefType int
            ) 

			DECLARE @accnumber nvarchar(128)

			SET @accnumber = @AccountNumber + '%'

			
			INSERT INTO @tblResult 
			SELECT DISTINCT  d.AccountingObjectID,null,null,b.SAInvoiceID,d.InvoiceNo,d.PostedDate,d.PostedDate,d.Date,d.No,d.No,d.Description,d.Reason,d.Account,b.Unit,b.Quantity,b.UnitPrice,0,0,0,0,0,0,1,2,d.DetailID,d.ReferenceID,d.CurrencyID,d.TypeID
			FROM  @tbDataGL d 
			INNER JOIN SAInvoiceDetail b ON d.DetailID = b.ID 
			WHERE d.PostedDate >=@FromDate 
			AND d.PostedDate <@ToDate AND d.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			AND d.TypeID NOT IN ('330','340') and MaterialGoodsID IN (SELECT * from @tblListMaterialsGoodID)
			AND AccountCorresponding NOT LIKE '111%' AND AccountCorresponding NOT LIKE '112%'
			 AND Account LIKE @accnumber AND CurrencyID = @CurrencyID
			
			UPDATE @tblResult SET ChietKhau = a.CA
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D, DetailID DID, CurrencyID CID
		From @tbDataGL
		WHERE  AccountingObjectID IN (SELECT AccountingObjectID from @tblListAccountObjectID) 
		AND TypeID NOT IN ('330','340') AND AccountCorresponding NOT LIKE '111%' AND AccountCorresponding NOT LIKE '112%'
		and AccountCorresponding not like '3331%' and CreditAmount >0
		AND Account LIKE @accnumber and CurrencyID = @CurrencyID
	) a
	WHERE  Account like @accnumber  AND Reason = a.D and DetailID = a.DID and CurrencyID = a.CID


	UPDATE @tblResult SET ThueGTGT = a.DA
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D,DetailID  DID, CurrencyID CID
		From @tbDataGL
		WHERE AccountCorresponding LIKE '3331%' AND AccountingObjectID IN (SELECT AccountingObjectID from @tblListAccountObjectID) 
		AND TypeID NOT IN ('330','340')  and DebitAmount >0 AND CurrencyID = @CurrencyID
	) a
	WHERE  Account = @AccountNumber  AND Reason = a.D and DetailID =  a.DID and Type = 1 and CurrencyID = a.CID
		
		UPDATE @tblResult SET SoTien = a.DA
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D,DetailID  DID, CurrencyID CID
		From @tbDataGL
		WHERE   TypeID NOT IN ('330','340') AND AccountCorresponding NOT LIKE '111%' AND AccountCorresponding NOT LIKE '112%'
		and AccountCorresponding NOT LIKE '3331%'
		AND Account LIKE '131%' AND CurrencyID = @CurrencyID  AND DebitAmount >0
	) a
	WHERE  Account = @AccountNumber  AND Reason = a.D and DetailID =  a.DID and Type = 1 and CurrencyID = a.CID


			
			UPDATE @tblResult SET SoConPhaiThu = (SoTien - ChietKhau + ThueGTGT -TraLaiGiamGia - SoDaThu ) WHERE Type = 1

			INSERT INTO @tblResult 
			SELECT AccountingObjectID,null,null,SAInvoiceDetail,InvoiceNo,PostedDateOrder,PostedDate, Date, No,NoOrder, Description,Description,null,null, 0,0,0,0,0,0,0,0,1,1,null,ReferenceID,CurrencyID,RefType
			From @tblResult
			WHERE Type = 1
			group by PostedDate,Date,No,Description,AccountingObjectID,SAInvoiceDetail,PostedDateOrder,InvoiceNo,NoOrder,ReferenceID,CurrencyID,RefType

			INSERT INTO @tblResult 
			SELECT DISTINCT  d.AccountingObjectID,null,null,b.SAInvoiceID,d.InvoiceNo,d.PostedDate,d.PostedDate,d.Date,d.No,d.No,d.Description,d.Reason,d.Account,b.Unit,b.Quantity,b.UnitPrice,0,0,0,0,0,0,2,2,d.DetailID,d.ReferenceID,d.CurrencyID, d.TypeID
			FROM  @tbDataGL d 
			INNER JOIN SAReturnDetail b ON d.DetailID = b.ID 
			WHERE d.PostedDate >=@FromDate 
			AND d.PostedDate <@ToDate AND d.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			AND d.TypeID ='330' and MaterialGoodsID IN (SELECT * from @tblListMaterialsGoodID)
			 AND Account LIKE @accnumber AND CurrencyID = @CurrencyID
			
			UPDATE @tblResult SET ChietKhau = a.DA
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D, DetailID DID, CurrencyID CID
		From @tbDataGL
		WHERE  AccountingObjectID IN (SELECT AccountingObjectID from @tblListAccountObjectID) 
		AND TypeID ='330'
		and AccountCorresponding not like '3331%' and DebitAmount >0
		AND Account LIKE @accnumber  AND CurrencyID = @CurrencyID
	) a
	WHERE  Account like @accnumber  AND Reason = a.D and DetailID = a.DID and CurrencyID = a.CID


	UPDATE @tblResult SET ThueGTGT = -(a.CA)
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D,DetailID  DID, CurrencyID CID
		From @tbDataGL
		WHERE AccountCorresponding LIKE '3331%' AND AccountingObjectID IN (SELECT AccountingObjectID from @tblListAccountObjectID) 
		AND TypeID ='330'  and CreditAmount >0 AND CurrencyID = @CurrencyID
	) a
	WHERE  Account = @AccountNumber  AND Reason = a.D and DetailID =  a.DID and Type = 2 and CurrencyID = a.CID
		
			UPDATE @tblResult SET TralaiGiamGia = -(a.CA)
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D,DetailID  DID, CurrencyID CID
		From @tbDataGL
		WHERE   AccountingObjectID IN (SELECT AccountingObjectID from @tblListAccountObjectID) 
		AND TypeID ='330'
		and AccountCorresponding not like '3331%'
		AND Account LIKE @accnumber
		AND CreditAmount >0
		AND CurrencyID = @CurrencyID
	) a
	WHERE  Account = @AccountNumber  AND Reason = a.D and DetailID =  a.DID and Type = 2 and CurrencyID = a.CID


			UPDATE @tblResult SET SoConPhaiThu = (SoTien + ChietKhau + ThueGTGT +TraLaiGiamGia - SoDaThu ) WHERE Type = 2

			INSERT INTO @tblResult 
			SELECT AccountingObjectID,null,null,SAInvoiceDetail,InvoiceNo,PostedDateOrder,PostedDate, Date, No,NoOrder, Description,Description,null,null, 0,0,0,0,0,0,0,0,2,1,null,ReferenceID,CurrencyID,RefType
			From @tblResult
			WHERE Type = 2
			group by PostedDate,Date,No,Description,AccountingObjectID,SAInvoiceDetail,PostedDateOrder,InvoiceNo,NoOrder,ReferenceID, CurrencyID, RefType


			INSERT INTO @tblResult 
			SELECT DISTINCT  d.AccountingObjectID,null,null,b.SAInvoiceID,d.InvoiceNo,d.PostedDate,d.PostedDate,d.Date,d.No,d.No,d.Description,d.Reason,d.Account,b.Unit,b.Quantity,b.UnitPrice,0,0,0,0,0,0,3,2,d.DetailID,d.ReferenceID,d.CurrencyID, d.TypeID
			FROM  @tbDataGL d 
			INNER JOIN SAReturnDetail b ON d.DetailID = b.ID 
			WHERE d.PostedDate >=@FromDate 
			AND d.PostedDate <@ToDate AND d.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			AND d.TypeID ='340' and MaterialGoodsID IN (SELECT * from @tblListMaterialsGoodID)
			 AND Account LIKE @accnumber AND CurrencyID = @CurrencyID
			
			UPDATE @tblResult SET ChietKhau = a.DA
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D, DetailID DID, CurrencyID CID
		From @tbDataGL
		WHERE  AccountingObjectID IN (SELECT AccountingObjectID from @tblListAccountObjectID) 
		AND TypeID ='340'
		and AccountCorresponding not like '3331%' and DebitAmount >0
		AND Account LIKE @accnumber 
		AND CurrencyID = @CurrencyID
	) a
	WHERE  Account like @accnumber  AND Reason = a.D and DetailID = a.DID and CurrencyID = a.CID


	UPDATE @tblResult SET ThueGTGT = -(a.CA)
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D,DetailID  DID, CurrencyID CID
		From @tbDataGL
		WHERE AccountCorresponding LIKE '3331%' AND AccountingObjectID IN (SELECT AccountingObjectID from @tblListAccountObjectID) 
		AND TypeID ='340'  and CreditAmount >0 AND CurrencyID = @CurrencyID
	) a
	WHERE  Account = @AccountNumber  AND Reason = a.D and DetailID =  a.DID and Type = 3 and CurrencyID = a.CID
		
		UPDATE @tblResult SET TralaiGiamGia = -(a.CA)
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D,DetailID  DID, CurrencyID CID
		From @tbDataGL
		WHERE   AccountingObjectID IN (SELECT AccountingObjectID from @tblListAccountObjectID) 
		AND TypeID ='340'
		and AccountCorresponding not like '3331%' and CreditAmount >0
		AND Account LIKE @accnumber  AND CurrencyID = @CurrencyID
	) a
	WHERE  Account = @AccountNumber  AND Reason = a.D and DetailID =  a.DID and Type = 3 and CurrencyID = a.CID


			
			UPDATE @tblResult SET SoConPhaiThu = (SoTien + ChietKhau + ThueGTGT + TraLaiGiamGia - SoDaThu ) WHERE Type = 3

			INSERT INTO @tblResult 
			SELECT AccountingObjectID,null,null,SAInvoiceDetail,InvoiceNo,PostedDateOrder,PostedDate, Date, No,NoOrder, Description,Description,null,null, 0,0,0,0,0,0,0,0,3,1,null,ReferenceID,CurrencyID,RefType
			From @tblResult
			WHERE Type = 3
			group by PostedDate,Date,No,Description,AccountingObjectID,SAInvoiceDetail,PostedDateOrder,InvoiceNo,NoOrder,ReferenceID,CurrencyID,RefType


			INSERT INTO @tblResult 
			SELECT DISTINCT  d.AccountingObjectID,null,null,null,d.InvoiceNo,d.PostedDate,d.PostedDate,d.Date,d.No,d.No,d.Description,d.Reason,d.Account,null,null,null,0,0,0,0,0,0,4,2,d.DetailID,d.ReferenceID,d.CurrencyID,d.TypeID
			FROM  @tbDataGL d 
			
			WHERE d.PostedDate >=@FromDate 
			AND d.PostedDate <@ToDate AND d.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			AND (AccountCorresponding LIKE '111%' OR AccountCorresponding LIKE '112%')
			 AND Account LIKE @accnumber AND CurrencyID = @CurrencyID
			
			

		
		UPDATE @tblResult SET SoDaThu = a.CA
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description as D,DetailID  DID,CurrencyID CID
		From @tbDataGL
		WHERE   AccountingObjectID IN (SELECT AccountingObjectID from @tblListAccountObjectID) 
		and CreditAmount >0
		AND (AccountCorresponding LIKE '111%' OR AccountCorresponding LIKE '112%' )
		AND Account LIKE @accnumber  AND CurrencyID = @CurrencyID
	) a
	WHERE  Account = @AccountNumber  AND Reason = a.D and DetailID =  a.DID and Type = 4 and CurrencyID = a.CID

			
			UPDATE @tblResult SET SoConPhaiThu = (SoTien + ChietKhau + ThueGTGT +TraLaiGiamGia - SoDaThu ) WHERE Type = 4

			INSERT INTO @tblResult 
			SELECT AccountingObjectID,null,null,SAInvoiceDetail,InvoiceNo,PostedDateOrder,PostedDate, Date, No,NoOrder, Description,Description,null,null, 0,0,0,0,0,0,0,0,4,1,null,ReferenceID,CurrencyID,RefType
			From @tblResult
			WHERE Type = 4
			group by PostedDate,Date,No,Description,AccountingObjectID,SAInvoiceDetail,PostedDateOrder,InvoiceNo,NoOrder,ReferenceID,CurrencyID,RefType

			 

			  UPDATE @tblResult SET AccountingObjectCode =a.AOC, AccountingObjectName = a.AON
			FROM (
			Select ID as AOI, AccountingObjectCode as AOC, AccountingObjectName AON
			From AccountingObject
			) a
			WHERE AccountingObjectID = a.AOI


				UPDATE @tblResult SET PostedDate = null,Date = null, No = null where OrderPriority = 2
		SELECT * from @tblResult
		order by PostedDateOrder, NoOrder,Type,OrderPriority
			
		
		       			
    END

	
	
	
	 
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_SoChiTietDoanhThuTheoNhanVien]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountingObjectCode NVARCHAR(MAX)
AS
BEGIN     
	DECLARE @tblSoChiTietDoanhThuTheoNV TABLE(
			AccountingObjectCode  NVARCHAR(25),
			AccountingObjectName NVARCHAR(512),
			RefID UNIQUEIDENTIFIER,
			RefType int,
			NgayChungTu Date,
			NgayHachToan Date,
			 SoChungTu NCHAR(20),
			 DienGiai NVARCHAR(MAX),
			 DoanhSo decimal(25,0),
			 GiamDoanhThu decimal (25,0),
			 GhiChu NVARCHAR(MAX))

	DECLARE @tblListAccountingObjectCode TABLE
	(
	AccountingObjectCode uniqueidentifier
	)
	
		 INSERT  INTO @tblListAccountingObjectCode
         SELECT  TG.ID
         FROM    AccountingObject AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountingObjectCode,',') AS AccountingObjectCode ON TG.ID = AccountingObjectCode.Value
         WHERE  AccountingObjectCode.Value IS NOT NULL

		INSERT INTO @tblSoChiTietDoanhThuTheoNV
		SELECT b.accountingobjectcode , b.accountingobjectname , a.ReferenceID as RefID, a.TypeID as RefType, a.date, a.postedDate , a.No , a.Description , a.CreditAmount , a.DebitAmount,''
		FROM [GeneralLedger] a join [AccountingObject] b on a.employeeid = b.id
		 where employeeid in (Select AccountingObjectCode From @tblListAccountingObjectCode)
		 And Account like '511%'
		 and PostedDate >= @FromDate and PostedDate <= @ToDate
	

		Select RefID, RefType, AccountingObjectCode, AccountingObjectName, NgayChungTu, NgayHachToan, SoChungTu, DienGiai, DoanhSo, GiamDoanhThu, GhiChu, 2 as ordercode
		into #tg1
		From @tblSoChiTietDoanhThuTheoNV order by AccountingObjectCode,NgayChungTu

		insert into #tg1 (RefID, RefType, AccountingObjectCode, AccountingObjectName, NgayChungTu, NgayHachToan, SoChungTu, DienGiai, DoanhSo, GiamDoanhThu, GhiChu, ordercode)
		select null, null, AccountingObjectCode, AccountingObjectName, null, null, null, N'Mã nhân viên: ' + AccountingObjectCode + '  -  ' + N'Tên nhân viên: ' + AccountingObjectName, null, null, null, 1
		From @tblSoChiTietDoanhThuTheoNV
		group by AccountingObjectCode, AccountingObjectName

		SELECT TOP 0 *
			into #tg2
			FROM #tg1

		insert into #tg2 (RefID, RefType, AccountingObjectCode, AccountingObjectName, NgayChungTu, NgayHachToan, SoChungTu, DienGiai, DoanhSo, GiamDoanhThu, GhiChu, ordercode)
		select null, null, AccountingObjectCode, AccountingObjectName, null, null, null, N'Cộng nhóm', SUM(DoanhSo), SUM(GiamDoanhThu), null, 3
		From @tblSoChiTietDoanhThuTheoNV
		group by AccountingObjectCode, AccountingObjectName

		INSERT INTO #tg1 SELECT * from #tg2
		SELECT * from #tg1 order by AccountingObjectCode,ordercode,NgayChungTu,NgayHachToan
END


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SoChiTietBanHangTheoNhanVien]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountObjectID AS NVARCHAR(MAX),
	@MaterialsGoodID as NVARCHAR(MAX),
	@EmployeeID as NVARCHAR(MAX)

AS
    BEGIN          
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

		DECLARE @tblListAccountObjectID TABLE
            (
             AccountObjectID UNIQUEIDENTIFIER ,
             AccountObjectGroupList NVARCHAR(MAX)           
            ) 

        INSERT  INTO @tblListAccountObjectID
        SELECT  
			AO.ID
			,AO.AccountObjectGroupID
        FROM AccountingObject AS AO 
        LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') AS TLAO 
			ON AO.ID = TLAO.Value
        WHERE 
		(@AccountObjectID IS NULL OR TLAO.Value IS NOT NULL)


		DECLARE @tblListMaterialsGoodID TABLE
            (
             MaterialsGoodID UNIQUEIDENTIFIER        
            ) 
             
   INSERT  INTO @tblListMaterialsGoodID
         SELECT  TG.id
         FROM    MaterialGoods AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@MaterialsGoodID,',') AS MaterialsGoodID ON TG.ID = MaterialsGoodID.Value
         WHERE  MaterialsGoodID.Value IS NOT NULL

		 DECLARE @tblListEmployeeID TABLE
            (
             EmployeeID UNIQUEIDENTIFIER        
            ) 
             
   INSERT  INTO @tblListEmployeeID
         SELECT  TG.id
         FROM    AccountingObject AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@EmployeeID,',') AS EmployeeID ON TG.ID = EmployeeID.Value
         WHERE  EmployeeID.Value IS NOT NULL

		 

		 DECLARE @tblResult TABLE
            (
			 AccountingObjectID uniqueidentifier,
			 AccountingObjectCode nvarchar(25),
			 AccountingObjectName nvarchar(512),
			 EmployeeID uniqueidentifier,
			 EmployeeCode nvarchar(25),
			 EmployeeName nvarchar(512),
			 SAInvoiceDetail uniqueidentifier,
			 Description nvarchar(512),
			 Date Datetime,
			 No nvarchar(25),
			 DetailID uniqueidentifier,
			 InvoiceNo nvarchar(25),
			 InvoiceDate Datetime,
			 MaterialGoodsID uniqueidentifier,
			 MaterialGoodsCode nvarchar(25),
			 MaterialGoodsName nvarchar(512),
			 Unit nvarchar(25),
			 Quantity decimal(25,10),
			 UnitPrice money,
			 DoanhSoBan money,
			 ChietKhau money,
			 SoLuongTraLai int,
			 GiaTriTraLai money,
			 GiamGia money,
			 Type int,
			 TypeID int,
			 RefID uniqueidentifier
            ) 

			BEGIN
			INSERT INTO @tblResult 
			SELECT DISTINCT GL.AccountingObjectID,null,null,GL.EmployeeID,null,null,SAD.SAInvoiceID,GL.Description,GL.Date,GL.No,GL.DetailID,GL.InvoiceNo,GL.InvoiceDate,SAD.MaterialGoodsID,null,null,SAD.Unit,SAD.Quantity,SAD.UnitPrice,0,0,0,0,0,1,GL.TypeID,GL.ReferenceID
			FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
			WHERE (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is null
			AND GL.PostedDate >=@FromDate 
			AND GL.PostedDate <@ToDate AND GL.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			AND SAD.MaterialGoodsID IN (SELECT * from @tblListMaterialsGoodID) AND GL.EmployeeID in (select * from @tblListEmployeeID)
			AND GL.Account LIKE '511%'
		
			UNION ALL
			SELECT DISTINCT GL.AccountingObjectID,null,null,GL.EmployeeID,null,null,SAD.SAInvoiceID,GL.Description,GL.Date,GL.No,GL.DetailID,SAB.InvoiceNo,SAB.InvoiceDate,SAD.MaterialGoodsID,null,null,SAD.Unit,SAD.Quantity,SAD.UnitPrice,0,0,0,0,0,1,GL.TypeID,GL.ReferenceID
			FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
			LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
			WHERE (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
			AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is not null
			AND GL.PostedDate >=@FromDate 
			AND GL.PostedDate <@ToDate AND GL.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			 and SAD.MaterialGoodsID IN (SELECT * from @tblListMaterialsGoodID) AND GL.EmployeeID in (select * from @tblListEmployeeID)
			 and GL.Account LIKE '511%'


			UPDATE @tblResult SET ChietKhau = a.DA
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description D, DetailID DID
		From @tbDataGL 
		WHERE Account LIKE '511%' and DebitAmount > 0
	) a
	WHERE Type = 1  AND Description = a.D and DetailID = DID
	UPDATE @tblResult SET 	DoanhSoBan = a.CA
	FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description D, DetailID DID
		From @tbDataGL 
		WHERE Account LIKE '511%' and CreditAmount > 0
	) a
	WHERE Type = 1  AND Description = a.D and DetailID = DID
	

			INSERT INTO @tblResult 
			SELECT DISTINCT GL.AccountingObjectID,null,null,GL.EmployeeID,null,null,SAR.SAInvoiceID,GL.Description,GL.Date,GL.No,GL.DetailID,GL.InvoiceNo,GL.InvoiceDate,SAR.MaterialGoodsID,null,null,SAR.Unit,0,SAR.UnitPrice,0,0,SAR.Quantity,0,0,2,GL.TypeID,GL.ReferenceID
			FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
			WHERE (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
			AND (GL.DetailID in (select ID from SAReturnDetail)) AND SA.BillRefID is null
			AND GL.PostedDate >=@FromDate 
			AND GL.PostedDate <@ToDate AND GL.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			AND GL.TypeID IN ('330') and SAR.MaterialGoodsID IN (SELECT * from @tblListMaterialsGoodID) AND GL.EmployeeID IN (SELECT * from @tblListEmployeeID)
			AND Account LIKE '511%'
			UNION ALL
			SELECT DISTINCT GL.AccountingObjectID,null,null,GL.EmployeeID,null,null,SAR.SAInvoiceID,GL.Description,GL.Date,GL.No,GL.DetailID,SAB.InvoiceNo,SAB.InvoiceDate,SAD.MaterialGoodsID,null,null,SAR.Unit,0,SAR.UnitPrice,0,0,SAR.Quantity,0,0,2,GL.TypeID,GL.ReferenceID
			FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
			LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
			WHERE (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
			AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAReturnDetail)) AND SA.BillRefID is not null
			AND GL.PostedDate >=@FromDate 
			AND GL.PostedDate <@ToDate AND GL.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			AND GL.TypeID IN ('330') and SAR.MaterialGoodsID IN (SELECT * from @tblListMaterialsGoodID) AND GL.EmployeeID IN (SELECT * from @tblListEmployeeID)
			AND Account LIKE '511%'
			
			
			UPDATE @tblResult SET ChietKhau = (0 - a.CA)
			FROM (
			Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description D, DetailID DID
			From @tbDataGL
			WHERE  Account LIKE '511%' and TypeID = '330' and CreditAmount >0
			) a
			 WHERE Type = 2 and Description = a.D and DetailID = a.DID
			
			

		UPDATE @tblResult SET GiaTriTraLai = a.DA
		FROM (
		Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount as DA, Account A, AccountCorresponding AC, Description D, DetailID DID
		From @tbDataGL 
		WHERE  Account LIKE '511%' and TypeID = '330' and DebitAmount >0
		) a
		WHERE Type = 2  and Description = a.D and DetailID = a.DID

			 			INSERT INTO @tblResult 
			SELECT DISTINCT GL.AccountingObjectID,null,null,GL.EmployeeID,null,null,SAR.SAInvoiceID,GL.Description,GL.Date,GL.No,GL.DetailID,SAB.InvoiceNo,SAB.InvoiceDate,SAR.MaterialGoodsID,null,null,SAR.Unit,0,SAR.UnitPrice,0,0,0,0,0,3,GL.TypeID,GL.ReferenceID
			FROM @tbDataGL GL 
			LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID 
			LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID 
			LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
			LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
			WHERE GL.PostedDate >=@FromDate 
			AND GL.PostedDate <@ToDate AND GL.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			AND GL.TypeID IN ('340') and SAR.MaterialGoodsID IN (SELECT * from @tblListMaterialsGoodID) AND GL.EmployeeID IN (SELECT * from @tblListEmployeeID)
			AND Account LIKE '511%'
			UNION ALL
			SELECT DISTINCT GL.AccountingObjectID,null,null,GL.EmployeeID,null,null,SAR.SAInvoiceID,GL.Description,GL.Date,GL.No,GL.DetailID,GL.InvoiceNo,GL.InvoiceDate,SAR.MaterialGoodsID,null,null,SAR.Unit,0,SAR.UnitPrice,0,0,0,0,0,3,GL.TypeID,GL.ReferenceID
			FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
			WHERE (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
			AND (GL.DetailID in (select ID from SAReturnDetail)) AND SA.BillRefID is null
			AND GL.PostedDate >=@FromDate 
			AND GL.PostedDate <@ToDate AND GL.AccountingObjectID IN (SELECT AccountObjectID From @tblListAccountObjectID)
			AND GL.TypeID IN ('340') and SAR.MaterialGoodsID IN (SELECT * from @tblListMaterialsGoodID) AND GL.EmployeeID IN (SELECT * from @tblListEmployeeID)
			AND Account LIKE '511%'
			
			
			
			UPDATE @tblResult SET ChietKhau =(0 - a.CA)
			FROM (
			Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description D, DetailID DID
			From @tbDataGL
			WHERE Account LIKE '511%' AND TypeID IN ('340') AND CreditAmount >0
			) a
			 WHERE Type = 3  and Description = a.D and DetailID = a.DID and AccountingObjectID = a.AOI
		
		UPDATE @tblResult SET GiamGia = a.DA
			FROM (
			Select AccountingObjectID as AOI, CreditAmount as CA, DebitAmount DA, Account A, AccountCorresponding AC, Description D, DetailID DID
			From @tbDataGL
			WHERE Account LIKE '511%' AND TypeID IN ('340') AND DebitAmount >0
			) a
			 WHERE Type = 3  and Description = a.D and DetailID = a.DID and AccountingObjectID = a.AOI
			
			UPDATE @tblResult SET AccountingObjectCode = a.AOC, AccountingObjectName = a.AON
			FROM(
			SELECT ID as AOI, AccountingObjectCode as AOC, AccountingObjectName as AON
			from AccountingObject
			) a
			where AccountingObjectID = a.AOI

				UPDATE @tblResult SET MaterialGoodsCode = a.MGC, MaterialGoodsName = a.MGN
			FROM(
			SELECT ID as MGI, MaterialGoodsCode as MGC, MaterialGoodsName as MGN
			from MaterialGoods
			) a
			where MaterialGoodsID = a.MGI

			UPDATE @tblResult SET EmployeeName = a.AON, EmployeeCode = a.AOC
			FROM(
			SELECT ID as AOI, AccountingObjectName as AON, AccountingObjectCode AOC
			from AccountingObject
			) a
			where EmployeeID =a.AOI 
		
	SELECT * From @tblResult order by Date,Type
   END
   END


	
	
	 
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoTheoDoiLaiLoTheoHopDongBan]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountObjectID AS NVARCHAR(MAX)
AS
BEGIN          
	
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
		/*end add by cuongpv*/

	DECLARE @tbAccountObjectID TABLE(
		AccountingObjectID UNIQUEIDENTIFIER
	)
	
	INSERT INTO @tbAccountObjectID
	SELECT tblAccOjectSelect.Value as AccountingObjectID
		FROM dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') tblAccOjectSelect
		WHERE tblAccOjectSelect.Value in (SELECT ID FROM AccountingObject)
		
	
    DECLARE @tbluutru TABLE (
		ContractID UNIQUEIDENTIFIER,
		RefType int,
		NgayKy Date,
		SoHopDong NVARCHAR(50),
		AccountingObjectID UNIQUEIDENTIFIER,
		DoiTuong NVARCHAR(512),
		TrichYeu NVARCHAR(512),
		GiaTriHopDong decimal(25,0),
		DoanhThuThucTe money,
		GiamTruDoanhThu money,
		GiaVon money,
		LaiLo money
    )
    
    INSERT INTO @tbluutru(ContractID,RefType,NgayKy,SoHopDong,AccountingObjectID,DoiTuong,TrichYeu,GiaTriHopDong)
		SELECT EMC.ID as ContractID, EMC.TypeID as RefType,EMC.SignedDate as NgayKy, EMC.Code as SoHopDong, EMC.AccountingObjectID, EMC.AccountingObjectName as DoiTuong,
		EMC.Name as TrichYeu, EMC.Amount as GiaTriHopDong
		FROM EMContract EMC
		WHERE EMC.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
	
	DECLARE @tbDataDoanhThuThucTe TABLE(
		ContractID UNIQUEIDENTIFIER,
		DoanhThuThucTe money
	)
	
	INSERT INTO @tbDataDoanhThuThucTe(ContractID,DoanhThuThucTe)
		SELECT GL.ContractID, SUM(GL.CreditAmount) as DoanhThuThucTe
		FROM @tbDataGL GL 
		WHERE (GL.Account like '511%') AND (GL.PostedDate between @FromDate and @ToDate) 
		AND GL.ContractID in (select ContractID from @tbluutru)
		GROUP BY GL.ContractID
		
	UPDATE @tbluutru SET DoanhThuThucTe = a.DoanhThuThucTe
	FROM (select ContractID as IDC, DoanhThuThucTe from @tbDataDoanhThuThucTe) a
	WHERE ContractID = a.IDC
	
	DECLARE @tbDataGiamTruDoanhThu TABLE(
		ContractID UNIQUEIDENTIFIER,
		GiamTruDoanhThu money
	)
	
	INSERT INTO @tbDataGiamTruDoanhThu(ContractID,GiamTruDoanhThu)
		SELECT GL.ContractID, SUM(GL.DebitAmount) as GiamTruDoanhThu
		FROM @tbDataGL GL 
		WHERE (GL.Account like '511%') AND (GL.PostedDate between @FromDate and @ToDate) 
		AND GL.ContractID in (select ContractID from @tbluutru)
		GROUP BY GL.ContractID
		
	UPDATE @tbluutru SET GiamTruDoanhThu = b.GiamTruDoanhThu
	FROM (select ContractID as IDC, GiamTruDoanhThu from @tbDataGiamTruDoanhThu) b
	WHERE ContractID = b.IDC
	
	DECLARE @tbDataGiaVon TABLE(
		ContractID UNIQUEIDENTIFIER,
		GiaVon money
	)
	
	INSERT INTO @tbDataGiaVon(ContractID,GiaVon)
		SELECT GL.ContractID, SUM(GL.DebitAmount - GL.CreditAmount) as GiaVon
		FROM @tbDataGL GL 
		WHERE (GL.Account like '632%') AND (GL.PostedDate between @FromDate and @ToDate) 
		AND GL.ContractID in (select ContractID from @tbluutru)
		GROUP BY GL.ContractID
		
	UPDATE @tbluutru SET GiaVon = c.GiaVon
	FROM (select ContractID as IDC, GiaVon from @tbDataGiaVon) c
	WHERE ContractID = c.IDC
	
	UPDATE @tbluutru SET LaiLo = ISNULL(DoanhThuThucTe,0) - (ISNULL(GiamTruDoanhThu,0) + ISNULL(GiaVon,0))
	
    SELECT ContractID,RefType,NgayKy, SoHopDong, DoiTuong, TrichYeu, GiaTriHopDong, DoanhThuThucTe, GiamTruDoanhThu, GiaVon, LaiLo FROM @tbluutru
    WHERE ((ISNULL(DoanhThuThucTe,0)>0) OR (ISNULL(GiamTruDoanhThu,0)>0)
			OR (ISNULL(GiaVon,0)>0) OR (ISNULL(LaiLo,0)>0))
	ORDER BY NgayKy, SoHopDong
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoTheoDoiCongNoPhaiThuTheoMatHang]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @MaterialGoodsID AS NVARCHAR(MAX)
AS
BEGIN          
	
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
		/*end add by cuongpv*/

	/*lay cac ma hang co trong bang SAInvoiceDetail va bang SAReturnDetail trong khoang @FromDate den @ToDate */
	DECLARE @tbDataMaterialGoodsID TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER
	)
	INSERT INTO @tbDataMaterialGoodsID
		SELECT SA.MaterialGoodsID from SAInvoiceDetail SA LEFT JOIN @tbDataGL GL on GL.DetailID=SA.ID 
		where GL.PostedDate between @FromDate and @ToDate
	
	INSERT INTO @tbDataMaterialGoodsID
		SELECT SA.MaterialGoodsID from SAReturnDetail SA LEFT JOIN @tbDataGL GL on GL.DetailID=SA.ID 
		where GL.PostedDate between @FromDate and @ToDate
	
    DECLARE @tbluutru TABLE (
		MaterialGoodsID UNIQUEIDENTIFIER,
		MaterialGoodsCode NVARCHAR(25),
		MaterialGoodsName NVARCHAR(512),
		GiaTriHang money,
		Thue money,
		ChietKhau money,
		GiamGia money,
		TraLai money,
		DaThanhToan money,
		ConLai money
    )
    INSERT INTO @tbluutru
		SELECT M.ID as MaterialGoodsID, M.MaterialGoodsCode, M.MaterialGoodsName, null GiaTriHang, null Thue, null ChietKhau, null GiamGia,
				null TraLai, null DaThanhToan, null ConLai
		FROM dbo.Func_ConvertStringIntoTable(@MaterialGoodsID,',') tblMatHangSelect LEFT JOIN dbo.MaterialGoods M ON M.ID=tblMatHangSelect.Value
		WHERE tblMatHangSelect.Value in (select distinct MaterialGoodsID from @tbDataMaterialGoodsID)
		
	DECLARE @tbDataGiaTriHang TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		GiaTriHang money
	)
	
	INSERT INTO @tbDataGiaTriHang 
		SELECT a.MaterialGoodsID,SUM(c.DebitAmount) as GiaTriHang 
		FROM SAInvoiceDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) 
		AND c.Account like '131%' AND (c.AccountCorresponding like '511%' OR c.AccountCorresponding like '711%')AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET GiaTriHang = k.GiaTriHang
	FROM (select MaterialGoodsID as Mid, GiaTriHang from @tbDataGiaTriHang) k
	WHERE MaterialGoodsID = k.Mid
	
	DECLARE @tbDataThue TABLE (
		MaterialGoodsID UNIQUEIDENTIFIER,
		Thue money
	)
	
	INSERT INTO @tbDataThue
		SELECT a.MaterialGoodsID,SUM(c.DebitAmount) as Thue 
		FROM SAInvoiceDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) 
		AND c.Account like '131%' AND c.AccountCorresponding like '3331%' AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET Thue = h.Thue
	FROM (select MaterialGoodsID as Mid, Thue from @tbDataThue) h
	WHERE MaterialGoodsID = h.Mid
	
	DECLARE @tbDataChietKhau TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		ChietKhau money
	)
	
	INSERT INTO @tbDataChietKhau 
		SELECT a.MaterialGoodsID, SUM(c.CreditAmount) as ChietKhau
		FROM SAInvoiceDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) 
		AND c.Account like '131%' AND c.AccountCorresponding like '511%' AND c.TypeID not in (330,340) 
		AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET ChietKhau = i.ChietKhau
	FROM (select MaterialGoodsID as Mid, ChietKhau from @tbDataChietKhau) i
	WHERE MaterialGoodsID = i.Mid
	
	DECLARE @tbDataGiamGia TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		GiamGia money
	)

	INSERT INTO @tbDataGiamGia 
		SELECT a.MaterialGoodsID,SUM(c.CreditAmount - c.DebitAmount) as GiamGia 
		FROM SAReturnDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) 
		AND c.Account like '131%' AND c.TypeID=340
		AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET GiamGia = j.GiamGia
	FROM (select MaterialGoodsID as Mid, GiamGia from @tbDataGiamGia) j
	WHERE MaterialGoodsID = j.Mid
	
	DECLARE @tbDataTraLai TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		TraLai money
	)

	INSERT INTO @tbDataTraLai 
		SELECT a.MaterialGoodsID,SUM(c.CreditAmount - c.DebitAmount) as TraLai 
		FROM SAReturnDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) 
		AND c.Account like '131%' AND c.TypeID=330
		AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET TraLai = n.TraLai
	FROM (select MaterialGoodsID as Mid, TraLai from @tbDataTraLai) n
	WHERE MaterialGoodsID = n.Mid
	
	DECLARE @tbDataDaThanhToan TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		DaThanhToan money
	)
	
	INSERT INTO @tbDataDaThanhToan 
		SELECT a.MaterialGoodsID,SUM(c.CreditAmount) as DaThanhToan 
		FROM SAInvoiceDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) 
		AND c.Account like '131%' AND ((AccountCorresponding like '111%') OR (AccountCorresponding like '112%'))
		AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
		
	UPDATE @tbluutru SET DaThanhToan = m.DaThanhToan
	FROM (select MaterialGoodsID as Mid, DaThanhToan from @tbDataDaThanhToan) m
	WHERE MaterialGoodsID = m.Mid
	
	UPDATE @tbluutru SET ConLai = ((ISNULL(GiaTriHang,0) + ISNULL(Thue,0)) - (ISNULL(ChietKhau,0)+ISNULL(GiamGia,0)+ISNULL(TraLai,0)+ISNULL(DaThanhToan,0)))
	
	select MaterialGoodsID,	MaterialGoodsCode, MaterialGoodsName, GiaTriHang, Thue, ChietKhau, GiamGia, TraLai, DaThanhToan, ConLai
	from @tbluutru WHERE MaterialGoodsID is not null AND ((ISNULL(GiaTriHang,0)>0) OR (ISNULL(Thue,0)>0) OR (ISNULL(ChietKhau,0)>0)
	OR (ISNULL(GiamGia,0)>0) OR (ISNULL(TraLai,0)>0) OR (ISNULL(DaThanhToan,0)>0) OR (ISNULL(ConLai,0)>0))
	order by MaterialGoodsName
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountObjectID AS NVARCHAR(MAX)
AS
BEGIN          
	
	/*Add by cuongpv de sua cach lam tron*/
	DECLARE @tbDataGL TABLE(
		ID uniqueidentifier,
		BranchID uniqueidentifier,
		ReferenceID uniqueidentifier,
		TypeID int,
		Date datetime,
		PostedDate datetime,
		No nvarchar(25),
		InvoiceDate datetime,
		InvoiceNo nvarchar(25),
		Account nvarchar(25),
		AccountCorresponding nvarchar(25),
		BankAccountDetailID uniqueidentifier,
		CurrencyID nvarchar(3),
		ExchangeRate decimal(25, 10),
		DebitAmount decimal(25,0),
		DebitAmountOriginal decimal(25,2),
		CreditAmount decimal(25,0),
		CreditAmountOriginal decimal(25,2),
		Reason nvarchar(512),
		Description nvarchar(512),
		VATDescription nvarchar(512),
		AccountingObjectID uniqueidentifier,
		EmployeeID uniqueidentifier,
		BudgetItemID uniqueidentifier,
		CostSetID uniqueidentifier,
		ContractID uniqueidentifier,
		StatisticsCodeID uniqueidentifier,
		InvoiceSeries nvarchar(25),
		ContactName nvarchar(512),
		DetailID uniqueidentifier,
		RefNo nvarchar(25),
		RefDate datetime,
		DepartmentID uniqueidentifier,
		ExpenseItemID uniqueidentifier,
		OrderPriority int,
		IsIrrationalCost bit
	)

	INSERT INTO @tbDataGL
	SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
	/*end add by cuongpv*/

	DECLARE @tbAccountObjectID TABLE(
		AccountingObjectID UNIQUEIDENTIFIER
	)
	
	INSERT INTO @tbAccountObjectID
	SELECT tblAccOjectSelect.Value as AccountingObjectID
		FROM dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') tblAccOjectSelect
		WHERE tblAccOjectSelect.Value in (SELECT ID FROM AccountingObject)
		
	
    DECLARE @tbluutru TABLE (
		ContractID UNIQUEIDENTIFIER,
		RefType int,
		NgayKy Date,
		SoHopDong NVARCHAR(50),
		AccountingObjectID UNIQUEIDENTIFIER,
		DoiTuong NVARCHAR(512),
		TrichYeu NVARCHAR(512),
		GiaTriHopDong decimal(25,0),
		GiaTriThucTe money,
		CacKhoanGiamTru money,
		DaThanhToan money,
		ConLai money
    )
    
    INSERT INTO @tbluutru(ContractID,RefType,NgayKy,SoHopDong,AccountingObjectID,DoiTuong,TrichYeu,GiaTriHopDong)
		SELECT EMC.ID as ContractID, EMC.TypeID as RefType,EMC.SignedDate as NgayKy, EMC.Code as SoHopDong, EMC.AccountingObjectID, EMC.AccountingObjectName as DoiTuong,
		EMC.Name as TrichYeu, EMC.Amount as GiaTriHopDong
		FROM EMContract EMC
		WHERE EMC.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
	
	DECLARE @tbDataGiaTriThucTe TABLE(
		ContractID UNIQUEIDENTIFIER,
		GiaTriThucTe money
	)
	
	INSERT INTO @tbDataGiaTriThucTe(ContractID,GiaTriThucTe)
		SELECT GL.ContractID, SUM(GL.DebitAmount) as GiaTriThucTe
		FROM @tbDataGL GL 
		WHERE (GL.Account like '131%') AND ((AccountCorresponding like '511%') OR (AccountCorresponding like '3331%'))
		AND (GL.PostedDate between @FromDate and @ToDate) 
		AND GL.ContractID in (select ContractID from @tbluutru)
		GROUP BY GL.ContractID
		
	UPDATE @tbluutru SET GiaTriThucTe = a.GiaTriThucTe
	FROM (select ContractID as IDC, GiaTriThucTe from @tbDataGiaTriThucTe) a
	WHERE ContractID = a.IDC
	
	DECLARE @tbDataCacKhoanGiamTru TABLE(
		ContractID UNIQUEIDENTIFIER,
		CacKhoanGiamTru money
	)
	
	INSERT INTO @tbDataCacKhoanGiamTru(ContractID,CacKhoanGiamTru)
		SELECT GL.ContractID, SUM(GL.CreditAmount) as CacKhoanGiamTru
		FROM @tbDataGL GL 
		WHERE (GL.Account like '131%') AND (AccountCorresponding like '635%')
		AND (GL.PostedDate between @FromDate and @ToDate) 
		AND GL.ContractID in (select ContractID from @tbluutru)
		GROUP BY GL.ContractID
		
	UPDATE @tbluutru SET CacKhoanGiamTru = b.CacKhoanGiamTru
	FROM (select ContractID as IDC, CacKhoanGiamTru from @tbDataCacKhoanGiamTru) b
	WHERE ContractID = b.IDC
	
	DECLARE @tbDataDaThanhToan TABLE(
		ContractID UNIQUEIDENTIFIER,
		DaThanhToan money
	)
	
	INSERT INTO @tbDataDaThanhToan(ContractID,DaThanhToan)
		SELECT GL.ContractID, SUM(GL.CreditAmount) as DaThanhToan
		FROM @tbDataGL GL 
		WHERE (GL.Account like '131%') AND ((AccountCorresponding like '111%') OR (AccountCorresponding like '112%'))
		AND (GL.PostedDate between @FromDate and @ToDate) 
		AND GL.ContractID in (select ContractID from @tbluutru)
		GROUP BY GL.ContractID
		
	UPDATE @tbluutru SET DaThanhToan = c.DaThanhToan
	FROM (select ContractID as IDC, DaThanhToan from @tbDataDaThanhToan) c
	WHERE ContractID = c.IDC
	
	UPDATE @tbluutru SET ConLai = ISNULL(GiaTriThucTe,0) - (ISNULL(CacKhoanGiamTru,0) + ISNULL(DaThanhToan,0))
	
    SELECT ContractID, RefType,NgayKy, SoHopDong, DoiTuong, TrichYeu, GiaTriHopDong, GiaTriThucTe, CacKhoanGiamTru, DaThanhToan, ConLai FROM @tbluutru
    WHERE ((ISNULL(GiaTriThucTe,0)>0) OR (ISNULL(CacKhoanGiamTru,0)>0)
			OR (ISNULL(DaThanhToan,0)>0) OR (ISNULL(ConLai,0)>0))
	ORDER BY NgayKy, SoHopDong
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoTheoDoiCongNoPhaiThuTheoHoaDon]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountObjectID AS NVARCHAR(MAX)
AS
BEGIN          
	
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
		/*end add by cuongpv*/

	DECLARE @tbAccountObjectID TABLE(
		AccountingObjectID UNIQUEIDENTIFIER
	)
	
	INSERT INTO @tbAccountObjectID
	SELECT tblAccOjectSelect.Value as AccountingObjectID
		FROM dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') tblAccOjectSelect
		WHERE tblAccOjectSelect.Value in (SELECT ID FROM AccountingObject)
		
	
    DECLARE @tbluutru TABLE (
		SAInvoiceID UNIQUEIDENTIFIER,
		RefType int,
		RefID UNIQUEIDENTIFIER,
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		TienHangVaCK money,
		TienThue money,
		TraLai money,
		ChietKhauTT_GiamTruKhac money
    )
    
    INSERT INTO @tbluutru(SAInvoiceID,RefType,RefID,DetailID,InvoiceDate,InvoiceNo,AccountingObjectID)
		SELECT DISTINCT SA.ID,GL.TypeID as RefType, GL.ReferenceID as RefID,GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID
		FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		WHERE (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
			AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID))
			AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is null
		UNION ALL
		SELECT DISTINCT SA.ID, GL.TypeID as RefType, GL.ReferenceID as RefID,GL.DetailID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID
		FROM @tbDataGL GL LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=GL.DetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		WHERE (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID))
		AND (GL.DetailID in (select ID from SAInvoiceDetail)) AND SA.BillRefID is not null

    
    DECLARE @tbTienHangVaCK TABLE(
		DetailID UNIQUEIDENTIFIER,
		DebitAmount money,
		CreditAmount money
		)
    
    INSERT INTO @tbTienHangVaCK
    SELECT GL.DetailID, GL.DebitAmount, GL.CreditAmount
    FROM @tbDataGL GL
    WHERE (GL.Account like '131%') AND (AccountCorresponding like '511%'  OR AccountCorresponding like '711%') AND (GL.DetailID in (select DetailID from @tbluutru))
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID))
    
    UPDATE @tbluutru SET TienHangVaCK = a.TienHangVaCK
    FROM (select DetailID as IvID, SUM(DebitAmount - CreditAmount) as TienHangVaCK
		from @tbTienHangVaCK
		group by DetailID) a
	WHERE DetailID = a.IvID


	DECLARE @tbTienThue TABLE (
		DetailID UNIQUEIDENTIFIER,
		DebitAmount money
		)
	
	INSERT INTO @tbTienThue
	SELECT GL.DetailID, GL.DebitAmount
	FROM @tbDataGL GL
	WHERE (GL.Account like '131%') AND (AccountCorresponding like '3331%') AND (GL.DetailID in (select DetailID from @tbluutru))
		  AND (GL.PostedDate between @FromDate and @ToDate) AND GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
	
	UPDATE @tbluutru SET TienThue = b.TienThue
	FROM (select DetailID as IvID, SUM(DebitAmount) as TienThue
		  from @tbTienThue
		  group by DetailID) b
	WHERE DetailID = b.IvID
	
	DECLARE @tbDataTraLai TABLE(
		SAInvoiceDetailID UNIQUEIDENTIFIER,
		TraLai money
	)
	INSERT INTO @tbDataTraLai(SAInvoiceDetailID,TraLai)
		SELECT SAR.SAInvoiceDetailID, SUM(GL.CreditAmount - GL.DebitAmount) as TraLai
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON GL.DetailID = SAR.ID
		WHERE (GL.Account like '131%') AND (GL.TypeID=330) AND (SAR.SAInvoiceDetailID in (select DetailID from @tbluutru))
		AND (GL.PostedDate between @FromDate and @ToDate)
		AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID))
		GROUP BY SAR.SAInvoiceDetailID
	
	UPDATE @tbluutru SET TraLai = TL.TraLai
	FROM (select SAInvoiceDetailID, TraLai from @tbDataTraLai) TL
	WHERE DetailID = TL.SAInvoiceDetailID
	

	DECLARE @tbChietKhauTT_GiamTruKhac TABLE(
		DetailID UNIQUEIDENTIFIER,
		CreditAmount money
	)
	
	INSERT INTO @tbChietKhauTT_GiamTruKhac
	SELECT GL.DetailID, GL.CreditAmount
	FROM @tbDataGL GL
	WHERE (GL.Account like '131%') AND (GL.AccountCorresponding like '635%')
		  AND (GL.TypeID not in (330,340)) AND (GL.DetailID in (select DetailID from @tbluutru))
		  AND (GL.PostedDate between @FromDate and @ToDate) AND GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
	
	UPDATE @tbluutru SET ChietKhauTT_GiamTruKhac = c.ChietKhauTT_GiamTruKhac
	FROM (select DetailID as IvID, /*InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID,*/ SUM(CreditAmount) as ChietKhauTT_GiamTruKhac
		  from @tbChietKhauTT_GiamTruKhac 
		  group by DetailID) c
	WHERE DetailID = c.IvID
	
	
	DECLARE @tbDataReturn TABLE (
		SAInvoiceID UNIQUEIDENTIFIER,
		RefType int,
		RefID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		AccountingObjectName NVARCHAR(512),
		TienHangVaCK money,
		TienThue money,
		GiaTriHoaDon money,
		TraLai money,
		GiamGia money,
		ChietKhauTT_GiamTruKhac money,
		SoDaThu money,
		SoConPhaiThu money
    )
    
    INSERT INTO @tbDataReturn(SAInvoiceID,RefType,RefID,InvoiceDate,InvoiceNo,AccountingObjectID,TienHangVaCK,TienThue,TraLai,ChietKhauTT_GiamTruKhac)
    SELECT SAInvoiceID,RefType,RefID,InvoiceDate,InvoiceNo,AccountingObjectID,SUM(TienHangVaCK) as TienHangVaCK, SUM(TienThue) as TienThue,
    SUM(TraLai) as TraLai, SUM(ChietKhauTT_GiamTruKhac) as ChietKhauTT_GiamTruKhac
    FROM @tbluutru
    GROUP BY SAInvoiceID,RefType,RefID,InvoiceDate,InvoiceNo,AccountingObjectID
    
	INSERT INTO @tbDataReturn(SAInvoiceID,RefType,RefID,InvoiceDate,InvoiceNo,AccountingObjectID,GiamGia)
		SELECT SA.ID, GL.TypeID as RefType, GL.ReferenceID as RefID,GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, SUM(GL.CreditAmount - GL.DebitAmount) as GiamGia
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		WHERE (GL.Account like '131%') AND (GL.TypeID=340) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAReturnDetail)) 
		AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)) AND SA.BillRefID is null
		GROUP BY SA.ID,GL.TypeID, GL.ReferenceID,GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID
		UNION ALL
		SELECT SA.ID,GL.TypeID as RefType, GL.ReferenceID as RefID, SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID, SUM(GL.CreditAmount - GL.DebitAmount) as GiamGia
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON SAR.ID=GL.DetailID LEFT JOIN SAInvoiceDetail SAD ON SAD.ID=SAR.SAInvoiceDetailID LEFT JOIN SAInvoice SA ON SA.ID = SAD.SAInvoiceID
		LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		WHERE (GL.Account like '131%') AND (GL.TypeID=340) AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail))
		AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)) AND SA.BillRefID is not null
		GROUP BY SA.ID, GL.TypeID, GL.ReferenceID ,SAB.InvoiceDate, SAB.InvoiceNo, GL.AccountingObjectID
		
	DECLARE @tbDataSoDaThu TABLE (
		SAInvoiceID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		SoDaThu money
    )

	INSERT INTO @tbDataSoDaThu
		/*lay da thu cua cac hoa don co BillRefID is null trong SAInvoice*/
		select SA.ID, SA.InvoiceDate, SA.InvoiceNo, SA.AccountingObjectID, SUM(MCC.Amount) as SoDaThu
		from @tbDataGL GL LEFT JOIN MCReceipt MC ON GL.ReferenceID = MC.ID
		LEFT JOIN MCReceiptDetailCustomer MCC ON MCC.MCReceiptID = MC.ID
		LEFT JOIN SAInvoice SA ON SA.ID = MCC.SaleInvoiceID
		where (GL.Account like '131%') AND ((GL.AccountCorresponding like '111%') OR (GL.AccountCorresponding like '112%')) 
		AND (SA.InvoiceDate is not null) AND (SA.InvoiceNo is not null) AND (SA.InvoiceNo <> '') 
		AND (GL.PostedDate between @FromDate and @ToDate) AND SA.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID) AND SA.BillRefID is null
		group by SA.ID, SA.InvoiceDate, SA.InvoiceNo, SA.AccountingObjectID
		  
		UNION ALL
		  
		select SA.ID, SA.InvoiceDate, SA.InvoiceNo, SA.AccountingObjectID, SUM(MBC.Amount) as SoDaThu
		from @tbDataGL GL LEFT JOIN MBDeposit MB ON GL.ReferenceID = MB.ID
		LEFT JOIN MBDepositDetailCustomer MBC ON MBC.MBDepositID = MB.ID
		LEFT JOIN SAInvoice SA ON SA.ID = MBC.SaleInvoiceID
		where (GL.Account like '131%') AND ((GL.AccountCorresponding like '111%') OR (GL.AccountCorresponding like '112%')) 
		AND (SA.InvoiceDate is not null) AND (SA.InvoiceNo is not null) AND (SA.InvoiceNo <> '') 
		AND (GL.PostedDate between @FromDate and @ToDate) AND SA.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID) AND SA.BillRefID is null
		group by SA.ID, SA.InvoiceDate, SA.InvoiceNo, SA.AccountingObjectID

		UNION ALL
		/*lay da thu cua cac hoa don co BillRefID is not null trong SAInvoice*/
		select SA.ID, SAB.InvoiceDate, SAB.InvoiceNo, SA.AccountingObjectID, SUM(MCC.Amount) as SoDaThu
		from @tbDataGL GL LEFT JOIN MCReceipt MC ON GL.ReferenceID = MC.ID
		LEFT JOIN MCReceiptDetailCustomer MCC ON MCC.MCReceiptID = MC.ID
		LEFT JOIN SAInvoice SA ON SA.ID = MCC.SaleInvoiceID
		LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		where (GL.Account like '131%') AND ((GL.AccountCorresponding like '111%') OR (GL.AccountCorresponding like '112%')) 
		AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '') 
		AND (GL.PostedDate between @FromDate and @ToDate) AND SA.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID) AND SA.BillRefID is not null
		group by SA.ID, SAB.InvoiceDate, SAB.InvoiceNo, SA.AccountingObjectID
		  
		UNION ALL
		  
		select SA.ID, SAB.InvoiceDate, SAB.InvoiceNo, SA.AccountingObjectID, SUM(MBC.Amount) as SoDaThu
		from @tbDataGL GL LEFT JOIN MBDeposit MB ON GL.ReferenceID = MB.ID
		LEFT JOIN MBDepositDetailCustomer MBC ON MBC.MBDepositID = MB.ID
		LEFT JOIN SAInvoice SA ON SA.ID = MBC.SaleInvoiceID
		LEFT JOIN SABill SAB ON SAB.ID=SA.BillRefID
		where (GL.Account like '131%') AND ((GL.AccountCorresponding like '111%') OR (GL.AccountCorresponding like '112%')) 
		AND (SAB.InvoiceDate is not null) AND (SAB.InvoiceNo is not null) AND (SAB.InvoiceNo <> '') 
		AND (GL.PostedDate between @FromDate and @ToDate) AND SA.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID) AND SA.BillRefID is not null
		group by SA.ID, SAB.InvoiceDate, SAB.InvoiceNo, SA.AccountingObjectID

	UPDATE @tbDataReturn SET SoDaThu = d.SoDaThu
	FROM (
		Select SAInvoiceID as SAId, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, SoDaThu
		From @tbDataSoDaThu
		  ) d
	WHERE SAInvoiceID = SAId AND InvoiceDate = d.IvDate AND InvoiceNo = d.IvNo AND AccountingObjectID = d.AOID
    

    UPDATE @tbDataReturn SET AccountingObjectName = AJ.AccountingObjectName
	FROM ( select ID,AccountingObjectName from AccountingObject) AJ
	where AccountingObjectID = AJ.ID
    
    UPDATE @tbDataReturn SET GiaTriHoaDon = (ISNULL(TienHangVaCK,0) + ISNULL(TienThue,0))
    
    UPDATE @tbDataReturn SET SoConPhaiThu = (ISNULL(GiaTriHoaDon,0) - (ISNULL(TraLai,0)+ISNULL(GiamGia,0)+ISNULL(ChietKhauTT_GiamTruKhac,0)+ISNULL(SoDaThu,0)))
    
    SELECT SAInvoiceID, RefType, RefID,InvoiceDate,InvoiceNo,AccountingObjectName,GiaTriHoaDon,TraLai,GiamGia,ChietKhauTT_GiamTruKhac,SoDaThu,SoConPhaiThu FROM @tbDataReturn
    WHERE ((ISNULL(GiaTriHoaDon,0)>0) OR (ISNULL(TraLai,0)>0) OR (ISNULL(GiamGia,0)>0) OR (ISNULL(ChietKhauTT_GiamTruKhac,0)>0)
			OR (ISNULL(SoDaThu,0)>0) OR (ISNULL(SoConPhaiThu,0)>0)) AND RefType in (220, 320, 321, 322, 323, 324, 325, 326, 340)
	ORDER BY InvoiceDate,InvoiceNo
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoCongCuDungCu]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @ToolsIDs AS NVARCHAR(MAX)
AS
BEGIN          
    DECLARE @tbluutru TABLE (
		ToolsID UNIQUEIDENTIFIER,
		ToolsCode NVARCHAR(25),
		ToolsName NVARCHAR(255),
		IncrementDate Date,
		PostedDate Date,
		Amount decimal(19,4),
		AllocationTimes int,
		AllocatedAmount decimal(19,4),
		DecrementAllocationTime int,
		RemainingAllocationTime int,
		DecrementAmount decimal(19,4),
		RemainingAmount decimal(19,4)
    )
    INSERT INTO @tbluutru(ToolsID, ToolsCode, ToolsName, Amount, AllocationTimes, AllocatedAmount)
		SELECT T.ID as ToolsID, T.ToolsCode, T.ToolsName, T.Amount, T.AllocationTimes, T.AllocatedAmount
		FROM dbo.Func_ConvertStringIntoTable(@ToolsIDs,',') tblCodeSelect LEFT JOIN dbo.TIInit T ON T.ID = tblCodeSelect.Value
		WHERE tblCodeSelect.Value is not null AND T.ID in (select DISTINCT ToolsID from ToolLedger where PostedDate <= @ToDate)
	
	

	UPDATE @tbluutru SET IncrementDate = a.IncrementDate
	FROM (Select DISTINCT ToolsID as Tid, IncrementDate From ToolLedger where ToolsID in (select ToolsID from @tbluutru) and (PostedDate <= @ToDate) and (IncrementDate is not null)) a
	WHERE ToolsID = a.Tid
	
	

	UPDATE @tbluutru SET PostedDate = b.PostedDate
	FROM (Select DISTINCT ToolsID as Tid, PostedDate From ToolLedger 
		  where ToolsID in (select ToolsID from @tbluutru) And TypeID = 431 and (PostedDate <= @ToDate) and (PostedDate is not null)) b
	WHERE ToolsID = b.Tid
	
	UPDATE @tbluutru SET DecrementAllocationTime = ISNULL(c.skdapb,0)
	FROM (Select DISTINCT ToolsID as Tid, SUM(DecrementAllocationTime) as skdapb From ToolLedger 
		  where ToolsID in (select ToolsID from @tbluutru) and (PostedDate <= @ToDate) and TypeID <> 431
		  group by ToolsID) c
	WHERE ToolsID = c.Tid
	
	DECLARE @id UNIQUEIDENTIFIER
	
	DECLARE cursorConLai CURSOR FOR
	SELECT ToolsID FROM @tbluutru
	OPEN cursorConLai
	FETCH NEXT FROM cursorConLai INTO @id
	WHILE @@FETCH_STATUS = 0
	BEGIN
		UPDATE @tbluutru SET RemainingAmount = d.RemainingAmount
		FROM (select Top 1 ToolsID as Tid, RemainingAmount from ToolLedger where ToolsID = @id and (PostedDate <= @ToDate) order by PostedDate DESC) d
		WHERE ToolsID = d.Tid
		FETCH NEXT FROM cursorConLai INTO @id
	END
	CLOSE cursorConLai
	DEALLOCATE cursorConLai
	
	UPDATE @tbluutru SET RemainingAllocationTime = ISNULL(AllocationTimes,0) - ISNULL(DecrementAllocationTime,0),
						 DecrementAmount = ISNULL(Amount,0) - ISNULL(RemainingAmount,0)
	
	select * from @tbluutru
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_ReceivableDetail]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3) ,
    @IsSimilarSum BIT 
AS
    BEGIN
		
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        DECLARE @ID UNIQUEIDENTIFIER
        SET @ID = '00000000-0000-0000-0000-000000000000'
               
        CREATE TABLE #tblListAccountObjectID  
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25)
                 ,
              AccountObjectName NVARCHAR(512)
                 ,
              AccountObjectAddress NVARCHAR(512)
                 ,
              AccountObjectGroupListCode NVARCHAR(MAX)
                
                NULL ,
              AccountObjectGroupListName NVARCHAR(MAX)
                
                NULL ,
              CompanyTaxCode NVARCHAR(50) 
                                          NULL
            ) 
        INSERT  INTO #tblListAccountObjectID
                SELECT  AO.ID ,
                        AccountingObjectCode ,
                        AccountingObjectName ,
                        [Address] ,
                        AOG.AccountingObjectGroupCode , 
                        AOG.AccountingObjectGroupName , 
                        CASE WHEN AO.ObjectType = 0 THEN TaxCode
                             ELSE ''
                        END AS CompanyTaxCode 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                           ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value 
                        left JOIN dbo.AccountingObjectGroup AOG ON AO.AccountObjectGroupID = AOG.ID 
          
        CREATE TABLE #tblResult
            (
              RowNum INT PRIMARY KEY
                         IDENTITY(1, 1) ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100)
                
                NULL ,
              AccountObjectName NVARCHAR(512)
                
                NULL ,
              AccountObjectGroupListCode NVARCHAR(MAX)
                
                NULL , 
              AccountObjectGroupListName NVARCHAR(MAX)
                
                NULL ,          
              AccountObjectAddress NVARCHAR(512)
                
                NULL , 
              CompanyTaxCode NVARCHAR(50) 
                                          NULL ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25) 
                                 NULL ,
              InvDate DATETIME ,
              InvNo NVARCHAR(MAX) 
                                  NULL ,
              
              RefType INT ,
              RefID UNIQUEIDENTIFIER ,                        
              RefDetailID NVARCHAR(50) 
                                       NULL ,
              JournalMemo NVARCHAR(512) 
                                        NULL ,
              AccountNumber NVARCHAR(20) 
                                         NULL ,
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20)
                
                NULL ,
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,
              DebitAmount MONEY , 
              CreditAmountOC MONEY ,
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY , 
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50)
                 , 
              InventoryItemName NVARCHAR(512)
                 ,
              UnitName NVARCHAR(20) 
                                    NULL ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(20, 6) ,            
              IsBold BIT , 
              OrderType INT ,
              BranchName NVARCHAR(512) 
                                       NULL ,
              GLJournalMemo NVARCHAR(512) 
                                          NULL 
              ,
              MasterCustomField1 NVARCHAR(255)
                
                NULL ,
              MasterCustomField2 NVARCHAR(512)
                
                NULL ,
              MasterCustomField3 NVARCHAR(512)
                
                NULL ,
              MasterCustomField4 NVARCHAR(512)
                
                NULL ,
              MasterCustomField5 NVARCHAR(512)
                
                NULL ,
              MasterCustomField6 NVARCHAR(512)
                
                NULL ,
              MasterCustomField7 NVARCHAR(512)
                
                NULL ,
              MasterCustomField8 NVARCHAR(512)
                
                NULL ,
              MasterCustomField9 NVARCHAR(512)
                
                NULL ,
              MasterCustomField10 NVARCHAR(512)
                
                NULL ,
              CustomField1 NVARCHAR(512) 
                                         NULL ,
              CustomField2 NVARCHAR(512) 
                                         NULL ,
              CustomField3 NVARCHAR(512) 
                                         NULL ,
              CustomField4 NVARCHAR(512) 
                                         NULL ,
              CustomField5 NVARCHAR(512) 
                                         NULL ,
              CustomField6 NVARCHAR(512) 
                                         NULL ,
              CustomField7 NVARCHAR(512) 
                                         NULL ,
              CustomField8 NVARCHAR(512) 
                                         NULL ,
              CustomField9 NVARCHAR(512) 
                                         NULL ,
              CustomField10 NVARCHAR(512) 
                                          NULL ,
              /*cột số lượng, đơn giá*/
              MainUnitName NVARCHAR(255) 
                                         NULL ,
              MainUnitPrice DECIMAL(20, 6) ,
              MainQuantity DECIMAL(22, 8) ,
              /*mã thống kê, và tên loại chứng từ*/
              ListItemCode NVARCHAR(20) ,
              ListItemName NVARCHAR(128) ,
              RefTypeName NVARCHAR(128),
			  OrderPriority int
            )
        
        CREATE TABLE #tblAccountNumber
            (
              AccountNumber NVARCHAR(20) 
                                         PRIMARY KEY ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL
            BEGIN
			INSERT  INTO #tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
            END
        ELSE
            BEGIN 
			   INSERT  INTO #tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   DetailType = '1'
                                AND Grade = 1
                                AND IsParentNode = 0
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END 

/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
		
		IF(@CurrencyID = 'VND')
		BEGIN
		IF @IsSimilarSum = 0 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,  
                          AccountObjectName ,	
                          AccountObjectGroupListCode ,
                          AccountObjectGroupListName ,        
                          AccountObjectAddress , 
                          CompanyTaxCode , 
                          PostedDate ,
                          RefDate , 
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,	
                          RefID ,  
                          RefDetailID ,
                          JournalMemo , 				  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,			  
                          ExchangeRate , 		  
                          DebitAmountOC ,
                          DebitAmount , 
                          CreditAmountOC , 
                          CreditAmount , 
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,	
                          ClosingCreditAmount ,               
                          IsBold , 
                          OrderType,
						  OrderPriority
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode , 
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,           
                                AccountObjectAddress ,
                                CompanyTaxCode , 
                                PostedDate ,
                                RefDate ,
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID ,
                                RefDetailID ,
                                JournalMemo , 				  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  
                                ExchangeRate ,  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) ,
                                SUM(CreditAmount) ,
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN SUM(ClosingDebitAmountOC)
                                            - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) ) > 0
                                                 THEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,         
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( SUM(ClosingDebitAmount)
                                              - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount)
                                                        - SUM(ClosingCreditAmount) ) > 0
                                                 THEN SUM(ClosingDebitAmount)
                                                      - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmountOC)
                                              - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) ) > 0
                                                 THEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmount)
                                              - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) ) > 0
                                                 THEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,
                                
                                IsBold , 
                                OrderType ,
								OrderPriority

                                
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,	
                                            LAOI.AccountObjectGroupListCode ,
                                            LAOI.AccountObjectGroupListName ,          
                                            LAOI.AccountObjectAddress , 
                                            LAOI.CompanyTaxCode , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,             
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefDate
                                            END AS RefDate , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefNo
                                            END AS RefNo , 
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN N'Số dư đầu kỳ'
                                                 ELSE ISNULL(AOL.DESCRIPTION,
                                                             AOL.Reason)
                                            END AS JournalMemo ,   
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber ,
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ExchangeRate
                                            END AS ExchangeRate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                               
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                     
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC ,           
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 1
                                                 ELSE 0
                                            END AS IsBold ,	
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType ,
											 CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN null
                                                 ELSE OrderPriority
                                            END AS OrderPriority 
                                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                            INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                            INNER JOIN #tblListAccountObjectID
                                            AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                  WHERE     AOL.PostedDate <= @ToDate
     
                                ) AS RSNS
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode , 
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,        
                                RSNS.AccountObjectAddress ,
                                CompanyTaxCode , 
                                RSNS.PostedDate ,
                                RSNS.RefDate ,
                                RSNS.RefNo , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,   
                                RSNS.RefDetailID ,
                                RSNS.JournalMemo , 				  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber , 		  
                                RSNS.ExchangeRate ,           
                                RSNS.IsBold ,
                                RSNS.OrderType,
								RSNS.OrderPriority
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC
                                       - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount
                                       - ClosingCreditAmount) <> 0
                        ORDER BY RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.OrderType ,
                                RSNS.PostedDate ,
								RSNS.OrderPriority,
                                RSNS.RefDate ,
                                RSNS.RefNo 
            END
        ELSE 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,   
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,            
                          AccountObjectAddress ,
                          CompanyTaxCode ,
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID ,   
                          RefDetailID ,
                          JournalMemo , 			  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,		  
                          ExchangeRate ,		  
                          DebitAmountOC ,
                          DebitAmount ,
                          CreditAmountOC , 
                          CreditAmount ,
                          ClosingDebitAmountOC ,
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount ,                                    
                          IsBold , 
                          OrderType,
						  OrderPriority
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,  
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,             
                                AccountObjectAddress , 
                                CompanyTaxCode ,
                                PostedDate ,	
                                RefDate , 
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID , 
                                RefDetailID ,
                                JournalMemo ,				  
                                AccountNumber ,
                                AccountCategoryKind ,
                                CorrespondingAccountNumber ,			  
                                ExchangeRate , 	  
                                DebitAmountOC ,
                                DebitAmount ,
                                CreditAmountOC ,
                                CreditAmount , 
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmountOC
                                              - ClosingCreditAmountOC )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmountOC
                                                        - ClosingCreditAmountOC ) > 0
                                                 THEN ClosingDebitAmountOC
                                                      - ClosingCreditAmountOC
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,  
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmount
                                              - ClosingCreditAmount )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmount
                                                        - ClosingCreditAmount ) > 0
                                                 THEN ClosingDebitAmount
                                                      - ClosingCreditAmount
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmountOC
                                              - ClosingDebitAmountOC )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC ) > 0
                                                 THEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmount
                                              - ClosingDebitAmount )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount ) > 0
                                                 THEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,                                     
                                IsBold , 
                                OrderType,
								OrderPriority
                        FROM    ( SELECT    AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode ,
                                            AD.AccountObjectGroupListName ,            
                                            AD.AccountObjectAddress ,
                                            CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType , 
                                            MAX(AD.RefDetailID) AS RefDetailID ,
                                            AD.JournalMemo ,   
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber , 
                                            AD.ExchangeRate , 
                                            SUM(AD.DebitAmountOC) AS DebitAmountOC ,                              
                                            SUM(AD.DebitAmount) AS DebitAmount ,                                        
                                            SUM(AD.CreditAmountOC) AS CreditAmountOC , 
                                            SUM(AD.CreditAmount) AS CreditAmount ,
                                            SUM(AD.ClosingDebitAmountOC) AS ClosingDebitAmountOC ,       
                                            SUM(AD.ClosingDebitAmount) AS ClosingDebitAmount ,
                                            SUM(AD.ClosingCreditAmountOC) AS ClosingCreditAmountOC , 
                                            SUM(AD.ClosingCreditAmount) AS ClosingCreditAmount ,
                                            AD.IsBold ,	
                                            AD.OrderType ,
											AD.OrderPriority
											
                                  FROM      ( SELECT    AOL.AccountingObjectID ,
                                                        LAOI.AccountObjectCode ,   
                                                        LAOI.AccountObjectName ,	
                                                        LAOI.AccountObjectGroupListCode ,
                                                        LAOI.AccountObjectGroupListName ,              
                                                        LAOI.AccountObjectAddress , 
                                                        LAOI.CompanyTaxCode , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.PostedDate
                                                        END AS PostedDate ,             
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefDate
                                                        END AS RefDate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefNo
                                                        END AS RefNo , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceDate
                                                        END AS InvDate ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceNo
                                                        END AS InvNo ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.ReferenceID
                                                        END AS RefID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.TypeID
                                                        END AS RefType ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE CAST(DetailID AS NVARCHAR(50))
                                                        END AS RefDetailID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN N'Số dư đầu kỳ'
                                                             ELSE AOL.Reason
                                                        END AS JournalMemo , 
                                                        TBAN.AccountNumber ,
                                                        TBAN.AccountCategoryKind ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.AccountCorresponding
                                                        END AS CorrespondingAccountNumber , 
                                                        
                                                        CASE WHEN AOL.PostedDate < @FromDate
															 THEN NULL
															 ELSE AOL.ExchangeRate
														END AS ExchangeRate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmountOriginal
                                                        END AS DebitAmountOC ,                         
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmount
                                                        END AS DebitAmount ,                           
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmountOriginal
                                                        END AS CreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmount
                                                        END AS CreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingDebitAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmount
                                                             ELSE $0
                                                        END AS ClosingDebitAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingCreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmount
                                                             ELSE $0
                                                        END AS ClosingCreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 1
                                                             ELSE 0
                                                        END AS IsBold ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 0
                                                             ELSE 1
                                                        END AS OrderType ,
                                                           CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 0
                                                             ELSE OrderPriority
                                                        END AS OrderPriority 
                                              FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                                        INNER JOIN #tblListAccountObjectID
                                                        AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                                        INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                              WHERE     AOL.PostedDate <= @ToDate
                                            ) AS AD
                                  GROUP BY  AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode , 
                                            AD.AccountObjectGroupListName ,     
                                            AD.AccountObjectAddress ,
                                            AD.CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType ,
                                            AD.JournalMemo ,
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber ,  
                                            AD.ExchangeRate ,
                                            AD.IsBold , 	
                                            AD.OrderType,
											AD.OrderPriority
                                ) AS RSS
                        WHERE   RSS.DebitAmountOC <> 0
                                OR RSS.CreditAmountOC <> 0
                                OR RSS.DebitAmount <> 0
                                OR RSS.CreditAmount <> 0
                                OR ClosingCreditAmountOC
                                - ClosingDebitAmountOC <> 0
                                OR ClosingCreditAmount - ClosingDebitAmount <> 0
                        ORDER BY RSS.AccountObjectCode ,
                                RSS.AccountNumber ,
                                RSS.OrderType ,
                                RSS.PostedDate ,
								RSS.OrderPriority,
                                RSS.RefDate ,
                                RSS.RefNo ,
                                RSS.InvDate ,
                                RSS.InvNo
            END
		END
		ELSE
		BEGIN
        IF @IsSimilarSum = 0 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,  
                          AccountObjectName ,	
                          AccountObjectGroupListCode ,
                          AccountObjectGroupListName ,        
                          AccountObjectAddress , 
                          CompanyTaxCode , 
                          PostedDate ,
                          RefDate , 
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,	
                          RefID ,  
                          RefDetailID ,
                          JournalMemo , 				  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,			  
                          ExchangeRate , 		  
                          DebitAmountOC ,
                          DebitAmount , 
                          CreditAmountOC , 
                          CreditAmount , 
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,	
                          ClosingCreditAmount ,               
                          IsBold , 
                          OrderType,
						  OrderPriority
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode , 
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,           
                                AccountObjectAddress ,
                                CompanyTaxCode , 
                                PostedDate ,
                                RefDate ,
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID ,
                                RefDetailID ,
                                JournalMemo , 				  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  
                                ExchangeRate ,  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) ,
                                SUM(CreditAmount) ,
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN SUM(ClosingDebitAmountOC)
                                            - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) ) > 0
                                                 THEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,         
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( SUM(ClosingDebitAmount)
                                              - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount)
                                                        - SUM(ClosingCreditAmount) ) > 0
                                                 THEN SUM(ClosingDebitAmount)
                                                      - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmountOC)
                                              - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) ) > 0
                                                 THEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmount)
                                              - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) ) > 0
                                                 THEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,
                                
                                IsBold , 
                                OrderType ,
								OrderPriority

                                
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,	
                                            LAOI.AccountObjectGroupListCode ,
                                            LAOI.AccountObjectGroupListName ,          
                                            LAOI.AccountObjectAddress , 
                                            LAOI.CompanyTaxCode , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,             
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefDate
                                            END AS RefDate , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefNo
                                            END AS RefNo , 
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN N'Số dư đầu kỳ'
                                                 ELSE ISNULL(AOL.DESCRIPTION,
                                                             AOL.Reason)
                                            END AS JournalMemo ,   
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber ,
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ExchangeRate
                                            END AS ExchangeRate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                               
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmount ,                                     
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC ,           
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 1
                                                 ELSE 0
                                            END AS IsBold ,	
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType ,
											 CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN null
                                                 ELSE OrderPriority
                                            END AS OrderPriority 
                                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                            INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                            INNER JOIN #tblListAccountObjectID
                                            AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )

                                ) AS RSNS
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode , 
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,        
                                RSNS.AccountObjectAddress ,
                                CompanyTaxCode , 
                                RSNS.PostedDate ,
                                RSNS.RefDate ,
                                RSNS.RefNo , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,   
                                RSNS.RefDetailID ,
                                RSNS.JournalMemo , 				  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber , 		  
                                RSNS.ExchangeRate ,           
                                RSNS.IsBold ,
                                RSNS.OrderType,
								RSNS.OrderPriority
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC
                                       - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount
                                       - ClosingCreditAmount) <> 0
                        ORDER BY RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.OrderType ,
                                RSNS.PostedDate ,
								RSNS.OrderPriority,
                                RSNS.RefDate ,
                                RSNS.RefNo 
            END
        ELSE 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,   
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,            
                          AccountObjectAddress ,
                          CompanyTaxCode ,
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID ,   
                          RefDetailID ,
                          JournalMemo , 			  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,		  
                          ExchangeRate ,		  
                          DebitAmountOC ,
                          DebitAmount ,
                          CreditAmountOC , 
                          CreditAmount ,
                          ClosingDebitAmountOC ,
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount ,                                    
                          IsBold , 
                          OrderType,
						  OrderPriority
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,  
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,             
                                AccountObjectAddress , 
                                CompanyTaxCode ,
                                PostedDate ,	
                                RefDate , 
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID , 
                                RefDetailID ,
                                JournalMemo ,				  
                                AccountNumber ,
                                AccountCategoryKind ,
                                CorrespondingAccountNumber ,			  
                                ExchangeRate , 	  
                                DebitAmountOC ,
                                DebitAmount ,
                                CreditAmountOC ,
                                CreditAmount , 
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmountOC
                                              - ClosingCreditAmountOC )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmountOC
                                                        - ClosingCreditAmountOC ) > 0
                                                 THEN ClosingDebitAmountOC
                                                      - ClosingCreditAmountOC
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,  
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmount
                                              - ClosingCreditAmount )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmount
                                                        - ClosingCreditAmount ) > 0
                                                 THEN ClosingDebitAmount
                                                      - ClosingCreditAmount
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmountOC
                                              - ClosingDebitAmountOC )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC ) > 0
                                                 THEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmount
                                              - ClosingDebitAmount )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount ) > 0
                                                 THEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,                                     
                                IsBold , 
                                OrderType,
								OrderPriority
                        FROM    ( SELECT    AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode ,
                                            AD.AccountObjectGroupListName ,            
                                            AD.AccountObjectAddress ,
                                            CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType , 
                                            MAX(AD.RefDetailID) AS RefDetailID ,
                                            AD.JournalMemo ,   
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber , 
                                            AD.ExchangeRate , 
                                            SUM(AD.DebitAmountOC) AS DebitAmountOC ,                              
                                            SUM(AD.DebitAmount) AS DebitAmount ,                                        
                                            SUM(AD.CreditAmountOC) AS CreditAmountOC , 
                                            SUM(AD.CreditAmount) AS CreditAmount ,
                                            SUM(AD.ClosingDebitAmountOC) AS ClosingDebitAmountOC ,       
                                            SUM(AD.ClosingDebitAmount) AS ClosingDebitAmount ,
                                            SUM(AD.ClosingCreditAmountOC) AS ClosingCreditAmountOC , 
                                            SUM(AD.ClosingCreditAmount) AS ClosingCreditAmount ,
                                            AD.IsBold ,	
                                            AD.OrderType ,
											AD.OrderPriority
											
                                  FROM      ( SELECT    AOL.AccountingObjectID ,
                                                        LAOI.AccountObjectCode ,   
                                                        LAOI.AccountObjectName ,	
                                                        LAOI.AccountObjectGroupListCode ,
                                                        LAOI.AccountObjectGroupListName ,              
                                                        LAOI.AccountObjectAddress , 
                                                        LAOI.CompanyTaxCode , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.PostedDate
                                                        END AS PostedDate ,             
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefDate
                                                        END AS RefDate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefNo
                                                        END AS RefNo , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceDate
                                                        END AS InvDate ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceNo
                                                        END AS InvNo ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.ReferenceID
                                                        END AS RefID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.TypeID
                                                        END AS RefType ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE CAST(DetailID AS NVARCHAR(50))
                                                        END AS RefDetailID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN N'Số dư đầu kỳ'
                                                             ELSE AOL.Reason
                                                        END AS JournalMemo , 
                                                        TBAN.AccountNumber ,
                                                        TBAN.AccountCategoryKind ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.AccountCorresponding
                                                        END AS CorrespondingAccountNumber , 
                                                        
                                                        CASE WHEN AOL.PostedDate < @FromDate
															 THEN NULL
															 ELSE AOL.ExchangeRate
														END AS ExchangeRate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmountOriginal
                                                        END AS DebitAmountOC ,                         
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmountOriginal
                                                        END AS DebitAmount ,                           
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmountOriginal
                                                        END AS CreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmountOriginal
                                                        END AS CreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingDebitAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingDebitAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingCreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingCreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 1
                                                             ELSE 0
                                                        END AS IsBold ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 0
                                                             ELSE 1
                                                        END AS OrderType ,
                                                           CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 0
                                                             ELSE OrderPriority
                                                        END AS OrderPriority 
                                              FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                                        INNER JOIN #tblListAccountObjectID
                                                        AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                                        INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                              WHERE     AOL.PostedDate <= @ToDate
                                                        AND ( @CurrencyID IS NULL
                                                              OR AOL.CurrencyID = @CurrencyID
                                                            )
                                            ) AS AD
                                  GROUP BY  AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode , 
                                            AD.AccountObjectGroupListName ,     
                                            AD.AccountObjectAddress ,
                                            AD.CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType ,
                                            AD.JournalMemo ,
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber ,  
                                            AD.ExchangeRate ,
                                            AD.IsBold , 	
                                            AD.OrderType,
											AD.OrderPriority
                                ) AS RSS
                        WHERE   RSS.DebitAmountOC <> 0
                                OR RSS.CreditAmountOC <> 0
                                OR RSS.DebitAmount <> 0
                                OR RSS.CreditAmount <> 0
                                OR ClosingCreditAmountOC
                                - ClosingDebitAmountOC <> 0
                                OR ClosingCreditAmount - ClosingDebitAmount <> 0
                        ORDER BY RSS.AccountObjectCode ,
                                RSS.AccountNumber ,
                                RSS.OrderType ,
                                RSS.PostedDate ,
								RSS.OrderPriority,
                                RSS.RefDate ,
                                RSS.RefNo ,
                                RSS.InvDate ,
                                RSS.InvNo
            END
			END
/* Tính số tồn */
        DECLARE @CloseAmountOC AS DECIMAL(22, 8) ,
            @CloseAmount AS DECIMAL(22, 8) ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0
                                        THEN ( CASE WHEN ClosingDebitAmountOC = 0
                                                    THEN ClosingCreditAmountOC
                                                    ELSE -1
                                                         * ClosingDebitAmountOC
                                               END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber
                                        THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC
                                             - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0
                                              THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1
                                              THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0
                                                        THEN -1
                                                             * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1
                                               THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0
                                               THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0
                                                         THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0
                                      THEN ( CASE WHEN ClosingDebitAmount = 0
                                                  THEN ClosingCreditAmount
                                                  ELSE -1 * ClosingDebitAmount
                                             END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber
                                      THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount
                                           - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0
                                            THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1
                                            THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0
                                                      THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1
                                             THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0
                                             THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0
                                                       THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
       SELECT  RefID,RefType,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,DebitAmountOC,DebitAmount,CreditAmountOC,CreditAmount,ClosingDebitAmountOC,ClosingDebitAmount,ClosingCreditAmountOC,ClosingCreditAmount,AccountObjectCode,AccountObjectName,2 as ordercode 
		into #tg1
        FROM    #tblResult RS
        ORDER BY AccountObjectCode
        
			INSERT INTO #tg1 (RefID,RefType,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,DebitAmountOC,DebitAmount,CreditAmountOC,CreditAmount,ClosingDebitAmountOC,ClosingDebitAmount,ClosingCreditAmountOC,ClosingCreditAmount,AccountObjectCode,AccountObjectName,ordercode)  
			SELECT  null,null,null,null,null,N'Tên khách hàng: ' + AccountObjectName,null,null,null,null,null,null,null,null,null,null,AccountObjectCode,AccountObjectName,1
			FROM #tblResult
			group by AccountObjectCode,AccountObjectName
				
			SELECT TOP 0 *
			into #tg2
			FROM #tg1

			INSERT INTO #tg2 (RefID,RefType,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,DebitAmountOC,DebitAmount,CreditAmountOC,CreditAmount,ClosingDebitAmountOC,ClosingDebitAmount,ClosingCreditAmountOC,ClosingCreditAmount,AccountObjectCode,AccountObjectName,ordercode)  
			SELECT  null,null,null,null,null,N'Cộng nhóm',null,null,Sum(DebitAmountOC),Sum(DebitAmount),Sum(CreditAmountOC),Sum(CreditAmount),0,0,0,0,AccountObjectCode,AccountObjectName,3
			FROM #tblResult
			group by AccountObjectCode,AccountObjectName

			SELECT  AccountObjectCode into #Account from #tg1

			DECLARE @AccCode nvarchar(512)
			DECLARE @COUNT INT
			DECLARE @SODUDAUKYDebit Decimal(25,0)
			DECLARE @SODUDAUKYCredit Decimal(25,0)
			DECLARE @SODUDAUKYDebitOC Decimal(25,4)
			DECLARE @SODUDAUKYCreditOC Decimal(25,4)
			DECLARE @amount Decimal(25,0)
			DECLARE @amountOC Decimal(25,4)

			SET @COUNT = (SELECT COUNT(*) From #Account)
			WHILE(@COUNT > 0)
			BEGIN
			SET @AccCode = (SELECT TOP 1 AccountObjectCode FROM #Account)
			SET @SODUDAUKYDebit = ISNULL((select Sum(ClosingDebitAmount) from #tg1 where JournalMemo like N'%Số dư đầu kỳ%' AND  AccountObjectCode = @AccCode ),0)
			SET @SODUDAUKYCredit = ISNULL((select Sum(ClosingCreditAmount) from #tg1 where JournalMemo like N'%Số dư đầu kỳ%' AND  AccountObjectCode = @AccCode ),0)
			SET @SODUDAUKYDebitOC = ISNULL((select Sum(ClosingDebitAmountOC) from #tg1 where JournalMemo like N'%Số dư đầu kỳ%' AND  AccountObjectCode = @AccCode ),0)
			SET @SODUDAUKYCreditOC = ISNULL((select Sum(ClosingCreditAmountOC) from #tg1 where JournalMemo like N'%Số dư đầu kỳ%' AND  AccountObjectCode = @AccCode ),0)
			
			SET @amount = @SODUDAUKYDebit - @SODUDAUKYCredit 
			+ ((SELECT SUM(DebitAmount) FROM #tg1 WHERE AccountObjectCode = @AccCode) 
			- (SELECT SUM(CreditAmount) FROM #tg1 WHERE AccountObjectCode = @AccCode))

			SET @amountOC = @SODUDAUKYDebitOC - @SODUDAUKYCreditOC 
			+ ((SELECT SUM(DebitAmount) FROM #tg1 WHERE AccountObjectCode = @AccCode) 
			- (SELECT SUM(CreditAmount) FROM #tg1 WHERE AccountObjectCode = @AccCode))
			
			if(@amount > 0)
			BEGIN
				UPDATE #tg2
				SET ClosingDebitAmount  =  @amount,
				ClosingDebitAmountOC  =  @amountOC
			

				WHERE AccountObjectCode = @AccCode
				SET @COUNT = @COUNT -1;
				DELETE #Account where AccountObjectCode = @AccCode
				END
			
			else
			BEGIN
				UPDATE #tg2
				SET ClosingCreditAmount  =  @amount * -1,
				ClosingCreditAmountOC  =  @amountOC * -1
			

				WHERE AccountObjectCode = @AccCode
				SET @COUNT = @COUNT -1;
				DELETE #Account where AccountObjectCode = @AccCode
				END
			END
		

			

		INSERT INTO #tg1 SELECT * from #tg2

			
			SELECT * from #tg1 order by AccountObjectCode,ordercode
        DROP TABLE #tblResult
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*=================================================================================================
 * Created By:		haipl
 * Created Date:	25/12/2017
 * Description:		Lay du lieu cho bao cao << Sổ chi tiết bán hàng >>
 * Edit by: Cuongpv
 * Edit Description: Lay du lieu day du cac truong hop cho bao cao <<So chi tiet ban hang>>
===================================================================================================== */
ALTER PROCEDURE [dbo].[Proc_SA_GetSalesBookDetail]
  
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @InventoryItemID NVARCHAR(MAX) , 
    @OrganizationUnitID UNIQUEIDENTIFIER ,
    @AccountObjectID NVARCHAR(MAX) ,
	@SumGiaVon decimal output
AS

    BEGIN
        SET NOCOUNT ON;
        declare @v1 DECIMAL(29,4)                            
        CREATE TABLE #tblListInventoryItemID
            (
              InventoryItemID UNIQUEIDENTIFIER PRIMARY KEY
            ) 
        INSERT  INTO #tblListInventoryItemID
                    SELECT  T.*
                    FROM    dbo.Func_ConvertStringIntoTable(@InventoryItemID,
                                                              ',')	
                                                              AS T  
        /*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between  @FromDate and @ToDate
		/*end add by cuongpv*/

		/*add by cuongpv*/
		DECLARE @tbLuuTru TABLE(
			ngay_CT datetime,
			ngay_HT datetime,
			So_Hieu nvarchar(25),
			Ngay_HD datetime,
			SO_HD nvarchar(25),
			Dien_giai nvarchar(512),
			TK_DOIUNG nvarchar(25),
			DVT nvarchar(25),
			SL decimal(25,10),
			DON_GIA money,
			KHAC decimal(25,0),
			TT decimal(25,0),
			InventoryItemID uniqueidentifier,
			MaterialGoodsName nvarchar(512),
			RefID uniqueidentifier,
			TypeID int
		)
		
		INSERT INTO @tbLuuTru
		SELECT ngay_CT, ngay_HT, So_Hieu, Ngay_HD, SO_HD, Dien_giai, TK_DOIUNG, DVT, SL, DON_GIA, SUM(KHAC) as KHAC, SUM(TT) as TT, InventoryItemID, MaterialGoodsName, RefID, TypeID
		FROM (
		/*end add by cuongpv*/
			/*lay hoa don ban hang*/
			select a.Date as ngay_CT, a.PostedDate as ngay_HT, a.No as So_Hieu, a.InvoiceDate as Ngay_HD, a.InvoiceNo as SO_HD, d.reason  as Dien_giai, accountCorresponding as TK_DOIUNG,
			c.Unit as DVT, b.Quantity as SL, b.UnitPrice as DON_GIA, null as KHAC, sum(CreditAmount) as TT, c.ID as  InventoryItemID, c.MaterialGoodsName, a.OrderPriority, a.ReferenceID as RefID, a.TypeID
			from @tbDataGL a left join SAInvoiceDetail b on a.DetailID = b.ID /*edit by cuongpv GeneralLedger -> @tbDataGL*/
			left join MaterialGoods c on b.MaterialGoodsID = c.ID
			left join SAInvoice d on d.ID = b.SAInvoiceID
			where a.PostedDate between  @FromDate and @ToDate and c.ID in (select InventoryItemID from #tblListInventoryItemID)
			and Account like '511%' and d.BillRefID is null /*add by cuonpv: "and d.BillRefID is null"*/
			group by a.Date,  a.PostedDate, a.No ,a.InvoiceDate , a.InvoiceNo, accountCorresponding , /*edit by cuonpv bo: " , Sreason "*/
			c.Unit , b.Quantity , b.UnitPrice, c.ID,c.MaterialGoodsName,d.reason,a.OrderPriority, a.ReferenceID, a.TypeID
			Union all
			/*lay chiet khau ban*/
			select a.Date as ngay_CT, a.PostedDate as ngay_HT, a.No as So_Hieu, a.InvoiceDate as Ngay_HD, a.InvoiceNo as SO_HD, d.reason  as Dien_giai, accountCorresponding as TK_DOIUNG,
			c.Unit as DVT, b.Quantity as SL, b.UnitPrice as DON_GIA, sum(debitAmount) as KHAC, null as TT, c.ID as  InventoryItemID, c.MaterialGoodsName, a.OrderPriority, a.ReferenceID as RefID, a.TypeID
			from @tbDataGL a left join SAInvoiceDetail b on a.DetailID = b.ID /*edit by cuongpv GeneralLedger -> @tbDataGL*/
			left join MaterialGoods c on b.MaterialGoodsID = c.ID
			left join SAInvoice d on d.ID = b.SAInvoiceID
			where a.PostedDate between  @FromDate and @ToDate and c.ID in (select InventoryItemID from #tblListInventoryItemID)
			and Account like '511%' and (a.TypeID not in (330,340)) and d.BillRefID is null 
			group by a.Date,  a.PostedDate, a.No ,a.InvoiceDate , a.InvoiceNo, accountCorresponding , 
			c.Unit , b.Quantity , b.UnitPrice, c.ID,c.MaterialGoodsName,d.reason,a.OrderPriority, a.ReferenceID, a.TypeID
		) SA
		GROUP BY ngay_CT, ngay_HT, So_Hieu, Ngay_HD, SO_HD, Dien_giai, TK_DOIUNG, DVT, SL, DON_GIA, InventoryItemID, MaterialGoodsName, RefID, TypeID
		/*add by cuongpv*/
		UNION ALL
		SELECT ngay_CT, ngay_HT, So_Hieu, Ngay_HD, SO_HD, Dien_giai, TK_DOIUNG, DVT, SL, DON_GIA, SUM(KHAC) as KHAC, SUM(TT) as TT, InventoryItemID, MaterialGoodsName, RefID, TypeID
		FROM (
			/*lay hoa don  lien ket voi SABill*/
			select a.Date as ngay_CT, a.PostedDate as ngay_HT, a.No as So_Hieu, SABi.InvoiceDate as Ngay_HD, SABi.InvoiceNo as SO_HD, d.reason  as Dien_giai, accountCorresponding as TK_DOIUNG,
			c.Unit as DVT, b.Quantity as SL, b.UnitPrice as DON_GIA, null as KHAC, sum(CreditAmount) as TT, c.ID as  InventoryItemID, c.MaterialGoodsName, a.OrderPriority, a.ReferenceID as RefID, a.TypeID
			from @tbDataGL a left join SAInvoiceDetail b on a.DetailID = b.ID
			left join MaterialGoods c on b.MaterialGoodsID = c.ID
			left join SAInvoice d on d.ID = b.SAInvoiceID
			left join SABill SABi on d.BillRefID = SABi.ID
			where a.PostedDate between  @FromDate and @ToDate and c.ID in (select InventoryItemID from #tblListInventoryItemID)
			and Account like '511%' and d.BillRefID is not null
			group by a.Date,  a.PostedDate, a.No ,SABi.InvoiceDate , SABi.InvoiceNo, accountCorresponding , 
			c.Unit , b.Quantity , b.UnitPrice, c.ID,c.MaterialGoodsName,d.reason,a.OrderPriority, a.ReferenceID, a.TypeID
			Union all
			select a.Date as ngay_CT, a.PostedDate as ngay_HT, a.No as So_Hieu, SABi.InvoiceDate as Ngay_HD, SABi.InvoiceNo as SO_HD, d.reason  as Dien_giai, accountCorresponding as TK_DOIUNG,
			c.Unit as DVT, b.Quantity as SL, b.UnitPrice as DON_GIA, sum(debitAmount) as KHAC, null as TT, c.ID as  InventoryItemID, c.MaterialGoodsName, a.OrderPriority, a.ReferenceID as RefID, a.TypeID
			from @tbDataGL a left join SAInvoiceDetail b on a.DetailID = b.ID
			left join MaterialGoods c on b.MaterialGoodsID = c.ID
			left join SAInvoice d on d.ID = b.SAInvoiceID
			left join SABill SABi on d.BillRefID = SABi.ID
			where a.PostedDate between  @FromDate and @ToDate and c.ID in (select InventoryItemID from #tblListInventoryItemID)
			and Account like '511%' and (a.TypeID not in (330,340)) and d.BillRefID is not null
			group by a.Date,  a.PostedDate, a.No ,SABi.InvoiceDate , SABi.InvoiceNo, accountCorresponding , 
			c.Unit , b.Quantity , b.UnitPrice, c.ID,c.MaterialGoodsName,d.reason,a.OrderPriority, a.ReferenceID, a.TypeID
		) SAB
		GROUP BY ngay_CT, ngay_HT, So_Hieu, Ngay_HD, SO_HD, Dien_giai, TK_DOIUNG, DVT, SL, DON_GIA, InventoryItemID, MaterialGoodsName, RefID, TypeID
		
		UNION ALL
		/*lay gia tri tra lai giam gia*/
		SELECT ngay_CT, ngay_HT, So_Hieu, Ngay_HD, SO_HD, Dien_giai, TK_DOIUNG, DVT, SL, DON_GIA, SUM(KHAC) as KHAC, SUM(TT) as TT, InventoryItemID, MaterialGoodsName, RefID, TypeID
		FROM (
			/*lay dong gia tri SAInvoice*/
			/*lay hoa don ban hang*/
			select a.Date as ngay_CT, a.PostedDate as ngay_HT, a.No as So_Hieu, a.InvoiceDate as Ngay_HD, a.InvoiceNo as SO_HD, d.reason  as Dien_giai, accountCorresponding as TK_DOIUNG,
			null as DVT, null as SL, null as DON_GIA, SUM(DebitAmount - CreditAmount) as KHAC, 0 as TT, c.ID as  InventoryItemID, c.MaterialGoodsName, a.OrderPriority, a.ReferenceID as RefID, a.TypeID
			from @tbDataGL a left join SAReturnDetail SAR on SAR.ID=a.DetailID join SAInvoiceDetail b on b.ID=SAR.SAInvoiceDetailID
			left join MaterialGoods c on b.MaterialGoodsID = c.ID
			left join SAInvoice d on d.ID = b.SAInvoiceID
			where a.PostedDate between  @FromDate and @ToDate and c.ID in (select InventoryItemID from #tblListInventoryItemID)
			and Account like '511%' and (a.TypeID in (330,340)) and d.BillRefID is null 
			group by a.Date,  a.PostedDate, a.No ,a.InvoiceDate , a.InvoiceNo, accountCorresponding ,
			c.Unit , b.Quantity , b.UnitPrice, c.ID,c.MaterialGoodsName,d.reason,a.OrderPriority, a.ReferenceID, a.TypeID
			union all
			select a.Date as ngay_CT, a.PostedDate as ngay_HT, a.No as So_Hieu, SABi.InvoiceDate as Ngay_HD, SABi.InvoiceNo as SO_HD, d.reason  as Dien_giai, accountCorresponding as TK_DOIUNG,
			null as DVT, null as SL, null as DON_GIA, SUM(DebitAmount - CreditAmount) as KHAC, 0 as TT, c.ID as  InventoryItemID, c.MaterialGoodsName, a.OrderPriority, a.ReferenceID as RefID, a.TypeID
			from @tbDataGL a left join SAReturnDetail SAR on SAR.ID=a.DetailID join SAInvoiceDetail b on b.ID=SAR.SAInvoiceDetailID
			left join MaterialGoods c on b.MaterialGoodsID = c.ID
			left join SAInvoice d on d.ID = b.SAInvoiceID
			left join SABill SABi on d.BillRefID = SABi.ID
			where a.PostedDate between  @FromDate and @ToDate and c.ID in (select InventoryItemID from #tblListInventoryItemID)
			and Account like '511%' and (a.TypeID in (330,340)) and d.BillRefID is not null
			group by a.Date,  a.PostedDate, a.No ,SABi.InvoiceDate , SABi.InvoiceNo, accountCorresponding , 
			c.Unit , b.Quantity , b.UnitPrice, c.ID,c.MaterialGoodsName,d.reason,a.OrderPriority, a.ReferenceID, a.TypeID
		) GG
		GROUP BY ngay_CT, ngay_HT, So_Hieu, Ngay_HD, SO_HD, Dien_giai, TK_DOIUNG, DVT, SL, DON_GIA, InventoryItemID, MaterialGoodsName, RefID, TypeID

		SELECT * FROM @tbLuuTru ORDER BY MaterialGoodsName, ngay_HT, So_Hieu
		/*end add by cuongpv*/

		
		/*add by cuongpv*/
		SELECT SUM(g.Sum_Gia_Goc - Sum_Gia_TLGG) as Sum_Gia_Goc, g.MaterialGoodsCode, g.InventoryItemID
		FROM(/*end add by cuongpv*/
		 select isnull(sum(OWAmount),0) Sum_Gia_Goc, 0 as Sum_Gia_TLGG, b.MaterialGoodsCode ,b.ID InventoryItemID from SAInvoiceDetail a, MaterialGoods b 
		 where exists (select InventoryItemID from #tblListInventoryItemID where InventoryItemID = a.MaterialGoodsID)
		 and exists ( select ReferenceID from @tbDataGL where  account like '632%' and PostedDate between @FromDate and @ToDate and ReferenceID=a.SAinvoiceID )/*edit by cuonpv: GeneralLedger -> @tbDataGL;account = '632' -> account like '632%'*/
		 and a.MaterialGoodsID=b.ID group by b.ID,b.MaterialGoodsCode,OrderPriority
		 union all
		 select 0 as Sum_Gia_Goc, isnull(sum(OWAmount),0) as Sum_Gia_TLGG, b.MaterialGoodsCode ,b.ID InventoryItemID from SAReturnDetail a, MaterialGoods b 
		 where exists (select InventoryItemID from #tblListInventoryItemID where InventoryItemID = a.MaterialGoodsID)
		 and exists ( select ReferenceID from @tbDataGL where  account like '632%' and PostedDate between @FromDate and @ToDate and DetailID=a.ID )
		 and a.MaterialGoodsID=b.ID group by b.ID,b.MaterialGoodsCode,OrderPriority
		/*add by cuongpv*/
		) g
		GROUP BY g.MaterialGoodsCode, g.InventoryItemID
		ORDER BY MaterialGoodsCode
		/*end add by cuongpv*/
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_PO_SummaryPayable]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS
    BEGIN
	    
		/*Add by cuongpv de sua cach lam tron*/
	DECLARE @tbDataGL TABLE(
		ID uniqueidentifier,
		BranchID uniqueidentifier,
		ReferenceID uniqueidentifier,
		TypeID int,
		Date datetime,
		PostedDate datetime,
		No nvarchar(25),
		InvoiceDate datetime,
		InvoiceNo nvarchar(25),
		Account nvarchar(25),
		AccountCorresponding nvarchar(25),
		BankAccountDetailID uniqueidentifier,
		CurrencyID nvarchar(3),
		ExchangeRate decimal(25, 10),
		DebitAmount decimal(25,0),
		DebitAmountOriginal decimal(25,2),
		CreditAmount decimal(25,0),
		CreditAmountOriginal decimal(25,2),
		Reason nvarchar(512),
		Description nvarchar(512),
		VATDescription nvarchar(512),
		AccountingObjectID uniqueidentifier,
		EmployeeID uniqueidentifier,
		BudgetItemID uniqueidentifier,
		CostSetID uniqueidentifier,
		ContractID uniqueidentifier,
		StatisticsCodeID uniqueidentifier,
		InvoiceSeries nvarchar(25),
		ContactName nvarchar(512),
		DetailID uniqueidentifier,
		RefNo nvarchar(25),
		RefDate datetime,
		DepartmentID uniqueidentifier,
		ExpenseItemID uniqueidentifier,
		OrderPriority int,
		IsIrrationalCost bit
	)

	INSERT INTO @tbDataGL
	SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
	/*end add by cuongpv*/
		
		DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(512) ,
              AccountObjectAddress NVARCHAR(512) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        null AccountingObjectGroupCode ,
                        null AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
      
                 
		
		DECLARE @StartDateOfPeriod DATETIME
		SET @StartDateOfPeriod = '1/1/'+CAST(Year(@FromDate) AS NVARCHAR(5))
                         
           
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(512) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
	    IF(@CurrencyID = 'VND')
		BEGIN
		 SELECT  ROW_NUMBER() OVER ( ORDER BY AccountObjectCode ) AS RowNum ,
                AccountingObjectID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountObjectAddress ,
                AccountObjectTaxCode ,
                AccountNumber ,
                AccountCategoryKind ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC)
                            - SUM(OpenningCreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) ) > 0
                                 THEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN ( SUM(OpenningDebitAmount - OpenningCreditAmount) )
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmount
                                            - OpenningCreditAmount) ) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount)
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmountOC
                                  - OpenningDebitAmountOC) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) ) > 0
                                 THEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmount - OpenningDebitAmount) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) ) > 0
                                 THEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmount ,
                SUM(DebitAmountOC) AS DebitAmountOC ,
                SUM(DebitAmount) AS DebitAmount ,
                SUM(CreditAmountOC) AS CreditAmountOC ,
                SUM(CreditAmount) AS CreditAmount ,
                SUM(AccumDebitAmountOC) AS AccumDebitAmountOC ,
                SUM(AccumDebitAmount) AS AccumDebitAmount ,
                SUM(AccumCreditAmountOC) AS AccumCreditAmountOC ,
                SUM(AccumCreditAmount) AS AccumCreditAmount ,
                /* Số dư cuối kỳ = Dư Có đầu kỳ - Dư Nợ đầu kỳ + Phát sinh Có – Phát sinh Nợ
				Nếu Số dư cuối kỳ >0 thì hiển bên cột Dư Có cuối kỳ 
				Nếu số dư cuối kỳ <0 thì hiển thị bên cột Dư Nợ cuối kỳ */
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC
                                + DebitAmountOC - CreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC) > 0
                                 THEN $0
                                 ELSE SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC)
                            END
                  END ) AS CloseDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC
                                + CreditAmountOC - DebitAmountOC)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC
                                            + CreditAmountOC - DebitAmountOC) ) > 0
                                 THEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                                + DebitAmount - CreditAmount)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount) > 0 THEN $0
                                 ELSE SUM(OpenningDebitAmount
                                          - OpenningCreditAmount + DebitAmount
                                          - CreditAmount)
                            END
                  END ) AS ClosingDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                                + CreditAmount - DebitAmount)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount
                                            + CreditAmount - DebitAmount) ) > 0
                                 THEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)
                                 ELSE $0
                            END
                  END ) AS ClosingCreditAmount ,
                AccountObjectGroupListCode ,
                AccountObjectGroupListName
        FROM    ( SELECT    AOL.AccountingObjectID ,
                            LAOI.AccountObjectCode ,
                            LAOI.AccountObjectName ,
                            LAOI.AccountObjectAddress,
                            LAOI.AccountObjectTaxCode ,
                            TBAN.AccountNumber ,
                            TBAN.AccountCategoryKind ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmount
                                 ELSE $0
                            END AS OpenningDebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmount
                                 ELSE $0
                            END AS OpenningCreditAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmountOriginal
                            END AS DebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmount
                            END AS DebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmountOriginal
                            END AS CreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmount
                            END AS CreditAmount ,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmountOriginal ELSE 0 END AS AccumDebitAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmount ELSE 0 END AS AccumDebitAmount,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmountOriginal ELSE 0 END AS AccumCreditAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmount ELSE 0 END AS AccumCreditAmount,
                            LAOI.AccountObjectGroupListCode ,
                            LAOI.AccountObjectGroupListName
                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                  WHERE     AOL.PostedDate <= @ToDate
                           
                ) AS RSNS
        GROUP BY RSNS.AccountingObjectID ,
                RSNS.AccountObjectCode ,
                RSNS.AccountObjectName ,
                RSNS.AccountObjectAddress ,
                RSNS.AccountObjectTaxCode ,
                RSNS.AccountNumber ,
                RSNS.AccountCategoryKind ,
                RSNS.AccountObjectGroupListCode ,
                RSNS.AccountObjectGroupListName 
        HAVING 
				SUM(DebitAmountOC) <> 0
                OR SUM(DebitAmount) <> 0
                OR SUM(CreditAmountOC) <> 0
                OR SUM(CreditAmount) <> 0
                OR SUM(OpenningDebitAmount - OpenningCreditAmount) <> 0 
                OR SUM(OpenningDebitAmountOC - OpenningCreditAmountOC) <> 0 
				OR ( 
					(SUM(AccumDebitAmountOC) <> 0
					OR SUM(AccumDebitAmount) <> 0
					OR SUM(AccumCreditAmountOC) <> 0
					OR SUM(AccumCreditAmount) <> 0) )
        ORDER BY RSNS.AccountObjectCode
        OPTION (RECOMPILE)
		END
		ELSE
		BEGIN
        SELECT  ROW_NUMBER() OVER ( ORDER BY AccountObjectCode ) AS RowNum ,
                AccountingObjectID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountObjectAddress ,
                AccountObjectTaxCode ,
                AccountNumber ,
                AccountCategoryKind ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC)
                            - SUM(OpenningCreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) ) > 0
                                 THEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN ( SUM(OpenningDebitAmount - OpenningCreditAmount) )
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmount
                                            - OpenningCreditAmount) ) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount)
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmountOC
                                  - OpenningDebitAmountOC) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) ) > 0
                                 THEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmount - OpenningDebitAmount) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) ) > 0
                                 THEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmount ,
                SUM(DebitAmountOC) AS DebitAmountOC ,
                SUM(DebitAmount) AS DebitAmount ,
                SUM(CreditAmountOC) AS CreditAmountOC ,
                SUM(CreditAmount) AS CreditAmount ,
                SUM(AccumDebitAmountOC) AS AccumDebitAmountOC ,
                SUM(AccumDebitAmount) AS AccumDebitAmount ,
                SUM(AccumCreditAmountOC) AS AccumCreditAmountOC ,
                SUM(AccumCreditAmount) AS AccumCreditAmount ,
                /* Số dư cuối kỳ = Dư Có đầu kỳ - Dư Nợ đầu kỳ + Phát sinh Có – Phát sinh Nợ
				Nếu Số dư cuối kỳ >0 thì hiển bên cột Dư Có cuối kỳ 
				Nếu số dư cuối kỳ <0 thì hiển thị bên cột Dư Nợ cuối kỳ */
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC
                                + DebitAmountOC - CreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC) > 0
                                 THEN $0
                                 ELSE SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC)
                            END
                  END ) AS CloseDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC
                                + CreditAmountOC - DebitAmountOC)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC
                                            + CreditAmountOC - DebitAmountOC) ) > 0
                                 THEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                                + DebitAmount - CreditAmount)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount) > 0 THEN $0
                                 ELSE SUM(OpenningDebitAmount
                                          - OpenningCreditAmount + DebitAmount
                                          - CreditAmount)
                            END
                  END ) AS ClosingDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                                + CreditAmount - DebitAmount)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount
                                            + CreditAmount - DebitAmount) ) > 0
                                 THEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)
                                 ELSE $0
                            END
                  END ) AS ClosingCreditAmount ,
                AccountObjectGroupListCode ,
                AccountObjectGroupListName
        FROM    ( SELECT    AOL.AccountingObjectID ,
                            LAOI.AccountObjectCode ,
                            LAOI.AccountObjectName ,
                            LAOI.AccountObjectAddress,
                            LAOI.AccountObjectTaxCode ,
                            TBAN.AccountNumber ,
                            TBAN.AccountCategoryKind ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmountOriginal
                            END AS DebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmountOriginal
                            END AS DebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmountOriginal
                            END AS CreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmountOriginal
                            END AS CreditAmount ,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmountOriginal ELSE 0 END AS AccumDebitAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmount ELSE 0 END AS AccumDebitAmount,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmountOriginal ELSE 0 END AS AccumCreditAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmount ELSE 0 END AS AccumCreditAmount,
                            LAOI.AccountObjectGroupListCode ,
                            LAOI.AccountObjectGroupListName
                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                  WHERE     AOL.PostedDate <= @ToDate
                            AND ( @CurrencyID IS NULL
                                  OR AOL.CurrencyID = @CurrencyID
                                )
                ) AS RSNS
        GROUP BY RSNS.AccountingObjectID ,
                RSNS.AccountObjectCode ,
                RSNS.AccountObjectName ,
                RSNS.AccountObjectAddress ,
                RSNS.AccountObjectTaxCode ,
                RSNS.AccountNumber ,
                RSNS.AccountCategoryKind ,
                RSNS.AccountObjectGroupListCode ,
                RSNS.AccountObjectGroupListName 
        HAVING 
				SUM(DebitAmountOC) <> 0
                OR SUM(DebitAmount) <> 0
                OR SUM(CreditAmountOC) <> 0
                OR SUM(CreditAmount) <> 0
                OR SUM(OpenningDebitAmount - OpenningCreditAmount) <> 0 
                OR SUM(OpenningDebitAmountOC - OpenningCreditAmountOC) <> 0 
				OR ( 
					(SUM(AccumDebitAmountOC) <> 0
					OR SUM(AccumDebitAmount) <> 0
					OR SUM(AccumCreditAmountOC) <> 0
					OR SUM(AccumCreditAmount) <> 0) )
        ORDER BY RSNS.AccountObjectCode
        OPTION (RECOMPILE)
		END
		
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER procedure [dbo].[Proc_PO_GetDetailBook]
@FromDate DATETIME ,
    @ToDate DATETIME,
    @AccountObjectID AS NVARCHAR(MAX),
	@MaterialGoods as NVARCHAR(MAX)
as
begin
    CREATE TABLE #tblListAccountObjectID  
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25)
                 ,
              AccountObjectName NVARCHAR(512)
                 
            ) 
    CREATE TABLE #tblListMaterialGoods  
            (
              MaterialGoodsID UNIQUEIDENTIFIER ,
              MaterialGoodsCode NVARCHAR(25)
                 ,
              MaterialGoodsName NVARCHAR(512)
                 ,
			  Unit NVARCHAR(25)  ,
			 Quantity decimal,
			 UnitPrice Money 
            ) 
        INSERT  INTO #tblListAccountObjectID
                SELECT  AO.ID ,
                        ao.AccountingObjectCode ,
                        ao.AccountingObjectName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                           ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value 
        INSERT  INTO #tblListMaterialGoods
                SELECT  MG.ID ,
                        MG.MaterialGoodsCode ,
                        MG.MaterialGoodsName,
						MG.Unit,
						MG.Quantity,
						MG.UnitPrice 
                FROM    dbo.Func_ConvertStringIntoTable(@MaterialGoods,
                                                           ',') tblAccountObjectSelected
                        INNER JOIN dbo.MaterialGoods MG ON MG.ID = tblAccountObjectSelected.Value 
	/*select * from #tblListMaterialGoods
	select * from #tblListAccountObjectID*/
	/*Tao bang tam luu du lieu ra bao cao*/
	DECLARE @tbDataResult TABLE(
		AccountObjectID UNIQUEIDENTIFIER,
		MaKH NVARCHAR(25),
		TenKH NVARCHAR(512),
		NgayHachToan DATE,
		NgayCTu DATE,
		SoCTu NVARCHAR(25),
		SoHoaDon NVARCHAR(25),
		NgayHoaDon DATE,
		MaterialGoodsID UNIQUEIDENTIFIER ,
        Mahang NVARCHAR(25),
		Tenhang NVARCHAR(512),
		DVT NVARCHAR(25),
		SoLuongMua DECIMAL(25,10),
		DonGia MONEY,
		GiaTriMua DECIMAL(25,0),
		ChietKhau DECIMAL(25,0),
		SoLuongTraLai DECIMAL(25,10),
		GiaTriTraLai DECIMAL(25,0),
		GiaTriGiamGia DECIMAL(25,0),
		TypeMG  NVARCHAR(1),
		RefID UNIQUEIDENTIFIER ,
		TypeID int
	)

	IF((@AccountObjectID is not null) AND (@MaterialGoods is not null))
	BEGIN
		/*lay gia tri PPInvoice vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, SoLuongMua, DonGia, GiaTriMua, ChietKhau, TypeMG, RefID, TypeID)
		SELECT a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, b.InvoiceNo as SoHoaDon, b.InvoiceDate as NgayHoaDon, 
			   b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.Quantity as SoLuongMua, b.UnitPrice as DonGia, b.Amount as GiaTriMua, b.DiscountAmount as ChietKhau, 'I' as TypeMG, a.ID as RefID, a.TypeID  		 
		   FROM PPInvoiceDetail b LEFT JOIN PPInvoice a ON a.id = b.PPInvoiceID 
		   WHERE a.PostedDate between @FromDate and @ToDate
		   and a.Recorded = '1'
		   and ((a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID)) and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods)))


		/*Lay gia tri PPService vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, SoLuongMua, DonGia, GiaTriMua, ChietKhau, TypeMG, RefID, TypeID)
		   SELECT  a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, b.InvoiceNo as SoHoaDon, b.InvoiceDate as NgayHoaDon, 
				   b.MaterialGoodsID as MaterialGoodsID, b.Quantity as SoLuongMua, b.UnitPrice as DonGia, b.Amount as GiaTriMua, b.DiscountAmount as ChietKhau, 'S' as TypeMG , a.ID as RefID, a.TypeID 		
		   FROM PPServiceDetail b LEFT JOIN PPService a ON a.ID = b.PPServiceID
		   WHERE a.PostedDate between @FromDate and @ToDate
		   and a.Recorded = '1' 
		   and ((a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID)) and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods)))

		/*Update DVT cua cac gia tri lay tu PPService*/
		UPDATE @tbDataResult SET DVT = MS.DVT
		FROM (
			SELECT ID as MSID, Unit as DVT FROM dbo.MaterialGoods
		) MS
		WHERE MaterialGoodsID = MS.MSID AND TypeMG = 'S'


		/*Lay gia tri Tra lai vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, DonGia, SoLuongTraLai, GiaTriTraLai, TypeMG, RefID, TypeID)
		SELECT  a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, a.InvoiceNo as SoHoaDon, a.InvoiceDate as NgayHoaDon,
				  b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.UnitPrice as DonGia, b.Quantity as SoLuongTraLai, b.Amount as GiaTriTraLai, 'T' as TypeMG 	, a.ID as RefID, a.TypeID	
			FROM PPDiscountReturnDetail b LEFT JOIN PPDiscountReturn a ON a.ID = b.PPDiscountReturnID
			WHERE a.PostedDate between @FromDate and @ToDate and a.Recorded = '1' and TypeID = '220'
			and ((a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID)) and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods)))



		/*lay gia tri giam gia vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, GiaTriGiamGia, TypeMG, RefID, TypeID)
			select a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, InvoiceNo as SoHoaDon, InvoiceDate as NgayHoaDon, 
				   b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.Amount as GiaTriGiamGia, 'G' as TypeMG, a.ID as RefID, a.TypeID	   	
			from PPDiscountReturnDetail b LEFT JOIN PPDiscountReturn a ON a.ID = b.PPDiscountReturnID 
			where a.PostedDate between @FromDate and @ToDate and TypeID = '230'
			and ((a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID)) and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods)))
	END
	ELSE IF((@AccountObjectID is not null) AND (@MaterialGoods is null))
	BEGIN
		/*lay gia tri PPInvoice vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, SoLuongMua, DonGia, GiaTriMua, ChietKhau, TypeMG, RefID, TypeID)
		SELECT a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, b.InvoiceNo as SoHoaDon, b.InvoiceDate as NgayHoaDon, 
			   b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.Quantity as SoLuongMua, b.UnitPrice as DonGia, b.Amount as GiaTriMua, b.DiscountAmount as ChietKhau, 'I' as TypeMG , a.ID as RefID, a.TypeID  		 
		   FROM PPInvoiceDetail b LEFT JOIN PPInvoice a ON a.id = b.PPInvoiceID 
		   WHERE a.PostedDate between @FromDate and @ToDate
		   and a.Recorded = '1'
		   and (a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID))


		/*Lay gia tri PPService vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, SoLuongMua, DonGia, GiaTriMua, ChietKhau, TypeMG, RefID, TypeID)
		   SELECT  a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, b.InvoiceNo as SoHoaDon, b.InvoiceDate as NgayHoaDon, 
				   b.MaterialGoodsID as MaterialGoodsID, b.Quantity as SoLuongMua, b.UnitPrice as DonGia, b.Amount as GiaTriMua, b.DiscountAmount as ChietKhau, 'S' as TypeMG , a.ID as RefID, a.TypeID 		
		   FROM PPServiceDetail b LEFT JOIN PPService a ON a.ID = b.PPServiceID
		   WHERE a.PostedDate between @FromDate and @ToDate
		   and a.Recorded = '1' 
		   and (a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID))

		/*Update DVT cua cac gia tri lay tu PPService*/
		UPDATE @tbDataResult SET DVT = MS.DVT
		FROM (
			SELECT ID as MSID, Unit as DVT FROM dbo.MaterialGoods
		) MS
		WHERE MaterialGoodsID = MS.MSID AND TypeMG = 'S'


		/*Lay gia tri Tra lai vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, DonGia, SoLuongTraLai, GiaTriTraLai, TypeMG, RefID, TypeID)
		SELECT  a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, a.InvoiceNo as SoHoaDon, a.InvoiceDate as NgayHoaDon,
				  b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.UnitPrice as DonGia, b.Quantity as SoLuongTraLai, b.Amount as GiaTriTraLai, 'T' as TypeMG, a.ID as RefID, a.TypeID 		
			FROM PPDiscountReturnDetail b LEFT JOIN PPDiscountReturn a ON a.ID = b.PPDiscountReturnID
			WHERE a.PostedDate between @FromDate and @ToDate and a.Recorded = '1' and TypeID = '220'
			and (a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID))



		/*lay gia tri giam gia vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, GiaTriGiamGia, TypeMG, RefID, TypeID)
			select a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, InvoiceNo as SoHoaDon, InvoiceDate as NgayHoaDon, 
				   b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.Amount as GiaTriGiamGia, 'G' as TypeMG, a.ID as RefID, a.TypeID	   	
			from PPDiscountReturnDetail b LEFT JOIN PPDiscountReturn a ON a.ID = b.PPDiscountReturnID 
			where a.PostedDate between @FromDate and @ToDate and TypeID = '230'
			and (a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID))
	END
	ELSE
	BEGIN
		/*lay gia tri PPInvoice vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, SoLuongMua, DonGia, GiaTriMua, ChietKhau, TypeMG, RefID, TypeID)
		SELECT a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, b.InvoiceNo as SoHoaDon, b.InvoiceDate as NgayHoaDon, 
			   b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.Quantity as SoLuongMua, b.UnitPrice as DonGia, b.Amount as GiaTriMua, b.DiscountAmount as ChietKhau, 'I' as TypeMG , a.ID as RefID, a.TypeID  		 
		   FROM PPInvoiceDetail b LEFT JOIN PPInvoice a ON a.id = b.PPInvoiceID 
		   WHERE a.PostedDate between @FromDate and @ToDate
		   and a.Recorded = '1'
		   and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods))


		/*Lay gia tri PPService vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, SoLuongMua, DonGia, GiaTriMua, ChietKhau, TypeMG, RefID, TypeID)
		   SELECT  a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, b.InvoiceNo as SoHoaDon, b.InvoiceDate as NgayHoaDon, 
				   b.MaterialGoodsID as MaterialGoodsID, b.Quantity as SoLuongMua, b.UnitPrice as DonGia, b.Amount as GiaTriMua, b.DiscountAmount as ChietKhau, 'S' as TypeMG, a.ID as RefID, a.TypeID  		
		   FROM PPServiceDetail b LEFT JOIN PPService a ON a.ID = b.PPServiceID
		   WHERE a.PostedDate between @FromDate and @ToDate
		   and a.Recorded = '1' 
		   and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods))

		/*Update DVT cua cac gia tri lay tu PPService*/
		UPDATE @tbDataResult SET DVT = MS.DVT
		FROM (
			SELECT ID as MSID, Unit as DVT FROM dbo.MaterialGoods
		) MS
		WHERE MaterialGoodsID = MS.MSID AND TypeMG = 'S'


		/*Lay gia tri Tra lai vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, DonGia, SoLuongTraLai, GiaTriTraLai, TypeMG, RefID, TypeID)
		SELECT  a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, a.InvoiceNo as SoHoaDon, a.InvoiceDate as NgayHoaDon,
				  b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.UnitPrice as DonGia, b.Quantity as SoLuongTraLai, b.Amount as GiaTriTraLai, 'T' as TypeMG 	, a.ID as RefID, a.TypeID	
			FROM PPDiscountReturnDetail b LEFT JOIN PPDiscountReturn a ON a.ID = b.PPDiscountReturnID
			WHERE a.PostedDate between @FromDate and @ToDate and a.Recorded = '1' and TypeID = '220'
			and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods))



		/*lay gia tri giam gia vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, GiaTriGiamGia, TypeMG, RefID, TypeID)
			select a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, InvoiceNo as SoHoaDon, InvoiceDate as NgayHoaDon, 
				   b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.Amount as GiaTriGiamGia, 'G' as TypeMG, a.ID as RefID, a.TypeID	   	
			from PPDiscountReturnDetail b LEFT JOIN PPDiscountReturn a ON a.ID = b.PPDiscountReturnID 
			where a.PostedDate between @FromDate and @ToDate and TypeID = '230'
			and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods))
	END

	/*Update makh, tenkh, mahang, ten hang vao @tbDataResult*/
	UPDATE @tbDataResult SET MaKH = AO.MaKH, TenKH = AO.TenKH
	FROM (
		SELECT ID as AOID, AccountingObjectCode as MaKH, AccountingObjectName as TenKH FROM dbo.AccountingObject
	) AO
	WHERE AccountObjectID = AO.AOID

	UPDATE @tbDataResult SET Mahang = M.Mahang, Tenhang = M.Tenhang
	FROM (
		SELECT ID as MID, MaterialGoodsCode as Mahang, MaterialGoodsName as Tenhang FROM dbo.MaterialGoods
	) M
	WHERE MaterialGoodsID = M.MID

	/*lay du lieu ra bao cao*/
	SELECT MaKH, TenKH, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, Mahang, Tenhang, DVT, SoLuongMua, DonGia, GiaTriMua, ChietKhau, SoLuongTraLai, GiaTriTraLai, GiaTriGiamGia, RefID, TypeID
	FROM @tbDataResult
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.AccountDefault SET FilterAccount = N'632;642' WHERE ID = 'f1e9fc06-e2c5-4cac-98d1-665b3d748860'
UPDATE dbo.AccountDefault SET FilterAccount = N'111;642' WHERE ID = '88cf7782-e79a-4bf1-8343-86fb6ecb5dd4'
UPDATE dbo.AccountDefault SET FilterAccount = N'112;642' WHERE ID = '420229e0-05e7-4efd-b310-a9d568cd750e'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.TemplateColumn WHERE ID = '229a1814-f968-460b-a077-a38ab31c488f'
DELETE dbo.TemplateColumn WHERE ID = '96994d13-0411-447d-8569-ccad81bdc68b'
DELETE dbo.TemplateColumn WHERE ID = '097990ce-d807-49f1-9b5a-e7fee573a870'

UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'fc001771-2961-45a8-b5ee-15766556ed9b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'be0bd06f-0ed0-4c71-86b7-19072a465d3d'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'cd5039db-c195-46bf-a88c-1a389e4a6ee2'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd7b46838-d8f7-45b9-a963-23b56dc13faa'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '7c1c49d8-7d13-4d64-ad33-26a033c63d95'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c1644b93-fd2b-4834-b678-26efc836b478'
UPDATE dbo.TemplateColumn SET VisiblePosition = 0 WHERE ID = '3d4ebf71-09b3-4eb2-8102-2c271bca5139'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '04d7a1e7-0d74-4620-aa91-2f37e7c1b73e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 5 WHERE ID = '0938bb8e-fd7c-486a-89f9-30e898fb1945'
UPDATE dbo.TemplateColumn SET VisiblePosition = 2 WHERE ID = '7c9d9086-2d19-427f-a4bc-32ebe57998c9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'ab7b298e-0b35-4c32-a364-343202e2a6e3'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '902f37b1-57fd-4107-a2ca-388cb9b59b44'
UPDATE dbo.TemplateColumn SET VisiblePosition = 1 WHERE ID = '33dd86f2-9d23-464a-97da-447384c1075a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 2 WHERE ID = '4fd7391a-5abe-4a96-aad8-4527c103c11a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'c8733753-4295-436d-9a04-5961a330bfc3'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAllOriginal' WHERE ID = '41ccdfde-f4d5-4986-bb73-5be0bf247f69'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'c2ec3b20-47d3-4e9e-ae6a-5f7c0692bd7e'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '1cff1890-dab3-4e2c-bfd0-617c45250bae'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6f14b7ee-5886-400b-ab93-756efc4faa9e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 1 WHERE ID = '44ad4ec6-13f3-489d-a928-7e50c8071f45'
UPDATE dbo.TemplateColumn SET VisiblePosition = 6 WHERE ID = '318f07e3-53b2-471d-a5c3-a3795e063b15'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '6350b467-0756-4927-8bde-aa9afd93fb29'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'b9b17632-4935-4f19-8674-adaac787312b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8b48b6f2-10c5-4fab-a3c6-b4ecd9393426'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAllOriginal', VisiblePosition = 3 WHERE ID = 'de0984bc-f036-47cb-9b3b-b53403e3412f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 7 WHERE ID = '5135b08b-6abe-4acd-b189-bb429585dcee'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ff29c87e-0a29-4aaa-8318-c0fd9345239a'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '48352cf1-3851-47f8-a20c-c61eac15c6c9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'a1aa9060-fff7-416d-9b89-db15fbe4f99b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 6 WHERE ID = '2ae91f54-f126-41cb-a61a-dc0b09913713'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '35214391-f4a1-4b0a-afb9-e77a872b8dd9'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = '6af30839-8f90-4816-9aff-f04ce5db5d61'
UPDATE dbo.TemplateColumn SET VisiblePosition = 5 WHERE ID = '58aca82a-f004-42e0-b13a-f0db4de4af9b'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = 'd75e5765-3ea4-42ab-a727-fd5257eb6460'
UPDATE dbo.TemplateColumn SET VisiblePosition = 0 WHERE ID = '46272da6-8714-4d13-989d-ff31c0482240'

INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('222e5276-0ba1-457e-b79f-45735a65d816', '48cfc152-5b7f-4178-aa1c-2cd9c102706a', N'TotalAllOriginal', N'Số tiền', N'Số tiền', 0, 0, 120, 0, 0, 1, 3)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d81f3202-92f2-4dbb-9312-457e3f0c426e', '48cfc152-5b7f-4178-aa1c-2cd9c102706a', N'TotalAll', N'Quy đổi', NULL, 0, 0, 120, 0, 0, 0, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('0eaabb09-f14b-4753-9dd6-4e5c64709f89', '504758cd-c2ae-490a-a540-57a7741b6c31', N'TotalAll', N'Quy đổi', N'Quy đổi', 0, 0, 120, 0, 0, 0, 4)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('902303ff-f96d-4d5c-9d3b-975d226565eb', '01825aa7-0303-4f6f-82a3-90489a66cb97', N'TotalAll', N'Quy đổi', NULL, 0, 0, 105, 0, 0, 0, 6)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'' WHERE ID = 121
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF