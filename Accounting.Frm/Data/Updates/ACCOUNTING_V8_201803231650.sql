
SET CONCAT_NULL_YIELDS_NULL, ANSI_NULLS, ANSI_PADDING, QUOTED_IDENTIFIER, ANSI_WARNINGS, ARITHABORT, XACT_ABORT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS OFF
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO

CREATE TABLE [dbo].[FRTemplate] (
  [ItemID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_FRTemplate_ItemID] DEFAULT (newid()),
  [ReportID] [varchar](100) NOT NULL,
  [ItemCode] [nvarchar](20) NULL,
  [ItemName] [nvarchar](255) NULL,
  [ItemNameEnglish] [nvarchar](255) NULL,
  [ItemIndex] [int] NULL CONSTRAINT [DF_FRTemplate_ItemIndex] DEFAULT (0),
  [Description] [nvarchar](50) NULL,
  [FormulaType] [int] NULL CONSTRAINT [DF_FRTemplate_UseFormula] DEFAULT (0),
  [FormulaFrontEnd] [nvarchar](max) NULL,
  [Formula] [xml] NULL,
  [Hidden] [bit] NULL CONSTRAINT [DF_FRTemplate_Hidden] DEFAULT (0),
  [IsBold] [bit] NULL CONSTRAINT [DF_FRTemplate_IsBold] DEFAULT (0),
  [IsItalic] [bit] NULL CONSTRAINT [DF_FRTemplate_IsItalic] DEFAULT (0),
  [AccountingSystem] [int] NULL CONSTRAINT [DF_FRTemplate_AccountingSystem] DEFAULT (0),
  [CreatedDate] [datetime] NULL,
  [CreatedBy] [nvarchar](50) NULL,
  [ModifiedDate] [datetime] NULL,
  [ModifiedBy] [nvarchar](50) NULL,
  [Category] [int] NULL,
  CONSTRAINT [PK_FRTemplate] PRIMARY KEY CLUSTERED ([ItemID])
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[FAInit] (
  [ID] [uniqueidentifier] NOT NULL,
  [BranchID] [uniqueidentifier] NULL,
  [TypeID] [uniqueidentifier] NULL,
  [PostedDate] [datetime] NULL,
  [FixedAssetID] [uniqueidentifier] NULL,
  [DepartmentID] [uniqueidentifier] NULL,
  [IncrementDate] [datetime] NULL,
  [DepreciationDate] [datetime] NULL,
  [UsedTime] [decimal] NULL,
  [UsedTimeRemain] [decimal] NULL,
  [OriginalPrice] [money] NULL,
  [PurchasePrice] [money] NULL,
  [AcDepreciationAmount] [money] NULL,
  [RemainingAmount] [money] NULL,
  [MonthPeriodDepreciationAmount] [money] NULL,
  [OriginalPriceAccount] [nvarchar](25) NULL,
  [ExpenditureAccount] [nvarchar](25) NULL,
  [DepreciationAccount] [nvarchar](25) NULL,
  [CustomProperty1] [nvarchar](512) NULL,
  [CustomProperty2] [nvarchar](512) NULL,
  [CustomProperty3] [nvarchar](512) NULL,
  [BudgetItemID] [uniqueidentifier] NULL,
  [CostSetID] [uniqueidentifier] NULL,
  [FixedAssetName] [nvarchar](128) NULL,
  [FixedAssetCode] [nvarchar](128) NULL,
  [StatisticsCodeID] [uniqueidentifier] NULL,
  [IsMonthUsedTime] [bit] NULL,
  [IsMonthUsedTimeRemain] [bit] NULL,
  [FixedAssetCategoryID] [uniqueidentifier] NULL,
  CONSTRAINT [PK_FAInit] PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [AccountingObjectTaxName] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [CustomProperty3] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [OrgPriceOriginal] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [AccountingObjectID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [DiscountAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [ImportTaxExpenseAmountOriginal] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [VATAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [SpecialConsumeTaxAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [VATRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [AmountOriginal] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [FreightAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [AccountingObjectTaxID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [ContractID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [ImportTaxAccount] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [CompanyTaxCode] [nvarchar](50) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [CustomUnitPriceOriginal] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [InvoiceNo] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [DiscountAmountOriginal] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [DiscountRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [SpecialConsumeTaxAmountOriginal] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [ImportTaxAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [ImportTaxAmountOriginal] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [FixedAssetID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [OrgPrice] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [DeductionDebitAccount] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [InvoiceDate] [datetime] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [InvoiceType] [int] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [ImportTaxExpenseAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [GoodsServicePurchaseID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [FreightAmountOriginal] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [SpecialConsumeTaxRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [CustomUnitPrice] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [InvoiceTypeID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [InvoiceSeries] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [ImportTaxRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [VATAmountOriginal] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [IssueBy] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [CurrencyID] [nvarchar](3) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [TotalAmountOriginal] [decimal](19, 4) NULL DEFAULT (0.0000)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [TotalVATAmount] [decimal](19, 4) NULL DEFAULT (0.0000)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [TotalVATAmountOriginal] [decimal](19, 4) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [TotalDiscountAmount] [decimal](19, 4) NULL DEFAULT (0.0000)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [TotalDiscountAmountOriginal] [decimal](19, 4) NULL DEFAULT (0.0000)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [TotalFreightAmount] [decimal](19, 4) NULL DEFAULT (0.0000)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [TotalFreightAmountOriginal] [decimal](19, 4) NULL DEFAULT (0.0000)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [TotalOrgPrice] [decimal](19, 4) NULL DEFAULT (0.0000)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [TotalOrgPriceOriginal] [decimal](19, 4) NULL DEFAULT (0.0000)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [IsImportPurchase] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [CustomProperty1] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [CustomProperty2] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [CustomProperty3] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [EmployeeID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [ExchangeRate] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [PaymentClauseID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [DueDate] [datetime] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [AccountingObjectBankAccount] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [AccountingObjectBankName] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [AccountingObjectID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [AccountingObjectName] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrement]
  ADD [AccountingObjectAddress] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[TIAdjustment]', N'tmp_devart_TIAdjustment', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename table [dbo].[TIAdjustment] to [dbo].[tmp_devart_TIAdjustment]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TIAdjustment] (
  [ID] [uniqueidentifier] NOT NULL,
  [BranchID] [uniqueidentifier] NULL,
  [TypeID] [int] NOT NULL,
  [Date] [datetime] NOT NULL,
  [No] [nvarchar](25) NOT NULL CONSTRAINT [DF__TIAdjustment__No__23BE4960] DEFAULT (NULL),
  [PostedDate] [datetime] NOT NULL CONSTRAINT [DF__TIAdjustm__Poste__24B26D99] DEFAULT (NULL),
  [Reason] [nvarchar](512) NULL,
  [TotalAmount] [decimal](19, 4) NOT NULL CONSTRAINT [DF__TIAdjustm__Total__25A691D2] DEFAULT (NULL),
  [Recorded] [bit] NOT NULL CONSTRAINT [DF__TIAdjustm__Recor__269AB60B] DEFAULT (NULL),
  [Exported] [bit] NOT NULL CONSTRAINT [DF__TIAdjustm__Expor__278EDA44] DEFAULT (NULL),
  [TemplateID] [uniqueidentifier] NULL
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

INSERT [dbo].[TIAdjustment](ID, Date, No, PostedDate, Reason, TotalAmount, Recorded, Exported, TemplateID)
  SELECT ID, Date, No, PostedDate, Reason, TotalAmount, Recorded, Exported, TemplateID FROM [dbo].[tmp_devart_TIAdjustment] WITH (NOLOCK)

if @@ERROR = 0
  DROP TABLE [dbo].[tmp_devart_TIAdjustment]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIAdjustment]
  ADD CONSTRAINT [PK__TIAdjust__3214EC27FDF7957F] PRIMARY KEY CLUSTERED ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[FAAuditMemberDetail].[AccountObjectName]', N'AccountingObjectName', 'COLUMN'
IF @res <> 0
  RAISERROR ('Error while Rename column [AccountObjectName] to [AccountingObjectName] on table [dbo].[FAAuditMemberDetail]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[FAAuditMemberDetail]
  ADD [AccountingObjectID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[FAAuditDetail].[Depreciation]', N'DepreciationAmount', 'COLUMN'
IF @res <> 0
  RAISERROR ('Error while Rename column [Depreciation] to [DepreciationAmount] on table [dbo].[FAAuditDetail]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[EMContractDetailMG]
  ADD [SAOrderID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[EMContractDetailMG]
  ADD [UnitPrice] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
/*
--region [dbo].[Proc_GetBAListOfBalance]

------------------------------------------------------------------------------------------------------------------------
-- Generated By:   nmtruong 27/9/2014
-- Template:       Sổ tiền gửi ngân hàng
-- Procedure Name: [dbo].[Proc_BAR_GetBAListOfBalance] NULL,'112',NULL,NULL,'1/1/2014','12/30/2014',1,1
-- vhanh modified 08.03.2016: Thêm cột tên chi nhánh
------------------------------------------------------------------------------------------------------------------------
*/
ALTER PROCEDURE [dbo].[Proc_BA_GetOverBalanceBook]
    @BranchID UNIQUEIDENTIFIER = NULL ,
    @AccountNumber NVARCHAR(25) = NULL ,
    @BankAccountID UNIQUEIDENTIFIER = NULL ,
    @CurrencyID NVARCHAR(3) = NULL ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @IsWorkingWithManagementBook BIT ,
    @IncludeDependentBranch BIT
AS 
    SET NOCOUNT ON		

    DECLARE @AccountNumberPercent NVARCHAR(26)
    SET @AccountNumberPercent = @AccountNumber + '%'
    SELECT  ROW_NUMBER() OVER ( ORDER BY B.BankName, BA.BankAccount ) AS RowNum ,
            BA.ID,
            BA.BankAccount,
            B.BankName,
			BA.BankBranchName ,
            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal- GL.CreditAmountOriginal ELSE 0.0 END) AS OpenAmountOC ,
            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0.0 END) AS OpenAmount ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.DebitAmountOriginal ELSE 0.0 END) AS DebitAmountOC ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN  GL.DebitAmount ELSE 0.0 END) AS DebitAmount ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmountOriginal ELSE 0.0 END) AS CreditAmountOC ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmount ELSE 0.0 END) AS CreditAmount ,
            SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) AS CloseAmountOC ,
            SUM(GL.DebitAmount - GL.CreditAmount) AS CloseAmount
    FROM    dbo.GeneralLedger GL
            INNER JOIN dbo.BankAccountDetail BA ON GL.BankAccountDetailID = BA.ID          
            INNER JOIN dbo.Bank B ON BA.BankID =  B.ID
    WHERE   GL.PostedDate <= @ToDate
            AND ( GL.Account LIKE @AccountNumberPercent )
            AND ( GL.BankAccountDetailID = @BankAccountID
                  OR @BankAccountID IS NULL
                )
            AND ( @CurrencyID IS NULL
                  OR GL.CurrencyID = @CurrencyID
                )
            
    GROUP BY BA.ID, 
			BA.BankAccount ,
            B.BankName,BA.BankBranchName
    HAVING
			SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal- GL.CreditAmountOriginal ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.DebitAmountOriginal ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN  GL.DebitAmount ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmountOriginal ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmount ELSE 0.0 END)<>0 OR 
            SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal)<>0 OR 
            SUM(GL.DebitAmount - GL.CreditAmount)<>0
            
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
/*
------------------------------------------------------------------------------------------------------------------------
-- Creation by : HaiPL
-- Created Date : 19102017
-- So tien gui ngan hang
-- Procedure Name: [dbo].[Proc_BA_GetBookDepositListDetail] NULL,'112',NULL,NULL,'1/1/2014','12/30/2014',1,1
------------------------------------------------------------------------------------------------------------------------
*/
ALTER PROCEDURE [dbo].[Proc_BA_GetBookDepositListDetail_Bck]
    @BranchID UNIQUEIDENTIFIER = NULL ,
    @AccountNumber NVARCHAR(25) = NULL ,
    @BankAccountID UNIQUEIDENTIFIER = NULL ,
    @CurrencyID NVARCHAR(3) = NULL ,
    @FromDate DATETIME ,
    @ToDate DATETIME 
AS 
    SET NOCOUNT ON		

    DECLARE @AccountNumberPercent NVARCHAR(26)
    SET @AccountNumberPercent = @AccountNumber + '%'
    SELECT  ROW_NUMBER() OVER ( ORDER BY B.BankName, BA.BankAccount ) AS RowNum ,
            BA.ID,
            BA.BankAccount,
            B.BankName,
            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal- GL.CreditAmountOriginal ELSE 0.0 END) AS OpenAmountOC ,
            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0.0 END) AS OpenAmount ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.DebitAmountOriginal ELSE 0.0 END) AS DebitAmountOC ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN  GL.DebitAmount ELSE 0.0 END) AS DebitAmount ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmountOriginal ELSE 0.0 END) AS CreditAmountOC ,
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmount ELSE 0.0 END) AS CreditAmount ,
            SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) AS CloseAmountOC ,
            SUM(GL.DebitAmount - GL.CreditAmount) AS CloseAmount
    FROM    dbo.GeneralLedger GL
            INNER JOIN dbo.BankAccountDetail BA ON GL.BankAccountDetailID = BA.ID          
            INNER JOIN dbo.Bank B ON BA.BankID =  B.ID
    WHERE   GL.PostedDate <= @ToDate
            AND ( GL.Account LIKE @AccountNumberPercent )
            AND ( GL.BankAccountDetailID = @BankAccountID
                  OR @BankAccountID IS NULL
                )
            AND ( @CurrencyID IS NULL
                  OR GL.CurrencyID = @CurrencyID
                )
            
    GROUP BY BA.ID, 
			BA.BankAccount ,
            B.BankName
    HAVING
			SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal- GL.CreditAmountOriginal ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.DebitAmountOriginal ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN  GL.DebitAmount ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmountOriginal ELSE 0.0 END)<>0 OR 
            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmount ELSE 0.0 END)<>0 OR 
            SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal)<>0 OR 
            SUM(GL.DebitAmount - GL.CreditAmount)<>0
            
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
/*
------------------------------------------------------------------------------------------------------------------------
-- Created By:  nmtruong 22/12/2017
-- Procedure Name: [dbo].[Proc_BA_GetBookDepositListDetail]
-- Description: Sổ tiền gửi ngân hàng
*/
ALTER PROCEDURE [dbo].[Proc_BA_GetBookDepositListDetail]
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @CurrencyID NVARCHAR(3) ,
    @AccountNumber NVARCHAR(25) ,
    @BankAccountID UNIQUEIDENTIFIER ,
    @IsSimilarSum BIT = 0 ,
    @IsSoftOrderVoucher BIT = 0
AS
    BEGIN
        SET NOCOUNT ON
        
        CREATE TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
              BankAccount NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              RefType INT ,
              JournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CorrespondingAccountNumber NVARCHAR(20)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              ExchangeRate DECIMAL ,
              DebitAmountOC DECIMAL ,
              DebitAmount DECIMAL ,
              CreditAmountOC DECIMAL ,
              CreditAmount DECIMAL ,
              ClosingAmountOC DECIMAL ,
              ClosingAmount DECIMAL ,
              AccountObjectCode NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectName NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              EmployeeCode NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              EmployeeName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              ProjectWorkCode NVARCHAR(20)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              ProjectWorkName NVARCHAR(128)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              
              ExpenseItemCode NVARCHAR(20)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              ExpenseItemName NVARCHAR(128)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              ListItemCode NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              ListItemName NVARCHAR(128) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              ContractCode NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS ,					

              BranchID UNIQUEIDENTIFIER ,
              BranchCode NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              BranchName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              IsBold BIT ,
              PaymentType INT ,
              OrderType INT ,
              RefOrder INT
            )	
		
        CREATE TABLE #Result1
            (
              RowNum INT PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
              BankAccount NVARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255),
              CorrespondingAccountNumber NVARCHAR(20) ,
              ExchangeRate DECIMAL ,
              DebitAmountOC DECIMAL ,
              DebitAmount DECIMAL ,
              CreditAmountOC DECIMAL ,
              CreditAmount DECIMAL ,
              ClosingAmountOC DECIMAL ,
              ClosingAmount DECIMAL ,
              AccountObjectCode NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectName NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              EmployeeCode NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              EmployeeName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,

              ProjectWorkCode NVARCHAR(20) ,
              ProjectWorkName NVARCHAR(128) ,
              
              ExpenseItemCode NVARCHAR(20) ,
              ExpenseItemName NVARCHAR(128) ,
              ListItemCode NVARCHAR(20) ,
              ListItemName NVARCHAR(128)  ,
              ContractCode NVARCHAR(50) ,					

              BranchID UNIQUEIDENTIFIER ,
              BranchCode NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              BranchName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              IsBold BIT ,
              PaymentType INT ,
              OrderType INT ,
              RefOrder INT
            )	
		
        DECLARE @BankAccountAll UNIQUEIDENTIFIER
        SET @BankAccountAll = 'A0624CFA-D105-422f-BF20-11F246704DC3'
		
        DECLARE @AccountNumberPercent NVARCHAR(25)
        SET @AccountNumberPercent = @AccountNumber + '%'
    
        DECLARE @ClosingAmountOC DECIMAL(29, 4)
        DECLARE @ClosingAmount DECIMAL(29, 4)
   	    DECLARE @tblListBankAccountDetail TABLE
            (
			  ID UNIQUEIDENTIFIER,
              BankAccount NVARCHAR(Max) ,
              BankAccountName NVARCHAR(MAX) 
            ) 
        if(@BankAccountID is null)     
        INSERT  INTO @tblListBankAccountDetail
                SELECT  BAD.ID,
				        BAD.BankAccount,
				        BAD.BankName
                FROM    BankAccountDetail BAD,
				        Bank BA
                WHERE BAD.BankID = BA.ID    
		else 	
			INSERT  INTO @tblListBankAccountDetail
                SELECT  BAD.ID,
				        BAD.BankAccount,
				        BAD.BankName
                FROM    BankAccountDetail BAD,
				        Bank BA
                WHERE BAD.BankID = BA.ID  
				and BAD.BankAccount =  @BankAccountID 
	
        IF @ClosingAmount IS NULL
            SET @ClosingAmount = 0
        IF @ClosingAmountOC IS NULL
            SET @ClosingAmountOC = 0
      
        INSERT  #Result
                ( BankAccount ,
                  JournalMemo ,
                  DebitAmount ,
                  DebitAmountOC ,
                  CreditAmount ,
                  CreditAmountOC ,
                  ClosingAmountOC ,
                  ClosingAmount ,
                  IsBold ,
                  OrderType    
			  
                )
                SELECT  
		                BA.BankAccount + ' - ' + BA.BankAccountName,
                        N'Số dư đầu kỳ' AS JournalMemo ,
                        $0 AS DebitAmountOC ,
                        $0 AS DebitAmount ,
                        $0 AS CreditAmountOC ,
                        $0 AS CreditAmount ,
                        ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) ,
                        ISNULL(SUM(GL.DebitAmount - GL.CreditAmount), 0) ,
                        1 ,
                        0
                FROM    dbo.GeneralLedger AS GL
				        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID
                WHERE   ( GL.PostedDate < @FromDate )
                        AND GL.Account LIKE @AccountNumberPercent
                        AND ( @CurrencyID IS NULL
                              OR GL.CurrencyID = @CurrencyID
                            )
                GROUP BY BA.BankAccount , BA.BankAccountName
                HAVING  ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) <> 0
                        OR ISNULL(SUM(GL.DebitAmount - GL.CreditAmount), 0) <> 0

       INSERT  #Result
	   (
              RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder )
                    SELECT  GL.ReferenceID ,
	                        BA.BankAccount + ' - ' + BA.BankAccountName,
                            GL.PostedDate ,
                            GL.RefDate ,
                            GL.RefNo AS RefNoFinance ,
                            GL.Description,
                            GL.AccountCorresponding ,
                            GL.ExchangeRate ,
                            ISNULL(GL.DebitAmountOriginal, 0) AS DebitAmountOC ,
                            ISNULL(GL.DebitAmount, 0) AS DebitAmount ,
                            ISNULL(GL.CreditAmountOriginal, 0) AS CreditAmountOC ,
                            ISNULL(GL.CreditAmount, 0) AS CreditAmount ,
                            $0 AS ClosingAmountOC ,
                            $0 AS ClosingAmount ,
                            AO.AccountingObjectCode ,
                            AO.AccountingObjectName AS AccountObjectName ,
                            EI.ExpenseItemCode ,
                            EI.ExpenseItemName ,
                            GL.BranchID ,
                            0 ,
                            CASE WHEN GL.DebitAmount <> 0 THEN 0
                                 ELSE 1
                            END AS PaymentType ,
                            1 ,
                            0
                    FROM    dbo.GeneralLedger AS GL
					        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID
                            LEFT JOIN dbo.AccountingObject AO ON GL.AccountingObjectID = AO.ID
                            LEFT JOIN dbo.ExpenseItem EI ON EI.ID = GL.ExpenseItemID
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND GL.Account LIKE @AccountNumberPercent
                            AND ( @CurrencyID IS NULL
                                  OR GL.CurrencyID = @CurrencyID
                                )
                            AND ( GL.DebitAmountOriginal <> 0
                                  OR GL.CreditAmountOriginal <> 0
                                  OR GL.DebitAmount <> 0
                                  OR GL.CreditAmount <> 0
                                )
                    ORDER BY BA.BankAccount ,
                            PostedDate ,
                            RefDate ,
                            PaymentType ,
                            RefNo
                     
          
   
        DECLARE @BankAccount NVARCHAR(500)
        DECLARE @CloseAmountOC MONEY
        DECLARE @CloseAmount MONEY
        SET @BankAccount = NULL
        INSERT  INTO #Result1
		(     RowNum,
		      RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder)
                SELECT  ROW_NUMBER() OVER ( ORDER BY BankAccount , OrderType, PostedDate , CASE
                                                              WHEN @IsSoftOrderVoucher = 1
                                                              THEN RefOrder
                                                              ELSE 0
                                                              END, RefDate , PaymentType , RefNo ) , 
                        RefID ,
                        BankAccount ,
                        PostedDate ,
                        RefDate ,
                        RefNo ,
                        JournalMemo ,
                        CorrespondingAccountNumber ,
                        ExchangeRate ,
                        DebitAmountOC ,
                        DebitAmount ,
                        CreditAmountOC ,
                        CreditAmount ,
                        ClosingAmountOC ,
                        ClosingAmount ,
                        AccountObjectCode ,
                        AccountObjectName ,
                        ExpenseItemCode ,
                        ExpenseItemName ,
                        BranchID ,
						IsBold  ,
					    PaymentType  ,
						OrderType  ,
						RefOrder
                FROM    #Result
                ORDER BY BankAccount ,
                        OrderType ,
                        PostedDate ,
                        CASE WHEN @IsSoftOrderVoucher = 1 THEN RefOrder
                             ELSE 0
                        END ,
                        RefDate ,
                        PaymentType ,
                        RefNo
        
      
        SET @CloseAmount = 0
        SET @CloseAmountOC = 0
      
        UPDATE  #Result1
        SET     
                @CloseAmount = ( CASE WHEN OrderType = 0
                                      THEN ( CASE WHEN ClosingAmount = 0
                                                  THEN 0
                                                  ELSE ClosingAmount
                                             END )
                                      WHEN @BankAccount <> BankAccount
                                      THEN DebitAmount - CreditAmount
                                      ELSE @CloseAmount + DebitAmount
                                           - CreditAmount
                                 END ) ,
                @CloseAmountOC = ( CASE WHEN OrderType = 0
                                        THEN ( CASE WHEN ClosingAmountOC = 0
                                                    THEN 0
                                                    ELSE ClosingAmountOC
                                               END )
                                        WHEN @BankAccount <> BankAccount
                                        THEN DebitAmountOC - CreditAmountOC
                                        ELSE @CloseAmountOC + DebitAmountOC
                                             - CreditAmountOC
                                   END ) ,
                ClosingAmount = @CloseAmount ,
                ClosingAmountOC = @CloseAmountOC ,
                @BankAccount = BankAccount
        
        SELECT  *
        FROM    #Result1
        ORDER BY BankAccount ,
                OrderType ,
                PostedDate ,
                CASE WHEN @IsSoftOrderVoucher = 1 THEN RefOrder
                     ELSE 0
                END ,
                RefDate ,
                PaymentType ,
                RefNo
     
        DROP TABLE #Result
        DROP TABLE #Result1
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
/*
-- =============================================
-- Author:		haipl
-- Create date: <15.03.2018>
-- Description:	Lấy lên Báo cáo bảng cân đối kế toán
-- Lam mở để sau theo QD 15/200

-- =============================================
*/
CREATE FUNCTION [dbo].[Func_GetB01_DN]
    (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT,
      @IsB01bDNN BIT ,
      @isPrintByYear BIT,
      @PrevFromDate DATETIME,
      @PrevToDate DATETIME
    )
RETURNS @Result TABLE
    (
      ItemID UNIQUEIDENTIFIER ,
      ItemCode NVARCHAR(25) ,
      ItemName NVARCHAR(255) ,
      ItemNameEnglish NVARCHAR(255) ,
      ItemIndex INT ,
      Description NVARCHAR(255) ,
      FormulaType INT ,
      FormulaFrontEnd NVARCHAR(MAX) ,
      Formula XML ,
      Hidden BIT ,
      IsBold BIT ,
      IsItalic BIT ,
      Category INT ,
      SortOrder INT
      ,
      Amount DECIMAL(25, 4) ,
      PrevAmount DECIMAL(25, 4)
    )

    BEGIN 
            DECLARE @AccountingSystem INT	
            SET @AccountingSystem = 48 
    
            DECLARE @SubAccountSystem INT
            SELECT  @SubAccountSystem = 133
	
            DECLARE @ReportID NVARCHAR(100)
            SET @ReportID = '1'

            IF @AccountingSystem = 48
                AND @SubAccountSystem = 133
                AND @IsB01bDNN = 0
                BEGIN

                    SET @ReportID = '7'	
                END

            DECLARE @ItemForeignCurrency UNIQUEIDENTIFIER
            DECLARE @ItemIndex INT
            DECLARE @MainCurrency NVARCHAR(3)
            IF @AccountingSystem = 48
                AND @SubAccountSystem <> 133
                SET @ItemForeignCurrency = '6D6BA7EB-E60A-46ED-A556-E674709F5466'
            ELSE
                SET @ItemForeignCurrency = '00000000-0000-0000-0000-000000000000'	
	
            SET @ItemIndex = ( SELECT TOP 1
                                        ItemIndex
                               FROM     FRTemplate
                               WHERE    ItemID = @ItemForeignCurrency
                             )
            SET @MainCurrency = 'VND'
            DECLARE @tblItem TABLE
                (
                  ItemID UNIQUEIDENTIFIER ,
                  ItemIndex INT ,
                  ItemName NVARCHAR(255) ,
                  OperationSign INT ,
                  OperandString NVARCHAR(255) ,
                  AccountNumberPercent NVARCHAR(25) ,
                  AccountNumber NVARCHAR(25) ,
                  CorrespondingAccountNumber VARCHAR(25) ,
                  IsDetailByAO INT ,
                  AccountKind INT
                )
	
            INSERT  @tblItem
                    SELECT  ItemID ,
                            ItemIndex ,
                            ItemName ,
                            x.r.value('@OperationSign', 'INT') ,
                            RTRIM(LTRIM(x.r.value('@OperandString',
                                                  'nvarchar(255)'))) ,
                            x.r.value('@AccountNumber', 'nvarchar(25)') + '%' ,
                            x.r.value('@AccountNumber', 'nvarchar(25)') ,
                            CASE WHEN x.r.value('@CorrespondingAccountNumber',
                                                'nvarchar(25)') <> ''
                                 THEN x.r.value('@CorrespondingAccountNumber',
                                                'nvarchar(25)') + '%'
                                 ELSE ''
                            END ,
                            CASE WHEN x.r.value('@OperandString',
                                                'nvarchar(255)') LIKE '%ChitietTheoTKvaDoituong'
                                 THEN 1
                                 ELSE CASE WHEN x.r.value('@OperandString',
                                                          'nvarchar(255)') LIKE '%ChitietTheoTK'
                                           THEN 2
                                           ELSE 0
                                      END
                            END ,
                            NULL
                    FROM    dbo.FRTemplate
                            CROSS APPLY Formula.nodes('/root/DetailFormula')
                            AS x ( r )
                    WHERE   AccountingSystem = @AccountingSystem
                            AND ReportID = @ReportID
                            AND FormulaType = 0
                            AND Formula IS NOT NULL
                            AND ItemID <> @ItemForeignCurrency
	
            UPDATE  @tblItem
            SET     AccountKind = A.AccountGroupKind
            FROM    dbo.Account A
            WHERE   A.AccountNumber = [@tblItem].AccountNumber
		
            DECLARE @AccountBalance TABLE
                (
                  AccountNumber NVARCHAR(20) ,
                  AccountCategoryKind INT ,
                  IsDetailByAO INT 
	            )
	 
            INSERT  @AccountBalance
                    SELECT 
		DISTINCT            A.AccountNumber ,
                            A.AccountGroupKind ,
                            B.IsDetailByAO
                    FROM    dbo.Account A
                            INNER JOIN @tblItem B ON A.AccountNumber LIKE B.AccountNumberPercent
	
            DECLARE @Balance TABLE
                (
                  AccountNumber NVARCHAR(20) ,
                  AccountCategoryKind INT ,
                  AccountObjectID UNIQUEIDENTIFIER ,
                  IsDetailByAO INT ,
                  OpeningDebit DECIMAL(25, 4) ,
                  OpeningCredit DECIMAL(25, 4) ,
                  ClosingDebit DECIMAL(25, 4) ,
                  ClosingCredit DECIMAL(25, 4) ,
                  BranchID UNIQUEIDENTIFIER
                )
            DECLARE @GeneralLedger TABLE
                (
                  AccountNumber NVARCHAR(20) ,
                  AccountObjectID UNIQUEIDENTIFIER ,
                  BranchID UNIQUEIDENTIFIER ,
                  IsOPN BIT ,
                  DebitAmount DECIMAL(25, 4) ,
                  CreditAmount DECIMAL(25, 4)
                )
	
            INSERT  INTO @GeneralLedger
                    ( AccountNumber ,
                      AccountObjectID ,
                      BranchID ,
                      IsOPN ,
                      DebitAmount ,
                      CreditAmount
	                )
                    SELECT  GL.Account ,
                            GL.AccountingObjectID ,
                            GL.BranchID ,
                            CASE WHEN GL.PostedDate < @FromDate THEN 1
                                 ELSE 0
                            END ,
                            SUM(DebitAmount) AS DebitAmount ,
                            SUM(CreditAmount) AS CreditAmount
                    FROM    dbo.GeneralLedger GL
                    WHERE   PostedDate <= @ToDate
                    GROUP BY GL.Account ,
                            GL.AccountingObjectID ,
                            GL.BranchID ,
                            CASE WHEN GL.PostedDate < @FromDate THEN 1
                                 ELSE 0
                            END
	
	
            DECLARE @StartDate DATETIME
			SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'
	 
            DECLARE @GeneralLedger421 TABLE
                (
                  OperandString NVARCHAR(255) ,
                  ClosingAmount DECIMAL(25, 4) ,
                  OpeningAmount DECIMAL(25, 4)
                )
	
	
	
	
	/* 
	 

Đối với báo cáo kỳ khác năm:
*Cuối kỳ: (Dư Có – Dư Nợ) TK 4211 trên sổ cái tính đến Từ ngày -1 + (Dư Có – Dư Nợ) TK 4212 trên sổ cái tính đến Từ ngày -1 
* Đầu kỳ: 
(Dư Có – Dư Nợ) TK 4211 trên sổ cái tính đến từ ngày -1 của kỳ trước liền kề+ (Dư Có – Dư Nợ) TK 4212 trên sổ cái tính đến Từ ngày -1 của kỳ trước liền kề. 
→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, 
VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10) thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4211 – Dư Nợ TK 4211 trên Sổ cái tính đến Từ ngày -1 

	 */
	 /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
            INSERT  INTO @GeneralLedger421
                    ( OperandString ,
                      ClosingAmount ,
                      OpeningAmount
	                )
                    SELECT  N'Solieuchitieu421a_Ky_khac_nam' AS AccountNumber ,
                            ISNULL( SUM(CASE WHEN GL.Account LIKE '4211%'
                                          AND GL.PostedDate < @FromDate
                                     THEN CreditAmount - DebitAmount
                                     ELSE 0
                                END),0)
                         
                            + ISNULL( SUM(CASE WHEN GL.Account LIKE '4212%'
                                            AND GL.PostedDate < @FromDate
                                       THEN CreditAmount -DebitAmount
                                       ELSE 0
                                  END),0)
                           AS ClosingAmount ,
                                  
                            ISNULL( SUM(
									CASE WHEN DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0 ELSE 
										CASE WHEN GL.Account LIKE '4211%'
													  AND GL.PostedDate < @PrevFromDate
												 THEN CreditAmount -DebitAmount
												 ELSE 0
											END
									end		
                                ),0)
                         
                            + SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0
                                       ELSE CASE WHEN GL.Account LIKE '4212%'
                                                      AND GL.PostedDate < @PrevFromDate
                                                 THEN CreditAmount -DebitAmount
                                                 ELSE 0
                                            END
                                  END)
                     
                        +   ISNULL(SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate) 
									  AND GL.Account LIKE '4211%'
												 THEN CASE WHEN GL.PostedDate < @FromDate then CreditAmount - DebitAmount ELSE 0 end
									 ELSE 0
								END)
								,0) AS OpeningAmount                                  
                                  
                    FROM    dbo.GeneralLedger GL
                    WHERE   PostedDate <= @ToDate
                            AND gl.Account LIKE '421%'
	
	/*
	

Đối với báo cáo kỳ khác năm:
* Cuối kỳ: PS Có TK 4212 trên sổ cái trong kỳ báo cáo (không bao gồm ĐU N4211/Có TK 4212) - PSN TK 4212 trên sổ cái trong kỳ báo cáo (không bao gồm ĐU N4212/Có TK 4211)
* Đầu kỳ: 
PS Có TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4211/Có TK 4212) – PS Nợ TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4212/Có TK 4211)
→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10) thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4212 – Dư Nợ TK 4212 trên Sổ cái tính đến Từ ngày -1 

	*/
	/*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
	
            INSERT  INTO @GeneralLedger421
                    ( OperandString ,
                      ClosingAmount ,
                      OpeningAmount
	                )
                    SELECT  N'Solieuchitieu421b_Ky_khac_nam' AS AccountNumber ,
                           ISNULL( SUM(CASE WHEN GL.Account LIKE '4212%'
                                          AND GL.PostedDate BETWEEN @fromdate AND @ToDate
                                     THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                 THEN 0
                                                 ELSE CreditAmount
                                            END )
                                     ELSE 0
                                END),0)
                            - ISNULL( SUM(CASE WHEN GL.Account LIKE '4212%'
                                            AND GL.PostedDate BETWEEN @Fromdate AND @ToDate
                                       THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                   THEN 0
                                                   ELSE DebitAmount
                                              END )
                                       ELSE 0
                                  END),0) AS ClosingAmount ,
		 /** Đầu kỳ: 
PS Có TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4211/Có TK 4212) – PS Nợ TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4212/Có TK 4211)
→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10)
 thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4212 – Dư Nợ TK 4212 trên Sổ cái tính đến Từ ngày -1 
*/
                            ISNULL( SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0
                                     ELSE ( CASE WHEN GL.Account LIKE '4212%'
                                                      AND GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                                 THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                             THEN 0
                                                             ELSE CreditAmount
                                                        END )
                                                 ELSE 0
                                            END )
                                END),0)
                            - ISNULL(  SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0
                                       ELSE CASE WHEN GL.Account LIKE '4212%'
                                                      AND GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                                 THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                             THEN 0
                                                             ELSE DebitAmount
                                                        END )
                                                 ELSE 0
                                            END
                                  END),0)
                            + /*nếu không  là kỳ đầu tiên
		→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, 
		VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10) thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4212 – Dư Nợ TK 4212 trên Sổ cái tính đến Từ ngày -1 
		*/
			ISNULL(SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate) 
                          AND GL.Account LIKE '4212%'
                     THEN CASE WHEN GL.PostedDate < @FromDate then CreditAmount - DebitAmount ELSE 0 end
                     ELSE 0
                END),0) AS OpeningAmount
                    FROM    dbo.GeneralLedger GL
                    WHERE   PostedDate <= @ToDate
                            AND gl.Account LIKE '421%'
	
            INSERT  INTO @Balance
                    SELECT  GL.AccountNumber ,
                            A.AccountCategoryKind ,
                            CASE WHEN A.IsDetailByAO = 1
                                 THEN GL.AccountObjectID
                                 ELSE NULL
                            END AS AccountObjectID ,
                            A.IsDetailByAO ,
                            SUM(CASE WHEN GL.IsOPN = 1
                                     THEN DebitAmount - CreditAmount
                                     ELSE 0
                                END) OpeningDebit ,
                            0 ,
                            SUM(DebitAmount - CreditAmount) ClosingDebit ,
                            0
                            ,
                            CASE WHEN A.IsDetailByAO = 1
                                      AND @IsSimilarBranch = 0
                                 THEN GL.BranchID
                                 ELSE NULL
                            END AS BranchID
                    FROM    @GeneralLedger GL
                            INNER JOIN @AccountBalance A ON GL.AccountNumber = A.AccountNumber
                    GROUP BY GL.AccountNumber ,
                            A.AccountCategoryKind ,
                            CASE WHEN A.IsDetailByAO = 1
                                 THEN GL.AccountObjectID
                                 ELSE NULL
                            END ,
                            A.IsDetailByAO
                            ,
                            CASE WHEN A.IsDetailByAO = 1
                                      AND @IsSimilarBranch = 0
                                 THEN GL.BranchID
                                 ELSE NULL
                            END
            OPTION  ( RECOMPILE )
            UPDATE  @Balance
            SET     OpeningCredit = -OpeningDebit ,
                    OpeningDebit = 0
            WHERE   AccountCategoryKind = 1
                    OR ( AccountCategoryKind NOT IN ( 0, 1 )
                         AND OpeningDebit < 0
                       )
			
            UPDATE  @Balance
            SET     ClosingCredit = -ClosingDebit ,
                    ClosingDebit = 0
            WHERE   AccountCategoryKind = 1
                    OR ( AccountCategoryKind NOT IN ( 0, 1 )
                         AND ClosingDebit < 0
                       )	
	
            DECLARE @tblMasterDetail TABLE
                (
                  ItemID UNIQUEIDENTIFIER ,
                  DetailItemID UNIQUEIDENTIFIER ,
                  OperationSign INT ,
                  Grade INT ,
                  OpeningAmount DECIMAL(25, 4) ,
                  ClosingAmount DECIMAL(25, 4)
                )	
	
	
            INSERT  INTO @tblMasterDetail
                    SELECT  I.ItemID ,
                            NULL ,
                            1 ,
                            -1
                            ,
                            SUM(( CASE I.OperandString
                                    WHEN 'DUNO'
                                    THEN
                                         CASE I.AccountKind
                                           WHEN 1 THEN 0
                                           WHEN 0
                                           THEN I.OpeningDebit
                                                - I.OpeningCredit
                                           ELSE CASE WHEN I.OpeningDebit
                                                          - I.OpeningCredit > 0
                                                     THEN I.OpeningDebit
                                                          - I.OpeningCredit
                                                     ELSE 0
                                                END
                                         END
                                         /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUNO_ky_nam'
                                    THEN 
                                         CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.AccountKind
                                                     WHEN 1 THEN 0
                                                     WHEN 0
                                                     THEN I.OpeningDebit
                                                          - I.OpeningCredit
                                                     ELSE CASE
                                                              WHEN I.OpeningDebit
                                                              - I.OpeningCredit > 0
                                                              THEN I.OpeningDebit
                                                              - I.OpeningCredit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'DUCO'
                                    THEN CASE I.AccountKind
                                           WHEN 0 THEN 0
                                           WHEN 1
                                           THEN I.OpeningCredit
                                                - I.OpeningDebit
                                           ELSE CASE WHEN I.OpeningCredit
                                                          - I.OpeningDebit > 0
                                                     THEN I.OpeningCredit
                                                          - I.OpeningDebit
                                                     ELSE 0
                                                END
                                         END
                                         /* Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUCO_ky_nam'
                                    THEN CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.AccountKind
                                                     WHEN 0 THEN 0
                                                     WHEN 1
                                                     THEN I.OpeningCredit
                                                          - I.OpeningDebit
                                                     ELSE CASE
                                                              WHEN I.OpeningCredit
                                                              - I.OpeningDebit > 0
                                                              THEN I.OpeningCredit
                                                              - I.OpeningDebit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421a_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.OpeningAmount
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421b_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.OpeningAmount
                                              ELSE 0
                                         END
                                         /* - Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    ELSE CASE WHEN LEFT(I.OperandString, 4) = 'DUNO'
                                              THEN I.OpeningDebit
                                              ELSE I.OpeningCredit
                                         END
                                  END ) * I.OperationSign) AS OpeningAmount ,
                            SUM(( CASE I.OperandString
                                    WHEN 'DUNO'
                                    THEN CASE I.Accountkind
                                           WHEN 1 THEN 0
                                           WHEN 0
                                           THEN I.ClosingDebit
                                                - I.ClosingCredit
                                           ELSE CASE WHEN I.ClosingDebit
                                                          - I.ClosingCredit > 0
                                                     THEN I.ClosingDebit
                                                          - I.ClosingCredit
                                                     ELSE 0
                                                END
                                         END
                                         /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUNO_ky_nam'
                                    THEN CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.Accountkind
                                                     WHEN 1 THEN 0
                                                     WHEN 0
                                                     THEN I.ClosingDebit
                                                          - I.ClosingCredit
                                                     ELSE CASE
                                                              WHEN I.ClosingDebit
                                                              - I.ClosingCredit > 0
                                                              THEN I.ClosingDebit
                                                              - I.ClosingCredit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'DUCO'
                                    THEN CASE I.AccountKind
                                           WHEN 0 THEN 0
                                           WHEN 1
                                           THEN I.ClosingCredit
                                                - I.ClosingDebit
                                           ELSE CASE WHEN I.ClosingCredit
                                                          - I.ClosingDebit > 0
                                                     THEN I.ClosingCredit
                                                          - I.ClosingDebit
                                                     ELSE 0
                                                END
                                         END
                                         /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUCO_ky_nam'
                                    THEN CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.AccountKind
                                                     WHEN 0 THEN 0
                                                     WHEN 1
                                                     THEN I.ClosingCredit
                                                          - I.ClosingDebit
                                                     ELSE CASE
                                                              WHEN I.ClosingCredit
                                                              - I.ClosingDebit > 0
                                                              THEN I.ClosingCredit
                                                              - I.ClosingDebit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421a_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.ClosingAmount
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421b_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.ClosingAmount
                                              ELSE 0
                                         END
                                         /* - Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    ELSE CASE WHEN LEFT(I.OperandString, 4) = 'DUNO'
                                              THEN I.ClosingDebit
                                              ELSE I.ClosingCredit
                                         END
                                  END ) * I.OperationSign) AS ClosingAmount
                    FROM    ( SELECT    I.ItemID ,
                                        I.OperandString ,
                                        I.OperationSign ,
                                        I.AccountNumber ,
                                        I.AccountKind ,
                                        SUM(B.OpeningDebit) AS OpeningDebit ,
                                        SUM(B.OpeningCredit) AS OpeningCredit ,
                                        SUM(B.ClosingDebit) AS ClosingDebit ,
                                        SUM(B.ClosingCredit) AS ClosingCredit
                              FROM      @tblItem I
                                        INNER JOIN @Balance B ON B.AccountNumber LIKE I.AccountNumberPercent
                                                              AND B.IsDetailByAO = I.IsDetailByAO
                              GROUP BY  I.ItemID ,
                                        I.OperandString ,
                                        I.OperationSign ,
                                        I.AccountNumber ,
                                        I.AccountKind
                            ) I
                            /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                            LEFT JOIN @GeneralLedger421 C ON I.OperandString = C.OperandString
                    GROUP BY I.ItemID			
	
            INSERT  @tblMasterDetail
                    SELECT  ItemID ,
                            x.r.value('@ItemID', 'NVARCHAR(100)') ,
                            x.r.value('@OperationSign', 'INT') ,
                            0 ,
                            0.0 ,
                            0.0
                    FROM    dbo.FRTemplate
                            CROSS APPLY Formula.nodes('/root/MasterFormula')
                            AS x ( r )
                    WHERE   AccountingSystem = @AccountingSystem
                            AND ReportID = @ReportID
                            AND FormulaType = 1
                            AND Formula IS NOT NULL 
	
	;
            WITH    V ( ItemID, DetailItemID, OpeningAmount, ClosingAmount, OperationSign )
                      AS ( SELECT   ItemID ,
                                    DetailItemID ,
                                    OpeningAmount ,
                                    ClosingAmount ,
                                    OperationSign
                           FROM     @tblMasterDetail
                           WHERE    Grade = -1
                           UNION ALL
                           SELECT   B.ItemID ,
                                    B.DetailItemID ,
                                    V.OpeningAmount ,
                                    V.ClosingAmount ,
                                    B.OperationSign * V.OperationSign AS OperationSign
                           FROM     @tblMasterDetail B ,
                                    V
                           WHERE    B.DetailItemID = V.ItemID
                         )
	INSERT  @Result
            SELECT  FR.ItemID ,
                    FR.ItemCode ,
                    FR.ItemName ,
                    FR.ItemNameEnglish ,
                    FR.ItemIndex ,
                    FR.Description ,
                    FR.FormulaType ,
                    CASE WHEN FR.FormulaType = 1 THEN FR.FormulaFrontEnd
                         ELSE ''
                    END AS FormulaFrontEnd ,
                    CASE WHEN FR.FormulaType = 1 THEN FR.Formula
                         ELSE NULL
                    END AS Formula ,
                    FR.Hidden ,
                    FR.IsBold ,
                    FR.IsItalic ,
                    FR.Category ,
                    -1 AS SortOrder ,
                    ISNULL(X.Amount, 0) AS Amount ,
                    ISNULL(X.PrevAmount, 0) AS PrevAmount
            FROM    ( SELECT    ItemID ,
                                SUM(V.OperationSign * V.OpeningAmount) AS PrevAmount ,
                                SUM(V.OperationSign * V.ClosingAmount) AS Amount
                      FROM      V
                      GROUP BY  ItemID
                    ) AS X
                    RIGHT JOIN FRTemplate FR ON FR.ItemID = X.ItemID
            WHERE   AccountingSystem = @AccountingSystem
                    AND ReportID = @ReportID
	
            RETURN 
        END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

/*
-- =============================================
-- Author:		haipl
-- Create date: <18.12.2017>
-- Report Name : F01-DNN
-- Description:	Lấy dữ liệu bảng cân đối tài khoản

-- =============================================
*/
CREATE PROCEDURE [dbo].[Proc_GetB01_DN]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT
      ,
      @IsB01bDNN BIT ,
      @isPrintByYear BIT
      ,
      @PrevFromDate DATETIME
      ,
      @PrevToDate DATETIME
    )
AS
BEGIN
  
SELECT * FROM [dbo].[Func_GetB01_DN] (
    @BranchID
  ,@IncludeDependentBranch
  ,@FromDate
  ,@ToDate
  ,@IsSimilarBranch
  ,@IsB01bDNN
  ,@isPrintByYear
  ,@PrevFromDate
  ,@PrevToDate)
order by ItemIndex
end


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
/*
-- =============================================
-- Author:		nmtruong
-- Create date: <16/01/2018>
-- Description:	Lấy dữ liệu bảng cân đối tài khoản
*/
ALTER FUNCTION [dbo].[XXXXXXXXX]
      (
        @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT ,
        @IsBalanceBothSide BIT
      )
RETURNS @Result TABLE
      (
        AccountID UNIQUEIDENTIFIER ,
        DetailByAccountObject BIT ,
        AccountObjectType INT ,
        AccountNumber NVARCHAR(50) ,
        AccountName NVARCHAR(255) ,
        AccountNameEnglish NVARCHAR(255) ,
        AccountCategoryKind INT ,
        IsParent BIT ,
        ParentID UNIQUEIDENTIFIER ,
        Grade INT ,
        AccountKind INT ,
        SortOrder INT ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
      )
AS
    BEGIN
	   DECLARE @MainCurrency NVARCHAR(3)
        SET @MainCurrency = 'VND'	
        /*ngày bắt đầu chương trình*/
        DECLARE @StartDate DATETIME
        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'      
        IF @IsBalanceBothSide = 1
           BEGIN
					
                 DECLARE @GeneralLedger TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           BranchID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(3) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4) ,
                           OpenningDebitAmountOC DECIMAL(25, 4) ,
                           DebitAmountOC DECIMAL(25, 4) ,
                           CreditAmountOC DECIMAL(25, 4) ,
                           DebitAmountOCAccum DECIMAL(25, 4) ,
                           CreditAmountOCAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedger
                        SELECT  G.*
                        FROM    (
                                  SELECT    Account ,
                                            GL.BranchID ,
                                            GL.CurrencyID ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                                     ELSE 0
                                                END) AS OpenningDebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal - GL.CreditAmountOriginal
                                                     ELSE 0
                                                END) AS OpenningDebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOCAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOCAccum
                                  FROM      dbo.GeneralLedger AS GL
                                  WHERE    GL.PostedDate <= @ToDate
                                  GROUP BY  Account ,
                                            GL.BranchID ,
                                            GL.CurrencyID
                                ) G
			
                 DECLARE @DetailData TABLE
                         (
                           AccountNumber NVARCHAR(50) ,
                           CurrencyID NVARCHAR(20) ,
                           SortOrder INT ,
                           OpeningDebitAmount DECIMAL(25, 4) ,
                           OpeningCreditAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,/*Nợ lũy kế*/
                           CreditAmountAccum DECIMAL(25, 4) ,/*có lũy kế*/
                           ClosingDebitAmount DECIMAL(25, 4) ,
                           ClosingCreditAmount DECIMAL(25, 4)
                         )
	
                  INSERT @DetailData
                        SELECT  F.AccountNumber ,
                                '' ,
                                0 ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount > 0
                                                 ) THEN F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount < 0
                                                 ) THEN -1 * F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount ,
                                SUM(F.DebitAmount) AS DebitAmount ,
                                SUM(F.CreditAmount) AS CreditAmount ,
                                SUM(F.DebitAmountAccum) AS DebitAmountAccum ,
                                SUM(F.CreditAmountAccum) AS CreditAmountAccum ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                                 ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                                 ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.AccountNumber ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            A.AccountGroupKind AccountCategoryKind ,
                                            GL.BranchID BranchID
                                  FROM      @GeneralLedger AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber = A.AccountNumber
                                  WHERE     GL.AccountNumber NOT LIKE '0%'
                                            AND A.IsParentNode = 0
                                  GROUP BY  A.AccountNumber ,
                                            A.AccountGroupKind ,
                                            GL.BranchID
                                  HAVING    SUM(OpenningDebitAmount) <> 0
                                            OR SUM(DebitAmount) <> 0
                                            OR SUM(CreditAmount) <> 0
                                ) AS F
                        GROUP BY F.AccountNumber ,
                                F.AccountCategoryKind
                 OPTION ( RECOMPILE )	         					
				
                 INSERT @Result
                        SELECT  A.ID ,
						        null,
								null,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                0 AccountKind,
                                D.SortOrder ,
                                SUM(D.OpeningDebitAmount) ,
                                SUM(D.OpeningCreditAmount) ,
                                SUM(D.DebitAmount) ,
                                SUM(D.CreditAmount) ,
                                SUM(D.DebitAmountAccum) ,
                                SUM(D.CreditAmountAccum) ,
                                SUM(D.ClosingDebitAmount) ,
                                SUM(D.ClosingCreditAmount)
                        FROM    @DetailData D
                        INNER JOIN dbo.Account A ON D.AccountNumber LIKE A.AccountNumber + '%'
                        WHERE   A.Grade <= @MaxAccountGrade
                        GROUP BY A.ID ,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                D.SortOrder
                        HAVING  SUM(D.OpeningDebitAmount) <> 0
                                OR SUM(D.OpeningCreditAmount) <> 0
                                OR SUM(D.DebitAmount) <> 0
                                OR SUM(D.CreditAmount) <> 0
                                OR SUM(D.ClosingDebitAmount) <> 0
                                OR SUM(D.ClosingCreditAmount) <> 0
                 OPTION ( RECOMPILE )
           END
        ELSE
           BEGIN
                 DECLARE @GeneralLedgerP1 TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedgerP1
                        SELECT  Account ,
                                SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmountAccum ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmountAccum
                        FROM    dbo.GeneralLedger AS GL
                        WHERE   GL.PostedDate <= @ToDate
                        GROUP BY Account

                 INSERT @Result
                        SELECT  A.ID ,
                                null DetailByAccountObject ,
                                null AccountObjectType,
                                A.AccountNumber ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade AS Grade ,
                                F.AccountKind, 
                                F.SortOrder ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount > 0
                                               ) THEN F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount < 0
                                               ) THEN -1 * F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount ,
                                ( F.DebitAmount ) AS DebitAmount ,
                                ( F.CreditAmount ) AS CreditAmount ,
                                ( F.DebitAmountAccum ) AS DebitAmountAccum ,
                                ( F.CreditAmountAccum ) AS CreditAmountAccum ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                               ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                               ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.ID ,
                                            null DetailByAccountObject,
                                            null AccountObjectType, 
                                            A.AccountNumber ,
                                            '' AS CurrencyID ,
                                            0 AS SortOrder ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            0 AS AccountKind
                                  FROM      @GeneralLedgerP1 AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber LIKE A.AccountNumber + '%' 
                                  WHERE     A.Grade <= @MaxAccountGrade
                                            AND (
                                                  OpenningDebitAmount <> 0
                                                  OR DebitAmount <> 0
                                                  OR CreditAmount <> 0
                                                )
                                  GROUP BY  A.ID ,
                                            A.AccountNumber      
                                ) AS F
                        INNER JOIN Account A ON F.AccountNumber = A.AccountNumber
                 OPTION ( RECOMPILE )      
           END
        RETURN
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

/*
-- =============================================
-- Author:		HaiPL
-- Create date: <14.12.2017>
-- Description:	Tinh toan so du

-- =============================================
*/
ALTER FUNCTION [dbo].[Func_SoDu]
      ( @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT 
      )
RETURNS @Result TABLE
      (
        AccountID UNIQUEIDENTIFIER ,
        DetailByAccountObject BIT ,
        AccountObjectType INT ,
		AccountObjectID NVARCHAR(50) ,
        AccountNumber NVARCHAR(10) ,
        AccountName NVARCHAR(255) ,
        AccountNameEnglish NVARCHAR(255) ,
        AccountCategoryKind INT ,
        IsParent BIT ,
        ParentID UNIQUEIDENTIFIER ,
        Grade INT ,
        AccountKind INT ,
        SortOrder INT ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
      )
AS
    BEGIN
        DECLARE @MainCurrency NVARCHAR(3)
        SET @MainCurrency = 'VND'	
        /*ngày bắt đầu chương trình*/
        DECLARE @StartDate DATETIME

        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'                
           BEGIN
					
                 DECLARE @GeneralLedger TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           AccountObjectID UNIQUEIDENTIFIER ,
                           BranchID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(3) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4) ,
                           OpenningDebitAmountOC DECIMAL(25, 4) ,
                           DebitAmountOC DECIMAL(25, 4) ,
                           CreditAmountOC DECIMAL(25, 4) ,
                           DebitAmountOCAccum DECIMAL(25, 4) ,
                           CreditAmountOCAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedger
                        SELECT  G.*
                        FROM    (
                                  SELECT    Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                                     ELSE 0
                                                END) AS OpenningDebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal - GL.CreditAmountOriginal
                                                     ELSE 0
                                                END) AS OpenningDebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOCAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOCAccum
                                  FROM      dbo.GeneralLedger AS GL
                                  WHERE    GL.PostedDate <= @ToDate
                                  GROUP BY  Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID
                                ) G
			
                 DECLARE @Data TABLE
                         (
                           AccountNumber NVARCHAR(50) ,
						   AccountObjectID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(20) ,
                           SortOrder INT ,
                           OpeningDebitAmount DECIMAL(25, 4) ,
                           OpeningCreditAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,/*Nợ lũy kế*/
                           CreditAmountAccum DECIMAL(25, 4) ,/*có lũy kế*/
                           ClosingDebitAmount DECIMAL(25, 4) ,
                           ClosingCreditAmount DECIMAL(25, 4)
                         )
	
                 INSERT @Data
                        SELECT  F.AccountNumber ,
						        F.AccountObjectID,
                                '' ,
                                0 ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount > 0
                                                 ) THEN F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount < 0
                                                 ) THEN -1 * F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount ,
                                SUM(F.DebitAmount) AS DebitAmount ,
                                SUM(F.CreditAmount) AS CreditAmount ,
                                SUM(F.DebitAmountAccum) AS DebitAmountAccum ,
                                SUM(F.CreditAmountAccum) AS CreditAmountAccum ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                                 ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                                 ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.AccountNumber ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            GL.AccountObjectID AccountObjectID ,
                                            A.AccountGroupKind AccountCategoryKind ,
                                            GL.BranchID BranchID
                                  FROM      @GeneralLedger AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber = A.AccountNumber
                                  WHERE     GL.AccountNumber NOT LIKE '0%'
                                            AND A.IsParentNode = 0
                                  GROUP BY  A.AccountNumber ,
                                            GL.AccountObjectID,
                                            A.AccountGroupKind ,
                                            GL.BranchID
                                  HAVING    SUM(OpenningDebitAmount) <> 0
                                            OR SUM(DebitAmount) <> 0
                                            OR SUM(CreditAmount) <> 0
                                ) AS F
                        GROUP BY F.AccountNumber ,
                                F.AccountCategoryKind,
								F.AccountObjectID
                 OPTION ( RECOMPILE )		         					    
                 INSERT @Result
                        SELECT  A.ID ,
						        null,
								null,
								D.AccountObjectID,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                0 AccountKind,
                                D.SortOrder ,
                                SUM(D.OpeningDebitAmount) ,
                                SUM(D.OpeningCreditAmount) ,
                                SUM(D.DebitAmount) ,
                                SUM(D.CreditAmount) ,
                                SUM(D.DebitAmountAccum) ,
                                SUM(D.CreditAmountAccum) ,
                                SUM(D.ClosingDebitAmount) ,
                                SUM(D.ClosingCreditAmount)
                        FROM    @Data D
                        INNER JOIN dbo.Account A ON D.AccountNumber LIKE A.AccountNumber + '%'
                        WHERE   A.Grade <= @MaxAccountGrade
                        GROUP BY A.ID ,
                                A.AccountNumber ,
								D.AccountObjectID,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                D.SortOrder
                        HAVING  SUM(D.OpeningDebitAmount) <> 0
                                OR SUM(D.OpeningCreditAmount) <> 0
                                OR SUM(D.DebitAmount) <> 0
                                OR SUM(D.CreditAmount) <> 0
                                OR SUM(D.ClosingDebitAmount) <> 0
                                OR SUM(D.ClosingCreditAmount) <> 0
                 OPTION ( RECOMPILE )
           END
        RETURN
    END



GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
/*
-- =============================================
-- Author:		nmtruong
-- Create date: <16/01/2018>
-- Description:	Lấy dữ liệu bảng cân đối tài khoản
*/
ALTER FUNCTION [dbo].[Func_GetBalanceAccountF01]
      (
        @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT ,
        @IsBalanceBothSide BIT
      )
RETURNS @Result TABLE
      (
        AccountID UNIQUEIDENTIFIER ,
        DetailByAccountObject BIT ,
        AccountObjectType INT ,
        AccountNumber NVARCHAR(50) ,
        AccountName NVARCHAR(255) ,
        AccountNameEnglish NVARCHAR(255) ,
        AccountCategoryKind INT ,
        IsParent BIT ,
        ParentID UNIQUEIDENTIFIER ,
        Grade INT ,
        AccountKind INT ,
        SortOrder INT ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
      )
AS
    BEGIN
	   DECLARE @MainCurrency NVARCHAR(3)
        SET @MainCurrency = 'VND'	
        /*ngày bắt đầu chương trình*/
        DECLARE @StartDate DATETIME
        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'
        IF @IsBalanceBothSide = 1
           BEGIN
                 DECLARE @GeneralLedger TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           AccountObjectID UNIQUEIDENTIFIER ,
                           BranchID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(3) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4) ,
                           OpenningDebitAmountOC DECIMAL(25, 4) ,
                           DebitAmountOC DECIMAL(25, 4) ,
                           CreditAmountOC DECIMAL(25, 4) ,
                           DebitAmountOCAccum DECIMAL(25, 4) ,
                           CreditAmountOCAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedger
                        SELECT  G.*
                        FROM    (
                                  SELECT    Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                                     ELSE 0
                                                END) AS OpenningDebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal - GL.CreditAmountOriginal
                                                     ELSE 0
                                                END) AS OpenningDebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOCAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOCAccum
                                  FROM      dbo.GeneralLedger AS GL
                                  WHERE    GL.PostedDate <= @ToDate
                                  GROUP BY  Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID
                                ) G
			
                 DECLARE @Data TABLE
                         (
                           AccountNumber NVARCHAR(50) ,
						   AccountObjectID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(20) ,
                           SortOrder INT ,
                           OpeningDebitAmount DECIMAL(25, 4) ,
                           OpeningCreditAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,/*Nợ lũy kế*/
                           CreditAmountAccum DECIMAL(25, 4) ,/*có lũy kế*/
                           ClosingDebitAmount DECIMAL(25, 4) ,
                           ClosingCreditAmount DECIMAL(25, 4)
                         )
	
                 INSERT @Data
                        SELECT  F.AccountNumber ,
						        F.AccountObjectID,
                                '' ,
                                0 ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount > 0
                                                 ) THEN F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount < 0
                                                 ) THEN -1 * F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount ,
                                SUM(F.DebitAmount) AS DebitAmount ,
                                SUM(F.CreditAmount) AS CreditAmount ,
                                SUM(F.DebitAmountAccum) AS DebitAmountAccum ,
                                SUM(F.CreditAmountAccum) AS CreditAmountAccum ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                                 ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                                 ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.AccountNumber ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            GL.AccountObjectID AccountObjectID ,
                                            A.AccountGroupKind AccountCategoryKind ,
                                            GL.BranchID BranchID
                                  FROM      @GeneralLedger AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber = A.AccountNumber
                                  WHERE     GL.AccountNumber NOT LIKE '0%'
                                            AND A.IsParentNode = 0
                                  GROUP BY  A.AccountNumber ,
                                            GL.AccountObjectID,
                                            A.AccountGroupKind ,
                                            GL.BranchID
                                  HAVING    SUM(OpenningDebitAmount) <> 0
                                            OR SUM(DebitAmount) <> 0
                                            OR SUM(CreditAmount) <> 0
                                ) AS F
                        GROUP BY F.AccountNumber ,
                                F.AccountCategoryKind,
								F.AccountObjectID
                 OPTION ( RECOMPILE )		         					    
                 INSERT @Result
                        SELECT  A.ID ,
						        null,
								null,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                0 AccountKind,
                                D.SortOrder ,
                                SUM(D.OpeningDebitAmount) ,
                                SUM(D.OpeningCreditAmount) ,
                                SUM(D.DebitAmount) ,
                                SUM(D.CreditAmount) ,
                                SUM(D.DebitAmountAccum) ,
                                SUM(D.CreditAmountAccum) ,
                                SUM(D.ClosingDebitAmount) ,
                                SUM(D.ClosingCreditAmount)
                        FROM    @Data D
                        INNER JOIN dbo.Account A ON D.AccountNumber LIKE A.AccountNumber + '%'
                        WHERE   A.Grade <= @MaxAccountGrade
                        GROUP BY A.ID ,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                D.SortOrder
                        HAVING  SUM(D.OpeningDebitAmount) <> 0
                                OR SUM(D.OpeningCreditAmount) <> 0
                                OR SUM(D.DebitAmount) <> 0
                                OR SUM(D.CreditAmount) <> 0
                                OR SUM(D.ClosingDebitAmount) <> 0
                                OR SUM(D.ClosingCreditAmount) <> 0
                 OPTION ( RECOMPILE )
           END
        ELSE
           BEGIN
                 DECLARE @GeneralLedgerP1 TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedgerP1
                        SELECT  Account ,
                                SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmountAccum ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmountAccum
                        FROM    dbo.GeneralLedger AS GL
                        WHERE   GL.PostedDate <= @ToDate
                        GROUP BY Account
                 INSERT @Result
                        SELECT  A.ID ,
                                null DetailByAccountObject ,
                                null AccountObjectType,
                                A.AccountNumber ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade AS Grade ,
                                F.AccountKind,
                                F.SortOrder ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount > 0
                                               ) THEN F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount < 0
                                               ) THEN -1 * F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount ,
                                ( F.DebitAmount ) AS DebitAmount ,
                                ( F.CreditAmount ) AS CreditAmount ,
                                ( F.DebitAmountAccum ) AS DebitAmountAccum ,
                                ( F.CreditAmountAccum ) AS CreditAmountAccum ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                               ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                               ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.ID ,
                                            null DetailByAccountObject,
                                            null AccountObjectType,
                                            A.AccountNumber ,
                                            '' AS CurrencyID ,
                                            0 AS SortOrder ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            0 AS AccountKind
                                  FROM      @GeneralLedgerP1 AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber LIKE A.AccountNumber + '%'
                                  WHERE     A.Grade <= @MaxAccountGrade
                                            AND (
                                                  OpenningDebitAmount <> 0
                                                  OR DebitAmount <> 0
                                                  OR CreditAmount <> 0
                                                )
                                  GROUP BY  A.ID ,
                                            A.AccountNumber      
                                ) AS F
                        INNER JOIN Account A ON F.AccountNumber = A.AccountNumber
                 OPTION ( RECOMPILE )      
           END
        RETURN
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
/*
-- =============================================
-- Author:		haipl
-- Create date: <18.12.2017>
-- Report Name : F01-DNN
-- Description:	Lấy dữ liệu bảng cân đối tài khoản

-- =============================================
*/
ALTER PROCEDURE [dbo].[Proc_GetBalanceAccountF01]
      ( @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT,
		@IsBalanceBothSide BIT
		)
AS
BEGIN
 
SELECT t.AccountID ,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName,
			   round(sum(t.OpeningDebitAmount),0) OpeningDebitAmount,
			   round(sum(t.OpeningCreditAmount),0) OpeningCreditAmount,
			   round(sum(t.DebitAmount),0) DebitAmount,
			   round(sum(t.CreditAmount),0) CreditAmount,
			   round(sum(t.DebitAmountAccum),0) DebitAmountAccum,
			   round(sum(t.CreditAmountAccum),0) CreditAmountAccum,
			   round(sum(t.ClosingDebitAmount),0) ClosingDebitAmount,
			   round(sum(t.ClosingCreditAmount),0) ClosingCreditAmount FROM [dbo].[Func_GetBalanceAccountF01] (
   @FromDate
  ,@ToDate
  ,@MaxAccountGrade
  ,@IsBalanceBothSide) t
  group by t.AccountID,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName
  order by t.AccountNumber
end

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
/*
-- =============================================
-- Author:		haipl
-- Create date: 01.01.2018
-- Description:	<Lấy số liệu báo cáo sổ nhật ký bán hàng>

-- =============================================
*/
ALTER PROCEDURE [dbo].[Proc_SA_GetSalesDiaryBook]
    @FromDate DATETIME ,
    @ToDate DATETIME,
    @IsDisplayNotReceiptOnly BIT
AS 
    BEGIN 
		
        DECLARE @listRefType NVARCHAR(MAX)
        IF @IsDisplayNotReceiptOnly = 1 
            SET @listRefType = '3530,3532,3534,3536,'  
        ELSE 
            SET @listRefType = '3530,3531,3532,3534,3535,3536,3537,3538,3540,3541,3542,3543,3544,3545,3550,3551,3552,3553,3554,3555,'

              select ROW_NUMBER() OVER ( ORDER BY a.PostedDate, a.Date, a.No ) AS RowNum ,
			         a.Date as ngay_CTU, 
			         a.PostedDate as ngay_HT, 
					 a.No as So_CTU,
					 a.InvoiceDate as Ngay_HD, 
					 a.InvoiceNo as SO_HD, 
					 Sreason as Dien_giai,
					 SUM(CASE WHEN a.Account IN ( '5111' ) THEN a.CreditAmount
                         ELSE $0
                    END) AS TurnOverAmountInv , 
                    SUM(CASE WHEN a.Account IN ( '5112' ) THEN a.CreditAmount
                         ELSE $0
                    END) AS TurnOverAmountFinishedInv ,		
                SUM(CASE WHEN a.Account IN ( '5113' ) THEN a.CreditAmount
                         ELSE $0
                    END) AS TurnOverAmountServiceInv ,		
                SUM(CASE WHEN LEFT(a.Account, 3) IN ( '511' )
                              AND ( NOT LEFT(a.Account, 4) IN ( '5111',
                                                              '5112', '5113' )
                                  ) THEN a.CreditAmount
                         ELSE $0
                    END) AS TurnOverAmountOther, 	
                SUM(b.DiscountAmount) AS DiscountAmount , 
				SUM(CASE WHEN a.typeID = '330' THEN a.DebitAmount
                         ELSE $0
                    END) AS ReturnAmount, 	
                SUM(CASE WHEN a.typeID = '340' THEN a.DebitAmount
                         ELSE $0
                    END) AS ReduceAmount, 	
			    SUM(b.VATAmount) AS VATAmount,
				ao.AccountingObjectCode as CustomerCode, 
				ao.AccountingObjectName as CustomerName 
				INTO    #Result
				from GeneralLedger a 
				join SAInvoiceDetail b on a.DetailID = b.ID 
				join MaterialGoods c on b.MaterialGoodsID = c.ID
				join SAInvoice d on a.ReferenceID = d.ID 
				join Type e on a.TypeID = e.ID
				join AccountingObject ao on a.AccountingObjectID = ao.ID 
				where a.PostedDate between  @FromDate and @ToDate and Account = '5111'
				group by a.Date,  a.PostedDate, a.No ,a.InvoiceDate , a.InvoiceNo , Sreason,
				ao.AccountingObjectCode,ao.AccountingObjectName;

       select * from #Result ORDER BY RowNum    
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
/*
-- =============================================
-- Author:		HaiPL
-- Create date: 29.01.2018
-- Description:	<Mua hàng: Lấy số liệu cho báo cáo Sổ nhật ký mua hàng S03a3-DNN>
-- =============================================
*/
ALTER PROCEDURE [dbo].[Proc_PO_DiaryBook]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER = NULL ,
    @IncludeDependentBranch BIT = 0 ,
    @IsNotPaid BIT = 0
AS 
    BEGIN
        
        DECLARE @listRefType NVARCHAR(MAX)
        IF @IsNotPaid = 1
            SET @listRefType = '302,312,318,324,330,352,362,368,374' 
        ELSE 
            SET @listRefType = '302,307,308,309,310,312,313,314,315,316,318,319,320,321,322,324,325,326,327,328,330,331,332,333,334,352,357,358,359,360,362,363,364,365,366,368,369,370,371,372,374,375,376,377,378' 
           
        SELECT  ROW_NUMBER() OVER ( ORDER BY PL.PostedDate , PL.RefDate , PL.RefNo ) AS RowNum ,
                PL.ReferenceID ,
                TypeID ,
                PL.PostedDate ,
                PL.RefDate ,
                PL.RefNo ,
                PL.InvoiceDate ,
                PL.InvoiceNo ,
                PL.Description ,
                SUM(CASE WHEN LEFT(Account, 3) = N'156'
                              
                         THEN DebitAmount
                         ELSE $0
                    END) AS GoodsAmount ,
                SUM(CASE WHEN LEFT(Account, 3) = N'152'
                         THEN ( DebitAmount )
                         ELSE $0
                    END) AS EquipmentAmount ,
                ( CASE WHEN LEFT(Account, 3) <> N'152'
                            AND LEFT(Account, 3) <> N'156'
                            AND LEFT(Account, 3) <> N'1331'
                       THEN Account
                       ELSE NULL
                  END ) AS AnotherAccount , 
                SUM(CASE WHEN LEFT(Account, 3) <> N'152'
                              AND LEFT(Account, 3) <> N'156'
                              AND LEFT(Account, 3) <> N'1331'
                         THEN (DebitAmount)
                         ELSE $0
                    END) AS AnotherAmount , 
                SUM(CASE WHEN LEFT(Account, 3) = N'156'
                         THEN DebitAmount
                         ELSE $0
                    END
                    + CASE WHEN LEFT(Account, 3) = N'152'
                         THEN ( DebitAmount )
                         ELSE $0
                    END
                    + CASE WHEN LEFT(Account, 3) <> N'152'
                              AND LEFT(Account, 3) <> N'156'
                              AND LEFT(Account, 3) <> N'1331'
                         THEN (DebitAmount)
                         ELSE $0
                    END) AS PaymentableAmount ,
                AO.AccountingObjectCode , 
                CASE WHEN PL.TypeID IN ( 330, 331, 332, 333, 334, 352, 357,
                                          358, 359, 360, 362, 363, 364, 365,
                                          366, 368, 369, 370, 371, 372, 374,
                                          375, 376, 377, 378 )
                     THEN null 
                     ELSE AO.AccountingObjectName
                END AS AccountObjectNameDI 
        FROM    dbo.GeneralLedger AS PL,
		        dbo.AccountingObject as AO
        WHERE   PL.PostedDate BETWEEN @FromDate AND @ToDate
		and PL.AccountingObjectID = Ao.ID
                AND @listRefType LIKE N'%' + CAST(PL.TypeID AS NVARCHAR(4))
                + N'%'

        GROUP BY PL.ReferenceID ,
                PL.TypeID ,
                PL.PostedDate ,
                PL.RefDate ,
                PL.RefNo ,
                PL.InvoiceDate ,
                PL.InvoiceNo ,
                PL.Description ,
                AO.AccountingObjectCode ,
				 ( CASE WHEN LEFT(Account, 3) <> N'152'
                            AND LEFT(Account, 3) <> N'156'
                            AND LEFT(Account, 3) <> N'1331'
                       THEN Account
                       ELSE NULL
                  END ),
				CASE WHEN PL.TypeID IN ( 330, 331, 332, 333, 334, 352, 357,
                                          358, 359, 360, 362, 363, 364, 365,
                                          366, 368, 369, 370, 371, 372, 374,
                                          375, 376, 377, 378 )
                     THEN null
                     ELSE AO.AccountingObjectName
                END 
        HAVING  sum(CASE WHEN LEFT(Account, 3) = N'156'
                              
                         THEN DebitAmount
                         ELSE $0
                    END) <> 0
                OR SUM(CASE WHEN LEFT(Account, 3) = N'152'
                              
                         THEN ( DebitAmount )
                         ELSE $0
                    END) <> 0
                OR SUM(CASE WHEN LEFT(Account, 3) <> N'152'
                              AND LEFT(Account, 3) <> N'156'
                              AND LEFT(Account, 3) <> N'1331'
                         THEN (DebitAmount)
                         ELSE $0
                    END) <> 0
        ORDER BY PL.PostedDate ,
                PL.RefDate ,
                PL.RefNo           
    END
    


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO


/*
	 select * from [dbo].[Func_ConvertStringIntoTable_Nvarchar] ('USD;', ';')
	 Hàm convert một chuỗi thành một bảng 
*/
ALTER FUNCTION [dbo].[Func_ConvertStringIntoTable_Nvarchar]
    (
      @ValueList NVARCHAR(MAX) ,
      @SeparateCharacter NCHAR(1) /*Ký tự phân tách*/
    )
RETURNS @ValueTable TABLE ( Value NVARCHAR(MAX))
AS
    BEGIN
		
 
        DECLARE @ValueListLength INT ,
            @StartingPosition INT ,
            @Value NVARCHAR(MAX) ,
            @SecondPosition INT 
        SET @StartingPosition = 1
        SET @Value = SPACE(0)
        
        IF SUBSTRING(@ValueList, 1, 1) <> @SeparateCharacter
            SET @ValueList = @SeparateCharacter + @ValueList
        SET @ValueListLength = LEN(@ValueList)
			
        IF SUBSTRING(@ValueList, @ValueListLength - 1, @ValueListLength) <> @SeparateCharacter
            SET @ValueList = @ValueList + @SeparateCharacter
			
        WHILE @StartingPosition < @ValueListLength
            BEGIN
                SET @SecondPosition = CHARINDEX(@SeparateCharacter, @ValueList,
                                                @StartingPosition + 1)

                SET @Value = SUBSTRING(@ValueList, @StartingPosition + 1,
                                       @SecondPosition - @StartingPosition - 1)	

                SET @StartingPosition = @SecondPosition
                IF @Value <> SPACE(0)
                    INSERT  INTO @ValueTable
                            ( Value )
                    VALUES  ( LTRIM(RTRIM(@Value)) ) 
            END
        RETURN 
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
/*
-- =============================================
-- Author:		<haipl>
-- Create date: <05-02-2017>
-- Description:	<Sổ cái (hình thức nhật ký chung)>
-- S03a-DNN sổ nhật ký chung
-- =============================================
*/
ALTER PROCEDURE [dbo].[Proc_GL_GetGLAccountLedgerDiaryBook]
    @BranchID UNIQUEIDENTIFIER,
    @IncludeDependentBranch BIT,
    @StartDate DATETIME,
    @FromDate DATETIME,
    @ToDate DATETIME,    
    @AccountNumber NVARCHAR(MAX),
    @IsSimilarSum BIT,
	@IsSummaryChildAccount BIT
AS
    BEGIN    
        SET NOCOUNT ON       
        CREATE TABLE #Result
            (
             KeyID NVARCHAR(100)COLLATE SQL_Latin1_General_CP1_CI_AS,
             RefID UNIQUEIDENTIFIER,
             RefType INT,
             PostedDate DATETIME,
             RefNo NVARCHAR(25)COLLATE SQL_Latin1_General_CP1_CI_AS,
             RefDate DATETIME,
             InvDate DATETIME,
             InvNo NVARCHAR(MAX)COLLATE SQL_Latin1_General_CP1_CI_AS, 
             JournalMemo NVARCHAR(255)COLLATE SQL_Latin1_General_CP1_CI_AS,
             AccountNumber NVARCHAR(25)COLLATE SQL_Latin1_General_CP1_CI_AS,
			 DetailAccountNumber NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS,
             AccountCategoryKind INT,
             AccountName NVARCHAR(255)COLLATE SQL_Latin1_General_CP1_CI_AS,
             CorrespondingAccountNumber NVARCHAR(25)COLLATE SQL_Latin1_General_CP1_CI_AS,
             DebitAmount MONEY,
             CreditAmount MONEY,
             OrderType INT,
             IsBold BIT,
             OrderNumber int
            )
        CREATE TABLE #tblAccountNumber
            (
             AccountNumber NVARCHAR(25)  COLLATE SQL_Latin1_General_CP1_CI_AS  PRIMARY KEY,
             AccountName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
             AccountNumberPercent NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,
             AccountCategoryKind INT,
			 IsParent BIT
            )
			
       
	
        INSERT  #tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountName,
                        A1.AccountNumber + '%',
                        A1.AccountGroupKind,
						A1.IsParentNode
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
	            					
               
        IF @IsSimilarSum = 0
            INSERT  #Result
                    (KeyID,
                     RefID,
                     RefType,
                     PostedDate,
                     RefNo,
                     RefDate,
                     InvDate,
                     InvNo,
                     JournalMemo,
                     AccountNumber,
					 DetailAccountNumber,
                     AccountName,
                     CorrespondingAccountNumber,
                     AccountCategoryKind,
                     DebitAmount,
                     CreditAmount,
                     ORDERType,
                     IsBold,
                     OrderNumber
					
                    )
                    SELECT 
							convert(nvarchar(100),GL.ID)  + '-' + AC.AccountNumber,
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Description AS JournalMemo,
                            AC.AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind,
                            DebitAmount,
                            CreditAmount,
                            1 AS ORDERType,
                            0,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%') 
											AND GL.DebitAmount <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    dbo.GeneralLedger AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND (GL.DebitAmount <> 0
                                 OR GL.CreditAmount <> 0
                                 or GL.DebitAmountOriginal <> 0
                                 OR GL.CreditAmountOriginal <> 0
                                )                           
                     
        ELSE
			/*Nếu tick Hiển thị phát sinh theo tiết khoản của TK tổng hợp thì lấy lên chi tiết tài khoản và group by (CR 13084)*/
			INSERT  INTO #Result
						(KeyID,
						 RefID,
						 RefType,
						 PostedDate,
						 RefNo,
						 RefDate,
						 InvDate,
						 InvNo,
						 JournalMemo,
						 AccountNumber,
						 DetailAccountNumber,
						 AccountName,
						 CorrespondingAccountNumber,
						 AccountCategoryKind,
						 DebitAmount,
						 CreditAmount,
						 ORDERType,
						 IsBold,
						 OrderNumber
					
						)
						SELECT  CAST(MAX(GL.ID) AS NVARCHAR(20))
								+ '-' + AC.AccountNumber,
								GL.ReferenceID,
								GL.TypeID,
								GL.PostedDate,
								RefNo,
								GL.RefDate,
								Gl.InvoiceDate,
								GL.InvoiceNo,
								GL.Description AS JournalMemo,							
								AC.AccountNumber,
								CASE WHEN @IsSummaryChildAccount = 1 THEN GL.Account
									 ELSE NULL 
								END AS DetailAccountNumber,
								AC.AccountName,
								GL.AccountCorresponding,
								AC.AccountCategoryKind,
								SUM(GL.DebitAmount) AS DebitAmount,
								SUM(GL.CreditAmount) AS CreditAmount,
								1 AS ORDERType,
								0 AS IsBold,
								 CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%') 
										   AND SUM(GL.DebitAmount) <> 0
										   THEN 0
										   ELSE 1
								  END AS OrderNumber
						FROM    dbo.GeneralLedger AS GL
								INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
						WHERE   (GL.PostedDate BETWEEN @FromDate AND @ToDate)                        
						GROUP BY GL.ReferenceID,
								GL.TypeID,
								GL.PostedDate,
								GL.RefNo,
								GL.RefDate,
								Gl.InvoiceDate,
								GL.InvoiceNo,
								GL.Description,
								AC.AccountNumber,
								CASE WHEN @IsSummaryChildAccount = 1 THEN GL.Account 
									 ELSE NULL 
								END,
								AC.AccountName,
								GL.AccountCorresponding,
								AC.AccountCategoryKind
						HAVING  SUM(GL.DebitAmount) <> 0
								OR SUM(GL.CreditAmount) <> 0 
								OR SUM(GL.DebitAmountOriginal) <> 0
								OR SUM(GL.CreditAmountOriginal) <> 0		
       
        SELECT  AccountNumber,
                AccountName,
				IsParent,
                SUM(OpenningDebitAmount) AS OpenningDebitAmount,
                SUM(OpenningCreditAmount) AS OpenningCreditAmount,
                SUM(ClosingDebitAmount) AS ClosingDebitAmount,
                SUM(ClosingCreditAmount) AS ClosingCreditAmount,
                SUM(AccumDebitAmount) AS AccumDebitAmount,
                SUM(AccumCreditAmount) AS AccumCreditAmount
        FROM    (
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS OpenningDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent
                 FROM   dbo.GeneralLedger GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate < @FromDate                       
                 GROUP BY GL.BranchID,
                        AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS ClosingDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent
                 FROM   dbo.GeneralLedger GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate <= @ToDate                        
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        SUM(GL.DebitAmount) AS AccumDebitAmount,
                        SUM(GL.CreditAmount) AS AccumCreditAmount,
						AC.IsParent
                 FROM   dbo.GeneralLedger GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate BETWEEN @StartDate AND @ToDate                        
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                ) AS A
        GROUP BY AccountNumber,
                AccountName,
				IsParent                
        HAVING
				SUM(OpenningDebitAmount) <>0 OR
                SUM(OpenningCreditAmount)  <>0 OR
                SUM(ClosingDebitAmount)  <>0 OR
                SUM(ClosingCreditAmount) <>0 OR
                SUM(AccumDebitAmount) <>0 OR
                SUM(AccumCreditAmount) <>0 
        ORDER BY AccountNumber
        		
		SELECT  ROW_NUMBER() OVER (ORDER BY AccountNumber,DetailAccountNumber, OrderType, PostedDate, RefDate,OrderNumber, RefNo) AS RowNum,
				KeyID,
				RefID,
				RefType,
				PostedDate,
				RefDate,
				RefNo,
				InvDate,
				InvNo,
				JournalMemo,
				AccountNumber,
				DetailAccountNumber,
				CorrespondingAccountNumber,
				DebitAmount,
				CreditAmount,
				IsBold
		FROM    #Result
			
              
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
/*
-- =============================================
-- Author:		Haipl
-- Create date: 06.01.2018
-- Description:	Báo cáo Sổ chi tiết các tài khoản

--USE [ACCOUNTING]
--GO

--DECLARE	@return_value int

--EXEC	@return_value = [dbo].[Proc_GL_GetBookDetailPaymentByAccountNumber]
--		@BranchID = null,
--		@IncludeDependentBranch = null,
--		@FromDate = N'1-JAN-17',
--		@ToDate = N'31-DEC-17',
--		@AccountNumber = N'131',
--		@GroupTheSameItem = 1

--GO

-- =============================================
*/
ALTER PROCEDURE [dbo].[Proc_GL_GetBookDetailPaymentByAccountNumber]
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @GroupTheSameItem BIT
AS
    BEGIN

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1) PRIMARY KEY,
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              InvDate DATETIME ,
              InvNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255) ,
              AccountNumber NVARCHAR(25),
              CorrespondingAccountNumber NVARCHAR(25) ,
              DebitAmount DECIMAL(22,4) ,
              CreditAmount DECIMAL(22,4) ,
              ClosingDebitAmount DECIMAL(22,4) ,
              ClosingCreditAmount DECIMAL(22,4) ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              OrderType INT , 
              IsBold BIT ,
              BranchName NVARCHAR(128)
            )      
       
        
        DECLARE @tblAccountNumber TABLE
            (              
			  AccountNumber  NVARCHAR(25),  
              AccountNumberPercent NVARCHAR(25)              
            )
        INSERT  @tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountNumber + '%'
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        IF @GroupTheSameItem = 1
            BEGIN
                INSERT  #Result					
				SELECT
					RefID ,
                    PostedDate ,
                    RefDate ,
                    RefNo ,
                    InvDate ,
                    InvNo ,
                    RefType ,
                    JournalMemo ,
                    AccountNumber,
                    CorrespondingAccountNumber ,
                    ISNULL(DebitAmount,0) ,
                    ISNULL(CreditAmount,0) ,
                    ISNULL(ClosingDebitAmount,0),
                    ISNULL(ClosingCreditAmount,0),
                    AccountingObjectCode,
                    AccountObjectName,
                    OrderType ,
                    IsBold ,
                    BranchName	
                 FROM
					(SELECT  
								NULL as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư đầu kỳ' as JournalMemo ,
                                A.AccountNumber,
                                NULL as CorrespondingAccountNumber ,
								$0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
									 THEN SUM(GL.DebitAmount - GL.CreditAmount)
									 ELSE 0
								END AS ClosingDebitAmount ,
								CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
									 THEN SUM(GL.CreditAmount - GL.DebitAmount)
									 ELSE 0
								END AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                0 AS OrderType ,
                                1 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
                            FROM    dbo.GeneralLedger AS GL
							INNER JOIN @tblAccountNumber A ON GL.Account LIKE A.AccountNumberPercent
							WHERE   ( GL.PostedDate < @FromDate )                       
							GROUP BY 
								A.AccountNumber
							HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Description,
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                SUM(GL.DebitAmount) AS DebitAmount ,
                                SUM(GL.CreditAmount) AS CreditAmount ,
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 As isBold,
                                '' as BranchName
								,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
                        FROM    dbo.GeneralLedger AS GL
                                INNER JOIN @tblAccountNumber A ON GL.Account LIKE A.AccountNumberPercent
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )                              
                        GROUP BY GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Description ,
                                A.AccountNumber,
                                GL.AccountCorresponding 
                        HAVING  SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0                       
                      )T
                      ORDER BY 
						AccountNumber,
                        OrderType ,
                        postedDate ,
                        RefDate ,
                        RefNo      
                        ,SortOrder,DetailPostOrder,EntryType            
            END
            
        ELSE
            BEGIN             
                
                INSERT  #Result					
				SELECT
					RefID ,
                    PostedDate ,
                    RefDate ,
                    RefNo ,
                    InvDate ,
                    InvNo ,
                    RefType ,
                    JournalMemo ,
                    AccountNumber,
                    CorrespondingAccountNumber ,
                    DebitAmount ,
                    CreditAmount ,
                    ClosingDebitAmount ,
                    ClosingCreditAmount ,
                    AccountingObjectCode,
                    AccountObjectName,
                    OrderType ,
                    IsBold ,
                    BranchName	
                 FROM
					(SELECT  
								null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư đầu kỳ' as JournalMemo ,
                                A.AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
									 THEN SUM(GL.DebitAmount - GL.CreditAmount)
									 ELSE 0
								END AS ClosingDebitAmount ,
								CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
									 THEN SUM(GL.CreditAmount - GL.DebitAmount)
									 ELSE 0
								END AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                0 AS OrderType ,
                                1 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType
                            FROM    dbo.GeneralLedger AS GL
							INNER JOIN @tblAccountNumber A ON GL.Account LIKE A.AccountNumberPercent
							WHERE   ( GL.PostedDate < @FromDate )                       
							GROUP BY 
								A.AccountNumber
							HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                CASE WHEN GL.DESCRIPTION IS NULL
                                          OR LTRIM(GL.Description) = ''
                                     THEN GL.Reason
                                     ELSE GL.Description
                                END AS JournalMemo ,
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                GL.DebitAmount ,
                                GL.CreditAmount ,
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 ,
                                null BranchName
								,null
								,null
								,null
                        FROM    dbo.GeneralLedger AS GL
                                INNER JOIN @tblAccountNumber A ON GL.Account LIKE A.AccountNumberPercent
                        WHERE   (GL.PostedDate BETWEEN @FromDate AND @ToDate )                              
                                AND ( GL.DebitAmount <> 0
                                      OR GL.CreditAmount <> 0
                                    )   			                  
                      )T
                      ORDER BY 
						AccountNumber,
                        OrderType ,
                        postedDate ,
                        RefDate ,
                        RefNo,SortOrder,DetailPostOrder,EntryType                                                          
               
            END         
       
        DECLARE @AccountObjectCode NVARCHAR(25)
			 ,@AccountNumber1 NVARCHAR(25)
			 ,@ClosingDebitAmount DECIMAL(22,4)
			 
		SELECT @ClosingDebitAmount = 0
		
        UPDATE  #Result
        SET     @ClosingDebitAmount = @ClosingDebitAmount + ISNULL(ClosingDebitAmount,0) - 
		                              ISNULL(ClosingCreditAmount,0) + ISNULL(DebitAmount,0) - ISNULL(CreditAmount,0),
		
                ClosingDebitAmount = CASE WHEN @ClosingDebitAmount > 0
                                          THEN @ClosingDebitAmount
                                          ELSE 0
                                     END ,
                ClosingCreditAmount = CASE WHEN @ClosingDebitAmount < 0
                                           THEN - @ClosingDebitAmount
                                           ELSE 0
                                      END 
        DECLARE @DebitAmount MONEY  
		DECLARE @CreditAmount MONEY 
		DECLARE @ClosingDeditAmount_SDDK MONEY 
		DECLARE @ClosingCreditAmount_SDDK MONEY 
	    select @DebitAmount = sum(DebitAmount),
		       @CreditAmount = sum(CreditAmount)
		from #Result Res
		INNER JOIN @tblAccountNumber A ON Res.AccountNumber LIKE A.AccountNumberPercent
		where OrderType = 1
		select @ClosingDeditAmount_SDDK = sum(ClosingDebitAmount),
		       @ClosingCreditAmount_SDDK = sum(ClosingCreditAmount)
		from #Result where OrderType = 0
        SELECT  RowNum ,
                RefID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountNumber,
                PostedDate ,
                RefDate ,
                RefNo ,
                InvDate ,
                InvNo ,
                RefType ,
                JournalMemo ,
                CorrespondingAccountNumber ,
                DebitAmount ,
                CreditAmount ,
                ClosingDebitAmount ,
                ClosingCreditAmount ,
               
                OrderType AS InvoiceOrder ,
                IsBold ,
                BranchName
        FROM    #Result
        ORDER BY RowNum
		select 
		null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư cuối kỳ' as JournalMemo ,
                                AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 DebitAmount ,
								$0 CreditAmount ,
								sum(ClosingDebitAmount) + sum(sumDebitAmount) - sum(sumCreditAmount) as ClosingDebitAmount,
								$0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                2 AS OrderType ,
                                2 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType
        from (SELECT  
								null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư cuối kỳ' as JournalMemo ,
                                A.AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN GL.OrderType = 0 and gl.IsBold = 1 THEN sum(ClosingDebitAmount)
									 ELSE 0 
								END
								AS ClosingDebitAmount,
								sum(DebitAmount) sumDebitAmount,
								sum(CreditAmount) sumCreditAmount,
								0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                2 AS OrderType ,
                                2 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType
                            FROM    #Result AS GL
							INNER JOIN @tblAccountNumber A ON GL.AccountNumber LIKE A.AccountNumberPercent                 
							GROUP BY OrderType,IsBold,
								A.AccountNumber) as A1
        group by AccountNumber
        DROP TABLE #Result 
            
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
ALTER PROCEDURE [dbo].[Proc_FU_GetCashDetailBook]
    (
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @AccountNumber NVARCHAR(20) ,
      @BranchID UNIQUEIDENTIFIER ,
      @CurrencyID NVARCHAR(3)
    )
AS 
    BEGIN

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
			  AccountNumber NVARCHAR(15),
              PostedDate DATETIME ,
              RefDate DATETIME ,
              ReceiptRefNo NVARCHAR(25) ,
              PaymentRefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255) ,
              CorrespondingAccountNumber NVARCHAR(20) ,
              ExchangeRate Numeric ,
              DebitAmount Numeric ,
              DebitAmountOC Numeric ,
              CreditAmount Numeric ,
              CreditAmountOC Numeric ,
              ClosingAmount Numeric ,
              ClosingAmountOC Numeric ,
              ContactName NVARCHAR(255) ,
			  AccountObjectId NVARCHAR(255),
              AccountObjectCode NVARCHAR(255) ,
              AccountObjectName NVARCHAR(255) , 
              EmployeeCode NVARCHAR(255) ,
              EmployeeName NVARCHAR(255) , 
              InvoiceOrder INT , 
              BranchID UNIQUEIDENTIFIER ,
              BranchName NVARCHAR(255) ,
              IsBold BIT
            )
         DECLARE @tblAccountNumber TABLE
            (              
			  AccountNumber  NVARCHAR(25),  
              AccountNumberPercent NVARCHAR(25)              
            )
        INSERT  @tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountNumber + '%'
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        DECLARE @AccountNumberPercent NVARCHAR(25)
        SET @AccountNumberPercent = @AccountNumber + '%'
        
        DECLARE @DiscountNumber NVARCHAR(25)       
        SET @DiscountNumber = '5211%'
   
        DECLARE @ClosingAmountOC MONEY
        DECLARE @ClosingAmount MONEY
   
 INSERT  #Result
	          (RefID ,
			  AccountNumber,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  CorrespondingAccountNumber)
			  select ReferenceID ,
			  Account,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  AccountCorresponding from 
			            (SELECT null as ReferenceID,
						    Acc.AccountNumber as Account,
							null as PostedDate,
							null as RefDate,
							null as ReceiptRefNo,
							null as PaymentRefNo,
							null as RefType,
					        N'Số tồn đầu kỳ' AS JournalMemo ,
                            $0 AS ExchangeRate ,
                            $0 AS DebitAmount ,
                            $0 AS DebitAmountOC ,
                            $0 AS CreditAmount ,
                            $0 AS CreditAmountOC ,
							SUM(GL.DebitAmount - GL.CreditAmount) as ClosingAmount,
							SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) as ClosingAmountOC,
							null as ContactName,
							null as AccountObjectId,
                            0 AS InvoiceOrder ,
                            null BranchID ,
                            0 as BranchName,
                            null AccountCorresponding
							 from GeneralLedger gl
							 inner join @tblAccountNumber Acc on GL.Account = Acc.AccountNumber
							 where ( GL.PostedDate < @FromDate )
								AND ( @CurrencyID IS NULL
								OR GL.CurrencyID = @CurrencyID)
						    group by Acc.AccountNumber
				        union all
                        SELECT  GL.ReferenceID,
						        GL.Account,
                                GL.PostedDate ,
                                GL.RefDate ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS ReceiptRefNo ,
                                CASE WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS PaymentRefNo ,
                                GL.TypeID ,
                                GL.Description ,
                                GL.ExchangeRate ,
                                GL.DebitAmount ,
                                GL.DebitAmountOriginal ,
                                GL.CreditAmount ,
                                GL.CreditAmountOriginal ,
                                $0 AS ClosingAmount ,
                                $0 AS ClosingAmountOC ,
                                GL.ContactName ,
                                GL.AccountingObjectID ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0 THEN 1
                                     WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0 THEN 2
                                     ELSE 3
                                END AS InvoiceOrder ,
                                GL.BranchID ,
                                0,
                               GL.AccountCorresponding
                        FROM    dbo.GeneralLedger AS GL ,
						        @tblAccountNumber Acc
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )
                                AND GL.Account = Acc.AccountNumber
                                AND ( @CurrencyID IS NULL
                                      OR GL.CurrencyID = @CurrencyID
                                    )
                                AND ( GL.CreditAmount <> 0
                                      OR GL.DebitAmount <> 0
                                      OR GL.CreditAmountOriginal <> 0
                                      OR GL.DebitAmountOriginal <> 0
                                    )) T
                        ORDER BY Account,
						        PostedDate ,
                                RefDate ,
                                InvoiceOrder 

		
        SET @ClosingAmount = 0
        SET @ClosingAmountOC = 0
        UPDATE  #Result
        SET     @ClosingAmount = @ClosingAmount  + ClosingAmount +ISNULL(DebitAmount, 0)
                - ISNULL(CreditAmount, 0) ,
                ClosingAmount = @ClosingAmount ,
                @ClosingAmountOC = @ClosingAmountOC + ClosingAmountOC + ISNULL(DebitAmountOC, 0)
                - ISNULL(CreditAmountOC, 0) ,
                ClosingAmountOC = @ClosingAmountOC 
        SELECT  *
        FROM    #Result R       
        DROP TABLE #Result 
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
/*
-- =============================================
-- Author:  HaiPL
-- ALTER date: 25122017
-- Description: <Hàm chuyển chuỗi các string thành bảng chứa các GUI>
-- =============================================
*/
ALTER FUNCTION [dbo].[Func_ConvertStringIntoTable]
    (
      @GUIString NVARCHAR(MAX),
      @SeparateCharacter NCHAR(1)
    )
RETURNS @ValueTable TABLE (Value UNIQUEIDENTIFIER)
AS BEGIN
 
    DECLARE @ValueListLength INT,
			@StartingPosition INT,
			@Value NVARCHAR(50),
			@SecondPosition INT
			
    SET @ValueListLength = LEN(@GUIString)
    SET @StartingPosition = 1
    SET @Value = SPACE(0)
 
	WHILE @StartingPosition < @ValueListLength
		BEGIN
			SET @SecondPosition = CHARINDEX(@SeparateCharacter,@GUIString,@StartingPosition+1)
			SET @Value = SUBSTRING(@GUIString,@StartingPosition+1,@SecondPosition-@StartingPosition-1)	
			SET @StartingPosition = @SecondPosition
			IF @Value <> SPACE(0)
				INSERT  INTO @ValueTable (Value) VALUES(CAST(@Value AS UNIQUEIDENTIFIER))
		END
	RETURN 
   END



GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DROP FUNCTION [dbo].[Func_GetAccountObjectName]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
/*
-- =============================================
-- Author:		HaiPL
-- Create date: <29/12/2017>
-- Description:	<Lấy danh sách tên nhóm KH/NCC theo danh sách mã nhóm KH/NCC>
-- =============================================
*/
create FUNCTION [dbo].[Func_GetAccountObjectName]
    (
      @CategoryListCode NVARCHAR(MAX)
    )
RETURNS NVARCHAR(MAX)
AS 
    BEGIN
        DECLARE @CategoryListName AS NVARCHAR(MAX)
        DECLARE @CategoryListNameTable AS TABLE
            (
              CategoryName NVARCHAR(255)
            )      
        INSERT  INTO @CategoryListNameTable
                ( CategoryName                         
                )
                SELECT AOG.AccountingObjectGroupName
                FROM (SELECT  Value
                        FROM    dbo.Func_ConvertStringIntoTable(';'+ @CategoryListCode+ ';',
                                                              ';')
					) List INNER JOIN AccountingObjectGroup AOG ON List.Value = AOG.AccountingObjectGroupCode
                

        SET @CategoryListName = ''
        SELECT  @CategoryListName = @CategoryListName + '; '
                + ISNULL(CategoryName, '')
        FROM    @CategoryListNameTable
        
        IF LEN(@CategoryListName) > 1 
            BEGIN
                SET @CategoryListName = SUBSTRING(@CategoryListName, 3,
                                                  LEN(@CategoryListName))
            END
        RETURN  ISNULL(@CategoryListName,NULL)
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
/*
-- =============================================
-- Author:		HaiPL
-- Create date: 02/01/2018
-- Description:	Lấy dữ liệu cho Báo cáo Chi tiết công nợ phải thu
-- =============================================
*/
ALTER PROCEDURE [dbo].[Proc_SA_ReceivableDetail]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3) ,
    @IsSimilarSum BIT 
AS
    BEGIN
        DECLARE @ID UNIQUEIDENTIFIER
        SET @ID = '00000000-0000-0000-0000-000000000000'
               
        CREATE TABLE #tblListAccountObjectID  
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectName NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectAddress NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectGroupListCode NVARCHAR(MAX)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              AccountObjectGroupListName NVARCHAR(MAX)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              CompanyTaxCode NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
                                          NULL
            ) 
        INSERT  INTO #tblListAccountObjectID
                SELECT  AO.ID ,
                        AccountingObjectCode ,
                        AccountingObjectName ,
                        [Address] ,
                        AOG.AccountingObjectGroupCode , 
                        AOG.AccountingObjectGroupName , 
                        CASE WHEN AO.ObjectType = 0 THEN TaxCode
                             ELSE ''
                        END AS CompanyTaxCode 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                           ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value 
                        INNER JOIN dbo.AccountingObjectGroup AOG ON AO.AccountObjectGroupID = AOG.ID 
          
        CREATE TABLE #tblResult
            (
              RowNum INT PRIMARY KEY
                         IDENTITY(1, 1) ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              AccountObjectName NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              AccountObjectGroupListCode NVARCHAR(MAX)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL , 
              AccountObjectGroupListName NVARCHAR(MAX)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,          
              AccountObjectAddress NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL , 
              CompanyTaxCode NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
                                          NULL ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS
                                 NULL ,
              InvDate DATETIME ,
              InvNo NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                  NULL ,
              
              RefType INT ,
              RefID UNIQUEIDENTIFIER ,                        
              RefDetailID NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
                                       NULL ,
              JournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                        NULL ,
              AccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,
              DebitAmount MONEY , 
              CreditAmountOC MONEY ,
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY , 
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50)
                COLLATE SQL_Latin1_General_CP1_CI_AS , 
              InventoryItemName NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS ,
              UnitName NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
                                    NULL ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(20, 6) ,            
              IsBold BIT , 
              OrderType INT ,
              BranchName NVARCHAR(128) COLLATE SQL_Latin1_General_CP1_CI_AS
                                       NULL ,
              GLJournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                          NULL 
              ,
              MasterCustomField1 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField2 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField3 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField4 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField5 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField6 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField7 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField8 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField9 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              MasterCustomField10 NVARCHAR(255)
                COLLATE SQL_Latin1_General_CP1_CI_AS
                NULL ,
              CustomField1 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField2 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField3 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField4 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField5 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField6 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField7 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField8 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField9 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              CustomField10 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                          NULL ,
              /*cột số lượng, đơn giá*/
              MainUnitName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         NULL ,
              MainUnitPrice DECIMAL(20, 6) ,
              MainQuantity DECIMAL(22, 8) ,
              /*mã thống kê, và tên loại chứng từ*/
              ListItemCode NVARCHAR(20) ,
              ListItemName NVARCHAR(128) ,
              RefTypeName NVARCHAR(128)
            )
        
        CREATE TABLE #tblAccountNumber
            (
              AccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS
                                         PRIMARY KEY ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL
            BEGIN
			INSERT  INTO #tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
            END
        ELSE
            BEGIN 
			   INSERT  INTO #tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   DetailType = '1'
                                AND Grade = 1
                                AND IsParentNode = 0
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END 

/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
		
        IF @IsSimilarSum = 0 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,  
                          AccountObjectName ,	
                          AccountObjectGroupListCode ,
                          AccountObjectGroupListName ,        
                          AccountObjectAddress , 
                          CompanyTaxCode , 
                          PostedDate ,
                          RefDate , 
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,	
                          RefID ,  
                          RefDetailID ,
                          JournalMemo , 				  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,			  
                          ExchangeRate , 		  
                          DebitAmountOC ,
                          DebitAmount , 
                          CreditAmountOC , 
                          CreditAmount , 
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,	
                          ClosingCreditAmount ,               
                          IsBold , 
                          OrderType
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode , 
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,           
                                AccountObjectAddress ,
                                CompanyTaxCode , 
                                PostedDate ,
                                RefDate ,
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID ,
                                RefDetailID ,
                                JournalMemo , 				  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  
                                ExchangeRate ,  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) ,
                                SUM(CreditAmount) ,
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN SUM(ClosingDebitAmountOC)
                                            - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) ) > 0
                                                 THEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,         
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( SUM(ClosingDebitAmount)
                                              - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount)
                                                        - SUM(ClosingCreditAmount) ) > 0
                                                 THEN SUM(ClosingDebitAmount)
                                                      - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmountOC)
                                              - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) ) > 0
                                                 THEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmount)
                                              - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) ) > 0
                                                 THEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,
                                
                                IsBold , 
                                OrderType 
                                
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,	
                                            LAOI.AccountObjectGroupListCode ,
                                            LAOI.AccountObjectGroupListName ,          
                                            LAOI.AccountObjectAddress , 
                                            LAOI.CompanyTaxCode , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,             
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefDate
                                            END AS RefDate , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefNo
                                            END AS RefNo , 
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN N'Số dư đầu kỳ'
                                                 ELSE ISNULL(AOL.DESCRIPTION,
                                                             AOL.Reason)
                                            END AS JournalMemo ,   
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber ,
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ExchangeRate
                                            END AS ExchangeRate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                               
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                     
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC ,           
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 1
                                                 ELSE 0
                                            END AS IsBold ,	
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType 
                                  FROM      dbo.GeneralLedger AS AOL
                                            INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                            INNER JOIN #tblListAccountObjectID
                                            AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )

                                ) AS RSNS
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode , 
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,        
                                RSNS.AccountObjectAddress ,
                                CompanyTaxCode , 
                                RSNS.PostedDate ,
                                RSNS.RefDate ,
                                RSNS.RefNo , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,   
                                RSNS.RefDetailID ,
                                RSNS.JournalMemo , 				  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber , 		  
                                RSNS.ExchangeRate ,           
                                RSNS.IsBold ,
                                RSNS.OrderType
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC
                                       - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount
                                       - ClosingCreditAmount) <> 0
                        ORDER BY RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.OrderType ,
                                RSNS.PostedDate ,
                                RSNS.RefDate ,
                                RSNS.RefNo 
            END
        ELSE 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,   
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,            
                          AccountObjectAddress ,
                          CompanyTaxCode ,
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID ,   
                          RefDetailID ,
                          JournalMemo , 			  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,		  
                          ExchangeRate ,		  
                          DebitAmountOC ,
                          DebitAmount ,
                          CreditAmountOC , 
                          CreditAmount ,
                          ClosingDebitAmountOC ,
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount ,                                    
                          IsBold , 
                          OrderType
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,  
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,             
                                AccountObjectAddress , 
                                CompanyTaxCode ,
                                PostedDate ,	
                                RefDate , 
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID , 
                                RefDetailID ,
                                JournalMemo ,				  
                                AccountNumber ,
                                AccountCategoryKind ,
                                CorrespondingAccountNumber ,			  
                                ExchangeRate , 	  
                                DebitAmountOC ,
                                DebitAmount ,
                                CreditAmountOC ,
                                CreditAmount , 
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmountOC
                                              - ClosingCreditAmountOC )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmountOC
                                                        - ClosingCreditAmountOC ) > 0
                                                 THEN ClosingDebitAmountOC
                                                      - ClosingCreditAmountOC
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,  
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmount
                                              - ClosingCreditAmount )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmount
                                                        - ClosingCreditAmount ) > 0
                                                 THEN ClosingDebitAmount
                                                      - ClosingCreditAmount
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmountOC
                                              - ClosingDebitAmountOC )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC ) > 0
                                                 THEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmount
                                              - ClosingDebitAmount )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount ) > 0
                                                 THEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,                                     
                                IsBold , 
                                OrderType
                        FROM    ( SELECT    AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode ,
                                            AD.AccountObjectGroupListName ,            
                                            AD.AccountObjectAddress ,
                                            CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType , 
                                            MAX(AD.RefDetailID) AS RefDetailID ,
                                            AD.JournalMemo ,   
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber , 
                                            AD.ExchangeRate , 
                                            SUM(AD.DebitAmountOC) AS DebitAmountOC ,                              
                                            SUM(AD.DebitAmount) AS DebitAmount ,                                        
                                            SUM(AD.CreditAmountOC) AS CreditAmountOC , 
                                            SUM(AD.CreditAmount) AS CreditAmount ,
                                            SUM(AD.ClosingDebitAmountOC) AS ClosingDebitAmountOC ,       
                                            SUM(AD.ClosingDebitAmount) AS ClosingDebitAmount ,
                                            SUM(AD.ClosingCreditAmountOC) AS ClosingCreditAmountOC , 
                                            SUM(AD.ClosingCreditAmount) AS ClosingCreditAmount ,
                                            AD.IsBold ,	
                                            AD.OrderType 
                                  FROM      ( SELECT    AOL.AccountingObjectID ,
                                                        LAOI.AccountObjectCode ,   
                                                        LAOI.AccountObjectName ,	
                                                        LAOI.AccountObjectGroupListCode ,
                                                        LAOI.AccountObjectGroupListName ,              
                                                        LAOI.AccountObjectAddress , 
                                                        LAOI.CompanyTaxCode , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.PostedDate
                                                        END AS PostedDate ,             
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefDate
                                                        END AS RefDate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefNo
                                                        END AS RefNo , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceDate
                                                        END AS InvDate ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceNo
                                                        END AS InvNo ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.ReferenceID
                                                        END AS RefID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.TypeID
                                                        END AS RefType ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE CAST(DetailID AS NVARCHAR(50))
                                                        END AS RefDetailID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN N'Số dư đầu kỳ'
                                                             ELSE AOL.Reason
                                                        END AS JournalMemo , 
                                                        TBAN.AccountNumber ,
                                                        TBAN.AccountCategoryKind ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.AccountCorresponding
                                                        END AS CorrespondingAccountNumber , 
                                                        
                                                        CASE WHEN AOL.PostedDate < @FromDate
															 THEN NULL
															 ELSE AOL.ExchangeRate
														END AS ExchangeRate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmountOriginal
                                                        END AS DebitAmountOC ,                         
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmount
                                                        END AS DebitAmount ,                           
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmountOriginal
                                                        END AS CreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmount
                                                        END AS CreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingDebitAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmount
                                                             ELSE $0
                                                        END AS ClosingDebitAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingCreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmount
                                                             ELSE $0
                                                        END AS ClosingCreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 1
                                                             ELSE 0
                                                        END AS IsBold ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 0
                                                             ELSE 1
                                                        END AS OrderType 
                                                        
                                              FROM      dbo.GeneralLedger
                                                        AS AOL
                                                        INNER JOIN #tblListAccountObjectID
                                                        AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                                        INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                              WHERE     AOL.PostedDate <= @ToDate
                                                        AND ( @CurrencyID IS NULL
                                                              OR AOL.CurrencyID = @CurrencyID
                                                            )
                                            ) AS AD
                                  GROUP BY  AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode , 
                                            AD.AccountObjectGroupListName ,     
                                            AD.AccountObjectAddress ,
                                            AD.CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType ,
                                            AD.JournalMemo ,
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber ,  
                                            AD.ExchangeRate ,
                                            AD.IsBold , 	
                                            AD.OrderType
                                ) AS RSS
                        WHERE   RSS.DebitAmountOC <> 0
                                OR RSS.CreditAmountOC <> 0
                                OR RSS.DebitAmount <> 0
                                OR RSS.CreditAmount <> 0
                                OR ClosingCreditAmountOC
                                - ClosingDebitAmountOC <> 0
                                OR ClosingCreditAmount - ClosingDebitAmount <> 0
                        ORDER BY RSS.AccountObjectCode ,
                                RSS.AccountNumber ,
                                RSS.OrderType ,
                                RSS.PostedDate ,
                                RSS.RefDate ,
                                RSS.RefNo ,
                                RSS.InvDate ,
                                RSS.InvNo
            END

/* Tính số tồn */
        DECLARE @CloseAmountOC AS DECIMAL(22, 8) ,
            @CloseAmount AS DECIMAL(22, 8) ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0
                                        THEN ( CASE WHEN ClosingDebitAmountOC = 0
                                                    THEN ClosingCreditAmountOC
                                                    ELSE -1
                                                         * ClosingDebitAmountOC
                                               END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber
                                        THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC
                                             - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0
                                              THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1
                                              THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0
                                                        THEN -1
                                                             * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1
                                               THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0
                                               THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0
                                                         THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0
                                      THEN ( CASE WHEN ClosingDebitAmount = 0
                                                  THEN ClosingCreditAmount
                                                  ELSE -1 * ClosingDebitAmount
                                             END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber
                                      THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount
                                           - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0
                                            THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1
                                            THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0
                                                      THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1
                                             THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0
                                             THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0
                                                       THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
        SELECT  RS.*
        FROM    #tblResult RS
        ORDER BY RowNum
        
        DROP TABLE #tblResult
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

/*=================================================================================================
 * Created By:		haipl
 * Created Date:	25/12/2017
 * Description:		Lay du lieu cho bao cao << Sổ chi tiết bán hàng >>
===================================================================================================== */
ALTER PROCEDURE [dbo].[Proc_SA_GetSalesBookDetail]
  
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @InventoryItemID NVARCHAR(MAX) , 
    @OrganizationUnitID UNIQUEIDENTIFIER ,
    @AccountObjectID NVARCHAR(MAX) 
AS

    BEGIN
        SET NOCOUNT ON;
           
		                          
        CREATE TABLE #tblListInventoryItemID
            (
              InventoryItemID UNIQUEIDENTIFIER PRIMARY KEY
            ) 
        INSERT  INTO #tblListInventoryItemID
                    SELECT  T.*
                    FROM    dbo.Func_ConvertStringIntoTable(@InventoryItemID,
                                                              ',')	
                                                              AS T  
        select a.Date as ngay_CT, a.PostedDate as ngay_HT, a.No as So_Hieu,a.InvoiceDate as Ngay_HD, a.InvoiceNo as SO_HD, Sreason as Dien_giai, accountCorresponding as TK_DOIUNG,
		c.Unit as DVT, Quantity as SL, UnitPriceOriginal as DON_GIA, sum(debitAmount) as KHAC, sum(CreditAmount) as TT, c.ID as  InventoryItemID,c.MaterialGoodsName
		from GeneralLedger a join SAInvoiceDetail b on a.DetailID = b.ID 
		join MaterialGoods c on b.MaterialGoodsID = c.ID
		join SAInvoice d on a.ReferenceID = d.ID
		where a.PostedDate between  @FromDate and @ToDate and c.ID in (select InventoryItemID from #tblListInventoryItemID)
		and Account like '5%'
		group by a.Date,  a.PostedDate, a.No ,a.InvoiceDate , a.InvoiceNo , Sreason , accountCorresponding , 
		c.Unit , Quantity , UnitPriceOriginal, c.ID,c.MaterialGoodsName ;
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
/*
-- =============================================
-- Author:		Haipl
-- Create date: 06.01.2018
-- Description:	Báo cáo Tổng hợp công nợ phải thu theo nhóm khách hàng
-- Proc_SA_GetReceivableSummaryByGroup '1/1/2015','9/30/2015',NULL, 1, NULL,',0091378C-F09D-47E9-B326-151EC3712BEF,',NULL,0
-- =============================================
*/
ALTER PROCEDURE [dbo].[Proc_SA_GetReceivableSummaryByGroup]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountNumber NVARCHAR(25),
    @AccountObjectID AS NVARCHAR(MAX),
    @CurrencyID NVARCHAR(3),
    @AccountObjectGroupID AS NVARCHAR(MAX) 
AS
    BEGIN          
        DECLARE @tblListAccountObjectID TABLE
            (
             AccountObjectID UNIQUEIDENTIFIER ,
             AccountObjectGroupList NVARCHAR(MAX)           
            ) 
             
        INSERT  INTO @tblListAccountObjectID
        SELECT  
			AO.ID
			,AO.AccountObjectGroupID
        FROM AccountingObject AS AO 
        LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') AS TLAO 
			ON AO.ID = TLAO.Value
        WHERE 
		(@AccountObjectID IS NULL OR TLAO.Value IS NOT NULL)
                                    		
        DECLARE @tblAccountNumber TABLE
            (
             AccountNumber NVARCHAR(255) PRIMARY KEY,
             AccountName NVARCHAR(255),
             AccountNumberPercent NVARCHAR(255),
             AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber,
                                A.AccountName,
                                A.AccountNumber + '%',
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber,
                                A.AccountName                 
            END
        ELSE
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber,
                                A.AccountName,
                                A.AccountNumber + '%',
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   DetailType = '1'
                                AND Grade = 1
                                AND IsParentNode = 0
                        ORDER BY A.AccountNumber,
                                A.AccountName
            END
	    
	    DECLARE @tblDebt TABLE
			(
				AccountObjectID			UNIQUEIDENTIFIER
				,AccountNumber			NVARCHAR (25)				
				,AccountObjectGroupList NVARCHAR(Max)				
				,OpenDebitAmountOC		DECIMAL (28,4)
				,OpenDebitAmount		DECIMAL (28,4)
				,OpenCreditAmountOC		DECIMAL (28,4)
				,OpenCreditAmount		DECIMAL (28,4)
				,DebitAmountOC			DECIMAL (28,4)
				,DebitAmount			DECIMAL (28,4)
				,CreditAmountOC			DECIMAL (28,4)
				,CreditAmount			DECIMAL (28,4)
				,CloseDebitAmountOC		DECIMAL (28,4)
				,CloseDebitAmount		DECIMAL (28,4)
				,CloseCreditAmountOC	DECIMAL (28,4)
				,CloseCreditAmount		DECIMAL (28,4)					
			)				
		INSERT @tblDebt
        
        SELECT  
                AccountingObjectID,               
                AccountNumber,       
                AccountObjectGroupList,
                (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN (SUM(OpenningDebitAmountOC - OpenningCreditAmountOC)) > 0
                                THEN (SUM(OpenningDebitAmountOC - OpenningCreditAmountOC))
                                ELSE $0
                           END
                 END) AS OpenningDebitAmountOC,        
                (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmount - OpenningCreditAmount)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN SUM(OpenningDebitAmount - OpenningCreditAmount) > 0
                                THEN SUM(OpenningDebitAmount  - OpenningCreditAmount)
                                ELSE $0
                           END
                 END) AS OpenningDebitAmount,
                (CASE WHEN AccountCategoryKind = 1
                      THEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC))
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC)) > 0
                                THEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC))
                                ELSE $0
                           END
                 END) AS OpenningCreditAmountOC,
                (CASE WHEN AccountCategoryKind = 1
                      THEN (SUM(OpenningCreditAmount - OpenningDebitAmount))
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmount - OpenningDebitAmount)) > 0
                                THEN (SUM(OpenningCreditAmount - OpenningDebitAmount))
                                ELSE $0
                           END
                 END) AS OpenningCreditAmount, 
                SUM(DebitAmountOC) AS DebitAmountOC,
                SUM(DebitAmount) AS DebitAmount, 
                SUM(CreditAmountOC) AS CreditAmountOC,
                SUM(CreditAmount) AS CreditAmount, 
                (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC + DebitAmountOC - CreditAmountOC)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC + DebitAmountOC - CreditAmountOC) > 0
                                THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC + DebitAmountOC - CreditAmountOC)
                                ELSE $0
                           END
                 END) AS CloseDebitAmountOC,	
                 (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                               + DebitAmount - CreditAmount)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN SUM(OpenningDebitAmount
                                         - OpenningCreditAmount - CreditAmount
                                         + DebitAmount) > 0
                                THEN SUM(OpenningDebitAmount
                                         - OpenningCreditAmount - CreditAmount
                                         + DebitAmount)
                                ELSE $0
                           END
                 END) AS CloseDebitAmount,
                (CASE WHEN AccountCategoryKind = 1
                      THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC - DebitAmountOC + CreditAmountOC)
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC - DebitAmountOC + CreditAmountOC)) > 0
                                THEN SUM(OpenningCreditAmountOC  - OpenningDebitAmountOC - DebitAmountOC + CreditAmountOC)
                                ELSE $0
                           END
                 END) AS CloseCreditAmountOC,	     
                (CASE WHEN AccountCategoryKind = 1
                      THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                               + CreditAmount - DebitAmount)
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)) > 0
                                THEN SUM(OpenningCreditAmount
                                         - OpenningDebitAmount + CreditAmount
                                         - DebitAmount)
                                ELSE $0
                           END
                 END) AS CloseCreditAmount
        FROM    (SELECT AOL.AccountingObjectID,                        
                        TBAN.AccountNumber,
                        TBAN.AccountCategoryKind,
                        ISNULL(AccountObjectGroupList,'') AccountObjectGroupList,
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.DebitAmountOriginal
                             ELSE $0
                        END) AS OpenningDebitAmountOC,	
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.DebitAmount
                             ELSE $0
                        END) AS OpenningDebitAmount, 
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.CreditAmountOriginal
                             ELSE $0
                        END) AS OpenningCreditAmountOC, 
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.CreditAmount
                             ELSE $0
                        END) AS OpenningCreditAmount,                                         
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.DebitAmountOriginal
                             ELSE 0
                        END) AS DebitAmountOC,                      
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.DebitAmount
                             ELSE 0
                        END) AS DebitAmount,                    
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.CreditAmountOriginal
                             ELSE 0
                        END) AS CreditAmountOC, 
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.CreditAmount
                             ELSE 0
                        END) AS CreditAmount
                 FROM   dbo.GeneralLedger AS AOL                       
                        INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                        INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                        INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                      
                 WHERE  AOL.PostedDate <= @ToDate
                        AND (@CurrencyID IS NULL OR AOL.CurrencyID = @CurrencyID)
                        AND AN.DetailType = '1'
                 GROUP BY 
						AOL.AccountingObjectID,                       
                        TBAN.AccountNumber, 
                        TBAN.AccountCategoryKind
                        ,AccountObjectGroupList                      
                ) AS RSNS
       
        GROUP BY RSNS.AccountingObjectID,               
                 RSNS.AccountNumber 
                 ,RSNS.AccountCategoryKind
                 ,RSNS.AccountObjectGroupList
        HAVING
				SUM(DebitAmountOC)<>0 OR
                SUM(DebitAmount)<>0 OR
                SUM(CreditAmountOC) <>0 OR
                SUM(CreditAmount) <>0 OR
                SUM(OpenningDebitAmountOC - OpenningCreditAmountOC)<>0 OR
                SUM(OpenningDebitAmount - OpenningCreditAmount)<>0
                

		SELECT   AOG.ID,
		        AOG.AccountingObjectGroupCode,
				AOG.AccountingObjectGroupName
		        ,AO.AccountingObjectCode
				,AO.AccountingObjectName
				,Ao.Address AS AccountObjectAddress
				,D.* FROM @tblDebt D
		INNER JOIN dbo.AccountingObject AO ON D.AccountObjectID = AO.ID
		INNER JOIN dbo.AccountingObjectGroup AOG ON AO.AccountObjectGroupID = AOG.ID
		ORDER BY D.AccountNumber
		
		
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
/*
-- =============================================
-- Author:		HaiPL
-- Create date: 29/12/2017
-- Description:	Báo cáo Tổng hợp công nợ phải thu

-- =============================================
*/
ALTER PROCEDURE [dbo].[Proc_SA_GetReceivableSummary]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER ,
    @AccountNumber NVARCHAR(25) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3) ,
    @IsShowInPeriodOnly BIT 
AS
    BEGIN
               
        DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectGroupListCode NVARCHAR(MAX) ,
              AccountObjectCategoryName NVARCHAR(MAX)
            ) 
             
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID ,
                        AO.AccountObjectGroupID ,
						null
                FROM    AccountingObject AS AO
                        LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                              ',') AS TLAO ON AO.ID = TLAO.Value
                        
                WHERE  
				      ( @AccountObjectID IS NULL
                              OR TLAO.Value IS NOT NULL
                            )
                              			
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(255) PRIMARY KEY ,
              AccountName NVARCHAR(255) ,
              AccountNumberPercent NVARCHAR(255) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   DetailType = '1'
                                AND Grade = 1
                                AND IsParentNode = 0
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
            
	    DECLARE @FirstDateOfYear AS DATETIME
	    SET @FirstDateOfYear = '1/1/' + CAST(Year(@FromDate) AS NVARCHAR(4))	   
	    
        SELECT  ROW_NUMBER() OVER ( ORDER BY AccountingObjectCode ) AS RowNum ,
                AccountingObjectID ,
                AccountingObjectCode ,
                AccountingObjectName ,
                AccountObjectAddress ,
                AccountObjectTaxCode ,
                AccountNumber , 
                AccountCategoryKind ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC)
                            - SUM(OpenningCreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) ) > 0
                                 THEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpenningDebitAmountOC ,        
                ( CASE WHEN AccountCategoryKind = 0
                       THEN ( SUM(OpenningDebitAmount - OpenningCreditAmount) )
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmount
                                            - OpenningCreditAmount) ) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount)
                                 ELSE $0
                            END
                  END ) AS OpenningDebitAmount , 
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmountOC
                                  - OpenningDebitAmountOC) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) ) > 0
                                 THEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpenningCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmount - OpenningDebitAmount) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) ) > 0
                                 THEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) )
                                 ELSE $0
                            END
                  END ) AS OpenningCreditAmount ,
                SUM(DebitAmountOC) AS DebitAmountOC ,
                SUM(DebitAmount) AS DebitAmount ,
                SUM(CreditAmountOC) AS CreditAmountOC ,
                SUM(CreditAmount) AS CreditAmount ,
                SUM(AccumDebitAmountOC) AS AccumDebitAmountOC ,
                SUM(AccumDebitAmount) AS AccumDebitAmount ,
                SUM(AccumCreditAmountOC) AS AccumCreditAmountOC ,
                SUM(AccumCreditAmount) AS AccumCreditAmount , 
                                        
                /* Số dư cuối kỳ = Dư Có đầu kỳ - Dư Nợ đầu kỳ + Phát sinh Có – Phát sinh Nợ
				Nếu Số dư cuối kỳ >0 thì hiển bên cột Dư Có cuối kỳ 
				Nếu số dư cuối kỳ <0 thì hiển thị bên cột Dư Nợ cuối kỳ */
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC
                                + DebitAmountOC - CreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC) > 0
                                 THEN SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseDebitAmountOC ,	
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC
                                - DebitAmountOC + CreditAmountOC)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC
                                            - DebitAmountOC + CreditAmountOC) ) > 0
                                 THEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          - DebitAmountOC + CreditAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmountOC ,	
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                                + DebitAmount - CreditAmount)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount
                                          - CreditAmount + DebitAmount) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount
                                          - CreditAmount + DebitAmount)
                                 ELSE $0
                            END
                  END ) AS CloseDebitAmount ,	
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                                + CreditAmount - DebitAmount)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount
                                            + CreditAmount - DebitAmount) ) > 0
                                 THEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmount ,	
                AccountObjectGroupListCode ,
                AccountObjectCategoryName
        FROM    ( SELECT    AO.ID as AccountingObjectID ,
                            AO.AccountingObjectCode ,  
                            AO.AccountingObjectName ,	
                            AO.Address AS AccountObjectAddress ,
                            AO.TaxCode AS AccountObjectTaxCode , 
                            TBAN.AccountNumber , 
                            TBAN.AccountCategoryKind , 
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmountOC ,
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.DebitAmount
                                 ELSE $0
                            END AS OpenningDebitAmount ,
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmountOC , 
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.CreditAmount
                                 ELSE $0
                            END AS OpenningCreditAmount ,                                                
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.DebitAmountOriginal
                                 ELSE 0
                            END AS DebitAmountOC ,                       
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.DebitAmount
                                 ELSE 0
                            END AS DebitAmount ,                                    
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.CreditAmountOriginal
                                 ELSE 0
                            END AS CreditAmountOC ,
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.CreditAmount
                                 ELSE 0
                            END AS CreditAmount ,
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.DebitAmountOriginal
                                 ELSE 0
                            END AS AccumDebitAmountOC ,                                  
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.DebitAmount
                                 ELSE 0
                            END AS AccumDebitAmount ,                                       
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.CreditAmountOriginal
                                 ELSE 0
                            END AS AccumCreditAmountOC ,
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.CreditAmount
                                 ELSE 0
                            END AS AccumCreditAmount ,
                            LAOI.AccountObjectGroupListCode ,
                            LAOI.AccountObjectCategoryName 
                  FROM      dbo.GeneralLedger AS GL
                            INNER JOIN dbo.AccountingObject AO ON AO.ID = GL.AccountingObjectID
                            INNER JOIN @tblAccountNumber TBAN ON GL.Account LIKE TBAN.AccountNumberPercent
                            INNER JOIN dbo.Account AS AN ON GL.Account = AN.AccountNumber
                            INNER JOIN @tblListAccountObjectID AS LAOI ON GL.AccountingObjectID = LAOI.AccountObjectID
                  WHERE     GL.PostedDate <= @ToDate
                            AND ( @CurrencyID IS NULL
                                  OR GL.CurrencyID = @CurrencyID
                                )
                            AND AN.DetailType = '1'
                ) AS RSNS
        GROUP BY RSNS.AccountingObjectID ,
                RSNS.AccountingObjectCode ,
                RSNS.AccountingObjectName ,    
                RSNS.AccountObjectAddress ,
                RSNS.AccountObjectTaxCode , 
                RSNS.AccountNumber ,
                RSNS.AccountCategoryKind ,
                RSNS.AccountObjectGroupListCode ,
                RSNS.AccountObjectCategoryName 
        HAVING  SUM(DebitAmountOC) <> 0
                OR SUM(DebitAmount) <> 0
                OR SUM(CreditAmountOC) <> 0
                OR SUM(CreditAmount) <> 0
                OR SUM(OpenningDebitAmountOC - OpenningCreditAmountOC) <> 0
                OR SUM(OpenningDebitAmount - OpenningCreditAmount) <> 0                
                OR(@IsShowInPeriodOnly = 0 AND (
                 SUM(AccumDebitAmountOC) <>0	
                OR SUM(AccumDebitAmount) <>0
                OR SUM(AccumCreditAmountOC) <>0  
                OR SUM(AccumCreditAmount)<>0))
        ORDER BY RSNS.AccountingObjectCode
        OPTION(RECOMPILE)
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

/*
--=============================================
-- Author:		HaiPL
-- Create date: 18/01/2018
-- Description:	Lấy dữ liệu cho Báo cáo Chi tiết công nợ phải trả

-- =============================================
*/
ALTER PROCEDURE [dbo].[Proc_PO_PayableDetail]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS 
    BEGIN     
        DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              AccountObjectAddress NVARCHAR(255) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        AOG.AccountingObjectGroupCode ,
                        AOG.AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
                        INNER JOIN dbo.AccountingObjectGroup AOG ON AO.AccountObjectGroupID = AOG.ID
      
        CREATE TABLE #tblResult
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              AccountObjectGroupListCode NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS
                                                       NULL ,             
              AccountObjectAddress NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              DueDate DATETIME , 
              InvDate DATETIME ,
              InvNo NVARCHAR(25) COLLATE SQL_Latin1_General_CP1_CI_AS ,

              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              JournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS , 
              AccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS , 
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS , 
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,	
              DebitAmount MONEY , 
              CreditAmountOC MONEY , 
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY ,
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,	
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              InventoryItemName NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              UnitName NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(18, 4) , 
              BranchName NVARCHAR(128) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              IsBold BIT ,	
              OrderType INT ,
              GLJournalMemo NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              DocumentIncluded NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField1 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField2 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField3 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField4 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField5 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField6 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField7 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField8 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField9 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              CustomField10 NVARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS,
               MainUnitName NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS ,
              MainQuantity DECIMAL(22, 8) ,
              MainUnitPrice DECIMAL(18, 4)  
              
            )
        
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(255) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
            
                
/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode , 
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,      
                          AccountObjectAddress , 
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID , 
                          JournalMemo , 				  
                          AccountNumber , 
                          AccountCategoryKind ,
                          CorrespondingAccountNumber ,	  			  
                          DebitAmountOC ,	
                          DebitAmount , 
                          CreditAmountOC ,
                          CreditAmount ,
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount ,
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount,
						  OrderType   
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,
                                AccountObjectName ,	
                                AccountObjectGroupListCode , 
                                AccountObjectGroupListName ,            
                                AccountObjectAddress , 
                                RSNS.PostedDate ,
                                RefDate ,
                                RefNo ,
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RSNS.RefID , 
                                JournalMemo ,			  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  			  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) , 
                                SUM(CreditAmount) , 
                                ( CASE WHEN AccountCategoryKind = 0 THEN SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) ) > 0 THEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,      
                                ( CASE WHEN AccountCategoryKind = 0 THEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) ) > 0 THEN SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) ) > 0 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) ) > 0 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount,  
								  OrderType
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,
                                            AccountObjectGroupListCode ,
                                            AccountObjectGroupListName ,         
                                            LAOI.AccountObjectAddress ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,                      
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE RefDate
                                            END AS RefDate , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.RefNo
                                            END AS RefNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType , 
                                            CASE WHEN AOL.PostedDate < @FromDate 
												      THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN N'Số dư đầu kỳ'
                                                 ELSE AOL.Description
                                            END AS JournalMemo , 
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                             
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                  
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount,
											CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType 
                                  FROM      dbo.GeneralLedger AS AOL
                                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                                         
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )
                                ) AS RSNS                               
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode ,  
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,
                                RSNS.AccountObjectAddress ,
                                RSNS.PostedDate ,
                                RSNS.RefDate , 
                                RSNS.RefNo , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,
                                RSNS.JournalMemo , 		  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber,
								OrderType 		   
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount - ClosingCreditAmount) <> 0
                        ORDER BY RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.PostedDate ,
                                RSNS.RefDate ,
                                RSNS.RefNo
                OPTION  ( RECOMPILE )    
                        
/* Tính số tồn */
        DECLARE @CloseAmountOC AS DECIMAL(22, 8) ,
            @CloseAmount AS DECIMAL(22, 8) ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmountOC = 0 THEN ClosingCreditAmountOC
                                                                       ELSE -1 * ClosingDebitAmountOC
                                                                  END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1 THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0 THEN -1 * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0 THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0 THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmount = 0 THEN ClosingCreditAmount
                                                                     ELSE -1 * ClosingDebitAmount
                                                                END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1 THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0 THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0 THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0 THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
        SELECT 
                RS.*,
				fn.OpeningDebitAmount,
				fn.OpeningCreditAmount
				
        FROM    #tblResult RS ,
		        [dbo].[Func_SoDu] (
   @FromDate
  ,@ToDate
  ,3) fn
  where rs.AccountNumber = fn.AccountNumber
  and rs.AccountObjectID = fn.AccountObjectID
        DROP TABLE #tblResult
    END				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

	CREATE FUNCTION [dbo].[fn_diagramobjects]() 
	RETURNS int
	WITH EXECUTE AS N'dbo'
	AS
	BEGIN
		declare @id_upgraddiagrams		int
		declare @id_sysdiagrams			int
		declare @id_helpdiagrams		int
		declare @id_helpdiagramdefinition	int
		declare @id_creatediagram	int
		declare @id_renamediagram	int
		declare @id_alterdiagram 	int 
		declare @id_dropdiagram		int
		declare @InstalledObjects	int

		select @InstalledObjects = 0

		select 	@id_upgraddiagrams = object_id(N'dbo.sp_upgraddiagrams'),
			@id_sysdiagrams = object_id(N'dbo.sysdiagrams'),
			@id_helpdiagrams = object_id(N'dbo.sp_helpdiagrams'),
			@id_helpdiagramdefinition = object_id(N'dbo.sp_helpdiagramdefinition'),
			@id_creatediagram = object_id(N'dbo.sp_creatediagram'),
			@id_renamediagram = object_id(N'dbo.sp_renamediagram'),
			@id_alterdiagram = object_id(N'dbo.sp_alterdiagram'), 
			@id_dropdiagram = object_id(N'dbo.sp_dropdiagram')

		if @id_upgraddiagrams is not null
			select @InstalledObjects = @InstalledObjects + 1
		if @id_sysdiagrams is not null
			select @InstalledObjects = @InstalledObjects + 2
		if @id_helpdiagrams is not null
			select @InstalledObjects = @InstalledObjects + 4
		if @id_helpdiagramdefinition is not null
			select @InstalledObjects = @InstalledObjects + 8
		if @id_creatediagram is not null
			select @InstalledObjects = @InstalledObjects + 16
		if @id_renamediagram is not null
			select @InstalledObjects = @InstalledObjects + 32
		if @id_alterdiagram  is not null
			select @InstalledObjects = @InstalledObjects + 64
		if @id_dropdiagram is not null
			select @InstalledObjects = @InstalledObjects + 128
		
		return @InstalledObjects 
	END
	
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GRANT EXECUTE ON [dbo].[fn_diagramobjects] TO [public]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
/*
==================================================
Data Version 18.3.15.7
==================================================
*/
update TemplateColumn set VisiblePosition= '47' where ID='2AC09628-C69A-4379-B646-5B435968B31B'
update TemplateColumn set VisiblePosition= '48' where ID='972E547E-CB1A-461F-9B9D-E839BCA07FB3'
update TemplateColumn set VisiblePosition= '49' where ID='90077261-F9CF-4E97-ABAE-C2F9C7394D18'
GO

INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'390D7423-84D5-44A4-A108-6CE290854CD2', N'54E26D46-5908-421B-BA5D-58DF9FEF76EA', N'ConfrontInvNo', N'Số hóa đơn', N'Số hóa đơn', 0, 1, 81, 0, 0, 1, 50)
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'209313FC-6D9A-49E6-B8F8-F3ABD78BB918', N'74E58FFB-76D7-4504-93CD-9AC421D4C9F8', N'ConfrontInvNo', N'Số hóa đơn', N'Số hóa đơn', 0, 1, 81, 0, 0, 1, 50)
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'61A80441-FD72-4173-AE6B-7C4986935BF7', N'5C8F394B-080E-4C92-AE4E-57827FE9565F', N'ConfrontInvNo', N'Số hóa đơn', N'Số hóa đơn', 0, 1, 81, 0, 0, 1, 50)
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'1A5E9065-F422-4EFE-9233-3BD694535291', N'54E26D46-5908-421B-BA5D-58DF9FEF76EA', N'ConfrontInvDate', N'Ngày hóa đơn', N'Ngày hóa đơn', 0, 1, 105, 0, 0, 1, 51)
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'62BAB2FD-FE33-4703-841E-FDA887C34E3C', N'74E58FFB-76D7-4504-93CD-9AC421D4C9F8', N'ConfrontInvDate', N'Ngày hóa đơn', N'Ngày hóa đơn', 0, 1, 105, 0, 0, 1, 51)
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'8AF899C5-3DC9-4E2A-A028-70FE5BE3DC9F', N'5C8F394B-080E-4C92-AE4E-57827FE9565F', N'ConfrontInvDate', N'Ngày hóa đơn', N'Ngày hóa đơn', 0, 1, 105, 0, 0, 1, 51)
GO

INSERT [dbo].[AccountGroup] ([ID], [AccountGroupName], [AccountGroupKind], [DetailType]) VALUES (N'128', N'Đầu tư nắm giữ đến ngày đáo hạn', 0, N'-1')
INSERT [dbo].[AccountGroup] ([ID], [AccountGroupName], [AccountGroupKind], [DetailType]) VALUES (N'136', N'Phải thu nội bộ', 0, N'-1')
INSERT [dbo].[AccountGroup] ([ID], [AccountGroupName], [AccountGroupKind], [DetailType]) VALUES (N'151', N'Hàng mua đang đi đường', 0, N'-1')
INSERT [dbo].[AccountGroup] ([ID], [AccountGroupName], [AccountGroupKind], [DetailType]) VALUES (N'228', N'Đầu tư góp vốn vào đơn vị khác', 0, N'-1')
INSERT [dbo].[AccountGroup] ([ID], [AccountGroupName], [AccountGroupKind], [DetailType]) VALUES (N'336', N'Phải trả nội bộ', 0, N'-1')
GO

INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'399F97CE-8CA2-46C2-9069-2FE7A0D854CC', N'128', N'Đầu tư nắm giữ đến ngày đáo hạn', NULL, NULL, NULL, 0, 1, N'128', 0, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'747727CD-3D84-49E6-BEFD-2A549A62FF65', N'136', N'Phải thu nội bộ', NULL, NULL, NULL, 0, 1, N'136', 0, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'EA2EAA7A-7450-4F91-8225-CFC6FC4AB16F', N'151', N'Hàng mua đang đi đường', NULL, NULL, NULL, 0, 1, N'151', 0, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'04CFEB78-C9D3-47E4-94E1-461E00E98926', N'228', N'Đầu tư góp vốn vào đơn vị khác', NULL, NULL, NULL, 0, 1, N'228', 0, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'4A0760B1-FDFA-4439-B0EE-3F6D5FDABC42', N'336', N'Phải trả nội bộ', NULL, NULL, NULL, 0, 1, N'336', 1, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'2A570657-3768-4B8D-B575-6A95EA96A80A', N'1281', N'Tiền gửi có kỳ hạn', NULL, NULL, N'399F97CE-8CA2-46C2-9069-2FE7A0D854CC', 0, 2, N'128', 0, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'F2673BFE-F9A9-4E69-A92E-6BF9390027F6', N'1288', N'Các khoản đầu tư khác nắm giữ đến ngày đáo hạn', NULL, NULL, N'399F97CE-8CA2-46C2-9069-2FE7A0D854CC', 0, 2, N'128', 0, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'162A30B8-B517-4CF8-9C43-24A64D0925C8', N'1361', N'Vốn kinh doanh ở đơn vị trực thuộc', NULL, NULL, N'747727CD-3D84-49E6-BEFD-2A549A62FF65', 0, 2, N'136', 0, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'7CC5ABBE-E26B-4D5C-AA3C-D01AE4AFA07D', N'1368', N'Phải thu nội bộ khác', NULL, NULL, N'747727CD-3D84-49E6-BEFD-2A549A62FF65', 0, 2, N'136', 0, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'A0F55128-0A61-4101-AAA8-72CC4CFFC624', N'2281', N'Đầu tư vào công ty liên doanh, liên kết', NULL, NULL, N'04CFEB78-C9D3-47E4-94E1-461E00E98926', 0, 2, N'228', 0, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'072A239A-21AC-45BB-B0D9-19B61CE777A8', N'2288', N'Đầu tư khác', NULL, NULL, N'04CFEB78-C9D3-47E4-94E1-461E00E98926', 0, 2, N'228', 0, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'DA8ED7FC-845B-40A6-AE5F-B4632D787E68', N'3361', N'Phải trả nội bộ về vốn kinh doanh', NULL, NULL, N'4A0760B1-FDFA-4439-B0EE-3F6D5FDABC42', 0, 2, N'336', 1, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'80ACF9C2-19BD-40AC-BD67-F472683F2721', N'3368', N'Phải trả nội bộ khác', NULL, NULL, N'4A0760B1-FDFA-4439-B0EE-3F6D5FDABC42', 0, 2, N'336', 1, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'8B30B5F3-B6D8-41B2-8691-512316278FFF', N'3385', N'Bảo hiểm thất nghiệp', NULL, NULL, '8CB3FEF4-5EE4-413C-BD1A-A19D0D668301', 0, 2, N'338', 2, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'8A664628-08D0-4933-AFA0-F7E9B18B2865', N'3521', N'Dự phòng bảo hành sản phẩm hàng hóa', NULL, NULL, '05D067F4-5CD2-49A1-9C5F-70E03D927CF4', 0, 2, N'352', 1, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'76A9DF4A-9452-46EC-AC98-1F983292DE8F', N'3522', N'Dự phòng bảo hành công trình xây dựng', NULL, NULL, '05D067F4-5CD2-49A1-9C5F-70E03D927CF4', 0, 2, N'352', 1, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'61B686E2-A5A3-4899-A586-21082BCD8687', N'3524', N'Dự phòng phải trả khác', NULL, NULL, '05D067F4-5CD2-49A1-9C5F-70E03D927CF4', 0, 2, N'352', 1, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'F298FAC8-FEA0-449B-982C-CCA1D92E9200', N'33381', N'Thuế bảo vệ môi trường', NULL, NULL, '2C2F861C-D50D-4B10-929D-FDCF63E0C2B4', 0, 3, N'3338', 2, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'B9B17B43-02F6-4EAB-955E-6F3861230223', N'33382', N'Các loại thuế khác', NULL, NULL, '2C2F861C-D50D-4B10-929D-FDCF63E0C2B4', 0, 3, N'3338', 2, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'B9834976-0B39-4FF8-BF0C-38E065753F0B', N'2291', N'Dự phòng giảm giá chứng khoán kinh doanh', NULL, NULL, '580759E3-3322-4058-9E6D-D303DF12F9F3', 0, 2, N'229', 0, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'FFA2144A-CA74-4880-9F2E-4AFEF0F9609C', N'2292', N'Dự phòng tổn thất đầu tư vào đơn vị khác', NULL, NULL, '580759E3-3322-4058-9E6D-D303DF12F9F3', 0, 2, N'229', 0, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'42760517-E110-45C8-886B-89D9B7F9FA02', N'2293', N'Dự phòng phải thu khó đòi', NULL, NULL, '580759E3-3322-4058-9E6D-D303DF12F9F3', 0, 2, N'229', 0, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'B30A4B77-34F9-488D-A0E7-B35C59242ADD', N'2294', N'Dự phòng giảm giá hàng tồn kho', NULL, NULL, '580759E3-3322-4058-9E6D-D303DF12F9F3', 0, 2, N'229', 0, N'-1', 1)
INSERT [dbo].[Account] ([ID], [AccountNumber], [AccountName], [AccountNameGlobal], [Description], [ParentID], [IsParentNode], [Grade], [AccountGroupID], [AccountGroupKind], [DetailType], [IsActive]) VALUES (N'9046F5CF-8E7B-43CC-B2A6-E81CCC306C21', N'1386', N'Cầm cố, thế chấp, ký quỹ, ký cược', NULL, NULL, '0F464EC8-858B-4802-9097-7D8ED6E02565', 0, 2, N'138', 2, N'-1', 1)
GO

Update Account set AccountName = N'Chứng khoán kinh doanh' where AccountNumber = 121
Update Account set AccountName = N'Dự phòng tổn thất tài sản' where AccountNumber = 229
Update Account set AccountName = N'Chi phí trả trước' where AccountNumber = 242
Update Account set AccountName = N'Thuế bảo vệ môi trường và các thuế khác' where AccountNumber = 3338
Update Account set AccountName = N'Nhận ký quỹ, ký cược' where AccountNumber = 3386
Update Account set AccountName = N'Vay và nợ thuê tài chính' where AccountNumber = 341
Update Account set AccountName = N'Các khoản vay' where AccountNumber = 3411
Update Account set AccountName = N'Nợ thuê tài chính' where AccountNumber = 3412
Update Account set AccountName = N'Vốn đầu tư của chủ sở hữu' where AccountNumber = 411
Update Account set AccountName = N'Vốn góp của chủ sở hữu' where AccountNumber = 4111
Update Account set AccountName = N'Lợi nhuận sau thuế chưa phân phối' where AccountNumber = 421
Update Account set AccountName = N'Lợi nhuận sau thuế chưa phân phối năm trước' where AccountNumber = 4211
Update Account set AccountName = N'Xây dựng cơ bản' where AccountNumber = 2412
Update Account set AccountName = N'Các khoản đi vay	' where AccountNumber = 3411
Update Account set AccountName = N'Lợi nhuận sau thuế chưa phân phối năm nay' where AccountNumber = 4212
Update Account set AccountGroupKind = 0
GO

Delete Account where AccountNumber in (1591,1592,1593,34131,34132,34133,5211,5212,5213,1113,1123,142,159,171,244,311,315,3389,3413,3414,351,521,221,2212,2213,2218)
UPDATE AccountDefault SET FilterAccount = '' WHERE TypeID = 430
GO

INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'E6AF3466-40AE-4FFF-BD87-4D2250D97595', N'fade7842-1400-4ac7-90e8-370c0deb1b14', N'OriginalPrice', N'Nguyên giá', NULL, 0, 0, 100, 0, 0, 1, 19)
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'147E2B27-3D41-4D4D-BC17-3621400418A7', N'fade7842-1400-4ac7-90e8-370c0deb1b14', N'PurchasePrice', N'Giá trị tính khấu hao', NULL, 0, 0, 100, 0, 0, 1, 20)
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'5AFB4A56-AA4C-4F3E-A865-0F011328CB36', N'fade7842-1400-4ac7-90e8-370c0deb1b14', N'AcDepreciationAmount', N'Hao mòn lũy kế', NULL, 0, 0, 100, 0, 0, 1, 21)
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'9202A4DD-A07D-4098-B0C8-7D0C3939DD8D', N'fade7842-1400-4ac7-90e8-370c0deb1b14', N'RemainingAmount', N'Giá trị còn lại', NULL, 0, 0, 100, 0, 0, 1, 22)
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'73C7C4ED-0E52-46DF-9A2D-9A302D34A3A2', N'fade7842-1400-4ac7-90e8-370c0deb1b14', N'OriginalPriceAccount', N'TK nguyên giá', N'Tài khoản nguyên giá', 0, 0, 100, 0, 0, 1, 23)
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'9DAE84AB-912C-4C79-88CF-D1EDD79DE737', N'fade7842-1400-4ac7-90e8-370c0deb1b14', N'DepreciationAccount', N'TK hao mòn', N'Tài khoản hao mòn', 0, 0, 100, 0, 0, 1, 24)
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'8E044534-42B5-4898-8549-27999572E224', N'fade7842-1400-4ac7-90e8-370c0deb1b14', N'ExpenditureAccount', N'TK chi phí', N'Tài khoản chi phí', 0, 0, 100, 0, 0, 1, 25)
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'6446C2DE-13AA-4912-A97C-E6BD86B5E133', N'fade7842-1400-4ac7-90e8-370c0deb1b14', N'HandlingResidualValueAccount', N'TK xử lý giá trị còn lại', N'Tài khoản xử lý giá trị còn lại', 0, 0, 100, 0, 0, 1, 26)
GO

INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'1ED5F7A2-B544-4B4E-8CBF-E465DD05373A', N'bb4aa193-7056-4583-b858-9f56e3b95ae1', N'FixedAssetCategoryName', N'Loại tài sản', N'', 1, 0, 250, 0, 0, 1, 3);
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'39C5157E-27A6-4D97-AE10-F9BE60906CD2', N'bb4aa193-7056-4583-b858-9f56e3b95ae1', N'Quantity', N'Số lượng', N'', 1, 0, 100, 0, 0, 1, 4);
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'0FE6D7DA-47F3-4681-829C-CA002DC2529F', N'bb4aa193-7056-4583-b858-9f56e3b95ae1', N'MonthlyDepreciationAmountOriginal', N'Giá trị khấu hao tháng', NULL, 1, 0, 250, 0, 0, 1, 6);
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'8AA21C84-4677-4078-AE0F-8394E4976E6E', N'bb4aa193-7056-4583-b858-9f56e3b95ae1', N'AllocationAmount', N'Số tiền PB', N'Số tiền phân bổ', 1, 0, 250, 0, 0, 1, 8);
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'8B5322F9-F2A4-494F-8376-F6C62AD84012', N'bb4aa193-7056-4583-b858-9f56e3b95ae1', N'ContractID', N'Hợp đồng', N'', 1, 0, 120, 0, 0, 1, 13);
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES (N'237FBD9E-94AF-4B29-AC67-849BDB382F56', N'bb4aa193-7056-4583-b858-9f56e3b95ae1', N'StatisticsCodeID', N'Mã thống kê', N'', 1, 0, 120, 0, 0, 1, 14);
GO

INSERT [dbo].[TemplateDetail] ([ID], [TemplateID], [GridType], [TabIndex], [TabCaption]) VALUES (N'A0E45A11-86A4-4C66-89DE-90957A60C4AE', N'e1adad03-54cb-419c-8cd0-69c70198d793', 1, 1, N'Hạch toán');
UPDATE "dbo"."TemplateDetail" SET "TabCaption"=N'&1. Khấu hao, phân bổ ' WHERE  "ID"='{BB4AA193-7056-4583-B858-9F56E3B95AE1}';
GO

INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('1A7C02B9-6B77-4097-ADED-AF56B96DDEE7', 'A0E45A11-86A4-4C66-89DE-90957A60C4AE', N'Reason', N'Ghi chú', N'', '1', '0', 120, 0, 0, '1', 0);
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('1C14B9DC-86B7-416C-BFF7-ED8A914DC9FD', 'A0E45A11-86A4-4C66-89DE-90957A60C4AE', N'DepartmentID', N'Phòng ban', N'', '1', '0', 120, 0, 0, '1', 1);
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('EA73815F-93D4-40F1-A003-EDF9A9D2DD81', 'A0E45A11-86A4-4C66-89DE-90957A60C4AE', N'DebitAccount', N'TK nợ', N'', '1', '0', 120, 0, 0, '1', 2);
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('C0A8C5CA-5B7F-4445-AC0B-4B0889D3EA8F', 'A0E45A11-86A4-4C66-89DE-90957A60C4AE', N'CreditAccount', N'TK có', N'', '1', '0', 120, 0, 0, '1', 3);
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('5060B34F-7E49-4192-A36A-19715F77F346', 'A0E45A11-86A4-4C66-89DE-90957A60C4AE', N'TotalOrgPrice', N'Số tiền', N'', '1', '0', 120, 0, 0, '1', 4);
INSERT [dbo].[TemplateColumn] ([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('FEDDC58C-AAFE-4949-A535-8C7D75EF05F1', 'A0E45A11-86A4-4C66-89DE-90957A60C4AE', N'TotalOrgPriceOriginal', N'Số tiền', N'', '1', '0', 120, 0, 0, '0', 5);
GO

INSERT  [dbo].[TypeGroup]([ID], [TypeGroupName], [DebitAccountID], [CreditAccountID]) VALUES (56, N'Kiểm kê TSCĐ', NULL, NULL);
INSERT  [dbo].[GenCode]([ID], [BranchID], [TypeGroupID], [TypeGroupName], [Prefix], [Length], [Suffix], [CurrentValue], [IsSecurity]) VALUES ('F4EAB988-3EBA-4273-BE51-3BFD28B1A1D4', NULL, 56, N'Kiểm kê TSCĐ', N'KKTSCD', 6, N'', 9, '1');
GO

SET IDENTITY_INSERT [dbo].[Type] ON
INSERT  [dbo].[Type]([ID], [TypeName], [TypeGroupID], [Recordable], [Searchable], [PostType], [OrderPriority]) VALUES (560, N'Kiểm kê TSCĐ', 56, '1', '1', 1, 76);
SET IDENTITY_INSERT [dbo].[Type] OFF
GO

SET IDENTITY_INSERT [dbo].[Template] ON
INSERT  [dbo].[Template]([ID], [TemplateName], [TypeID], [IsDefault], [IsActive], [IsSecurity], [OrderPriority]) VALUES ('5BD2564F-6F4A-4A4A-B2DB-017A5CFD4EB9', N'Mẫu ngầm định', 560, '1', '0', '1', 1);
INSERT  [dbo].[Template]([ID], [TemplateName], [TypeID], [IsDefault], [IsActive], [IsSecurity], [OrderPriority]) VALUES ('790850A0-C45B-47DF-8F64-83CD6914C7EA', N'Mẫu chuẩn', 560, '0', '1', '1', 1);
SET IDENTITY_INSERT [dbo].[Template] OFF
GO

INSERT  [dbo].[TemplateDetail]([ID], [TemplateID], [GridType], [TabIndex], [TabCaption]) VALUES ('A5A3BF5C-5075-4433-A43F-1BAC98233B27', '5BD2564F-6F4A-4A4A-B2DB-017A5CFD4EB9', 1, 0, N'&1. Kiểm kê');
INSERT  [dbo].[TemplateDetail]([ID], [TemplateID], [GridType], [TabIndex], [TabCaption]) VALUES ('A284B7FD-2189-47EE-A2CA-3E6A25D0BB6E', '5BD2564F-6F4A-4A4A-B2DB-017A5CFD4EB9', 1, 1, N'&2. Thành viên tham gia');
INSERT  [dbo].[TemplateDetail]([ID], [TemplateID], [GridType], [TabIndex], [TabCaption]) VALUES ('3B1F6277-9F0C-4FE2-89F8-7FC686D9BD97', '790850A0-C45B-47DF-8F64-83CD6914C7EA', 1, 0, N'&1. Kiểm kê');
INSERT  [dbo].[TemplateDetail]([ID], [TemplateID], [GridType], [TabIndex], [TabCaption]) VALUES ('93A2D382-EF37-4A7F-89B5-927C0F821149', '790850A0-C45B-47DF-8F64-83CD6914C7EA', 1, 1, N'&2. Thành viên tham gia');
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('1D89FA26-DD43-4D38-B86B-00CF6A8B31BD', '93A2D382-EF37-4A7F-89B5-927C0F821149', N'AccountingObjectID', N'Mã nhân viên', N'', '0', '0', 120, 0, 0, '1', 0);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('BA736E22-3961-4330-99E8-153EC7B5C1BE', '3B1F6277-9F0C-4FE2-89F8-7FC686D9BD97', N'OriginalPrice', N'Nguyên giá', N'', '0', '0', 150, 0, 0, '1', 3);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('F67601FD-F223-4A66-9690-301E86478860', 'A284B7FD-2189-47EE-A2CA-3E6A25D0BB6E', N'DepartmentID', N'Phòng ban', N'', '0', '0', 120, 0, 0, '1', 2);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('B740AA6D-7101-40DF-9D0E-31C92375FE67', '3B1F6277-9F0C-4FE2-89F8-7FC686D9BD97', N'DepreciationAmount', N'Giá trị tính khấu hao', N'', '0', '0', 150, 0, 0, '1', 4);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('65348DEF-B227-489B-8ACF-34974BF9615F', 'A284B7FD-2189-47EE-A2CA-3E6A25D0BB6E', N'AccountingObjectTitle', N'Chức vụ', N'', '0', '0', 120, 0, 0, '1', 1);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('5CDB2A92-864B-4DE6-979B-365C4765ACE6', 'A5A3BF5C-5075-4433-A43F-1BAC98233B27', N'FixedAssetName', N'Tên tài sản', N'', '0', '0', 250, 0, 0, '1', 1);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('9F68FB1F-654F-41C9-9A0B-41492F9AC702', 'A5A3BF5C-5075-4433-A43F-1BAC98233B27', N'ExistInStock', N'KQ kiểm kê', N'Kết quả kiểm kê', '0', '0', 100, 0, 0, '1', 7);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('11EE1C0A-BFDA-492D-9061-48F7396D9A47', 'A5A3BF5C-5075-4433-A43F-1BAC98233B27', N'OriginalPrice', N'Nguyên giá', N'', '0', '0', 150, 0, 0, '1', 3);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('AF254AFF-785D-44C3-BD6A-49D39563B1A6', '3B1F6277-9F0C-4FE2-89F8-7FC686D9BD97', N'AcDepreciationAmount', N'Hao mòn lũy kế', NULL, '0', '0', 150, 0, 0, '1', 5);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('C14E5CA5-F2C8-4085-9F43-507BB27868EE', '3B1F6277-9F0C-4FE2-89F8-7FC686D9BD97', N'FixedAssetID', N'Mã tài sản', N'', '0', '0', 120, 0, 0, '1', 0);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('2D2BE932-DB7C-4A11-9AF3-55305556CD7D', '3B1F6277-9F0C-4FE2-89F8-7FC686D9BD97', N'RemainingAmount', N'Giá trị còn lại', N'Giá trị còn lại', '0', '0', 150, 0, 0, '1', 6);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('349FBCDF-8CAA-4A54-8219-70D1DCCB0F2D', '3B1F6277-9F0C-4FE2-89F8-7FC686D9BD97', N'FixedAssetName', N'Tên tài sản', N'', '0', '0', 250, 0, 0, '1', 1);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('F784E4C3-C1CD-40EE-B3B5-786B090BA7AF', 'A5A3BF5C-5075-4433-A43F-1BAC98233B27', N'AcDepreciationAmount', N'Hao mòn lũy kế', NULL, '0', '0', 150, 0, 0, '1', 5);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('58F47DA1-F140-43BD-ABF1-7918E546B027', 'A284B7FD-2189-47EE-A2CA-3E6A25D0BB6E', N'AccountingObjectName', N'Họ tên', N'', '0', '0', 120, 0, 0, '1', 0);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('3326BFF5-C4F4-4F84-A833-8F2F0134470F', '93A2D382-EF37-4A7F-89B5-927C0F821149', N'AccountingObjectTitle', N'Chức vụ', N'', '0', '0', 120, 0, 0, '1', 2);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('44E049CF-43CB-4A3B-9E37-93D1B9D76A0A', '3B1F6277-9F0C-4FE2-89F8-7FC686D9BD97', N'DepartmentID', N'Phòng ban', N'', '0', '0', 250, 0, 0, '1', 2);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('51C11317-406D-40CF-B354-9581DE746951', 'A5A3BF5C-5075-4433-A43F-1BAC98233B27', N'DepartmentID', N'Phòng ban', N'', '0', '0', 250, 0, 0, '1', 2);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('44895B4B-4C17-4C8A-97DC-A0B0FF550E4A', '3B1F6277-9F0C-4FE2-89F8-7FC686D9BD97', N'Recommendation', N'Hình thức xử lý', N'', '0', '0', 100, 0, 0, '1', 8);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('F309936D-DE10-4B50-966E-B0657EF1E6CD', 'A5A3BF5C-5075-4433-A43F-1BAC98233B27', N'Recommendation', N'Hình thức xử lý', N'', '0', '0', 100, 0, 0, '1', 8);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('EED6510F-AFAC-416C-BF96-BFC084171408', '93A2D382-EF37-4A7F-89B5-927C0F821149', N'DepartmentID', N'Phòng ban', N'', '0', '0', 120, 0, 0, '1', 3);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('FC409AAD-539B-48FE-840C-CDC67C890EC1', '3B1F6277-9F0C-4FE2-89F8-7FC686D9BD97', N'Note', N'Ghi chú', N'', '0', '0', 250, 0, 0, '1', 9);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('FF7C6EEA-0094-4C01-AA93-D76EACCF3D7E', '3B1F6277-9F0C-4FE2-89F8-7FC686D9BD97', N'ExistInStock', N'KQ kiểm kê', N'Kết quả kiểm kê', '0', '0', 100, 0, 0, '1', 7);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('C0EBC3D2-8934-4E01-85BD-EF4C7E8F0B73', 'A5A3BF5C-5075-4433-A43F-1BAC98233B27', N'Note', N'Ghi chú', N'', '0', '0', 250, 0, 0, '1', 9);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('07B920FA-F0C2-4375-A664-F081A1704AFC', '93A2D382-EF37-4A7F-89B5-927C0F821149', N'AccountingObjectName', N'Họ tên', N'', '0', '0', 120, 0, 0, '1', 1);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('B292BABC-CF44-4544-9A6E-F31CFFA01A4D', 'A5A3BF5C-5075-4433-A43F-1BAC98233B27', N'FixedAssetID', N'Mã tài sản', N'', '0', '0', 120, 0, 0, '1', 0);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('C028D89F-3D82-4597-8003-F8577695BCCF', 'A5A3BF5C-5075-4433-A43F-1BAC98233B27', N'RemainingAmount', N'Giá trị còn lại', N'Giá trị còn lại', '0', '0', 150, 0, 0, '1', 6);
INSERT  [dbo].[TemplateColumn]([ID], [TemplateDetailID], [ColumnName], [ColumnCaption], [ColumnToolTip], [IsColumnHeader], [IsReadOnly], [ColumnWidth], [ColumnMaxWidth], [ColumnMinWidth], [IsVisible], [VisiblePosition]) VALUES ('3BC82B78-9F71-4CBC-874F-F8C25393DD77', 'A5A3BF5C-5075-4433-A43F-1BAC98233B27', N'DepreciationAmount', N'Giá trị tính khấu hao', N'', '0', '0', 150, 0, 0, '1', 4);
GO

update TemplateColumn set IsVisible = 0 where id = '39C5157E-27A6-4D97-AE10-F9BE60906CD2'
update TemplateColumn set IsVisible = 0 where id in ('E4F2F692-CE37-4D21-B5AF-45F2AC082BF7', '43736D24-96B5-40EB-B207-29558B3822D2', 'AD6793B4-C7F1-4D65-8554-731CA702B829');
update TemplateColumn set VisiblePosition= '5' where ID='BE85BF54-EE9B-469B-9B10-5AE99DCF7184'
update TemplateColumn set VisiblePosition= '6' where ID='60A0A73E-690E-4DB1-9CDF-AEE29456478F'
GO

update AccountDefault set FilterAccount = '154;241;631;642;632;811'  where id ='9E6425C4-D961-439A-8FC0-C926C0B472CA'
update TemplateColumn set IsVisible= '0' where ID='2B7A0611-4AF0-44A8-B108-A55DBE472991'
update TemplateColumn set VisiblePosition= '0' where ID='62288350-8DBA-42AE-93FA-E334C90C53E5'
update TemplateColumn set IsVisible= '0' where ID='62288350-8DBA-42AE-93FA-E334C90C53E5'
GO

delete dbo.TemplateColumn where ID in ('7FA7A4D0-8263-4DC3-AFBD-B6FF849F3323','85B38F23-8EFF-4690-9FFA-5DA44FB111D6','7473474F-F748-43B7-B3CD-0C4CDBB3F3C8','99DC69AA-065F-47D0-B7F4-938FCC0991EB','DF1096CD-5CB0-4C11-97FC-8E2C46CDD3D7','4B3DC771-FEEE-4559-B9B8-E2CBAA8E9A8E','DAC42055-43BF-4C9C-9F75-01C9C56ED387','20519428-CAD7-406C-B38C-778D6A1426E8','C8FC451A-E477-4A6F-BF4B-99B61CE26119','34323A18-72BE-4FF0-BEA9-E4526688DB81','22B45398-E2C2-4061-BCC2-00EDEE85F032','0357636E-B341-4B7D-B7AD-D708B7B53033')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES  */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
DELETE FROM "Account";
/*!40000 ALTER TABLE "Account" DISABLE KEYS */;
INSERT INTO "Account" ("ID", "AccountNumber", "AccountName", "AccountNameGlobal", "Description", "ParentID", "IsParentNode", "Grade", "AccountGroupID", "AccountGroupKind", "DetailType", "IsActive") VALUES
	(N'{115D21DF-AFFC-44F3-983D-0154D0184960}', N'811', N'Chi phí khác', N'Other expenses', N'', NULL, N'False', 1, N'811', 3, N'-1', N'True'),
	(N'{6336E36B-D8EF-4F60-9851-02F73A7F6843}', N'635', N'Chi phí tài chính', N'Financial activities expenses', N'', NULL, N'False', 1, N'635', 3, N'-1', N'True'),
	(N'{73AFCA6E-92B4-4D86-B43B-0321083FB0AD}', N'21131', N'Quyền sử dụng đất', N'Land using right', N'', N'{168820BE-7173-476E-BAB3-FF4CAA7EFD1D}', N'False', 3, N'211', 0, N'-1', N'False'),
	(N'{293410A6-A1D8-4B8F-8A11-039BA7B296D7}', N'2147', N'Hao mòn bất động sản đầu tư', N'Investment real estate depreciation', N'', N'{7261A5DA-CEC1-457C-BBE9-20F1BCA122D9}', N'False', 2, N'214', 1, N'-1', N'True'),
	(N'{20EFBC4C-D102-48A2-A8DD-063137BD43E6}', N'007', N'Ngoại tệ các loại', N'Foreign currencies', N'', NULL, N'False', 1, N'007', 0, N'8', N'False'),
	(N'{61163644-3894-4257-9A21-0940EDDD836F}', N'911', N'Xác định kết quả kinh doanh', N'Evaluation of business results', N'', NULL, N'False', 1, N'911', 3, N'-1', N'True'),
	(N'{FD3F6E50-C1D7-40A7-868F-09FF29086205}', N'2413', N'Sửa chữa lớn TSCĐ', N'Major repair of fixed assets', NULL, N'{A724E9E7-BCDC-4E83-96D1-12A4EE7DE3F7}', N'False', 2, N'241', 0, N'-1', N'True'),
	(N'{6BA57631-C48F-43D1-8663-0CA651358974}', N'4111', N'Vốn góp của chủ sở hữu', N'Contributed legal capital', N'', N'{A81EAD52-1D99-471E-9D24-E1228A056878}', N'False', 2, N'411', 1, N'-1', N'True'),
	(N'{33600939-6890-4946-9E3F-0D51A304A77C}', N'611', N'Mua hàng', N'Purchase', N'', NULL, N'False', 1, N'611', 3, N'-1', N'True'),
	(N'{A724E9E7-BCDC-4E83-96D1-12A4EE7DE3F7}', N'241', N'Xây dựng cơ bản dở dang', N'Construction in process', NULL, NULL, N'True', 1, N'241', 0, N'-1', N'True'),
	(N'{8B908B63-011E-49CD-9460-13708BA63717}', N'152', N'Nguyên liệu, vật liệu', N'Raw materials', N'', NULL, N'False', 1, N'152', 0, N'5', N'True'),
	(N'{072A239A-21AC-45BB-B0D9-19B61CE777A8}', N'2288', N'Đầu tư khác', NULL, NULL, N'{04CFEB78-C9D3-47E4-94E1-461E00E98926}', N'False', 2, N'228', 0, N'-1', N'True'),
	(N'{80FE6D05-83D0-432D-806F-1C0655AA4919}', N'1112', N'Ngoại tệ', N'Foreign currency', NULL, N'{CFA92323-A8B2-4355-B0A3-E868F54E3AED}', N'False', 2, N'111', 0, N'8', N'True'),
	(N'{90AF297C-B2F5-47A8-9B5C-1C5B94752263}', N'21136', N'Giấy phép và giấy chuyển nhượng quyền', N'License and right concession permits', N'', N'{168820BE-7173-476E-BAB3-FF4CAA7EFD1D}', N'False', 3, N'211', 0, N'-1', N'False'),
	(N'{7061C24B-3931-4EA3-99D8-1D87DAFE6217}', N'155', N'Thành phẩm', N'Finished goods', N'', NULL, N'False', 1, N'155', 0, N'5', N'True'),
	(N'{9AFA59F4-C269-4FC8-8623-1F2137D70BF5}', N'157', N'Hàng gửi đi bán', N'Goods on consignment', N'', NULL, N'False', 1, N'157', 0, N'5', N'True'),
	(N'{8EED1E1F-FAB1-45E5-8750-1F7DE5B8B2F7}', N'341', N'Vay và nợ thuê tài chính', N'Long-term borrowing and liabilities', N'', NULL, N'False', 1, N'341', 1, N'-1', N'True'),
	(N'{76A9DF4A-9452-46EC-AC98-1F983292DE8F}', N'3522', N'Dự phòng bảo hành công trình xây dựng', N'', N'', N'{05D067F4-5CD2-49A1-9C5F-70E03D927CF4}', N'False', 2, N'352', 1, N'-1', N'True'),
	(N'{7261A5DA-CEC1-457C-BBE9-20F1BCA122D9}', N'214', N'Hao mòn tài sản cố định', N'Depreciation of fixed assets', N'', NULL, N'True', 1, N'214', 1, N'-1', N'True'),
	(N'{61B686E2-A5A3-4899-A586-21082BCD8687}', N'3524', N'Dự phòng phải trả khác', N'', N'', N'{05D067F4-5CD2-49A1-9C5F-70E03D927CF4}', N'False', 2, N'352', 1, N'-1', N'True'),
	(N'{76011DD3-7B97-4279-861E-24322EBBF0F1}', N'511', N'Doanh thu bán hàng và cung cấp dịch vụ', N'Sales', N'', NULL, N'True', 1, N'511', 3, N'-1', N'True'),
	(N'{162A30B8-B517-4CF8-9C43-24A64D0925C8}', N'1361', N'Vốn kinh doanh ở đơn vị trực thuộc', NULL, NULL, N'{747727CD-3D84-49E6-BEFD-2A549A62FF65}', N'False', 2, N'136', 0, N'-1', N'True'),
	(N'{F96F1DF3-C26C-4AE0-BF88-274EB95A87CA}', N'3383', N'Bảo hiểm xã hội', N'Social insurance', N'', N'{8CB3FEF4-5EE4-413C-BD1A-A19D0D668301}', N'False', 2, N'338', 2, N'2', N'True'),
	(N'{0455B75F-1B2C-4AF5-B842-2A006A8F0479}', N'3333', N'Thuế xuất, nhập khẩu', N'Import & export duties', N'', N'{4D47EBE7-A76E-4C82-890E-E5CE5F5437C3}', N'False', 2, N'333', 2, N'-1', N'True'),
	(N'{747727CD-3D84-49E6-BEFD-2A549A62FF65}', N'136', N'Phải thu nội bộ', NULL, NULL, NULL, N'False', 1, N'136', 0, N'-1', N'True'),
	(N'{52464609-8146-462D-A98B-2F66CB7EE5E7}', N'2411', N'Mua sắm TSCĐ', N'Fixed assets purchases', NULL, N'{A724E9E7-BCDC-4E83-96D1-12A4EE7DE3F7}', N'False', 2, N'241', 0, N'-1', N'True'),
	(N'{399F97CE-8CA2-46C2-9069-2FE7A0D854CC}', N'128', N'Đầu tư nắm giữ đến ngày đáo hạn', NULL, NULL, NULL, N'False', 1, N'128', 0, N'-1', N'True'),
	(N'{8A26EE37-AC3D-475B-907C-31084A5EAD77}', N'4118', N'Vốn khác', N'Other capital', N'', N'{A81EAD52-1D99-471E-9D24-E1228A056878}', N'False', 2, N'411', 1, N'-1', N'True'),
	(N'{7583753F-0793-4852-B913-33D7AB80102F}', N'3337', N'Thuế nhà đất, tiền thuê đất', N'Land & housing tax, land rental charges', N'', N'{4D47EBE7-A76E-4C82-890E-E5CE5F5437C3}', N'False', 2, N'333', 2, N'-1', N'True'),
	(N'{A16EBA02-1B5E-469F-9DF4-347AC7F152F3}', N'004', N'Nợ khó đòi đã xử lý', N'Bad debt written off', N'', NULL, N'False', 1, N'004', 0, N'-1', N'False'),
	(N'{A89575BD-6224-4298-B3F1-363E4A7AE481}', N'21133', N'Bản quyền, bằng sáng chế', N'Copyright, patents', N'', N'{168820BE-7173-476E-BAB3-FF4CAA7EFD1D}', N'False', 3, N'211', 0, N'-1', N'False'),
	(N'{B9834976-0B39-4FF8-BF0C-38E065753F0B}', N'2291', N'Dự phòng giảm giá chứng khoán kinh doanh', NULL, NULL, N'{580759E3-3322-4058-9E6D-D303DF12F9F3}', N'False', 2, N'229', 0, N'-1', N'True'),
	(N'{823D84E9-F096-42A6-A676-3964EE2BC408}', N'21113', N'Phương tiện vận tải, truyền dẫn', N'Means of transport, conveyance equipment', N'', N'{82EE395E-3A7C-41F8-A6BF-67F1710B5D83}', N'False', 3, N'211', 0, N'-1', N'False'),
	(N'{59DB7A86-FF15-467F-AE02-396827874D7E}', N'5112', N'Doanh thu bán các thành phẩm', N'Finished product sale', N'', N'{76011DD3-7B97-4279-861E-24322EBBF0F1}', N'False', 2, N'511', 3, N'-1', N'True'),
	(N'{AE7FCBBB-B811-4A54-9C0D-3B5292FDDE26}', N'33311', N'Thuế GTGT đầu ra', N'VAT output', NULL, N'{A278B707-C226-42F0-A46C-DCF9C351F35A}', N'False', 3, N'333', 0, N'-1', N'True'),
	(N'{6E5040DE-459E-4504-B45C-3CB4AA8A7A9B}', N'002', N'Vật tư, hàng hóa nhận giữ hộ, nhận gia công', N'Goods held under trust or for processing', N'', NULL, N'False', 1, N'002', 0, N'-1', N'False'),
	(N'{4A0760B1-FDFA-4439-B0EE-3F6D5FDABC42}', N'336', N'Phải trả nội bộ', N'', N'', NULL, N'False', 1, N'336', 1, N'-1', N'True'),
	(N'{F46B94F7-E0C5-46AD-9089-42FC0634DDBA}', N'1332', N'Thuế GTGT được khấu trừ của TSCĐ', N'VAT deduction of fixed assets', NULL, N'{E307B2C8-AAD7-41FC-9B01-AF758F52F740}', N'False', 2, N'133', 0, N'-1', N'True'),
	(N'{39D57678-9AC6-45B7-82F0-43AD5D7D2FCF}', N'1331', N'Thuế GTGT được khấu trừ của hàng hóa, dịch vụ', N'VAT deduction of goods, services', N'', N'{E307B2C8-AAD7-41FC-9B01-AF758F52F740}', N'False', 2, N'133', 0, N'-1', N'True'),
	(N'{00E0ACFE-02B2-476B-B99E-457065523383}', N'131', N'Phải thu của khách hàng', N'Accounts receivable - trade', N'', NULL, N'False', 1, N'131', 2, N'1', N'True'),
	(N'{04CFEB78-C9D3-47E4-94E1-461E00E98926}', N'228', N'Đầu tư góp vốn vào đơn vị khác', NULL, NULL, NULL, N'False', 1, N'228', 0, N'-1', N'True'),
	(N'{FFA2144A-CA74-4880-9F2E-4AFEF0F9609C}', N'2292', N'Dự phòng tổn thất đầu tư vào đơn vị khác', NULL, NULL, N'{580759E3-3322-4058-9E6D-D303DF12F9F3}', N'False', 2, N'229', 0, N'-1', N'True'),
	(N'{8B30B5F3-B6D8-41B2-8691-512316278FFF}', N'3385', N'Bảo hiểm thất nghiệp', N'', N'', N'{8CB3FEF4-5EE4-413C-BD1A-A19D0D668301}', N'False', 2, N'338', 2, N'-1', N'True'),
	(N'{BF1B8906-EAE4-48AA-8B3F-55A8051635EF}', N'21132', N'Quyền phát hành', N'Distribution rights', N'', N'{168820BE-7173-476E-BAB3-FF4CAA7EFD1D}', N'False', 3, N'211', 0, N'-1', N'False'),
	(N'{999EFCE1-BA4F-4EBE-83C0-561C2DC32D8F}', N'1111', N'Tiền Việt Nam', N'Vietnam dong', N'', N'{CFA92323-A8B2-4355-B0A3-E868F54E3AED}', N'False', 2, N'111', 0, N'-1', N'True'),
	(N'{70440878-3FBB-4034-83F0-5BE6369A8E16}', N'356', N'Quỹ phát triển khoa học và công nghệ', N'Development of science and technology fund', N'', NULL, N'True', 1, N'356', 1, N'-1', N'True'),
	(N'{C87C8AD9-5BFE-4056-B52B-5DA82BBD3708}', N'418', N'Các quỹ thuộc vốn chủ sở hữu', N'Other funds', N'', NULL, N'False', 1, N'418', 1, N'-1', N'True'),
	(N'{82EE395E-3A7C-41F8-A6BF-67F1710B5D83}', N'2111', N'TSCĐ hữu hình', N'Tangible fixed assets', NULL, N'{703532AA-D5E3-4EB7-8490-8816B5A32E5A}', N'False', 2, N'211', 0, N'-1', N'True'),
	(N'{13431AAC-C5F0-4EFC-A05B-68533F647576}', N'154', N'Chi phí sản xuất, kinh doanh dở dang', N'Work in progress', NULL, NULL, N'False', 1, N'154', 0, N'-1', N'True'),
	(N'{8A56ABC3-274C-46FE-A8C7-68E074270C98}', N'711', N'Thu nhập khác', N'Other income', N'', NULL, N'False', 1, N'711', 3, N'-1', N'True'),
	(N'{2A570657-3768-4B8D-B575-6A95EA96A80A}', N'1281', N'Tiền gửi có kỳ hạn', NULL, NULL, N'{399F97CE-8CA2-46C2-9069-2FE7A0D854CC}', N'False', 2, N'128', 0, N'-1', N'True'),
	(N'{F2673BFE-F9A9-4E69-A92E-6BF9390027F6}', N'1288', N'Các khoản đầu tư khác nắm giữ đến ngày đáo hạn', NULL, NULL, N'{399F97CE-8CA2-46C2-9069-2FE7A0D854CC}', N'False', 2, N'128', 0, N'-1', N'True'),
	(N'{6761628C-F99F-4234-995A-6C492C79E9F6}', N'3532', N'Quỹ phúc lợi', N'Welfare fund', N'', N'{81369FA4-A45C-4020-920F-B8698A042698}', N'False', 2, N'353', 1, N'-1', N'True'),
	(N'{733D8864-5735-4673-8159-6DB85ABAB6A9}', N'4212', N'Lợi nhuận sau thuế chưa phân phối năm nay', N'This year undistributed earnings', N'', N'{181852E3-ADB3-4222-A22B-BE5132859A3D}', N'False', 2, N'421', 2, N'-1', N'True'),
	(N'{B9B17B43-02F6-4EAB-955E-6F3861230223}', N'33382', N'Các loại thuế khác', N'', N'', N'{2C2F861C-D50D-4B10-929D-FDCF63E0C2B4}', N'False', 3, N'333', 2, N'-1', N'True'),
	(N'{50795A48-F27C-437A-A8A9-70B853F2FE47}', N'003', N'Hàng hóa nhận bán hộ, nhận ký gửi, ký cược', N'Goods received on consignment for sale, deposit', N'', NULL, N'False', 1, N'003', 0, N'-1', N'False'),
	(N'{05D067F4-5CD2-49A1-9C5F-70E03D927CF4}', N'352', N'Dự phòng phải trả', N'Provisions for payables', N'', NULL, N'False', 1, N'352', 1, N'-1', N'True'),
	(N'{C005C550-2D48-4F30-94E6-71DB25FE2D82}', N'21134', N'Nhãn hiệu hàng hóa', N'Trademark', NULL, N'{168820BE-7173-476E-BAB3-FF4CAA7EFD1D}', N'False', 3, N'211', 0, N'-1', N'False'),
	(N'{840C966B-9521-4626-B079-7212429E62A6}', N'001', N'Tài sản thuê ngoài', N'Operating lease assets', N'Tài sản thuê ngoài', NULL, N'False', 1, N'001', 0, N'-1', N'False'),
	(N'{A0F55128-0A61-4101-AAA8-72CC4CFFC624}', N'2281', N'Đầu tư vào công ty liên doanh, liên kết', NULL, NULL, N'{04CFEB78-C9D3-47E4-94E1-461E00E98926}', N'False', 2, N'228', 0, N'-1', N'True'),
	(N'{D0A7BCA6-F58D-4B79-BBBB-73DD6517E704}', N'3534', N'Quỹ thưởng ban quản lý điều hành công ty', N'Reward fund for management and operating company', N'', N'{81369FA4-A45C-4020-920F-B8698A042698}', N'False', 2, N'353', 1, N'-1', N'True'),
	(N'{4955AFA1-C87E-4F8F-ACEF-7666946E652C}', N'632', N'Giá vốn hàng bán', N'Cost of goods sold', N'', NULL, N'False', 1, N'632', 3, N'-1', N'True'),
	(N'{0F464EC8-858B-4802-9097-7D8ED6E02565}', N'138', N'Phải thu khác', N'Other receivable', N'', NULL, N'True', 1, N'138', 2, N'-1', N'True'),
	(N'{3B29167F-C10E-427E-BEBD-84095C1E3567}', N'3339', N'Phí, lệ phí và các khoản phải nộp khác', N'Fee & charge & other payables', N'', N'{4D47EBE7-A76E-4C82-890E-E5CE5F5437C3}', N'False', 2, N'333', 2, N'-1', N'True'),
	(N'{A9024E2B-8C31-439A-8C73-864FB148A2BF}', N'821', N'Chi phí thuế thu nhập doanh nghiệp', N'Business Income tax charge', N'', NULL, N'False', 1, N'821', 3, N'-1', N'True'),
	(N'{FD349ED4-8176-460D-9BEB-877C2AED92CA}', N'1122', N'Ngoại tệ', N'Foreign currency', NULL, N'{D303C286-C4A6-496D-AD6F-9899E23050EA}', N'False', 2, N'112', 0, N'8;6', N'True'),
	(N'{703532AA-D5E3-4EB7-8490-8816B5A32E5A}', N'211', N'Tài sản cố định ', N'Fixed assets', NULL, NULL, N'True', 1, N'211', 0, N'-1', N'True'),
	(N'{42760517-E110-45C8-886B-89D9B7F9FA02}', N'2293', N'Dự phòng phải thu khó đòi', NULL, NULL, N'{580759E3-3322-4058-9E6D-D303DF12F9F3}', N'False', 2, N'229', 0, N'-1', N'True'),
	(N'{E971C7A2-ACBF-49FE-BA48-8C12CC072960}', N'3562', N'Quỹ phát triển khoa học và công nghệ đã hình thành TSCĐ', N'Development of science and technology fund used to fixed assets', N'', N'{70440878-3FBB-4034-83F0-5BE6369A8E16}', N'False', 2, N'356', 1, N'-1', N'True'),
	(N'{DF6C256F-3D79-42C3-9658-8CEE022BDBEB}', N'1388', N'Phải thu khác', N'Other receivable', N'', N'{0F464EC8-858B-4802-9097-7D8ED6E02565}', N'False', 2, N'138', 2, N'1', N'True'),
	(N'{DA5E6C9C-38CE-4012-93A9-8ECC90702EC2}', N'2112', N'TSCĐ thuê tài chính', N'Financial leasing fixed assets', NULL, N'{703532AA-D5E3-4EB7-8490-8816B5A32E5A}', N'False', 2, N'211', 0, N'-1', N'True'),
	(N'{E22D515A-8492-415D-8A91-97C9EC576A7D}', N'6421', N'Chi phí bán hàng', N'Selling expenses', N'', N'{646D0E5E-9D13-44C2-A4DA-E7C8B7DC1F02}', N'False', 2, N'642', 3, N'-1', N'True'),
	(N'{7CA64BE9-CE4C-47DF-81E7-986B5BCD2CB4}', N'3335', N'Thuế thu nhập cá nhân', N'Personal income tax', N'', N'{4D47EBE7-A76E-4C82-890E-E5CE5F5437C3}', N'False', 2, N'333', 2, N'-1', N'True'),
	(N'{D303C286-C4A6-496D-AD6F-9899E23050EA}', N'112', N'Tiền gửi Ngân hàng', N'Cash in bank', NULL, NULL, N'False', 1, N'112', 0, N'6', N'True'),
	(N'{DF9BF405-18A3-4A18-8E7E-9ADA05C08149}', N'21111', N'Nhà cửa, vật kiến trúc', N'Houses and architectural', N'', N'{82EE395E-3A7C-41F8-A6BF-67F1710B5D83}', N'False', 3, N'211', 0, N'-1', N'False'),
	(N'{398AC7AF-432C-4E2F-84EE-9B02AEEEBB2B}', N'331', N'Phải trả cho người bán', N'Payable to seller', N'', NULL, N'False', 1, N'331', 2, N'0', N'True'),
	(N'{3D7138ED-B8C4-4AE6-B38A-9BCD2B35CD07}', N'3412', N'Nợ thuê tài chính', N'Long-term liabilites', N'', N'{8EED1E1F-FAB1-45E5-8750-1F7DE5B8B2F7}', N'False', 2, N'341', 1, N'-1', N'True'),
	(N'{8CB3FEF4-5EE4-413C-BD1A-A19D0D668301}', N'338', N'Phải trả, phải nộp khác', N'Other payable', N'', NULL, N'False', 1, N'338', 2, N'-1', N'True'),
	(N'{F28093A6-285E-469E-9BFF-A2A0B6424F2B}', N'21135', N'Phần mềm máy tính', N'Software', N'', N'{168820BE-7173-476E-BAB3-FF4CAA7EFD1D}', N'False', 3, N'211', 0, N'-1', N'False'),
	(N'{40DF88DB-962D-466D-B169-A2C00A7F9375}', N'3336', N'Thuế tài nguyên', N'Natural resource tax', N'', N'{4D47EBE7-A76E-4C82-890E-E5CE5F5437C3}', N'False', 2, N'333', 2, N'-1', N'True'),
	(N'{537608C4-6CDF-40E5-A9CA-A324644E6CFF}', N'21115', N'Cây lâu năm, súc vật làm việc và cho sản phẩm', N'Long term trees, working & killed animals', NULL, N'{82EE395E-3A7C-41F8-A6BF-67F1710B5D83}', N'False', 3, N'211', 0, N'-1', N'False'),
	(N'{5D9ADFD2-BB4E-4694-BED0-A347F8C257F6}', N'3386', N'Nhận ký quỹ, ký cược', N'Short-term deposits received', N'', N'{8CB3FEF4-5EE4-413C-BD1A-A19D0D668301}', N'False', 2, N'338', 2, N'-1', N'True'),
	(N'{B7005980-1D6C-4C77-9A00-A6E326B63E5F}', N'4211', N'Lợi nhuận sau thuế chưa phân phối năm trước', N'Previous year undistributed earnings', N'', N'{181852E3-ADB3-4222-A22B-BE5132859A3D}', N'False', 2, N'421', 2, N'-1', N'True'),
	(N'{987FA92C-7B7B-496D-8447-A8B6FBF83DEE}', N'3531', N'Quỹ khen thưởng', N'Bonus fund', N'', N'{81369FA4-A45C-4020-920F-B8698A042698}', N'False', 2, N'353', 1, N'-1', N'True'),
	(N'{8B0785EF-8580-4FD0-9D53-A8FE01ECD715}', N'21112', N'Máy móc, thiết bị', N'Equipment & machines', N'', N'{82EE395E-3A7C-41F8-A6BF-67F1710B5D83}', N'False', 3, N'211', 0, N'-1', N'False'),
	(N'{C5F9765C-BC6C-4427-801F-A98FC6280E8F}', N'242', N'Chi phí trả trước', N'Long-term prepaid expenses', NULL, NULL, N'False', 1, N'242', 0, N'-1', N'True'),
	(N'{E179F840-6B6F-4300-9A17-AC3338F4EA92}', N'334', N'Phải trả người lao động', N'Payable to employees', N'', NULL, N'False', 1, N'334', 2, N'-1', N'True'),
	(N'{76F01739-D301-40C8-9C2F-AD5F790B66FD}', N'3533', N'Quỹ phúc lợi đã hình thành TSCĐ', N'Welfare fund used to acquire fixed assets', N'', N'{81369FA4-A45C-4020-920F-B8698A042698}', N'False', 2, N'353', 1, N'-1', N'True'),
	(N'{E307B2C8-AAD7-41FC-9B01-AF758F52F740}', N'133', N'Thuế GTGT được khấu trừ', N'VAT deducted', NULL, NULL, N'True', 1, N'133', 0, N'1', N'True'),
	(N'{E7D30ACA-4854-4DC2-8384-B0C7EECA02CE}', N'3382', N'Kinh phí công đoàn', N'Trade Union fees', N'', N'{8CB3FEF4-5EE4-413C-BD1A-A19D0D668301}', N'False', 2, N'338', 2, N'-1', N'True'),
	(N'{9987673D-DB59-46F9-A577-B162A33C9068}', N'5113', N'Doanh thu cung cấp dịch vụ', N'Turnover from service provision', N'', N'{76011DD3-7B97-4279-861E-24322EBBF0F1}', N'False', 2, N'511', 3, N'-1', N'True'),
	(N'{7C6F1112-377F-4F95-AEF8-B1C37DEABC8F}', N'3561', N'Quỹ phát triển khoa học và công nghệ', N'Development of science and technology fund', N'', N'{70440878-3FBB-4034-83F0-5BE6369A8E16}', N'False', 2, N'356', 1, N'-1', N'True'),
	(N'{F45FF65C-4123-4037-AE17-B2E1B6491349}', N'21114', N'Thiết bị, dụng cụ quản lý', N'Managerial equipment and instruments', N'', N'{82EE395E-3A7C-41F8-A6BF-67F1710B5D83}', N'False', 3, N'211', 0, N'-1', N'False'),
	(N'{B30A4B77-34F9-488D-A0E7-B35C59242ADD}', N'2294', N'Dự phòng giảm giá hàng tồn kho', NULL, NULL, N'{580759E3-3322-4058-9E6D-D303DF12F9F3}', N'False', 2, N'229', 0, N'-1', N'True'),
	(N'{98A56E04-D383-46D5-B551-B3D1D7140CBC}', N'335', N'Chi phí phải trả', N'Accruals', N'', NULL, N'False', 1, N'335', 1, N'-1', N'True'),
	(N'{DA8ED7FC-845B-40A6-AE5F-B4632D787E68}', N'3361', N'Phải trả nội bộ về vốn kinh doanh', N'', N'', N'{4A0760B1-FDFA-4439-B0EE-3F6D5FDABC42}', N'False', 2, N'336', 1, N'-1', N'True'),
	(N'{76ED0734-2372-4644-9EFE-B55BAF57870A}', N'6422', N'Chi phí quản lý doanh nghiệp', N'General & administration expenses', N'', N'{646D0E5E-9D13-44C2-A4DA-E7C8B7DC1F02}', N'False', 2, N'642', 3, N'-1', N'True'),
	(N'{81369FA4-A45C-4020-920F-B8698A042698}', N'353', N'Quỹ khen thưởng, phúc lợi', N'Bonus & welfare funds', N'', NULL, N'True', 1, N'353', 1, N'-1', N'True'),
	(N'{C5895FB7-D1CB-4493-91CB-B9B3CC32D96D}', N'5118', N'Doanh thu khác', N'Other sales', N'', N'{76011DD3-7B97-4279-861E-24322EBBF0F1}', N'False', 2, N'511', 3, N'-1', N'True'),
	(N'{CFB957C2-6D7C-41B0-BCBE-B9E7444E7AFF}', N'33312', N'Thuế GTGT hàng nhập khẩu', N'VAT for imported goods', NULL, N'{A278B707-C226-42F0-A46C-DCF9C351F35A}', N'False', 3, N'333', 0, N'-1', N'True'),
	(N'{89D80BE7-29FB-49C7-BE72-BB7CE995D577}', N'3388', N'Phải trả, phải nộp khác', N'Other payable', N'', N'{8CB3FEF4-5EE4-413C-BD1A-A19D0D668301}', N'False', 2, N'338', 2, N'-1', N'True'),
	(N'{96C2BBB2-4DDD-4404-9A45-BCE76CDD7BBB}', N'2141', N'Hao mòn TSCĐ hữu hình', N'Tangible fixed assets depreciation', N'', N'{7261A5DA-CEC1-457C-BBE9-20F1BCA122D9}', N'False', 2, N'214', 1, N'-1', N'True'),
	(N'{CC103408-C0D4-47CD-8606-BD8BD4E1B440}', N'1381', N'Tài sản thiếu chờ xử lý', N'Shortage of assets awaiting resolution', N'', N'{0F464EC8-858B-4802-9097-7D8ED6E02565}', N'False', 2, N'138', 2, N'-1', N'True'),
	(N'{181852E3-ADB3-4222-A22B-BE5132859A3D}', N'421', N'Lợi nhuận sau thuế chưa phân phối', N'Undistributed earnings', N'', NULL, N'True', 1, N'421', 2, N'-1', N'True'),
	(N'{2F077F7D-6A3E-48D6-B327-C0AE0CBC6FAA}', N'515', N'Doanh thu hoạt động tài chính', N'Turnover from financial operations', N'', NULL, N'False', 1, N'515', 3, N'-1', N'True'),
	(N'{B0A2BFFD-D7E1-4116-B3F1-C36662FB989E}', N'419', N'Cổ phiếu quỹ', N'Treasury stock', N'', NULL, N'False', 1, N'419', 1, N'-1', N'True'),
	(N'{E5DC7000-DA05-4808-B65E-CC5F948E70C7}', N'5111', N'Doanh thu bán hàng hóa', N'Goods sale', N'', N'{76011DD3-7B97-4279-861E-24322EBBF0F1}', N'False', 2, N'511', 3, N'-1', N'True'),
	(N'{F298FAC8-FEA0-449B-982C-CCA1D92E9200}', N'33381', N'Thuế bảo vệ môi trường', N'', N'', N'{2C2F861C-D50D-4B10-929D-FDCF63E0C2B4}', N'False', 3, N'333', 2, N'-1', N'True'),
	(N'{B94A2806-D514-4B0B-9248-CEEA38FECC0C}', N'121', N'Chứng khoán kinh doanh', N'', N'', NULL, N'False', 1, N'121', 0, N'-1', N'True'),
	(N'{EA2EAA7A-7450-4F91-8225-CFC6FC4AB16F}', N'151', N'Hàng mua đang đi đường', NULL, NULL, NULL, N'False', 1, N'151', 0, N'-1', N'True'),
	(N'{A05D95CA-0641-4EBA-AE48-D010326D3863}', N'2142', N'Hao mòn TSCĐ thuê tài chính', N'Financial leasing fixed assets depreciation', N'', N'{7261A5DA-CEC1-457C-BBE9-20F1BCA122D9}', N'False', 2, N'214', 1, N'-1', N'True'),
	(N'{7CC5ABBE-E26B-4D5C-AA3C-D01AE4AFA07D}', N'1368', N'Phải thu nội bộ khác', N'', N'', N'{747727CD-3D84-49E6-BEFD-2A549A62FF65}', N'False', 2, N'136', 0, N'-1', N'True'),
	(N'{D3ADDA5D-9564-477C-B76F-D02511497841}', N'4112', N'Thặng dư vốn cổ phần', N'Share premium', N'', N'{A81EAD52-1D99-471E-9D24-E1228A056878}', N'False', 2, N'411', 1, N'-1', N'True'),
	(N'{6159F4CA-F2F0-45D6-AFF6-D2156255116A}', N'2143', N'Hao mòn TSCĐ vô hình', N'Intangible fixed assets depreciation', N'', N'{7261A5DA-CEC1-457C-BBE9-20F1BCA122D9}', N'False', 2, N'214', 1, N'-1', N'True'),
	(N'{2969DC08-9C7C-4D80-9DA2-D290D354F70D}', N'2412', N'Xây dựng cơ bản', N'Construction in process', NULL, N'{A724E9E7-BCDC-4E83-96D1-12A4EE7DE3F7}', N'False', 2, N'241', 0, N'-1', N'True'),
	(N'{580759E3-3322-4058-9E6D-D303DF12F9F3}', N'229', N'Dự phòng tổn thất tài sản', N'Provision for decline in long term investments', NULL, NULL, N'False', 1, N'229', 0, N'-1', N'True'),
	(N'{9DD673D2-DE90-4592-9324-D3B21F05CA1E}', N'413', N'Chênh lệch tỷ giá hối đoái', N'Foreign exchange differences', N'', NULL, N'False', 1, N'413', 1, N'-1', N'True'),
	(N'{2554B1EE-4DB4-4D3C-A2EE-DAD12BC57D82}', N'3411', N'Các khoản đi vay	', N'Long-term borrowing', N'', N'{8EED1E1F-FAB1-45E5-8750-1F7DE5B8B2F7}', N'False', 2, N'341', 1, N'-1', N'True'),
	(N'{80C7787F-EC70-40FF-8259-DCF913821B77}', N'21138', N'TSCĐ vô hình khác', N'Other intangible fixed assets', N'', N'{168820BE-7173-476E-BAB3-FF4CAA7EFD1D}', N'False', 3, N'211', 0, N'-1', N'False'),
	(N'{A278B707-C226-42F0-A46C-DCF9C351F35A}', N'3331', N'Thuế giá trị gia tăng phải nộp', N'Value Added Tax', N'', N'{4D47EBE7-A76E-4C82-890E-E5CE5F5437C3}', N'True', 2, N'333', 2, N'-1', N'True'),
	(N'{18C9B9B5-F2A0-45C8-9B2B-DD523E6F7BC5}', N'3384', N'Bảo hiểm y tế', N'Health insurance', N'', N'{8CB3FEF4-5EE4-413C-BD1A-A19D0D668301}', N'False', 2, N'338', 2, N'-1', N'True'),
	(N'{DF4ADBB7-0DC7-4E23-AB82-E011BD40D93D}', N'141', N'Tạm ứng', N'Advances', N'', NULL, N'False', 1, N'141', 0, N'2', N'True'),
	(N'{A81EAD52-1D99-471E-9D24-E1228A056878}', N'411', N'Vốn đầu tư của chủ sở hữu', N'Working capital', N'', NULL, N'True', 1, N'411', 1, N'-1', N'True'),
	(N'{8B0C2A4E-24EE-4950-8824-E24300A5883C}', N'217', N'Bất động sản đầu tư', N'Investment real estate', NULL, NULL, N'False', 1, N'217', 0, N'-1', N'True'),
	(N'{20051CF2-0828-4722-9DC6-E2B130FE0112}', N'1121', N'Tiền Việt Nam', N'Vietnam dong', NULL, N'{D303C286-C4A6-496D-AD6F-9899E23050EA}', N'False', 2, N'112', 0, N'6', N'True'),
	(N'{8A614B4D-B528-4E56-BA97-E2BFC68FA896}', N'153', N'Công cụ, dụng cụ', N'Tools and supplies', N'', NULL, N'False', 1, N'153', 0, N'5', N'True'),
	(N'{4D47EBE7-A76E-4C82-890E-E5CE5F5437C3}', N'333', N'Thuế và các khoản phải nộp Nhà nước', N'Taxes and payable to state budget', N'', NULL, N'True', 1, N'333', 2, N'-1', N'True'),
	(N'{646D0E5E-9D13-44C2-A4DA-E7C8B7DC1F02}', N'642', N'Chi phí quản lý kinh doanh', N'Business management expenses', N'', NULL, N'True', 1, N'642', 3, N'-1', N'True'),
	(N'{9046F5CF-8E7B-43CC-B2A6-E81CCC306C21}', N'1386', N'Cầm cố, thế chấp, ký quỹ, ký cược', N'', N'', N'{0F464EC8-858B-4802-9097-7D8ED6E02565}', N'False', 2, N'138', 2, N'-1', N'True'),
	(N'{CFA92323-A8B2-4355-B0A3-E868F54E3AED}', N'111', N'Tiền mặt', N'Cash in hand', N'', NULL, N'False', 1, N'111', 0, N'-1', N'True'),
	(N'{97AC7260-27AD-4D64-81CA-E96712FDB118}', N'3332', N'Thuế tiêu thụ đặc biệt', N'Special consumption tax', N'', N'{4D47EBE7-A76E-4C82-890E-E5CE5F5437C3}', N'False', 2, N'333', 2, N'-1', N'True'),
	(N'{4DC41060-A86C-4BF1-88F1-ECBB724C9D0B}', N'631', N'Giá thành sản xuất', N'Production cost', N'', NULL, N'False', 1, N'631', 3, N'-1', N'True'),
	(N'{0478CA57-B2CA-47E3-AE59-EE0E6C202EE2}', N'21118', N'TSCĐ khác', N'Other tangible fixed assets', N'', N'{82EE395E-3A7C-41F8-A6BF-67F1710B5D83}', N'False', 3, N'211', 0, N'-1', N'False'),
	(N'{EAE725AF-6702-486D-8F7B-EFED6CC51725}', N'3381', N'Tài sản thừa chờ giải quyết', N'Surplus assets awaiting for resolution', N'', N'{8CB3FEF4-5EE4-413C-BD1A-A19D0D668301}', N'False', 2, N'338', 2, N'-1', N'True'),
	(N'{80ACF9C2-19BD-40AC-BD67-F472683F2721}', N'3368', N'Phải trả nội bộ khác', N'', N'', N'{4A0760B1-FDFA-4439-B0EE-3F6D5FDABC42}', N'False', 2, N'336', 1, N'-1', N'True'),
	(N'{CA302434-105C-4DC1-81BF-F76403EEFA52}', N'156', N'Hàng hóa', N'Merchandise inventory', N'', NULL, N'False', 1, N'156', 0, N'5', N'True'),
	(N'{8A664628-08D0-4933-AFA0-F7E9B18B2865}', N'3521', N'Dự phòng bảo hành sản phẩm hàng hóa', N'', N'', N'{05D067F4-5CD2-49A1-9C5F-70E03D927CF4}', N'False', 2, N'352', 1, N'-1', N'True'),
	(N'{194E95C4-DB0E-4DB3-85F1-F8B9F0EB52D3}', N'3334', N'Thuế thu nhập doanh nghiệp', N'Profit tax', N'', N'{4D47EBE7-A76E-4C82-890E-E5CE5F5437C3}', N'False', 2, N'333', 2, N'-1', N'True'),
	(N'{B44C72A7-A4AE-4A6A-8D24-FC0DFEEDFF5C}', N'3387', N'Doanh thu chưa thực hiện', N'Unrealized turnover', N'', N'{8CB3FEF4-5EE4-413C-BD1A-A19D0D668301}', N'False', 2, N'338', 2, N'-1', N'True'),
	(N'{2C2F861C-D50D-4B10-929D-FDCF63E0C2B4}', N'3338', N'Thuế bảo vệ môi trường và các thuế khác', N'Other taxes', N'', N'{4D47EBE7-A76E-4C82-890E-E5CE5F5437C3}', N'False', 2, N'333', 2, N'-1', N'True'),
	(N'{168820BE-7173-476E-BAB3-FF4CAA7EFD1D}', N'2113', N'TSCĐ vô hình ', N'Intangible fixed assets', NULL, N'{703532AA-D5E3-4EB7-8490-8816B5A32E5A}', N'False', 2, N'211', 0, N'-1', N'True');
/*!40000 ALTER TABLE "Account" ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, N'') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO
SET NOEXEC OFF
GO
