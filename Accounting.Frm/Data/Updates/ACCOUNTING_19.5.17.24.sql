SET CONCAT_NULL_YIELDS_NULL, ANSI_NULLS, ANSI_PADDING, QUOTED_IDENTIFIER, ANSI_WARNINGS, ARITHABORT, XACT_ABORT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS OFF
GO

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[TM01TAINDetail]', N'tmp_devart_TM01TAINDetail', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename table [dbo].[TM01TAINDetail] to [dbo].[tmp_devart_TM01TAINDetail]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM01TAINDetail] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM01TAINID] [uniqueidentifier] NULL,
  [Type] [int] NULL,
  [MaterialGoodsResourceTaxGroupID] [uniqueidentifier] NULL,
  [MaterialGoodsResourceTaxGroupName] [nvarchar](512) NULL,
  [Unit] [nvarchar](25) NULL,
  [Quantity] [decimal](25, 10) NULL,
  [UnitPrice] [money] NULL,
  [TaxRate] [decimal](25, 10) NULL,
  [ResourceTaxTaxAmountUnit] [decimal](25, 10) NULL,
  [ResourceTaxAmountIncurration] [money] NULL,
  [ResourceTaxAmountDeduction] [money] NULL,
  [ResourceTaxAmount] [money] NULL,
  [OrderPriority] [int] NOT NULL
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

INSERT [dbo].[TM01TAINDetail](ID, TM01TAINID, Type, MaterialGoodsResourceTaxGroupID, MaterialGoodsResourceTaxGroupName, Unit, Quantity, UnitPrice, TaxRate, ResourceTaxTaxAmountUnit, ResourceTaxAmountIncurration, ResourceTaxAmountDeduction, ResourceTaxAmount, OrderPriority)
  SELECT ID, TM01TAINID, Type, MaterialGoodsResourceTaxGroupID, MaterialGoodsResourceTaxGroupName, Unit, Quantity, UnitPrice, TaxRate, ResourceTaxTaxAmountUnit, ResourceTaxAmountIncurration, ResourceTaxAmountDeduction, ResourceTaxAmount, OrderPriority FROM [dbo].[tmp_devart_TM01TAINDetail] WITH (NOLOCK)

if @@ERROR = 0
  DROP TABLE [dbo].[tmp_devart_TM01TAINDetail]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TM01TAINDetail]
  ADD PRIMARY KEY CLUSTERED ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[TM01GTGTDetail]', N'tmp_devart_TM01GTGTDetail', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename table [dbo].[TM01GTGTDetail] to [dbo].[tmp_devart_TM01GTGTDetail]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM01GTGTDetail] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM01GTGTID] [uniqueidentifier] NULL,
  [Code] [nvarchar](512) NULL,
  [Name] [nvarchar](512) NULL,
  [Data] [nvarchar](512) NULL,
  [OrderPriority] [int] NULL
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

INSERT [dbo].[TM01GTGTDetail](ID, TM01GTGTID, Code, Name, Data, OrderPriority)
  SELECT ID, TM01GTGTID, Code, Name, Data, OrderPriority FROM [dbo].[tmp_devart_TM01GTGTDetail] WITH (NOLOCK)

if @@ERROR = 0
  DROP TABLE [dbo].[tmp_devart_TM01GTGTDetail]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TM01GTGTDetail]
  ADD PRIMARY KEY CLUSTERED ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[TM01GTGTAdjust]', N'tmp_devart_TM01GTGTAdjust', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename table [dbo].[TM01GTGTAdjust] to [dbo].[tmp_devart_TM01GTGTAdjust]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM01GTGTAdjust] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM01GTGTID] [uniqueidentifier] NOT NULL,
  [Code] [nvarchar](512) NULL,
  [Name] [nvarchar](512) NULL,
  [DeclaredAmount] [money] NULL,
  [AdjustAmount] [money] NULL,
  [DifferAmount] [money] NULL,
  [LateDays] [int] NULL,
  [LateAmount] [money] NULL,
  [ExplainAmount] [money] NULL,
  [CommandNo] [nvarchar](512) NULL,
  [CommandDate] [datetime] NULL,
  [TaxCompanyName] [nvarchar](512) NULL,
  [TaxCompanyDecisionName] [nvarchar](512) NULL,
  [ReceiveDays] [int] NULL,
  [ExplainLateAmount] [money] NULL,
  [DifferReason] [nvarchar](2048) NULL,
  [OrderPriority] [int] NULL,
  [Type] [int] NULL
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

INSERT [dbo].[TM01GTGTAdjust](ID, TM01GTGTID, Code, Name, DeclaredAmount, AdjustAmount, DifferAmount, LateDays, LateAmount, ExplainAmount, CommandNo, CommandDate, TaxCompanyName, TaxCompanyDecisionName, ReceiveDays, ExplainLateAmount, DifferReason, OrderPriority, Type)
  SELECT ID, TM01GTGTID, Code, Name, DeclaredAmount, AdjustAmount, DifferAmount, LateDays, LateAmount, ExplainAmount, CommandNo, CommandDate, TaxCompanyName, TaxCompanyDecisionName, ReceiveDays, ExplainLateAmount, DifferReason, OrderPriority, Type FROM [dbo].[tmp_devart_TM01GTGTAdjust] WITH (NOLOCK)

if @@ERROR = 0
  DROP TABLE [dbo].[tmp_devart_TM01GTGTAdjust]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TM01GTGTAdjust]
  ADD PRIMARY KEY CLUSTERED ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TM01GTGTAdjust', 'COLUMN', N'Type'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TM01GTGTAdjust', 'COLUMN', N'OrderPriority'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[TM01GTGT]', N'tmp_devart_TM01GTGT', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename table [dbo].[TM01GTGT] to [dbo].[tmp_devart_TM01GTGT]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM01GTGT] (
  [ID] [uniqueidentifier] NOT NULL,
  [BranchID] [uniqueidentifier] NULL,
  [TypeID] [int] NOT NULL,
  [DeclarationName] [nvarchar](512) NULL,
  [DeclarationTerm] [nvarchar](512) NULL,
  [FromDate] [datetime] NULL,
  [ToDate] [datetime] NULL,
  [IsFirstDeclaration] [bit] NULL,
  [AdditionTime] [int] NULL,
  [AdditionDate] [datetime] NULL,
  [IsExtend] [bit] NULL,
  [IsAppendix011GTGT] [bit] NULL,
  [IsAppendix012GTGT] [bit] NULL,
  [CompanyName] [nvarchar](512) NULL,
  [CompanyTaxCode] [nvarchar](50) NULL,
  [TaxAgencyTaxCode] [nvarchar](50) NULL,
  [TaxAgencyName] [nvarchar](512) NULL,
  [TaxAgencyEmployeeName] [nvarchar](512) NULL,
  [CertificationNo] [nvarchar](512) NULL,
  [SignName] [nvarchar](512) NULL,
  [SignDate] [datetime] NULL,
  [Career] [nvarchar](512) NULL DEFAULT (NULL),
  [ExtensionCase] [nvarchar](512) NULL DEFAULT (NULL),
  [AdditionTerm] [nvarchar](512) NULL DEFAULT (NULL)
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

INSERT [dbo].[TM01GTGT](ID, BranchID, TypeID, DeclarationName, DeclarationTerm, FromDate, ToDate, IsFirstDeclaration, AdditionTime, AdditionDate, Career, IsExtend, ExtensionCase, IsAppendix011GTGT, IsAppendix012GTGT, CompanyName, CompanyTaxCode, TaxAgencyTaxCode, TaxAgencyName, TaxAgencyEmployeeName, CertificationNo, SignName, SignDate)
  SELECT ID, BranchID, TypeID, DeclarationName, DeclarationTerm, FromDate, ToDate, IsFirstDeclaration, AdditionTime, AdditionDate, Career, IsExtend, ExtensionCase, IsAppendix011GTGT, IsAppendix012GTGT, CompanyName, CompanyTaxCode, TaxAgencyTaxCode, TaxAgencyName, TaxAgencyEmployeeName, CertificationNo, SignName, SignDate FROM [dbo].[tmp_devart_TM01GTGT] WITH (NOLOCK)

if @@ERROR = 0
  DROP TABLE [dbo].[tmp_devart_TM01GTGT]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TM01GTGT]
  ADD PRIMARY KEY CLUSTERED ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TM01GTGT', 'COLUMN', N'AdditionTerm'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TM01GTGT', 'COLUMN', N'ExtensionCase'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TM01GTGT', 'COLUMN', N'Career'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[TM012GTGT]', N'tmp_devart_TM012GTGT', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename table [dbo].[TM012GTGT] to [dbo].[tmp_devart_TM012GTGT]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM012GTGT] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM01GTGTID] [uniqueidentifier] NOT NULL,
  [VoucherID] [uniqueidentifier] NULL,
  [VoucherDetailID] [uniqueidentifier] NULL,
  [InvoiceDate] [datetime] NULL,
  [InvoiceNo] [nvarchar](25) NULL,
  [InvoiceSeries] [nvarchar](25) NULL,
  [AccountingObjectID] [uniqueidentifier] NULL,
  [AccountingObjectName] [nvarchar](512) NULL,
  [TaxCode] [nvarchar](50) NULL,
  [PretaxAmount] [money] NOT NULL,
  [VATRate] [decimal](25) NULL,
  [TaxAmount] [money] NULL,
  [Note] [nvarchar](512) NULL,
  [GroupName] [nvarchar](512) NULL,
  [OrderPriority] [int] NULL,
  [Type] [int] NULL
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

INSERT [dbo].[TM012GTGT](ID, TM01GTGTID, VoucherID, VoucherDetailID, InvoiceDate, InvoiceNo, InvoiceSeries, AccountingObjectID, AccountingObjectName, TaxCode, PretaxAmount, TaxAmount, Note, OrderPriority, Type)
  SELECT ID, TM01GTGTID, VoucherID, VoucherDetailID, InvoiceDate, InvoiceNo, InvoiceSeries, AccountingObjectID, AccountingObjectName, TaxCode, PretaxAmount, TaxAmount, Note, OrderPriority, Type FROM [dbo].[tmp_devart_TM012GTGT] WITH (NOLOCK)

if @@ERROR = 0
  DROP TABLE [dbo].[tmp_devart_TM012GTGT]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TM012GTGT]
  ADD PRIMARY KEY CLUSTERED ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'TM012GTGT', 'COLUMN', N'Type'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @res int
EXEC @res = sp_rename N'[dbo].[TM011GTGT]', N'tmp_devart_TM011GTGT', 'OBJECT'
IF @res <> 0
  RAISERROR ('Error while Rename table [dbo].[TM011GTGT] to [dbo].[tmp_devart_TM011GTGT]', 11, 1 );
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[TM011GTGT] (
  [ID] [uniqueidentifier] NOT NULL,
  [TM01GTGTID] [uniqueidentifier] NOT NULL,
  [VoucherID] [uniqueidentifier] NULL,
  [VoucherDetailID] [uniqueidentifier] NULL,
  [InvoiceDate] [datetime] NULL,
  [InvoiceNo] [nvarchar](25) NULL,
  [InvoiceSeries] [nvarchar](25) NULL,
  [AccountingObjectID] [uniqueidentifier] NULL,
  [AccountingObjectName] [nvarchar](512) NULL,
  [TaxCode] [nvarchar](50) NULL,
  [PretaxAmount] [money] NOT NULL,
  [VATRate] [decimal](25) NULL,
  [TaxAmount] [money] NULL,
  [Note] [nvarchar](512) NULL,
  [GroupName] [nvarchar](512) NULL,
  [OrderPriority] [int] NULL
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

INSERT [dbo].[TM011GTGT](ID, TM01GTGTID, VoucherID, VoucherDetailID, InvoiceDate, InvoiceNo, InvoiceSeries, AccountingObjectID, AccountingObjectName, TaxCode, PretaxAmount, VATRate, TaxAmount, Note, OrderPriority)
  SELECT ID, TM01GTGTID, VoucherID, VoucherDetailID, InvoiceDate, InvoiceNo, InvoiceSeries, AccountingObjectID, AccountingObjectName, TaxCode, PretaxAmount, VATRate, TaxAmount, Note, OrderPriority FROM [dbo].[tmp_devart_TM011GTGT] WITH (NOLOCK)

if @@ERROR = 0
  DROP TABLE [dbo].[tmp_devart_TM011GTGT]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TM011GTGT]
  ADD PRIMARY KEY CLUSTERED ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAReturn]
  ADD [IsDeliveryVoucher] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_GetBkeInv_Sale]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT
    )
AS
BEGIN 
/*
VATRate type :
-1: Không chịu thuế
0: thuê 0%
5: 5%
10: 10%
*/
/*add by cuongpv de thong nhat cach lam tron so lieu*/
	DECLARE @tbDataLocal TABLE(
		VATRate DECIMAL(25,10),
		so_hd NVARCHAR(25),
		ngay_hd DATETIME,
		Nguoi_mua NVARCHAR(512),
		MST NVARCHAR(50),
		Mat_hang NVARCHAR(512),
		Amount DECIMAL(25,0),
		DiscountAmount DECIMAL(25,0),
		Thue_GTGT DECIMAL(25,0),
		TKThue NVARCHAR(25),
		flag int
	)
/*end add by cuongpv*/
	if @IsSimilarBranch = 0
	Begin
		/*add by cuongpv*/
		INSERT INTO @tbDataLocal
		/*end add by cuongpv*/
		 select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
		  Description as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
		   from SAinvoice a join SAInvoiceDetail b on a.id = b.SAInvoiceID where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1 
		union all
		select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST, 
		Description as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
		 from  SABill a join SABillDetail b on a.ID = b.SABillID  where cast(InvoiceDate As Date) between @FromDate and @ToDate
		union all
		select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
		  Description as Mat_hang,  Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,-1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
		 from SAReturn a join SAReturnDetail b on a.id = b.SAReturnID
		where cast(InvoiceDate As Date) between @FromDate and @ToDate and recorded = 1 
		

		select VATRate, so_hd, ngay_hd, Nguoi_mua, MST, Mat_hang, (Amount - DiscountAmount) as Doanh_so, Thue_GTGT, TKThue, flag
		from @tbDataLocal order by VATRate, ngay_hd, so_hd, Nguoi_mua
	End
	else
	Begin
		/*add by cuongpv*/
		INSERT INTO @tbDataLocal
		/*end add by cuongpv*/
			select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
			  a.Reason as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
			   from SAinvoice a join SAInvoiceDetail b on a.id = b.SAInvoiceID where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1 
			union all
			select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST, 
			a.Reason as Mat_hang, Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
			 from  SABill a join SABillDetail b on a.ID = b.SABillID  where cast(InvoiceDate As Date) between @FromDate and @ToDate
			union all
			select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
			  a.Reason as Mat_hang,  Amount, DiscountAmount, VATAmount as Thue_GTGT, VATAccount as TKThue,-1 flag  /*Edit by cuongpv (Amount-DiscountAmount) as Doanh_so*/
			 from SAReturn a join SAReturnDetail b on a.id = b.SAReturnID
			where cast(InvoiceDate As Date) between @FromDate and @ToDate and recorded = 1

		select VATRate, 
			   so_hd, 
			   ngay_hd, 
			   Nguoi_mua, 
			   MST,
			   Mat_hang, 
			   sum(Doanh_so) Doanh_so, 
			   sum(Thue_GTGT) Thue_GTGT, 
			   TKThue,
			   flag
		from
			( select VATRate, so_hd, ngay_hd, Nguoi_mua, MST, Mat_hang, (Amount - DiscountAmount) as Doanh_so, Thue_GTGT, TKThue, flag 
			  from @tbDataLocal
			) aa
		group by VATRate, 
			   so_hd, 
			   ngay_hd, 
			   Nguoi_mua, 
			   MST,
			   Mat_hang, 
			   TKThue ,
			   flag
		order by VATRate, ngay_hd, so_hd,Nguoi_mua
	End
end;
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPServiceDetail]
  ADD [Quantity] [decimal](25, 10) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPServiceDetail]
  ADD [UnitPrice] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPServiceDetail]
  ADD [UnitPriceOriginal] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PPDiscountReturn]
  ADD [IsDeliveryVoucher] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sp_refreshview '[dbo].[ViewVoucherInvisible]'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_GetBkeInv_Buy]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT
    )
AS
BEGIN
/*add by cuongpv de xu ly cach thong nhat lam tron*/
	DECLARE @tbDataLocal TABLE(
		GoodsServicePurchaseCode NVARCHAR(25),
		GoodsServicePurchaseName NVARCHAR(512),
		So_HD NVARCHAR(25),
		Ngay_HD DATETIME,
		ten_NBan NVARCHAR(512),
		MST NVARCHAR(50),
		Mat_hang NVARCHAR(512),
		GT_chuathue decimal(25,0),
		Thue_suat decimal(25,10),
		Thue_GTGT decimal(25,0),
		TK_Thue NVARCHAR(25),
		flag int
	)
/*end add by cuongpv*/
	if @IsSimilarBranch = 0
	Begin    
		/*add by cuongpv*/
		INSERT INTO @tbDataLocal
		/*end add by cuongpv*/
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
		 b.Description as Mat_hang, InwardAmount  as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag                  
		 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
		where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1
		union all         
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
		 b.Description as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag         
		from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
		join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
		where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1
		union all    
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
		 b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag         
		 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
		where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
		b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag																	
		from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where InvoiceDate between @FromDate and @ToDate and Recorded = 1
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
		b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag																
		from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where InvoiceDate between @FromDate and @ToDate and Recorded = 1
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag																		
		 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where InvoiceDate between @FromDate and @ToDate and Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag																	
		 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where InvoiceDate between @FromDate and @ToDate and a.Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag																		
		 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where InvoiceDate between @FromDate and @ToDate and a.Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag																	
		 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where InvoiceDate between @FromDate and @ToDate and a.Recorded = 1;
	
		/*edit by cuongpv order by c.GoodsServicePurchaseCode, InvoiceDate, InvoiceNo -> SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;*/
		SELECT * FROM @tbDataLocal order by GoodsServicePurchaseCode, Ngay_HD, So_HD;
	End
	else
	Begin
		/*add by cuongpv*/
		INSERT INTO @tbDataLocal
		/*end add by cuongpv*/
		 select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
		 a.Reason as Mat_hang, InwardAmount  as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag         
		 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
		where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1
		union all         
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
		 a.Reason as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag         
		from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
		join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
		where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1
		union all    
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
		 a.Reason as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue ,1 flag        
		 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
		where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
		b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag																	
		from FAIncrement a join FAIncrementDetail b on a.ID = b.FAIncrementID																	
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where InvoiceDate between @FromDate and @ToDate and Recorded = 1
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,																	
		b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag																
		from TIIncrement a join TIIncrementDetail b on a.ID = b.TIIncrementID																	
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where InvoiceDate between @FromDate and @ToDate and Recorded = 1
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag																		
		 from GOtherVoucher a join GOtherVoucherDetailTax b on a.ID = b.GOtherVoucherID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where InvoiceDate between @FromDate and @ToDate and Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag																	
		 from MCPayment a join MCPaymentDetailTax b on a.ID = b.MCPaymentID																
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where InvoiceDate between @FromDate and @ToDate and a.Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag																		
		 from MBTellerPaper a join MBTellerPaperDetailTax b on a.ID = b.MBTellerPaperID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where InvoiceDate between @FromDate and @ToDate and a.Recorded = 1 
		union all
		select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, b.AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,			
		 b.Description as Mat_hang, PretaxAmount as GT_chuathue, VATRate as Thue_suat, b.VATAmount as Thue_GTGT, VATAccount as TK_Thue	,1 flag																	
		 from MBCreditCard a join MBCreditCardDetailTax b on a.ID = b.MBCreditCardID		
		join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID																	
		where InvoiceDate between @FromDate and @ToDate and a.Recorded = 1

	select aa.GoodsServicePurchaseCode,
		   aa.GoodsServicePurchaseName,
		   aa.So_HD,
		   aa.Ngay_HD,
		   aa.ten_NBan,
		   aa.MST,
		   aa.Mat_hang,
		   sum(aa.GT_chuathue) GT_chuathue,
		   aa.Thue_suat ,
		   sum(aa.Thue_GTGT) Thue_GTGT,
		   aa.TK_Thue,
		   flag
	from (
		 select * from @tbDataLocal
		) aa
	group by GoodsServicePurchaseCode,
		   GoodsServicePurchaseName,
		   So_HD,
		   Ngay_HD,
		   ten_NBan,
		   MST,
		   Mat_hang,
		   TK_Thue,
		   flag,
		   aa.Thue_suat
	order by aa.GoodsServicePurchaseCode, aa.Ngay_HD, aa.So_HD
	End
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*
*/
ALTER FUNCTION [dbo].[Func_SoDu]
      ( @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT 
      )
RETURNS @Result TABLE
      (
        AccountID UNIQUEIDENTIFIER ,
        DetailByAccountObject BIT ,
        AccountObjectType INT ,
		AccountObjectID NVARCHAR(50) ,
        AccountNumber NVARCHAR(10) ,
        AccountName NVARCHAR(255) ,
        AccountNameEnglish NVARCHAR(255) ,
        AccountCategoryKind INT ,
        IsParent BIT ,
        ParentID UNIQUEIDENTIFIER ,
        Grade INT ,
        AccountKind INT ,
        SortOrder INT ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
      )
AS
    BEGIN
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        DECLARE @MainCurrency NVARCHAR(3)
        SET @MainCurrency = 'VND'	
        /*ngày bắt đầu chương trình*/
        DECLARE @StartDate DATETIME

        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'                
           BEGIN
					
                 DECLARE @GeneralLedger TABLE
                         (
                           AccountNumber NVARCHAR(30) ,
                           AccountObjectID UNIQUEIDENTIFIER ,
                           BranchID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(3) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4) ,
                           OpenningDebitAmountOC DECIMAL(25, 4) ,
                           DebitAmountOC DECIMAL(25, 4) ,
                           CreditAmountOC DECIMAL(25, 4) ,
                           DebitAmountOCAccum DECIMAL(25, 4) ,
                           CreditAmountOCAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedger
                        SELECT  G.*
                        FROM    (
                                  SELECT    Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                                     ELSE 0
                                                END) AS OpenningDebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal - GL.CreditAmountOriginal
                                                     ELSE 0
                                                END) AS OpenningDebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOCAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOCAccum
                                  FROM      @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                  WHERE    GL.PostedDate <= @ToDate
                                  GROUP BY  Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID
                                ) G
			
                 DECLARE @Data TABLE
                         (
                           AccountNumber NVARCHAR(50) ,
						   AccountObjectID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(20) ,
                           SortOrder INT ,
                           OpeningDebitAmount DECIMAL(25, 4) ,
                           OpeningCreditAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,/*Nợ lũy kế*/
                           CreditAmountAccum DECIMAL(25, 4) ,/*có lũy kế*/
                           ClosingDebitAmount DECIMAL(25, 4) ,
                           ClosingCreditAmount DECIMAL(25, 4)
                         )
	
                 INSERT @Data
                        SELECT  F.AccountNumber ,
						        F.AccountObjectID,
                                '' ,
                                0 ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount > 0
                                                 ) THEN F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount < 0
                                                 ) THEN -1 * F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount ,
                                SUM(F.DebitAmount) AS DebitAmount ,
                                SUM(F.CreditAmount) AS CreditAmount ,
                                SUM(F.DebitAmountAccum) AS DebitAmountAccum ,
                                SUM(F.CreditAmountAccum) AS CreditAmountAccum ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                                 ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                                 ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.AccountNumber ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            GL.AccountObjectID AccountObjectID ,
                                            A.AccountGroupKind AccountCategoryKind ,
                                            GL.BranchID BranchID
                                  FROM      @GeneralLedger AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber = A.AccountNumber
                                  WHERE     GL.AccountNumber NOT LIKE '0%'
                                            AND A.IsParentNode = 0
                                  GROUP BY  A.AccountNumber ,
                                            GL.AccountObjectID,
                                            A.AccountGroupKind ,
                                            GL.BranchID
                                  HAVING    SUM(OpenningDebitAmount) <> 0
                                            OR SUM(DebitAmount) <> 0
                                            OR SUM(CreditAmount) <> 0
                                ) AS F
                        GROUP BY F.AccountNumber ,
                                F.AccountCategoryKind,
								F.AccountObjectID
                 OPTION ( RECOMPILE )		         					    
                 INSERT @Result
                        SELECT  A.ID ,
						        null,
								null,
								D.AccountObjectID,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                0 AccountKind,
                                D.SortOrder ,
                                SUM(D.OpeningDebitAmount) ,
                                SUM(D.OpeningCreditAmount) ,
                                SUM(D.DebitAmount) ,
                                SUM(D.CreditAmount) ,
                                SUM(D.DebitAmountAccum) ,
                                SUM(D.CreditAmountAccum) ,
                                SUM(D.ClosingDebitAmount) ,
                                SUM(D.ClosingCreditAmount)
                        FROM    @Data D
                        INNER JOIN dbo.Account A ON D.AccountNumber LIKE A.AccountNumber + '%'
                        WHERE   A.Grade <= @MaxAccountGrade
                        GROUP BY A.ID ,
                                A.AccountNumber ,
								D.AccountObjectID,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                D.SortOrder
                        HAVING  SUM(D.OpeningDebitAmount) <> 0
                                OR SUM(D.OpeningCreditAmount) <> 0
                                OR SUM(D.DebitAmount) <> 0
                                OR SUM(D.CreditAmount) <> 0
                                OR SUM(D.ClosingDebitAmount) <> 0
                                OR SUM(D.ClosingCreditAmount) <> 0
                 OPTION ( RECOMPILE )
           END
        RETURN
    END



GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER FUNCTION [dbo].[Func_GetBalanceAccountF01]
      (
        @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT ,
        @IsBalanceBothSide BIT
      )
RETURNS @Result TABLE
      (
        AccountID UNIQUEIDENTIFIER ,
        DetailByAccountObject BIT ,
        AccountObjectType INT ,
        AccountNumber NVARCHAR(50) ,
        AccountName NVARCHAR(255) ,
        AccountNameEnglish NVARCHAR(255) ,
        AccountCategoryKind INT ,
        IsParent BIT ,
        ParentID UNIQUEIDENTIFIER ,
        Grade INT ,
        AccountKind INT ,
        SortOrder INT ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
      )
AS
    BEGIN
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/
	   DECLARE @MainCurrency NVARCHAR(3)
        SET @MainCurrency = 'VND'	
        /*ngày bắt đầu chương trình*/
        DECLARE @StartDate DATETIME
        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'
        IF @IsBalanceBothSide = 1
           BEGIN
                 DECLARE @GeneralLedger TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           AccountObjectID UNIQUEIDENTIFIER ,
                           BranchID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(3) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4) ,
                           OpenningDebitAmountOC DECIMAL(25, 4) ,
                           DebitAmountOC DECIMAL(25, 4) ,
                           CreditAmountOC DECIMAL(25, 4) ,
                           DebitAmountOCAccum DECIMAL(25, 4) ,
                           CreditAmountOCAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedger
                        SELECT  G.*
                        FROM    (
                                  SELECT    Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                                     ELSE 0
                                                END) AS OpenningDebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal - GL.CreditAmountOriginal
                                                     ELSE 0
                                                END) AS OpenningDebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOCAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOCAccum
                                  FROM      @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                  WHERE    GL.PostedDate <= @ToDate
                                  GROUP BY  Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID
                                ) G
			
                 DECLARE @Data TABLE
                         (
                           AccountNumber NVARCHAR(50) ,
						   AccountObjectID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(20) ,
                           SortOrder INT ,
                           OpeningDebitAmount DECIMAL(25, 4) ,
                           OpeningCreditAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,/*Nợ lũy kế*/
                           CreditAmountAccum DECIMAL(25, 4) ,/*có lũy kế*/
                           ClosingDebitAmount DECIMAL(25, 4) ,
                           ClosingCreditAmount DECIMAL(25, 4)
                         )
	
                 INSERT @Data
                        SELECT  F.AccountNumber ,
						        null,
                                '' ,
                                0 ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount > 0
                                                 ) THEN F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount < 0
                                                 ) THEN -1 * F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount ,
                                SUM(F.DebitAmount) AS DebitAmount ,
                                SUM(F.CreditAmount) AS CreditAmount ,
                                SUM(F.DebitAmountAccum) AS DebitAmountAccum ,
                                SUM(F.CreditAmountAccum) AS CreditAmountAccum ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                                 ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                                 ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.AccountNumber ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            CASE A.DetailByAccountObject
                                                                  WHEN 0 THEN NULL
                                                                  ELSE GL.AccountObjectID
                                                                END AS AccountObjectID ,
                                            A.AccountGroupKind AccountCategoryKind ,
                                            GL.BranchID BranchID
                                  FROM      @GeneralLedger AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber = A.AccountNumber
                                  WHERE     GL.AccountNumber NOT LIKE '0%'
                                            AND A.IsParentNode = 0
                                  GROUP BY  A.AccountNumber ,
                                            A.AccountGroupKind ,
                                            GL.BranchID,
											CASE A.DetailByAccountObject
                                                                  WHEN 0 THEN NULL
                                                                  ELSE GL.AccountObjectID
                                                                END
                                  HAVING    SUM(OpenningDebitAmount) <> 0
                                            OR SUM(DebitAmount) <> 0
                                            OR SUM(CreditAmount) <> 0
                                ) AS F
                        GROUP BY F.AccountNumber ,
                                F.AccountCategoryKind,
								F.AccountObjectID
                 OPTION ( RECOMPILE )		         					    
                 INSERT @Result
                        SELECT  A.ID ,
						        null,
								null,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                0 AccountKind,
                                D.SortOrder ,
                                SUM(D.OpeningDebitAmount) ,
                                SUM(D.OpeningCreditAmount) ,
                                SUM(D.DebitAmount) ,
                                SUM(D.CreditAmount) ,
                                SUM(D.DebitAmountAccum) ,
                                SUM(D.CreditAmountAccum) ,
                                SUM(D.ClosingDebitAmount) ,
                                SUM(D.ClosingCreditAmount)
                        FROM    @Data D
                        INNER JOIN dbo.Account A ON D.AccountNumber LIKE A.AccountNumber + '%'
                        WHERE   A.Grade <= @MaxAccountGrade
                        GROUP BY A.ID ,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                D.SortOrder
                        HAVING  SUM(D.OpeningDebitAmount) <> 0
                                OR SUM(D.OpeningCreditAmount) <> 0
                                OR SUM(D.DebitAmount) <> 0
                                OR SUM(D.CreditAmount) <> 0
                                OR SUM(D.ClosingDebitAmount) <> 0
                                OR SUM(D.ClosingCreditAmount) <> 0
                 OPTION ( RECOMPILE )
           END
        ELSE
           BEGIN
                 DECLARE @GeneralLedgerP1 TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedgerP1
                        SELECT  Account ,
                                SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmountAccum ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmountAccum
                        FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                        WHERE   GL.PostedDate <= @ToDate
                        GROUP BY Account
                 INSERT @Result
                        SELECT  A.ID ,
                                null DetailByAccountObject ,
                                null AccountObjectType,
                                A.AccountNumber ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade AS Grade ,
                                F.AccountKind,
                                F.SortOrder ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount > 0
                                               ) THEN F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount < 0
                                               ) THEN -1 * F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount ,
                                ( F.DebitAmount ) AS DebitAmount ,
                                ( F.CreditAmount ) AS CreditAmount ,
                                ( F.DebitAmountAccum ) AS DebitAmountAccum ,
                                ( F.CreditAmountAccum ) AS CreditAmountAccum ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                               ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                               ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.ID ,
                                            null DetailByAccountObject,
                                            null AccountObjectType,
                                            A.AccountNumber ,
                                            '' AS CurrencyID ,
                                            0 AS SortOrder ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            0 AS AccountKind
                                  FROM      @GeneralLedgerP1 AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber LIKE A.AccountNumber + '%'
                                  WHERE     A.Grade <= @MaxAccountGrade
                                            AND (
                                                  OpenningDebitAmount <> 0
                                                  OR DebitAmount <> 0
                                                  OR CreditAmount <> 0
                                                )
                                  GROUP BY  A.ID ,
                                            A.AccountNumber      
                                ) AS F
                        INNER JOIN Account A ON F.AccountNumber = A.AccountNumber
                 OPTION ( RECOMPILE )      
           END
        RETURN
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*

*/
ALTER FUNCTION [dbo].[Func_GetB01_DN]
    (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT,
      @IsB01bDNN BIT ,
      @isPrintByYear BIT,
      @PrevFromDate DATETIME,
      @PrevToDate DATETIME
    )
RETURNS @Result TABLE
    (
      ItemID UNIQUEIDENTIFIER ,
      ItemCode NVARCHAR(25) ,
      ItemName NVARCHAR(255) ,
      ItemNameEnglish NVARCHAR(255) ,
      ItemIndex INT ,
      Description NVARCHAR(255) ,
      FormulaType INT ,
      FormulaFrontEnd NVARCHAR(MAX) ,
      Formula XML ,
      Hidden BIT ,
      IsBold BIT ,
      IsItalic BIT ,
      Category INT ,
      SortOrder INT
      ,
      Amount DECIMAL(25, 4) ,
      PrevAmount DECIMAL(25, 4)
    )

    BEGIN 
            DECLARE @AccountingSystem INT	
            SET @AccountingSystem = 48 
    
            DECLARE @SubAccountSystem INT
            SELECT  @SubAccountSystem = 133
	
            DECLARE @ReportID NVARCHAR(100)
            SET @ReportID = '1'

            IF @AccountingSystem = 48
                AND @SubAccountSystem = 133
                AND @IsB01bDNN = 0
                BEGIN

                    SET @ReportID = '7'	
                END

            DECLARE @ItemForeignCurrency UNIQUEIDENTIFIER
            DECLARE @ItemIndex INT
            DECLARE @MainCurrency NVARCHAR(3)
            IF @AccountingSystem = 48
                AND @SubAccountSystem <> 133
                SET @ItemForeignCurrency = '6D6BA7EB-E60A-46ED-A556-E674709F5466'
            ELSE
                SET @ItemForeignCurrency = '00000000-0000-0000-0000-000000000000'	
	
            SET @ItemIndex = ( SELECT TOP 1
                                        ItemIndex
                               FROM     FRTemplate
                               WHERE    ItemID = @ItemForeignCurrency
                             )
            SET @MainCurrency = 'VND'
            DECLARE @tblItem TABLE
                (
                  ItemID UNIQUEIDENTIFIER ,
                  ItemIndex INT ,
                  ItemName NVARCHAR(255) ,
                  OperationSign INT ,
                  OperandString NVARCHAR(255) ,
                  AccountNumberPercent NVARCHAR(25) ,
                  AccountNumber NVARCHAR(25) ,
                  CorrespondingAccountNumber VARCHAR(25) ,
                  IsDetailByAO INT ,
                  AccountKind INT
                )
	
            INSERT  @tblItem
                    SELECT  ItemID ,
                            ItemIndex ,
                            ItemName ,
                            x.r.value('@OperationSign', 'INT') ,
                            RTRIM(LTRIM(x.r.value('@OperandString',
                                                  'nvarchar(255)'))) ,
                            x.r.value('@AccountNumber', 'nvarchar(25)') + '%' ,
                            x.r.value('@AccountNumber', 'nvarchar(25)') ,
                            CASE WHEN x.r.value('@CorrespondingAccountNumber',
                                                'nvarchar(25)') <> ''
                                 THEN x.r.value('@CorrespondingAccountNumber',
                                                'nvarchar(25)') + '%'
                                 ELSE ''
                            END ,
                            CASE WHEN x.r.value('@OperandString',
                                                'nvarchar(255)') LIKE '%ChitietTheoTKvaDoituong'
                                 THEN 1
                                 ELSE CASE WHEN x.r.value('@OperandString',
                                                          'nvarchar(255)') LIKE '%ChitietTheoTK'
                                           THEN 2
                                           ELSE 0
                                      END
                            END ,
                            NULL
                    FROM    dbo.FRTemplate
                            CROSS APPLY Formula.nodes('/root/DetailFormula')
                            AS x ( r )
                    WHERE   AccountingSystem = @AccountingSystem
                            AND ReportID = @ReportID
                            AND FormulaType = 0
                            AND Formula IS NOT NULL
                            AND ItemID <> @ItemForeignCurrency
	
            UPDATE  @tblItem
            SET     AccountKind = A.AccountGroupKind
            FROM    dbo.Account A
            WHERE   A.AccountNumber = [@tblItem].AccountNumber
		
            DECLARE @AccountBalance TABLE
                (
                  AccountNumber NVARCHAR(20) ,
                  AccountCategoryKind INT ,
                  IsDetailByAO INT 
	            )
	 
	 /*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

            INSERT  @AccountBalance
                    SELECT 
		DISTINCT            A.AccountNumber ,
                            A.AccountGroupKind ,
                            B.IsDetailByAO
                    FROM    dbo.Account A
                            INNER JOIN @tblItem B ON A.AccountNumber LIKE B.AccountNumberPercent
	
            DECLARE @Balance TABLE
                (
                  AccountNumber NVARCHAR(20) ,
                  AccountCategoryKind INT ,
                  AccountObjectID UNIQUEIDENTIFIER ,
                  IsDetailByAO INT ,
                  OpeningDebit DECIMAL(25, 4) ,
                  OpeningCredit DECIMAL(25, 4) ,
                  ClosingDebit DECIMAL(25, 4) ,
                  ClosingCredit DECIMAL(25, 4) ,
                  BranchID UNIQUEIDENTIFIER
                )
            DECLARE @GeneralLedger TABLE
                (
                  AccountNumber NVARCHAR(20) ,
                  AccountObjectID UNIQUEIDENTIFIER ,
                  BranchID UNIQUEIDENTIFIER ,
                  IsOPN BIT ,
                  DebitAmount DECIMAL(25, 4) ,
                  CreditAmount DECIMAL(25, 4)
                )
	
            INSERT  INTO @GeneralLedger
                    ( AccountNumber ,
                      AccountObjectID ,
                      BranchID ,
                      IsOPN ,
                      DebitAmount ,
                      CreditAmount
	                )
                    SELECT  GL.Account ,
                            GL.AccountingObjectID ,
                            GL.BranchID ,
                            CASE WHEN GL.PostedDate < @FromDate THEN 1
                                 ELSE 0
                            END ,
                            SUM(DebitAmount) AS DebitAmount ,
                            SUM(CreditAmount) AS CreditAmount
                    FROM    @tbDataGL GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                    WHERE   PostedDate <= @ToDate
                    GROUP BY GL.Account ,
                            GL.AccountingObjectID ,
                            GL.BranchID ,
                            CASE WHEN GL.PostedDate < @FromDate THEN 1
                                 ELSE 0
                            END
	
	
            DECLARE @StartDate DATETIME
			SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'
	 
            DECLARE @GeneralLedger421 TABLE
                (
                  OperandString NVARCHAR(255) ,
                  ClosingAmount DECIMAL(25, 4) ,
                  OpeningAmount DECIMAL(25, 4)
                )
	
	
	
	
	/* 
	 

Đối với báo cáo kỳ khác năm:
*Cuối kỳ: (Dư Có – Dư Nợ) TK 4211 trên sổ cái tính đến Từ ngày -1 + (Dư Có – Dư Nợ) TK 4212 trên sổ cái tính đến Từ ngày -1 
* Đầu kỳ: 
(Dư Có – Dư Nợ) TK 4211 trên sổ cái tính đến từ ngày -1 của kỳ trước liền kề+ (Dư Có – Dư Nợ) TK 4212 trên sổ cái tính đến Từ ngày -1 của kỳ trước liền kề. 
→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, 
VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10) thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4211 – Dư Nợ TK 4211 trên Sổ cái tính đến Từ ngày -1 

	 */
	 /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
            INSERT  INTO @GeneralLedger421
                    ( OperandString ,
                      ClosingAmount ,
                      OpeningAmount
	                )
                    SELECT  N'Solieuchitieu421a_Ky_khac_nam' AS AccountNumber ,
                            ISNULL( SUM(CASE WHEN GL.Account LIKE '4211%'
                                          AND GL.PostedDate < @FromDate
                                     THEN CreditAmount - DebitAmount
                                     ELSE 0
                                END),0)
                         
                            + ISNULL( SUM(CASE WHEN GL.Account LIKE '4212%'
                                            AND GL.PostedDate < @FromDate
                                       THEN CreditAmount -DebitAmount
                                       ELSE 0
                                  END),0)
                           AS ClosingAmount ,
                                  
                            ISNULL( SUM(
									CASE WHEN DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0 ELSE 
										CASE WHEN GL.Account LIKE '4211%'
													  AND GL.PostedDate < @PrevFromDate
												 THEN CreditAmount -DebitAmount
												 ELSE 0
											END
									end		
                                ),0)
                         
                            + SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0
                                       ELSE CASE WHEN GL.Account LIKE '4212%'
                                                      AND GL.PostedDate < @PrevFromDate
                                                 THEN CreditAmount -DebitAmount
                                                 ELSE 0
                                            END
                                  END)
                     
                        +   ISNULL(SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate) 
									  AND GL.Account LIKE '4211%'
												 THEN CASE WHEN GL.PostedDate < @FromDate then CreditAmount - DebitAmount ELSE 0 end
									 ELSE 0
								END)
								,0) AS OpeningAmount                                  
                                  
                    FROM    @tbDataGL GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                    WHERE   PostedDate <= @ToDate
                            AND gl.Account LIKE '421%'
	
	/*
	

Đối với báo cáo kỳ khác năm:
* Cuối kỳ: PS Có TK 4212 trên sổ cái trong kỳ báo cáo (không bao gồm ĐU N4211/Có TK 4212) - PSN TK 4212 trên sổ cái trong kỳ báo cáo (không bao gồm ĐU N4212/Có TK 4211)
* Đầu kỳ: 
PS Có TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4211/Có TK 4212) – PS Nợ TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4212/Có TK 4211)
→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10) thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4212 – Dư Nợ TK 4212 trên Sổ cái tính đến Từ ngày -1 

	*/
	/*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
	
            INSERT  INTO @GeneralLedger421
                    ( OperandString ,
                      ClosingAmount ,
                      OpeningAmount
	                )
                    SELECT  N'Solieuchitieu421b_Ky_khac_nam' AS AccountNumber ,
                           ISNULL( SUM(CASE WHEN GL.Account LIKE '4212%'
                                          AND GL.PostedDate BETWEEN @fromdate AND @ToDate
                                     THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                 THEN 0
                                                 ELSE CreditAmount
                                            END )
                                     ELSE 0
                                END),0)
                            - ISNULL( SUM(CASE WHEN GL.Account LIKE '4212%'
                                            AND GL.PostedDate BETWEEN @Fromdate AND @ToDate
                                       THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                   THEN 0
                                                   ELSE DebitAmount
                                              END )
                                       ELSE 0
                                  END),0) AS ClosingAmount ,
		 /** Đầu kỳ: 
PS Có TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4211/Có TK 4212) – PS Nợ TK 4212 trên sổ cái trong kỳ trước liền kề (không bao gồm ĐU N4212/Có TK 4211)
→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10)
 thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4212 – Dư Nợ TK 4212 trên Sổ cái tính đến Từ ngày -1 
*/
                            ISNULL( SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0
                                     ELSE ( CASE WHEN GL.Account LIKE '4212%'
                                                      AND GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                                 THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                             THEN 0
                                                             ELSE CreditAmount
                                                        END )
                                                 ELSE 0
                                            END )
                                END),0)
                            - ISNULL(  SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate)  THEN 0
                                       ELSE CASE WHEN GL.Account LIKE '4212%'
                                                      AND GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                                 THEN ( CASE WHEN GL.AccountCorresponding LIKE N'4211%'
                                                             THEN 0
                                                             ELSE DebitAmount
                                                        END )
                                                 ELSE 0
                                            END
                                  END),0)
                            + /*nếu không  là kỳ đầu tiên
		→ Nếu Từ ngày của kỳ trùng với ngày bắt đầu kỳ kế toán năm (theo năm tài chính trên hệ thống, 
		VD: kỳ năm tài chính bắt đầu từ 01/10 thì ngày bắt đầu kỳ kế toán năm sẽ là 01/10) thì đầu kỳ sẽ lấy theo công thức: Dư Có TK 4212 – Dư Nợ TK 4212 trên Sổ cái tính đến Từ ngày -1 
		*/
			ISNULL(SUM(CASE WHEN  DAY(@StartDate) = DAY(@FromDate) AND MONTH(@StartDate) =MONTH(@FromDate) 
                          AND GL.Account LIKE '4212%'
                     THEN CASE WHEN GL.PostedDate < @FromDate then CreditAmount - DebitAmount ELSE 0 end
                     ELSE 0
                END),0) AS OpeningAmount
                    FROM    @tbDataGL GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                    WHERE   PostedDate <= @ToDate
                            AND gl.Account LIKE '421%'
	
            INSERT  INTO @Balance
                    SELECT  GL.AccountNumber ,
                            A.AccountCategoryKind ,
                            CASE WHEN A.IsDetailByAO = 1
                                 THEN GL.AccountObjectID
                                 ELSE NULL
                            END AS AccountObjectID ,
                            A.IsDetailByAO ,
                            SUM(CASE WHEN GL.IsOPN = 1
                                     THEN DebitAmount - CreditAmount
                                     ELSE 0
                                END) OpeningDebit ,
                            0 ,
                            SUM(DebitAmount - CreditAmount) ClosingDebit ,
                            0
                            ,
                            CASE WHEN A.IsDetailByAO = 1
                                      AND @IsSimilarBranch = 0
                                 THEN GL.BranchID
                                 ELSE NULL
                            END AS BranchID
                    FROM    @GeneralLedger GL
                            INNER JOIN @AccountBalance A ON GL.AccountNumber = A.AccountNumber
                    GROUP BY GL.AccountNumber ,
                            A.AccountCategoryKind ,
                            CASE WHEN A.IsDetailByAO = 1
                                 THEN GL.AccountObjectID
                                 ELSE NULL
                            END ,
                            A.IsDetailByAO
                            ,
                            CASE WHEN A.IsDetailByAO = 1
                                      AND @IsSimilarBranch = 0
                                 THEN GL.BranchID
                                 ELSE NULL
                            END
            OPTION  ( RECOMPILE )
            UPDATE  @Balance
            SET     OpeningCredit = -OpeningDebit ,
                    OpeningDebit = 0
            WHERE   AccountCategoryKind = 1
                    OR ( AccountCategoryKind NOT IN ( 0, 1 )
                         AND OpeningDebit < 0
                       )
			
            UPDATE  @Balance
            SET     ClosingCredit = -ClosingDebit ,
                    ClosingDebit = 0
            WHERE   AccountCategoryKind = 1
                    OR ( AccountCategoryKind NOT IN ( 0, 1 )
                         AND ClosingDebit < 0
                       )	
	
            DECLARE @tblMasterDetail TABLE
                (
                  ItemID UNIQUEIDENTIFIER ,
                  DetailItemID UNIQUEIDENTIFIER ,
                  OperationSign INT ,
                  Grade INT ,
                  OpeningAmount DECIMAL(25, 4) ,
                  ClosingAmount DECIMAL(25, 4)
                )	
	
	
            INSERT  INTO @tblMasterDetail
                    SELECT  I.ItemID ,
                            NULL ,
                            1 ,
                            -1
                            ,
                            SUM(( CASE I.OperandString
                                    WHEN 'DUNO'
                                    THEN
                                         CASE I.AccountKind
                                           WHEN 1 THEN 0
                                           WHEN 0
                                           THEN I.OpeningDebit
                                                - I.OpeningCredit
                                           ELSE CASE WHEN I.OpeningDebit
                                                          - I.OpeningCredit > 0
                                                     THEN I.OpeningDebit
                                                          - I.OpeningCredit
                                                     ELSE 0
                                                END
                                         END
                                         /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUNO_ky_nam'
                                    THEN 
                                         CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.AccountKind
                                                     WHEN 1 THEN 0
                                                     WHEN 0
                                                     THEN I.OpeningDebit
                                                          - I.OpeningCredit
                                                     ELSE CASE
                                                              WHEN I.OpeningDebit
                                                              - I.OpeningCredit > 0
                                                              THEN I.OpeningDebit
                                                              - I.OpeningCredit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'DUCO'
                                    THEN CASE I.AccountKind
                                           WHEN 0 THEN 0
                                           WHEN 1
                                           THEN I.OpeningCredit
                                                - I.OpeningDebit
                                           ELSE CASE WHEN I.OpeningCredit
                                                          - I.OpeningDebit > 0
                                                     THEN I.OpeningCredit
                                                          - I.OpeningDebit
                                                     ELSE 0
                                                END
                                         END
                                         /* Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUCO_ky_nam'
                                    THEN CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.AccountKind
                                                     WHEN 0 THEN 0
                                                     WHEN 1
                                                     THEN I.OpeningCredit
                                                          - I.OpeningDebit
                                                     ELSE CASE
                                                              WHEN I.OpeningCredit
                                                              - I.OpeningDebit > 0
                                                              THEN I.OpeningCredit
                                                              - I.OpeningDebit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421a_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.OpeningAmount
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421b_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.OpeningAmount
                                              ELSE 0
                                         END
                                         /* - Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    ELSE CASE WHEN LEFT(I.OperandString, 4) = 'DUNO'
                                              THEN I.OpeningDebit
                                              ELSE I.OpeningCredit
                                         END
                                  END ) * I.OperationSign) AS OpeningAmount ,
                            SUM(( CASE I.OperandString
                                    WHEN 'DUNO'
                                    THEN CASE I.Accountkind
                                           WHEN 1 THEN 0
                                           WHEN 0
                                           THEN I.ClosingDebit
                                                - I.ClosingCredit
                                           ELSE CASE WHEN I.ClosingDebit
                                                          - I.ClosingCredit > 0
                                                     THEN I.ClosingDebit
                                                          - I.ClosingCredit
                                                     ELSE 0
                                                END
                                         END
                                         /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUNO_ky_nam'
                                    THEN CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.Accountkind
                                                     WHEN 1 THEN 0
                                                     WHEN 0
                                                     THEN I.ClosingDebit
                                                          - I.ClosingCredit
                                                     ELSE CASE
                                                              WHEN I.ClosingDebit
                                                              - I.ClosingCredit > 0
                                                              THEN I.ClosingDebit
                                                              - I.ClosingCredit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'DUCO'
                                    THEN CASE I.AccountKind
                                           WHEN 0 THEN 0
                                           WHEN 1
                                           THEN I.ClosingCredit
                                                - I.ClosingDebit
                                           ELSE CASE WHEN I.ClosingCredit
                                                          - I.ClosingDebit > 0
                                                     THEN I.ClosingCredit
                                                          - I.ClosingDebit
                                                     ELSE 0
                                                END
                                         END
                                         /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    WHEN 'DUCO_ky_nam'
                                    THEN CASE WHEN @isPrintByYear = 1
                                              THEN CASE I.AccountKind
                                                     WHEN 0 THEN 0
                                                     WHEN 1
                                                     THEN I.ClosingCredit
                                                          - I.ClosingDebit
                                                     ELSE CASE
                                                              WHEN I.ClosingCredit
                                                              - I.ClosingDebit > 0
                                                              THEN I.ClosingCredit
                                                              - I.ClosingDebit
                                                              ELSE 0
                                                          END
                                                   END
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421a_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.ClosingAmount
                                              ELSE 0
                                         END
                                    WHEN 'Solieuchitieu421b_Ky_khac_nam'
                                    THEN CASE WHEN @isPrintByYear = 0
                                              THEN C.ClosingAmount
                                              ELSE 0
                                         END
                                         /* - Thêm điều kiện chọn theo năm hoặc khác năm.*/
                                    ELSE CASE WHEN LEFT(I.OperandString, 4) = 'DUNO'
                                              THEN I.ClosingDebit
                                              ELSE I.ClosingCredit
                                         END
                                  END ) * I.OperationSign) AS ClosingAmount
                    FROM    ( SELECT    I.ItemID ,
                                        I.OperandString ,
                                        I.OperationSign ,
                                        I.AccountNumber ,
                                        I.AccountKind ,
                                        SUM(B.OpeningDebit) AS OpeningDebit ,
                                        SUM(B.OpeningCredit) AS OpeningCredit ,
                                        SUM(B.ClosingDebit) AS ClosingDebit ,
                                        SUM(B.ClosingCredit) AS ClosingCredit
                              FROM      @tblItem I
                                        INNER JOIN @Balance B ON B.AccountNumber LIKE I.AccountNumberPercent
                                                              AND B.IsDetailByAO = I.IsDetailByAO
                              GROUP BY  I.ItemID ,
                                        I.OperandString ,
                                        I.OperationSign ,
                                        I.AccountNumber ,
                                        I.AccountKind
                            ) I
                            /*- Thêm điều kiện chọn theo năm hoặc khác năm.*/
                            LEFT JOIN @GeneralLedger421 C ON I.OperandString = C.OperandString
                    GROUP BY I.ItemID			
	
            INSERT  @tblMasterDetail
                    SELECT  ItemID ,
                            x.r.value('@ItemID', 'NVARCHAR(100)') ,
                            x.r.value('@OperationSign', 'INT') ,
                            0 ,
                            0.0 ,
                            0.0
                    FROM    dbo.FRTemplate
                            CROSS APPLY Formula.nodes('/root/MasterFormula')
                            AS x ( r )
                    WHERE   AccountingSystem = @AccountingSystem
                            AND ReportID = @ReportID
                            AND FormulaType = 1
                            AND Formula IS NOT NULL 
	
	;
            WITH    V ( ItemID, DetailItemID, OpeningAmount, ClosingAmount, OperationSign )
                      AS ( SELECT   ItemID ,
                                    DetailItemID ,
                                    OpeningAmount ,
                                    ClosingAmount ,
                                    OperationSign
                           FROM     @tblMasterDetail
                           WHERE    Grade = -1
                           UNION ALL
                           SELECT   B.ItemID ,
                                    B.DetailItemID ,
                                    V.OpeningAmount ,
                                    V.ClosingAmount ,
                                    B.OperationSign * V.OperationSign AS OperationSign
                           FROM     @tblMasterDetail B ,
                                    V
                           WHERE    B.DetailItemID = V.ItemID
                         )
	INSERT  @Result
            SELECT  FR.ItemID ,
                    FR.ItemCode ,
                    FR.ItemName ,
                    FR.ItemNameEnglish ,
                    FR.ItemIndex ,
                    FR.Description ,
                    FR.FormulaType ,
                    CASE WHEN FR.FormulaType = 1 THEN FR.FormulaFrontEnd
                         ELSE ''
                    END AS FormulaFrontEnd ,
                    CASE WHEN FR.FormulaType = 1 THEN FR.Formula
                         ELSE NULL
                    END AS Formula ,
                    FR.Hidden ,
                    FR.IsBold ,
                    FR.IsItalic ,
                    FR.Category ,
                    -1 AS SortOrder ,
                    ISNULL(X.Amount, 0) AS Amount ,
                    ISNULL(X.PrevAmount, 0) AS PrevAmount
            FROM    ( SELECT    ItemID ,
                                SUM(V.OperationSign * V.OpeningAmount) AS PrevAmount ,
                                SUM(V.OperationSign * V.ClosingAmount) AS Amount
                      FROM      V
                      GROUP BY  ItemID
                    ) AS X
                    RIGHT JOIN FRTemplate FR ON FR.ItemID = X.ItemID
            WHERE   AccountingSystem = @AccountingSystem
                    AND ReportID = @ReportID
	
            RETURN 
        END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_TAX_ListTaxSubmit]
    @inputDate DATETIME
AS
BEGIN          
    
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @inputDate
		/*end add by cuongpv*/
	
	DECLARE @tbluutru TABLE (
		icheck BIT,
		tkThue NVARCHAR(25),
		dienGiai NVARCHAR(512),
		soTienPhaiNop money,
		soTienNopLanNay money,
		soTienDaNop money
    )
    
    DECLARE @icheck BIT
    SET @icheck = 0
    
    DECLARE @ThueGTGTDauRa money
    set @ThueGTGTDauRa = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '33311%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'33311', N'Thuế GTGT đầu ra', @ThueGTGTDauRa)
    
    DECLARE @ThueGTGTHangNK money
    set @ThueGTGTHangNK = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '33312%' 
    and (GL.AccountCorresponding in ('1331', '1332')) and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'33312', N'Thuế GTGT hàng nhập khẩu', @ThueGTGTHangNK)
    
    DECLARE @ThueGTGTHangNKDaNop money
    set @ThueGTGTHangNKDaNop = (select SUM(GL.DebitAmount) from @tbDataGL GL where GL.Account like '33312%' 
    and ((GL.AccountCorresponding like '111%') OR  (GL.AccountCorresponding like '112%')) and GL.PostedDate <= @inputDate)
    
    UPDATE @tbluutru SET soTienPhaiNop = ISNULL(soTienPhaiNop,0) - ISNULL(@ThueGTGTHangNKDaNop,0) WHERE tkThue like '33312%'
    
    DECLARE @ThueTieuThuDB money
    set @ThueTieuThuDB = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '3332%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3332', N'Thuế tiêu thụ đặc biệt', @ThueTieuThuDB)
    
    DECLARE @ThueXuatNhapKhau money
    set @ThueXuatNhapKhau = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '3333%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3333', N'Thuế xuất nhập khẩu', @ThueXuatNhapKhau)
    
    DECLARE @ThueThuNhapDN money
    set @ThueThuNhapDN = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '3334%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3334', N'Thuế thu nhập doanh nghiệp', @ThueThuNhapDN)
    
    DECLARE @ThueThuNhapCN money
    set @ThueThuNhapCN = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '3335%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3335', N'Thuế thu nhập cá nhân', @ThueThuNhapCN)
    
    DECLARE @ThueTaiNguyen money
    set @ThueTaiNguyen = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '3336%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3336', N'Thuế tài nguyên', @ThueTaiNguyen)
    
    DECLARE @ThueNhaDat money
    set @ThueNhaDat = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '3337%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3337', N'Thuế nhà đất, tiền thuê dất', @ThueNhaDat)
    
    DECLARE @ThueBaoVeMT money
    set @ThueBaoVeMT = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '33381%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'33381', N'Thuế bảo vệ môi trường', @ThueBaoVeMT)
    
    DECLARE @ThueKhac money
    set @ThueKhac = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '33382%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'33382', N'Các loại thuế khác', @ThueKhac)
    
    DECLARE @PhiNopKhac money
    set @PhiNopKhac = (select SUM(GL.CreditAmount - GL.DebitAmount) from @tbDataGL GL where GL.Account like '3339%' and GL.PostedDate <= @inputDate)
    INSERT INTO @tbluutru(icheck, tkThue, dienGiai, soTienPhaiNop)
    VALUES(@icheck, N'3339', N'Phí, lệ phí và các khoản nộp khác', @PhiNopKhac)
    
    UPDATE @tbluutru SET soTienNopLanNay = soTienPhaiNop
    
    SELECT icheck, tkThue, dienGiai, soTienPhaiNop, soTienNopLanNay from @tbluutru where soTienPhaiNop>0
    
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoTheoDoiLaiLoTheoHoaDon]
    @FromDate DATETIME,
    @ToDate DATETIME
AS
BEGIN          
    
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
		/*end add by cuongpv*/
	
	DECLARE @tbluutru TABLE (
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		Reason NVARCHAR(512),
		GiaTriHHDV money,
		ChietKhauBan money,
		ChietKhau_TLGG money,
		GiamGia money,
		TraLai money,
		GiaVon money,
		LaiLo money
    )
    INSERT INTO @tbluutru(DetailID, InvoiceDate,InvoiceNo,AccountingObjectID,Reason)
		SELECT DISTINCT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
		FROM @tbDataGL GL 
		WHERE (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail))
		
    UPDATE @tbluutru SET GiaTriHHDV = a.GiaTriHHDV
	FROM (SELECT GL.DetailID as IvID, GL.InvoiceDate as IvDate, GL.InvoiceNo as IvNo, GL.AccountingObjectID as AOID, GL.Reason as Re, SUM(GL.CreditAmount) as GiaTriHHDV
		FROM @tbDataGL GL 
		WHERE (GL.Account like '511%') AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail))
		GROUP BY GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason) a
	WHERE DetailID = a.IvID AND InvoiceDate = a.IvDate AND InvoiceNo = a.IvNo AND AccountingObjectID = a.AOID AND Reason = a.Re
	
	UPDATE @tbluutru SET ChietKhauBan = b.ChietKhauBan
	FROM (SELECT GL.DetailID as IvID, GL.InvoiceDate as IvDate, GL.InvoiceNo as IvNo, GL.AccountingObjectID as AOID, GL.Reason as Re, SUM(GL.DebitAmount) as ChietKhauBan
		FROM @tbDataGL GL 
		WHERE (GL.Account like '511%') AND (GL.TypeID not in (330,340)) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail))
		GROUP BY GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason) b
	WHERE DetailID = b.IvID AND InvoiceDate = b.IvDate AND InvoiceNo = b.IvNo AND AccountingObjectID = b.AOID AND Reason = b.Re
	
	UPDATE @tbluutru SET ChietKhau_TLGG = c.ChietKhau_TLGG
	FROM (SELECT GL.DetailID as IvID, GL.InvoiceDate as IvDate, GL.InvoiceNo as IvNo, GL.AccountingObjectID as AOID, GL.Reason as Re, SUM(GL.CreditAmount) as ChietKhau_TLGG
		FROM @tbDataGL GL
		WHERE (GL.Account like '511%') AND (GL.TypeID in (330,340)) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAInvoiceDetail))
		GROUP BY GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason) c
	WHERE DetailID = c.IvID AND InvoiceDate = c.IvDate AND InvoiceNo = c.IvNo AND AccountingObjectID = c.AOID AND Reason = c.Re
	
	DECLARE @tbDataTraLai TABLE(
		SAInvoiceDetailID UNIQUEIDENTIFIER,
		TraLai money
	)
	INSERT INTO @tbDataTraLai(SAInvoiceDetailID,TraLai)
		SELECT SAR.SAInvoiceDetailID, SUM(GL.DebitAmount - GL.CreditAmount) as TraLai
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON GL.DetailID = SAR.ID
		WHERE (GL.Account like '511%') AND (GL.TypeID=330) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate)
		GROUP BY SAR.SAInvoiceDetailID
	
	UPDATE @tbluutru SET TraLai = TL.TraLai
	FROM (select SAInvoiceDetailID, TraLai from @tbDataTraLai) TL
	WHERE DetailID = TL.SAInvoiceDetailID
	
	UPDATE @tbluutru SET GiaVon = d.GiaVon
	FROM (SELECT GL.DetailID as IvID, GL.InvoiceDate as IvDate, GL.InvoiceNo as IvNo, GL.AccountingObjectID as AOID, GL.Reason as Re, SUM(GL.DebitAmount - GL.CreditAmount) as GiaVon
		FROM @tbDataGL GL 
		WHERE (GL.Account like '632%') AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate)
		GROUP BY GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason) d
	WHERE DetailID = d.IvID AND InvoiceDate = d.IvDate AND InvoiceNo = d.IvNo AND AccountingObjectID = d.AOID AND Reason = d.Re
		
	
	DECLARE @tbDataReturn TABLE (
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		AccountingObjectName NVARCHAR(512),
		Reason NVARCHAR(512),
		GiaTriHHDV money,
		ChietKhauBan money,
		ChietKhau_TLGG money,
		ChietKhau money,
		GiamGia money,
		TraLai money,
		GiaVon money,
		LaiLo money
    )
    
    INSERT INTO @tbDataReturn(InvoiceDate,InvoiceNo,AccountingObjectID,Reason,GiaTriHHDV,ChietKhauBan,ChietKhau_TLGG,TraLai,GiaVon)
    SELECT InvoiceDate,InvoiceNo,AccountingObjectID,Reason,SUM(GiaTriHHDV) as GiaTriHHDV, SUM(ChietKhauBan) as ChietKhauBan,
    SUM(ChietKhau_TLGG) as ChietKhau_TLGG, SUM(TraLai) as TraLai, SUM(GiaVon) as GiaVon
    FROM @tbluutru
    GROUP BY InvoiceDate,InvoiceNo,AccountingObjectID,Reason
    
	INSERT INTO @tbDataReturn(InvoiceDate,InvoiceNo,AccountingObjectID,Reason,GiamGia)
		SELECT GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason, SUM(GL.DebitAmount - GL.CreditAmount) as GiamGia
		FROM @tbDataGL GL 
		WHERE (GL.Account like '511%') AND (GL.TypeID=340) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAReturnDetail))
		GROUP BY GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.Reason
    
    UPDATE @tbDataReturn SET AccountingObjectName = AJ.AccountingObjectName
	FROM ( select ID,AccountingObjectName from AccountingObject) AJ
	where AccountingObjectID = AJ.ID
    
    UPDATE @tbDataReturn SET ChietKhau = (ISNULL(ChietKhauBan,0) - ISNULL(ChietKhau_TLGG,0))
    
    UPDATE @tbDataReturn SET LaiLo = (ISNULL(GiaTriHHDV,0) - (ISNULL(ChietKhau,0)+ISNULL(GiamGia,0)+ISNULL(TraLai,0)+ISNULL(GiaVon,0)))
    
    SELECT InvoiceDate,InvoiceNo,AccountingObjectName,Reason,GiaTriHHDV,ChietKhau,GiamGia,TraLai,GiaVon,LaiLo FROM @tbDataReturn
    WHERE ((ISNULL(GiaTriHHDV,0)>0) OR (ISNULL(ChietKhau,0)>0) OR (ISNULL(GiamGia,0)>0)
			OR (ISNULL(TraLai,0)>0) OR (ISNULL(GiaVon,0)>0) OR (ISNULL(LaiLo,0)>0))
	ORDER BY InvoiceDate,InvoiceNo
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*

*/
ALTER PROCEDURE [dbo].[Proc_SA_GetSalesDiaryBook]
    @FromDate DATETIME ,
    @ToDate DATETIME,
    @IsDisplayNotReceiptOnly BIT
AS 
    BEGIN 
		
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between  @FromDate and @ToDate
		/*end add by cuongpv*/

        DECLARE @listRefType NVARCHAR(MAX)
        IF @IsDisplayNotReceiptOnly = 1 
            SET @listRefType = '3530,3532,3534,3536,'  
        ELSE 
            SET @listRefType = '3530,3531,3532,3534,3535,3536,3537,3538,3540,3541,3542,3543,3544,3545,3550,3551,3552,3553,3554,3555,'

              select ROW_NUMBER() OVER ( ORDER BY a.PostedDate, a.Date, a.No ) AS RowNum ,
			         a.Date as ngay_CTU, 
			         a.PostedDate as ngay_HT, 
					 a.No as So_CTU,
					 a.InvoiceDate as Ngay_HD, 
					 a.InvoiceNo as SO_HD, 
					 d.reason as Dien_giai,
					 SUM(CASE WHEN a.Account IN ( '5111' ) THEN a.CreditAmount
                         ELSE $0
                    END) AS TurnOverAmountInv ,
                    SUM(CASE WHEN a.Account IN ( '5112' ) THEN a.CreditAmount
                         ELSE $0
                    END) AS TurnOverAmountFinishedInv ,
                SUM(CASE WHEN a.Account IN ( '5113' ) THEN a.CreditAmount
                         ELSE $0
                    END) AS TurnOverAmountServiceInv ,
                SUM(CASE WHEN a.Account IN ( '5118' ) THEN a.CreditAmount
                         ELSE $0
                    END)AS TurnOverAmountOther,
                SUM(CASE WHEN a.Account IN ( '5111' ) THEN a.DebitAmount
                         ELSE $0
                    END) AS DiscountAmount ,
				SUM(CASE WHEN (a.typeID = '330' and a.Account IN ( '5111' )) THEN a.DebitAmount
                         ELSE $0
                    END) AS ReturnAmount,
                SUM(CASE WHEN (a.typeID = '340' and a.Account IN ( '5111' )) THEN a.DebitAmount
                         ELSE $0
                    END) AS ReduceAmount,
			    SUM(b.VATAmount) AS VATAmount,
				ao.AccountingObjectCode as CustomerCode, 
				ao.AccountingObjectName as CustomerName 
				INTO    #Result
				from @tbDataGL a /*edit by cuongpv GeneralLedger -> @tbDataGL*/
				join SAInvoiceDetail b on a.DetailID = b.ID 
				join MaterialGoods c on b.MaterialGoodsID = c.ID
				join SAInvoice d on a.ReferenceID = d.ID 
				join Type e on a.TypeID = e.ID
				join AccountingObject ao on a.AccountingObjectID = ao.ID 
				where a.PostedDate between  @FromDate and @ToDate
				group by a.Date,  a.PostedDate, a.No ,a.InvoiceDate , a.InvoiceNo , d.reason,
				ao.AccountingObjectCode,ao.AccountingObjectName;

       select	RowNum ,
				ngay_CTU, 
				ngay_HT, 
				So_CTU,
				Ngay_HD, 
				SO_HD, 
				Dien_giai,
				TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther as SumTurnOver,
				TurnOverAmountInv ,
				TurnOverAmountFinishedInv ,
                TurnOverAmountServiceInv ,
                TurnOverAmountOther,
                DiscountAmount ,
				ReturnAmount,
                ReduceAmount,
				(TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther) - (DiscountAmount + ReturnAmount + ReduceAmount) as TurnOverPure,
				CustomerCode, 
				CustomerName 
   from #Result 
   where TurnOverAmountInv + TurnOverAmountFinishedInv + TurnOverAmountServiceInv +
				TurnOverAmountOther <> 0
   ORDER BY RowNum   
    
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_PO_DiaryBook]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER = NULL ,
    @IncludeDependentBranch BIT = 0 ,
    @IsNotPaid BIT = 0
AS 
    BEGIN
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
		/*end add by cuongpv*/
		
        select
		 DetailID,
		 TypeID,
		 PostedDate,
		 RefDate,
		 RefNo,
		 InvoiceDate,
		 InvoiceNo,
		 Description,
		 GoodsAmount,
		 EquipmentAmount,
		 AnotherAccount,
		 AnotherAmount,
		 sum(GoodsAmount) + sum(EquipmentAmount) + sum(AnotherAmount) as PaymentableAmount
		from
		(select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description , sum(DebitAmount) as GoodsAmount, 
		0 as EquipmentAmount, null as AnotherAccount, 0 as AnotherAmount,a.TypeID
		  from @tbDataGL a join PPInvoiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '156' and AccountCorresponding not in ('3333', '3332') group by  a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description , sum(DebitAmount) as GoodsAmount,
		0 as EquipmentAmount, null as AnotherAccount, 0 as AnotherAmount,a.TypeID
		  from @tbDataGL a join PPServiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '156' and AccountCorresponding not in ('3333', '3332') group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description, sum(DebitAmount) as GoodsAmount,
		0 as EquipmentAmount, null as AnotherAccount, 0 as AnotherAmount,a.TypeID
		  from @tbDataGL a join TIIncrementDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '156' and AccountCorresponding not in ('3333', '3332') group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID

		union all

		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount, sum(DebitAmount) as EquipmentAmount,
		null as AnotherAccount, 0 as AnotherAmount,a.TypeID
		  from @tbDataGL a join PPInvoiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '152' and AccountCorresponding not in ('3333', '3332') group by  a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount, sum(DebitAmount) as EquipmentAmount,
		null as AnotherAccount, 0 as AnotherAmount,a.TypeID
		  from @tbDataGL a join PPServiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '152' and AccountCorresponding not in ('3333', '3332') group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description,0 as GoodsAmount , sum(DebitAmount) as EquipmentAmount,
		null as AnotherAccount, 0 as AnotherAmount,a.TypeID
		  from @tbDataGL a join TIIncrementDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account =  '152' and AccountCorresponding not in ('3333', '3332') group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , b.Description, a.DetailID,a.TypeID
		union all

		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description,0 as GoodsAmount,0 as EquipmentAmount , a.Account as AnotherAccount, sum(DebitAmount) as AnotherAmount,a.TypeID
		  from @tbDataGL a join PPInvoiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account in ('153','154','242') and AccountCorresponding not in ('3333', '3332') and AccountCorresponding in ('331', '111','112')
		group by  a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , a.Account, b.Description, a.DetailID,a.TypeID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount,0 as EquipmentAmount, a.Account as AnotherAccount, sum(DebitAmount) as AnotherAmount,a.TypeID
		  from @tbDataGL a join PPServiceDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate  /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		  and a.Account in ('153','154','242') and AccountCorresponding not in ('3333', '3332') and AccountCorresponding in ('331', '111','112')
		group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , a.Account, b.Description, a.DetailID,a.TypeID
		union all
		select a.DetailID,a.date as RefDate, a.PostedDate , a.No as RefNo, a.InvoiceDate , a.InvoiceNo , b.Description ,0 as GoodsAmount,0 as EquipmentAmount, a.Account as AnotherAccount,sum(DebitAmount) as AnotherAmount,a.TypeID
		  from @tbDataGL a join TIIncrementDetail b on a.DetailID = b.ID where a.PostedDate BETWEEN @FromDate AND @ToDate /*edit by cuongpv GeneralLedger ->  @tbDataGL*/
		and a.Account in ('153','154','242') and AccountCorresponding not in ('3333', '3332') and AccountCorresponding in ('331', '111','112')
		group by a.date, a.PostedDate, a.No , a.InvoiceDate , a.InvoiceNo , a.Account, b.Description, a.DetailID,a.TypeID) t
group by 
         DetailID,
		 TypeID,
		 PostedDate,
		 RefDate,
		 RefNo,
		 InvoiceDate,
		 InvoiceNo,
		 Description,
		 GoodsAmount,
		 EquipmentAmount,
		 AnotherAccount,
		 AnotherAmount
ORDER BY PostedDate ,
               RefDate ,RefNo           
   


           
                              
                              
                              
   
    END
    


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

ALTER PROCEDURE [dbo].[Proc_GLR_GetS03a2_DN]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @CurrencyID NVARCHAR(10) ,
    @AccountNumber NVARCHAR(25) ,
    @BankAccountID UNIQUEIDENTIFIER ,
    @IsSimilarSum BIT
AS
    BEGIN
        SET NOCOUNT ON;     
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate BETWEEN @FromDate AND @ToDate
		/*end add by cuongpv*/
		
		CREATE TABLE #Result
            (
              BranchName NVARCHAR(255)  ,
              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefNo NVARCHAR(22) ,
              RefDate DATETIME ,
              [Description] NVARCHAR(255) ,
              AccountNumber NVARCHAR(20) ,
              CorrespondingAccountNumber NVARCHAR(20) ,
              Amount MONEY ,
              Col2 MONEY DEFAULT 0 ,
              Col3 MONEY DEFAULT 0 ,
              Col4 MONEY DEFAULT 0 ,
              Col5 MONEY DEFAULT 0 ,
              Col6 MONEY DEFAULT 0 ,
              Col7 MONEY DEFAULT 0 ,
              ColOtherAccount NVARCHAR(20) ,
              AccountNumberList NVARCHAR(120) ,
              /*Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
              SortOrder INT,
              DetailPostOrder int    
            )
                     
         /*tài khoản ngân hàng khác - lấy các phát sinh không chọn tài khoản ngân hàng*/		
        DECLARE @BankAccountOther UNIQUEIDENTIFIER
        SET @BankAccountOther = '12345678-2222-48B8-AE4B-5CF7FA7FB3F5'
        
        INSERT  #Result
                ( BranchName ,
                  RefType ,
                  RefID ,
                  PostedDate ,
                  RefNo ,
                  RefDate ,
                  Description ,
                  AccountNumber ,
                  CorrespondingAccountNumber ,
                  Amount,
                    SortOrder,
                  DetailPostOrder
                )
                SELECT  null as BranchName ,
                        GL.TypeID ,
                        GL.ReferenceID ,
                        GL.PostedDate ,
                        GL.RefNo ,
                        GL.RefDate ,
                        CASE WHEN @IsSimilarSum = 1 THEN GL.Reason
                             ELSE ISNULL(GL.[Description], GL.Reason)
                        END ,
                        @AccountNumber AS AccountNumber ,
                        GL.AccountCorresponding ,
                        GL.CreditAmount AS Amount,
                          /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        GL.OrderPriority,
                        null
                FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                        INNER JOIN dbo.Account AS A ON GL.Account = A.AccountNumber
                WHERE   PostedDate BETWEEN @FromDate AND @ToDate
                        AND GL.Account LIKE @AccountNumber + '%'
                        AND GL.CreditAmount <> 0
                        AND ( @CurrencyID IS NULL
                              OR GL.CurrencyID = @CurrencyID
                            )
                        AND ( @BankAccountID IS NULL
                              OR ( (GL.BankAccountDetailID = @BankAccountID
                                   AND @BankAccountID <> @BankAccountOther )
                                 )
                         
                              OR ( (GL.BankAccountDetailID IS  NULL
                                   AND @BankAccountID = @BankAccountOther )
                                 )
                            )
        IF EXISTS ( SELECT TOP 1
                            *
                    FROM    #Result )
            BEGIN
                DECLARE @AccountNumberAdded NVARCHAR(50)
                DECLARE @AccountNumberList NVARCHAR(MAX)
                DECLARE @Updatesql1 NVARCHAR(MAX)
                DECLARE @Updatesql2 NVARCHAR(MAX)
                DECLARE @tblName NVARCHAR(20)
                DECLARE @colNumber INT
                
                DECLARE @CoresAccountNumberList NVARCHAR(MAX)
        
                SET @tblName = '#Result'
                SET @Updatesql1 = ''
                SET @Updatesql2 = ''
                SET @colNumber = 2
                SET @AccountNumberList = @AccountNumber + ','
                SET @CoresAccountNumberList = ''
		
                DECLARE curAcc CURSOR FAST_FORWARD READ_ONLY
                FOR
                    SELECT DISTINCT TOP 4
                            R.CorrespondingAccountNumber
                    FROM    #Result AS R
                    WHERE   ISNULL(R.CorrespondingAccountNumber, '') <> ''
                    ORDER BY R.CorrespondingAccountNumber
                OPEN curAcc

                FETCH NEXT FROM curAcc INTO @AccountNumberAdded

                WHILE @@FETCH_STATUS = 0
                    BEGIN
                        IF @Updatesql1 <> ''
                            SET @Updatesql1 = @Updatesql1 + ', '
                        SET @Updatesql1 = @Updatesql1 + '[col'
                            + CONVERT(NVARCHAR(10), @colNumber) + ']'
                            + ' = (CASE WHEN ISNULL(CorrespondingAccountNumber,'''') = '''
                            + @AccountNumberAdded
                            + ''' THEN Amount Else 0 END)'                
                        IF @Updatesql2 <> ''
                            SET @Updatesql2 = @Updatesql2 + ' AND '
                        SET @Updatesql2 = @Updatesql2
                            + 'ISNULL(CorrespondingAccountNumber,'''') <> '''
                            + @AccountNumberAdded + ''' '
                        SET @AccountNumberList = @AccountNumberList
                            + @AccountNumberAdded + ','
                            
                            
                        SET @CoresAccountNumberList = @CoresAccountNumberList
                            + ',''' + @AccountNumberAdded + ''''
                        SET @colNumber = @colNumber + 1                          
                        FETCH NEXT FROM curAcc INTO @AccountNumberAdded
                
                    END
                CLOSE curAcc
                DEALLOCATE curAcc
                
			
                SET @Updatesql1 = 'UPDATE #Result SET ' + @Updatesql1
                    +
                    ', [col7] = (CASE WHEN ' + @Updatesql2
                    + ' THEN Amount ELSE 0 END)'
                    +
                    ', [ColOtherAccount] = (CASE WHEN ' + @Updatesql2
                    + ' THEN CorrespondingAccountNumber END)'
                 
       
                EXEC (@Updatesql1)
                UPDATE  #Result
                SET     AccountNumberList = @AccountNumberList          
            END
        
        IF @IsSimilarSum = 1
            BEGIN
                SELECT  BranchName ,
                        RefType ,
                        RefID ,
                        PostedDate ,
                        RefNo ,
                        RefDate ,
                        [Description] ,
                        AccountNumber ,
                        SUM(Amount) AS Amount ,
                        SUM(Col2) AS Col2 ,
                        SUM(Col3) AS Col3 ,
                        SUM(Col4) AS Col4 ,
                        SUM(Col5) AS Col5 ,
                        SUM(Col6) AS Col6 ,
                        SUM(Col7) AS Col7 ,
                        /*ISNULL(ColOtherAccount,
                               Temp1.CorrespondingAccountNumber) AS ColOtherAccount , comment by cuongpv*/
						ColOtherAccount, /*add by cuongpv*/
                        AccountNumberList
                FROM    #Result AS R
                        /*OUTER APPLY ( SELECT TOP 1
                                                CorrespondingAccountNumber
                                      FROM      #Result AS R1
                                      WHERE     R1.RefID = R.RefID
                                                AND ColOtherAccount NOT IN (
                                                STUFF(@CoresAccountNumberList,
                                                      1, 1, '') )
                                      ORDER BY  CorrespondingAccountNumber
                                    ) AS Temp1 comment by cuongpv*/
                GROUP BY BranchName ,
                        RefType ,
                        RefID ,
                        PostedDate ,
                        RefNo ,
                        RefDate ,
                        [Description] ,
                        AccountNumber ,
                        /*ISNULL(ColOtherAccount,
                               Temp1.CorrespondingAccountNumber) , comment by cuongpv*/
						ColOtherAccount, /*add by cuongpv*/
                        AccountNumberList
                  /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */       
                ORDER BY 
						R.PostedDate ,
                        R.RefDate ,
                        R.RefNo      
            END
        ELSE
            BEGIN
            
                SELECT  *
                FROM    #Result AS R
                ORDER BY R.PostedDate ,
                        R.RefDate ,
                        R.RefNo ,
                          /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        R.SortOrder,
                        R.DetailPostOrder
            END

        DROP TABLE #Result
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

ALTER PROCEDURE [dbo].[Proc_GLR_GetS03a1_DN]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @CurrencyID NVARCHAR(10) ,
    @AccountNumber NVARCHAR(25) ,
    @BankAccountID UNIQUEIDENTIFIER ,
    @IsSimilarSum BIT
AS
    BEGIN
        SET NOCOUNT ON;
		
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate BETWEEN @FromDate AND @ToDate
		/*end add by cuongpv*/

        CREATE TABLE #Result
            (
			
              BranchName NVARCHAR(255)  ,
              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefNo NVARCHAR(22) ,
              RefDate DATETIME ,
              [Description] NVARCHAR(255) ,
              AccountNumber NVARCHAR(20) ,
              CorrespondingAccountNumber NVARCHAR(20) ,
              Amount MONEY ,
              Col2 MONEY DEFAULT 0 ,
              Col3 MONEY DEFAULT 0 ,
              Col4 MONEY DEFAULT 0 ,
              Col5 MONEY DEFAULT 0 ,
              Col6 MONEY DEFAULT 0 ,
              Col7 MONEY DEFAULT 0 ,
              ColOtherAccount NVARCHAR(20) ,
              AccountNumberList NVARCHAR(120) ,
                  /*sửa lỗi 139606 -Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
              SortOrder INT,
              DetailPostOrder int
              
            )
                     
         /*tài khoản ngân hàng khác - lấy các phát sinh không chọn tài khoản ngân hàng*/		
        DECLARE @BankAccountOther UNIQUEIDENTIFIER
        SET @BankAccountOther = '12345678-2222-48B8-AE4B-5CF7FA7FB3F5'
        
        INSERT  #Result
                ( BranchName ,
                  RefType ,
                  RefID ,
                  PostedDate ,
                  RefNo ,
                  RefDate ,
                  Description ,
                  AccountNumber ,
                  CorrespondingAccountNumber ,
                  Amount,
                  SortOrder,
                  DetailPostOrder
                )
                SELECT  null as BranchName ,
                        GL.TypeID ,
                        GL.ReferenceID ,
                        GL.PostedDate ,
                        GL.RefNo ,
                        GL.RefDate ,
                        CASE WHEN @IsSimilarSum = 1 THEN GL.Reason
                             ELSE ISNULL(GL.[Description], GL.Reason)
                        END ,
                        @AccountNumber AS AccountNumber ,
                        GL.AccountCorresponding ,
                        GL.DebitAmount AS Amount,
                         /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        GL.OrderPriority,
                        null
                FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                        INNER JOIN dbo.Account AS A ON GL.Account = A.AccountNumber
                WHERE   PostedDate BETWEEN @FromDate AND @ToDate
                        AND GL.Account LIKE @AccountNumber + '%'
                        AND GL.DebitAmount <> 0
                        AND ( @CurrencyID IS NULL
                              OR GL.CurrencyID = @CurrencyID
                            )
                        AND ( @BankAccountID IS NULL
                              OR ( (GL.BankAccountDetailID = @BankAccountID
                                   AND @BankAccountID <> @BankAccountOther )
                                 )
                         
                              OR ( (GL.BankAccountDetailID IS  NULL
                                   AND @BankAccountID = @BankAccountOther )
                                 )
                            )

        IF EXISTS ( SELECT TOP 1
                            *
                    FROM    #Result )
            BEGIN
                DECLARE @AccountNumberAdded NVARCHAR(50)
                DECLARE @AccountNumberList NVARCHAR(MAX)
                DECLARE @Updatesql1 NVARCHAR(MAX)
                DECLARE @Updatesql2 NVARCHAR(MAX)
                DECLARE @tblName NVARCHAR(20)
                DECLARE @colNumber INT
                
                DECLARE @CoresAccountNumberList NVARCHAR(MAX)
        
                SET @tblName = '#Result'
                SET @Updatesql1 = ''
                SET @Updatesql2 = ''
                SET @colNumber = 2
                SET @AccountNumberList = @AccountNumber + ','
                SET @CoresAccountNumberList = ''
		
                DECLARE curAcc CURSOR FAST_FORWARD READ_ONLY
                FOR
                    SELECT DISTINCT TOP 4
                            R.CorrespondingAccountNumber
                    FROM    #Result AS R
                    WHERE   ISNULL(R.CorrespondingAccountNumber, '') <> ''
                    ORDER BY R.CorrespondingAccountNumber
                OPEN curAcc

                FETCH NEXT FROM curAcc INTO @AccountNumberAdded

                WHILE @@FETCH_STATUS = 0
                    BEGIN
                        IF @Updatesql1 <> ''
                            SET @Updatesql1 = @Updatesql1 + ', '
                        SET @Updatesql1 = @Updatesql1 + '[col'
                            + CONVERT(NVARCHAR(10), @colNumber) + ']'
                            + ' = (CASE WHEN ISNULL(CorrespondingAccountNumber,'''') = '''
                            + @AccountNumberAdded
                            + ''' THEN Amount Else 0 END)'                
                        IF @Updatesql2 <> ''
                            SET @Updatesql2 = @Updatesql2 + ' AND '
                        SET @Updatesql2 = @Updatesql2
                            + 'ISNULL(CorrespondingAccountNumber,'''') <> '''
                            + @AccountNumberAdded + ''' '
                        SET @AccountNumberList = @AccountNumberList
                            + @AccountNumberAdded + ','
                            
                            
                        SET @CoresAccountNumberList = @CoresAccountNumberList
                            + ',''' + @AccountNumberAdded + ''''
                        SET @colNumber = @colNumber + 1                          
                        FETCH NEXT FROM curAcc INTO @AccountNumberAdded
                
                    END
                CLOSE curAcc
                DEALLOCATE curAcc
                
			
                SET @Updatesql1 = 'UPDATE #Result SET ' + @Updatesql1
                    +
                    ', [col7] = (CASE WHEN ' + @Updatesql2
                    + ' THEN Amount ELSE 0 END)'
                    +
                    ', [ColOtherAccount] = (CASE WHEN ' + @Updatesql2
                    + ' THEN CorrespondingAccountNumber END)'
                 
        
                EXEC (@Updatesql1)
                UPDATE  #Result
                SET     AccountNumberList = @AccountNumberList          
            END
        
        IF @IsSimilarSum = 1
            BEGIN
                SELECT  BranchName ,
                        RefType ,
                        RefID ,
                        PostedDate ,
                        RefNo ,
                        RefDate ,
                        [Description] ,
                        AccountNumber ,
                        SUM(Amount) AS Amount ,
                        SUM(Col2) AS Col2 ,
                        SUM(Col3) AS Col3 ,
                        SUM(Col4) AS Col4 ,
                        SUM(Col5) AS Col5 ,
                        SUM(Col6) AS Col6 ,
                        SUM(Col7) AS Col7 ,
                        /*ISNULL(ColOtherAccount,
                               Temp1.CorrespondingAccountNumber) AS ColOtherAccount , comment by cuongpv*/
						ColOtherAccount, /*add by cuongpv*/
                        AccountNumberList
                FROM    #Result AS R
                        /*OUTER APPLY ( SELECT TOP 1
                                                CorrespondingAccountNumber
                                      FROM      #Result AS R1
                                      WHERE     R1.RefID = R.RefID
                                                AND ColOtherAccount NOT IN (
                                                STUFF(@CoresAccountNumberList,
                                                      1, 1, '') )
                                      ORDER BY  CorrespondingAccountNumber
                                    ) AS Temp1 comment by cuongpv*/
                GROUP BY BranchName ,
                        RefType ,
                        RefID ,
                        PostedDate ,
                        RefNo ,
                        RefDate ,
                        [Description] ,
                        AccountNumber ,
                        /*ISNULL(ColOtherAccount,
                               Temp1.CorrespondingAccountNumber) , commnet by cuongpv*/
						ColOtherAccount, /*add by cuongpv*/
                        AccountNumberList
                     /* -Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */       
                ORDER BY 
						R.PostedDate ,
                        R.RefDate ,
                        R.RefNo             
                       
                                    
            
            END
        ELSE
            BEGIN
            
            
                SELECT  *
                FROM    #Result AS R                                                
                ORDER BY R.PostedDate ,
                        R.RefDate ,
                        R.RefNo ,
                        /*-Nhật ký thu tiền, chi tiền: Sắp xếp không đúng theo thứ tự nhập trên chứng từ */
                        R.SortOrder,
                        R.DetailPostOrder
            END

        DROP TABLE #Result
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

ALTER PROCEDURE [dbo].[Proc_GL_GeneralDiaryBook_S03a]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @GroupTheSameItem BIT ,/*cộng gộp các bút toán giống nhau*/
    @IsShowAccumAmount BIT
AS 
    BEGIN
    
       DECLARE @PrevFromDate AS DATETIME
       SET @PrevFromDate = DATEADD(MILLISECOND, -10, @FromDate)
       DECLARE @StartDate DATETIME
        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'

		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        IF @GroupTheSameItem = 1 
            BEGIN
			select * from
            (SELECT -1 AS RowNum,
				CAST(0 AS BIT) AS IsSummaryRow,
				CAST (1 AS BIT) AS IsBold ,
                NULL AS [RefID],
				NULL AS RefType,
				NULL AS PostedDate,
				NULL AS RefDate,
              NULL AS [RefNo],
              NULL AS RefNo1 ,
              NULL AS RefOrder ,
              NULL AS [InvDate]  ,
              NULL AS [InvNo]  ,
              N'Số lũy kế kỳ trước chuyển sang' AS [JournalMemo] ,
              /*15.09.2017 bổ sung trường diễn giải thông tin chung cr 137228*/              
              '' AS JournalMemoMaster ,
              NULL AS [AccountNumber],
              NULL AS [CorrespondingAccountNumber],
              NULL AS [RefTypeName],
              NULL AS AccountObjectCode,
              NULL AS AccountObjectName ,
              NULL AS EmployeeCode ,
              NULL AS EmployeeName ,
              NULL AS ExpenseItemCode ,
              NULL AS ExpenseItemName ,
              NULL AS JobCode ,
              NULL AS JobName ,
              NULL AS ProjectWorkCode ,
              NULL AS ProjectWorkName,
              NULL AS OrderNo ,
              NULL AS [PUContractCode]  ,
              NULL AS [ContractCode]  ,
              NULL AS ListItemCode ,
              NULL AS ListItemName,
              SUM(GL.DebitAmount) AS DebitAmount ,
              SUM(GL.CreditAmount) AS CreditAmount,
              NULL AS BranchName,
              NULL AS OrganizationUnitCode,
              NULL AS OrganizationUnitName,
              NULL AS MasterCustomField1,
              NULL AS MasterCustomField2,
              NULL AS MasterCustomField3,
              NULL AS MasterCustomField4 ,
              NULL AS MasterCustomField5 ,
              NULL AS MasterCustomField6 ,
              NULL AS MasterCustomField7 ,
              NULL AS MasterCustomField8 ,
              NULL AS MasterCustomField9 ,
              NULL AS MasterCustomField10,
              NULL AS CustomField1 ,
              NULL AS CustomField2 ,
              NULL AS CustomField3 ,
              NULL AS CustomField4 ,
              NULL AS CustomField5 ,
              NULL AS CustomField6 ,
              NULL AS CustomField7 ,
              NULL AS CustomField8 ,
              NULL AS CustomField9 ,
              NULL AS CustomField10,
              NULL AS UnResonableCost ,
              NULL AS Sort,
              NULL AS RefIDSort	,
              NULL AS SortOrder 
              ,NULL AS DetailPostOrder
			  ,null as OrderPriority
			  ,null as OrderTotal
		FROM    @tbDataGL GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
        WHERE	
			(@IsShowAccumAmount = 1	AND @StartDate < @FromDate)
                AND GL.PostedDate BETWEEN @StartDate
                                    AND     @PrevFromDate
                AND GL.AccountCorresponding IS NOT NULL
                AND GL.AccountCorresponding <> ''
        HAVING  SUM(GL.[DebitAmount]) <> 0
                OR SUM(GL.[CreditAmount]) <> 0

		UNION ALL
        SELECT  ROW_NUMBER() OVER ( ORDER BY gl.PostedDate, gl.RefDate
						 		,CASE WHEN GL.TypeID = 4012 THEN 1 ELSE 0 END
								, GL.RefNo , GL.TypeID ) AS RowNum ,
                        CAST (1 AS BIT) AS IsSummaryRow ,
						CAST(0 AS BIT) AS IsBold ,
                        GL.[ReferenceID] ,
                        GL.[TypeID] ,
                        GL.[PostedDate] ,
                        GL.[RefDate] ,
                        GL.No,
                        GL.No ,
                        NULL AS RefOrder ,
                        GL.[InvoiceDate] ,
                        GL.[InvoiceNo] ,
                        GL.[Reason] AS JournalMemo,/*edit by cuongpv Description -> Reason*/
                        /*bổ sung trường diễn giải thông tin chung cr 137228*/              
						GL.Reason AS JournalMemoMaster ,/*edit by cuongpv Description -> Reason*/
                        GL.[Account] ,
                        GL.[AccountCorresponding] ,
						'' AS [RefTypeName],
                        '' AS AccountObjectCode ,
                        '' AS AccountObjectName ,
                        '' AS EmployeeCode ,
                        '' AS EmployeeName ,
                        '' AS ExpenseItemCode ,
                        '' AS ExpenseItemName ,
                        '' AS JobCode ,
                        '' AS JobName ,
                        '' AS ProjectWorkCode ,
                        '' AS ProjectWorkName ,
                        '' AS OrderNo ,
                        '' AS [PUContractCode] ,
                        '' AS [ContractCode] ,
                        '' AS ListItemCode ,
                        '' AS ListItemName ,
                        SUM(GL.[DebitAmount]) AS DebitAmount ,
                        null AS CreditAmount ,
                        '' as BranchName,
                        '' AS OrganizationUnitCode ,
                        '' AS OrganizationUnitName ,
                        '' AS MasterCustomField1 ,
                        '' AS MasterCustomField2 ,
                        '' AS MasterCustomField3 ,
                        '' AS MasterCustomField4 ,
                        '' AS MasterCustomField5 ,
                        '' AS MasterCustomField6 ,
                        '' AS MasterCustomField7 ,
                        '' AS MasterCustomField8 ,
                        '' AS MasterCustomField9 ,
                        '' AS MasterCustomField10 ,
                        '' AS CustomField1 ,
                        '' AS CustomField2 ,
                        '' AS CustomField3 ,
                        '' AS CustomField4 ,
                        '' AS CustomField5 ,
                        '' AS CustomField6 ,
                        '' AS CustomField7 ,
                        '' AS CustomField8 ,
                        '' AS CustomField9 ,
                        '' AS CustomField10 ,
                        '' AS UnResonableCost ,
                        CASE WHEN SUM(DebitAmount) <> 0
                                THEN Gl.Account
                                    + AccountCorresponding
                                WHEN SUM(CreditAmount) <> 0
                                THEN gl.AccountCorresponding
                                    + GL.Account
                        END AS Sort ,
                        GL.ReferenceID
                        AS RefIDSort,
                        0 AS SortOrder,
                        0 AS DetailPostOrder,
						MAX(GL.OrderPriority) as OrderPriority,
                        SUM(DebitAmount) as OrderTotal
                FROM      @tbDataGL GL /*edit by cuongpv [dbo].[GeneralLedger] -> @tbDataGL*/
                        LEFT JOIN dbo.AccountingObject ao ON ao.ID = GL.AccountingObjectID
                WHERE     gl.PostedDate BETWEEN @FromDate AND @ToDate
                        AND ISNULL(gl.AccountCorresponding,'') <> ''
                GROUP BY  GL.[ReferenceID] ,
                        GL.[TypeID] ,
                        GL.[PostedDate] ,
                        GL.[RefDate] ,
                        GL.[RefNo] ,
                        GL.No ,
                        GL.[InvoiceDate] ,
                        GL.[InvoiceNo] ,
                        GL.[Reason] ,
                        GL.[Account] ,
                        GL.[AccountCorresponding]
						/*gl.OrderPriority comment by cuongpv*/
                                            
                HAVING    SUM(GL.[DebitAmount]) <> 0 /*edit by cuongpv bo (OR SUM(GL.[CreditAmount]) <> 0)*/
			UNION ALL 
			/*add by cuongpv*/
			SELECT  ROW_NUMBER() OVER ( ORDER BY gl.PostedDate, gl.RefDate
						 		,CASE WHEN GL.TypeID = 4012 THEN 1 ELSE 0 END
								, GL.RefNo , GL.TypeID ) AS RowNum ,
                        CAST (1 AS BIT) AS IsSummaryRow ,
						CAST(0 AS BIT) AS IsBold ,
                        GL.[ReferenceID] ,
                        GL.[TypeID] ,
                        GL.[PostedDate] ,
                        GL.[RefDate] ,
                        GL.No,
                        GL.No ,
                        NULL AS RefOrder ,
                        GL.[InvoiceDate] ,
                        GL.[InvoiceNo] ,
                        GL.[Reason] AS JournalMemo, /*edit by cuongpv Description -> Reason*/
                        /*bổ sung trường diễn giải thông tin chung cr 137228*/              
						GL.Reason AS JournalMemoMaster ,/*edit by cuongpv Description -> Reason*/
                        GL.[Account] ,
                        GL.[AccountCorresponding] ,
						'' AS [RefTypeName],
                        '' AS AccountObjectCode ,
                        '' AS AccountObjectName ,
                        '' AS EmployeeCode ,
                        '' AS EmployeeName ,
                        '' AS ExpenseItemCode ,
                        '' AS ExpenseItemName ,
                        '' AS JobCode ,
                        '' AS JobName ,
                        '' AS ProjectWorkCode ,
                        '' AS ProjectWorkName ,
                        '' AS OrderNo ,
                        '' AS [PUContractCode] ,
                        '' AS [ContractCode] ,
                        '' AS ListItemCode ,
                        '' AS ListItemName ,
                        null AS DebitAmount ,
                        SUM(GL.[CreditAmount]) AS CreditAmount ,
                        '' as BranchName,
                        '' AS OrganizationUnitCode ,
                        '' AS OrganizationUnitName ,
                        '' AS MasterCustomField1 ,
                        '' AS MasterCustomField2 ,
                        '' AS MasterCustomField3 ,
                        '' AS MasterCustomField4 ,
                        '' AS MasterCustomField5 ,
                        '' AS MasterCustomField6 ,
                        '' AS MasterCustomField7 ,
                        '' AS MasterCustomField8 ,
                        '' AS MasterCustomField9 ,
                        '' AS MasterCustomField10 ,
                        '' AS CustomField1 ,
                        '' AS CustomField2 ,
                        '' AS CustomField3 ,
                        '' AS CustomField4 ,
                        '' AS CustomField5 ,
                        '' AS CustomField6 ,
                        '' AS CustomField7 ,
                        '' AS CustomField8 ,
                        '' AS CustomField9 ,
                        '' AS CustomField10 ,
                        '' AS UnResonableCost ,
                        CASE WHEN SUM(DebitAmount) <> 0
                                THEN Gl.Account
                                    + AccountCorresponding
                                WHEN SUM(CreditAmount) <> 0
                                THEN gl.AccountCorresponding
                                    + GL.Account
                        END AS Sort ,
                        GL.ReferenceID
                        AS RefIDSort,
                        1 AS SortOrder,
                        0 AS DetailPostOrder,
						MAX(GL.OrderPriority) as OrderPriority,
                        SUM(CreditAmount) as OrderTotal                    
                FROM      @tbDataGL GL /*edit by cuongpv [dbo].[GeneralLedger] -> @tbDataGL*/
                        LEFT JOIN dbo.AccountingObject ao ON ao.ID = GL.AccountingObjectID
                WHERE     gl.PostedDate BETWEEN @FromDate AND @ToDate
                        AND ISNULL(gl.AccountCorresponding,'') <> ''
                GROUP BY  GL.[ReferenceID] ,
                        GL.[TypeID] ,
                        GL.[PostedDate] ,
                        GL.[RefDate] ,
                        GL.[RefNo] ,
                        GL.No ,
                        GL.[InvoiceDate] ,
                        GL.[InvoiceNo] ,
                        GL.Reason ,
                        GL.[Account] ,
                        GL.[AccountCorresponding]
						/*gl.OrderPriority comment by cuongpv*/
                                            
                HAVING    SUM(GL.[CreditAmount]) <> 0
			/*end add by cuongpv*/
            ) t
			order by PostedDate,RefNo,OrderTotal,SortOrder,OrderPriority
			END
        ELSE 
            BEGIN
			select * from
				(SELECT -1 AS RowNum,
					CAST(0 AS BIT) AS IsSummaryRow,
					CAST (1 AS BIT) AS IsBold ,
					NULL AS [RefID],
					NULL AS RefType,
					NULL AS PostedDate,
					NULL AS RefDate,
				  NULL AS [RefNo],
				  NULL AS RefNo1 ,
				  NULL AS RefOrder ,
				  NULL AS [InvDate]  ,
				  NULL AS [InvNo]  ,
				  N'Số lũy kế kỳ trước chuyển sang' AS [JournalMemo] ,
				  /*bổ sung trường diễn giải thông tin chung cr 137228*/              
				 '' AS JournalMemoMaster ,
				  NULL AS [AccountNumber],
				  NULL AS [CorrespondingAccountNumber],
				  NULL AS [RefTypeName],
				  NULL AS AccountObjectCode,
				  NULL AS AccountObjectName ,
				  NULL AS EmployeeCode ,
				  NULL AS EmployeeName ,
				  NULL AS ExpenseItemCode ,
				  NULL AS ExpenseItemName ,
				  NULL AS JobCode ,
				  NULL AS JobName ,
				  NULL AS ProjectWorkCode ,
				  NULL AS ProjectWorkName,
				  NULL AS OrderNo ,
				  NULL AS [PUContractCode]  ,
				  NULL AS [ContractCode]  ,
				  NULL AS ListItemCode ,
				  NULL AS ListItemName,
				  SUM(GL.DebitAmount) AS DebitAmount ,
				  SUM(GL.CreditAmount) AS CreditAmount,
				  NULL AS BranchName,
				  NULL AS OrganizationUnitCode,
				  NULL AS OrganizationUnitName,
				  NULL AS MasterCustomField1,
				  NULL AS MasterCustomField2,
				  NULL AS MasterCustomField3,
				  NULL AS MasterCustomField4 ,
				  NULL AS MasterCustomField5 ,
				  NULL AS MasterCustomField6 ,
				  NULL AS MasterCustomField7 ,
				  NULL AS MasterCustomField8 ,
				  NULL AS MasterCustomField9 ,
				  NULL AS MasterCustomField10,
				  NULL AS CustomField1 ,
				  NULL AS CustomField2 ,
				  NULL AS CustomField3 ,
				  NULL AS CustomField4 ,
				  NULL AS CustomField5 ,
				  NULL AS CustomField6 ,
				  NULL AS CustomField7 ,
				  NULL AS CustomField8 ,
				  NULL AS CustomField9 ,
				  NULL AS CustomField10,
				  NULL AS UnResonableCost ,
				  NULL AS Sort,
				  NULL AS RefIDSort	,
				  NULL AS SortOrder 
				  ,NULL AS DetailPostOrder
				  , null as OrderPriority
			FROM    @tbDataGL GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
			WHERE	
				(@IsShowAccumAmount = 1	AND @StartDate < @FromDate)
					AND GL.PostedDate BETWEEN @StartDate
										AND     @PrevFromDate
					AND GL.AccountCorresponding IS NOT NULL
					AND GL.AccountCorresponding <> ''
			HAVING  SUM(GL.[DebitAmount]) <> 0
					OR SUM(GL.[CreditAmount]) <> 0

			UNION ALL
			SELECT  
								ROW_NUMBER() OVER ( ORDER BY gl.PostedDate, gl.RefDate
						 			,CASE WHEN GL.TypeID = 4012 THEN 1 ELSE 0 END
									, GL.RefNo , GL.TypeID ) AS RowNum ,							
								CAST (1 AS BIT) AS IsSummaryRow ,
								CAST(0 AS BIT) AS IsBold ,
								GL.[ReferenceID] ,
								GL.[TypeID] ,
								GL.[PostedDate] ,
								GL.[RefDate] ,
								GL.No,
								GL.No ,
								'' AS RefOrder ,
								GL.[InvoiceDate] ,
								GL.[InvoiceNo] ,
								GL.Description AS [JournalMemo] ,
								/* bổ sung trường diễn giải thông tin chung cr 137228*/              
								GL.Reason AS JournalMemoMaster ,
								GL.[Account] ,
								GL.[AccountCorresponding] ,
								'' as RefTypeName,
								ao.AccountingObjectCode ,
								ao.AccountingObjectName ,
								'' as EmployeeCode,
								'' as EmployeeName,
								'' as ExpenseItemCode ,
								'' as ExpenseItemName ,
								'' as JobCode,
								'' as JobName,
								'' as ProjectWorkCode ,
								'' as ProjectWorkName ,
								'' as OrderNo,
								'' as PUContractCode,
								'' as ContractCode ,
								'' as ListItemCode ,
								'' as ListItemName ,
								GL.[DebitAmount] ,
								GL.[CreditAmount] ,
								'' as BranchName,
								'' as OrganizationUnitCode,
								'' as OrganizationUnitName,
								'' as MasterCustomField1,
								'' as MasterCustomField2,
								'' as MasterCustomField3,
								'' as MasterCustomField4,
								'' as MasterCustomField5,
								'' as MasterCustomField6,
								'' as MasterCustomField7,
								'' as MasterCustomField8,
								'' as MasterCustomField9,
								'' as MasterCustomField10,
								'' as CustomField1,
								'' as CustomField2,
								'' as CustomField3,
								'' as CustomField4,
								'' as CustomField5,
								'' as CustomField6,
								'' as CustomField7,
								'' as CustomField8,
								'' as CustomField9,
								'' as CustomField10,
								N'Chi phí hợp lý' AS UnResonableCost ,
								CASE WHEN DebitAmount <> 0
										THEN Gl.Account
											+ AccountCorresponding
											+ CAST(DebitAmount AS NVARCHAR(22))
										WHEN CreditAmount <> 0
										THEN gl.AccountCorresponding
											+ GL.Account
											+ CAST(CreditAmount AS NVARCHAR(22))
								END AS Sort ,
								GL.ReferenceID
								AS RefIDSort,
								0 as SortOrder,
								0 as DetailPostOrder,
								GL.OrderPriority
						FROM      @tbDataGL GL /*edit by cuongpv [dbo].[GeneralLedger] -> @tbDataGL*/
								LEFT JOIN dbo.AccountingObject ao ON ao.ID = GL.AccountingObjectID
						WHERE     gl.PostedDate BETWEEN @FromDate AND @ToDate
								AND ISNULL(gl.AccountCorresponding,
											'') <> ''
								AND ( GL.[DebitAmount] <> 0
										OR GL.[CreditAmount] <> 0
									)
				 ) t
				order by t.PostedDate, t.OrderPriority
            END 
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GetHealthFinanceChart]
      ( @FromDate DATETIME ,
        @ToDate DATETIME
		)
AS
BEGIN
	DECLARE @tbTemp TABLE(
		PostedDate datetime,
		PostedDay int,
		PostedMonth int,
		PostedYear int,
		Account nvarchar(25),
		AccountCorresponding nvarchar(25),
		DebitAmount decimal(25,0),
		CreditAmount decimal(25,0)
	)
	
	INSERT INTO @tbTemp
	SELECT PostedDate, DAY(PostedDate) as Ngay, MONTH(PostedDate) as Thang, YEAR(PostedDate) as Nam, Account, AccountCorresponding, 
	DebitAmount, CreditAmount
	FROM GeneralLedger
	WHERE PostedDate between @FromDate AND @ToDate
	
	Declare @tbDataTempChart TABLE(
		PostedMonth int,
		PostedYear int,
		TurnoverTotal money,
		CostsTotal money
	)
	
	INSERT INTO @tbDataTempChart(PostedMonth,PostedYear,TurnoverTotal)
	SELECT PostedMonth, PostedYear, SUM(CreditAmount-DebitAmount) as TurnoverTotal
	FROM @tbTemp
	WHERE ((Account like '511%') OR (Account like '515%') OR (Account like '711%')) AND AccountCorresponding <> '911'
	GROUP BY PostedMonth, PostedYear
	
	INSERT INTO @tbDataTempChart(PostedMonth,PostedYear,CostsTotal)
	SELECT PostedMonth, PostedYear, SUM(DebitAmount-CreditAmount) as CostsTotal
	FROM @tbTemp
	WHERE ((Account like '632%') OR (Account like '642%') OR (Account like '635%') OR (Account like '811%') OR (Account like '821%')) 
	AND AccountCorresponding <> '911'
	GROUP BY PostedMonth, PostedYear
	
	Declare @tbDataChart TABLE(
		PostedMonthYear nvarchar(15),
		PostedMonth int,
		PostedYear int,
		TurnoverTotal money,
		CostsTotal money
	)
	
	INSERT INTO @tbDataChart(PostedMonth,PostedYear,TurnoverTotal,CostsTotal)
	SELECT PostedMonth,PostedYear,SUM(TurnoverTotal) as TurnoverTotal, SUM(CostsTotal) as CostsTotal
	FROM @tbDataTempChart
	GROUP BY PostedMonth,PostedYear
	
	UPDATE @tbDataChart SET TurnoverTotal=0 WHERE  TurnoverTotal<0
	UPDATE @tbDataChart SET CostsTotal=0 WHERE  CostsTotal<0
	
	UPDATE @tbDataChart SET PostedMonthYear=CONVERT(nvarchar(2),PostedMonth)+'/'+CONVERT(nvarchar(4),PostedYear)
	
	SELECT PostedMonthYear,TurnoverTotal,CostsTotal FROM @tbDataChart
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

ALTER PROCEDURE [dbo].[Proc_GetCACashBookInCABook]
    (
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @CurrencyID NVARCHAR(3) ,
      @AccountNumber NVARCHAR(25)
    )
AS
    BEGIN

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
              RefType INT ,
              RefDate DATETIME ,
              PostedDate DATETIME ,
              ReceiptRefNo NVARCHAR(20)  ,
              PaymentRefNo NVARCHAR(20)  ,
              CashBookPostedDate DATETIME ,
              AccountObjectName NVARCHAR(128)
                 ,
              JournalMemo NVARCHAR(255)  ,
              CurrencyID NVARCHAR(3)  ,
              TotalReceiptFBCurrencyID MONEY ,
              TotalPaymentFBCurrencyID MONEY ,
              ClosingFBCurrencyID MONEY ,
              BranchID UNIQUEIDENTIFIER ,
              BranchName NVARCHAR(128)  ,
              RefTypeName NVARCHAR(100)  ,
              Note NVARCHAR(255)  ,
              CAType INT ,
              IsBold BIT ,
               /* - bổ sung mã nhân viên , tên nhân viên */
              EmployeeCode NVARCHAR(25)  ,
              EmployeeName NVARCHAR(128)  ,
            )       
       
	   IF(@CurrencyID = 'VND')
	   BEGIN
			/*Add by cuongpv de sua cach lam tron*/
			DECLARE @tbDataGL TABLE(
				ID uniqueidentifier,
				BranchID uniqueidentifier,
				ReferenceID uniqueidentifier,
				TypeID int,
				Date datetime,
				PostedDate datetime,
				No nvarchar(25),
				InvoiceDate datetime,
				InvoiceNo nvarchar(25),
				Account nvarchar(25),
				AccountCorresponding nvarchar(25),
				BankAccountDetailID uniqueidentifier,
				CurrencyID nvarchar(3),
				ExchangeRate decimal(25, 10),
				DebitAmount decimal(25,0),
				DebitAmountOriginal decimal(25,0),
				CreditAmount decimal(25,0),
				CreditAmountOriginal decimal(25,0),
				Reason nvarchar(512),
				Description nvarchar(512),
				VATDescription nvarchar(512),
				AccountingObjectID uniqueidentifier,
				EmployeeID uniqueidentifier,
				BudgetItemID uniqueidentifier,
				CostSetID uniqueidentifier,
				ContractID uniqueidentifier,
				StatisticsCodeID uniqueidentifier,
				InvoiceSeries nvarchar(25),
				ContactName nvarchar(512),
				DetailID uniqueidentifier,
				RefNo nvarchar(25),
				RefDate datetime,
				DepartmentID uniqueidentifier,
				ExpenseItemID uniqueidentifier,
				OrderPriority int,
				IsIrrationalCost bit
			)

			INSERT INTO @tbDataGL
			SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
			/*end add by cuongpv*/

			INSERT  #Result
                ( RefID ,
                  RefType ,
                  RefDate ,
                  PostedDate ,
                  ReceiptRefNo ,
                  PaymentRefNo ,
                  CashBookPostedDate ,
                  AccountObjectName ,
                  JournalMemo ,
                  CurrencyID ,
                  TotalReceiptFBCurrencyID ,
                  TotalPaymentFBCurrencyID ,
                  ClosingFBCurrencyID ,
                  BranchID ,
                  BranchName ,
                  RefTypeName ,
                  CAType ,
                  IsBold ,
                  EmployeeCode ,
                  EmployeeName
                )
                SELECT  RefID ,
                        RefType ,
                        RefDate ,
                        R.PostedDate ,
                        CASE WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        CASE WHEN TotalPaymentFBCurrencyID
                                  - TotalReceiptFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        R.PostedDate ,
                        ContactName ,
                        JournalMemo ,
                        @CurrencyID ,
                        SUM(CASE WHEN TotalReceiptFBCurrencyID
                                      - TotalPaymentFBCurrencyID > 0
                                 THEN TotalReceiptFBCurrencyID
                                      - TotalPaymentFBCurrencyID
                                 ELSE 0
                            END) AS TotalReceiptFBCurrencyID ,
                        SUM(CASE WHEN TotalPaymentFBCurrencyID
                                      - TotalReceiptFBCurrencyID > 0
                                 THEN TotalPaymentFBCurrencyID
                                      - TotalReceiptFBCurrencyID
                                 ELSE 0
                            END) AS TotalPaymentFBCurrencyID ,
                        SUM(ClosingFBCurrencyID) ,
                        BranchID ,
                        BranchName ,
                        RefTypeName ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END AS CAType ,
                        IsBold ,
                        EmployeeCode ,
                        EmployeeName
                FROM    ( SELECT    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ReferenceID
                                    END AS RefID ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.TypeID
                                    END AS RefType ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefDate
                                    END AS RefDate ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END AS PostedDate ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefNo
                                    END AS RefNo ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ContactName
                                    END AS ContactName ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN N'Số dư đầu kỳ'
                                         ELSE GL.Description
                                    END AS JournalMemo ,
                                    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal > 0
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) AS TotalReceiptFBCurrencyID ,
                                    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.CreditAmountOriginal
                                                  - GL.DebitAmountOriginal > 0
                                             THEN GL.CreditAmountOriginal
                                                  - GL.DebitAmountOriginal
                                             ELSE 0
                                        END) AS TotalPaymentFBCurrencyID ,
                                    SUM(CASE WHEN GL.PostedDate < @FromDate
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) AS ClosingFBCurrencyID ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.BranchID
                                    END AS BranchID ,
                                    null AS BranchName ,
                                    null RefTypeName ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE 0
                                    END AS CAType ,
                                    CASE WHEN GL.PostedDate < @FromDate THEN 1
                                         ELSE 0
                                    END AS IsBold ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectCode
                                    END AS EmployeeCode ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectName
                                    END AS EmployeeName
                          FROM      @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                    LEFT JOIN dbo.AccountingObject AS AO ON AO.ID = GL.EmployeeID
                          WHERE     ( GL.PostedDate <= @ToDate )
                                    AND ( @CurrencyID IS NULL
                                          OR GL.CurrencyID = @CurrencyID
                                        )
                                    AND GL.Account LIKE @AccountNumber
                                    + '%'
                          GROUP BY  CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ReferenceID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.TypeID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefNo
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ContactName
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN N'Số dư đầu kỳ'
                                         ELSE GL.Description
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.BranchID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE 0
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate THEN 1
                                         ELSE 0
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectCode
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectName
                                    END
                          HAVING    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal > 0
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) <> SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                              AND GL.CreditAmountOriginal
                                                              - GL.DebitAmountOriginal > 0
                                                         THEN GL.CreditAmountOriginal
                                                              - GL.DebitAmountOriginal
                                                         ELSE 0
                                                    END)
                                    OR SUM(CASE WHEN GL.PostedDate < @FromDate
                                                THEN GL.DebitAmountOriginal
                                                     - GL.CreditAmountOriginal
                                                ELSE 0
                                           END) <> 0
                        ) AS R
                GROUP BY R.RefID ,
                        R.RefType ,
                        R.RefDate ,
                        R.PostedDate ,
                        CASE WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        CASE WHEN TotalPaymentFBCurrencyID
                                  - TotalReceiptFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        R.RefNo ,
                        R.ContactName ,
                        R.JournalMemo ,
                        R.BranchID ,
                        R.BranchName ,
                        R.RefTypeName ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END ,
                        R.IsBold ,
                        R.EmployeeCode ,
                        R.EmployeeName
                ORDER BY R.PostedDate ,
                        R.RefDate ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END ,
                        R.RefNo	
	   END
	   ELSE
	   BEGIN
			INSERT  #Result
                ( RefID ,
                  RefType ,
                  RefDate ,
                  PostedDate ,
                  ReceiptRefNo ,
                  PaymentRefNo ,
                  CashBookPostedDate ,
                  AccountObjectName ,
                  JournalMemo ,
                  CurrencyID ,
                  TotalReceiptFBCurrencyID ,
                  TotalPaymentFBCurrencyID ,
                  ClosingFBCurrencyID ,
                  BranchID ,
                  BranchName ,
                  RefTypeName ,
                  CAType ,
                  IsBold ,
                  EmployeeCode ,
                  EmployeeName
                )
                SELECT  RefID ,
                        RefType ,
                        RefDate ,
                        R.PostedDate ,
                        CASE WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        CASE WHEN TotalPaymentFBCurrencyID
                                  - TotalReceiptFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        R.PostedDate ,
                        ContactName ,
                        JournalMemo ,
                        @CurrencyID ,
                        SUM(CASE WHEN TotalReceiptFBCurrencyID
                                      - TotalPaymentFBCurrencyID > 0
                                 THEN TotalReceiptFBCurrencyID
                                      - TotalPaymentFBCurrencyID
                                 ELSE 0
                            END) AS TotalReceiptFBCurrencyID ,
                        SUM(CASE WHEN TotalPaymentFBCurrencyID
                                      - TotalReceiptFBCurrencyID > 0
                                 THEN TotalPaymentFBCurrencyID
                                      - TotalReceiptFBCurrencyID
                                 ELSE 0
                            END) AS TotalPaymentFBCurrencyID ,
                        SUM(ClosingFBCurrencyID) ,
                        BranchID ,
                        BranchName ,
                        RefTypeName ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END AS CAType ,
                        IsBold ,
                        EmployeeCode ,
                        EmployeeName
                FROM    ( SELECT    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ReferenceID
                                    END AS RefID ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.TypeID
                                    END AS RefType ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefDate
                                    END AS RefDate ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END AS PostedDate ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefNo
                                    END AS RefNo ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ContactName
                                    END AS ContactName ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN N'Số dư đầu kỳ'
                                         ELSE GL.Description
                                    END AS JournalMemo ,
                                    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal > 0
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) AS TotalReceiptFBCurrencyID ,
                                    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.CreditAmountOriginal
                                                  - GL.DebitAmountOriginal > 0
                                             THEN GL.CreditAmountOriginal
                                                  - GL.DebitAmountOriginal
                                             ELSE 0
                                        END) AS TotalPaymentFBCurrencyID ,
                                    SUM(CASE WHEN GL.PostedDate < @FromDate
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) AS ClosingFBCurrencyID ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.BranchID
                                    END AS BranchID ,
                                    null AS BranchName ,
                                    null RefTypeName ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE 0
                                    END AS CAType ,
                                    CASE WHEN GL.PostedDate < @FromDate THEN 1
                                         ELSE 0
                                    END AS IsBold ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectCode
                                    END AS EmployeeCode ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectName
                                    END AS EmployeeName
                          FROM      dbo.GeneralLedger AS GL
                                    LEFT JOIN dbo.AccountingObject AS AO ON AO.ID = GL.EmployeeID
                          WHERE     ( GL.PostedDate <= @ToDate )
                                    AND ( @CurrencyID IS NULL
                                          OR GL.CurrencyID = @CurrencyID
                                        )
                                    AND GL.Account LIKE @AccountNumber
                                    + '%'
                          GROUP BY  CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ReferenceID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.TypeID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.RefNo
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.PostedDate
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.ContactName
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN N'Số dư đầu kỳ'
                                         ELSE GL.Description
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE GL.BranchID
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE 0
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate THEN 1
                                         ELSE 0
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectCode
                                    END ,
                                    CASE WHEN GL.PostedDate < @FromDate
                                         THEN NULL
                                         ELSE AO.AccountingObjectName
                                    END
                          HAVING    SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                  AND GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal > 0
                                             THEN GL.DebitAmountOriginal
                                                  - GL.CreditAmountOriginal
                                             ELSE 0
                                        END) <> SUM(CASE WHEN GL.PostedDate >= @FromDate
                                                              AND GL.CreditAmountOriginal
                                                              - GL.DebitAmountOriginal > 0
                                                         THEN GL.CreditAmountOriginal
                                                              - GL.DebitAmountOriginal
                                                         ELSE 0
                                                    END)
                                    OR SUM(CASE WHEN GL.PostedDate < @FromDate
                                                THEN GL.DebitAmountOriginal
                                                     - GL.CreditAmountOriginal
                                                ELSE 0
                                           END) <> 0
                        ) AS R
                GROUP BY R.RefID ,
                        R.RefType ,
                        R.RefDate ,
                        R.PostedDate ,
                        CASE WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        CASE WHEN TotalPaymentFBCurrencyID
                                  - TotalReceiptFBCurrencyID > 0 THEN R.RefNo
                             ELSE NULL
                        END ,
                        R.RefNo ,
                        R.ContactName ,
                        R.JournalMemo ,
                        R.BranchID ,
                        R.BranchName ,
                        R.RefTypeName ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END ,
                        R.IsBold ,
                        R.EmployeeCode ,
                        R.EmployeeName
                ORDER BY R.PostedDate ,
                        R.RefDate ,
                        CASE WHEN R.CAType IS NULL THEN NULL
                             WHEN TotalReceiptFBCurrencyID
                                  - TotalPaymentFBCurrencyID > 0 THEN 0
                             ELSE 1
                        END ,
                        R.RefNo	
	   END

        DECLARE @ClosingAmount AS DECIMAL(22, 8)
        SET @ClosingAmount = 0
        SELECT  @ClosingAmount = ClosingFBCurrencyID
        FROM    #Result
        WHERE   RefID IS NULL
        UPDATE  #Result
        SET     @ClosingAmount = @ClosingAmount
                + ISNULL(TotalReceiptFBCurrencyID, 0)
                - ISNULL(TotalPaymentFBCurrencyID, 0) ,
                ClosingFBCurrencyID = @ClosingAmount	
    
        SELECT  *
        FROM    #Result R       
    
        DROP TABLE #Result
        
    END


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_GetB09_SecVI] 
	@StartDate DateTime,
	@EndDate DateTime,
	@StartDateAgo DateTime
AS
BEGIN
	
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @EndDate
		/*end add by cuongpv*/

	/*=========== Tinh VI_1_a_1 ============*/
	SELECT 'VI_1_a_1' Name,
		(
			SELECT sum (CreditAmount)								
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/
			Where Account like '5111%'								
			and PostedDate >= @StartDate and  PostedDate <= @EndDate								
			and  typeID not in ('330', '340')								
			and AccountCorresponding <> '911%'
		) ThisYear,
		(
			SELECT sum (CreditAmount)								
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/							
			Where Account like '5111%'								
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate								
			and  typeID not in ('330', '340')								
			and AccountCorresponding <> '911%'
		) LastYear
	/*=========== Tinh VI_1_a_2 ============*/
	UNION ALL
	SELECT 'VI_1_a_2' Name,
		(
			SELECT sum (CreditAmount)								
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/							
			Where Account like '5112%'								
			and PostedDate >= @StartDate and  PostedDate <= @EndDate								
			and  typeID not in ('330', '340')

		) ThisYear,
		(
			SELECT sum (CreditAmount)								
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/							
			Where Account like '5112%'								
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate								
			and  typeID not in ('330', '340')
		) LastYear
	/*=========== Tinh VI_1_a_3 ============*/
	UNION ALL
	SELECT 'VI_1_a_3' Name,
		(
			SELECT sum (CreditAmount)								
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/							
			Where Account like '5113%'								
			and PostedDate >= @StartDate and  PostedDate <= @EndDate								
			and  typeID not in ('330', '340')								
			and AccountCorresponding <> '911%'
		) ThisYear,
		(
			SELECT sum (CreditAmount)								
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/							
			Where Account like '5113%'								
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate								
			and  typeID not in ('330', '340')								
			and AccountCorresponding <> '911%'
		) LastYear
	/*=========== Tinh VI_1_a_4 ============*/
	UNION ALL
	SELECT 'VI_1_a_4' Name,
		(
			SELECT sum (CreditAmount)								
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/							
			Where Account like '5118%'								
			and PostedDate >= @StartDate and  PostedDate <= @EndDate								
			and  typeID not in ('330', '340')								
			and AccountCorresponding <> '911%'

		) ThisYear,
		(
			SELECT sum (CreditAmount)								
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/							
			Where Account like '5118%'								
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate								
			and  typeID not in ('330', '340')								
			and AccountCorresponding <> '911%'
		) LastYear
	/*=========== Tinh VI_2_1 ============*/
	UNION ALL
	SELECT 'VI_2_1' Name,
		(
			SELECT sum (DebitAmount)								
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/								
			where Account like '511%' and DebitAmount > 0 								
			and PostedDate >= @StartDate and  PostedDate <= @EndDate								
			and TypeID in ('320','321','322','323','324','325')
		) ThisYear,
		(
			SELECT sum (DebitAmount)								
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/								
			where Account like '511%' and DebitAmount > 0 								
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate								
			and TypeID in ('320','321','322','323','324','325')
		) LastYear
	/*=========== Tinh VI_2_2 ============*/
	UNION ALL
	SELECT 'VI_2_2' Name,
		(
			SELECT sum (DebitAmount) - Sum (CreditAmount)								
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/								
			where Account like '511%'								
			and PostedDate >= @StartDate and  PostedDate <= @EndDate								
			and TypeID in ('340')
		) ThisYear,
		(
			SELECT sum (DebitAmount) - Sum (CreditAmount)								
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/								
			where Account like '511%'								
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate								
			and TypeID in ('340')
		) LastYear
	/*=========== Tinh VI_2_3 ============*/
	UNION ALL
	SELECT 'VI_2_3' Name,
		(
			SELECT sum (DebitAmount) - sum (CreditAmount)								
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/								
			where Account like '511%'								
			and PostedDate >= @StartDate and  PostedDate <= @EndDate								
			and TypeID in ('330')
		) ThisYear,
		(
			SELECT sum (DebitAmount) - sum (CreditAmount)								
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/								
			where Account like '511%'								
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate								
			and TypeID in ('330')
		) LastYear
	/*=========== Tinh VI_3_1 ============*/
	UNION ALL
	SELECT 'VI_3_1' Name,
		(
			SELECT sum (CreditAmount)								
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/								
			where Account like '632%' and CreditAmount > 0 								
			and AccountCorresponding like '911%'								
			and PostedDate >= @StartDate and  PostedDate <= @EndDate
		) ThisYear,
		(
			SELECT sum (CreditAmount)								
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/								
			where Account like '632%' and CreditAmount > 0 								
			and AccountCorresponding like '911%'								
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate
		) LastYear
	/*=========== Tinh VI_4_6 ============*/
	UNION ALL
	SELECT 'VI_4_6' Name,
		(
			SELECT sum (DebitAmount)
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/
			where Account like '515%' and DebitAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDate and  PostedDate <= @EndDate
		) ThisYear,
		(
			SELECT sum (DebitAmount)
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/
			where Account like '515%' and DebitAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate
		) LastYear
	/*=========== Tinh VI_5_6 ============*/
	UNION ALL
	SELECT 'VI_5_6' Name,
		(
			SELECT sum (CreditAmount)
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/
			where Account like '635%' and CreditAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDate and  PostedDate <= @EndDate
		) ThisYear,
		(
			SELECT sum (CreditAmount)
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/
			where Account like '635%' and CreditAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate
		) LastYear
	/*=========== Tinh VI_6_1 ============*/
	UNION ALL
	SELECT 'VI_6_a' Name,
		(
			SELECT sum (CreditAmount)
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/
			where Account like '6422%' and CreditAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDate and  PostedDate <= @EndDate
		) ThisYear,
		(
			SELECT sum (CreditAmount)
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/
			where Account like '6422%' and CreditAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate
		) LastYear
	/*=========== Tinh VI_6_2 ============*/
	UNION ALL
	SELECT 'VI_6_b' Name,
		(
			SELECT sum (CreditAmount)
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/
			where Account like '6421%' and CreditAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDate and  PostedDate <= @EndDate
		) ThisYear,
		(
			SELECT sum (CreditAmount)
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/
			where Account like '6421%' and CreditAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate
		) LastYear
	/*=========== Tinh VI_7_5 ============*/
	UNION ALL
	SELECT 'VI_7_5' Name,
		(
			SELECT sum (DebitAmount)
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/
			where Account like '711%' and DebitAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDate and  PostedDate <= @EndDate
		) ThisYear,
		(
			SELECT sum (DebitAmount)
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/
			where Account like '711%' and DebitAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate
		) LastYear
	/*=========== Tinh VI_8_4 ============*/
	UNION ALL
	SELECT 'VI_8_4' Name,
		(
			SELECT sum (CreditAmount)
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/
			where Account like '811%' and CreditAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDate and  PostedDate <= @EndDate
		) ThisYear,
		(
			SELECT sum (CreditAmount)
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/
			where Account like '811%' and CreditAmount > 0 
			and AccountCorresponding like '911%'
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate
		) LastYear
	/*=========== Tinh VI_9_3 ============*/
	UNION ALL
	SELECT 'VI_9_3' Name,
		(
			SELECT sum (CreditAmount)	
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/	
			where Account like '821%' and CreditAmount > 0 	
			and AccountCorresponding like '911%'	
			and PostedDate >= @StartDate and  PostedDate <= @EndDate
		) ThisYear,
		(
			SELECT sum (CreditAmount)	
			FROM @tbDataGL /*edit by cuongpv [GeneralLedger] -> @tbDataGL*/	
			where Account like '821%' and CreditAmount > 0 	
			and AccountCorresponding like '911%'	
			and PostedDate >= @StartDateAgo and  PostedDate < @StartDate
		) LastYear
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*=================================================================================================
 * Created By:		thaivh	
 * Created Date:	15/5/2018
 * Description:		Lay du lieu cho hop dong << hợp đồng >>
===================================================================================================== */
ALTER PROCEDURE [dbo].[Proc_EM_GetContract]
  
    @FromDate DATETIME ,
    @ToDate DATETIME,
	@IsContractSale bit
AS
BEGIN
	/*add by cuongpv*/
	DECLARE @tbDataEMContract TABLE(
	ID uniqueidentifier,
	AccountingObjectID uniqueidentifier,
	MaterialGoodsID uniqueidentifier,
	SignedDate datetime,
	TypeID int,
	Unit nvarchar(25),
	Quantity decimal(25,10),
	QuantityReceipt decimal(25,10),
	TotalAmount decimal(25,0),
	DiscountAmount decimal(25,0),
	VATAmount decimal(25,0)
	)

	INSERT INTO @tbDataEMContract
	SELECT a.ID, a.AccountingObjectID, b.MaterialGoodsID, a.SignedDate, a.TypeID, b.Unit, b.Quantity, b.QuantityReceipt, b.TotalAmount, b.DiscountAmount, b.VATAmount
	FROM EMContract a LEFT JOIN EMContractDetailMG b ON a.ID = b.ContractID
	WHERE a.SignedDate BETWEEN @FromDate and @ToDate

	DECLARE @tbluutru TABLE(
		ContractID UNIQUEIDENTIFIER,
		SignedDate DATE,
		Code NVARCHAR(50),
		AccountingObjectName NVARCHAR(512),
		AccountingObjectID UNIQUEIDENTIFIER,
		MaterialGoodsName NVARCHAR(512),
		MaterialGoodsID UNIQUEIDENTIFIER,
		Unit NVARCHAR(25),
		Quantity DECIMAL(25,10),
		QuantityReceipt DECIMAL(25,10),
		Amount MONEY,
		DiscountAmount MONEY,
		VATAmount MONEY,
		ContractAmount MONEY,
		DSBanHang MONEY,
		TraLai MONEY,
		GiamGia MONEY,
		ActionAmount MONEY,
		RevenueType BIT,
		AccountObjectGroupID UNIQUEIDENTIFIER,
		MaterialGoodsCategoryID UNIQUEIDENTIFIER,
		DepartmentID UNIQUEIDENTIFIER,
		ContractEmployeeID UNIQUEIDENTIFIER
	)
	/*end add by cuongpv*/
    SET NOCOUNT ON;
	
    if @IsContractSale = 1
		Begin
			
			/*Add by cuongpv de sua cach lam tron*/
			DECLARE @tbDataGL TABLE(
				ID uniqueidentifier,
				BranchID uniqueidentifier,
				ReferenceID uniqueidentifier,
				TypeID int,
				Date datetime,
				PostedDate datetime,
				No nvarchar(25),
				InvoiceDate datetime,
				InvoiceNo nvarchar(25),
				Account nvarchar(25),
				AccountCorresponding nvarchar(25),
				BankAccountDetailID uniqueidentifier,
				CurrencyID nvarchar(3),
				ExchangeRate decimal(25, 10),
				DebitAmount decimal(25,0),
				DebitAmountOriginal decimal(25,2),
				CreditAmount decimal(25,0),
				CreditAmountOriginal decimal(25,2),
				Reason nvarchar(512),
				Description nvarchar(512),
				VATDescription nvarchar(512),
				AccountingObjectID uniqueidentifier,
				EmployeeID uniqueidentifier,
				BudgetItemID uniqueidentifier,
				CostSetID uniqueidentifier,
				ContractID uniqueidentifier,
				StatisticsCodeID uniqueidentifier,
				InvoiceSeries nvarchar(25),
				ContactName nvarchar(512),
				DetailID uniqueidentifier,
				RefNo nvarchar(25),
				RefDate datetime,
				DepartmentID uniqueidentifier,
				ExpenseItemID uniqueidentifier,
				OrderPriority int,
				IsIrrationalCost bit
			)

			INSERT INTO @tbDataGL
			SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate BETWEEN @FromDate and @ToDate

			/*end add by cuongpv*/

			/*add by cuongpv*/
			INSERT INTO @tbluutru(ContractID, AccountingObjectID, MaterialGoodsID, Unit, Quantity, QuantityReceipt, Amount, 
			DiscountAmount, VATAmount)
			/*SELECT a.ID as ContractID, a.AccountingObjectID, b.MaterialGoodsID, MAX(b.Unit) as Unit, SUM(b.Quantity) as Quantity, SUM(b.QuantityReceipt) as QuantityReceipt,
			SUM(b.TotalAmount) as Amount, SUM(b.DiscountAmount) as DiscountAmount, SUM(b.VATAmount) as VATAmount
			FROM EMContract a LEFT JOIN EMContractDetailMG b ON a.ID = b.ContractID
			WHERE (a.SignedDate BETWEEN @FromDate and @ToDate) AND a.TypeID = 860 comment by cuongpv*/
			SELECT ID as ContractID, AccountingObjectID, MaterialGoodsID, MAX(Unit) as Unit, SUM(Quantity) as Quantity, SUM(QuantityReceipt) as QuantityReceipt,
			SUM(TotalAmount) as Amount, SUM(DiscountAmount) as DiscountAmount, SUM(VATAmount) as VATAmount
			FROM @tbDataEMContract
			WHERE (SignedDate BETWEEN @FromDate and @ToDate) AND TypeID = 860
			GROUP BY ID, AccountingObjectID, MaterialGoodsID
			
			DECLARE @tbDSBanHang TABLE(
				ContractID UNIQUEIDENTIFIER,
				AccountingObjectID UNIQUEIDENTIFIER,
				MaterialGoodsID UNIQUEIDENTIFIER,
				DSBanHang decimal(25,10)
			)
			INSERT INTO @tbDSBanHang
			SELECT c.ContractID, c.AccountingObjectID, a.MaterialGoodsID, SUM(c.CreditAmount - c.DebitAmount) as DSBanHang 
				FROM SAInvoiceDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID /*edit by cuongpv GeneralLedger -> @tbDataGL*/
				WHERE c.ContractID in (select ContractID from @tbluutru) AND c.Account like '511%'
				 AND (c.PostedDate between @FromDate and @ToDate)
				GROUP BY c.ContractID, c.AccountingObjectID, a.MaterialGoodsID
			
			UPDATE @tbluutru SET DSBanHang = f.DSBanHang
			FROM (select ContractID as Eid, AccountingObjectID as AOID, MaterialGoodsID as Mid, DSBanHang from @tbDSBanHang) f
			WHERE ContractID = f.Eid AND AccountingObjectID = f.AOID AND MaterialGoodsID = f.Mid
			
			DECLARE @tbTraLai TABLE(
				ContractID UNIQUEIDENTIFIER,
				AccountingObjectID UNIQUEIDENTIFIER,
				MaterialGoodsID UNIQUEIDENTIFIER,
				TraLai decimal(25,10)
			)
			INSERT INTO @tbTraLai
			SELECT c.ContractID, c.AccountingObjectID, a.MaterialGoodsID, SUM(c.DebitAmount - c.CreditAmount) as TraLai 
				FROM SAReturnDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID /*edit by cuongpv GeneralLedger -> @tbDataGL*/
				WHERE c.ContractID in (select ContractID from @tbluutru) AND c.Account like '511%' AND c.TypeID=330
				 AND (c.PostedDate between @FromDate and @ToDate)
				GROUP BY c.ContractID, c.AccountingObjectID, a.MaterialGoodsID
			
			UPDATE @tbluutru SET TraLai = tr.TraLai
			FROM (select ContractID as Eid, AccountingObjectID as AOID, MaterialGoodsID as Mid, TraLai from @tbTraLai) tr
			WHERE ContractID = tr.Eid AND AccountingObjectID = tr.AOID AND MaterialGoodsID = tr.Mid
			
			DECLARE @tbGiamGia TABLE(
				ContractID UNIQUEIDENTIFIER,
				AccountingObjectID UNIQUEIDENTIFIER,
				MaterialGoodsID UNIQUEIDENTIFIER,
				GiamGia decimal(25,10)
			)
			INSERT INTO @tbGiamGia
			SELECT c.ContractID, c.AccountingObjectID, a.MaterialGoodsID, SUM(c.DebitAmount - c.CreditAmount) as GiamGia 
				FROM SAReturnDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID /*edit by cuongpv GeneralLedger -> @tbDataGL*/
				WHERE c.ContractID in (select ContractID from @tbluutru) AND c.Account like '511%' AND c.TypeID=340
				 AND (c.PostedDate between @FromDate and @ToDate)
				GROUP BY c.ContractID, c.AccountingObjectID, a.MaterialGoodsID
			
			UPDATE @tbluutru SET GiamGia = gg.GiamGia
			FROM (select ContractID as Eid, AccountingObjectID as AOID, MaterialGoodsID as Mid, GiamGia from @tbGiamGia) gg
			WHERE ContractID = gg.Eid AND AccountingObjectID = gg.AOID AND MaterialGoodsID = gg.Mid
			
			UPDATE @tbluutru SET ActionAmount = ISNULL(DSBanHang,0) - ISNULL(TraLai,0) - ISNULL(GiamGia,0)
			
			UPDATE @tbluutru SET ContractAmount = ISNULL(Amount,0) - ISNULL(DiscountAmount,0) + ISNULL(VATAmount,0)
			
			/*end add by cuongpv*/
		End
	else
		Begin
			INSERT INTO @tbluutru(ContractID, AccountingObjectID, MaterialGoodsID, Unit, Quantity, QuantityReceipt, Amount, 
			DiscountAmount, VATAmount)
			/*SELECT a.ID as ContractID, a.AccountingObjectID, b.MaterialGoodsID, MAX(b.Unit) as Unit, SUM(b.Quantity) as Quantity, SUM(b.QuantityReceipt) as QuantityReceipt,
			SUM(b.TotalAmount) as Amount, SUM(b.DiscountAmount) as DiscountAmount, SUM(b.VATAmount) as VATAmount
			FROM EMContract a LEFT JOIN EMContractDetailMG b ON a.ID = b.ContractID
			WHERE (a.SignedDate BETWEEN @FromDate and @ToDate) AND a.TypeID = 850
			GROUP BY a.ID, a.AccountingObjectID, b.MaterialGoodsID Comment by cuongpv*/
			SELECT ID as ContractID, AccountingObjectID, MaterialGoodsID, MAX(Unit) as Unit, SUM(Quantity) as Quantity, SUM(QuantityReceipt) as QuantityReceipt,
			SUM(TotalAmount) as Amount, SUM(DiscountAmount) as DiscountAmount, SUM(VATAmount) as VATAmount
			FROM @tbDataEMContract
			WHERE (SignedDate BETWEEN @FromDate and @ToDate) AND TypeID = 850
			GROUP BY ID, AccountingObjectID, MaterialGoodsID
			
			/*add by cuongpv lay du lieu tho lam tron tung dong PPInvoiceDetail, PPServiceDetail*/
			DECLARE @tbDataPPinvoiceDetail TABLE(
				ContractID UNIQUEIDENTIFIER,
				AccountingObjectID UNIQUEIDENTIFIER,
				MaterialGoodsID UNIQUEIDENTIFIER,
				Amount decimal(25,0),
				DiscountAmount decimal(25,0),
				VATAmount decimal(25,0)
			)
			INSERT INTO @tbDataPPinvoiceDetail
			SELECT ContractID, AccountingObjectID, MaterialGoodsID, Amount, DiscountAmount, VATAmount 
			FROM PPInvoiceDetail 
			WHERE ContractID in (select ContractID from @tbluutru)

			DECLARE @tbDataPPServiceDetail TABLE(
				ContractID UNIQUEIDENTIFIER,
				AccountingObjectID UNIQUEIDENTIFIER,
				MaterialGoodsID UNIQUEIDENTIFIER,
				Amount decimal(25,0),
				DiscountAmount decimal(25,0),
				VATAmount decimal(25,0)
			)
			INSERT INTO @tbDataPPServiceDetail
			SELECT ContractID, AccountingObjectID, MaterialGoodsID, Amount, DiscountAmount, VATAmount 
			FROM PPServiceDetail 
			WHERE ContractID in (select ContractID from @tbluutru)
			/*end add by cuongpv*/

			DECLARE @tbDSDSMuaHang TABLE(
				ContractID UNIQUEIDENTIFIER,
				AccountingObjectID UNIQUEIDENTIFIER,
				MaterialGoodsID UNIQUEIDENTIFIER,
				DSMuaHang decimal(25,10)
			)
			INSERT INTO @tbDSDSMuaHang
			SELECT a.ContractID, a.AccountingObjectID, a.MaterialGoodsID, SUM(a.Amount - a.DiscountAmount + a.VATAmount) as DSMuaHang 
				FROM @tbDataPPinvoiceDetail a 
				/*WHERE a.ContractID in (select ContractID from @tbluutru) comment by cuongpv*/
				GROUP BY a.ContractID, a.AccountingObjectID, a.MaterialGoodsID
			UNION ALL
			SELECT a.ContractID, a.AccountingObjectID, a.MaterialGoodsID, SUM(a.Amount - a.DiscountAmount + a.VATAmount) as DSMuaHang 
				FROM @tbDataPPServiceDetail a 
				/*WHERE a.ContractID in (select ContractID from @tbluutru) comment by cuongpv*/
				GROUP BY a.ContractID, a.AccountingObjectID, a.MaterialGoodsID
			
			UPDATE @tbluutru SET DSBanHang = mh.DSMuaHang
			FROM (select ContractID as Eid, AccountingObjectID as AOId, MaterialGoodsID as Mid, DSMuaHang from @tbDSDSMuaHang) mh
			WHERE ContractID = mh.Eid AND AccountingObjectID = mh.AOId AND MaterialGoodsID = mh.Mid
			
			/*add by cuongpv lay du lieu lam tron tung dong PPDiscountReturnDetail*/
			DECLARE @tbDataPPDiscountReturnDetail TABLE(
				ContractID UNIQUEIDENTIFIER,
				AccountingObjectID UNIQUEIDENTIFIER,
				MaterialGoodsID UNIQUEIDENTIFIER,
				Amount decimal(25,0),
				VATAmount decimal(25,0)
			)
			INSERT INTO @tbDataPPDiscountReturnDetail
			SELECT ContractID, AccountingObjectID, MaterialGoodsID, Amount, VATAmount
			FROM PPDiscountReturnDetail 
			WHERE ContractID in (select ContractID from @tbluutru)
			/*end add by cuongpv*/

			DECLARE @tbTraLaiGiamGia TABLE(
				ContractID UNIQUEIDENTIFIER,
				AccountingObjectID UNIQUEIDENTIFIER,
				MaterialGoodsID UNIQUEIDENTIFIER,
				TraLaiGiamGia decimal(25,10)
			)
			INSERT INTO @tbTraLaiGiamGia
			SELECT a.ContractID, a.AccountingObjectID, a.MaterialGoodsID, SUM(a.Amount + a.VATAmount) as TraLaiGiamGia 
				FROM @tbDataPPDiscountReturnDetail a
				/*WHERE a.ContractID in (select ContractID from @tbluutru) comment by cuongpv*/
				GROUP BY a.ContractID, a.AccountingObjectID, a.MaterialGoodsID
			
			UPDATE @tbluutru SET TraLai = trg.TraLaiGiamGia
			FROM (select ContractID as Eid, AccountingObjectID as AOId, MaterialGoodsID as Mid, TraLaiGiamGia from @tbTraLaiGiamGia) trg
			WHERE ContractID = trg.Eid AND AccountingObjectID = trg.AOId AND MaterialGoodsID = trg.Mid
			
			UPDATE @tbluutru SET ActionAmount = ISNULL(DSBanHang,0) - ISNULL(TraLai,0)
			
			UPDATE @tbluutru SET ContractAmount = ISNULL(Amount,0) - ISNULL(DiscountAmount,0) + ISNULL(VATAmount,0)
		End
		
			UPDATE @tbluutru SET SignedDate = hd.SignedDate, Code = hd.Code, AccountingObjectName = hd.AccountingObjectName, RevenueType = hd.RevenueType,
			DepartmentID = hd.DepartmentID, ContractEmployeeID = hd.ContractEmployeeID
			FROM (select ID as Eid, SignedDate, Code, AccountingObjectName, RevenueType, DepartmentID, ContractEmployeeID from EMContract) hd
			WHERE ContractID = hd.Eid
			
			UPDATE @tbluutru SET MaterialGoodsCategoryID = M.MaterialGoodsCategoryID, MaterialGoodsName = M.MaterialGoodsName
			FROM (select ID as Mid, MaterialGoodsCategoryID, MaterialGoodsName from MaterialGoods) M
			WHERE MaterialGoodsID = M.Mid
			
			UPDATE @tbluutru SET AccountObjectGroupID = AO.AccountObjectGroupID
			FROM (select ID as AOid, AccountObjectGroupID from AccountingObject) AO
			WHERE AccountingObjectID = AO.AOID
		
		SELECT SignedDate, Code, AccountingObjectName, AccountingObjectID, MaterialGoodsName, MaterialGoodsID, Unit, Quantity, QuantityReceipt, Amount,
		DiscountAmount, VATAmount, ContractAmount, ActionAmount, RevenueType, AccountObjectGroupID, MaterialGoodsCategoryID, DepartmentID, ContractEmployeeID
		FROM @tbluutru
		ORDER BY Code
	/*end add by cuongpv*/
END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*

*/
ALTER PROCEDURE [dbo].[Proc_BA_GetOverBalanceBook]
    @BranchID UNIQUEIDENTIFIER = NULL ,
    @AccountNumber NVARCHAR(25) = NULL ,
    @BankAccountID UNIQUEIDENTIFIER = NULL ,
    @CurrencyID NVARCHAR(3) = NULL ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @IsWorkingWithManagementBook BIT ,
    @IncludeDependentBranch BIT
AS 
    SET NOCOUNT ON		
	/*add by cuongpv xu ly cach lam tron*/
	DECLARE @tbResult TABLE(
		RowNum BIGINT,
		ID UNIQUEIDENTIFIER,
		BankAccount NVARCHAR(50),
		BankName NVARCHAR(512),
		BankBranchName NVARCHAR(512),
		OpenAmount MONEY,
		DebitAmount MONEY,
		CreditAmount MONEY,
		CloseAmount MONEY
	)
	/*end add by cuongpv*/
    DECLARE @AccountNumberPercent NVARCHAR(26)
    SET @AccountNumberPercent = @AccountNumber + '%'

	IF(@CurrencyID = 'VND')
	BEGIN
		/*Add by cuongpv de sua cach lam tron*/
			DECLARE @tbDataGL TABLE(
				ID uniqueidentifier,
				BranchID uniqueidentifier,
				ReferenceID uniqueidentifier,
				TypeID int,
				Date datetime,
				PostedDate datetime,
				No nvarchar(25),
				InvoiceDate datetime,
				InvoiceNo nvarchar(25),
				Account nvarchar(25),
				AccountCorresponding nvarchar(25),
				BankAccountDetailID uniqueidentifier,
				CurrencyID nvarchar(3),
				ExchangeRate decimal(25, 10),
				DebitAmount decimal(25,0),
				DebitAmountOriginal decimal(25,0),
				CreditAmount decimal(25,0),
				CreditAmountOriginal decimal(25,0),
				Reason nvarchar(512),
				Description nvarchar(512),
				VATDescription nvarchar(512),
				AccountingObjectID uniqueidentifier,
				EmployeeID uniqueidentifier,
				BudgetItemID uniqueidentifier,
				CostSetID uniqueidentifier,
				ContractID uniqueidentifier,
				StatisticsCodeID uniqueidentifier,
				InvoiceSeries nvarchar(25),
				ContactName nvarchar(512),
				DetailID uniqueidentifier,
				RefNo nvarchar(25),
				RefDate datetime,
				DepartmentID uniqueidentifier,
				ExpenseItemID uniqueidentifier,
				OrderPriority int,
				IsIrrationalCost bit
			)

			INSERT INTO @tbDataGL
			SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
			/*end add by cuongpv*/

			SELECT  ROW_NUMBER() OVER ( ORDER BY B.BankName, BA.BankAccount ) AS RowNum ,
					BA.ID,
					BA.BankAccount,
					B.BankName,
					BA.BankBranchName ,
					SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0.0 END) AS OpenAmount ,
					SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN  GL.DebitAmount ELSE 0.0 END) AS DebitAmount ,
					SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmount ELSE 0.0 END) AS CreditAmount ,
					SUM(GL.DebitAmount - GL.CreditAmount) AS CloseAmount
			FROM    @tbDataGL GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
					INNER JOIN dbo.BankAccountDetail BA ON GL.BankAccountDetailID = BA.ID          
					INNER JOIN dbo.Bank B ON BA.BankID =  B.ID
			WHERE   GL.PostedDate <= @ToDate
					AND ( GL.Account LIKE @AccountNumberPercent )
					AND ( GL.BankAccountDetailID = @BankAccountID
						  OR @BankAccountID IS NULL
						)
					AND ( @CurrencyID IS NULL
						  OR GL.CurrencyID = @CurrencyID
						)
            
			GROUP BY BA.ID, 
				BA.BankAccount ,
				B.BankName,BA.BankBranchName
			HAVING
				SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0.0 END)<>0 OR 
				SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN  GL.DebitAmount ELSE 0.0 END)<>0 OR 
				SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmount ELSE 0.0 END)<>0 OR 
				SUM(GL.DebitAmount - GL.CreditAmount)<>0
	END
	ELSE
	BEGIN
		/*add by cuongpv*/
		SELECT  ROW_NUMBER() OVER ( ORDER BY B.BankName, BA.BankAccount ) AS RowNum ,
					BA.ID,
					BA.BankAccount,
					B.BankName,
					BA.BankBranchName ,
					SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal- GL.CreditAmountOriginal ELSE 0.0 END) AS OpenAmount ,
					SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.DebitAmountOriginal ELSE 0.0 END) AS DebitAmount ,
					SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmountOriginal ELSE 0.0 END) AS CreditAmount ,
					SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) AS CloseAmount
			FROM    dbo.GeneralLedger GL
					INNER JOIN dbo.BankAccountDetail BA ON GL.BankAccountDetailID = BA.ID          
					INNER JOIN dbo.Bank B ON BA.BankID =  B.ID
			WHERE   GL.PostedDate <= @ToDate
					AND ( GL.Account LIKE @AccountNumberPercent )
					AND ( GL.BankAccountDetailID = @BankAccountID
						  OR @BankAccountID IS NULL
						)
					AND ( @CurrencyID IS NULL
						  OR GL.CurrencyID = @CurrencyID
						)
            
			GROUP BY BA.ID, 
				BA.BankAccount ,
				B.BankName,BA.BankBranchName
			HAVING
				SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal- GL.CreditAmountOriginal ELSE 0.0 END)<>0 OR 
				SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.DebitAmountOriginal ELSE 0.0 END)<>0 OR  
				SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN GL.CreditAmountOriginal ELSE 0.0 END)<>0 OR 
				SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal)<>0 
	END
	/*end add by cuongpv*/
            
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO




ALTER PROCEDURE [dbo].[Proc_BA_GetBookDepositListDetail]
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @CurrencyID NVARCHAR(3) ,
    @AccountNumber NVARCHAR(25) ,
    @BankAccountID UNIQUEIDENTIFIER ,
    @IsSimilarSum BIT = 0 ,
    @IsSoftOrderVoucher BIT = 0
AS
    BEGIN
        SET NOCOUNT ON
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        CREATE TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
              BankAccount NVARCHAR(500)  ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25)  ,
              RefType INT ,
              JournalMemo NVARCHAR(255)  ,
              CorrespondingAccountNumber NVARCHAR(20)
                 ,
              ExchangeRate DECIMAL ,
              DebitAmountOC DECIMAL ,
              DebitAmount DECIMAL ,
              CreditAmountOC DECIMAL ,
              CreditAmount DECIMAL ,
              ClosingAmountOC DECIMAL ,
              ClosingAmount DECIMAL ,
              AccountObjectCode NVARCHAR(255)
                 ,
              AccountObjectName NVARCHAR(255)
                 ,
              EmployeeCode NVARCHAR(255)  ,
              EmployeeName NVARCHAR(255)  ,
              ProjectWorkCode NVARCHAR(20)
                 ,
              ProjectWorkName NVARCHAR(128)
                 ,
              

              ExpenseItemCode NVARCHAR(20)
                 ,
              ExpenseItemName NVARCHAR(128)
                 ,
              ListItemCode NVARCHAR(20)  ,
              ListItemName NVARCHAR(128)  ,
              ContractCode NVARCHAR(50)  ,

              BranchID UNIQUEIDENTIFIER ,
              BranchCode NVARCHAR(25)  ,
              BranchName NVARCHAR(255)  ,
              IsBold BIT ,
              PaymentType INT ,
              OrderType INT ,
              RefOrder INT
            )	
		
        CREATE TABLE #Result1
            (
              RowNum INT PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
              BankAccount NVARCHAR(500)  ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255),
              CorrespondingAccountNumber NVARCHAR(20) ,
              ExchangeRate DECIMAL ,
              DebitAmountOC DECIMAL ,
              DebitAmount DECIMAL ,
              CreditAmountOC DECIMAL ,
              CreditAmount DECIMAL ,
              ClosingAmountOC DECIMAL ,
              ClosingAmount DECIMAL ,
              AccountObjectCode NVARCHAR(255)
                 ,
              AccountObjectName NVARCHAR(255)
                 ,
              EmployeeCode NVARCHAR(255)  ,
              EmployeeName NVARCHAR(255)  ,

              ProjectWorkCode NVARCHAR(20) ,
              ProjectWorkName NVARCHAR(128) ,
              

              ExpenseItemCode NVARCHAR(20) ,
              ExpenseItemName NVARCHAR(128) ,
              ListItemCode NVARCHAR(20) ,
              ListItemName NVARCHAR(128)  ,
              ContractCode NVARCHAR(50) ,

              BranchID UNIQUEIDENTIFIER ,
              BranchCode NVARCHAR(25)  ,
              BranchName NVARCHAR(255)  ,
              IsBold BIT ,
              PaymentType INT ,
              OrderType INT ,
              RefOrder INT
            )	
		
        DECLARE @BankAccountAll UNIQUEIDENTIFIER/*tất cả tài khoản ngân hàng*/
        SET @BankAccountAll = 'A0624CFA-D105-422f-BF20-11F246704DC3'
		
        DECLARE @AccountNumberPercent NVARCHAR(25)
        SET @AccountNumberPercent = @AccountNumber + '%'
    
        DECLARE @ClosingAmountOC DECIMAL(29, 4)
        DECLARE @ClosingAmount DECIMAL(29, 4)
   	    DECLARE @tblListBankAccountDetail TABLE
            (
			  ID UNIQUEIDENTIFIER,
              BankAccount NVARCHAR(Max) ,
              BankAccountName NVARCHAR(MAX) 
            ) 
        if(@BankAccountID is null)     
        INSERT  INTO @tblListBankAccountDetail
                SELECT  BAD.ID,
				        BAD.BankAccount,
				        BAD.BankName
                FROM    BankAccountDetail BAD,
				        Bank BA
                WHERE BAD.BankID = BA.ID    
		else 	
			INSERT  INTO @tblListBankAccountDetail
                SELECT  BAD.ID,
				        BAD.BankAccount,
				        BAD.BankName
                FROM    BankAccountDetail BAD,
				        Bank BA
                WHERE BAD.BankID = BA.ID  
				and BAD.ID =  @BankAccountID 
	

        IF @ClosingAmount IS NULL
            SET @ClosingAmount = 0
        IF @ClosingAmountOC IS NULL
            SET @ClosingAmountOC = 0
      

        INSERT  #Result
                ( BankAccount ,
                  JournalMemo ,
                  DebitAmount ,
                  DebitAmountOC ,
                  CreditAmount ,
                  CreditAmountOC ,
                  ClosingAmountOC ,
                  ClosingAmount ,
                  IsBold ,
                  OrderType    
			  
                )
                SELECT  







		                BA.BankAccount + ' - ' + BA.BankAccountName,
                        N'Số dư đầu kỳ' AS JournalMemo ,
                        $0 AS DebitAmountOC ,
                        $0 AS DebitAmount ,
                        $0 AS CreditAmountOC ,
                        $0 AS CreditAmount ,
                        ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) ,
                        ISNULL(SUM(GL.DebitAmount - GL.CreditAmount), 0) ,
                        1 ,
                        0
                FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
				        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID


                WHERE   ( GL.PostedDate < @FromDate )
                        AND GL.Account LIKE @AccountNumberPercent








                        AND ( @CurrencyID IS NULL
                              OR GL.CurrencyID = @CurrencyID
                            )
                GROUP BY BA.BankAccount , BA.BankAccountName








                HAVING  ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) <> 0
                        OR ISNULL(SUM(GL.DebitAmount - GL.CreditAmount), 0) <> 0

       INSERT  #Result
	   (
              RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder )
                    SELECT  GL.ReferenceID ,












	                        BA.BankAccount + ' - ' + BA.BankAccountName,
                            GL.PostedDate ,
                            GL.RefDate ,
                            GL.RefNo AS RefNoFinance ,

                            GL.Description,
                            GL.AccountCorresponding ,
                            GL.ExchangeRate ,
                            ISNULL(GL.DebitAmountOriginal, 0) AS DebitAmountOC ,
                            ISNULL(GL.DebitAmount, 0) AS DebitAmount ,
                            ISNULL(GL.CreditAmountOriginal, 0) AS CreditAmountOC ,
                            ISNULL(GL.CreditAmount, 0) AS CreditAmount ,
                            $0 AS ClosingAmountOC ,
                            $0 AS ClosingAmount ,
                            AO.AccountingObjectCode ,
                            AO.AccountingObjectName AS AccountObjectName ,



                            EI.ExpenseItemCode ,
                            EI.ExpenseItemName ,


                            GL.BranchID ,
                            0 ,
                            CASE WHEN GL.DebitAmount <> 0 THEN 0
                                 ELSE 1
                            END AS PaymentType ,
                            1 ,
                            0
                    FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
					        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID


                            LEFT JOIN dbo.AccountingObject AO ON GL.AccountingObjectID = AO.ID
                            LEFT JOIN dbo.ExpenseItem EI ON EI.ID = GL.ExpenseItemID
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND GL.Account LIKE @AccountNumberPercent








                            AND ( @CurrencyID IS NULL
                                  OR GL.CurrencyID = @CurrencyID
                                )
                            AND ( GL.DebitAmountOriginal <> 0
                                  OR GL.CreditAmountOriginal <> 0
                                  OR GL.DebitAmount <> 0
                                  OR GL.CreditAmount <> 0
                                )
                    ORDER BY BA.BankAccount ,
                            PostedDate ,
                            RefDate ,
                            PaymentType ,
                            RefNo
                     
          
   
        DECLARE @BankAccount NVARCHAR(500)
        DECLARE @CloseAmountOC MONEY
        DECLARE @CloseAmount MONEY
        SET @BankAccount = NULL
        INSERT  INTO #Result1
		(     RowNum,
		      RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder)
                SELECT  ROW_NUMBER() OVER ( ORDER BY BankAccount , OrderType, PostedDate , CASE
                                                              WHEN @IsSoftOrderVoucher = 1
                                                              THEN RefOrder
                                                              ELSE 0
                                                              END, RefDate , PaymentType , RefNo ) ,
                        RefID ,
                        BankAccount ,
                        PostedDate ,
                        RefDate ,
                        RefNo ,
                        JournalMemo ,
                        CorrespondingAccountNumber ,
                        ExchangeRate ,
                        DebitAmountOC ,
                        DebitAmount ,
                        CreditAmountOC ,
                        CreditAmount ,
                        ClosingAmountOC ,
                        ClosingAmount ,
                        AccountObjectCode ,
                        AccountObjectName ,

                        ExpenseItemCode ,
                        ExpenseItemName ,
                        BranchID ,
						IsBold  ,
					    PaymentType  ,
						OrderType  ,
						RefOrder
                FROM    #Result
                ORDER BY BankAccount ,
                        OrderType ,
                        PostedDate ,
                        CASE WHEN @IsSoftOrderVoucher = 1 THEN RefOrder
                             ELSE 0
                        END ,
                        RefDate ,
                        PaymentType ,
                        RefNo
        
      
        SET @CloseAmount = 0
        SET @CloseAmountOC = 0
      

        UPDATE  #Result1
        SET

                @CloseAmount = ( CASE WHEN OrderType = 0
                                      THEN ( CASE WHEN ClosingAmount = 0
                                                  THEN 0
                                                  ELSE ClosingAmount
                                             END )
                                      WHEN @BankAccount <> BankAccount
                                      THEN DebitAmount - CreditAmount
                                      ELSE @CloseAmount + DebitAmount
                                           - CreditAmount
                                 END ) ,
                @CloseAmountOC = ( CASE WHEN OrderType = 0
                                        THEN ( CASE WHEN ClosingAmountOC = 0
                                                    THEN 0
                                                    ELSE ClosingAmountOC
                                               END )
                                        WHEN @BankAccount <> BankAccount
                                        THEN DebitAmountOC - CreditAmountOC
                                        ELSE @CloseAmountOC + DebitAmountOC
                                             - CreditAmountOC
                                   END ) ,
                ClosingAmount = @CloseAmount ,
                ClosingAmountOC = @CloseAmountOC ,
                @BankAccount = BankAccount
        
        SELECT  *
        FROM    #Result1
        ORDER BY BankAccount ,
                OrderType ,
                PostedDate ,
                CASE WHEN @IsSoftOrderVoucher = 1 THEN RefOrder
                     ELSE 0
                END ,
                RefDate ,
                PaymentType ,
                RefNo
     
        DROP TABLE #Result
        DROP TABLE #Result1
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER FUNCTION [dbo].[Func_GetB03_DN]
(
	@BranchID UNIQUEIDENTIFIER
	,@IncludeDependentBranch BIT
	,@FromDate DATETIME
	,@ToDate DATETIME
	,@PrevFromDate DATETIME
	,@PrevToDate DATETIME	
)
RETURNS 
@Result TABLE 
(	
		ItemID UNIQUEIDENTIFIER
      ,ItemCode NVARCHAR(25)
      ,ItemName NVARCHAR(255)
      ,ItemNameEnglish NVARCHAR(255)
      ,ItemIndex INT
      ,Description NVARCHAR(255)
      ,FormulaType INT
      ,FormulaFrontEnd NVARCHAR(MAX)
      ,Formula XML
      ,Hidden BIT
      ,IsBold BIT
      ,IsItalic BIT
      ,Amount DECIMAL(25,4)
      ,PrevAmount  DECIMAL(25,4)    
)
AS
BEGIN

	DECLARE @AccountingSystem INT	
    SET @AccountingSystem = 48
    
    set @PrevToDate = DATEADD(DD,-1,@FromDate)
	declare @CalPrevDate nvarchar(20)
	declare @CalPrevMonth nvarchar(20)
	declare @CalPrevDay nvarchar(20)
	set @CalPrevDate = CAST(day(@FromDate) AS varchar) + CAST(month(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevMonth = CAST(month(@FromDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevDay = CAST(day(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar)
	if @CalPrevDate = '113112'
	 set @PrevFromDate = convert(DATETIME,dbo.ctod('D',dbo.godtos('M', -12, @FromDate)),103)
	else if (@CalPrevMonth in (55,77,1010,1212) and @CalPrevDay in (131) )
	 set @PrevFromDate = DATEADD(DD,-30,@FromDate)
	else if (@CalPrevMonth in (11,22,88,44,66,99,1111) and @CalPrevDay in (131,130) )
	 set @PrevFromDate = DATEADD(DD,-31,@FromDate)
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 = 0)
	 set @PrevFromDate = DATEADD(DD,-29,@FromDate)
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 <> 0)
	 set @PrevFromDate = DATEADD(DD,-28,@FromDate)
	else if ( (@CalPrevMonth in (13,1012) and @CalPrevDay = '131') or (@CalPrevMonth in (46,79) and @CalPrevDay = '130') )
	 set @PrevFromDate = DATEADD(QQ,-1,@FromDate)
	else set @PrevFromDate = DATEADD(DD,-1-DATEDIFF(day, @FromDate, @ToDate),@FromDate)

	DECLARE @ReportID NVARCHAR(100)
	SET @ReportID = '3'
	
	DECLARE @ItemBeginingCash UNIQUEIDENTIFIER	

	SET @ItemBeginingCash = (Select ItemID From FRTemplate Where ItemCode = '60' AND ReportID = 3 AND AccountingSystem = @AccountingSystem)

	DECLARE @tblItem TABLE
	(
		ItemID				UNIQUEIDENTIFIER
		,ItemIndex			INT
		,ItemName			NVARCHAR(255)	
		,OperationSign		INT
		,OperandString		NVARCHAR(255)	
		,AccountNumber		NVARCHAR(25)
		,AccountNumberPercent NVARCHAR(25) 
		,CorrespondingAccountNumber	VARCHAR(25)
	)
	
	INSERT @tblItem
	SELECT ItemID 
		, ItemIndex
		, ItemName
		, x.r.value('@OperationSign','INT')
		, x.r.value('@OperandString','nvarchar(255)')
		, x.r.value('@AccountNumber','nvarchar(25)') 
		, x.r.value('@AccountNumber','nvarchar(25)') + '%'
		, CASE WHEN x.r.value('@CorrespondingAccountNumber','nvarchar(25)') <>'' 
			THEN x.r.value('@CorrespondingAccountNumber','nvarchar(25)') + '%'
			ELSE ''
		 END
	FROM dbo.FRTemplate
	CROSS APPLY Formula.nodes('/root/DetailFormula') AS x(r)
	WHERE AccountingSystem = @AccountingSystem
		  AND ReportID = @ReportID
		  AND FormulaType = 0 AND Formula IS NOT NULL 
	
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/
		  
	DECLARE @Balance TABLE 
		(
			AccountNumber NVARCHAR(25)	
			,CorrespondingAccountNumber NVARCHAR(25)		
			,PrevBusinessAmount DECIMAL(25,4)
			,BusinessAmount DECIMAL(25,4)							
			,PrevInvestmentAmount DECIMAL(25,4)
			,InvestmentAmount DECIMAL(25,4)
			,PrevFinancialAmount DECIMAL(25,4)
			,FinancialAmount DECIMAL(25,4)
		)			
		
	INSERT INTO @Balance			
	SELECT 
		GL.Account
		,GL.AccountCorresponding		
		,SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND(AC.ActivityID IS NULL OR AC.ActivityID = 0) 
				THEN DebitAmount  ELSE 0 END) AS PrevBusinessAmount
		,SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND (AC.ActivityID IS NULL OR AC.ActivityID = 0) 
				THEN DebitAmount ELSE 0 END) AS BusinessAmount		
		,SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND  AC.ActivityID = 1 
				THEN DebitAmount  ELSE 0 END) AS PrevInvestmentAmount
		,SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 1 
				THEN DebitAmount ELSE 0 END) AS InvestmentAmount		
		,SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND AC.ActivityID = 2 
				THEN DebitAmount  ELSE 0 END) AS PrevFinancialAmount
		,SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 2 
				THEN DebitAmount ELSE 0 END) AS FinancialAmount		
	FROM @tbDataGL GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
		INNER JOIN dbo.Account A ON GL.Account = A.AccountNumber
		LEFT JOIN [dbo].[FRB03ReportDetailActivity] AC   ON GL.DetailID = Ac.RefDetailID
													/*Khi JOIN thêm điều kiện sổ*/
	WHERE PostedDate BETWEEN @PrevFromDate AND @ToDate	
	and GL.No <> 'OPN'
	GROUP BY
		GL.Account
		,GL.AccountCorresponding
	HAVING
		SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND(AC.ActivityID IS NULL OR AC.ActivityID = 0) 
			THEN DebitAmount  ELSE 0 END)<>0
		OR SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND (AC.ActivityID IS NULL OR AC.ActivityID = 0) 
			THEN DebitAmount ELSE 0 END) <>0
		OR SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND  AC.ActivityID = 1 
			THEN DebitAmount  ELSE 0 END) <>0
		OR SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 1 
			THEN DebitAmount ELSE 0 END)<>0
		OR SUM(CASE WHEN PostedDate BETWEEN @PrevFromDate AND @PrevToDate AND AC.ActivityID = 2 
			THEN DebitAmount  ELSE 0 END) <>0
		OR SUM (CASE WHEN PostedDate BETWEEN @FromDate AND @ToDate AND AC.ActivityID = 2 
			THEN DebitAmount ELSE 0 END) <>0
	
	DECLARE @DebitBalance TABLE
	(	
		ItemID				UNIQUEIDENTIFIER
		,OperationSign		INT
		,AccountNumber NVARCHAR(25)
		,AccountKind	INT
		,PrevDebitAmount Decimal(25,4)
		,DebitAmount Decimal(25,4)
	)
	
	INSERT @DebitBalance
	SELECT
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
		,SUM(CASE WHEN  GL.PostedDate < @PrevFromDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0 END)	
		,SUM(GL.DebitAmount - GL.CreditAmount)		
	FROM  @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
	INNER JOIN @tblItem I ON GL.Account LIKE I.AccountNumberPercent	
	LEFT JOIN dbo.Account A ON A.AccountNumber = I.AccountNumber
	WHERE GL.PostedDate < @FromDate	
	    AND I.OperandString = 'DUNO'
	    AND I.ItemID = @ItemBeginingCash
	Group By
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
	
	UNION ALL	
	SELECT
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
		,SUM(CASE WHEN  GL.PostedDate <= @PrevToDate THEN GL.DebitAmount - GL.CreditAmount ELSE 0 END)	
		,SUM(GL.DebitAmount - GL.CreditAmount)		
	FROM  @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
	INNER JOIN @tblItem I ON GL.Account LIKE I.AccountNumberPercent	
	LEFT JOIN dbo.Account A ON A.AccountNumber = I.AccountNumber
	WHERE GL.PostedDate <= @ToDate
	    AND I.OperandString = 'DUNO'
	    AND I.ItemID <> @ItemBeginingCash
	Group By
		I.ItemID
		,I.OperationSign
		,I.AccountNumber
		,A.AccountGroupKind
		
		   
	UPDATE @DebitBalance
	SET PrevDebitAmount = 0 WHERE AccountKind = 1 OR (AccountKind NOT IN (0,1)  AND PrevDebitAmount < 0) 
	
	UPDATE @DebitBalance
	SET DebitAmount = 0 WHERE AccountKind = 1 OR (AccountKind NOT IN (0,1)  AND DebitAmount < 0) 
	
	DECLARE @tblMasterDetail Table
		(
			ItemID UNIQUEIDENTIFIER
			,DetailItemID UNIQUEIDENTIFIER
			,OperationSign INT
			,Grade INT
			,PrevAmount DECIMAL(25,4)
			,Amount DECIMAL(25,4)		
		)	
	
	
	INSERT INTO @tblMasterDetail
	SELECT I.ItemID
			,NULL
			,1
			,-1
			,SUM(I.PrevAmount)
			,SUM(I.Amount)
	FROM	
		(SELECT			
			I.ItemID
			,SUM(CASE				
					WHEN I.OperandString = 'PhatsinhDU' THEN b.PrevBusinessAmount + b.PrevInvestmentAmount + b.PrevFinancialAmount
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_DAUTU' THEN b.PrevInvestmentAmount
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_TAICHINH' THEN b.PrevFinancialAmount
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_SXKD' THEN b.PrevBusinessAmount
				 END * I.OperationSign) AS  PrevAmount			
			,SUM(CASE 
					WHEN I.OperandString = 'PhatsinhDU' THEN (b.BusinessAmount + b.InvestmentAmount + b.FinancialAmount)
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_DAUTU' THEN (b.InvestmentAmount)
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_TAICHINH' THEN (b.FinancialAmount)
					WHEN I.OperandString = 'PhatsinhDUChiTietTheoHD_SXKD' THEN (b.BusinessAmount)
			    END * I.OperationSign) AS Amount
			FROM @tblItem I 			
				 INNER JOIN @Balance B 
					ON (B.AccountNumber like I.AccountNumberPercent)
					AND (B.CorrespondingAccountNumber like I.CorrespondingAccountNumber)	
				WHERE I.OperandString IN ('PhatsinhDU','PhatsinhDUChiTietTheoHD_DAUTU','PhatsinhDUChiTietTheoHD_TAICHINH','PhatsinhDUChiTietTheoHD_SXKD')					
			GROUP BY I.ItemID
		UNION ALL
			SELECT
		    I.ItemID
			,SUM((b.PrevBusinessAmount + b.PrevInvestmentAmount + b.PrevFinancialAmount)* I.OperationSign) AS  PrevAmount			
			,SUM((b.BusinessAmount + b.InvestmentAmount + b.FinancialAmount)* I.OperationSign) AS Amount
			FROM @tblItem I 
				 INNER JOIN @Balance B 
					ON (B.AccountNumber like I.AccountNumberPercent)
			WHERE I.OperandString = 'PhatsinhNO'				
			GROUP BY I.ItemID
		UNION ALL
			SELECT
		    I.ItemID
			,SUM((b.PrevBusinessAmount + b.PrevInvestmentAmount + b.PrevFinancialAmount)* I.OperationSign) AS  PrevAmount			
			,SUM((b.BusinessAmount + b.InvestmentAmount + b.FinancialAmount)* I.OperationSign) AS Amount
			FROM @tblItem I 
				 INNER JOIN @Balance B 
					ON (B.CorrespondingAccountNumber like I.AccountNumberPercent)
			WHERE I.OperandString = 'PhatsinhCO'		
					
			GROUP BY I.ItemID
			
		UNION ALL
			SELECT I.ItemID
				,I.PrevDebitAmount * I.OperationSign
				,I.DebitAmount * I.OperationSign
			FROM @DebitBalance I
		 ) AS I
	Group By I.ITemID
		
	
		
	
	       
	    
	INSERT @tblMasterDetail	
	SELECT ItemID 
	, x.r.value('@ItemID','NVARCHAR(100)')
	, x.r.value('@OperationSign','INT')	
	, 0
	, 0.0	
	, 0.0
	FROM dbo.FRTemplate 
	CROSS APPLY Formula.nodes('/root/MasterFormula') AS x(r)
	WHERE AccountingSystem = @AccountingSystem
		  AND ReportID = @ReportID
		  AND FormulaType = 1 AND Formula IS NOT NULL 
	
	;
	WITH V(ItemID,DetailItemID,PrevAmount,Amount,OperationSign)
		AS 
		(
			SELECT ItemID,DetailItemID,PrevAmount, Amount, OperationSign
			FROM @tblMasterDetail WHERE Grade = -1 
			UNION ALL
			SELECT B.ItemID
				, B.DetailItemID				
				, V.PrevAmount
				, V.Amount
				, B.OperationSign * V.OperationSign AS OperationSign
			FROM @tblMasterDetail B, V 
			WHERE B.DetailItemID = V.ItemID	
		)
		
	INSERT @Result      
	SELECT 
		FR.ItemID
		, FR.ItemCode
		, FR.ItemName
		, FR.ItemNameEnglish
		, FR.ItemIndex
		, FR.Description		
		, FR.FormulaType
		, CASE WHEN FR.FormulaType = 1 THEN FR.FormulaFrontEnd	ELSE ''	END AS FormulaFrontEnd
		, CASE WHEN FR.FormulaType = 1 THEN FR.Formula ELSE NULL END AS Formula
		, FR.Hidden
		, FR.IsBold
		, FR.IsItalic
		,CASE WHEN FR.Formula IS NOT NULL THEN ISNULL(X.Amount,0)
			ELSE X.Amount
		 END AS Amount
		,CASE WHEN FR.Formula IS NOT NULL THEN ISNULL(X.PrevAmount,0) 
			ELSE X.PrevAmount 
		END AS PrevAmount
		FROM		
		(
			SELECT V.ItemID
				,SUM(V.OperationSign * V.Amount)  AS Amount
				,SUM(V.OperationSign * V.PrevAmount) AS PrevAmount
			FROM V
			GROUP BY ItemID
		) AS X		
		RIGHT JOIN dbo.FRTemplate FR ON FR.ItemID = X.ItemID	
	WHERE AccountingSystem = @AccountingSystem
		  AND ReportID = @ReportID
		  and Hidden = 0
	Order by ItemIndex	
	RETURN 
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER FUNCTION [dbo].[Func_GetB02_DN]
    (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @PrevFromDate DATETIME ,
      @PrevToDate DATETIME
    )
RETURNS @Result TABLE
    (
      ItemID UNIQUEIDENTIFIER ,
      ItemCode NVARCHAR(25) ,
      ItemName NVARCHAR(255) ,
      ItemNameEnglish NVARCHAR(255) ,
      ItemIndex INT ,
      Description NVARCHAR(255) ,
      FormulaType INT ,
      FormulaFrontEnd NVARCHAR(MAX) ,
      Formula XML ,
      Hidden BIT ,
      IsBold BIT ,
      IsItalic BIT
      ,
      Amount DECIMAL(25, 4) ,
      PrevAmount DECIMAL(25, 4)
    )
AS 
    BEGIN
	DECLARE @AccountingSystem INT	
            SET @AccountingSystem = 48
        DECLARE @ReportID NVARCHAR(100)
        SET @ReportID = '2'/*báo cáo kết quả HĐ kinh doanh*/
    set @PrevToDate = DATEADD(DD,-1,@FromDate)
	declare @CalPrevDate nvarchar(20)
	declare @CalPrevMonth nvarchar(20)
	declare @CalPrevDay nvarchar(20)
	set @CalPrevDate = CAST(day(@FromDate) AS varchar) + CAST(month(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevMonth = CAST(month(@FromDate) AS varchar) + CAST(month(@ToDate) AS varchar)
	set @CalPrevDay = CAST(day(@FromDate) AS varchar) + CAST(day(@ToDate) AS varchar)
	if @CalPrevDate = '113112'
	 set @PrevFromDate = convert(DATETIME,dbo.ctod('D',dbo.godtos('M', -12, @FromDate)),103)
	else if (@CalPrevMonth in (55,77,1010,1212) and @CalPrevDay in (131) )
	 set @PrevFromDate = DATEADD(DD,-30,@FromDate)
	else if (@CalPrevMonth in (11,22,88,44,66,99,1111) and @CalPrevDay in (131,130) )
	 set @PrevFromDate = DATEADD(DD,-31,@FromDate)
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 = 0)
	 set @PrevFromDate = DATEADD(DD,-29,@FromDate)
	else if (@CalPrevMonth in (33) and @CalPrevDay in (131) and CAST(year(@FromDate) AS varchar)%4 <> 0)
	 set @PrevFromDate = DATEADD(DD,-28,@FromDate)
	else if ( (@CalPrevMonth in (13,1012) and @CalPrevDay = '131') or (@CalPrevMonth in (46,79) and @CalPrevDay = '130') )
	 set @PrevFromDate = DATEADD(QQ,-1,@FromDate)
	else set @PrevFromDate = DATEADD(DD,-1-DATEDIFF(day, @FromDate, @ToDate),@FromDate)
	
        DECLARE @tblItem TABLE
            (
              ItemID UNIQUEIDENTIFIER ,
              ItemName NVARCHAR(255) ,
              OperationSign INT ,
              OperandString NVARCHAR(255) ,
              AccountNumber VARCHAR(25) ,
              CorrespondingAccountNumber VARCHAR(25) ,
              IsDetailGreaterThanZero BIT
	      )
	
        INSERT  @tblItem
                SELECT  ItemID ,
                        ItemName ,
                        x.r.value('@OperationSign', 'INT') ,
                        x.r.value('@OperandString', 'nvarchar(255)') ,
                        x.r.value('@AccountNumber', 'nvarchar(25)') + '%' ,
                        CASE WHEN x.r.value('@CorrespondingAccountNumber',
                                            'nvarchar(25)') <> ''
                             THEN x.r.value('@CorrespondingAccountNumber',
                                            'nvarchar(25)') + '%'
                             ELSE ''
                        END ,
                        CASE WHEN FormulaType = 0 THEN CAST(0 AS BIT)
                             ELSE CAST(1 AS BIT)
                        END
                FROM    FRTemplate
                        CROSS APPLY Formula.nodes('/root/DetailFormula') AS x ( r )
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
                        AND ( FormulaType = 0/*chỉ tiêu chi tiết*/
                              OR FormulaType = 2/*chỉ tiêu chi tiết chỉ được lấy số liệu khi kết quả>0*/
                            )
                        AND Formula IS NOT NULL
		
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate BETWEEN @PrevFromDate AND @ToDate
		/*end add by cuongpv*/

        DECLARE @Balance TABLE
            (
              AccountNumber NVARCHAR(25) ,
              CorrespondingAccountNumber NVARCHAR(25) ,
              PostedDate DATETIME ,
              CreditAmount DECIMAL(25, 4) ,
              DebitAmount DECIMAL(25, 4) ,
              CreditAmountDetailBy DECIMAL(25, 4) ,
              DebitAmountDetailBy DECIMAL(25, 4) ,
              IsDetailBy BIT,
              /*add by hoant 16.09.2016 theo cr 116741*/
                CreditAmountByBussinessType0 DECIMAL(25, 4) ,/*Có Chi tiết theo loại  0-Chiết khấu thương mai*/
               CreditAmountByBussinessType1 DECIMAL(25, 4) ,/*Có Chi tiết theo loại  1 - Giảm giá hàng bán*/
               CreditAmountByBussinessType2 DECIMAL(25, 4) ,/*Có Chi tiết theo loại   2- trả lại hàng bán*/
              DebitAmountByBussinessType0 DECIMAL(25, 4) ,/*Nợ Chi tiết theo loại  0-Chiết khấu thương mai*/
              DebitAmountByBussinessType1 DECIMAL(25, 4) ,/*Nợ Chi tiết theo loại  1 - Giảm giá hàng bán*/
              DebitAmountByBussinessType2 DECIMAL(25, 4) /*Nợ Chi tiết theo loại   2- trả lại hàng bán*/
            )			
/*
1. Doanh thu bán hàng và cung cấp dịch vụ

PS Có TK 511 (không kể PSĐƯ N911/C511, không kể các nghiệp vụ: Chiết khấu thương mại (bán hàng),

 Giảm giá hàng bán, Trả lại hàng bán)
  – PS Nợ TK 511 (không kể PSĐƯ N511/911, không kể các nghiệp vụ: Chiết khấu thương mại (bán hàng), 
  Giảm giá hàng bán, Trả lại hàng bán)
*/
		
        INSERT  INTO @Balance
                SELECT  GL.Account ,
                        GL.AccountCorresponding ,
                        GL.PostedDate ,
                        SUM(ISNULL(CreditAmount, 0)) AS CreditAmount ,
                        SUM(ISNULL(DebitAmount, 0)) AS DebitAmount ,
                        0 AS CreditAmountDetailBy ,
                        0 AS DebitAmountDetailBy ,
                        0,
                        SUM(CASE WHEN (GL.TypeID in ('320','321','322','323','324','325','350','360') and gl.Account like '511%')
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType0 ,
                        SUM(CASE WHEN GL.TypeID =340
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType1 
                            ,
                        SUM(CASE WHEN GL.TypeID =330
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType2 ,
                        SUM(CASE WHEN (GL.TypeID in ('320','321','322','323','324','325','350','360') and gl.Account like '511%')
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType0
                            ,
                        SUM(CASE WHEN GL.TypeID =340
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType1
                            ,
                        SUM(CASE WHEN GL.TypeID =330
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType2
                FROM    @tbDataGL GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                WHERE   PostedDate BETWEEN @PrevFromDate AND @ToDate
                GROUP BY GL.Account ,
                        GL.AccountCorresponding,
                        GL.PostedDate                     
        OPTION  ( RECOMPILE )


        DECLARE @tblMasterDetail TABLE
            (
              ItemID UNIQUEIDENTIFIER ,
              DetailItemID UNIQUEIDENTIFIER ,
              OperationSign INT ,
              Grade INT ,
              DebitAmount DECIMAL(25, 4) ,
              PrevDebitAmount DECIMAL(25, 4)
            )	
	
	
	/*
	PhatsinhCO(511) - PhatsinhDU(911/511) + PhatsinhCO_ChitietChietKhauThuongmai(511)
	 + PhatsinhCO_ChitietGiamgiaHangBan(511) + PhatsinhCO_ChitietTralaiHangBan(511) +
	  PhatsinhNO(511) - PhatsinhDU(511/911) - PhatsinhNO_ChitietChietKhauThuongmai(511) - PhatsinhNO_ChitietGiamgiaHangBan(511) - PhatsinhNO_ChitietTralaiHangBan(511)
	*/
        INSERT  INTO @tblMasterDetail
                SELECT  I.ItemID ,
                        NULL ,
                        1 ,
                        -1
                        ,
                        CASE WHEN I.IsDetailGreaterThanZero = 0
                                  OR ( SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                                THEN ( CASE WHEN I.OperandString = 'PhatsinhCO'
                                                            THEN GL.CreditAmount
                                                            WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                            THEN GL.DebitAmount
                                                        
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                        
                                                            WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
															 THEN GL.CreditAmount
															 WHEN I.OperandString IN (
																  'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
															 THEN GL.DebitAmount
															 ELSE 0
														
                                                       END ) * I.OperationSign
                                                ELSE 0
                                           END) ) > 0
                             THEN ( SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                             THEN ( CASE WHEN I.OperandString = 'PhatsinhCO'
                                                         THEN GL.CreditAmount
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                         THEN GL.DebitAmount
                                                    
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                     
                                                              
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.CreditAmount
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.DebitAmount
                                                         ELSE 0
                                                    END ) * I.OperationSign
                                             ELSE 0
                                        END) )
                             ELSE 0
                        END AS DebitAmount ,
                        CASE WHEN I.IsDetailGreaterThanZero = 0
                                  OR ( SUM(CASE WHEN GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                                THEN CASE WHEN I.OperandString = 'PhatsinhCO'
                                                          THEN GL.CreditAmount
                                                          WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                          THEN GL.DebitAmount
            
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                         
                                                          WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.CreditAmount
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.DebitAmount
                                                         ELSE 0    
                                                     END * I.OperationSign
                                                ELSE 0
                                           END) ) > 0
                             THEN SUM(CASE WHEN GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                           THEN CASE WHEN I.OperandString = 'PhatsinhCO'
                                                     THEN GL.CreditAmount
                                                     WHEN I.OperandString IN (
                                                          'PhatsinhNO',
                                                          'PhatsinhDU' )
                                                     THEN GL.DebitAmount
                                                     
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                              
                                                              
                                                              
                                                     WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.CreditAmount
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                         THEN GL.DebitAmount
                                                         ELSE 0
                                                END * I.OperationSign
                                           ELSE 0
                                      END)
                             ELSE 0
                        END AS DebitAmount
                FROM    @tblItem I
                        INNER JOIN @Balance AS GL ON ( GL.AccountNumber LIKE I.AccountNumber )
                                                     AND ( I.OperandString <> 'PhatsinhDU'
                                                           OR ( I.OperandString = 'PhatsinhDU'
                                                              AND GL.CorrespondingAccountNumber LIKE I.CorrespondingAccountNumber
                                                              )
                                                         )
                GROUP BY I.ItemID ,
                        I.IsDetailGreaterThanZero
        OPTION  ( RECOMPILE )
			
        INSERT  @tblMasterDetail
                SELECT  ItemID ,
                        x.r.value('@ItemID', 'UNIQUEIDENTIFIER') ,
                        x.r.value('@OperationSign', 'INT') ,
                        0 ,
                        0.0 ,
                        0.0
                FROM    FRTemplate
                        CROSS APPLY Formula.nodes('/root/MasterFormula') AS x ( r )
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
                        AND FormulaType = 1
                        AND Formula IS NOT NULL 
	
	

	;
        WITH    V ( ItemID, DetailItemID, DebitAmount, PrevDebitAmount, OperationSign )
                  AS ( SELECT   ItemID ,
                                DetailItemID ,
                                DebitAmount ,
                                PrevDebitAmount ,
                                OperationSign
                       FROM     @tblMasterDetail
                       WHERE    Grade = -1
                       UNION ALL
                       SELECT   B.ItemID ,
                                B.DetailItemID ,
                                V.DebitAmount ,
                                V.PrevDebitAmount ,
                                B.OperationSign * V.OperationSign AS OperationSign
                       FROM     @tblMasterDetail B ,
                                V
                       WHERE    B.DetailItemID = V.ItemID
                     )
	INSERT    @Result
                SELECT  FR.ItemID ,
                        FR.ItemCode ,
                        FR.ItemName ,
                        FR.ItemNameEnglish ,
                        FR.ItemIndex ,
                        FR.Description ,
                        FR.FormulaType ,
                        CASE WHEN FR.FormulaType = 1 THEN FR.FormulaFrontEnd
                             ELSE ''
                        END AS FormulaFrontEnd ,
                        CASE WHEN FR.FormulaType = 1 THEN FR.Formula
                             ELSE NULL
                        END AS Formula ,
                        FR.Hidden ,
                        FR.IsBold ,
                        FR.IsItalic ,
                        ISNULL(X.Amount, 0) AS Amount ,
                        ISNULL(X.PrevAmount, 0) AS PrevAmount
                FROM    ( SELECT    V.ItemID ,
                                    SUM(V.OperationSign * V.DebitAmount) AS Amount ,
                                    SUM(V.OperationSign * V.PrevDebitAmount) AS PrevAmount
                          FROM      V
                          GROUP BY  ItemID
                        ) AS X
                        RIGHT JOIN dbo.FRTemplate FR ON FR.ItemID = X.ItemID
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
                ORDER BY ItemIndex
	
        RETURN 
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GL_GetGLAccountLedgerDiaryBook]
    @BranchID UNIQUEIDENTIFIER,
    @IncludeDependentBranch BIT,
    @StartDate DATETIME,
    @FromDate DATETIME,
    @ToDate DATETIME,    
    @AccountNumber NVARCHAR(MAX),
    @IsSimilarSum BIT
AS
    BEGIN    
        SET NOCOUNT ON       
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

		CREATE TABLE #Result
            (
             RefID UNIQUEIDENTIFIER,
             RefType INT,
             PostedDate DATETIME,
             RefNo NVARCHAR(25),
             RefDate DATETIME,
             InvDate DATETIME,
             InvNo NVARCHAR(MAX), 
             JournalMemo NVARCHAR(255),
             AccountNumber NVARCHAR(25),
			 DetailAccountNumber NVARCHAR(25) ,
             AccountCategoryKind INT,
             AccountName NVARCHAR(255),
             CorrespondingAccountNumber NVARCHAR(25),
             DebitAmount MONEY,
             CreditAmount MONEY,
             OrderType INT,
             IsBold BIT,
             OrderNumber int
            )
        CREATE TABLE #tblAccountNumber
            (
             AccountNumber NVARCHAR(25)    PRIMARY KEY,
             AccountName NVARCHAR(255)  ,
             AccountNumberPercent NVARCHAR(25)  ,
             AccountCategoryKind INT,
			 IsParent BIT
            )
			
       
	
        INSERT  #tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountName,
                        A1.AccountNumber + '%',
                        A1.AccountGroupKind,
						A1.IsParentNode
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
	            					
               
        IF @IsSimilarSum = 0 /*khong cong gop but ke toan*/
		Begin
            INSERT  #Result
                    (
                     RefID,
                     RefType,
                     PostedDate,
                     RefNo,
                     RefDate,
                     InvDate,
                     InvNo,
                     JournalMemo,
                     AccountNumber,
					 DetailAccountNumber,
                     AccountName,
                     CorrespondingAccountNumber,
                     AccountCategoryKind,
                     DebitAmount,
                     CreditAmount,
                     ORDERType,
                     IsBold,
                     OrderNumber
					
                    )
                    SELECT 
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Description AS JournalMemo,
                            AC.AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind,
                            DebitAmount,
                            CreditAmount,
                            1 AS ORDERType,
                            0,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%') 
											AND GL.DebitAmount <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND (GL.DebitAmount <> 0
                                 OR GL.CreditAmount <> 0
                                 or GL.DebitAmountOriginal <> 0
                                 OR GL.CreditAmountOriginal <> 0
                                )                           
              End
			  Else
			  Begin /*cong gop but toan thue*/
				INSERT  #Result(RefID, RefType, PostedDate, RefNo, RefDate, InvDate, InvNo, JournalMemo, AccountNumber, DetailAccountNumber, AccountName, CorrespondingAccountNumber,
                     AccountCategoryKind, DebitAmount, CreditAmount, ORDERType, IsBold, OrderNumber)
                SELECT RefID, RefType, PostedDate, RefNo, RefDate, InvDate, InvNo, JournalMemo, AccountNumber, DetailAccountNumber, AccountName, CorrespondingAccountNumber,
                     AccountCategoryKind, DebitAmount, CreditAmount, ORDERType, IsBold, OrderNumber
				FROM
				    (SELECT 
                            GL.ReferenceID as RefID,
                            GL.TypeID as RefType,
                            GL.PostedDate as PostedDate,
                            RefNo,
                            GL.RefDate as RefDate,
                            Gl.InvoiceDate as InvDate,
                            GL.InvoiceNo as InvNo,
                            GL.Reason AS JournalMemo,
                            AC.AccountNumber as AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName as AccountName,
                            GL.AccountCorresponding as CorrespondingAccountNumber,
                            AC.AccountCategoryKind as AccountCategoryKind,
                            SUM(GL.DebitAmount) as DebitAmount,
                            0 as CreditAmount,
                            1 AS ORDERType,
                            0 as IsBold,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%') 
											AND SUM(GL.DebitAmount) <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
					GROUP BY
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Reason,
                            AC.AccountNumber,
							GL.Account,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind
					HAVING SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0 
				
				UNION ALL

				SELECT 
                            GL.ReferenceID as RefID,
                            GL.TypeID as RefType,
                            GL.PostedDate as PostedDate,
                            RefNo,
                            GL.RefDate as RefDate,
                            Gl.InvoiceDate as InvDate,
                            GL.InvoiceNo as InvNo,
                            GL.Reason AS JournalMemo,
                            AC.AccountNumber as AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName as AccountName,
                            GL.AccountCorresponding as CorrespondingAccountNumber,
                            AC.AccountCategoryKind as AccountCategoryKind,
                            0 as DebitAmount,
                            SUM(GL.CreditAmount) as CreditAmount,
                            1 AS ORDERType,
                            0 as IsBold,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%') 
											AND SUM(GL.CreditAmount) <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
					GROUP BY
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Reason,
                            AC.AccountNumber,
							GL.Account,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind
					HAVING SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0 
				) T
				where T.DebitAmount > 0 OR T.CreditAmount>0
			  End      
					
       
        SELECT  AccountNumber,
                AccountName,
				IsParent,
                SUM(OpenningDebitAmount) AS OpenningDebitAmount,
                SUM(OpenningCreditAmount) AS OpenningCreditAmount,
                SUM(ClosingDebitAmount) AS ClosingDebitAmount,
                SUM(ClosingCreditAmount) AS ClosingCreditAmount,
                SUM(AccumDebitAmount) AS AccumDebitAmount,
                SUM(AccumCreditAmount) AS AccumCreditAmount,
				AccountCategoryKind
        FROM    (
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS OpenningDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate < @FromDate                       
                 GROUP BY GL.BranchID,
                        AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS ClosingDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate <= @ToDate                        
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        SUM(GL.DebitAmount) AS AccumDebitAmount,
                        SUM(GL.CreditAmount) AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate BETWEEN @StartDate AND @ToDate                        
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                ) AS A
        GROUP BY AccountNumber,
                AccountName,
				IsParent ,
				AccountCategoryKind         
        HAVING
				SUM(OpenningDebitAmount) <>0 OR
                SUM(OpenningCreditAmount)  <>0 OR
                SUM(ClosingDebitAmount)  <>0 OR
                SUM(ClosingCreditAmount) <>0 OR
                SUM(AccumDebitAmount) <>0 OR
                SUM(AccumCreditAmount) <>0 
        ORDER BY AccountNumber
        		
		SELECT  ROW_NUMBER() OVER (ORDER BY AccountNumber,DetailAccountNumber, OrderType, PostedDate, RefDate,OrderNumber, RefNo) AS RowNum,
				RefID,
				RefType,
				PostedDate,
				RefDate,
				RefNo,
				InvDate,
				InvNo,
				JournalMemo,
				AccountNumber,
				DetailAccountNumber,
				CorrespondingAccountNumber,
				DebitAmount,
				CreditAmount,
				IsBold,
				AccountCategoryKind
		FROM    #Result
			
              
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

ALTER PROCEDURE [dbo].[Proc_GL_GetBookDetailPaymentByAccountNumber]
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(MAX) ,
	/*@CurrencyId NVARCHAR(10) , comment by cuongpv*/
    @GroupTheSameItem BIT
AS
    BEGIN

		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1) PRIMARY KEY,
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              InvDate DATETIME ,
              InvNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255) ,
              AccountNumber NVARCHAR(25) ,
              CorrespondingAccountNumber NVARCHAR(25) ,
              DebitAmount DECIMAL(22,4) ,
              CreditAmount DECIMAL(22,4) ,
              ClosingDebitAmount DECIMAL(22,4) ,
              ClosingCreditAmount DECIMAL(22,4) ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              OrderType INT , 
              IsBold BIT ,
              BranchName NVARCHAR(128),
			  AccountGroupKind NVARCHAR(25) 
            )      
       
        
        DECLARE @tblAccountNumber TABLE
            (              
			  AccountNumber  NVARCHAR(25) ,  
              AccountNumberPercent NVARCHAR(25)   , 
			  AccountGroupKind NVARCHAR(25) 
            )
        INSERT  @tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountNumber + '%',
						A1.AccountGroupKind
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        IF @GroupTheSameItem = 1
            BEGIN
                INSERT  #Result					
				SELECT
					RefID ,
                    PostedDate ,
                    RefDate ,
                    RefNo ,
                    InvDate ,
                    InvNo ,
                    RefType ,
                    JournalMemo ,
                    AccountNumber,
                    CorrespondingAccountNumber ,
                    ISNULL(DebitAmount,0) ,
                    ISNULL(CreditAmount,0) ,
                    ISNULL(ClosingDebitAmount,0),
                    ISNULL(ClosingCreditAmount,0),
                    AccountingObjectCode,
                    AccountObjectName,
                    OrderType ,
                    IsBold ,
                    BranchName,
					AccountGroupKind
                 FROM
					(SELECT  
								NULL as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư đầu kỳ' as JournalMemo ,
                                A.AccountNumber,
                                NULL as CorrespondingAccountNumber ,
								$0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
									 THEN SUM(GL.DebitAmount - GL.CreditAmount)
									 ELSE 0
								END AS ClosingDebitAmount ,
								CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
									 THEN SUM(GL.CreditAmount - GL.DebitAmount)
									 ELSE 0
								END AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                0 AS OrderType ,
                                1 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
								,A.AccountGroupKind
                            FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
							INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
							WHERE   ( GL.PostedDate < @FromDate )  
							/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
							GROUP BY 
								A.AccountNumber,A.AccountGroupKind
							HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv Description-> Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                SUM(GL.DebitAmount) AS DebitAmount ,
                                null AS CreditAmount ,/*edit by cuongpv SUM(GL.CreditAmount) -> null*/
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 As isBold,
                                '' as BranchName
								,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
								,A.AccountGroupKind
                        FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )    
						/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
						and GL.AccountCorresponding <> ''      
                        GROUP BY GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv GL.Description -> GL.Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding,
								A.AccountGroupKind
                        HAVING  SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0
						/*add by cuongpv de sua cong gop but toan*/
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv Description-> Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                null AS DebitAmount ,
                                SUM(GL.CreditAmount) AS CreditAmount ,
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 As isBold,
                                '' as BranchName
								,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
								,A.AccountGroupKind
                        FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )    
						/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
						and GL.AccountCorresponding <> ''      
                        GROUP BY GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Reason,/*edit by cuongpv GL.Description -> GL.Reason*/
                                A.AccountNumber,
                                GL.AccountCorresponding,
								A.AccountGroupKind
                        HAVING  SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0 
						/*end add by cuongpv*/                       
                      )T
                      ORDER BY 
						AccountNumber,
                        OrderType ,
                        postedDate ,
                        RefDate ,
                        RefNo      
                        ,SortOrder,DetailPostOrder,EntryType            
            END
            
        ELSE
            BEGIN             
                
                INSERT  #Result					
				SELECT
					RefID ,
                    PostedDate ,
                    RefDate ,
                    RefNo ,
                    InvDate ,
                    InvNo ,
                    RefType ,
                    JournalMemo ,
                    AccountNumber,
                    CorrespondingAccountNumber ,
                    DebitAmount ,
                    CreditAmount ,
                    ClosingDebitAmount ,
                    ClosingCreditAmount ,
                    AccountingObjectCode,
                    AccountObjectName,
                    OrderType ,
                    IsBold ,
                    BranchName,
					AccountGroupKind
                 FROM
					(SELECT  
								null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư đầu kỳ' as JournalMemo ,
                                A.AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
									 THEN SUM(GL.DebitAmount - GL.CreditAmount)
									 ELSE 0
								END AS ClosingDebitAmount ,
								CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
									 THEN SUM(GL.CreditAmount - GL.DebitAmount)
									 ELSE 0
								END AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                0 AS OrderType ,
                                1 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType
								,A.AccountGroupKind
                            FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
							INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
							WHERE   ( GL.PostedDate < @FromDate )   
							/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
							GROUP BY 
								A.AccountNumber,A.AccountGroupKind
							HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                CASE WHEN GL.DESCRIPTION IS NULL
                                          OR LTRIM(GL.Description) = ''
                                     THEN GL.Reason
                                     ELSE GL.Description
                                END AS JournalMemo ,
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                GL.DebitAmount ,
                                GL.CreditAmount ,
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 ,
                                null BranchName
								,null
								,null
								,null
								,A.AccountGroupKind
                        FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
                        WHERE   (GL.PostedDate BETWEEN @FromDate AND @ToDate )                              
                                AND ( GL.DebitAmount <> 0
                                      OR GL.CreditAmount <> 0
                                    )   	
									/*and GL.CurrencyID = @CurrencyId comment by cuongpv*/
									and GL.AccountCorresponding <> ''                     
                      )T
                      ORDER BY
						AccountNumber,
                        OrderType ,
                        postedDate ,
                        RefDate ,
                        RefNo,SortOrder,DetailPostOrder,EntryType                                                          
               
            END         
       
			 

		DECLARE @AccountNumber1 NVARCHAR(25)

		DECLARE C1 CURSOR FOR  
		SELECT  F.Value
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
	    order by F.Value
		OPEN c1;  
		FETCH NEXT FROM c1 INTO @AccountNumber1  ;  
		WHILE @@FETCH_STATUS = 0  
		   BEGIN  
		      PRINT @AccountNumber1
			  DECLARE @AccountObjectCode NVARCHAR(25)
			 ,@ClosingDebitAmount DECIMAL(22,4)
			 
		      SELECT @ClosingDebitAmount = 0
			  UPDATE  #Result
			  SET     @ClosingDebitAmount = @ClosingDebitAmount + ISNULL(ClosingDebitAmount,0) - 
		                              ISNULL(ClosingCreditAmount,0) + ISNULL(DebitAmount,0) - ISNULL(CreditAmount,0),
		
                ClosingDebitAmount = CASE WHEN @ClosingDebitAmount > 0
                                          THEN @ClosingDebitAmount
                                          ELSE 0
                                     END ,
                ClosingCreditAmount = CASE WHEN @ClosingDebitAmount < 0
                                           THEN - @ClosingDebitAmount
                                           ELSE 0
                                      END 
               where AccountNumber = @AccountNumber1
			   FETCH NEXT FROM c1 INTO @AccountNumber1  ; 
			   
		   END;  
		CLOSE c1;  
		DEALLOCATE c1;  
		
        DECLARE @DebitAmount DECIMAL(22,4) 
		DECLARE @CreditAmount DECIMAL(22,4)  
		DECLARE @ClosingDeditAmount_SDDK DECIMAL(22,4)  
		DECLARE @ClosingCreditAmount_SDDK DECIMAL(22,4)  
	    select @DebitAmount = sum(DebitAmount),
		       @CreditAmount = sum(CreditAmount)
		from #Result Res
		INNER JOIN @tblAccountNumber A ON Res.AccountNumber LIKE A.AccountNumberPercent
		where OrderType = 1
		select @ClosingDeditAmount_SDDK = sum(ClosingDebitAmount),
		       @ClosingCreditAmount_SDDK = sum(ClosingCreditAmount)
		from #Result where OrderType = 0
        SELECT  RowNum ,
                RefID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountNumber,
                PostedDate ,
                RefDate ,
                RefNo ,
                InvDate ,
                InvNo ,
                RefType ,
                JournalMemo ,
                CorrespondingAccountNumber ,
                DebitAmount ,
                CreditAmount ,
                ClosingDebitAmount ,
                ClosingCreditAmount ,               
                OrderType AS InvoiceOrder ,
                IsBold ,
                BranchName,
				AccountGroupKind
        FROM    #Result
		WHERE (DebitAmount > 0 OR CreditAmount > 0 AND OrderType = 1) OR OrderType = 0 /*add by cuongpv*/
        ORDER BY RowNum
		
		
		select 
		null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư cuối kỳ' as JournalMemo ,
                                AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 DebitAmount ,
								$0 CreditAmount ,
								case 
								when A1.AccountGroupKind=0 or (A1.AccountGroupKind=2 and (A1.AccountNumber like '1%' or A1.AccountNumber like '2%' or A1.AccountNumber like '6%' 
								or A1.AccountNumber like '8%')) then
								sum(ClosingDebitAmount) + sum(sumDebitAmount) - sum(sumCreditAmount) else $0 
								end as ClosingDebitAmount,
								case 
								when A1.AccountGroupKind=1 or (A1.AccountGroupKind=2 and (A1.AccountNumber like '3%' or A1.AccountNumber like '4%' or A1.AccountNumber like '5%' 
								or A1.AccountNumber like '7%')) then
								sum(ClosingCreditAmount) + sum(sumCreditAmount) - sum(sumDebitAmount) else $0 
								end AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                2 AS OrderType ,
                                2 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType,
								A1.AccountGroupKind
        from (SELECT  
								null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư cuối kỳ' as JournalMemo ,
                                A.AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN GL.OrderType = 0 and gl.IsBold = 1 THEN sum(ClosingDebitAmount)
									 ELSE 0 
								END
								AS ClosingDebitAmount,
								sum(DebitAmount) sumDebitAmount,
								sum(CreditAmount) sumCreditAmount,
								0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                2 AS OrderType ,
                                2 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType,
								A.AccountGroupKind
                            FROM    #Result AS GL
							INNER JOIN @tblAccountNumber A ON GL.AccountNumber = A.AccountNumber
							GROUP BY OrderType,IsBold,
								A.AccountNumber,A.AccountGroupKind) as A1
        group by AccountNumber,AccountGroupKind
        DROP TABLE #Result 
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_FU_GetCashDetailBook]
    (
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @AccountNumber NVARCHAR(20) ,
      @BranchID UNIQUEIDENTIFIER ,
      @CurrencyID NVARCHAR(3)
    )
AS 
    BEGIN
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
			  AccountNumber NVARCHAR(15),
              PostedDate DATETIME ,
              RefDate DATETIME ,
              ReceiptRefNo NVARCHAR(25) ,
              PaymentRefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255) ,
              CorrespondingAccountNumber NVARCHAR(20) ,
              ExchangeRate Numeric ,
              DebitAmount Numeric ,
              DebitAmountOC Numeric ,
              CreditAmount Numeric ,
              CreditAmountOC Numeric ,
              ClosingAmount Numeric ,
              ClosingAmountOC Numeric ,
              ContactName NVARCHAR(255) ,
			  AccountObjectId NVARCHAR(255),
              AccountObjectCode NVARCHAR(255) ,
              AccountObjectName NVARCHAR(255) , 
              EmployeeCode NVARCHAR(255) ,
              EmployeeName NVARCHAR(255) , 
              InvoiceOrder INT , 
              BranchID UNIQUEIDENTIFIER ,
              BranchName NVARCHAR(255) ,
              IsBold BIT
            )
         DECLARE @tblAccountNumber TABLE
            (              
			  AccountNumber  NVARCHAR(25),  
              AccountNumberPercent NVARCHAR(25)              
            )
        INSERT  @tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountNumber + '%'
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        DECLARE @AccountNumberPercent NVARCHAR(25)
        SET @AccountNumberPercent = @AccountNumber + '%'
        
        DECLARE @DiscountNumber NVARCHAR(25)       
        SET @DiscountNumber = '5211%'
   
        DECLARE @ClosingAmountOC MONEY
        DECLARE @ClosingAmount MONEY
   
 INSERT  #Result
	          (RefID ,
			  AccountNumber,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  CorrespondingAccountNumber)
			  select ReferenceID ,
			  Account,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  AccountCorresponding from 
			            (SELECT null as ReferenceID,
						    Acc.AccountNumber as Account,
							null as PostedDate,
							null as RefDate,
							null as ReceiptRefNo,
							null as PaymentRefNo,
							null as RefType,
					        N'Số tồn đầu kỳ' AS JournalMemo ,
                            $0 AS ExchangeRate ,
                            $0 AS DebitAmount ,
                            $0 AS DebitAmountOC ,
                            $0 AS CreditAmount ,
                            $0 AS CreditAmountOC ,
							SUM(GL.DebitAmount - GL.CreditAmount) as ClosingAmount,
							SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) as ClosingAmountOC,
							null as ContactName,
							null as AccountObjectId,
                            0 AS InvoiceOrder ,
                            null BranchID ,
                            0 as BranchName,
                            null AccountCorresponding
							 from @tbDataGL gl /*edit by cuongpv GeneralLedger -> @tbDataGL*/
							 inner join @tblAccountNumber Acc on GL.Account = Acc.AccountNumber
							 where ( GL.PostedDate < @FromDate )
								AND ( @CurrencyID IS NULL
								OR GL.CurrencyID = @CurrencyID)
						    group by Acc.AccountNumber
				        union all
                        SELECT  GL.ReferenceID,
						        GL.Account,
                                GL.PostedDate ,
                                GL.RefDate ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS ReceiptRefNo ,
                                CASE WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS PaymentRefNo ,
                                GL.TypeID ,
								case when GL.Reason is null then
                                GL.Description 
								else GL.Reason 
								end as Description, 
                                GL.ExchangeRate ,
                                GL.DebitAmount ,
                                GL.DebitAmountOriginal ,
                                GL.CreditAmount ,
                                GL.CreditAmountOriginal ,
                                $0 AS ClosingAmount ,
                                $0 AS ClosingAmountOC ,
                                GL.ContactName ,
                                GL.AccountingObjectID ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0 THEN 1
                                     WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0 THEN 2
                                     ELSE 3
                                END AS InvoiceOrder ,
                                GL.BranchID ,
                                0,
                               GL.AccountCorresponding
                        FROM    @tbDataGL AS GL ,/*edit by cuongpv GeneralLedger -> @tbDataGL*/
						        @tblAccountNumber Acc
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )
                                AND GL.Account = Acc.AccountNumber
                                AND ( @CurrencyID IS NULL
                                      OR GL.CurrencyID = @CurrencyID
                                    )
                                AND ( GL.CreditAmount <> 0
                                      OR GL.DebitAmount <> 0
                                      OR GL.CreditAmountOriginal <> 0
                                      OR GL.DebitAmountOriginal <> 0
                                    )) T
                        ORDER BY Account,
						        PostedDate ,
                                RefDate ,
                                InvoiceOrder, 
								ReceiptRefNo,
								PaymentRefNo
		
        SET @ClosingAmount = 0
        SET @ClosingAmountOC = 0
        UPDATE  #Result
        SET     @ClosingAmount = @ClosingAmount  + ClosingAmount +ISNULL(DebitAmount, 0)
                - ISNULL(CreditAmount, 0) ,
                ClosingAmount = @ClosingAmount ,
                @ClosingAmountOC = @ClosingAmountOC + ClosingAmountOC + ISNULL(DebitAmountOC, 0)
                - ISNULL(CreditAmountOC, 0) ,
                ClosingAmountOC = @ClosingAmountOC 
        SELECT  *
        FROM    #Result R   
		order by rownum
        DROP TABLE #Result 
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoTheoDoiLaiLoTheoMatHang]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @MaterialGoodsID AS NVARCHAR(MAX)
AS
BEGIN          
    
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
		/*end add by cuongpv*/
	
	DECLARE @tbluutru TABLE (
		MaterialGoodsID UNIQUEIDENTIFIER,
		MaterialGoodsCode NVARCHAR(25),
		MaterialGoodsName NVARCHAR(512),
		Unit NVARCHAR(25),
		Quantity decimal(25,10),
		DoanhSoBan money,
		ChietKhau money,
		GiamGia money,
		TraLai money,
		GiaVon money,
		LaiLo money
    )
    INSERT INTO @tbluutru
		SELECT M.ID as MaterialGoodsID, M.MaterialGoodsCode, null MaterialGoodsName, null Unit, null Quantity, null DoanhSoBan, null ChietKhau,
				null GiamGia, null TraLai, null GiaVon, null LaiLo
		FROM dbo.Func_ConvertStringIntoTable(@MaterialGoodsID,',') tblMatHangSelect LEFT JOIN dbo.MaterialGoods M ON M.ID=tblMatHangSelect.Value
		WHERE tblMatHangSelect.Value in (select SA.MaterialGoodsID from SAInvoiceDetail SA LEFT JOIN @tbDataGL GL on GL.DetailID=SA.ID 
										 where GL.PostedDate between @FromDate and @ToDate)
		
	UPDATE @tbluutru SET MaterialGoodsName = a.MaterialGoodsName, Unit = a.Unit, Quantity=a.Quantity
	FROM ( select ID,MaterialGoodsID as MId, Description as MaterialGoodsName, Unit, Quantity from SAInvoiceDetail) a LEFT JOIN @tbDataGL GL on GL.DetailID=a.ID
	where MaterialGoodsID = a.MId AND (GL.PostedDate between @FromDate and @ToDate)
	

	DECLARE @tbDataDoanhSoBan TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		DoanhSoBan money
	)
	
	INSERT INTO @tbDataDoanhSoBan 
		SELECT a.MaterialGoodsID,SUM(c.CreditAmount) as DoanhSoBan 
		FROM SAInvoiceDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '511%' AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET DoanhSoBan = k.DoanhSoBan
	FROM (select MaterialGoodsID as Mid, DoanhSoBan from @tbDataDoanhSoBan) k
	WHERE MaterialGoodsID = k.Mid
	
	DECLARE @tbDataChietKhau TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		ChietKhauBan money,
		ChietKhau_TLGG money,
		ChietKhau money
	)
	
	INSERT INTO @tbDataChietKhau 
		SELECT a.MaterialGoodsID, SUM(c.DebitAmount) as ChietKhauBan, null ChietKhau_TLGG, null ChietKhau
		FROM SAInvoiceDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '511%' AND c.TypeID not in (330,340) 
		AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
		
	UPDATE @tbDataChietKhau SET ChietKhau_TLGG = g.ChietKhau_TLGG
	FROM (
		SELECT a.MaterialGoodsID as Mid, SUM(c.CreditAmount) as ChietKhau_TLGG
		FROM SAInvoiceDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '511%' AND c.TypeID in (330,340) 
		AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	) g
	WHERE MaterialGoodsID=g.Mid
	
	UPDATE @tbDataChietKhau SET ChietKhau = (ISNULL(ChietKhauBan,0) - ISNULL(ChietKhau_TLGG,0))
	
	UPDATE @tbluutru SET ChietKhau = e.ChietKhau
	FROM (select MaterialGoodsID as Mid, ChietKhau from @tbDataChietKhau) e
	WHERE MaterialGoodsID = e.Mid
	
	DECLARE @tbDataGiamGia TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		GiamGia money
	)

	INSERT INTO @tbDataGiamGia 
		SELECT a.MaterialGoodsID,SUM(c.DebitAmount - c.CreditAmount) as GiamGia 
		FROM SAReturnDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '511%' AND c.TypeID=340
		 AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET GiamGia = f.GiamGia
	FROM (select MaterialGoodsID as Mid, GiamGia from @tbDataGiamGia) f
	WHERE MaterialGoodsID = f.Mid
	
	DECLARE @tbDataTraLai TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		TraLai money
	)

	INSERT INTO @tbDataTraLai 
		SELECT a.MaterialGoodsID,SUM(c.DebitAmount - c.CreditAmount) as TraLai 
		FROM SAReturnDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '511%' AND c.TypeID=330
		 AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET TraLai = h.TraLai
	FROM (select MaterialGoodsID as Mid, TraLai from @tbDataTraLai) h
	WHERE MaterialGoodsID = h.Mid
	
	DECLARE @tbDataGiaVon TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		GiaVon money
	)
	
	INSERT INTO @tbDataGiaVon 
		SELECT a.MaterialGoodsID,SUM(c.DebitAmount - c.CreditAmount) as GiaVon 
		FROM SAInvoiceDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '632%'
		 AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
		UNION ALL
		SELECT a.MaterialGoodsID,SUM(c.DebitAmount - c.CreditAmount) as GiaVon 
		FROM SAReturnDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) AND c.Account like '632%'
		 AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
		
		
	UPDATE @tbluutru SET GiaVon = j.GiaVon
	FROM (select MaterialGoodsID as Mid, SUM(GiaVon) as GiaVon from @tbDataGiaVon group by MaterialGoodsID) j
	WHERE MaterialGoodsID = j.Mid
	
	UPDATE @tbluutru SET LaiLo = ISNULL(DoanhSoBan,0) - (ISNULL(ChietKhau,0)+ISNULL(GiamGia,0)+ISNULL(TraLai,0)+ISNULL(GiaVon,0))
	
	select MaterialGoodsID,	MaterialGoodsCode, MaterialGoodsName, Unit, Quantity, DoanhSoBan, ChietKhau, GiamGia, TraLai, GiaVon, LaiLo
	from @tbluutru WHERE MaterialGoodsID is not null AND ((ISNULL(Quantity,0)>0) OR (ISNULL(DoanhSoBan,0)>0) OR (ISNULL(ChietKhau,0)>0)
	OR (ISNULL(GiamGia,0)>0) OR (ISNULL(TraLai,0)>0) OR (ISNULL(GiaVon,0)>0) OR (ISNULL(LaiLo,0)>0))
	order by MaterialGoodsName
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoTheoDoiLaiLoTheoHopDongBan]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountObjectID AS NVARCHAR(MAX)
AS
BEGIN          
	
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
		/*end add by cuongpv*/

	DECLARE @tbAccountObjectID TABLE(
		AccountingObjectID UNIQUEIDENTIFIER
	)
	
	INSERT INTO @tbAccountObjectID
	SELECT tblAccOjectSelect.Value as AccountingObjectID
		FROM dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') tblAccOjectSelect
		WHERE tblAccOjectSelect.Value in (SELECT ID FROM AccountingObject)
		
	
    DECLARE @tbluutru TABLE (
		ContractID UNIQUEIDENTIFIER,
		NgayKy Date,
		SoHopDong NVARCHAR(50),
		AccountingObjectID UNIQUEIDENTIFIER,
		DoiTuong NVARCHAR(512),
		TrichYeu NVARCHAR(512),
		GiaTriHopDong decimal(25,0),
		DoanhThuThucTe money,
		GiamTruDoanhThu money,
		GiaVon money,
		LaiLo money
    )
    
    INSERT INTO @tbluutru(ContractID,NgayKy,SoHopDong,AccountingObjectID,DoiTuong,TrichYeu,GiaTriHopDong)
		SELECT EMC.ID as ContractID, EMC.SignedDate as NgayKy, EMC.Code as SoHopDong, EMC.AccountingObjectID, EMC.AccountingObjectName as DoiTuong,
		EMC.Name as TrichYeu, EMC.Amount as GiaTriHopDong
		FROM EMContract EMC
		WHERE EMC.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
	
	DECLARE @tbDataDoanhThuThucTe TABLE(
		ContractID UNIQUEIDENTIFIER,
		DoanhThuThucTe money
	)
	
	INSERT INTO @tbDataDoanhThuThucTe(ContractID,DoanhThuThucTe)
		SELECT GL.ContractID, SUM(GL.CreditAmount) as DoanhThuThucTe
		FROM @tbDataGL GL 
		WHERE (GL.Account like '511%') AND (GL.PostedDate between @FromDate and @ToDate) 
		AND GL.ContractID in (select ContractID from @tbluutru)
		GROUP BY GL.ContractID
		
	UPDATE @tbluutru SET DoanhThuThucTe = a.DoanhThuThucTe
	FROM (select ContractID as IDC, DoanhThuThucTe from @tbDataDoanhThuThucTe) a
	WHERE ContractID = a.IDC
	
	DECLARE @tbDataGiamTruDoanhThu TABLE(
		ContractID UNIQUEIDENTIFIER,
		GiamTruDoanhThu money
	)
	
	INSERT INTO @tbDataGiamTruDoanhThu(ContractID,GiamTruDoanhThu)
		SELECT GL.ContractID, SUM(GL.DebitAmount) as GiamTruDoanhThu
		FROM @tbDataGL GL 
		WHERE (GL.Account like '511%') AND (GL.PostedDate between @FromDate and @ToDate) 
		AND GL.ContractID in (select ContractID from @tbluutru)
		GROUP BY GL.ContractID
		
	UPDATE @tbluutru SET GiamTruDoanhThu = b.GiamTruDoanhThu
	FROM (select ContractID as IDC, GiamTruDoanhThu from @tbDataGiamTruDoanhThu) b
	WHERE ContractID = b.IDC
	
	DECLARE @tbDataGiaVon TABLE(
		ContractID UNIQUEIDENTIFIER,
		GiaVon money
	)
	
	INSERT INTO @tbDataGiaVon(ContractID,GiaVon)
		SELECT GL.ContractID, SUM(GL.DebitAmount - GL.CreditAmount) as GiaVon
		FROM @tbDataGL GL 
		WHERE (GL.Account like '632%') AND (GL.PostedDate between @FromDate and @ToDate) 
		AND GL.ContractID in (select ContractID from @tbluutru)
		GROUP BY GL.ContractID
		
	UPDATE @tbluutru SET GiaVon = c.GiaVon
	FROM (select ContractID as IDC, GiaVon from @tbDataGiaVon) c
	WHERE ContractID = c.IDC
	
	UPDATE @tbluutru SET LaiLo = ISNULL(DoanhThuThucTe,0) - (ISNULL(GiamTruDoanhThu,0) + ISNULL(GiaVon,0))
	
    SELECT NgayKy, SoHopDong, DoiTuong, TrichYeu, GiaTriHopDong, DoanhThuThucTe, GiamTruDoanhThu, GiaVon, LaiLo FROM @tbluutru
    WHERE ((ISNULL(DoanhThuThucTe,0)>0) OR (ISNULL(GiamTruDoanhThu,0)>0)
			OR (ISNULL(GiaVon,0)>0) OR (ISNULL(LaiLo,0)>0))
	ORDER BY NgayKy, SoHopDong
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoTheoDoiCongNoPhaiThuTheoMatHang]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @MaterialGoodsID AS NVARCHAR(MAX)
AS
BEGIN          
	
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
		/*end add by cuongpv*/

	/*lay cac ma hang co trong bang SAInvoiceDetail va bang SAReturnDetail trong khoang @FromDate den @ToDate */
	DECLARE @tbDataMaterialGoodsID TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER
	)
	INSERT INTO @tbDataMaterialGoodsID
		SELECT SA.MaterialGoodsID from SAInvoiceDetail SA LEFT JOIN @tbDataGL GL on GL.DetailID=SA.ID 
		where GL.PostedDate between @FromDate and @ToDate
	
	INSERT INTO @tbDataMaterialGoodsID
		SELECT SA.MaterialGoodsID from SAReturnDetail SA LEFT JOIN @tbDataGL GL on GL.DetailID=SA.ID 
		where GL.PostedDate between @FromDate and @ToDate
	
    DECLARE @tbluutru TABLE (
		MaterialGoodsID UNIQUEIDENTIFIER,
		MaterialGoodsCode NVARCHAR(25),
		MaterialGoodsName NVARCHAR(512),
		GiaTriHang money,
		Thue money,
		ChietKhau money,
		GiamGia money,
		TraLai money,
		DaThanhToan money,
		ConLai money
    )
    INSERT INTO @tbluutru
		SELECT M.ID as MaterialGoodsID, M.MaterialGoodsCode, M.MaterialGoodsName, null GiaTriHang, null Thue, null ChietKhau, null GiamGia,
				null TraLai, null DaThanhToan, null ConLai
		FROM dbo.Func_ConvertStringIntoTable(@MaterialGoodsID,',') tblMatHangSelect LEFT JOIN dbo.MaterialGoods M ON M.ID=tblMatHangSelect.Value
		WHERE tblMatHangSelect.Value in (select distinct MaterialGoodsID from @tbDataMaterialGoodsID)
		
	DECLARE @tbDataGiaTriHang TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		GiaTriHang money
	)
	
	INSERT INTO @tbDataGiaTriHang 
		SELECT a.MaterialGoodsID,SUM(c.DebitAmount) as GiaTriHang 
		FROM SAInvoiceDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) 
		AND c.Account like '131%' AND c.AccountCorresponding like '511%' AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET GiaTriHang = k.GiaTriHang
	FROM (select MaterialGoodsID as Mid, GiaTriHang from @tbDataGiaTriHang) k
	WHERE MaterialGoodsID = k.Mid
	
	DECLARE @tbDataThue TABLE (
		MaterialGoodsID UNIQUEIDENTIFIER,
		Thue money
	)
	
	INSERT INTO @tbDataThue
		SELECT a.MaterialGoodsID,SUM(c.DebitAmount) as Thue 
		FROM SAInvoiceDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) 
		AND c.Account like '131%' AND c.AccountCorresponding like '3331%' AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET Thue = h.Thue
	FROM (select MaterialGoodsID as Mid, Thue from @tbDataThue) h
	WHERE MaterialGoodsID = h.Mid
	
	DECLARE @tbDataChietKhau TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		ChietKhau money
	)
	
	INSERT INTO @tbDataChietKhau 
		SELECT a.MaterialGoodsID, SUM(c.CreditAmount) as ChietKhau
		FROM SAInvoiceDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) 
		AND c.Account like '131%' AND c.AccountCorresponding like '511%' AND c.TypeID not in (330,340) 
		AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET ChietKhau = i.ChietKhau
	FROM (select MaterialGoodsID as Mid, ChietKhau from @tbDataChietKhau) i
	WHERE MaterialGoodsID = i.Mid
	
	DECLARE @tbDataGiamGia TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		GiamGia money
	)

	INSERT INTO @tbDataGiamGia 
		SELECT a.MaterialGoodsID,SUM(c.CreditAmount - c.DebitAmount) as GiamGia 
		FROM SAReturnDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) 
		AND c.Account like '131%' AND c.TypeID=340
		AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET GiamGia = j.GiamGia
	FROM (select MaterialGoodsID as Mid, GiamGia from @tbDataGiamGia) j
	WHERE MaterialGoodsID = j.Mid
	
	DECLARE @tbDataTraLai TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		TraLai money
	)

	INSERT INTO @tbDataTraLai 
		SELECT a.MaterialGoodsID,SUM(c.CreditAmount - c.DebitAmount) as TraLai 
		FROM SAReturnDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) 
		AND c.Account like '131%' AND c.TypeID=330
		AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
	
	UPDATE @tbluutru SET TraLai = n.TraLai
	FROM (select MaterialGoodsID as Mid, TraLai from @tbDataTraLai) n
	WHERE MaterialGoodsID = n.Mid
	
	DECLARE @tbDataDaThanhToan TABLE(
		MaterialGoodsID UNIQUEIDENTIFIER,
		DaThanhToan money
	)
	
	INSERT INTO @tbDataDaThanhToan 
		SELECT a.MaterialGoodsID,SUM(c.CreditAmount) as DaThanhToan 
		FROM SAInvoiceDetail a LEFT JOIN @tbDataGL c on c.DetailID = a.ID
		WHERE a.MaterialGoodsID in (select MaterialGoodsID from @tbluutru) 
		AND c.Account like '131%' AND ((AccountCorresponding like '111%') OR (AccountCorresponding like '112%'))
		AND (PostedDate between @FromDate and @ToDate)
		GROUP BY a.MaterialGoodsID
		
	UPDATE @tbluutru SET DaThanhToan = m.DaThanhToan
	FROM (select MaterialGoodsID as Mid, DaThanhToan from @tbDataDaThanhToan) m
	WHERE MaterialGoodsID = m.Mid
	
	UPDATE @tbluutru SET ConLai = ((ISNULL(GiaTriHang,0) + ISNULL(Thue,0)) - (ISNULL(ChietKhau,0)+ISNULL(GiamGia,0)+ISNULL(TraLai,0)+ISNULL(DaThanhToan,0)))
	
	select MaterialGoodsID,	MaterialGoodsCode, MaterialGoodsName, GiaTriHang, Thue, ChietKhau, GiamGia, TraLai, DaThanhToan, ConLai
	from @tbluutru WHERE MaterialGoodsID is not null AND ((ISNULL(GiaTriHang,0)>0) OR (ISNULL(Thue,0)>0) OR (ISNULL(ChietKhau,0)>0)
	OR (ISNULL(GiamGia,0)>0) OR (ISNULL(TraLai,0)>0) OR (ISNULL(DaThanhToan,0)>0) OR (ISNULL(ConLai,0)>0))
	order by MaterialGoodsName
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoTheoDoiCongNoPhaiThuTheoHopDongBan]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountObjectID AS NVARCHAR(MAX)
AS
BEGIN          
	
	/*Add by cuongpv de sua cach lam tron*/
	DECLARE @tbDataGL TABLE(
		ID uniqueidentifier,
		BranchID uniqueidentifier,
		ReferenceID uniqueidentifier,
		TypeID int,
		Date datetime,
		PostedDate datetime,
		No nvarchar(25),
		InvoiceDate datetime,
		InvoiceNo nvarchar(25),
		Account nvarchar(25),
		AccountCorresponding nvarchar(25),
		BankAccountDetailID uniqueidentifier,
		CurrencyID nvarchar(3),
		ExchangeRate decimal(25, 10),
		DebitAmount decimal(25,0),
		DebitAmountOriginal decimal(25,2),
		CreditAmount decimal(25,0),
		CreditAmountOriginal decimal(25,2),
		Reason nvarchar(512),
		Description nvarchar(512),
		VATDescription nvarchar(512),
		AccountingObjectID uniqueidentifier,
		EmployeeID uniqueidentifier,
		BudgetItemID uniqueidentifier,
		CostSetID uniqueidentifier,
		ContractID uniqueidentifier,
		StatisticsCodeID uniqueidentifier,
		InvoiceSeries nvarchar(25),
		ContactName nvarchar(512),
		DetailID uniqueidentifier,
		RefNo nvarchar(25),
		RefDate datetime,
		DepartmentID uniqueidentifier,
		ExpenseItemID uniqueidentifier,
		OrderPriority int,
		IsIrrationalCost bit
	)

	INSERT INTO @tbDataGL
	SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
	/*end add by cuongpv*/

	DECLARE @tbAccountObjectID TABLE(
		AccountingObjectID UNIQUEIDENTIFIER
	)
	
	INSERT INTO @tbAccountObjectID
	SELECT tblAccOjectSelect.Value as AccountingObjectID
		FROM dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') tblAccOjectSelect
		WHERE tblAccOjectSelect.Value in (SELECT ID FROM AccountingObject)
		
	
    DECLARE @tbluutru TABLE (
		ContractID UNIQUEIDENTIFIER,
		NgayKy Date,
		SoHopDong NVARCHAR(50),
		AccountingObjectID UNIQUEIDENTIFIER,
		DoiTuong NVARCHAR(512),
		TrichYeu NVARCHAR(512),
		GiaTriHopDong decimal(25,0),
		GiaTriThucTe money,
		CacKhoanGiamTru money,
		DaThanhToan money,
		ConLai money
    )
    
    INSERT INTO @tbluutru(ContractID,NgayKy,SoHopDong,AccountingObjectID,DoiTuong,TrichYeu,GiaTriHopDong)
		SELECT EMC.ID as ContractID, EMC.SignedDate as NgayKy, EMC.Code as SoHopDong, EMC.AccountingObjectID, EMC.AccountingObjectName as DoiTuong,
		EMC.Name as TrichYeu, EMC.Amount as GiaTriHopDong
		FROM EMContract EMC
		WHERE EMC.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
	
	DECLARE @tbDataGiaTriThucTe TABLE(
		ContractID UNIQUEIDENTIFIER,
		GiaTriThucTe money
	)
	
	INSERT INTO @tbDataGiaTriThucTe(ContractID,GiaTriThucTe)
		SELECT GL.ContractID, SUM(GL.DebitAmount) as GiaTriThucTe
		FROM @tbDataGL GL 
		WHERE (GL.Account like '131%') AND ((AccountCorresponding like '511%') OR (AccountCorresponding like '3331%'))
		AND (GL.PostedDate between @FromDate and @ToDate) 
		AND GL.ContractID in (select ContractID from @tbluutru)
		GROUP BY GL.ContractID
		
	UPDATE @tbluutru SET GiaTriThucTe = a.GiaTriThucTe
	FROM (select ContractID as IDC, GiaTriThucTe from @tbDataGiaTriThucTe) a
	WHERE ContractID = a.IDC
	
	DECLARE @tbDataCacKhoanGiamTru TABLE(
		ContractID UNIQUEIDENTIFIER,
		CacKhoanGiamTru money
	)
	
	INSERT INTO @tbDataCacKhoanGiamTru(ContractID,CacKhoanGiamTru)
		SELECT GL.ContractID, SUM(GL.CreditAmount) as CacKhoanGiamTru
		FROM @tbDataGL GL 
		WHERE (GL.Account like '131%') AND (AccountCorresponding like '635%')
		AND (GL.PostedDate between @FromDate and @ToDate) 
		AND GL.ContractID in (select ContractID from @tbluutru)
		GROUP BY GL.ContractID
		
	UPDATE @tbluutru SET CacKhoanGiamTru = b.CacKhoanGiamTru
	FROM (select ContractID as IDC, CacKhoanGiamTru from @tbDataCacKhoanGiamTru) b
	WHERE ContractID = b.IDC
	
	DECLARE @tbDataDaThanhToan TABLE(
		ContractID UNIQUEIDENTIFIER,
		DaThanhToan money
	)
	
	INSERT INTO @tbDataDaThanhToan(ContractID,DaThanhToan)
		SELECT GL.ContractID, SUM(GL.CreditAmount) as DaThanhToan
		FROM @tbDataGL GL 
		WHERE (GL.Account like '131%') AND ((AccountCorresponding like '111%') OR (AccountCorresponding like '112%'))
		AND (GL.PostedDate between @FromDate and @ToDate) 
		AND GL.ContractID in (select ContractID from @tbluutru)
		GROUP BY GL.ContractID
		
	UPDATE @tbluutru SET DaThanhToan = c.DaThanhToan
	FROM (select ContractID as IDC, DaThanhToan from @tbDataDaThanhToan) c
	WHERE ContractID = c.IDC
	
	UPDATE @tbluutru SET ConLai = ISNULL(GiaTriThucTe,0) - (ISNULL(CacKhoanGiamTru,0) + ISNULL(DaThanhToan,0))
	
    SELECT NgayKy, SoHopDong, DoiTuong, TrichYeu, GiaTriHopDong, GiaTriThucTe, CacKhoanGiamTru, DaThanhToan, ConLai FROM @tbluutru
    WHERE ((ISNULL(GiaTriThucTe,0)>0) OR (ISNULL(CacKhoanGiamTru,0)>0)
			OR (ISNULL(DaThanhToan,0)>0) OR (ISNULL(ConLai,0)>0))
	ORDER BY NgayKy, SoHopDong
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoTheoDoiCongNoPhaiThuTheoHoaDon]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountObjectID AS NVARCHAR(MAX)
AS
BEGIN          
	
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between @FromDate and @ToDate
		/*end add by cuongpv*/

	DECLARE @tbAccountObjectID TABLE(
		AccountingObjectID UNIQUEIDENTIFIER
	)
	
	INSERT INTO @tbAccountObjectID
	SELECT tblAccOjectSelect.Value as AccountingObjectID
		FROM dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') tblAccOjectSelect
		WHERE tblAccOjectSelect.Value in (SELECT ID FROM AccountingObject)
		
	
    DECLARE @tbluutru TABLE (
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		TienHangVaCK money,
		TienThue money,
		TraLai money,
		ChietKhauTT_GiamTruKhac money
    )
    
    INSERT INTO @tbluutru(DetailID,InvoiceDate,InvoiceNo,AccountingObjectID)
		SELECT DISTINCT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID
		FROM @tbDataGL GL
		WHERE (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
			AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID))
			AND (GL.DetailID in (select ID from SAInvoiceDetail))
    
    DECLARE @tbTienHangVaCK TABLE(
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		DebitAmount money,
		CreditAmount money
		)
    
    INSERT INTO @tbTienHangVaCK
    SELECT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.DebitAmount, GL.CreditAmount
    FROM @tbDataGL GL
    WHERE (GL.Account like '131%') AND (AccountCorresponding like '511%') AND (GL.InvoiceDate is not null) 
		AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '') AND (GL.DetailID in (select ID from SAInvoiceDetail))
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID))
    
    UPDATE @tbluutru SET TienHangVaCK = a.TienHangVaCK
    FROM (select DetailID as IvID, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, SUM(DebitAmount - CreditAmount) as TienHangVaCK
		from @tbTienHangVaCK
		group by DetailID, InvoiceDate, InvoiceNo, AccountingObjectID) a
	WHERE DetailID = a.IvID AND InvoiceDate = a.IvDate AND InvoiceNo = a.IvNo AND AccountingObjectID = a.AOID
	
	DECLARE @tbTienThue TABLE (
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		DebitAmount money
		)
	
	INSERT INTO @tbTienThue
	SELECT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.DebitAmount
	FROM @tbDataGL GL
	WHERE (GL.Account like '131%') AND (AccountCorresponding like '3331%') AND (GL.InvoiceDate is not null) 
		  AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '') AND (GL.DetailID in (select ID from SAInvoiceDetail))
		  AND (GL.PostedDate between @FromDate and @ToDate) AND GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
	
	UPDATE @tbluutru SET TienThue = b.TienThue
	FROM (select DetailID as IvID, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, SUM(DebitAmount) as TienThue
		  from @tbTienThue
		  group by DetailID, InvoiceDate, InvoiceNo, AccountingObjectID) b
	WHERE DetailID = b.IvID AND InvoiceDate = b.IvDate AND InvoiceNo = b.IvNo AND AccountingObjectID = b.AOID
	
	DECLARE @tbDataTraLai TABLE(
		SAInvoiceDetailID UNIQUEIDENTIFIER,
		TraLai money
	)
	INSERT INTO @tbDataTraLai(SAInvoiceDetailID,TraLai)
		SELECT SAR.SAInvoiceDetailID, SUM(GL.CreditAmount - GL.DebitAmount) as TraLai
		FROM @tbDataGL GL LEFT JOIN SAReturnDetail SAR ON GL.DetailID = SAR.ID
		WHERE (GL.Account like '131%') AND (GL.TypeID=330) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate)
		AND GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
		GROUP BY SAR.SAInvoiceDetailID
	
	UPDATE @tbluutru SET TraLai = TL.TraLai
	FROM (select SAInvoiceDetailID, TraLai from @tbDataTraLai) TL
	WHERE DetailID = TL.SAInvoiceDetailID
	
	DECLARE @tbChietKhauTT_GiamTruKhac TABLE(
		DetailID UNIQUEIDENTIFIER,
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		CreditAmount money
	)
	
	INSERT INTO @tbChietKhauTT_GiamTruKhac
	SELECT GL.DetailID, GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, GL.CreditAmount
	FROM @tbDataGL GL
	WHERE (GL.Account like '131%') AND (GL.AccountCorresponding like '635%')
		  AND (GL.TypeID not in (330,340)) AND (GL.DetailID in (select ID from SAInvoiceDetail))
		  AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '') 
		  AND (GL.PostedDate between @FromDate and @ToDate) AND GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
	
	UPDATE @tbluutru SET ChietKhauTT_GiamTruKhac = c.ChietKhauTT_GiamTruKhac
	FROM (select DetailID as IvID, InvoiceDate as IvDate, InvoiceNo as IvNo, AccountingObjectID as AOID, SUM(CreditAmount) as ChietKhauTT_GiamTruKhac
		  from @tbChietKhauTT_GiamTruKhac 
		  group by DetailID, InvoiceDate, InvoiceNo, AccountingObjectID) c
	WHERE DetailID = c.IvID AND InvoiceDate = c.IvDate AND InvoiceNo = c.IvNo AND AccountingObjectID = c.AOID
	
	
	DECLARE @tbDataReturn TABLE (
		InvoiceDate Date,
		InvoiceNo NVARCHAR(25),
		AccountingObjectID UNIQUEIDENTIFIER,
		AccountingObjectName NVARCHAR(512),
		TienHangVaCK money,
		TienThue money,
		GiaTriHoaDon money,
		TraLai money,
		GiamGia money,
		ChietKhauTT_GiamTruKhac money,
		SoDaThu money,
		SoConPhaiThu money
    )
    
    INSERT INTO @tbDataReturn(InvoiceDate,InvoiceNo,AccountingObjectID,TienHangVaCK,TienThue,TraLai,ChietKhauTT_GiamTruKhac)
    SELECT InvoiceDate,InvoiceNo,AccountingObjectID,SUM(TienHangVaCK) as TienHangVaCK, SUM(TienThue) as TienThue,
    SUM(TraLai) as TraLai, SUM(ChietKhauTT_GiamTruKhac) as ChietKhauTT_GiamTruKhac
    FROM @tbluutru
    GROUP BY InvoiceDate,InvoiceNo,AccountingObjectID
    
	INSERT INTO @tbDataReturn(InvoiceDate,InvoiceNo,AccountingObjectID,GiamGia)
		SELECT GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID, SUM(GL.CreditAmount - GL.DebitAmount) as GiamGia
		FROM @tbDataGL GL 
		WHERE (GL.Account like '131%') AND (GL.TypeID=340) AND (GL.InvoiceDate is not null) AND (GL.InvoiceNo is not null) AND (GL.InvoiceNo <> '')
		AND (GL.PostedDate between @FromDate and @ToDate) AND (GL.DetailID in (select ID from SAReturnDetail)) 
		AND GL.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
		GROUP BY GL.InvoiceDate, GL.InvoiceNo, GL.AccountingObjectID
		
	UPDATE @tbDataReturn SET SoDaThu = d.SoDaThu
	FROM (select SA.InvoiceDate as IvDate, SA.InvoiceNo as IvNo, SA.AccountingObjectID as AOID, SUM(GL.CreditAmount) as SoDaThu
		  from @tbDataGL GL LEFT JOIN MCReceipt MC ON GL.ReferenceID = MC.ID
			LEFT JOIN MCReceiptDetailCustomer MCC ON MCC.MCReceiptID = MC.ID
			LEFT JOIN SAInvoice SA ON SA.ID = MCC.SaleInvoiceID
		  where (GL.Account like '131%') AND ((GL.AccountCorresponding like '111%') OR (GL.AccountCorresponding like '112%')) 
		  AND (SA.InvoiceDate is not null) AND (SA.InvoiceNo is not null) AND (SA.InvoiceNo <> '') 
		  AND (GL.PostedDate between @FromDate and @ToDate) AND SA.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
		  group by SA.InvoiceDate, SA.InvoiceNo, SA.AccountingObjectID
		  
		  UNION ALL
		  
		  select SA.InvoiceDate as IvDate, SA.InvoiceNo as IvNo, SA.AccountingObjectID as AOID, SUM(GL.CreditAmount) as SoDaThu
		  from @tbDataGL GL LEFT JOIN MBDeposit MB ON GL.ReferenceID = MB.ID
			LEFT JOIN MBDepositDetailCustomer MBC ON MBC.MBDepositID = MB.ID
			LEFT JOIN SAInvoice SA ON SA.ID = MBC.SaleInvoiceID
		  where (GL.Account like '131%') AND ((GL.AccountCorresponding like '111%') OR (GL.AccountCorresponding like '112%')) 
		  AND (SA.InvoiceDate is not null) AND (SA.InvoiceNo is not null) AND (SA.InvoiceNo <> '') 
		  AND (GL.PostedDate between @FromDate and @ToDate) AND SA.AccountingObjectID in (select AccountingObjectID from @tbAccountObjectID)
		  group by SA.InvoiceDate, SA.InvoiceNo, SA.AccountingObjectID
		  ) d
	WHERE InvoiceDate = d.IvDate AND InvoiceNo = d.IvNo AND AccountingObjectID = d.AOID
    
    UPDATE @tbDataReturn SET AccountingObjectName = AJ.AccountingObjectName
	FROM ( select ID,AccountingObjectName from AccountingObject) AJ
	where AccountingObjectID = AJ.ID
    
    UPDATE @tbDataReturn SET GiaTriHoaDon = (ISNULL(TienHangVaCK,0) + ISNULL(TienThue,0))
    
    UPDATE @tbDataReturn SET SoConPhaiThu = (ISNULL(GiaTriHoaDon,0) - (ISNULL(TraLai,0)+ISNULL(GiamGia,0)+ISNULL(ChietKhauTT_GiamTruKhac,0)+ISNULL(SoDaThu,0)))
    
    SELECT InvoiceDate,InvoiceNo,AccountingObjectName,GiaTriHoaDon,TraLai,GiamGia,ChietKhauTT_GiamTruKhac,SoDaThu,SoConPhaiThu FROM @tbDataReturn
    WHERE ((ISNULL(GiaTriHoaDon,0)>0) OR (ISNULL(TraLai,0)>0) OR (ISNULL(GiamGia,0)>0) OR (ISNULL(ChietKhauTT_GiamTruKhac,0)>0)
			OR (ISNULL(SoDaThu,0)>0) OR (ISNULL(SoConPhaiThu,0)>0))
	ORDER BY InvoiceDate,InvoiceNo
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoCongCuDungCu]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @ToolsIDs AS NVARCHAR(MAX)
AS
BEGIN          
    DECLARE @tbluutru TABLE (
		ToolsID UNIQUEIDENTIFIER,
		ToolsCode NVARCHAR(25),
		ToolsName NVARCHAR(255),
		IncrementDate Date,
		PostedDate Date,
		Amount decimal(19,4),
		AllocationTimes int,
		AllocatedAmount decimal(19,4),
		DecrementAllocationTime int,
		RemainingAllocationTime int,
		DecrementAmount decimal(19,4),
		RemainingAmount decimal(19,4)
    )
    INSERT INTO @tbluutru(ToolsID, ToolsCode, ToolsName, Amount, AllocationTimes, AllocatedAmount)
		SELECT T.ID as ToolsID, T.ToolsCode, T.ToolsName, T.Amount, T.AllocationTimes, T.AllocatedAmount
		FROM dbo.Func_ConvertStringIntoTable(@ToolsIDs,',') tblCodeSelect LEFT JOIN dbo.TIInit T ON T.ID = tblCodeSelect.Value
		WHERE tblCodeSelect.Value is not null AND T.ID in (select DISTINCT ToolsID from ToolLedger where PostedDate <= @ToDate)
	
	UPDATE @tbluutru SET IncrementDate = a.IncrementDate
	FROM (Select DISTINCT ToolsID as Tid, IncrementDate From ToolLedger where ToolsID in (select ToolsID from @tbluutru) and (PostedDate <= @ToDate)) a
	WHERE ToolsID = a.Tid
	
	UPDATE @tbluutru SET PostedDate = b.PostedDate
	FROM (Select DISTINCT ToolsID as Tid, PostedDate From ToolLedger 
		  where ToolsID in (select ToolsID from @tbluutru) And TypeID = 431 and (PostedDate <= @ToDate)) b
	WHERE ToolsID = b.Tid
	
	UPDATE @tbluutru SET DecrementAllocationTime = ISNULL(c.skdapb,0)
	FROM (Select DISTINCT ToolsID as Tid, SUM(DecrementAllocationTime) as skdapb From ToolLedger 
		  where ToolsID in (select ToolsID from @tbluutru) and (PostedDate <= @ToDate) and TypeID <> 431
		  group by ToolsID) c
	WHERE ToolsID = c.Tid
	
	DECLARE @id UNIQUEIDENTIFIER
	
	DECLARE cursorConLai CURSOR FOR
	SELECT ToolsID FROM @tbluutru
	OPEN cursorConLai
	FETCH NEXT FROM cursorConLai INTO @id
	WHILE @@FETCH_STATUS = 0
	BEGIN
		UPDATE @tbluutru SET RemainingAmount = d.RemainingAmount
		FROM (select Top 1 ToolsID as Tid, RemainingAmount from ToolLedger where ToolsID = @id and (PostedDate <= @ToDate) order by PostedDate DESC) d
		WHERE ToolsID = d.Tid
		FETCH NEXT FROM cursorConLai INTO @id
	END
	CLOSE cursorConLai
	DEALLOCATE cursorConLai
	
	UPDATE @tbluutru SET RemainingAllocationTime = ISNULL(AllocationTimes,0) - ISNULL(DecrementAllocationTime,0),
						 DecrementAmount = ISNULL(Amount,0) - ISNULL(RemainingAmount,0)
	
	select * from @tbluutru
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_ReceivableDetail]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3) ,
    @IsSimilarSum BIT 
AS
    BEGIN
		
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        DECLARE @ID UNIQUEIDENTIFIER
        SET @ID = '00000000-0000-0000-0000-000000000000'
               
        CREATE TABLE #tblListAccountObjectID  
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25)
                 ,
              AccountObjectName NVARCHAR(255)
                 ,
              AccountObjectAddress NVARCHAR(255)
                 ,
              AccountObjectGroupListCode NVARCHAR(MAX)
                
                NULL ,
              AccountObjectGroupListName NVARCHAR(MAX)
                
                NULL ,
              CompanyTaxCode NVARCHAR(50) 
                                          NULL
            ) 
        INSERT  INTO #tblListAccountObjectID
                SELECT  AO.ID ,
                        AccountingObjectCode ,
                        AccountingObjectName ,
                        [Address] ,
                        AOG.AccountingObjectGroupCode , 
                        AOG.AccountingObjectGroupName , 
                        CASE WHEN AO.ObjectType = 0 THEN TaxCode
                             ELSE ''
                        END AS CompanyTaxCode 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                           ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value 
                        left JOIN dbo.AccountingObjectGroup AOG ON AO.AccountObjectGroupID = AOG.ID 
          
        CREATE TABLE #tblResult
            (
              RowNum INT PRIMARY KEY
                         IDENTITY(1, 1) ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100)
                
                NULL ,
              AccountObjectName NVARCHAR(255)
                
                NULL ,
              AccountObjectGroupListCode NVARCHAR(MAX)
                
                NULL , 
              AccountObjectGroupListName NVARCHAR(MAX)
                
                NULL ,          
              AccountObjectAddress NVARCHAR(255)
                
                NULL , 
              CompanyTaxCode NVARCHAR(50) 
                                          NULL ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25) 
                                 NULL ,
              InvDate DATETIME ,
              InvNo NVARCHAR(MAX) 
                                  NULL ,
              
              RefType INT ,
              RefID UNIQUEIDENTIFIER ,                        
              RefDetailID NVARCHAR(50) 
                                       NULL ,
              JournalMemo NVARCHAR(255) 
                                        NULL ,
              AccountNumber NVARCHAR(20) 
                                         NULL ,
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20)
                
                NULL ,
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,
              DebitAmount MONEY , 
              CreditAmountOC MONEY ,
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY , 
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50)
                 , 
              InventoryItemName NVARCHAR(255)
                 ,
              UnitName NVARCHAR(20) 
                                    NULL ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(20, 6) ,            
              IsBold BIT , 
              OrderType INT ,
              BranchName NVARCHAR(128) 
                                       NULL ,
              GLJournalMemo NVARCHAR(255) 
                                          NULL 
              ,
              MasterCustomField1 NVARCHAR(255)
                
                NULL ,
              MasterCustomField2 NVARCHAR(255)
                
                NULL ,
              MasterCustomField3 NVARCHAR(255)
                
                NULL ,
              MasterCustomField4 NVARCHAR(255)
                
                NULL ,
              MasterCustomField5 NVARCHAR(255)
                
                NULL ,
              MasterCustomField6 NVARCHAR(255)
                
                NULL ,
              MasterCustomField7 NVARCHAR(255)
                
                NULL ,
              MasterCustomField8 NVARCHAR(255)
                
                NULL ,
              MasterCustomField9 NVARCHAR(255)
                
                NULL ,
              MasterCustomField10 NVARCHAR(255)
                
                NULL ,
              CustomField1 NVARCHAR(255) 
                                         NULL ,
              CustomField2 NVARCHAR(255) 
                                         NULL ,
              CustomField3 NVARCHAR(255) 
                                         NULL ,
              CustomField4 NVARCHAR(255) 
                                         NULL ,
              CustomField5 NVARCHAR(255) 
                                         NULL ,
              CustomField6 NVARCHAR(255) 
                                         NULL ,
              CustomField7 NVARCHAR(255) 
                                         NULL ,
              CustomField8 NVARCHAR(255) 
                                         NULL ,
              CustomField9 NVARCHAR(255) 
                                         NULL ,
              CustomField10 NVARCHAR(255) 
                                          NULL ,
              /*cột số lượng, đơn giá*/
              MainUnitName NVARCHAR(255) 
                                         NULL ,
              MainUnitPrice DECIMAL(20, 6) ,
              MainQuantity DECIMAL(22, 8) ,
              /*mã thống kê, và tên loại chứng từ*/
              ListItemCode NVARCHAR(20) ,
              ListItemName NVARCHAR(128) ,
              RefTypeName NVARCHAR(128)
            )
        
        CREATE TABLE #tblAccountNumber
            (
              AccountNumber NVARCHAR(20) 
                                         PRIMARY KEY ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL
            BEGIN
			INSERT  INTO #tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
            END
        ELSE
            BEGIN 
			   INSERT  INTO #tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   DetailType = '1'
                                AND Grade = 1
                                AND IsParentNode = 0
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END 

/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
		
        IF @IsSimilarSum = 0 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,  
                          AccountObjectName ,	
                          AccountObjectGroupListCode ,
                          AccountObjectGroupListName ,        
                          AccountObjectAddress , 
                          CompanyTaxCode , 
                          PostedDate ,
                          RefDate , 
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,	
                          RefID ,  
                          RefDetailID ,
                          JournalMemo , 				  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,			  
                          ExchangeRate , 		  
                          DebitAmountOC ,
                          DebitAmount , 
                          CreditAmountOC , 
                          CreditAmount , 
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,	
                          ClosingCreditAmount ,               
                          IsBold , 
                          OrderType
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode , 
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,           
                                AccountObjectAddress ,
                                CompanyTaxCode , 
                                PostedDate ,
                                RefDate ,
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID ,
                                RefDetailID ,
                                JournalMemo , 				  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  
                                ExchangeRate ,  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) ,
                                SUM(CreditAmount) ,
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN SUM(ClosingDebitAmountOC)
                                            - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) ) > 0
                                                 THEN ( SUM(ClosingDebitAmountOC)
                                                        - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,         
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( SUM(ClosingDebitAmount)
                                              - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount)
                                                        - SUM(ClosingCreditAmount) ) > 0
                                                 THEN SUM(ClosingDebitAmount)
                                                      - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmountOC)
                                              - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) ) > 0
                                                 THEN ( SUM(ClosingCreditAmountOC)
                                                        - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( SUM(ClosingCreditAmount)
                                              - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) ) > 0
                                                 THEN ( SUM(ClosingCreditAmount)
                                                        - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,
                                
                                IsBold , 
                                OrderType 
                                
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,	
                                            LAOI.AccountObjectGroupListCode ,
                                            LAOI.AccountObjectGroupListName ,          
                                            LAOI.AccountObjectAddress , 
                                            LAOI.CompanyTaxCode , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,             
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefDate
                                            END AS RefDate , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.RefNo
                                            END AS RefNo , 
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN N'Số dư đầu kỳ'
                                                 ELSE ISNULL(AOL.DESCRIPTION,
                                                             AOL.Reason)
                                            END AS JournalMemo ,   
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber ,
                                            
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN NULL
                                                 ELSE AOL.ExchangeRate
                                            END AS ExchangeRate ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                               
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                     
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC ,           
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 1
                                                 ELSE 0
                                            END AS IsBold ,	
                                            CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType 
                                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                            INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                            INNER JOIN #tblListAccountObjectID
                                            AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )

                                ) AS RSNS
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode , 
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,        
                                RSNS.AccountObjectAddress ,
                                CompanyTaxCode , 
                                RSNS.PostedDate ,
                                RSNS.RefDate ,
                                RSNS.RefNo , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,   
                                RSNS.RefDetailID ,
                                RSNS.JournalMemo , 				  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber , 		  
                                RSNS.ExchangeRate ,           
                                RSNS.IsBold ,
                                RSNS.OrderType
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC
                                       - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount
                                       - ClosingCreditAmount) <> 0
                        ORDER BY RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.OrderType ,
                                RSNS.PostedDate ,
                                RSNS.RefDate ,
                                RSNS.RefNo 
            END
        ELSE 
            BEGIN
                INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode ,   
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,            
                          AccountObjectAddress ,
                          CompanyTaxCode ,
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID ,   
                          RefDetailID ,
                          JournalMemo , 			  
                          AccountNumber ,
                          AccountCategoryKind , 
                          CorrespondingAccountNumber ,		  
                          ExchangeRate ,		  
                          DebitAmountOC ,
                          DebitAmount ,
                          CreditAmountOC , 
                          CreditAmount ,
                          ClosingDebitAmountOC ,
                          ClosingDebitAmount , 
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount ,                                    
                          IsBold , 
                          OrderType
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,  
                                AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,             
                                AccountObjectAddress , 
                                CompanyTaxCode ,
                                PostedDate ,	
                                RefDate , 
                                RefNo , 
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RefID , 
                                RefDetailID ,
                                JournalMemo ,				  
                                AccountNumber ,
                                AccountCategoryKind ,
                                CorrespondingAccountNumber ,			  
                                ExchangeRate , 	  
                                DebitAmountOC ,
                                DebitAmount ,
                                CreditAmountOC ,
                                CreditAmount , 
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmountOC
                                              - ClosingCreditAmountOC )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmountOC
                                                        - ClosingCreditAmountOC ) > 0
                                                 THEN ClosingDebitAmountOC
                                                      - ClosingCreditAmountOC
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,  
                                ( CASE WHEN AccountCategoryKind = 0
                                       THEN ( ClosingDebitAmount
                                              - ClosingCreditAmount )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( ClosingDebitAmount
                                                        - ClosingCreditAmount ) > 0
                                                 THEN ClosingDebitAmount
                                                      - ClosingCreditAmount
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmountOC
                                              - ClosingDebitAmountOC )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC ) > 0
                                                 THEN ( ClosingCreditAmountOC
                                                        - ClosingDebitAmountOC )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1
                                       THEN ( ClosingCreditAmount
                                              - ClosingDebitAmount )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount ) > 0
                                                 THEN ( ClosingCreditAmount
                                                        - ClosingDebitAmount )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount ,                                     
                                IsBold , 
                                OrderType
                        FROM    ( SELECT    AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode ,
                                            AD.AccountObjectGroupListName ,            
                                            AD.AccountObjectAddress ,
                                            CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType , 
                                            MAX(AD.RefDetailID) AS RefDetailID ,
                                            AD.JournalMemo ,   
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber , 
                                            AD.ExchangeRate , 
                                            SUM(AD.DebitAmountOC) AS DebitAmountOC ,                              
                                            SUM(AD.DebitAmount) AS DebitAmount ,                                        
                                            SUM(AD.CreditAmountOC) AS CreditAmountOC , 
                                            SUM(AD.CreditAmount) AS CreditAmount ,
                                            SUM(AD.ClosingDebitAmountOC) AS ClosingDebitAmountOC ,       
                                            SUM(AD.ClosingDebitAmount) AS ClosingDebitAmount ,
                                            SUM(AD.ClosingCreditAmountOC) AS ClosingCreditAmountOC , 
                                            SUM(AD.ClosingCreditAmount) AS ClosingCreditAmount ,
                                            AD.IsBold ,	
                                            AD.OrderType 
                                  FROM      ( SELECT    AOL.AccountingObjectID ,
                                                        LAOI.AccountObjectCode ,   
                                                        LAOI.AccountObjectName ,	
                                                        LAOI.AccountObjectGroupListCode ,
                                                        LAOI.AccountObjectGroupListName ,              
                                                        LAOI.AccountObjectAddress , 
                                                        LAOI.CompanyTaxCode , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.PostedDate
                                                        END AS PostedDate ,             
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefDate
                                                        END AS RefDate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.RefNo
                                                        END AS RefNo , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceDate
                                                        END AS InvDate ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.InvoiceNo
                                                        END AS InvNo ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.ReferenceID
                                                        END AS RefID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.TypeID
                                                        END AS RefType ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE CAST(DetailID AS NVARCHAR(50))
                                                        END AS RefDetailID ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN N'Số dư đầu kỳ'
                                                             ELSE AOL.Reason
                                                        END AS JournalMemo , 
                                                        TBAN.AccountNumber ,
                                                        TBAN.AccountCategoryKind ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN NULL
                                                             ELSE AOL.AccountCorresponding
                                                        END AS CorrespondingAccountNumber , 
                                                        
                                                        CASE WHEN AOL.PostedDate < @FromDate
															 THEN NULL
															 ELSE AOL.ExchangeRate
														END AS ExchangeRate , 
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmountOriginal
                                                        END AS DebitAmountOC ,                         
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.DebitAmount
                                                        END AS DebitAmount ,                           
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmountOriginal
                                                        END AS CreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN $0
                                                             ELSE AOL.CreditAmount
                                                        END AS CreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingDebitAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.DebitAmount
                                                             ELSE $0
                                                        END AS ClosingDebitAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmountOriginal
                                                             ELSE $0
                                                        END AS ClosingCreditAmountOC ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN AOL.CreditAmount
                                                             ELSE $0
                                                        END AS ClosingCreditAmount ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 1
                                                             ELSE 0
                                                        END AS IsBold ,
                                                        CASE WHEN AOL.PostedDate < @FromDate
                                                             THEN 0
                                                             ELSE 1
                                                        END AS OrderType 
                                                        
                                              FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                                        INNER JOIN #tblListAccountObjectID
                                                        AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                                                        INNER JOIN #tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumber
                                                              + '%'
                                              WHERE     AOL.PostedDate <= @ToDate
                                                        AND ( @CurrencyID IS NULL
                                                              OR AOL.CurrencyID = @CurrencyID
                                                            )
                                            ) AS AD
                                  GROUP BY  AD.AccountingObjectID ,
                                            AD.AccountObjectCode , 
                                            AD.AccountObjectName ,
                                            AD.AccountObjectGroupListCode , 
                                            AD.AccountObjectGroupListName ,     
                                            AD.AccountObjectAddress ,
                                            AD.CompanyTaxCode ,
                                            AD.PostedDate ,
                                            AD.RefDate ,
                                            AD.RefNo ,
                                            AD.InvDate ,
                                            AD.InvNo ,
                                            AD.RefID ,
                                            AD.RefType ,
                                            AD.JournalMemo ,
                                            AD.AccountNumber ,
                                            AD.AccountCategoryKind ,
                                            AD.CorrespondingAccountNumber ,  
                                            AD.ExchangeRate ,
                                            AD.IsBold , 	
                                            AD.OrderType
                                ) AS RSS
                        WHERE   RSS.DebitAmountOC <> 0
                                OR RSS.CreditAmountOC <> 0
                                OR RSS.DebitAmount <> 0
                                OR RSS.CreditAmount <> 0
                                OR ClosingCreditAmountOC
                                - ClosingDebitAmountOC <> 0
                                OR ClosingCreditAmount - ClosingDebitAmount <> 0
                        ORDER BY RSS.AccountObjectCode ,
                                RSS.AccountNumber ,
                                RSS.OrderType ,
                                RSS.PostedDate ,
                                RSS.RefDate ,
                                RSS.RefNo ,
                                RSS.InvDate ,
                                RSS.InvNo
            END

/* Tính số tồn */
        DECLARE @CloseAmountOC AS DECIMAL(22, 8) ,
            @CloseAmount AS DECIMAL(22, 8) ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0
                                        THEN ( CASE WHEN ClosingDebitAmountOC = 0
                                                    THEN ClosingCreditAmountOC
                                                    ELSE -1
                                                         * ClosingDebitAmountOC
                                               END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber
                                        THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC
                                             - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0
                                              THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1
                                              THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0
                                                        THEN -1
                                                             * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1
                                               THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0
                                               THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0
                                                         THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0
                                      THEN ( CASE WHEN ClosingDebitAmount = 0
                                                  THEN ClosingCreditAmount
                                                  ELSE -1 * ClosingDebitAmount
                                             END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber
                                      THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount
                                           - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0
                                            THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1
                                            THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0
                                                      THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1
                                             THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0
                                             THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0
                                                       THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
        SELECT  RS.*
        FROM    #tblResult RS
        ORDER BY RowNum
        
        DROP TABLE #tblResult
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*=================================================================================================
 * Created By:		haipl
 * Created Date:	25/12/2017
 * Description:		Lay du lieu cho bao cao << Sổ chi tiết bán hàng >>
===================================================================================================== */
ALTER PROCEDURE [dbo].[Proc_SA_GetSalesBookDetail]
  
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @InventoryItemID NVARCHAR(MAX) , 
    @OrganizationUnitID UNIQUEIDENTIFIER ,
    @AccountObjectID NVARCHAR(MAX) ,
	@SumGiaVon decimal output
AS

    BEGIN
        SET NOCOUNT ON;
        declare @v1 DECIMAL(29,4)                            
        CREATE TABLE #tblListInventoryItemID
            (
              InventoryItemID UNIQUEIDENTIFIER PRIMARY KEY
            ) 
        INSERT  INTO #tblListInventoryItemID
                    SELECT  T.*
                    FROM    dbo.Func_ConvertStringIntoTable(@InventoryItemID,
                                                              ',')	
                                                              AS T  
        /*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,0),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,0),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate between  @FromDate and @ToDate
		/*end add by cuongpv*/

		select a.Date as ngay_CT, 
		a.PostedDate as ngay_HT, 
		a.No as So_Hieu,
		a.InvoiceDate as Ngay_HD, 
		a.InvoiceNo as SO_HD, 
		d.reason  as Dien_giai, 
		accountCorresponding as TK_DOIUNG,
		c.Unit as DVT, 
		b.Quantity as SL, 
		b.UnitPrice as DON_GIA, 
		sum(debitAmount) as KHAC, 
		sum(CreditAmount) as TT, 
		c.ID as  InventoryItemID,
		c.MaterialGoodsName
		from @tbDataGL a join SAInvoiceDetail b on a.DetailID = b.ID /*edit by cuongpv GeneralLedger -> @tbDataGL*/
		join MaterialGoods c on b.MaterialGoodsID = c.ID
		join SAInvoice d on a.ReferenceID = d.ID
		where a.PostedDate between  @FromDate and @ToDate and c.ID in (select InventoryItemID from #tblListInventoryItemID)
		and Account like '511%'
		group by a.Date,  a.PostedDate, a.No ,a.InvoiceDate , a.InvoiceNo , Sreason , accountCorresponding , 
		c.Unit , b.Quantity , b.UnitPrice, c.ID,c.MaterialGoodsName,d.reason ;


		 select isnull(sum(OWAmount),0) Sum_Gia_Goc, b.MaterialGoodsCode ,b.ID InventoryItemID from SAInvoiceDetail a, MaterialGoods b 
		 where exists (select InventoryItemID from #tblListInventoryItemID where InventoryItemID = a.MaterialGoodsID)
		 and exists ( select ReferenceID from GeneralLedger where  account = '632' and PostedDate between @FromDate and @ToDate and ReferenceID=a.SAinvoiceID )
		 and a.MaterialGoodsID=b.ID group by b.ID,b.MaterialGoodsCode


    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_GetReceivableSummaryByGroup]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountNumber NVARCHAR(25),
    @AccountObjectID AS NVARCHAR(MAX),
    @CurrencyID NVARCHAR(3),
    @AccountObjectGroupID AS NVARCHAR(MAX) 
AS
    BEGIN          
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

		DECLARE @tblListAccountObjectID TABLE
            (
             AccountObjectID UNIQUEIDENTIFIER ,
             AccountObjectGroupList NVARCHAR(MAX)           
            ) 
             
        INSERT  INTO @tblListAccountObjectID
        SELECT  
			AO.ID
			,AO.AccountObjectGroupID
        FROM AccountingObject AS AO 
        LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') AS TLAO 
			ON AO.ID = TLAO.Value
        WHERE 
		(@AccountObjectID IS NULL OR TLAO.Value IS NOT NULL)
                                    		
        DECLARE @tblAccountNumber TABLE
            (
             AccountNumber NVARCHAR(255) PRIMARY KEY,
             AccountName NVARCHAR(255),
             AccountNumberPercent NVARCHAR(255),
             AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber,
                                A.AccountName,
                                A.AccountNumber + '%',
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber,
                                A.AccountName                 
            END
        ELSE
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber,
                                A.AccountName,
                                A.AccountNumber + '%',
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   DetailType = '1'
                                AND Grade = 1
                                AND IsParentNode = 0
                        ORDER BY A.AccountNumber,
                                A.AccountName
            END
	    
	    DECLARE @tblDebt TABLE
			(
				AccountObjectID			UNIQUEIDENTIFIER
				,AccountNumber			NVARCHAR (25)				
				,AccountObjectGroupList NVARCHAR(Max)				
				,OpenDebitAmountOC		DECIMAL (28,4)
				,OpenDebitAmount		DECIMAL (28,4)
				,OpenCreditAmountOC		DECIMAL (28,4)
				,OpenCreditAmount		DECIMAL (28,4)
				,DebitAmountOC			DECIMAL (28,4)
				,DebitAmount			DECIMAL (28,4)
				,CreditAmountOC			DECIMAL (28,4)
				,CreditAmount			DECIMAL (28,4)
				,CloseDebitAmountOC		DECIMAL (28,4)
				,CloseDebitAmount		DECIMAL (28,4)
				,CloseCreditAmountOC	DECIMAL (28,4)
				,CloseCreditAmount		DECIMAL (28,4)					
			)				
		INSERT @tblDebt
        
        SELECT  
                AccountingObjectID,               
                AccountNumber,       
                AccountObjectGroupList,
                (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN (SUM(OpenningDebitAmountOC - OpenningCreditAmountOC)) > 0
                                THEN (SUM(OpenningDebitAmountOC - OpenningCreditAmountOC))
                                ELSE $0
                           END
                 END) AS OpenningDebitAmountOC,        
                (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmount - OpenningCreditAmount)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN SUM(OpenningDebitAmount - OpenningCreditAmount) > 0
                                THEN SUM(OpenningDebitAmount  - OpenningCreditAmount)
                                ELSE $0
                           END
                 END) AS OpenningDebitAmount,
                (CASE WHEN AccountCategoryKind = 1
                      THEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC))
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC)) > 0
                                THEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC))
                                ELSE $0
                           END
                 END) AS OpenningCreditAmountOC,
                (CASE WHEN AccountCategoryKind = 1
                      THEN (SUM(OpenningCreditAmount - OpenningDebitAmount))
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmount - OpenningDebitAmount)) > 0
                                THEN (SUM(OpenningCreditAmount - OpenningDebitAmount))
                                ELSE $0
                           END
                 END) AS OpenningCreditAmount, 
                SUM(DebitAmountOC) AS DebitAmountOC,
                SUM(DebitAmount) AS DebitAmount, 
                SUM(CreditAmountOC) AS CreditAmountOC,
                SUM(CreditAmount) AS CreditAmount, 
                (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC + DebitAmountOC - CreditAmountOC)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC + DebitAmountOC - CreditAmountOC) > 0
                                THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC + DebitAmountOC - CreditAmountOC)
                                ELSE $0
                           END
                 END) AS CloseDebitAmountOC,	
                 (CASE WHEN AccountCategoryKind = 0
                      THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                               + DebitAmount - CreditAmount)
                      WHEN AccountCategoryKind = 1 THEN $0
                      ELSE CASE WHEN SUM(OpenningDebitAmount
                                         - OpenningCreditAmount - CreditAmount
                                         + DebitAmount) > 0
                                THEN SUM(OpenningDebitAmount
                                         - OpenningCreditAmount - CreditAmount
                                         + DebitAmount)
                                ELSE $0
                           END
                 END) AS CloseDebitAmount,
                (CASE WHEN AccountCategoryKind = 1
                      THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC - DebitAmountOC + CreditAmountOC)
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmountOC - OpenningDebitAmountOC - DebitAmountOC + CreditAmountOC)) > 0
                                THEN SUM(OpenningCreditAmountOC  - OpenningDebitAmountOC - DebitAmountOC + CreditAmountOC)
                                ELSE $0
                           END
                 END) AS CloseCreditAmountOC,	     
                (CASE WHEN AccountCategoryKind = 1
                      THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                               + CreditAmount - DebitAmount)
                      WHEN AccountCategoryKind = 0 THEN $0
                      ELSE CASE WHEN (SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)) > 0
                                THEN SUM(OpenningCreditAmount
                                         - OpenningDebitAmount + CreditAmount
                                         - DebitAmount)
                                ELSE $0
                           END
                 END) AS CloseCreditAmount
        FROM    (SELECT AOL.AccountingObjectID,                        
                        TBAN.AccountNumber,
                        TBAN.AccountCategoryKind,
                        ISNULL(AccountObjectGroupList,'') AccountObjectGroupList,
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.DebitAmountOriginal
                             ELSE $0
                        END) AS OpenningDebitAmountOC,	
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.DebitAmount
                             ELSE $0
                        END) AS OpenningDebitAmount, 
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.CreditAmountOriginal
                             ELSE $0
                        END) AS OpenningCreditAmountOC, 
                        SUM(CASE WHEN AOL.PostedDate < @FromDate
                             THEN AOL.CreditAmount
                             ELSE $0
                        END) AS OpenningCreditAmount,                                         
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.DebitAmountOriginal
                             ELSE 0
                        END) AS DebitAmountOC,                      
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.DebitAmount
                             ELSE 0
                        END) AS DebitAmount,                    
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.CreditAmountOriginal
                             ELSE 0
                        END) AS CreditAmountOC, 
                        SUM(CASE WHEN AOL.PostedDate BETWEEN @FromDate AND @ToDate
                             THEN AOL.CreditAmount
                             ELSE 0
                        END) AS CreditAmount
                 FROM   @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/    
                        INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                        INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                        INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                      
                 WHERE  AOL.PostedDate <= @ToDate
                        AND (@CurrencyID IS NULL OR AOL.CurrencyID = @CurrencyID)
                        AND AN.DetailType = '1'
                 GROUP BY 
						AOL.AccountingObjectID,                       
                        TBAN.AccountNumber, 
                        TBAN.AccountCategoryKind
                        ,AccountObjectGroupList                      
                ) AS RSNS
       
        GROUP BY RSNS.AccountingObjectID,               
                 RSNS.AccountNumber 
                 ,RSNS.AccountCategoryKind
                 ,RSNS.AccountObjectGroupList
        HAVING
				SUM(DebitAmountOC)<>0 OR
                SUM(DebitAmount)<>0 OR
                SUM(CreditAmountOC) <>0 OR
                SUM(CreditAmount) <>0 OR
                SUM(OpenningDebitAmountOC - OpenningCreditAmountOC)<>0 OR
                SUM(OpenningDebitAmount - OpenningCreditAmount)<>0
                

		SELECT   AOG.ID,
		        AOG.AccountingObjectGroupCode,
				AOG.AccountingObjectGroupName
		        ,AO.AccountingObjectCode
				,AO.AccountingObjectName
				,Ao.Address AS AccountObjectAddress
				,D.* FROM @tblDebt D
		INNER JOIN dbo.AccountingObject AO ON D.AccountObjectID = AO.ID
		left JOIN dbo.AccountingObjectGroup AOG ON AO.AccountObjectGroupID = AOG.ID
		ORDER BY D.AccountNumber
		
		
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_GetReceivableSummary]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER ,
    @AccountNumber NVARCHAR(25) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3) ,
    @IsShowInPeriodOnly BIT 
AS
    BEGIN
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/
		       
        DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectGroupListCode NVARCHAR(MAX) ,
              AccountObjectCategoryName NVARCHAR(MAX)
            ) 
             
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID ,
                        AO.AccountObjectGroupID ,
						null
                FROM    AccountingObject AS AO
                        LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                              ',') AS TLAO ON AO.ID = TLAO.Value
                        
                WHERE  
				      ( @AccountObjectID IS NULL
                              OR TLAO.Value IS NOT NULL
                            )
                              			
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(255) PRIMARY KEY ,
              AccountName NVARCHAR(255) ,
              AccountNumberPercent NVARCHAR(255) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   DetailType = '1'
                                AND Grade = 1
                                AND IsParentNode = 0
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
            
	    DECLARE @FirstDateOfYear AS DATETIME
	    SET @FirstDateOfYear = '1/1/' + CAST(Year(@FromDate) AS NVARCHAR(4))	   
	    
        SELECT  ROW_NUMBER() OVER ( ORDER BY AccountingObjectCode ) AS RowNum ,
                AccountingObjectID ,
                AccountingObjectCode ,
                AccountingObjectName ,
                AccountObjectAddress ,
                AccountObjectTaxCode ,
                AccountNumber , 
                AccountCategoryKind ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC)
                            - SUM(OpenningCreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) ) > 0
                                 THEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpenningDebitAmountOC ,        
                ( CASE WHEN AccountCategoryKind = 0
                       THEN ( SUM(OpenningDebitAmount - OpenningCreditAmount) )
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmount
                                            - OpenningCreditAmount) ) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount)
                                 ELSE $0
                            END
                  END ) AS OpenningDebitAmount , 
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmountOC
                                  - OpenningDebitAmountOC) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) ) > 0
                                 THEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpenningCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmount - OpenningDebitAmount) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) ) > 0
                                 THEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) )
                                 ELSE $0
                            END
                  END ) AS OpenningCreditAmount ,
                SUM(DebitAmountOC) AS DebitAmountOC ,
                SUM(DebitAmount) AS DebitAmount ,
                SUM(CreditAmountOC) AS CreditAmountOC ,
                SUM(CreditAmount) AS CreditAmount ,
                SUM(AccumDebitAmountOC) AS AccumDebitAmountOC ,
                SUM(AccumDebitAmount) AS AccumDebitAmount ,
                SUM(AccumCreditAmountOC) AS AccumCreditAmountOC ,
                SUM(AccumCreditAmount) AS AccumCreditAmount , 
                                        
                /* Số dư cuối kỳ = Dư Có đầu kỳ - Dư Nợ đầu kỳ + Phát sinh Có – Phát sinh Nợ
				Nếu Số dư cuối kỳ >0 thì hiển bên cột Dư Có cuối kỳ 
				Nếu số dư cuối kỳ <0 thì hiển thị bên cột Dư Nợ cuối kỳ */
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC
                                + DebitAmountOC - CreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC) > 0
                                 THEN SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseDebitAmountOC ,	
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC
                                - DebitAmountOC + CreditAmountOC)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC
                                            - DebitAmountOC + CreditAmountOC) ) > 0
                                 THEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          - DebitAmountOC + CreditAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmountOC ,	
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                                + DebitAmount - CreditAmount)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount
                                          - CreditAmount + DebitAmount) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount
                                          - CreditAmount + DebitAmount)
                                 ELSE $0
                            END
                  END ) AS CloseDebitAmount ,	
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                                + CreditAmount - DebitAmount)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount
                                            + CreditAmount - DebitAmount) ) > 0
                                 THEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmount ,	
                AccountObjectGroupListCode ,
                AccountObjectCategoryName
        FROM    ( SELECT    AO.ID as AccountingObjectID ,
                            AO.AccountingObjectCode ,  
                            AO.AccountingObjectName ,	
                            AO.Address AS AccountObjectAddress ,
                            AO.TaxCode AS AccountObjectTaxCode , 
                            TBAN.AccountNumber , 
                            TBAN.AccountCategoryKind , 
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmountOC ,
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.DebitAmount
                                 ELSE $0
                            END AS OpenningDebitAmount ,
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmountOC , 
                            CASE WHEN GL.PostedDate < @FromDate
                                 THEN GL.CreditAmount
                                 ELSE $0
                            END AS OpenningCreditAmount ,                                                
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.DebitAmountOriginal
                                 ELSE 0
                            END AS DebitAmountOC ,                       
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.DebitAmount
                                 ELSE 0
                            END AS DebitAmount ,                                    
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.CreditAmountOriginal
                                 ELSE 0
                            END AS CreditAmountOC ,
                            CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                 THEN GL.CreditAmount
                                 ELSE 0
                            END AS CreditAmount ,
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.DebitAmountOriginal
                                 ELSE 0
                            END AS AccumDebitAmountOC ,                                  
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.DebitAmount
                                 ELSE 0
                            END AS AccumDebitAmount ,                                       
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.CreditAmountOriginal
                                 ELSE 0
                            END AS AccumCreditAmountOC ,
                            CASE WHEN GL.PostedDate BETWEEN @FirstDateOfYear AND @ToDate
                                 THEN GL.CreditAmount
                                 ELSE 0
                            END AS AccumCreditAmount ,
                            LAOI.AccountObjectGroupListCode ,
                            LAOI.AccountObjectCategoryName 
                  FROM      @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                            INNER JOIN dbo.AccountingObject AO ON AO.ID = GL.AccountingObjectID
                            INNER JOIN @tblAccountNumber TBAN ON GL.Account LIKE TBAN.AccountNumberPercent
                            INNER JOIN dbo.Account AS AN ON GL.Account = AN.AccountNumber
                            INNER JOIN @tblListAccountObjectID AS LAOI ON GL.AccountingObjectID = LAOI.AccountObjectID
                  WHERE     GL.PostedDate <= @ToDate
                            AND ( @CurrencyID IS NULL
                                  OR GL.CurrencyID = @CurrencyID
                                )
                            AND AN.DetailType = '1'
                ) AS RSNS
        GROUP BY RSNS.AccountingObjectID ,
                RSNS.AccountingObjectCode ,
                RSNS.AccountingObjectName ,    
                RSNS.AccountObjectAddress ,
                RSNS.AccountObjectTaxCode , 
                RSNS.AccountNumber ,
                RSNS.AccountCategoryKind ,
                RSNS.AccountObjectGroupListCode ,
                RSNS.AccountObjectCategoryName 
        HAVING  SUM(DebitAmountOC) <> 0
                OR SUM(DebitAmount) <> 0
                OR SUM(CreditAmountOC) <> 0
                OR SUM(CreditAmount) <> 0
                OR SUM(OpenningDebitAmountOC - OpenningCreditAmountOC) <> 0
                OR SUM(OpenningDebitAmount - OpenningCreditAmount) <> 0                
                OR(@IsShowInPeriodOnly = 0 AND (
                 SUM(AccumDebitAmountOC) <>0	
                OR SUM(AccumDebitAmount) <>0
                OR SUM(AccumCreditAmountOC) <>0  
                OR SUM(AccumCreditAmount)<>0))
        ORDER BY RSNS.AccountingObjectCode
        OPTION(RECOMPILE)
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*Edit by: cuongpv
*Edit Date: 07/05/2019
*Description: lay them du lieu tu cac bang PPService, PPDiscountReturn, SAReturn
*/
ALTER procedure [dbo].[Proc_SA_GetDetailPayS12] 
(@fromdate DATETIME,
 @todate DATETIME,
 @account nvarchar(255),
 @currency nvarchar(3),
 @AccountObjectID AS NVARCHAR(MAX)
)
as
begin
DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(15),
			  AccountGroupKind int
            )
INSERT  INTO @tblAccountNumber
                SELECT  AO.AccountNumber,
				        AO.AccountGroupKind
                FROM    Account AS AO
                        inner JOIN dbo.Func_ConvertStringIntoTable_Nvarchar(@account,
                                                              ',') AS TLAO ON AO.AccountNumber = TLAO.Value
DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
			  AccountObjectCode NVARCHAR(100),
              AccountObjectGroupListCode NVARCHAR(MAX) ,
              AccountObjectCategoryName NVARCHAR(MAX)
            ) 
             
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID ,
				        AO.AccountingObjectCode,
                        AO.AccountObjectGroupID ,
						null
                FROM    AccountingObject AS AO
                        inner JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                              ',') AS TLAO ON AO.ID = TLAO.Value
                        

		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @todate
		/*end add by cuongpv*/

DECLARE @tblResult TABLE
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              AccountNumber NVARCHAR(20)  ,
			  AccountGroupKind int, 
              AccountObjectID UNIQUEIDENTIFIER ,
			  AccountObjectCode NVARCHAR(100)  ,
			  RefDate DATETIME , 
			  RefNo NVARCHAR(25)  ,
			  PostedDate DATETIME ,
			  JournalMemo NVARCHAR(255)  ,
			  CorrespondingAccountNumber NVARCHAR(20)  , 
			  DueDate DATETIME,
			  DebitAmount MONEY , 
              CreditAmount MONEY , 
			  ClosingDebitAmount MONEY , 
              ClosingCreditAmount MONEY,
			  OrderType int 
            )

if @account = '331'
Begin
	/*add by cuongpv tao bang du lieu DueDate tu cac bang*/
	DECLARE @tbDataDueDate TABLE(
		ID UNIQUEIDENTIFIER,
		IDDetail UNIQUEIDENTIFIER,
		DueDate DATETIME
	)
	/*end add by cuongpv*/
	/*add by cuongpv lay du lieu DueDate tu cac bang PPinvoice, PPservice, PPDiscountReturn*/
	INSERT INTO @tbDataDueDate
	SELECT Piv.ID, Pivdt.ID, Piv.DueDate FROM PPInvoiceDetail Pivdt LEFT JOIN PPInvoice Piv ON Piv.ID = Pivdt.PPInvoiceID
	WHERE Pivdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)
	
	UNION ALL

	SELECT Psv.ID, Psvdt.ID, Psv.DueDate FROM PPServiceDetail Psvdt LEFT JOIN PPService Psv ON Psv.ID = Psvdt.PPServiceID
	WHERE Psvdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)

	UNION ALL

	SELECT Pdcrt.ID, Pdcrtdt.ID, Pdcrt.DueDate FROM PPDiscountReturnDetail Pdcrtdt LEFT JOIN PPDiscountReturn Pdcrt ON Pdcrt.ID = Pdcrtdt.PPDiscountReturnID
	WHERE Pdcrtdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)
	/*end add by cuongpv*/
	insert into @tblResult
	SELECT  a.AccountNumber,
			A.AccountGroupKind,
			LAOI.AccountObjectID,
			LAOI.AccountObjectCode,
			null as NgayThangGS,
			null as Sohieu, 
			null as ngayCTu,
			N'Số dư đầu kỳ' as Diengiai ,
			null as TKDoiUng, 
			null as ThoiHanDuocCKhau, 
			$0 AS PSNO ,
			$0 AS PSCO ,
			CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
					THEN SUM(GL.DebitAmount - GL.CreditAmount)
					ELSE 0
			END AS DuNo ,
			CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
					THEN SUM(GL.CreditAmount - GL.DebitAmount)
					ELSE 0
			END AS DuCo,
			0 AS OrderType
		FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
		INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber 
		INNER JOIN @tblListAccountObjectID AS LAOI ON GL.AccountingObjectID = LAOI.AccountObjectID
		WHERE   ( GL.PostedDate < @FromDate )  
		and GL.CurrencyID = @currency                    
		GROUP BY 
			A.AccountNumber,LAOI.AccountObjectID,A.AccountGroupKind,LAOI.AccountObjectCode
		HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
	UNION ALL
	select  an.AccountNumber,
			An.AccountGroupKind,
			LAOI.AccountObjectID,
			LAOI.AccountObjectCode,
			a.PostedDate as NgayThangGS, 
			a.No as Sohieu, 
			a.Date as ngayCTu,
			a.Description as Diengiai, 
			a.AccountCorresponding as TKDoiUng, 
			DueDate as ThoiHanDuocCKhau, 
			sum(a.DebitAmount) as PSNO, 
			sum(a.CreditAmount) as PSCO,
			sum(fnsd.DebitAmount) DuNo,
			sum(fnsd.CreditAmount) DuCo,
			1 AS OrderType
	 from @tbDataGL a /*edit by cuongpv GeneralLedger -> @tbDataGL*/		
	LEFT JOIN @tbDataDueDate tbDuedate ON tbDuedate.IDDetail = a.DetailID
	inner join @tblAccountNumber as an on a.Account = an.AccountNumber
	INNER JOIN @tblListAccountObjectID AS LAOI ON a.AccountingObjectID = LAOI.AccountObjectID
	INNER JOIN Func_SoDu (
	   @fromdate
	  ,@todate
	  ,1) AS fnsd ON LAOI.AccountObjectID = fnsd.AccountObjectID and an.AccountNumber = fnsd.AccountNumber			
	where a.PostedDate between @fromdate and @todate
	and  a.CurrencyID = @currency  
	group by an.AccountNumber,
			LAOI.AccountObjectID,
			a.PostedDate,
			a.No , 
			a.Date ,
			a.Description, 
			a.AccountCorresponding, 
			DueDate,
			An.AccountGroupKind,
			LAOI.AccountObjectCode
	order by AccountNumber,NgayThangGS	
End
else
Begin
	/*add by cuongpv tao bang du lieu DueDate tu cac bang*/
	DECLARE @tbDataDueDate1 TABLE(
		ID UNIQUEIDENTIFIER,
		IDDetail UNIQUEIDENTIFIER,
		DueDate DATETIME
	)
	/*end add by cuongpv*/
	/*add by cuongpv lay du lieu DueDate tu cac bang SAInvoice, SAReturn*/
	INSERT INTO @tbDataDueDate1
	SELECT SAiv.ID, SAivdt.ID, SAiv.DueDate FROM SAInvoiceDetail SAivdt LEFT JOIN SAInvoice SAiv ON SAiv.ID = SAivdt.SAInvoiceID
	WHERE SAivdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)
	
	UNION ALL

	SELECT SArt.ID, SArtdt.ID, SArt.DueDate FROM SAReturnDetail SArtdt LEFT JOIN SAReturn SArt ON SArt.ID = SArtdt.SAReturnID
	WHERE SArtdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)
	/*end add by cuongpv*/
	insert into @tblResult
		SELECT  a.AccountNumber,
			A.AccountGroupKind,
			LAOI.AccountObjectID,
			LAOI.AccountObjectCode,
			null as NgayThangGS,
			null as Sohieu, 
			null as ngayCTu,
			N'Số dư đầu kỳ' as Diengiai ,
			null as TKDoiUng, 
			null as ThoiHanDuocCKhau, 
			$0 AS PSNO ,
			$0 AS PSCO ,
			CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
					THEN SUM(GL.DebitAmount - GL.CreditAmount)
					ELSE 0
			END AS DuNo ,
			CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
					THEN SUM(GL.CreditAmount - GL.DebitAmount)
					ELSE 0
			END AS DuCo,
			0 AS OrderType 
		FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
		INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber 
		INNER JOIN @tblListAccountObjectID AS LAOI ON GL.AccountingObjectID = LAOI.AccountObjectID
		WHERE   ( GL.PostedDate < @FromDate )  
		and GL.CurrencyID = @currency                    
		GROUP BY 
			A.AccountNumber,LAOI.AccountObjectID,A.AccountGroupKind,LAOI.AccountObjectCode
		HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
	UNION ALL
		select  an.AccountNumber,
				An.AccountGroupKind,
				LAOI.AccountObjectID,
				LAOI.AccountObjectCode,
				a.PostedDate as NgayThangGS, 
				a.No as Sohieu, 
				a.Date as ngayCTu,
				a.Description as Diengiai, 
				a.AccountCorresponding as TKDoiUng, 
				DueDate as ThoiHanDuocCKhau, 
				sum(a.DebitAmount) as PSNO, 
				sum(a.CreditAmount) as PSCO,
				sum(fnsd.DebitAmount) DuNo,
				sum(fnsd.CreditAmount) DuCo,
				1 AS OrderType
		 from @tbDataGL a	/*edit by cuongpv GeneralLedger -> @tbDataGL*/
		LEFT JOIN @tbDataDueDate1 tbDuedate ON tbDuedate.IDDetail = a.DetailID
		inner join @tblAccountNumber as an on a.Account = an.AccountNumber
		INNER JOIN @tblListAccountObjectID AS LAOI ON a.AccountingObjectID = LAOI.AccountObjectID
		INNER JOIN Func_SoDu (
		   @fromdate
		  ,@todate
		  ,1) AS fnsd ON LAOI.AccountObjectID = fnsd.AccountObjectID	and an.AccountNumber = fnsd.AccountNumber			
		where a.PostedDate between @fromdate and @todate
		and  a.CurrencyID = @currency  
		group by an.AccountNumber,
				LAOI.AccountObjectID,
				a.PostedDate,
				a.No , 
				a.Date ,
				a.Description, 
				a.AccountCorresponding, 
				DueDate,An.AccountGroupKind,LAOI.AccountObjectCode
		order by AccountNumber,AccountObjectID,NgayThangGS
End
DECLARE @CloseAmount AS DECIMAL(22, 8) ,
        @AccountObjectCode_tmp NVARCHAR(100) ,
        @AccountNumber_tmp NVARCHAR(20)
SELECT  @CloseAmount = 0 ,
        @AccountObjectCode_tmp = N'' ,
        @AccountNumber_tmp = N''
UPDATE  @tblResult
        SET     
                 @CloseAmount = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmount = 0 THEN ClosingCreditAmount
                                                                     ELSE -1 * ClosingDebitAmount
                                                                END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode or @AccountNumber_tmp <> AccountNumber THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountGroupKind = 0 THEN -1 * @CloseAmount
                                            WHEN AccountGroupKind = 1 THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0 THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountGroupKind = 1 THEN @CloseAmount
                                             WHEN AccountGroupKind = 0 THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0 THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
				@AccountNumber_tmp = AccountNumber							
select AccountNumber,
			AccountObjectID,
			RefDate as NgayThangGS, 
			RefNo as Sohieu, 
			PostedDate as ngayCTu,
			JournalMemo as Diengiai, 
			CorrespondingAccountNumber as TKDoiUng, 
			DueDate as ThoiHanDuocCKhau, 
			DebitAmount as PSNO, 
			CreditAmount as PSCO,
			ClosingDebitAmount as DuNo,
			ClosingCreditAmount as DuCo
from @tblResult 
order by RowNum
end				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_PO_SummaryPayable]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS
    BEGIN
	    
		/*Add by cuongpv de sua cach lam tron*/
	DECLARE @tbDataGL TABLE(
		ID uniqueidentifier,
		BranchID uniqueidentifier,
		ReferenceID uniqueidentifier,
		TypeID int,
		Date datetime,
		PostedDate datetime,
		No nvarchar(25),
		InvoiceDate datetime,
		InvoiceNo nvarchar(25),
		Account nvarchar(25),
		AccountCorresponding nvarchar(25),
		BankAccountDetailID uniqueidentifier,
		CurrencyID nvarchar(3),
		ExchangeRate decimal(25, 10),
		DebitAmount decimal(25,0),
		DebitAmountOriginal decimal(25,2),
		CreditAmount decimal(25,0),
		CreditAmountOriginal decimal(25,2),
		Reason nvarchar(512),
		Description nvarchar(512),
		VATDescription nvarchar(512),
		AccountingObjectID uniqueidentifier,
		EmployeeID uniqueidentifier,
		BudgetItemID uniqueidentifier,
		CostSetID uniqueidentifier,
		ContractID uniqueidentifier,
		StatisticsCodeID uniqueidentifier,
		InvoiceSeries nvarchar(25),
		ContactName nvarchar(512),
		DetailID uniqueidentifier,
		RefNo nvarchar(25),
		RefDate datetime,
		DepartmentID uniqueidentifier,
		ExpenseItemID uniqueidentifier,
		OrderPriority int,
		IsIrrationalCost bit
	)

	INSERT INTO @tbDataGL
	SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
	/*end add by cuongpv*/
		
		DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              AccountObjectAddress NVARCHAR(255) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        null AccountingObjectGroupCode ,
                        null AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
      
                 
		
		DECLARE @StartDateOfPeriod DATETIME
		SET @StartDateOfPeriod = '1/1/'+CAST(Year(@FromDate) AS NVARCHAR(5))
                         
           
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(255) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
	    
        SELECT  ROW_NUMBER() OVER ( ORDER BY AccountObjectCode ) AS RowNum ,
                AccountingObjectID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountObjectAddress ,
                AccountObjectTaxCode ,
                AccountNumber ,
                AccountCategoryKind ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC)
                            - SUM(OpenningCreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) ) > 0
                                 THEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN ( SUM(OpenningDebitAmount - OpenningCreditAmount) )
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmount
                                            - OpenningCreditAmount) ) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount)
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmountOC
                                  - OpenningDebitAmountOC) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) ) > 0
                                 THEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmount - OpenningDebitAmount) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) ) > 0
                                 THEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmount ,
                SUM(DebitAmountOC) AS DebitAmountOC ,
                SUM(DebitAmount) AS DebitAmount ,
                SUM(CreditAmountOC) AS CreditAmountOC ,
                SUM(CreditAmount) AS CreditAmount ,
                SUM(AccumDebitAmountOC) AS AccumDebitAmountOC ,
                SUM(AccumDebitAmount) AS AccumDebitAmount ,
                SUM(AccumCreditAmountOC) AS AccumCreditAmountOC ,
                SUM(AccumCreditAmount) AS AccumCreditAmount ,
                /* Số dư cuối kỳ = Dư Có đầu kỳ - Dư Nợ đầu kỳ + Phát sinh Có – Phát sinh Nợ
				Nếu Số dư cuối kỳ >0 thì hiển bên cột Dư Có cuối kỳ 
				Nếu số dư cuối kỳ <0 thì hiển thị bên cột Dư Nợ cuối kỳ */
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC
                                + DebitAmountOC - CreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC) > 0
                                 THEN $0
                                 ELSE SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC)
                            END
                  END ) AS CloseDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC
                                + CreditAmountOC - DebitAmountOC)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC
                                            + CreditAmountOC - DebitAmountOC) ) > 0
                                 THEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                                + DebitAmount - CreditAmount)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount) > 0 THEN $0
                                 ELSE SUM(OpenningDebitAmount
                                          - OpenningCreditAmount + DebitAmount
                                          - CreditAmount)
                            END
                  END ) AS ClosingDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                                + CreditAmount - DebitAmount)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount
                                            + CreditAmount - DebitAmount) ) > 0
                                 THEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)
                                 ELSE $0
                            END
                  END ) AS ClosingCreditAmount ,
                AccountObjectGroupListCode ,
                AccountObjectGroupListName
        FROM    ( SELECT    AOL.AccountingObjectID ,
                            LAOI.AccountObjectCode ,
                            LAOI.AccountObjectName ,
                            LAOI.AccountObjectAddress,
                            LAOI.AccountObjectTaxCode ,
                            TBAN.AccountNumber ,
                            TBAN.AccountCategoryKind ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmount
                                 ELSE $0
                            END AS OpenningDebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmount
                                 ELSE $0
                            END AS OpenningCreditAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmountOriginal
                            END AS DebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmount
                            END AS DebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmountOriginal
                            END AS CreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmount
                            END AS CreditAmount ,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmountOriginal ELSE 0 END AS AccumDebitAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmount ELSE 0 END AS AccumDebitAmount,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmountOriginal ELSE 0 END AS AccumCreditAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmount ELSE 0 END AS AccumCreditAmount,
                            LAOI.AccountObjectGroupListCode ,
                            LAOI.AccountObjectGroupListName
                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                  WHERE     AOL.PostedDate <= @ToDate
                            AND ( @CurrencyID IS NULL
                                  OR AOL.CurrencyID = @CurrencyID
                                )
                ) AS RSNS
        GROUP BY RSNS.AccountingObjectID ,
                RSNS.AccountObjectCode ,
                RSNS.AccountObjectName ,
                RSNS.AccountObjectAddress ,
                RSNS.AccountObjectTaxCode ,
                RSNS.AccountNumber ,
                RSNS.AccountCategoryKind ,
                RSNS.AccountObjectGroupListCode ,
                RSNS.AccountObjectGroupListName 
        HAVING 
				SUM(DebitAmountOC) <> 0
                OR SUM(DebitAmount) <> 0
                OR SUM(CreditAmountOC) <> 0
                OR SUM(CreditAmount) <> 0
                OR SUM(OpenningDebitAmount - OpenningCreditAmount) <> 0 
                OR SUM(OpenningDebitAmountOC - OpenningCreditAmountOC) <> 0 
				OR ( 
					(SUM(AccumDebitAmountOC) <> 0
					OR SUM(AccumDebitAmount) <> 0
					OR SUM(AccumCreditAmountOC) <> 0
					OR SUM(AccumCreditAmount) <> 0) )
        ORDER BY RSNS.AccountObjectCode
        OPTION (RECOMPILE)
		
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*
*/
ALTER PROCEDURE [dbo].[Proc_PO_PayDetailNCC]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS 
    BEGIN     
        DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              AccountObjectAddress NVARCHAR(255) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        null AccountingObjectGroupCode ,
                        null AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
		
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/
		      
        CREATE TABLE #tblResult
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100)  ,
              AccountObjectName NVARCHAR(255)  ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL ,             
              AccountObjectAddress NVARCHAR(255)  ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25)  ,
              DueDate DATETIME , 
              InvDate DATETIME ,
              InvNo NVARCHAR(25)  ,

              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              JournalMemo NVARCHAR(255)  , 
              AccountNumber NVARCHAR(20)  , 
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20)  , 
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,	
              DebitAmount MONEY , 
              CreditAmountOC MONEY , 
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY ,
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,	
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50)  ,
              InventoryItemName NVARCHAR(255)  ,
              UnitName NVARCHAR(20)  ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(18, 4) , 
              BranchName NVARCHAR(128)  ,
              IsBold BIT ,	
              OrderType INT ,
              GLJournalMemo NVARCHAR(255)  ,
              DocumentIncluded NVARCHAR(255)  ,
              CustomField1 NVARCHAR(255)  ,
              CustomField2 NVARCHAR(255)  ,
              CustomField3 NVARCHAR(255)  ,
              CustomField4 NVARCHAR(255)  ,
              CustomField5 NVARCHAR(255)  ,
              CustomField6 NVARCHAR(255)  ,
              CustomField7 NVARCHAR(255)  ,
              CustomField8 NVARCHAR(255)  ,
              CustomField9 NVARCHAR(255)  ,
              CustomField10 NVARCHAR(255) ,
               MainUnitName NVARCHAR(20)  ,
              MainQuantity DECIMAL(22, 8) ,
              MainUnitPrice DECIMAL(18, 4),
			  OrderPriority int  
              
            )
        
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(255) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
            
                
/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode , 
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,      
                          AccountObjectAddress , 
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID , 
                          JournalMemo , 				  
                          AccountNumber , 
                          AccountCategoryKind ,
                          CorrespondingAccountNumber ,	  			  
                          DebitAmountOC ,	
                          DebitAmount , 
                          CreditAmountOC ,
                          CreditAmount ,
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount ,
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount,
						  OrderType
						  /*OrderPriority - comment by cuongpv*/  
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,
                                AccountObjectName ,	
                                AccountObjectGroupListCode , 
                                AccountObjectGroupListName ,            
                                AccountObjectAddress , 
                                RSNS.PostedDate ,
                                NgayCtu ,
                                SoCtu ,
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RSNS.RefID , 
                                JournalMemo ,			  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  			  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) , 
                                SUM(CreditAmount) , 
                                ( CASE WHEN AccountCategoryKind = 0 THEN SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) ) > 0 THEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,      
                                ( CASE WHEN AccountCategoryKind = 0 THEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) ) > 0 THEN SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) ) > 0 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) ) > 0 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount,  
								  OrderType
								  /*OrderPriority - comment by cuongpv*/
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,
                                            AccountObjectGroupListCode ,
                                            AccountObjectGroupListName ,         
                                            LAOI.AccountObjectAddress ,
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.Date
                                            END AS NgayCtu ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.No
                                            END AS SoCtu ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType , 
                                            CASE WHEN AOL.PostedDate < @FromDate 
												      THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN N'Số dư đầu kỳ'
                                                 ELSE AOL.Description
                                            END AS JournalMemo , 
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                             
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                  
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount,
											CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType
											/*OrderPriority - comment by cuongpv*/
                                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                                         
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )
                                ) AS RSNS                               
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode ,  
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,
                                RSNS.AccountObjectAddress ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu , 
                                RSNS.SoCtu , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,
                                RSNS.JournalMemo , 		  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber,
								OrderType
								/*OrderPriority - comment by cuongpv*/	   
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount - ClosingCreditAmount) <> 0
                        ORDER BY 
						        RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu ,
                                RSNS.SoCtu
								/*OrderPriority - comment by cuongpv*/
                OPTION  ( RECOMPILE )                           
/* Tính số tồn */
        DECLARE @CloseAmountOC AS DECIMAL(22, 8) ,
            @CloseAmount AS DECIMAL(22, 8) ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmountOC = 0 THEN ClosingCreditAmountOC
                                                                       ELSE -1 * ClosingDebitAmountOC
                                                                  END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1 THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0 THEN -1 * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0 THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0 THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmount = 0 THEN ClosingCreditAmount
                                                                     ELSE -1 * ClosingDebitAmount
                                                                END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1 THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0 THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0 THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0 THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
        SELECT  RS.PostedDate as Ngay_HT, 
		        RS.RefDate as ngayCtu,
				RS.RefNo as SoCtu,
				Rs.JournalMemo as DienGiai,
				/*'331' as TK_CONGNO, comment by cuongpv*/
				Rs.CorrespondingAccountNumber as TkDoiUng,
                RS.AccountObjectCode,
				RS.AccountObjectName,
				RS.AccountNumber as TK_CONGNO, /*edit by cuongpv RS.AccountNumber -> RS.AccountNumber as TK_CONGNO*/
				fn.OpeningDebitAmount as OpeningDebitAmount,
				fn.OpeningCreditAmount as OpeningCreditAmount,
				Rs.DebitAmount as DebitAmount,
				Rs.DebitAmountOC as DebitAmountOC,
				Rs.CreditAmount as CreditAmount,
				Rs.CreditAmountOC as CreditAmountOC,
				Rs.ClosingDebitAmount as ClosingDebitAmount,
				Rs.ClosingDebitAmountOC as ClosingDebitAmountOC,
				Rs.ClosingCreditAmount as ClosingCreditAmount,
				Rs.ClosingCreditAmountOC as ClosingCreditAmountOC
        FROM    #tblResult RS ,
		        [dbo].[Func_SoDu] (
   @FromDate
  ,@ToDate
  ,3) fn
  where rs.AccountNumber = fn.AccountNumber
  and rs.AccountObjectID = fn.AccountObjectID
  order by RS.AccountObjectCode ,
                                RS.AccountNumber ,
                                RS.PostedDate ,
                                RS.RefDate ,
                                RS.RefNo,
								OrderPriority
        DROP TABLE #tblResult
    END				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER procedure [dbo].[Proc_PO_GetDetailBook]
@FromDate DATETIME ,
    @ToDate DATETIME,
    @AccountObjectID AS NVARCHAR(MAX),
	@MaterialGoods as NVARCHAR(MAX)
as
begin
    CREATE TABLE #tblListAccountObjectID  
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25)
                 ,
              AccountObjectName NVARCHAR(255)
                 
            ) 
    CREATE TABLE #tblListMaterialGoods  
            (
              MaterialGoodsID UNIQUEIDENTIFIER ,
              MaterialGoodsCode NVARCHAR(25)
                 ,
              MaterialGoodsName NVARCHAR(255)
                 ,
			  Unit NVARCHAR(25)  ,
			 Quantity decimal,
			 UnitPrice Money 
            ) 
        INSERT  INTO #tblListAccountObjectID
                SELECT  AO.ID ,
                        ao.AccountingObjectCode ,
                        ao.AccountingObjectName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                           ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value 
        INSERT  INTO #tblListMaterialGoods
                SELECT  MG.ID ,
                        MG.MaterialGoodsCode ,
                        MG.MaterialGoodsName,
						MG.Unit,
						MG.Quantity,
						MG.UnitPrice 
                FROM    dbo.Func_ConvertStringIntoTable(@MaterialGoods,
                                                           ',') tblAccountObjectSelected
                        INNER JOIN dbo.MaterialGoods MG ON MG.ID = tblAccountObjectSelected.Value 
	/*select * from #tblListMaterialGoods
	select * from #tblListAccountObjectID*/
	/*Tao bang tam luu du lieu ra bao cao*/
	DECLARE @tbDataResult TABLE(
		AccountObjectID UNIQUEIDENTIFIER,
		MaKH NVARCHAR(25),
		TenKH NVARCHAR(255),
		NgayHachToan DATE,
		NgayCTu DATE,
		SoCTu NVARCHAR(25),
		SoHoaDon NVARCHAR(25),
		NgayHoaDon DATE,
		MaterialGoodsID UNIQUEIDENTIFIER ,
        Mahang NVARCHAR(25),
		Tenhang NVARCHAR(255),
		DVT NVARCHAR(25),
		SoLuongMua DECIMAL(25,10),
		DonGia MONEY,
		GiaTriMua DECIMAL(25,0),
		ChietKhau DECIMAL(25,0),
		SoLuongTraLai DECIMAL(25,10),
		GiaTriTraLai DECIMAL(25,0),
		GiaTriGiamGia DECIMAL(25,0),
		TypeMG  NVARCHAR(1)
	)

	IF((@AccountObjectID is not null) AND (@MaterialGoods is not null))
	BEGIN
		/*lay gia tri PPInvoice vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, SoLuongMua, DonGia, GiaTriMua, ChietKhau, TypeMG)
		SELECT a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, b.InvoiceNo as SoHoaDon, b.InvoiceDate as NgayHoaDon, 
			   b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.Quantity as SoLuongMua, b.UnitPrice as DonGia, b.Amount as GiaTriMua, b.DiscountAmount as ChietKhau, 'I' as TypeMG   		 
		   FROM PPInvoiceDetail b LEFT JOIN PPInvoice a ON a.id = b.PPInvoiceID 
		   WHERE a.PostedDate between @FromDate and @ToDate
		   and a.Recorded = '1'
		   and ((a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID)) and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods)))


		/*Lay gia tri PPService vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, SoLuongMua, DonGia, GiaTriMua, ChietKhau, TypeMG)
		   SELECT  a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, b.InvoiceNo as SoHoaDon, b.InvoiceDate as NgayHoaDon, 
				   b.MaterialGoodsID as MaterialGoodsID, b.Quantity as SoLuongMua, b.UnitPrice as DonGia, b.Amount as GiaTriMua, b.DiscountAmount as ChietKhau, 'S' as TypeMG  		
		   FROM PPServiceDetail b LEFT JOIN PPService a ON a.ID = b.PPServiceID
		   WHERE a.PostedDate between @FromDate and @ToDate
		   and a.Recorded = '1' 
		   and ((a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID)) and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods)))

		/*Update DVT cua cac gia tri lay tu PPService*/
		UPDATE @tbDataResult SET DVT = MS.DVT
		FROM (
			SELECT ID as MSID, Unit as DVT FROM dbo.MaterialGoods
		) MS
		WHERE MaterialGoodsID = MS.MSID AND TypeMG = 'S'


		/*Lay gia tri Tra lai vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, DonGia, SoLuongTraLai, GiaTriTraLai, TypeMG)
		SELECT  a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, a.InvoiceNo as SoHoaDon, a.InvoiceDate as NgayHoaDon,
				  b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.UnitPrice as DonGia, b.Quantity as SoLuongTraLai, b.Amount as GiaTriTraLai, 'T' as TypeMG 		
			FROM PPDiscountReturnDetail b LEFT JOIN PPDiscountReturn a ON a.ID = b.PPDiscountReturnID
			WHERE a.PostedDate between @FromDate and @ToDate and a.Recorded = '1' and TypeID = '220'
			and ((a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID)) and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods)))



		/*lay gia tri giam gia vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, GiaTriGiamGia, TypeMG)
			select a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, InvoiceNo as SoHoaDon, InvoiceDate as NgayHoaDon, 
				   b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.Amount as GiaTriGiamGia, 'G' as TypeMG	   	
			from PPDiscountReturnDetail b LEFT JOIN PPDiscountReturn a ON a.ID = b.PPDiscountReturnID 
			where a.PostedDate between @FromDate and @ToDate and TypeID = '230'
			and ((a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID)) and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods)))
	END
	ELSE IF((@AccountObjectID is not null) AND (@MaterialGoods is null))
	BEGIN
		/*lay gia tri PPInvoice vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, SoLuongMua, DonGia, GiaTriMua, ChietKhau, TypeMG)
		SELECT a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, b.InvoiceNo as SoHoaDon, b.InvoiceDate as NgayHoaDon, 
			   b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.Quantity as SoLuongMua, b.UnitPrice as DonGia, b.Amount as GiaTriMua, b.DiscountAmount as ChietKhau, 'I' as TypeMG   		 
		   FROM PPInvoiceDetail b LEFT JOIN PPInvoice a ON a.id = b.PPInvoiceID 
		   WHERE a.PostedDate between @FromDate and @ToDate
		   and a.Recorded = '1'
		   and (a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID))


		/*Lay gia tri PPService vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, SoLuongMua, DonGia, GiaTriMua, ChietKhau, TypeMG)
		   SELECT  a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, b.InvoiceNo as SoHoaDon, b.InvoiceDate as NgayHoaDon, 
				   b.MaterialGoodsID as MaterialGoodsID, b.Quantity as SoLuongMua, b.UnitPrice as DonGia, b.Amount as GiaTriMua, b.DiscountAmount as ChietKhau, 'S' as TypeMG  		
		   FROM PPServiceDetail b LEFT JOIN PPService a ON a.ID = b.PPServiceID
		   WHERE a.PostedDate between @FromDate and @ToDate
		   and a.Recorded = '1' 
		   and (a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID))

		/*Update DVT cua cac gia tri lay tu PPService*/
		UPDATE @tbDataResult SET DVT = MS.DVT
		FROM (
			SELECT ID as MSID, Unit as DVT FROM dbo.MaterialGoods
		) MS
		WHERE MaterialGoodsID = MS.MSID AND TypeMG = 'S'


		/*Lay gia tri Tra lai vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, DonGia, SoLuongTraLai, GiaTriTraLai, TypeMG)
		SELECT  a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, a.InvoiceNo as SoHoaDon, a.InvoiceDate as NgayHoaDon,
				  b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.UnitPrice as DonGia, b.Quantity as SoLuongTraLai, b.Amount as GiaTriTraLai, 'T' as TypeMG 		
			FROM PPDiscountReturnDetail b LEFT JOIN PPDiscountReturn a ON a.ID = b.PPDiscountReturnID
			WHERE a.PostedDate between @FromDate and @ToDate and a.Recorded = '1' and TypeID = '220'
			and (a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID))



		/*lay gia tri giam gia vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, GiaTriGiamGia, TypeMG)
			select a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, InvoiceNo as SoHoaDon, InvoiceDate as NgayHoaDon, 
				   b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.Amount as GiaTriGiamGia, 'G' as TypeMG	   	
			from PPDiscountReturnDetail b LEFT JOIN PPDiscountReturn a ON a.ID = b.PPDiscountReturnID 
			where a.PostedDate between @FromDate and @ToDate and TypeID = '230'
			and (a.AccountingObjectID in (select AccountObjectID from #tblListAccountObjectID))
	END
	ELSE
	BEGIN
		/*lay gia tri PPInvoice vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, SoLuongMua, DonGia, GiaTriMua, ChietKhau, TypeMG)
		SELECT a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, b.InvoiceNo as SoHoaDon, b.InvoiceDate as NgayHoaDon, 
			   b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.Quantity as SoLuongMua, b.UnitPrice as DonGia, b.Amount as GiaTriMua, b.DiscountAmount as ChietKhau, 'I' as TypeMG   		 
		   FROM PPInvoiceDetail b LEFT JOIN PPInvoice a ON a.id = b.PPInvoiceID 
		   WHERE a.PostedDate between @FromDate and @ToDate
		   and a.Recorded = '1'
		   and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods))


		/*Lay gia tri PPService vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, SoLuongMua, DonGia, GiaTriMua, ChietKhau, TypeMG)
		   SELECT  a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, b.InvoiceNo as SoHoaDon, b.InvoiceDate as NgayHoaDon, 
				   b.MaterialGoodsID as MaterialGoodsID, b.Quantity as SoLuongMua, b.UnitPrice as DonGia, b.Amount as GiaTriMua, b.DiscountAmount as ChietKhau, 'S' as TypeMG  		
		   FROM PPServiceDetail b LEFT JOIN PPService a ON a.ID = b.PPServiceID
		   WHERE a.PostedDate between @FromDate and @ToDate
		   and a.Recorded = '1' 
		   and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods))

		/*Update DVT cua cac gia tri lay tu PPService*/
		UPDATE @tbDataResult SET DVT = MS.DVT
		FROM (
			SELECT ID as MSID, Unit as DVT FROM dbo.MaterialGoods
		) MS
		WHERE MaterialGoodsID = MS.MSID AND TypeMG = 'S'


		/*Lay gia tri Tra lai vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, DonGia, SoLuongTraLai, GiaTriTraLai, TypeMG)
		SELECT  a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, a.InvoiceNo as SoHoaDon, a.InvoiceDate as NgayHoaDon,
				  b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.UnitPrice as DonGia, b.Quantity as SoLuongTraLai, b.Amount as GiaTriTraLai, 'T' as TypeMG 		
			FROM PPDiscountReturnDetail b LEFT JOIN PPDiscountReturn a ON a.ID = b.PPDiscountReturnID
			WHERE a.PostedDate between @FromDate and @ToDate and a.Recorded = '1' and TypeID = '220'
			and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods))



		/*lay gia tri giam gia vao @tbDataResult*/
		INSERT INTO @tbDataResult(AccountObjectID, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, MaterialGoodsID, DVT, GiaTriGiamGia, TypeMG)
			select a.AccountingObjectID as AccountObjectID, a.Date as NgayHachToan, a.PostedDate as NgayCTu, a.No as SoCTu, InvoiceNo as SoHoaDon, InvoiceDate as NgayHoaDon, 
				   b.MaterialGoodsID as MaterialGoodsID, b.Unit as DVT, b.Amount as GiaTriGiamGia, 'G' as TypeMG	   	
			from PPDiscountReturnDetail b LEFT JOIN PPDiscountReturn a ON a.ID = b.PPDiscountReturnID 
			where a.PostedDate between @FromDate and @ToDate and TypeID = '230'
			and (b.MaterialGoodsID in (select MaterialGoodsID from #tblListMaterialGoods))
	END

	/*Update makh, tenkh, mahang, ten hang vao @tbDataResult*/
	UPDATE @tbDataResult SET MaKH = AO.MaKH, TenKH = AO.TenKH
	FROM (
		SELECT ID as AOID, AccountingObjectCode as MaKH, AccountingObjectName as TenKH FROM dbo.AccountingObject
	) AO
	WHERE AccountObjectID = AO.AOID

	UPDATE @tbDataResult SET Mahang = M.Mahang, Tenhang = M.Tenhang
	FROM (
		SELECT ID as MID, MaterialGoodsCode as Mahang, MaterialGoodsName as Tenhang FROM dbo.MaterialGoods
	) M
	WHERE MaterialGoodsID = M.MID

	/*lay du lieu ra bao cao*/
	SELECT MaKH, TenKH, NgayHachToan, NgayCTu, SoCTu, SoHoaDon, NgayHoaDon, Mahang, Tenhang, DVT, SoLuongMua, DonGia, GiaTriMua, ChietKhau, SoLuongTraLai, GiaTriTraLai, GiaTriGiamGia
	FROM @tbDataResult
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_GetGLBookDetailMoneyBorrow]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountID NVARCHAR(MAX) ,
    @AccountObjectID NVARCHAR(MAX)
AS 
    BEGIN
	
	/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        CREATE  TABLE #Result
            (
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255) ,
              AccountNumber NVARCHAR(25) ,
              AccountName NVARCHAR(255) ,
              CorrespondingAccountNumber NVARCHAR(25) ,
              DueDate DATETIME ,
              DebitAmount DECIMAL(22, 4) ,
              CreditAmount DECIMAL(22, 4) ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              OrderType INT ,
              IsBold BIT ,
              BranchName NVARCHAR(128) ,
              SortOrder INT ,
              DetailPostOrder INT
            )      

        DECLARE @tblAccountObject TABLE
            (
              AccountObjectID UNIQUEIDENTIFIER PRIMARY KEY,
			  AccountObjectCode NVARCHAR(25)
                 ,
              AccountObjectName NVARCHAR(255)
                
            )                       
        INSERT  INTO @tblAccountObject
                SELECT  f.value,A.AccountingObjectCode,A.AccountingObjectName
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                           ',') f
                 INNER JOIN dbo.AccountingObject A ON A.ID = F.Value 	
        CREATE TABLE #tblSelectedAccountNumber
            (
              AccountNumber NVARCHAR(20) 
                                         NOT NULL
                                         PRIMARY KEY ,
              AccountName NVARCHAR(255) 
                                        NULL
            )
        INSERT  INTO #tblSelectedAccountNumber
                SELECT DISTINCT  A.AccountNumber, A.AccountName
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountID, ',') F
                        INNER JOIN dbo.Account A ON A.AccountNumber like F.Value + '%'	/*edit by cuongpv (A.AccountNumber = F.Value) -> A.AccountNumber like F.Value + '%'*/		        
		/*select * from #tblSelectedAccountNumber*/
        BEGIN             
                
            INSERT  #Result
                    SELECT  RefID ,
                            PostedDate ,
                            RefDate ,
                            RefNo ,
                            RefType ,
                            JournalMemo ,
                            AccountNumber ,
                            AccountName ,
                            CorrespondingAccountNumber ,
                            DueDate ,
                            DebitAmount ,
                            CreditAmount ,
                            AccountObjectCode ,
                            AccountObjectName ,
                            OrderType ,
                            IsBold ,
                            BranchName ,
                            SortOrder ,
                            DetailPostOrder
                    FROM    ( SELECT    NULL AS RefID ,
                                        NULL AS PostedDate ,
                                        NULL AS RefDate ,
                                        NULL AS RefNo ,
                                        NULL AS RefType ,
                                        N'Số dư đầu kỳ' AS JournalMemo ,
                                        GL.Account AccountNumber,
                                        AC.AccountName ,
                                        NULL AS CorrespondingAccountNumber ,
                                        NULL DueDate ,
                                        CASE WHEN SUM(GL.DebitAmount
                                                      - GL.CreditAmount) > 0
                                             THEN SUM(GL.DebitAmount
                                                      - GL.CreditAmount)
                                             ELSE 0
                                        END AS DebitAmount ,
                                        CASE WHEN SUM(GL.CreditAmount
                                                      - GL.DebitAmount) > 0
                                             THEN SUM(GL.CreditAmount
                                                      - GL.DebitAmount)
                                             ELSE 0
                                        END AS CreditAmount ,
                                        AO.AccountObjectCode ,
                                        AO.AccountObjectName AS AccountObjectName ,
                                        0 AS OrderType ,
                                        1 AS isBold ,
                                        null  AS BranchName,
                                        0 AS sortOrder ,
                                        0 AS DetailPostOrder ,
                                        0 AS EntryType
                              FROM      @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                        inner JOIN #tblSelectedAccountNumber AC ON GL.Account = AC.AccountNumber /*edit by cuongpv like -> '='*/
                                                              /*+ '%' comment by cuongpv*/
                                        inner JOIN @tblAccountObject AO ON AO.AccountObjectID = GL.AccountingObjectID
                              WHERE     ( GL.PostedDate < @FromDate )
                              GROUP BY  GL.Account ,
                                        AC.AccountName ,
                                        AO.AccountObjectCode ,
                                        AO.AccountObjectName
                              HAVING    SUM(GL.DebitAmount - GL.CreditAmount) <> 0
                              UNION ALL
                              SELECT    GL.ReferenceID ,
                                        GL.PostedDate ,
                                        GL.RefDate ,
                                        GL.RefNo ,
                                        GL.TypeID ,
                                        CASE WHEN ( GL.Description IS NOT NULL
                                                    AND GL.Description <> ''
                                                  ) THEN GL.Description
                                             ELSE GL.Reason
                                        END AS JournalMemo ,
                                        GL.Account AccountNumber,
                                        AC.AccountName ,
                                        GL.AccountCorresponding,
                                        null as DueDate ,
                                        GL.DebitAmount ,
                                        GL.CreditAmount ,
                                        AO.AccountObjectCode ,
                                        AO.AccountObjectName AS AccountObjectName ,
                                        2 AS OrderType ,
                                        0 ,
                                        null AS BranchName,
                                        GL.OrderPriority,
                                        null,
                                        null
                              FROM      @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                                        inner JOIN #tblSelectedAccountNumber AC ON GL.Account = AC.AccountNumber /*edit by cuongpv like -> '='*/
                                                              /*+ '%' comment by cuongpv*/
                                        inner JOIN @tblAccountObject AO ON AO.AccountObjectID = GL.AccountingObjectID
                              WHERE     ( GL.PostedDate BETWEEN @FromDate AND @ToDate )
                                        AND ( GL.DebitAmount <> 0
                                              OR GL.CreditAmount <> 0
                                            )
                            ) T
                    ORDER BY AccountObjectName ,
                            AccountObjectCode ,
                            OrderType ,
                            postedDate ,
                            RefDate ,
                            RefNo ,
                            DetailPostOrder ,
                            SortOrder ,
                            EntryType                                                          
               
        END         
       
	   /*add by cuongpv insert total PS*/
	   INSERT  #Result
                ( JournalMemo ,
                  AccountNumber ,
                  AccountName ,
                  DebitAmount ,
                  CreditAmount ,
                  AccountObjectCode ,
                  AccountObjectName ,
                  OrderType ,
                  IsBold ,
                  BranchName
		        )                                                
                SELECT  N'Cộng số phát sinh' JournalMemo ,
                        AccountNumber ,
                        AccountName ,
                        SUM(DebitAmount) AS DebitAmount ,
                        SUM(CreditAmount) AS CreditAmount ,
                        AccountObjectCode ,
                        AccountObjectName ,
                        3 AS OrderType ,
                        1 ,
                        null as BranchName
                FROM    #Result
                WHERE   OrderType = 2
                GROUP BY AccountNumber ,
                        AccountName ,
                        AccountObjectCode ,
                        AccountObjectName 
	   /*end add by cuongpv*/     
       
      /*Insert lable*/
        INSERT  #Result
                ( JournalMemo ,
                  AccountNumber ,
                  AccountName ,
                  DebitAmount ,
                  CreditAmount ,
                  AccountObjectCode ,
                  AccountObjectName ,
                  OrderType ,
                  IsBold ,
                  BranchName
		        )                                                
                SELECT  N'Số dư cuối kỳ' JournalMemo ,
                        AccountNumber ,
                        AccountName ,
                        CASE WHEN SUM(DebitAmount - CreditAmount) > 0
                             THEN SUM(DebitAmount - CreditAmount)
                             ELSE 0
                        END AS DebitAmount ,
                        CASE WHEN SUM(CreditAmount - DebitAmount) > 0
                             THEN SUM(CreditAmount - DebitAmount)
                             ELSE 0
                        END AS CreditAmount ,
                        AccountObjectCode ,
                        AccountObjectName ,
                        4 AS OrderType ,
                        1 ,
                        null as BranchName
                FROM    #Result
                WHERE   OrderType = 2
                        OR OrderType = 0
                GROUP BY AccountNumber ,
                        AccountName ,
                        AccountObjectCode ,
                        AccountObjectName
                HAVING  ( SUM(DebitAmount) <> 0
                          OR SUM(CreditAmount) <> 0
                        )                        
	     /*IF EXISTS ( SELECT  * FROM    #Result ) comment by cuongpv*/
			SELECT  * ,
                ROW_NUMBER() OVER ( ORDER BY AccountNumber,AccountObjectName,AccountObjectCode,  OrderType , PostedDate , RefNo , RefDate, SortOrder, DetailPostOrder ) AS RowNum
			FROM    #Result  
        
        /*ELSE 
           BEGIN
                INSERT  #Result
                        ( JournalMemo ,
                          OrderType ,
                          IsBold ,
                          BranchName
		                )                        
                        SELECT  N'' JournalMemo ,
                                2 ,
                                1 ,
                                ''                                                                     
                SELECT  * ,
                        ROW_NUMBER() OVER ( ORDER BY AccountObjectName, AccountObjectCode , OrderType , PostedDate , RefDate, RefNo, SortOrder, DetailPostOrder ) AS RowNum
                FROM    #Result         
            END  comment by cuongpv*/                      
        DELETE  #Result
        DELETE  #tblSelectedAccountNumber        
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoTheoDoiThanhToanBangNgoaiTe]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @TypeMoney NVARCHAR(3),
    @Account NVARCHAR(25),
    @AccountObjectID AS NVARCHAR(MAX)
AS
BEGIN          
	DECLARE @sqlexc nvarchar(max)
	set @sqlexc = N''
	/*Add by cuongpv de sua cach lam tron*/
	select @sqlexc = @sqlexc + N' DECLARE @tbDataGL TABLE('
	select @sqlexc = @sqlexc + N' 	ID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	BranchID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	ReferenceID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	TypeID int,'
	select @sqlexc = @sqlexc + N' 	Date datetime,'
	select @sqlexc = @sqlexc + N' 	PostedDate datetime,'
	select @sqlexc = @sqlexc + N' 	No nvarchar(25),'
	select @sqlexc = @sqlexc + N' 	InvoiceDate datetime,'
	select @sqlexc = @sqlexc + N' 	InvoiceNo nvarchar(25),'
	select @sqlexc = @sqlexc + N' 	Account nvarchar(25),'
	select @sqlexc = @sqlexc + N' 	AccountCorresponding nvarchar(25),'
	select @sqlexc = @sqlexc + N' 	BankAccountDetailID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	CurrencyID nvarchar(3),'
	select @sqlexc = @sqlexc + N' 	ExchangeRate decimal(25, 10),'
	select @sqlexc = @sqlexc + N' 	DebitAmount decimal(25,0),'
	select @sqlexc = @sqlexc + N' 	DebitAmountOriginal decimal(25,2),'
	select @sqlexc = @sqlexc + N' 	CreditAmount decimal(25,0),'
	select @sqlexc = @sqlexc + N' 	CreditAmountOriginal decimal(25,2),'
	select @sqlexc = @sqlexc + N' 	Reason nvarchar(512),'
	select @sqlexc = @sqlexc + N' 	Description nvarchar(512),'
	select @sqlexc = @sqlexc + N' 	VATDescription nvarchar(512),'
	select @sqlexc = @sqlexc + N' 	AccountingObjectID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	EmployeeID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	BudgetItemID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	CostSetID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	ContractID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	StatisticsCodeID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	InvoiceSeries nvarchar(25),'
	select @sqlexc = @sqlexc + N' 	ContactName nvarchar(512),'
	select @sqlexc = @sqlexc + N' 	DetailID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	RefNo nvarchar(25),'
	select @sqlexc = @sqlexc + N' 	RefDate datetime,'
	select @sqlexc = @sqlexc + N' 	DepartmentID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	ExpenseItemID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	OrderPriority int,'
	select @sqlexc = @sqlexc + N' 	IsIrrationalCost bit'
	select @sqlexc = @sqlexc + N' )'

	select @sqlexc = @sqlexc + N' INSERT INTO @tbDataGL'
	select @sqlexc = @sqlexc + N' SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= '''+CONVERT(nvarchar(25),@ToDate,101)+''''
		/*end add by cuongpv*/
	set @sqlexc = @sqlexc + N' DECLARE @tbAccountObjectID TABLE('
	set @sqlexc = @sqlexc + N'	AccountingObjectID UNIQUEIDENTIFIER'
	set @sqlexc = @sqlexc + N')'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbAccountObjectID'
	set @sqlexc = @sqlexc + N' SELECT tblAccOjectSelect.Value as AccountingObjectID'
	set @sqlexc = @sqlexc + N'	FROM dbo.Func_ConvertStringIntoTable('''+@AccountObjectID+''','','') tblAccOjectSelect'
	set @sqlexc = @sqlexc + N'	WHERE tblAccOjectSelect.Value in (SELECT ID FROM AccountingObject)'
	
	select @sqlexc = @sqlexc + N' DECLARE @tbDataDauKy TABLE('
	select @sqlexc = @sqlexc + N' AccountingObjectID UNIQUEIDENTIFIER,'
	select @sqlexc = @sqlexc + N' Account nvarchar(25),'
	select @sqlexc = @sqlexc + N' SoDuDauKySoTien money,'
	select @sqlexc = @sqlexc + N' SoDuDauKyQuyDoi money'
	select @sqlexc = @sqlexc + N')'
	
	select @sqlexc = @sqlexc + N' INSERT INTO @tbDataDauKy(AccountingObjectID, Account, SoDuDauKySoTien, SoDuDauKyQuyDoi)'
	select @sqlexc = @sqlexc + N' SELECT GL.AccountingObjectID, GL.Account, SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) as SoDuDauKySoTien,'
	select @sqlexc = @sqlexc + N' SUM(GL.DebitAmount - GL.CreditAmount) as SoDuDauKyQuyDoi'
	select @sqlexc = @sqlexc + N' FROM @tbDataGL GL'
	select @sqlexc = @sqlexc + N' WHERE (GL.AccountingObjectID in (Select AccountingObjectID from @tbAccountObjectID))'
	select @sqlexc = @sqlexc + N' AND GL.PostedDate < '''+CONVERT(nvarchar(25),@FromDate,101)+''' AND (GL.CurrencyID = '''+@TypeMoney+''')'
	
	if(@Account='all')
	begin
		set @sqlexc = @sqlexc + N' AND ((GL.Account like ''131%'') OR (GL.Account like ''141%'') OR (GL.Account like ''331%''))'
	end
	else
	begin
		set @sqlexc = @sqlexc + N' AND (GL.Account like '''+@Account+'%'')'
	end
	
	select @sqlexc = @sqlexc + N' GROUP BY GL.AccountingObjectID, GL.Account'
	
	
	set @sqlexc = @sqlexc + N' DECLARE @tbDataReturn TABLE('
	set @sqlexc = @sqlexc + N'	stt bigint,'
	set @sqlexc = @sqlexc + N'	IdGroup smallint,'
	set @sqlexc = @sqlexc + N'	AccountingObjectID UNIQUEIDENTIFIER,'
	set @sqlexc = @sqlexc + N'	AccountingObjectName nvarchar(512),'
	set @sqlexc = @sqlexc + N'	Account nvarchar(25),'
	set @sqlexc = @sqlexc + N'	NgayHoachToan Date,'
	set @sqlexc = @sqlexc + N'	NgayChungTu Date,'
	set @sqlexc = @sqlexc + N'	SoChungTu nvarchar(25),'
	set @sqlexc = @sqlexc + N'	DienGiai nvarchar(512),'
	set @sqlexc = @sqlexc + N'	TKDoiUng nvarchar(512),'
	set @sqlexc = @sqlexc + N'	TyGiaHoiDoai decimal(25,10),'
	set @sqlexc = @sqlexc + N'	PSNSoTien money,'
	set @sqlexc = @sqlexc + N'	PSNQuyDoi money,'
	set @sqlexc = @sqlexc + N'	PSCSoTien money,'
	set @sqlexc = @sqlexc + N'	PSCQuyDoi money,'
	set @sqlexc = @sqlexc + N'	DuNoSoTien money,'
	set @sqlexc = @sqlexc + N'	DuNoQuyDoi money,'
	set @sqlexc = @sqlexc + N'	DuCoSoTien money,'
	set @sqlexc = @sqlexc + N'	DuCoQuyDoi money,'
	set @sqlexc = @sqlexc + N'	TonSoTien money,'
	set @sqlexc = @sqlexc + N'	TonQuyDoi money'
	set @sqlexc = @sqlexc + N')'
	
	set @sqlexc = @sqlexc + N' DECLARE @IdGroup1 smallint'
	set @sqlexc = @sqlexc + N' set @IdGroup1 = 5;'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbDataReturn(stt,IdGroup, AccountingObjectID, Account, NgayHoachToan, NgayChungTu, SoChungTu, DienGiai, TKDoiUng, TyGiaHoiDoai,'
	set @sqlexc = @sqlexc + N' PSNSoTien, PSNQuyDoi, PSCSoTien, PSCQuyDoi)'
	set @sqlexc = @sqlexc + N' SELECT ROW_NUMBER() OVER(ORDER BY GL.AccountingObjectID, GL.Account, GL.PostedDate) as stt, @IdGroup1 as IdGroup, GL.AccountingObjectID, Gl.Account, GL.PostedDate as NgayHoachToan, GL.Date as NgayChungTu, GL.No as SoChungTu,'
	set @sqlexc = @sqlexc + N' GL.Description as DienGiai, GL.AccountCorresponding as TKDoiUng, GL.ExchangeRate as TyGiaHoiDoai, GL.DebitAmountOriginal as PSNSoTien,'
	set @sqlexc = @sqlexc + N' GL.DebitAmount as PSNQuyDoi, Gl.CreditAmountOriginal as PSCSoTien, GL.CreditAmount as PSCQuyDoi'
	set @sqlexc = @sqlexc + N' FROM @tbDataGL GL'
	set @sqlexc = @sqlexc + N' WHERE (GL.PostedDate Between '''+CONVERT(nvarchar(25),@FromDate,101)+''' And '''+CONVERT(nvarchar(25),@ToDate,101)+''') AND (GL.CurrencyID = '''+@TypeMoney+''')'
	set @sqlexc = @sqlexc + N' AND (GL.AccountingObjectID in (Select AccountingObjectID from @tbAccountObjectID))'
	
	if(@Account='all')
	begin
		set @sqlexc = @sqlexc + N' AND ((GL.Account like ''131%'') OR (GL.Account like ''141%'') OR (GL.Account like ''331%''))'
	end
	else
	begin
		set @sqlexc = @sqlexc + N' AND (GL.Account like '''+@Account+'%'')'
	end
	
	set @sqlexc = @sqlexc + N' DECLARE @stt1 smallint'
	set @sqlexc = @sqlexc + N' set @stt1 = 0;'
	
	
	set @sqlexc = @sqlexc + N' Declare @SoDuDauKySoTien money'
	set @sqlexc = @sqlexc + N' Set @SoDuDauKySoTien = 0'
	set @sqlexc = @sqlexc + N' Declare @SoDuDauKyQuyDoi money'
	set @sqlexc = @sqlexc + N' Set @SoDuDauKyQuyDoi = 0'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbDataReturn(stt,IdGroup, AccountingObjectID, Account, DienGiai, TonSoTien, TonQuyDoi)'
	set @sqlexc = @sqlexc + N' SELECT @stt1, @IdGroup1, AccountingObjectID, Account, N''Số dư đầu kỳ'''
	set @sqlexc = @sqlexc + N' ,@SoDuDauKySoTien, @SoDuDauKyQuyDoi'
	set @sqlexc = @sqlexc + N' FROM @tbDataReturn'
	set @sqlexc = @sqlexc + N' GROUP BY AccountingObjectID, Account'
	
	set @sqlexc = @sqlexc + N' DECLARE @iIDDauKy UNIQUEIDENTIFIER'
	set @sqlexc = @sqlexc + N' DECLARE @iAccountDauKy NVARCHAR(25)'
	set @sqlexc = @sqlexc + N' DECLARE @iTonSoTienDauKy money'
	set @sqlexc = @sqlexc + N' DECLARE @iTonQuyDoiDauKy money'
	
	set @sqlexc = @sqlexc + N' DECLARE cursorDauKy CURSOR FOR'
	set @sqlexc = @sqlexc + N' SELECT AccountingObjectID, Account, SoDuDauKySoTien, SoDuDauKyQuyDoi FROM @tbDataDauKy'
	set @sqlexc = @sqlexc + N' OPEN cursorDauKy'
	set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorDauKy INTO @iIDDauKy, @iAccountDauKy, @iTonSoTienDauKy, @iTonQuyDoiDauKy'
	set @sqlexc = @sqlexc + N' WHILE @@FETCH_STATUS = 0'
	set @sqlexc = @sqlexc + N' BEGIN'
		set @sqlexc = @sqlexc + N' Declare @countCheck int'
		set @sqlexc = @sqlexc + N' set @countCheck = (select count(AccountingObjectID) from @tbDataReturn where AccountingObjectID = @iIDDauKy and Account = @iAccountDauKy)'
		
		set @sqlexc = @sqlexc + N' If(@countCheck > 0)'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' Update @tbDataReturn Set TonSoTien = @iTonSoTienDauKy, TonQuyDoi = @iTonQuyDoiDauKy' 
			set @sqlexc = @sqlexc + N' where AccountingObjectID = @iIDDauKy and Account = @iAccountDauKy'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' Else'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' Insert Into @tbDataReturn(stt,IdGroup, AccountingObjectID, Account, DienGiai, TonSoTien, TonQuyDoi)'
			set @sqlexc = @sqlexc + N' Values(@stt1, @IdGroup1, @iIDDauKy, @iAccountDauKy, N''Số dư đầu kỳ'', @iTonSoTienDauKy, @iTonQuyDoiDauKy)'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorDauKy INTO @iIDDauKy, @iAccountDauKy, @iTonSoTienDauKy, @iTonQuyDoiDauKy'
	set @sqlexc = @sqlexc + N' END'
	set @sqlexc = @sqlexc + N' CLOSE cursorDauKy'
	set @sqlexc = @sqlexc + N' DEALLOCATE cursorDauKy'
	
	
	set @sqlexc = @sqlexc + N' DECLARE @iID UNIQUEIDENTIFIER'
	set @sqlexc = @sqlexc + N' DECLARE @iAccount NVARCHAR(25)'
	set @sqlexc = @sqlexc + N' DECLARE @iTonSoTien money'
	set @sqlexc = @sqlexc + N' DECLARE @iTonQuyDoi money'
	
	set @sqlexc = @sqlexc + N' DECLARE cursorTon CURSOR FOR'
	set @sqlexc = @sqlexc + N' SELECT AccountingObjectID, Account, TonSoTien, TonQuyDoi FROM @tbDataReturn WHERE stt = 0'
	set @sqlexc = @sqlexc + N' OPEN cursorTon'
	set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorTon INTO @iID, @iAccount, @iTonSoTien, @iTonQuyDoi'
	set @sqlexc = @sqlexc + N' WHILE @@FETCH_STATUS = 0'
	set @sqlexc = @sqlexc + N' BEGIN'
		set @sqlexc = @sqlexc + N' Declare @tonSoTien money'
		set @sqlexc = @sqlexc + N' set @tonSoTien = ISNULL(@iTonSoTien,0)'
		set @sqlexc = @sqlexc + N' Declare @tonQuyDoi money'
		set @sqlexc = @sqlexc + N' set @tonQuyDoi = ISNULL(@iTonQuyDoi,0)'
		
		set @sqlexc = @sqlexc + N' Declare @minStt bigint'
		set @sqlexc = @sqlexc + N' set @minStt = (select MIN(stt) from @tbDataReturn where AccountingObjectID = @iID and Account = @iAccount and stt > 0)'
		set @sqlexc = @sqlexc + N' Declare @maxStt bigint'
		set @sqlexc = @sqlexc + N' set @maxStt = (select MAX(stt) from @tbDataReturn where AccountingObjectID = @iID and Account = @iAccount and stt > 0)'
		set @sqlexc = @sqlexc + N' WHILE @minStt <= @maxStt'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' set @tonSoTien = @tonSoTien + ISNULL((select PSNSoTien from @tbDataReturn where stt = @minStt),0) - ISNULL((select PSCSoTien from @tbDataReturn where stt = @minStt),0)'
			set @sqlexc = @sqlexc + N' set @tonQuyDoi = @tonQuyDoi + ISNULL((select PSNQuyDoi from @tbDataReturn where stt = @minStt),0) - ISNULL((select PSCQuyDoi from @tbDataReturn where stt = @minStt),0)'
			set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET TonSoTien = @tonSoTien, TonQuyDoi = @tonQuyDoi WHERE stt = @minStt'
			set @sqlexc = @sqlexc + N' set @minStt = @minStt + 1'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorTon INTO @iID, @iAccount, @iTonSoTien, @iTonQuyDoi'
	set @sqlexc = @sqlexc + N' END'
	set @sqlexc = @sqlexc + N' CLOSE cursorTon'
	set @sqlexc = @sqlexc + N' DEALLOCATE cursorTon'
	
	set @sqlexc = @sqlexc + N' DECLARE @IdGroup3 smallint'
	set @sqlexc = @sqlexc + N' SET @IdGroup3 = 3'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbDataReturn(IdGroup, AccountingObjectID, Account, PSNSoTien, PSNQuyDoi, PSCSoTien, PSCQuyDoi)'
	set @sqlexc = @sqlexc + N' SELECT @IdGroup3 as IdGroup, AccountingObjectID, Account, SUM(PSNSoTien) as PSNSoTien, SUM(PSNQuyDoi) as PSNQuyDoi'
	set @sqlexc = @sqlexc + N' , SUM(PSCSoTien) as PSCSoTien, SUM(PSCQuyDoi) as PSCQuyDoi'
	set @sqlexc = @sqlexc + N' FROM @tbDataReturn WHERE IdGroup = 5'
	set @sqlexc = @sqlexc + N' GROUP BY AccountingObjectID, Account'
	
	set @sqlexc = @sqlexc + N' DECLARE @iID1 UNIQUEIDENTIFIER'
	set @sqlexc = @sqlexc + N' DECLARE @iAccount1 NVARCHAR(25)'
	
	set @sqlexc = @sqlexc + N' DECLARE cursorTon1 CURSOR FOR'
	set @sqlexc = @sqlexc + N' SELECT AccountingObjectID, Account FROM @tbDataReturn WHERE stt = 0'
	set @sqlexc = @sqlexc + N' OPEN cursorTon1'
	set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorTon1 INTO @iID1, @iAccount1'
	set @sqlexc = @sqlexc + N' WHILE @@FETCH_STATUS = 0'
	set @sqlexc = @sqlexc + N' BEGIN'
		set @sqlexc = @sqlexc + N' Declare @maxStt1 bigint'
		set @sqlexc = @sqlexc + N' set @maxStt1 = (select MAX(stt) from @tbDataReturn where AccountingObjectID = @iID1 and Account = @iAccount1 and stt > 0)'
		set @sqlexc = @sqlexc + N' If(@maxStt1 > 0)'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET TonSoTien = (select TonSoTien from @tbDataReturn where stt = @maxStt1)'
			set @sqlexc = @sqlexc + N' ,TonQuyDoi = (select TonQuyDoi from @tbDataReturn where stt = @maxStt1) WHERE AccountingObjectID = @iID1'
			set @sqlexc = @sqlexc + N' AND Account = @iAccount1 AND IdGroup = 3'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' Else'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET TonSoTien = (select TonSoTien from @tbDataReturn where stt = 0 and AccountingObjectID = @iID1 and Account = @iAccount1)'
			set @sqlexc = @sqlexc + N' ,TonQuyDoi = (select TonQuyDoi from @tbDataReturn where stt = 0 and AccountingObjectID = @iID1 and Account = @iAccount1) WHERE AccountingObjectID = @iID1'
			set @sqlexc = @sqlexc + N' AND Account = @iAccount1 AND IdGroup = 3'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorTon1 INTO @iID1, @iAccount1'
	set @sqlexc = @sqlexc + N' END'
	set @sqlexc = @sqlexc + N' CLOSE cursorTon1'
	set @sqlexc = @sqlexc + N' DEALLOCATE cursorTon1'
	
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuNoSoTien = TonSoTien WHERE TonSoTien > 0'
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuCoSoTien = TonSoTien WHERE TonSoTien < 0'
	
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuNoQuyDoi = TonQuyDoi WHERE TonQuyDoi > 0'
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuCoQuyDoi = TonQuyDoi WHERE TonQuyDoi < 0'
	
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuCoSoTien = (-1 * DuCoSoTien) WHERE DuCoSoTien < 0'
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuCoQuyDoi = (-1 * DuCoQuyDoi) WHERE DuCoQuyDoi < 0'
	
	set @sqlexc = @sqlexc + N' DECLARE @IdGroup2 smallint'
	set @sqlexc = @sqlexc + N' SET @IdGroup2 = 1'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbDataReturn(IdGroup, AccountingObjectID, PSNSoTien, PSNQuyDoi, PSCSoTien, PSCQuyDoi'
	set @sqlexc = @sqlexc + N' , DuNoSoTien, DuNoQuyDoi, DuCoSoTien, DuCoQuyDoi)'
	set @sqlexc = @sqlexc + N' SELECT @IdGroup2 as IdGroup, AccountingObjectID, SUM(PSNSoTien) as PSNSoTien, SUM(PSNQuyDoi) as PSNQuyDoi'
	set @sqlexc = @sqlexc + N' , SUM(PSCSoTien) as PSCSoTien, SUM(PSCQuyDoi) as PSCQuyDoi, SUM(DuNoSoTien) as DuNoSoTien'
	set @sqlexc = @sqlexc + N' , SUM(DuNoQuyDoi) as DuNoQuyDoi, SUM(DuCoSoTien) as DuCoSoTien, SUM(DuCoQuyDoi) as DuCoQuyDoi'
	set @sqlexc = @sqlexc + N' FROM @tbDataReturn WHERE IdGroup = 3'
	set @sqlexc = @sqlexc + N' GROUP BY AccountingObjectID'
	
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET AccountingObjectName = AJ.AccountingObjectName'
	set @sqlexc = @sqlexc + N' FROM ( select ID,AccountingObjectName from AccountingObject) AJ'
	set @sqlexc = @sqlexc + N' where AccountingObjectID = AJ.ID'
	
	set @sqlexc = @sqlexc + N' SELECT stt,IdGroup, AccountingObjectID, AccountingObjectName, Account, NgayHoachToan, NgayChungTu, SoChungTu, DienGiai, TKDoiUng, TyGiaHoiDoai'
	set @sqlexc = @sqlexc + N',PSNSoTien, PSNQuyDoi, PSCSoTien, PSCQuyDoi, DuNoSoTien, DuNoQuyDoi, DuCoSoTien, DuCoQuyDoi'
	set @sqlexc = @sqlexc + N' FROM @tbDataReturn'
	set @sqlexc = @sqlexc + N' WHERE (ISNULL(PSNSoTien,0) > 0) OR (ISNULL(PSNQuyDoi,0) > 0) OR (ISNULL(PSCSoTien,0) > 0) OR (ISNULL(PSCQuyDoi,0) > 0)'
	set @sqlexc = @sqlexc + N' OR (ISNULL(DuNoSoTien,0) > 0) OR (ISNULL(DuNoQuyDoi,0) > 0) OR (ISNULL(DuCoSoTien,0) > 0) OR (ISNULL(DuCoQuyDoi,0) > 0)'
	set @sqlexc = @sqlexc + N' ORDER BY AccountingObjectID, Account, IdGroup, stt'
	
	
	EXECUTE sp_executesql @sqlexc
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Type
  DROP CONSTRAINT FK_Type_TypeGroup
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Template
  DROP CONSTRAINT FK_Template_Type
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
IF (OBJECT_ID('FK__TT153Adju__TypeI__25869641', 'F') IS NOT NULL)
BEGIN
ALTER TABLE dbo.TT153AdjustAnnouncement
  DROP CONSTRAINT FK__TT153Adju__TypeI__25869641
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
IF (OBJECT_ID('FK__TT153Dele__TypeI__119F9925', 'F') IS NOT NULL)
BEGIN
ALTER TABLE dbo.TT153DeletedInvoice
  DROP CONSTRAINT FK__TT153Dele__TypeI__119F9925
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
IF (OBJECT_ID('FK__TT153Dest__TypeI__22AA2996', 'F') IS NOT NULL)
BEGIN
ALTER TABLE dbo.TT153DestructionInvoice
  DROP CONSTRAINT FK__TT153Dest__TypeI__22AA2996
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
IF (OBJECT_ID('FK__TT153Lost__TypeI__1FCDBCEB', 'F') IS NOT NULL)
BEGIN
ALTER TABLE dbo.TT153LostInvoice
  DROP CONSTRAINT FK__TT153Lost__TypeI__1FCDBCEB
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
IF (OBJECT_ID('FK__TT153Publ__TypeI__1CF15040', 'F') IS NOT NULL)
BEGIN
ALTER TABLE dbo.TT153PublishInvoice
  DROP CONSTRAINT FK__TT153Publ__TypeI__1CF15040
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
IF (OBJECT_ID('FK__TT153Regi__TypeI__1A14E395', 'F') IS NOT NULL)
BEGIN
ALTER TABLE dbo.TT153RegisterInvoice
  DROP CONSTRAINT FK__TT153Regi__TypeI__1A14E395
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.AccountDefault SET FilterAccount = N'112;515' WHERE ID = '42264cb6-b102-4598-a864-1ab819f4f5c6'
UPDATE dbo.AccountDefault SET FilterAccount = N'152;153;156;611;6421;6422' WHERE ID = 'e8960a3a-ad55-496e-a7fb-21ac6ed66ff0'
UPDATE dbo.AccountDefault SET FilterAccount = N'3333' WHERE ID = '83460432-8c8a-489e-a3ad-21f3de488a1b'
UPDATE dbo.AccountDefault SET FilterAccount = N'3333' WHERE ID = '01eada3f-b70f-4229-a2c2-2231aeaf3936'
UPDATE dbo.AccountDefault SET DefaultAccount = N'33311' WHERE ID = 'a80e0cfd-d3d8-409d-be9f-26e7f2a5fe6d'
UPDATE dbo.AccountDefault SET FilterAccount = N'3333' WHERE ID = 'b37baafc-1f77-479f-b8df-29533cbcfb4e'
UPDATE dbo.AccountDefault SET FilterAccount = N'112;515' WHERE ID = '5d7cb490-96fb-4aac-832b-2fd091ed727c'
UPDATE dbo.AccountDefault SET FilterAccount = N'3333' WHERE ID = '3b493c1d-b9e8-4506-bf3f-4b38c577fa1b'
UPDATE dbo.AccountDefault SET FilterAccount = N'112;515' WHERE ID = '97d62fb0-b345-4f30-a54e-4d676500e1aa'
UPDATE dbo.AccountDefault SET DefaultAccount = N'33311' WHERE ID = 'c1520146-4156-4415-a66b-547e1f5059f4'
UPDATE dbo.AccountDefault SET DefaultAccount = N'3332' WHERE ID = 'a572dc43-e431-41d5-9472-5845e0e3c2c2'
UPDATE dbo.AccountDefault SET FilterAccount = N'3333' WHERE ID = 'd374da59-7b58-4a90-93de-5cb102319869'
UPDATE dbo.AccountDefault SET FilterAccount = N'112;515' WHERE ID = '259cb47b-f744-4531-9749-6d6d6e394501'
UPDATE dbo.AccountDefault SET FilterAccount = N'3333' WHERE ID = 'bb477284-47fd-4455-8168-7f90ccb40bfb'
UPDATE dbo.AccountDefault SET DefaultAccount = N'33311' WHERE ID = 'befc693c-db1d-4300-92b9-8607d3664ba9'
UPDATE dbo.AccountDefault SET DefaultAccount = N'33311' WHERE ID = '1ceb8724-40c9-4a16-9091-88d1621484d0'
UPDATE dbo.AccountDefault SET DefaultAccount = N'33311' WHERE ID = '91556800-9ba9-4d5c-b0e5-b7eeeca7d824'
UPDATE dbo.AccountDefault SET FilterAccount = N'152;153;156;611;6421;6422' WHERE ID = '61eef300-9d87-4e5b-b1d9-c7d493d58a56'
UPDATE dbo.AccountDefault SET FilterAccount = N'111;515' WHERE ID = '82855550-4169-4ce0-adbf-d7f451aa472f'
UPDATE dbo.AccountDefault SET DefaultAccount = N'1331' WHERE ID = '2d6eb113-0131-4e5e-a238-ecad55b24792'
UPDATE dbo.AccountDefault SET DefaultAccount = N'33311' WHERE ID = '6b8f1136-01b7-4203-8ee8-f1598fbb8a7b'
UPDATE dbo.AccountDefault SET DefaultAccount = N'1331' WHERE ID = '7e06903b-da7b-4232-97ac-f2010f7e0a6a'
UPDATE dbo.AccountDefault SET DefaultAccount = N'1331' WHERE ID = '3fa88405-e232-44aa-940d-f823858376f8'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.TemplateColumn WHERE ID = 'c47b45d1-ddcb-4338-aa70-212c49d11c17'
DELETE dbo.TemplateColumn WHERE ID = 'a9d85f68-a19e-4306-87bf-d39fc7048e28'
DELETE dbo.TemplateColumn WHERE ID = '836b9715-caca-438f-ad81-e85970221b17'

UPDATE dbo.TemplateColumn SET VisiblePosition = 39 WHERE ID = '75006b84-c909-47c1-b416-01fd3230ec2b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 15 WHERE ID = '28cef54e-9b78-4514-8640-03f76f994d00'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = '01a6e0cf-3259-4d0d-b3cf-04a27badbaa9'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'a509b1d4-f9a9-42fd-8056-09ffc8cb8013'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = '440707b0-1c73-4935-ab3d-0c8ee34914b2'
UPDATE dbo.TemplateColumn SET VisiblePosition = 30 WHERE ID = 'c4b886af-d060-4b71-8e20-0d7d075e0a56'
UPDATE dbo.TemplateColumn SET VisiblePosition = 24 WHERE ID = '95e74b5c-30e7-4b66-9bce-0dce314cfed9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = '936e4583-76bb-4865-aab1-0f6c11cfa60a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 34 WHERE ID = '00ad7906-bf14-4881-8906-0fdf737bcf95'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = '5a243233-7d64-4876-bf30-10f68215fcc1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = 'e6918663-c699-4c2c-8de0-119e481dd7d0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = '833ee49a-f5aa-49f7-a63a-12263f5bb7df'
UPDATE dbo.TemplateColumn SET VisiblePosition = 38 WHERE ID = '834ee49b-f5aa-49f6-b63a-12263f5bb7df'
UPDATE dbo.TemplateColumn SET VisiblePosition = 28 WHERE ID = 'c7b2457d-c590-452a-acae-138dc761d8ad'
UPDATE dbo.TemplateColumn SET VisiblePosition = 26 WHERE ID = 'a8b12b1d-b6d7-4d75-8486-17d3f0d8b8a2'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = 'c8af075d-c5f6-4deb-8fd0-1a8ff09c5258'
UPDATE dbo.TemplateColumn SET VisiblePosition = 33 WHERE ID = 'fb66a7d8-402f-443b-ad0f-1ac3f248ea2e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = '6bf102e7-2a58-4435-a568-1b86598e3239'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '78838a29-b68a-4b10-941f-1c8a7d15a73e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 19 WHERE ID = '7d6588c0-906a-4020-b8ff-1ca508aeef5a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 25 WHERE ID = 'c4e9dd12-42e0-4bf6-a81f-1d4b88bd2973'
UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = '418319ef-0c07-482f-b85f-1d671a9c695f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = '5cc333ad-0316-475c-b556-1e5ff31e897d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = '403ef675-3b6e-48a7-a97a-215180d1bcb6'
UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = 'e696f310-1eca-4b03-888b-24448fa2604d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = 'e1702dc2-0953-48a9-8989-2597b921a503'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = '90d4d7e5-4a8d-4603-8363-27a3911d0503'
UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = '8fa80e48-6f9f-47dd-907d-2997f0ecdb11'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = 'e1ce8edc-7e92-4cd8-8b6d-29ee6fe19f48'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = 'c7463abe-cae5-476c-8119-2b6323d0c998'
UPDATE dbo.TemplateColumn SET VisiblePosition = 24 WHERE ID = '846b9bac-ee41-4527-b248-2c8ee22bbf3d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = '5693e7de-0368-4553-a0cf-2d21dddc2f5d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 39 WHERE ID = 'a37084a6-fc13-4d74-8198-2d6fddc04356'
UPDATE dbo.TemplateColumn SET VisiblePosition = 19 WHERE ID = '1abdbf11-3c78-47fa-8590-2dc6ded4a993'
UPDATE dbo.TemplateColumn SET VisiblePosition = 39 WHERE ID = '27e5c3e3-9076-4254-966c-2e5844553eb1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 27 WHERE ID = 'bbabd570-37a0-49ff-8f41-2f1427c9e5f9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 15 WHERE ID = '82eaa3ef-42f8-44c4-82a8-2fadf7b4fd80'
UPDATE dbo.TemplateColumn SET VisiblePosition = 28 WHERE ID = '1526ffc4-113d-43b7-8be3-30831ef86ddb'
UPDATE dbo.TemplateColumn SET VisiblePosition = 15 WHERE ID = '52c0513f-dece-40ec-abeb-3261bc999914'
UPDATE dbo.TemplateColumn SET VisiblePosition = 25 WHERE ID = 'ce9f5a65-b403-49e5-8ddd-3770f79d0461'
UPDATE dbo.TemplateColumn SET VisiblePosition = 15 WHERE ID = 'b1c573d0-ba9b-4ed3-8991-37d9d355ca12'
UPDATE dbo.TemplateColumn SET VisiblePosition = 32 WHERE ID = '42f992d8-7697-481e-a312-38597003fd11'
UPDATE dbo.TemplateColumn SET VisiblePosition = 35 WHERE ID = '9d4765d9-d7b8-40a3-9f2d-38f87f9d1930'
UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = 'd629de13-14cb-49ac-97e8-3955ee0f7f6a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 33 WHERE ID = 'e7d7d501-49a2-4667-b1e2-398003aeed64'
UPDATE dbo.TemplateColumn SET VisiblePosition = 37 WHERE ID = '46e97751-5a8e-4913-b8df-39ad0570703d'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'c299228c-90cc-489f-a4b7-39e12d4d3cee'
UPDATE dbo.TemplateColumn SET VisiblePosition = 20 WHERE ID = '71fb2f7d-5a55-493f-aad3-3a6d8aaba919'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = 'e585b533-2c57-40a2-b30a-409cbbd27ad7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 25 WHERE ID = 'daba97f4-829d-4769-a9b7-40b12de0cdb1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 29 WHERE ID = 'fbf60b71-5a8a-46aa-9b1d-4168d7518fa9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 42 WHERE ID = 'cc181cd0-6c97-47ee-8715-41bc24589595'
UPDATE dbo.TemplateColumn SET VisiblePosition = 35 WHERE ID = '5a6e6104-89b6-4dbf-a449-45630cb18eaa'
UPDATE dbo.TemplateColumn SET VisiblePosition = 41 WHERE ID = '538acde1-9f20-405e-9cff-47b3e4f6c43a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 26 WHERE ID = 'c7448528-5be4-40d4-9c23-47c6ed3f6d6d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = '90c8bec3-48e4-4c90-9bce-48baacb359e0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 26 WHERE ID = '07d00271-4fdc-4343-88ef-4947a5d2fe23'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'd9cbc930-5a45-43f4-bddd-49f061bd9b7e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 34 WHERE ID = '40da52fa-6b74-47a8-9deb-4a8c6d333558'
UPDATE dbo.TemplateColumn SET VisiblePosition = 41 WHERE ID = '116affab-89e5-4104-8e7a-4aee0d055c72'
UPDATE dbo.TemplateColumn SET VisiblePosition = 42 WHERE ID = '8a3ca439-26c1-4be7-88e7-4bab6ea6c5a7'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'cd241421-e7b9-4a94-9688-4e1a87a602d3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = '4c2d453a-42f1-438a-9a81-510e274c0195'
UPDATE dbo.TemplateColumn SET VisiblePosition = 41 WHERE ID = 'fe86671b-2637-4d3a-8b67-546490fecb76'
UPDATE dbo.TemplateColumn SET VisiblePosition = 41 WHERE ID = '65457133-4124-4a02-848c-55ed95027e22'
UPDATE dbo.TemplateColumn SET VisiblePosition = 26 WHERE ID = '728cf2c0-cf5a-4e49-85da-5730edf6d09a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 32 WHERE ID = 'be8867e4-6969-4f4a-9606-599189524ec3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 31 WHERE ID = '35ec0fa1-3bab-4899-b9e6-5a2e73e27972'
UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = 'fe674553-c9d8-4988-abe4-5b35968c3f65'
UPDATE dbo.TemplateColumn SET VisiblePosition = 27 WHERE ID = '9ac30385-f182-4440-b96d-5bc8382b6d7b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 25 WHERE ID = '8d1f6073-7b51-4891-b503-5c84edcde0fa'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '26d69147-d392-466b-ba3a-5ca6d4476835'
UPDATE dbo.TemplateColumn SET VisiblePosition = 31 WHERE ID = '1fc074e8-ec3a-4fdd-819e-5d2ef7db2f13'
UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = '8419f771-54bd-4b86-8ea2-5d571a789672'
UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = '9064924b-f905-4d9d-b761-5d7f571c769b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 38 WHERE ID = '1e1a5c92-89b9-4375-90be-5e9da09ec93d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 15 WHERE ID = 'cc9cf802-91d7-4102-8621-5f2ebc6bff2c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = '008695b3-d20a-4e99-8d0b-61a25687e678'
UPDATE dbo.TemplateColumn SET VisiblePosition = 27 WHERE ID = '383246eb-8a21-4656-b6b7-631920396826'
UPDATE dbo.TemplateColumn SET VisiblePosition = 34 WHERE ID = 'df8eb5b2-bdb3-4922-bd7c-632a2fc7f9a0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 28 WHERE ID = '801a01ac-0d6a-4db5-818a-64ab5b5ef9d0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = 'c81d2b73-57d4-40a5-bb06-651d4530016d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 37 WHERE ID = 'e3feaa57-c256-40bf-9abf-6782fe2e0436'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = '87aed74e-d603-4658-8528-69a52dd905eb'
UPDATE dbo.TemplateColumn SET VisiblePosition = 30 WHERE ID = '2509d0d9-71e0-48dd-8358-6a232ae55122'
UPDATE dbo.TemplateColumn SET VisiblePosition = 19 WHERE ID = '5d249ad3-5881-4dd7-a9f2-6b0bf70e834d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 31 WHERE ID = '5d02c02d-71ba-4b5d-8b92-6e39a8596071'
UPDATE dbo.TemplateColumn SET VisiblePosition = 34 WHERE ID = 'f16af392-d98c-4926-bbf0-7167e8050b8c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 22 WHERE ID = 'ed010101-c5e0-4eec-b626-72d0a8b977c0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 29 WHERE ID = '923f9964-d0d5-4098-add3-74efe1d3e27d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 41 WHERE ID = '49718d83-37da-4514-a53b-753fc485cebc'
UPDATE dbo.TemplateColumn SET VisiblePosition = 38 WHERE ID = 'e3551320-3851-49f3-b172-773559505270'
UPDATE dbo.TemplateColumn SET VisiblePosition = 20 WHERE ID = 'd17fa748-c4d1-4d8d-aca5-79064b3cb388'
UPDATE dbo.TemplateColumn SET VisiblePosition = 28 WHERE ID = '84138eeb-9bc2-4939-90ba-795424007f06'
UPDATE dbo.TemplateColumn SET VisiblePosition = 37 WHERE ID = '4db34a9d-a274-47a5-994a-7a5a991493aa'
UPDATE dbo.TemplateColumn SET VisiblePosition = 40 WHERE ID = 'b4a815fd-d02e-4686-b334-7b28ab55aea1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 32 WHERE ID = '456cd9db-8fec-43a5-a5d9-7bdb1293ad16'
UPDATE dbo.TemplateColumn SET VisiblePosition = 33 WHERE ID = '53ad9004-6cc3-4689-bdd9-7d46c95a74d4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 26 WHERE ID = '1e3f2c1b-40ff-4d68-95d6-7eac245229aa'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = 'd0a85dec-4c40-4e49-97a3-8023b5b7a13e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 40 WHERE ID = 'da5d6136-a214-4125-b4cb-864b177a6bda'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = 'd9823b63-e2c2-43b7-b313-8827b35e16df'
UPDATE dbo.TemplateColumn SET VisiblePosition = 34 WHERE ID = '5126da67-6d75-40d7-a02c-889112544ed8'
UPDATE dbo.TemplateColumn SET VisiblePosition = 25 WHERE ID = '6bce0169-9cf2-4f1a-9e88-88b361a3a549'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = '84bd9a1a-d73b-4ca7-8183-88fabf1d275d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 24 WHERE ID = '594f5c84-7a1f-4385-8b38-8906fa45867a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 29 WHERE ID = 'ef5f7d8f-5497-44d9-9697-8afc5a8c87fd'
UPDATE dbo.TemplateColumn SET VisiblePosition = 40 WHERE ID = '0bbab689-d85a-47da-b932-8fdab43a08e3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 24 WHERE ID = '573959ef-6973-4f80-a059-90f0461f17a3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 37 WHERE ID = '91945670-5634-4b96-bf8f-9404898f9cae'
UPDATE dbo.TemplateColumn SET VisiblePosition = 27 WHERE ID = 'f6f2ab01-e17b-446b-921d-94579b027a42'
UPDATE dbo.TemplateColumn SET VisiblePosition = 30 WHERE ID = '47f5ba5a-b426-49d2-bb86-94cbb0f39fc1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 30 WHERE ID = '7cd26d4b-f0a2-4c79-99f1-94eab24c263a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = '71b6bcec-459e-45a4-a7eb-9517aa859ea0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 23 WHERE ID = 'e79dbac3-1ec5-4c80-b165-95275b0f5bbe'
UPDATE dbo.TemplateColumn SET VisiblePosition = 22 WHERE ID = '129757b4-b814-4b40-b394-97e9b3668a6c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 40 WHERE ID = 'bf65958a-8bf4-46a7-889a-9ab6be7122ad'
UPDATE dbo.TemplateColumn SET VisiblePosition = 40 WHERE ID = '9f735c3c-e364-4074-8ffa-9ad7822a9761'
UPDATE dbo.TemplateColumn SET VisiblePosition = 35 WHERE ID = 'dbcfe580-c403-4d02-9b5d-9bce121b07b1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 23 WHERE ID = '292b0abb-8de2-4814-8726-9c2af5688217'
UPDATE dbo.TemplateColumn SET VisiblePosition = 23 WHERE ID = 'd43f105e-bd04-49ae-8906-9c349e2c107a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 20 WHERE ID = '41ef5af8-5d07-4941-89ce-9d315d1998c0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 15 WHERE ID = '278950b5-0e49-4a0e-9cac-a11c92659dd5'
UPDATE dbo.TemplateColumn SET VisiblePosition = 23 WHERE ID = '590bc5ab-a58f-49a3-8d63-a18bbf55eaa4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = '0e82c630-6945-4560-b4a3-a2d66dc7d024'
UPDATE dbo.TemplateColumn SET VisiblePosition = 24 WHERE ID = 'a70c9b2d-49d2-41fe-87de-a47f971230b8'
UPDATE dbo.TemplateColumn SET VisiblePosition = 35 WHERE ID = '2f0393e2-96e8-4a9d-a6df-a5980a4c1ff0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 29 WHERE ID = '341cfe11-71bd-457c-8540-a5c53a66f1f8'
UPDATE dbo.TemplateColumn SET VisiblePosition = 31 WHERE ID = '71e613e3-fa43-4ba2-87ee-a74670100b52'
UPDATE dbo.TemplateColumn SET VisiblePosition = 19 WHERE ID = 'ffde9d29-b265-49af-8e42-a9276a2812d9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = '5db83f27-6eab-46a0-b6c0-a9a9be79a6cb'
UPDATE dbo.TemplateColumn SET VisiblePosition = 22 WHERE ID = '60a72455-ba04-432c-bcda-a9ef0d7f7125'
UPDATE dbo.TemplateColumn SET VisiblePosition = 27 WHERE ID = '38b2ddf7-3447-477c-9463-abf1b7502089'
UPDATE dbo.TemplateColumn SET VisiblePosition = 41 WHERE ID = '08a683c3-421f-427d-b84a-ac64c187d7a7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 32 WHERE ID = '6e7bdc36-03f8-4bbd-8565-ae6c5c21085b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = 'e3d5d459-c9fa-4d00-87e1-afbadd9408f3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 33 WHERE ID = '2911eefb-c95e-4f89-8109-b14388c76d5e'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '341a69f0-cfb3-4778-b7a6-bc3309450509'
UPDATE dbo.TemplateColumn SET VisiblePosition = 37 WHERE ID = '94c53ee3-e774-45d8-b97a-bc69aa61d206'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = 'a712e950-be6d-45b1-b35c-bc8ef8073aa2'
UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = '7543e454-68ac-46fc-adb5-bdec0858d4fb'
UPDATE dbo.TemplateColumn SET VisiblePosition = 39 WHERE ID = '7de2c9b2-ecf8-4344-a42f-bf5f2e9b9460'
UPDATE dbo.TemplateColumn SET VisiblePosition = 20 WHERE ID = '9bea85a1-e843-40b3-af68-c030522cb566'
UPDATE dbo.TemplateColumn SET VisiblePosition = 40 WHERE ID = '32d24a10-5095-45fa-bf09-c0e9a3e09155'
UPDATE dbo.TemplateColumn SET VisiblePosition = 30 WHERE ID = '3de27bcb-4ca5-4e2a-a2cd-c1c512180411'
UPDATE dbo.TemplateColumn SET VisiblePosition = 37 WHERE ID = 'cd4a7008-a5ea-4bb6-9140-c21340fd92fe'
UPDATE dbo.TemplateColumn SET VisiblePosition = 38 WHERE ID = '2a068106-6f94-4ee7-9d73-c26db4f8c565'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = 'ee3d32cb-0cf1-4767-9b2e-c36825c00e67'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = 'c5a3c8a3-9ee5-479b-8470-c4beefd8af20'
UPDATE dbo.TemplateColumn SET VisiblePosition = 22 WHERE ID = '895cc13c-ed63-4793-bab6-c58eea5d395a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 29 WHERE ID = 'ff7afca3-8780-4ffe-97c7-c6942fabd0e5'
UPDATE dbo.TemplateColumn SET VisiblePosition = 28 WHERE ID = 'b8b21ddc-aa05-4a78-944f-c6a1c533a055'
UPDATE dbo.TemplateColumn SET VisiblePosition = 33 WHERE ID = 'f0ff7a3d-ea46-493f-ad01-c76bbe6b5d56'
UPDATE dbo.TemplateColumn SET VisiblePosition = 39 WHERE ID = 'dde91053-6f4b-4017-aad1-c7796679ddc8'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = '7d8a0c3a-73d8-4392-95b0-cbe80a9844c3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = 'acc269fe-eef6-4b0f-b62a-cbf43c6496bc'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = 'fbffdebc-edd0-49b8-92cb-cdcc74921491'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = '01b37715-a4e2-4847-a398-d071f722a132'
UPDATE dbo.TemplateColumn SET VisiblePosition = 30 WHERE ID = '6f19160d-067c-45ab-b554-d463660d9ffe'
UPDATE dbo.TemplateColumn SET VisiblePosition = 32 WHERE ID = 'cca08a9a-e724-45e5-8b23-d4cab3868d3b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 39 WHERE ID = 'c6bc2f57-0142-440b-b282-d60985a231ee'
UPDATE dbo.TemplateColumn SET VisiblePosition = 19 WHERE ID = '9f84720a-fb83-40e8-844b-d90efdbc06bc'
UPDATE dbo.TemplateColumn SET VisiblePosition = 24 WHERE ID = 'e09b9ce9-081f-48eb-9d4c-d9e446a3e622'
UPDATE dbo.TemplateColumn SET VisiblePosition = 28 WHERE ID = 'e3fbe0b0-93c2-41ea-96bf-d9ed55ca72ac'
UPDATE dbo.TemplateColumn SET VisiblePosition = 33 WHERE ID = 'eff5d5ef-1e73-42b4-bf0c-da777582baa9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 34 WHERE ID = '7016b61c-d4bd-4f7e-b95a-dd28e259f96b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 35 WHERE ID = '4361ac97-b5e7-4002-abae-dd8c9cb305f7'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'f8fc4912-8285-423f-b4d0-de82e59f94d0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = '3620db1e-67f7-43f2-9d1e-e07c69082393'
UPDATE dbo.TemplateColumn SET VisiblePosition = 25 WHERE ID = 'e8bf8ab9-4078-400c-9f42-e1e26d07c343'
UPDATE dbo.TemplateColumn SET VisiblePosition = 35 WHERE ID = '1f700a0a-628d-48e2-a697-e28616864872'
UPDATE dbo.TemplateColumn SET VisiblePosition = 23 WHERE ID = '979f1804-f291-4102-bb19-e2a0dd93c2d3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = '72897c75-9733-4931-83a3-e45c18534db0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 38 WHERE ID = 'e633089b-0c54-43fe-927a-e4ff4916332c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = 'bad25e33-90d2-48d3-bc7e-e5da979ff626'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = 'db6d2311-d2a1-42c3-8b30-e7a93db2df75'
UPDATE dbo.TemplateColumn SET VisiblePosition = 22 WHERE ID = '79148a6c-0c37-4a24-95af-e97f2c2662bf'
UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = 'aff05876-196e-4714-8b4e-e9f9b3523f65'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = 'f8d0e354-80c9-436c-b9f3-ea8b1f45b9f7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 26 WHERE ID = '48cf9bc4-33e3-4ade-b0a8-ead82d7ddf5a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = '999b9613-41e2-4ccf-89a2-eb40f558ca94'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = 'da0e1ee1-b3f8-42f6-bf30-ec30c667089b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = '4972445e-fc12-4d51-b996-ec3c3ef3ef0b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = '945d9144-4c04-484c-aa2a-ec732bf2de6b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 20 WHERE ID = 'addb42cd-770b-4cb2-bb5c-ec91d56c2722'
UPDATE dbo.TemplateColumn SET VisiblePosition = 38 WHERE ID = 'a3f4b15f-95da-4151-8333-ee123868c226'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = '523b533d-f2e7-48e0-ad7e-efba63694198'
UPDATE dbo.TemplateColumn SET VisiblePosition = 32 WHERE ID = 'd951cc40-36db-4be6-8595-f019b6e1e9e6'
UPDATE dbo.TemplateColumn SET VisiblePosition = 23 WHERE ID = 'eefd9b8b-59f1-42a4-994b-f0e740550bf7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 29 WHERE ID = '40de6a2a-2d95-48b9-9725-f17190d4cdb7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = '10f7a4ab-24ad-4a5e-97be-f1de88f50755'
UPDATE dbo.TemplateColumn SET VisiblePosition = 19 WHERE ID = '0d0486c2-ce7f-4fb8-9c9b-f25c7da43081'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = '9806785c-a359-46a3-824d-f2c984dcad81'
UPDATE dbo.TemplateColumn SET VisiblePosition = 20 WHERE ID = 'c0fc7fb8-e01e-4f9e-9872-f58a3e35a883'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = '0315572c-16d7-4667-bc06-f64ebab414fb'
UPDATE dbo.TemplateColumn SET VisiblePosition = 22 WHERE ID = 'b874d151-18a3-439b-8390-f81f46560148'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = '7143f2a4-d805-49ba-b66f-fa9047de2041'
UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = 'cb922fdd-5fe4-48e7-bbfb-fa9d47cb5e7e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = 'c8136632-0a44-4cbf-a133-facd85c7cf7d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = 'afbc3c22-cbd1-4f54-9433-fb13660a883a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 31 WHERE ID = '3ffef982-210e-4374-838c-fbee7618da85'
UPDATE dbo.TemplateColumn SET VisiblePosition = 31 WHERE ID = '261751dc-e600-4495-83bd-fef6e1a48fd2'
UPDATE dbo.TemplateColumn SET VisiblePosition = 27 WHERE ID = 'cfbb71ac-5f61-4ff8-94dc-ffc66cef4434'

INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('80ff05d9-e827-4cb7-ad7a-0825c70116c1', '44ecd862-4c61-4adc-85de-6265c00c0784', N'UnitPrice', N'Đơn giá QĐ', N'Đơn giá quy đổi', 0, 0, 140, 0, 0, 0, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('691b0e26-546d-4869-90d2-1d403308a28e', 'b2ea65bc-28e5-4211-911a-8d5448d5f380', N'UnitPrice', N'Đơn giá QĐ', N'Đơn giá quy đổi', 0, 0, 140, 0, 0, 0, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('6f51e722-2191-48dc-ad89-20dac80adb0d', 'b2ea65bc-28e5-4211-911a-8d5448d5f380', N'Quantity', N'Số lượng', N'', 0, 0, 110, 0, 0, 1, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('eae314b2-cec9-4663-b322-2446c8aa9036', '9c7f0e85-42be-45ee-8122-33575ac35d8f', N'UnitPriceOriginal', N'Đơn giá', N'', 0, 0, 110, 0, 0, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('df9dd36e-a99c-4c3f-8953-39c1a36ee988', '44ecd862-4c61-4adc-85de-6265c00c0784', N'Quantity', N'Số lượng', N'', 0, 0, 110, 0, 0, 1, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('b377d385-3f46-4c53-b8f2-5ba1b88cab01', '0a8c7655-1cff-4a78-983b-bfdb8cb21f76', N'UnitPrice', N'Đơn giá QĐ', N'Đơn giá quy đổi', 0, 0, 140, 0, 0, 0, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a645e2ed-671d-4b25-9de8-5c60ed7d1c40', '9c7f0e85-42be-45ee-8122-33575ac35d8f', N'UnitPrice', N'Đơn giá QĐ', N'Đơn giá quy đổi', 0, 0, 140, 0, 0, 0, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e3a76bbc-fddf-4606-aaaa-6c30a4f3ccca', '0a8c7655-1cff-4a78-983b-bfdb8cb21f76', N'Quantity', N'Số lượng', N'', 0, 0, 110, 0, 0, 1, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9f141419-5694-41be-82ef-7746c75a1261', '0a8c7655-1cff-4a78-983b-bfdb8cb21f76', N'UnitPriceOriginal', N'Đơn giá', N'', 0, 0, 110, 0, 0, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('bec1bfbd-d92e-4782-b3aa-8dab26f16c62', '9a7b2621-6c85-4d7c-94f0-63e5f68e2292', N'UnitPriceOriginal', N'Đơn giá', N'', 0, 0, 110, 0, 0, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('81406f8c-47e0-4f16-8812-a674aa3a9f2d', '9a7b2621-6c85-4d7c-94f0-63e5f68e2292', N'UnitPrice', N'Đơn giá QĐ', N'Đơn giá quy đổi', 0, 0, 140, 0, 0, 0, 9)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9ce9fef6-5caf-43a2-bd83-c7d7de5e82ea', 'b2ea65bc-28e5-4211-911a-8d5448d5f380', N'UnitPriceOriginal', N'Đơn giá', N'', 0, 0, 110, 0, 0, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('3ad0f11d-b6d5-43d8-a45c-cc3170248399', '955ce7d1-9af4-4cbd-bbca-4129e1b7f3b2', N'UnitPriceOriginal', N'Đơn giá', N'', 0, 0, 110, 0, 0, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('409961bf-689f-40de-aa32-d6f53ee6bc51', '9a7b2621-6c85-4d7c-94f0-63e5f68e2292', N'Quantity', N'Số lượng', N'', 0, 0, 110, 0, 0, 1, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('caae3485-6fd9-49a2-81a4-d8d8ee607127', '9c7f0e85-42be-45ee-8122-33575ac35d8f', N'Quantity', N'Số lượng', N'', 0, 0, 110, 0, 0, 1, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('9167bd5a-7eff-4f57-b3ee-ddc748ee68f6', '44ecd862-4c61-4adc-85de-6265c00c0784', N'UnitPriceOriginal', N'Đơn giá', N'', 0, 0, 110, 0, 0, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('a8808122-4be2-4e0a-a351-e5395d936f95', '955ce7d1-9af4-4cbd-bbca-4129e1b7f3b2', N'Quantity', N'Số lượng', N'', 0, 0, 110, 0, 0, 1, 7)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('eeab820e-dd38-4c32-96ed-f07d0fabc9b9', '955ce7d1-9af4-4cbd-bbca-4129e1b7f3b2', N'UnitPrice', N'Đơn giá QĐ', N'Đơn giá quy đổi', 0, 0, 140, 0, 0, 0, 9)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

SET IDENTITY_INSERT dbo.Type ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.Type(ID, TypeName, TypeGroupID, Recordable, Searchable, PostType, OrderPriority) VALUES (602, N'Chứng từ khấu trừ thuế GTGT', 60, 1, 1, 1, 132)
GO
SET IDENTITY_INSERT dbo.Type OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DBCC CHECKIDENT('dbo.Type', RESEED, 132)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'340,330' WHERE ID = 153
UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'600,690,602,620' WHERE ID = 173

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
SET IDENTITY_INSERT dbo.VoucherPatternsReport ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (230, 0, 0, N'TAIN-02', N'tờ khai quyết toán thuế tài nguyên - 02', N'ToKhaiThueTaiNguyen02.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (231, 0, 0, N'TNDN-03', N'Tờ khai quyết toán thuế thu nhập doanh nghiệp 03', N'ToKhaiQuyetToanThueThuNhapDoanhNghiep.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (232, 0, 0, N'01-GTGT', N'Tờ khai thuế GTGT (Mẫu 01/GTGT)', N'ToKhaiThueGiaTriGiaTangTM01.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (233, 0, 0, N'TAIN-01', N'Tờ khai thuế tài nguyên 01', N'ToKhaiThueTaiNguyen01.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (234, 0, 0, N'BangTinhBaoHiem', N'Bảng tính bảo hiểm', N'BangTinhBaoHiemXaHoi.rst', 1, N'830,831,832')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (235, 0, 0, N'BangTongHopChamCong', N'Bảng tổng hợp chấm công', N'BangTongHopChamCong.rst', 1, N'810,811')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (236, 0, 10, N'MCReceipt-01-TT-A5', N'Phiếu thu: Mẫu 01-TT (A5)', N'MCReceipt-01-TT-A5.rst', 1, N'100,101')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (237, 0, 11, N'MCPayment-02-TT-A5', N'Phiếu chi: 02-TT (A5)', N'MCPayment-02-TT-A5.rst', 1, N'110,111,112,113,114,115,118,119,902')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (238, 0, 0, N'SAQuote-BH-PT-A5', N'Phiếu thu: Mẫu 01-TT (A5)', N'SAQuote-BH-PT-A5.rst', 1, N'321,322,102,324,325,103')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (239, 0, 0, N'PPDiscountReturn.PT-A5', N'Phiếu thu: Mẫu 01-TT (A5)', N'PPDiscountReturn.PT-A5.rst', 1, N'220,230')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (240, 0, 0, N'SAReturn-PC-A5', N'Phiếu chi: 02-TT (A5)', N'SAReturn-PC-A5.rst', 1, N'340,330')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (252, 0, 0, N'PPInvoice-02-TT-A5', N'Phiếu chi: 02-TT (A5)', N'PPInvoice-02-TT-A5.rst', 1, N'260,117')
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (259, 0, 0, N'PPService-02-TT-A5', N'Phiếu chi: 02-TT (A5)', N'PPService-02-TT-A5.rst', 1, N'240,116')
GO
SET IDENTITY_INSERT dbo.VoucherPatternsReport OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DBCC CHECKIDENT('dbo.VoucherPatternsReport', RESEED, 259)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Type WITH NOCHECK
  ADD CONSTRAINT FK_Type_TypeGroup FOREIGN KEY (TypeGroupID) REFERENCES dbo.TypeGroup(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Template WITH NOCHECK
  ADD CONSTRAINT FK_Template_Type FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153AdjustAnnouncement WITH NOCHECK
  ADD CONSTRAINT FK__TT153Adju__TypeI__25869641 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153DeletedInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Dele__TypeI__119F9925 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153DestructionInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Dest__TypeI__22AA2996 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153LostInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Lost__TypeI__1FCDBCEB FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153PublishInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Publ__TypeI__1CF15040 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153RegisterInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Regi__TypeI__1A14E395 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF
