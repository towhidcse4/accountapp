SET CONCAT_NULL_YIELDS_NULL, ANSI_NULLS, ANSI_PADDING, QUOTED_IDENTIFIER, ANSI_WARNINGS, ARITHABORT, XACT_ABORT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS OFF
GO

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO

ENABLE TRIGGER [dbo].[trgInsteadOfOperation] ON [dbo].[TemplateColumn]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GenCode]
  ADD [IsReset] [bit] NOT NULL DEFAULT ('1')
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'GenCode', 'COLUMN', N'IsReset'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoiceDetail]
  ADD [VATDescription] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE PROCEDURE [dbo].[Proc_GetBkeInv_Sale]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT
    )
AS
BEGIN 
/*
VATRate type :
-1: Không chịu thuế
0: thuê 0%
5: 5%
10: 10%
*/
if @IsSimilarBranch = 0
	 select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
	  Description as Mat_hang, (Amount-DiscountAmount) as Doanh_so, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  
	   from SAinvoice a join SAInvoiceDetail b on a.id = b.SAInvoiceID where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1 
	union all
	select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST, 
	Description as Mat_hang, (Amount-DiscountAmount) as Doanh_so, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  
	 from  SABill a join SABillDetail b on a.ID = b.SABillID  where cast(InvoiceDate As Date) between @FromDate and @ToDate
	union all
	select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
	  Description as Mat_hang,  (Amount-DiscountAmount) as Doanh_so, VATAmount as Thue_GTGT, VATAccount as TKThue,-1 flag  
	 from SAReturn a join SAReturnDetail b on a.id = b.SAReturnID
	where cast(InvoiceDate As Date) between @FromDate and @ToDate and recorded = 1 
	order by VATRate, InvoiceDate, InvoiceNo,AccountingObjectName
else
select VATRate, 
       so_hd, 
	   ngay_hd, 
	   Nguoi_mua, 
	   MST,
	   Mat_hang, 
	   sum(Doanh_so) Doanh_so, 
	   sum(Thue_GTGT) Thue_GTGT, 
	   TKThue,
	   flag
from
	(
	select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
	  a.Reason as Mat_hang, (Amount-DiscountAmount) as Doanh_so, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  
	   from SAinvoice a join SAInvoiceDetail b on a.id = b.SAInvoiceID where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1 
	union all
	select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST, 
	a.Reason as Mat_hang, (Amount-DiscountAmount) as Doanh_so, VATAmount as Thue_GTGT, VATAccount as TKThue,1 flag  
	 from  SABill a join SABillDetail b on a.ID = b.SABillID  where cast(InvoiceDate As Date) between @FromDate and @ToDate
	union all
	select VATRate, InvoiceNo as so_hd, InvoiceDate as ngay_hd, AccountingObjectName as Nguoi_mua, CompanyTaxCode as MST,
	  a.Reason as Mat_hang,  (Amount-DiscountAmount) as Doanh_so, VATAmount as Thue_GTGT, VATAccount as TKThue,-1 flag  
	 from SAReturn a join SAReturnDetail b on a.id = b.SAReturnID
	where cast(InvoiceDate As Date) between @FromDate and @ToDate and recorded = 1) aa
group by VATRate, 
       so_hd, 
	   ngay_hd, 
	   Nguoi_mua, 
	   MST,
	   Mat_hang, 
	   TKThue ,
	   flag
order by VATRate, ngay_hd, so_hd,Nguoi_mua
end;
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[PSSalarySheetDetail]
  ADD [BasicWageAmount] [money] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBInternalTransferTax]
  ADD [InvoiceTemplate] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBInternalTransferDetail]
  ADD [VATDescription] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE PROCEDURE [dbo].[Proc_GetBkeInv_Buy]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @IsSimilarBranch BIT
    )
AS
BEGIN

if @IsSimilarBranch = 0    
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
	 b.Description as Mat_hang, InwardAmount  as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag                  
	 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
	where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1
	union all         
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
	 b.Description as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag         
	from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
	join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
	where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1
	union all    
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
	 b.Description as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag         
	 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
	where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1 order by c.GoodsServicePurchaseCode, InvoiceDate, InvoiceNo;
else
select aa.GoodsServicePurchaseCode,
       aa.GoodsServicePurchaseName,
	   aa.So_HD,
	   aa.Ngay_HD,
	   aa.ten_NBan,
	   aa.MST,
	   aa.Mat_hang,
	   sum(aa.GT_chuathue) GT_chuathue,
	   aa.Thue_suat ,
	   sum(aa.Thue_GTGT) Thue_GTGT,
	   aa.TK_Thue,
	   flag
from (
     select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
	 a.Reason as Mat_hang, InwardAmount  as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,1 flag         
	 from PPInvoice a join PPInvoiceDetail b on a.ID = b.PPInvoiceID         
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
	where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1
	union all         
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, CompanyTaxCode as MST,         
	 a.Reason as Mat_hang, Amount as GiatriHH_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue,-1 flag         
	from PPDiscountReturn a join PPDiscountReturnDetail b on a.ID = b.PPDiscountReturnID         
	join GoodsServicePurchase c on c.ID= b.GoodsServicePurchaseID         
	where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1
	union all    
	select c.GoodsServicePurchaseCode,c.GoodsServicePurchaseName, InvoiceNo as So_HD, InvoiceDate as Ngay_HD, AccountingObjectName as ten_NBan, b.CompanyTaxCode as MST,         
	 a.Reason as Mat_hang, Amount as GT_chuathue, VATRate as Thue_suat, VATAmount as Thue_GTGT, VATAccount as TK_Thue ,1 flag        
	 from PPService a join PPServiceDetail b on a.ID = b.PPServiceID         
	join GoodsServicePurchase c on c.id = b.GoodsServicePurchaseID         
	where cast(InvoiceDate As Date) between @FromDate and @ToDate and Recorded = 1 ) aa
group by GoodsServicePurchaseCode,
       GoodsServicePurchaseName,
	   So_HD,
	   Ngay_HD,
	   ten_NBan,
	   MST,
	   Mat_hang,
	   TK_Thue,
	   flag,
	   aa.Thue_suat
order by aa.GoodsServicePurchaseCode, aa.Ngay_HD, aa.So_HD
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DROP VIEW [dbo].[ViewVoucherInvisible]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
Create View [dbo].[ViewVoucherInvisible] AS
SELECT 
	newid() as ID,
	dbo.MCPayment.ID as RefID, 
	dbo.MCPayment.TypeID, 
	dbo.Type.TypeName, 
	dbo.Type.TypeGroupID,
	dbo.MCPayment.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.MCPayment.Date,
	dbo.MCPayment.PostedDate, 
	dbo.MCPayment.CurrencyID, 
	dbo.MCPayment.Reason, 
	dbo.MCPayment.EmployeeID, 
	dbo.MCPayment.BranchID, 
	dbo.MCPayment.Recorded, 
	dbo.MCPayment.TotalAmount, 
	dbo.MCPayment.TotalAmountOriginal, 
	'MCPayment' AS RefTable
FROM dbo.MCPayment INNER JOIN
	dbo.Type ON MCPayment.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.MBDeposit.ID as RefID,  
	dbo.MBDeposit.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.MBDeposit.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.MBDeposit.Date, 
	dbo.MBDeposit.PostedDate, 
	dbo.MBDeposit.CurrencyID, 
	dbo.MBDeposit.Reason, 
	dbo.MBDeposit.EmployeeID, 
	dbo.MBDeposit.BranchID, 
	dbo.MBDeposit.Recorded, 
	dbo.MBDeposit.TotalAmount, 
	dbo.MBDeposit.TotalAmountOriginal, 
	'MBDeposit' AS RefTable
FROM dbo.MBDeposit INNER JOIN
	dbo.Type ON MBDeposit.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.MBInternalTransfer.ID as RefID, 
	dbo.MBInternalTransfer.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.MBInternalTransfer.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.MBInternalTransfer.Date, 
	dbo.MBInternalTransfer.PostedDate, 
	'' AS CurrencyID,
	dbo.MBInternalTransfer.Reason,
	NULL AS EmployeeID, 
	dbo.MBInternalTransfer.BranchID, 
	dbo.MBInternalTransfer.Recorded, 
	dbo.MBInternalTransfer.TotalAmountOriginal, 
	dbo.MBInternalTransfer.TotalAmount, 
	'MBInternalTransfer' AS RefTable
FROM dbo.MBInternalTransfer INNER JOIN
	dbo.Type ON dbo.MBInternalTransfer.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.MBTellerPaper.ID as RefID, 
	dbo.MBTellerPaper.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.MBTellerPaper.No, 
	NULL AS InwardNo,
	NULL AS OutwardNo, 
	dbo.MBTellerPaper.Date, 
	dbo.MBTellerPaper.PostedDate, 
	dbo.MBTellerPaper.CurrencyID, 
	dbo.MBTellerPaper.Reason, 
	dbo.MBTellerPaper.EmployeeID, 
	dbo.MBTellerPaper.BranchID, 
	dbo.MBTellerPaper.Recorded, 
	dbo.MBTellerPaper.TotalAmountOriginal, 
	dbo.MBTellerPaper.TotalAmount, 
	'MBTellerPaper' AS RefTable
FROM dbo.MBTellerPaper INNER JOIN
	dbo.Type ON dbo.MBTellerPaper.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.MBCreditCard.ID as RefID, 
	dbo.MBCreditCard.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.MBCreditCard.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.MBCreditCard.Date, 
	dbo.MBCreditCard.PostedDate, 
	dbo.MBCreditCard.CurrencyID, 
	dbo.MBCreditCard.Reason, 
	dbo.MBCreditCard.EmployeeID, 
	dbo.MBCreditCard.BranchID, 
	dbo.MBCreditCard.Recorded, 
	dbo.MBCreditCard.TotalAmountOriginal, 
	dbo.MBCreditCard.TotalAmount, 
	'MBCreditCard' AS RefTable
FROM dbo.MBCreditCard INNER JOIN
	dbo.Type ON dbo.MBCreditCard.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.MCReceipt.ID as RefID, 
	dbo.MCReceipt.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.MCReceipt.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.MCReceipt.Date, 
	dbo.MCReceipt.PostedDate, 
	dbo.MCReceipt.CurrencyID, 
	dbo.MCReceipt.Reason, 
	dbo.MCReceipt.EmployeeID, 
	dbo.MCReceipt.BranchID, 
	dbo.MCReceipt.Recorded, 
	dbo.MCReceipt.TotalAmountOriginal, 
	dbo.MCReceipt.TotalAmount,
	'MCReceipt' AS RefTable
FROM dbo.MCReceipt INNER JOIN
	dbo.Type ON MCReceipt.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.FAAdjustment.ID as RefID,
	dbo.FAAdjustment.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.FAAdjustment.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.FAAdjustment.Date, 
	dbo.FAAdjustment.PostedDate,
	NULL AS CurrencyID, 
	dbo.FAAdjustment.Reason, 
	NULL AS EmployeeID, 
	dbo.FAAdjustment.BranchID, 
	dbo.FAAdjustment.Recorded, 
	$ 0 AS TotalAmountOriginal, 
	dbo.FAAdjustment.TotalAmount, 
	'FAAdjustment' AS RefTable
FROM dbo.FAAdjustment INNER JOIN
	dbo.Type ON FAAdjustment.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.FADepreciation.ID as RefID, 
	dbo.FADepreciation.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.FADepreciation.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.FADepreciation.Date, 
	dbo.FADepreciation.PostedDate, 
	NULL AS CurrencyID,
	dbo.FADepreciation.Reason,
	NULL AS EmployeeID, 
	dbo.FADepreciation.BranchID, 
	dbo.FADepreciation.Recorded, 
	dbo.FADepreciation.TotalAmountOriginal, 
	dbo.FADepreciation.TotalAmount,
	'FADepreciation' AS RefTable
FROM dbo.FADepreciation INNER JOIN
	dbo.Type ON FADepreciation.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.FAIncrement.ID as RefID, 
	dbo.FAIncrement.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.FAIncrement.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.FAIncrement.Date, 
	dbo.FAIncrement.PostedDate, 
	FAIncrement.CurrencyID, 
	dbo.FAIncrement.Reason, 
	dbo.FAIncrement.EmployeeID, 
	dbo.FAIncrement.BranchID, 
	dbo.FAIncrement.Recorded, 
	dbo.FAIncrement.TotalAmountOriginal, 
	dbo.FAIncrement.TotalAmount, 
	'FAIncrement' AS RefTable
FROM dbo.FAIncrement INNER JOIN
	dbo.Type ON FAIncrement.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.FADecrement.ID as RefID, 
	dbo.FADecrement.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.FADecrement.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.FADecrement.Date, 
	dbo.FADecrement.PostedDate, 
	FADecrement.CurrencyID, 
	dbo.FADecrement.Reason, 
	NULL AS EmployeeID, 
	dbo.FADecrement.BranchID, 
	dbo.FADecrement.Recorded, 
	dbo.FADecrement.TotalAmountOriginal, 
	dbo.FADecrement.TotalAmount, 
	'FADecrement' AS RefTable
FROM dbo.FADecrement INNER JOIN
	dbo.Type ON FADecrement.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.FAAudit.ID as RefID,
	dbo.FAAudit.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.FAAudit.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.FAAudit.Date, 
	dbo.FAAudit.PostedDate,
	NULL AS CurrencyID, 
	dbo.FAAudit.Description AS Reason, 
	NULL AS EmployeeID, 
	dbo.FAAudit.BranchID, 
	0 AS Recorded, 
	0 AS TotalAmountOriginal, 
	0 AS TotalAmount, 
	'FAAudit' AS RefTable
FROM dbo.FAAudit INNER JOIN
	dbo.Type ON FAAudit.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.FATransfer.ID as RefID,
	dbo.FATransfer.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.FATransfer.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.FATransfer.Date, 
	dbo.FATransfer.Date AS PostedDate,
	NULL AS CurrencyID, 
	dbo.FATransfer.Reason, 
	NULL AS EmployeeID, 
	dbo.FATransfer.BranchID, 
	dbo.FATransfer.Recorded, 
	0 AS TotalAmountOriginal, 
	0 AS TotalAmount, 
	'FATransfer' AS RefTable
FROM dbo.FATransfer INNER JOIN
	dbo.Type ON FATransfer.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT 
	newid() as ID,
	dbo.GOtherVoucher.ID as RefID, 
	dbo.GOtherVoucher.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.GOtherVoucher.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.GOtherVoucher.Date, 
	dbo.GOtherVoucher.PostedDate, 
	NULL AS CurrencyID, 
	dbo.GOtherVoucher.Reason, 
	NULL AS EmployeeID, 
	dbo.GOtherVoucher.BranchID, 
	dbo.GOtherVoucher.Recorded, 
	dbo.GOtherVoucher.TotalAmountOriginal, 
	dbo.GOtherVoucher.TotalAmount, 
	'GOtherVoucher' AS RefTable
FROM dbo.GOtherVoucher INNER JOIN
	dbo.Type ON GOtherVoucher.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.RSInwardOutward.ID as RefID, 
	dbo.RSInwardOutward.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.RSInwardOutward.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.RSInwardOutward.Date, 
	dbo.RSInwardOutward.PostedDate, 
	RSInwardOutward.CurrencyID, 
	dbo.RSInwardOutward.Reason, 
	NULL AS EmployeeID, 
	dbo.RSInwardOutward.BranchID, 
	dbo.RSInwardOutward.Recorded, 
	dbo.RSInwardOutward.TotalAmountOriginal, 
	dbo.RSInwardOutward.TotalAmount, 
	'RSInwardOutward' AS RefTable
FROM dbo.RSInwardOutward INNER JOIN
	dbo.Type ON RSInwardOutward.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.RSTransfer.ID as RefID, 
	dbo.RSTransfer.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.RSTransfer.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.RSTransfer.Date, 
	dbo.RSTransfer.PostedDate, 
	RSTransfer.CurrencyID, 
	dbo.RSTransfer.Reason, 
	NULL AS EmployeeID, 
	dbo.RSTransfer.BranchID,
	dbo.RSTransfer.Recorded, 
	dbo.RSTransfer.TotalAmountOriginal, 
	dbo.RSTransfer.TotalAmount, 
	'RSTransfer' AS RefTable
FROM dbo.RSTransfer INNER JOIN
	dbo.Type ON RSTransfer.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.PPInvoice.ID as RefID, 
	dbo.PPInvoice.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.PPInvoice.No, 
	dbo.PPInvoice.InwardNo AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.PPInvoice.Date, 
	dbo.PPInvoice.PostedDate, 
	PPInvoice.CurrencyID,
	dbo.PPInvoice.Reason, 
	PPInvoice.EmployeeID, 
	dbo.PPInvoice.BranchID, 
	dbo.PPInvoice.Recorded, 
	dbo.PPInvoice.TotalAmountOriginal, 
	dbo.PPInvoice.TotalAmount,
	'PPInvoice' AS RefTable
FROM dbo.PPInvoice INNER JOIN
	dbo.Type ON PPInvoice.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.PPDiscountReturn.ID as RefID, 
	dbo.PPDiscountReturn.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.PPDiscountReturn.No, 
	NULL AS InwardNo, 
	dbo.PPDiscountReturn.OutwardNo AS OutwardNo, 
	dbo.PPDiscountReturn.Date, 
	dbo.PPDiscountReturn.PostedDate, 
	PPDiscountReturn.CurrencyID, 
	dbo.PPDiscountReturn.Reason, 
	PPDiscountReturn.EmployeeID, 
	dbo.PPDiscountReturn.BrachID AS BranchID, 
	dbo.PPDiscountReturn.Recorded, 
	dbo.PPDiscountReturn.TotalAmountOriginal, 
	dbo.PPDiscountReturn.TotalAmount, 
	'PPDiscountReturn' AS RefTable
FROM dbo.PPDiscountReturn INNER JOIN
	dbo.Type ON PPDiscountReturn.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.PPService.ID as RefID, 
	dbo.PPService.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.PPService.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.PPService.Date, 
	dbo.PPService.PostedDate, 
	PPService.CurrencyID, 
	dbo.PPService.Reason, 
	PPService.EmployeeID, 
	dbo.PPService.BranchID, 
	dbo.PPService.Recorded, 
	dbo.PPService.TotalAmountOriginal, 
	dbo.PPService.TotalAmount, 
	'PPService' AS RefTable
FROM dbo.PPService INNER JOIN
	dbo.Type ON PPService.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.PPOrder.ID as RefID, 
	dbo.PPOrder.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.PPOrder.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.PPOrder.Date, 
	dbo.PPOrder.DeliverDate AS PostedDate, 
	dbo.PPOrder.CurrencyID, 
	dbo.PPOrder.Reason, 
	dbo.PPOrder.EmployeeID, 
	dbo.PPOrder.BranchID, 
	dbo.PPOrder.Exported AS Recorded, 
	dbo.PPOrder.TotalAmountOriginal, 
	dbo.PPOrder.TotalAmount, 
	'PPOrder' AS RefTable
FROM dbo.PPOrder INNER JOIN 
	dbo.Type ON PPOrder.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.SAInvoice.ID as RefID, 
	dbo.SAInvoice.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.SAInvoice.No, 
	NULL AS InwardNo, 
	dbo.SAInvoice.OutwardNo AS OutwardNo, 
	dbo.SAInvoice.Date, 
	dbo.SAInvoice.PostedDate, 
	SAInvoice.CurrencyID,
	dbo.SAInvoice.Reason, 
	SAInvoice.EmployeeID, 
	dbo.SAInvoice.BranchID, 
	dbo.SAInvoice.Recorded, 
	dbo.SAInvoice.TotalAmountOriginal, 
	dbo.SAInvoice.TotalAmount, 
	'SAInvoice' AS RefTable
FROM dbo.SAInvoice INNER JOIN
	dbo.Type ON dbo.SAInvoice.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.SAReturn.ID as RefID, 
	dbo.SAReturn.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.SAReturn.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.SAReturn.Date, 
	dbo.SAReturn.PostedDate, 
	SAReturn.CurrencyID, 
	dbo.SAReturn.Reason, 
	SAReturn.EmployeeID, 
	dbo.SAReturn.BranchID, 
	SAReturn.Recorded AS Posted, 
	dbo.SAReturn.TotalAmountOriginal, 
	dbo.SAReturn.TotalAmount, 
	'SAReturn' AS RefTable
FROM dbo.SAReturn INNER JOIN
	dbo.Type ON dbo.SAReturn.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.SAOrder.ID as RefID, 
	dbo.SAOrder.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.SAOrder.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.SAOrder.Date, 
	dbo.SAOrder.DeliveDate AS PostedDate,
	SAOrder.CurrencyID, 
	dbo.SAOrder.Reason, 
	SAOrder.EmployeeID, 
	dbo.SAOrder.BranchID,
	SAOrder.Exported AS Recorded, 
	dbo.SAOrder.TotalAmountOriginal, 
	dbo.SAOrder.TotalAmount, 
	'SAOrder' AS RefTable
FROM dbo.SAOrder INNER JOIN
	dbo.Type ON dbo.SAOrder.TypeID = dbo.Type.ID
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.SAQuote.ID as RefID,
	dbo.SAQuote.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.SAQuote.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.SAQuote.Date, 
	dbo.SAQuote.Date AS PostedDate,
	dbo.SAQuote.CurrencyID, 
	dbo.SAQuote.Reason, 
	dbo.SAQuote.EmployeeID, 
	dbo.SAQuote.BranchID, 
	0 AS Recorded, 
	dbo.SAQuote.TotalAmountOriginal, 
	dbo.SAQuote.TotalAmount, 
	'SAQuote' AS RefTable
FROM dbo.SAQuote INNER JOIN
	dbo.Type ON SAQuote.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.TIAdjustment.ID as RefID,
	dbo.TIAdjustment.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.TIAdjustment.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.TIAdjustment.Date, 
	dbo.TIAdjustment.PostedDate,
	NULL AS CurrencyID, 
	dbo.TIAdjustment.Reason, 
	NULL AS EmployeeID, 
	dbo.TIAdjustment.BranchID, 
	dbo.TIAdjustment.Recorded, 
	$ 0 AS TotalAmountOriginal, 
	dbo.TIAdjustment.TotalAmount, 
	'TIAdjustment' AS RefTable
FROM dbo.TIAdjustment INNER JOIN
	dbo.Type ON TIAdjustment.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.TIAllocation.ID as RefID, 
	dbo.TIAllocation.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.TIAllocation.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.TIAllocation.Date, 
	dbo.TIAllocation.PostedDate, 
	NULL AS CurrencyID,
	dbo.TIAllocation.Reason,
	NULL AS EmployeeID, 
	dbo.TIAllocation.BranchID, 
	dbo.TIAllocation.Recorded, 
	dbo.TIAllocation.TotalAmountOriginal, 
	dbo.TIAllocation.TotalAmount,
	'TIAllocation' AS RefTable
FROM dbo.TIAllocation INNER JOIN
	dbo.Type ON TIAllocation.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.TIIncrement.ID as RefID, 
	dbo.TIIncrement.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.TIIncrement.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.TIIncrement.Date, 
	dbo.TIIncrement.PostedDate, 
	dbo.TIIncrement.CurrencyID, 
	dbo.TIIncrement.Reason, 
	dbo.TIIncrement.EmployeeID, 
	dbo.TIIncrement.BranchID, 
	dbo.TIIncrement.Recorded, 
	dbo.TIIncrement.TotalAmountOriginal, 
	dbo.TIIncrement.TotalAmount, 
	'TIIncrement' AS RefTable
FROM dbo.TIIncrement INNER JOIN
	dbo.Type ON TIIncrement.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.TIDecrement.ID as RefID, 
	dbo.TIDecrement.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.TIDecrement.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.TIDecrement.Date, 
	dbo.TIDecrement.PostedDate, 
	NULL AS CurrencyID, 
	dbo.TIDecrement.Reason, 
	NULL AS EmployeeID, 
	dbo.TIDecrement.BranchID, 
	dbo.TIDecrement.Recorded, 
	0 AS TotalAmountOriginal, 
	dbo.TIDecrement.TotalAmount, 
	'TIDecrement' AS RefTable
FROM dbo.TIDecrement INNER JOIN
	dbo.Type ON TIDecrement.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.TIAudit.ID as RefID, 
	dbo.TIAudit.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.TIAudit.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.TIAudit.Date, 
	dbo.TIAudit.PostedDate, 
	NULL AS CurrencyID, 
	NULL AS Reason, 
	NULL AS EmployeeID, 
	dbo.TIAudit.BranchID, 
	0 AS Recorded, 
	0 AS TotalAmountOriginal, 
	0 AS TotalAmount, 
	'TIAudit' AS RefTable
FROM dbo.TIAudit INNER JOIN
	dbo.Type ON TIAudit.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.TITransfer.ID as RefID,
	dbo.TITransfer.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.TITransfer.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.TITransfer.Date, 
	dbo.TITransfer.PostedDate,
	NULL AS CurrencyID, 
	dbo.TITransfer.Reason, 
	NULL AS EmployeeID, 
	dbo.TITransfer.BranchID, 
	dbo.TITransfer.Recorded, 
	0 AS TotalAmountOriginal, 
	0 AS TotalAmount, 
	'TITransfer' AS RefTable
FROM dbo.TITransfer INNER JOIN
	dbo.Type ON TITransfer.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.RSAssemblyDismantlement_new.ID as RefID, 
	dbo.RSAssemblyDismantlement_new.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.RSAssemblyDismantlement_new.No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.RSAssemblyDismantlement_new.Date, 
	dbo.RSAssemblyDismantlement_new.Date AS PostedDate, 
	dbo.RSAssemblyDismantlement_new.CurrencyID, 
	dbo.RSAssemblyDismantlement_new.Reason, 
	NULL AS EmployeeID, 
	dbo.RSAssemblyDismantlement_new.BranchID, 
	dbo.RSAssemblyDismantlement_new.Recorded, 
	dbo.RSAssemblyDismantlement_new.AmountOriginal AS TotalAmountOriginal, 
	dbo.RSAssemblyDismantlement_new.Amount AS TotalAmount, 
	'RSAssemblyDismantlement_new' AS RefTable
FROM dbo.RSAssemblyDismantlement_new INNER JOIN
	dbo.Type ON RSAssemblyDismantlement_new.TypeID = dbo.Type.ID 
/* =======================================================================*/ 
UNION ALL
SELECT        
	newid() as ID,
	dbo.EMContract.ID as RefID, 
	dbo.EMContract.TypeID, 
	dbo.Type.TypeName,  
	dbo.Type.TypeGroupID,
	dbo.EMContract.Code AS No, 
	NULL AS InwardNo, 
	NULL AS OutwardNo, 
	dbo.EMContract.SignedDate AS Date, 
	dbo.EMContract.SignedDate AS PostedDate, 
	dbo.EMContract.CurrencyID, 
	dbo.EMContract.Reason, 
	NULL AS EmployeeID, 
	dbo.EMContract.BranchID, 
	0 as Recored, 
	dbo.EMContract.AmountOriginal AS TotalAmountOriginal, 
	dbo.EMContract.Amount AS TotalAmount, 
	'EMContract' AS RefTable
FROM dbo.EMContract INNER JOIN
	dbo.Type ON EMContract.TypeID = dbo.Type.ID
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_Accreditative]
    @BankCode nvarchar(25)
AS

    BEGIN
        SET NOCOUNT ON;
   		                         
		select distinct a.ID, a.No, a.Date, a.BankName, a.AccountingObjectAddress, h.BankAccount as AccountingObjectBankAccount, a.AccountingObjectBankName,
		a.AccountingObjectName, c.BankAccount, c.BankBranchName, d.Tel, e.ID as CurrencyName, Sum(a.TotalAllOriginal) as TotalAmount, a.Reason, h.BankBranchName as Branch, f.BankCode
		from MBTellerPaper a left join MBTellerPaperDetail b on a.ID = b.MBTellerPaperID
		left join BankAccountDetail c on a.BankAccountDetailID = c.ID 
		left join AccountingObject d on a.AccountingObjectID = d.ID
		left join Currency e on a.CurrencyID = e.ID 		
		left join Bank f on c.BankID = f.ID
		left join AccountingObjectBankAccount h on a.AccountingObjectBankAccount = h.ID
		where f.BankCode = @BankCode
		group by a.ID, a.No, a.Date, a.BankName, a.AccountingObjectAddress, h.BankAccount, h.BankBranchName, a.AccountingObjectBankName,
		a.AccountingObjectName, c.BankAccount, c.BankBranchName, d.Tel, e.ID, a.Reason, f.BankCode
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[Account]
  ADD [DetailByAccountObject] [int] NOT NULL DEFAULT ('0')
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'Account', 'COLUMN', N'DetailByAccountObject'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
CREATE FUNCTION [dbo].[Func_GetBalanceAccountF01_v1]
      (
        @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT ,
        @IsBalanceBothSide BIT
      )
RETURNS @Result TABLE
      (
        AccountID UNIQUEIDENTIFIER ,
        DetailByAccountObject BIT ,
        AccountObjectType INT ,
        AccountNumber NVARCHAR(50) ,
        AccountName NVARCHAR(255) ,
        AccountNameEnglish NVARCHAR(255) ,
        AccountCategoryKind INT ,
        IsParent BIT ,
        ParentID UNIQUEIDENTIFIER ,
        Grade INT ,
        AccountKind INT ,
        SortOrder INT ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
      )
AS
    BEGIN
	   DECLARE @MainCurrency NVARCHAR(3)
        SET @MainCurrency = 'VND'	
        /*ngày bắt đầu chương trình*/
        DECLARE @StartDate DATETIME
        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'
        IF @IsBalanceBothSide = 1
           BEGIN
                 DECLARE @GeneralLedger TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           AccountObjectID UNIQUEIDENTIFIER ,
                           BranchID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(3) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4) ,
                           OpenningDebitAmountOC DECIMAL(25, 4) ,
                           DebitAmountOC DECIMAL(25, 4) ,
                           CreditAmountOC DECIMAL(25, 4) ,
                           DebitAmountOCAccum DECIMAL(25, 4) ,
                           CreditAmountOCAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedger
                        SELECT  G.*
                        FROM    (
                                  SELECT    Account ,
                                            null as AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                                     ELSE 0
                                                END) AS OpenningDebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal - GL.CreditAmountOriginal
                                                     ELSE 0
                                                END) AS OpenningDebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOCAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOCAccum
                                  FROM      dbo.GeneralLedger AS GL
                                  WHERE    GL.PostedDate <= @ToDate
                                  GROUP BY  Account ,
                                            GL.BranchID ,
                                            GL.CurrencyID
                                ) G
			
                 DECLARE @Data TABLE
                         (
                           AccountNumber NVARCHAR(50) ,
						   AccountObjectID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(20) ,
                           SortOrder INT ,
                           OpeningDebitAmount DECIMAL(25, 4) ,
                           OpeningCreditAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,/*Nợ lũy kế*/
                           CreditAmountAccum DECIMAL(25, 4) ,/*có lũy kế*/
                           ClosingDebitAmount DECIMAL(25, 4) ,
                           ClosingCreditAmount DECIMAL(25, 4)
                         )
	
                 INSERT @Data
                        SELECT  F.AccountNumber ,
						        F.AccountObjectID,
                                '' ,
                                0 ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount > 0
                                                 ) THEN F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount < 0
                                                 ) THEN -1 * F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount ,
                                SUM(F.DebitAmount) AS DebitAmount ,
                                SUM(F.CreditAmount) AS CreditAmount ,
                                SUM(F.DebitAmountAccum) AS DebitAmountAccum ,
                                SUM(F.CreditAmountAccum) AS CreditAmountAccum ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                                 ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                                 ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.AccountNumber ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            GL.AccountObjectID AccountObjectID ,
                                            A.AccountGroupKind AccountCategoryKind ,
                                            GL.BranchID BranchID
                                  FROM      @GeneralLedger AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber = A.AccountNumber
                                  WHERE     GL.AccountNumber NOT LIKE '0%'
                                  GROUP BY  A.AccountNumber ,
                                            GL.AccountObjectID,
                                            A.AccountGroupKind ,
                                            GL.BranchID
                                  HAVING    SUM(OpenningDebitAmount) <> 0
                                            OR SUM(DebitAmount) <> 0
                                            OR SUM(CreditAmount) <> 0
                                ) AS F
                        GROUP BY F.AccountNumber ,
                                F.AccountCategoryKind,
								F.AccountObjectID
                 OPTION ( RECOMPILE )		         					    
                 INSERT @Result
                        SELECT  A.ID ,
						        null,
								null,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                0 AccountKind,
                                D.SortOrder ,
                                SUM(D.OpeningDebitAmount) ,
                                SUM(D.OpeningCreditAmount) ,
                                SUM(D.DebitAmount) ,
                                SUM(D.CreditAmount) ,
                                SUM(D.DebitAmountAccum) ,
                                SUM(D.CreditAmountAccum) ,
                                SUM(D.ClosingDebitAmount) ,
                                SUM(D.ClosingCreditAmount)
                        FROM    @Data D
                        INNER JOIN dbo.Account A ON D.AccountNumber LIKE A.AccountNumber + '%'
                        WHERE   A.Grade <= @MaxAccountGrade
                        GROUP BY A.ID ,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                D.SortOrder
                        HAVING  SUM(D.OpeningDebitAmount) <> 0
                                OR SUM(D.OpeningCreditAmount) <> 0
                                OR SUM(D.DebitAmount) <> 0
                                OR SUM(D.CreditAmount) <> 0
                                OR SUM(D.ClosingDebitAmount) <> 0
                                OR SUM(D.ClosingCreditAmount) <> 0
                 OPTION ( RECOMPILE )
           END
        ELSE
           BEGIN
                 DECLARE @GeneralLedgerP1 TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedgerP1
                        SELECT  Account ,
                                SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmountAccum ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmountAccum
                        FROM    dbo.GeneralLedger AS GL
                        WHERE   GL.PostedDate <= @ToDate
                        GROUP BY Account
                 INSERT @Result
                        SELECT  A.ID ,
                                null DetailByAccountObject ,
                                null AccountObjectType,
                                A.AccountNumber ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade AS Grade ,
                                F.AccountKind,
                                F.SortOrder ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount > 0
                                               ) THEN F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount < 0
                                               ) THEN -1 * F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount ,
                                ( F.DebitAmount ) AS DebitAmount ,
                                ( F.CreditAmount ) AS CreditAmount ,
                                ( F.DebitAmountAccum ) AS DebitAmountAccum ,
                                ( F.CreditAmountAccum ) AS CreditAmountAccum ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                               ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                               ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.ID ,
                                            null DetailByAccountObject,
                                            null AccountObjectType,
                                            A.AccountNumber ,
                                            '' AS CurrencyID ,
                                            0 AS SortOrder ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            0 AS AccountKind
                                  FROM      @GeneralLedgerP1 AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber LIKE A.AccountNumber + '%'
                                  WHERE     A.Grade <= @MaxAccountGrade
                                            AND (
                                                  OpenningDebitAmount <> 0
                                                  OR DebitAmount <> 0
                                                  OR CreditAmount <> 0
                                                )
                                  GROUP BY  A.ID ,
                                            A.AccountNumber      
                                ) AS F
                        INNER JOIN Account A ON F.AccountNumber = A.AccountNumber
                 OPTION ( RECOMPILE )      
           END
        RETURN
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER FUNCTION [dbo].[Func_GetBalanceAccountF01]
      (
        @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT ,
        @IsBalanceBothSide BIT
      )
RETURNS @Result TABLE
      (
        AccountID UNIQUEIDENTIFIER ,
        DetailByAccountObject BIT ,
        AccountObjectType INT ,
        AccountNumber NVARCHAR(50) ,
        AccountName NVARCHAR(255) ,
        AccountNameEnglish NVARCHAR(255) ,
        AccountCategoryKind INT ,
        IsParent BIT ,
        ParentID UNIQUEIDENTIFIER ,
        Grade INT ,
        AccountKind INT ,
        SortOrder INT ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
      )
AS
    BEGIN
	   DECLARE @MainCurrency NVARCHAR(3)
        SET @MainCurrency = 'VND'	
        /*ngày bắt đầu chương trình*/
        DECLARE @StartDate DATETIME
        SELECT TOP 1
                @StartDate = CONVERT(DATETIME, Data, 103)
        FROM    dbo.SystemOption
        WHERE   Code LIKE N'%ApplicationStartDate%'
        IF @IsBalanceBothSide = 1
           BEGIN
                 DECLARE @GeneralLedger TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           AccountObjectID UNIQUEIDENTIFIER ,
                           BranchID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(3) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4) ,
                           OpenningDebitAmountOC DECIMAL(25, 4) ,
                           DebitAmountOC DECIMAL(25, 4) ,
                           CreditAmountOC DECIMAL(25, 4) ,
                           DebitAmountOCAccum DECIMAL(25, 4) ,
                           CreditAmountOCAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedger
                        SELECT  G.*
                        FROM    (
                                  SELECT    Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                                     ELSE 0
                                                END) AS OpenningDebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmount ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                                     ELSE 0
                                                END) AS DebitAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                                     ELSE 0
                                                END) AS CreditAmountAccum ,
                                            SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmountOriginal - GL.CreditAmountOriginal
                                                     ELSE 0
                                                END) AS OpenningDebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOC ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmountOriginal )
                                                     ELSE 0
                                                END) AS DebitAmountOCAccum ,
                                            SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmountOriginal )
                                                     ELSE 0
                                                END) AS CreditAmountOCAccum
                                  FROM      dbo.GeneralLedger AS GL
                                  WHERE    GL.PostedDate <= @ToDate
                                  GROUP BY  Account ,
                                            AccountingObjectID ,
                                            GL.BranchID ,
                                            GL.CurrencyID
                                ) G
			
                 DECLARE @Data TABLE
                         (
                           AccountNumber NVARCHAR(50) ,
						   AccountObjectID UNIQUEIDENTIFIER ,
                           CurrencyID NVARCHAR(20) ,
                           SortOrder INT ,
                           OpeningDebitAmount DECIMAL(25, 4) ,
                           OpeningCreditAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,/*Nợ lũy kế*/
                           CreditAmountAccum DECIMAL(25, 4) ,/*có lũy kế*/
                           ClosingDebitAmount DECIMAL(25, 4) ,
                           ClosingCreditAmount DECIMAL(25, 4)
                         )
	
                 INSERT @Data
                        SELECT  F.AccountNumber ,
						        F.AccountObjectID,
                                '' ,
                                0 ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount > 0
                                                 ) THEN F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount < 0
                                                 ) THEN -1 * F.OpenningDebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount ,
                                SUM(F.DebitAmount) AS DebitAmount ,
                                SUM(F.CreditAmount) AS CreditAmount ,
                                SUM(F.DebitAmountAccum) AS DebitAmountAccum ,
                                SUM(F.CreditAmountAccum) AS CreditAmountAccum ,
                                SUM(CASE WHEN F.AccountCategoryKind = 0
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                                 ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN F.AccountCategoryKind = 1
                                              OR (
                                                   F.AccountCategoryKind NOT IN ( 0, 1 )
                                                   AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                                 ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                         ELSE 0
                                    END) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.AccountNumber ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            CASE A.DetailByAccountObject
                                                                  WHEN 0 THEN NULL
                                                                  ELSE GL.AccountObjectID
                                                                END AS AccountObjectID ,
                                            A.AccountGroupKind AccountCategoryKind ,
                                            GL.BranchID BranchID
                                  FROM      @GeneralLedger AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber = A.AccountNumber
                                  WHERE     GL.AccountNumber NOT LIKE '0%'
                                            AND A.IsParentNode = 0
                                  GROUP BY  A.AccountNumber ,
                                            A.AccountGroupKind ,
                                            GL.BranchID,
											CASE A.DetailByAccountObject
                                                                  WHEN 0 THEN NULL
                                                                  ELSE GL.AccountObjectID
                                                                END
                                  HAVING    SUM(OpenningDebitAmount) <> 0
                                            OR SUM(DebitAmount) <> 0
                                            OR SUM(CreditAmount) <> 0
                                ) AS F
                        GROUP BY F.AccountNumber ,
                                F.AccountCategoryKind,
								F.AccountObjectID
                 OPTION ( RECOMPILE )		         					    
                 INSERT @Result
                        SELECT  A.ID ,
						        null,
								null,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                0 AccountKind,
                                D.SortOrder ,
                                SUM(D.OpeningDebitAmount) ,
                                SUM(D.OpeningCreditAmount) ,
                                SUM(D.DebitAmount) ,
                                SUM(D.CreditAmount) ,
                                SUM(D.DebitAmountAccum) ,
                                SUM(D.CreditAmountAccum) ,
                                SUM(D.ClosingDebitAmount) ,
                                SUM(D.ClosingCreditAmount)
                        FROM    @Data D
                        INNER JOIN dbo.Account A ON D.AccountNumber LIKE A.AccountNumber + '%'
                        WHERE   A.Grade <= @MaxAccountGrade
                        GROUP BY A.ID ,
                                A.AccountNumber ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN D.CurrencyID <> '' THEN D.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade ,
                                D.SortOrder
                        HAVING  SUM(D.OpeningDebitAmount) <> 0
                                OR SUM(D.OpeningCreditAmount) <> 0
                                OR SUM(D.DebitAmount) <> 0
                                OR SUM(D.CreditAmount) <> 0
                                OR SUM(D.ClosingDebitAmount) <> 0
                                OR SUM(D.ClosingCreditAmount) <> 0
                 OPTION ( RECOMPILE )
           END
        ELSE
           BEGIN
                 DECLARE @GeneralLedgerP1 TABLE
                         (
                           AccountNumber NVARCHAR(20) ,
                           OpenningDebitAmount DECIMAL(25, 4) ,
                           DebitAmount DECIMAL(25, 4) ,
                           CreditAmount DECIMAL(25, 4) ,
                           DebitAmountAccum DECIMAL(25, 4) ,
                           CreditAmountAccum DECIMAL(25, 4)
                         )
                 INSERT INTO @GeneralLedgerP1
                        SELECT  Account ,
                                SUM(CASE WHEN GL.PostedDate < @FromDate THEN GL.DebitAmount - GL.CreditAmount
                                         ELSE 0
                                    END) AS OpenningDebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmount ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.DebitAmount )
                                         ELSE 0
                                    END) AS DebitAmountAccum ,
                                SUM(CASE WHEN GL.PostedDate BETWEEN @StartDate AND @ToDate THEN ( GL.CreditAmount )
                                         ELSE 0
                                    END) AS CreditAmountAccum
                        FROM    dbo.GeneralLedger AS GL
                        WHERE   GL.PostedDate <= @ToDate
                        GROUP BY Account
                 INSERT @Result
                        SELECT  A.ID ,
                                null DetailByAccountObject ,
                                null AccountObjectType,
                                A.AccountNumber ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountName
                                END ,
                                CASE WHEN F.CurrencyID <> '' THEN F.CurrencyID
                                     ELSE A.AccountNameGlobal
                                END ,
                                A.AccountGroupKind ,
                                A.IsParentNode ,
                                A.ParentID ,
                                A.Grade AS Grade ,
                                F.AccountKind,
                                F.SortOrder ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount > 0
                                               ) THEN F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount < 0
                                               ) THEN -1 * F.OpenningDebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount ,
                                ( F.DebitAmount ) AS DebitAmount ,
                                ( F.CreditAmount ) AS CreditAmount ,
                                ( F.DebitAmountAccum ) AS DebitAmountAccum ,
                                ( F.CreditAmountAccum ) AS CreditAmountAccum ,
                                ( CASE WHEN A.AccountGroupKind = 0
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount > 0
                                               ) THEN F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount
                                       ELSE 0
                                  END ) AS OpenningDebitAmount ,
                                ( CASE WHEN A.AccountGroupKind = 1
                                            OR (
                                                 A.AccountGroupKind NOT IN ( 0, 1 )
                                                 AND F.OpenningDebitAmount + F.DebitAmount - F.CreditAmount < 0
                                               ) THEN F.CreditAmount - F.OpenningDebitAmount - F.DebitAmount
                                       ELSE 0
                                  END ) AS OpenningCreditAmount
                        FROM    (
                                  SELECT    A.ID ,
                                            null DetailByAccountObject,
                                            null AccountObjectType,
                                            A.AccountNumber ,
                                            '' AS CurrencyID ,
                                            0 AS SortOrder ,
                                            SUM(OpenningDebitAmount) AS OpenningDebitAmount ,
                                            SUM(DebitAmount) AS DebitAmount ,
                                            SUM(CreditAmount) AS CreditAmount ,
                                            SUM(DebitAmountAccum) AS DebitAmountAccum ,
                                            SUM(CreditAmountAccum) AS CreditAmountAccum ,
                                            0 AS AccountKind
                                  FROM      @GeneralLedgerP1 AS GL
                                  INNER JOIN dbo.Account A ON GL.AccountNumber LIKE A.AccountNumber + '%'
                                  WHERE     A.Grade <= @MaxAccountGrade
                                            AND (
                                                  OpenningDebitAmount <> 0
                                                  OR DebitAmount <> 0
                                                  OR CreditAmount <> 0
                                                )
                                  GROUP BY  A.ID ,
                                            A.AccountNumber      
                                ) AS F
                        INNER JOIN Account A ON F.AccountNumber = A.AccountNumber
                 OPTION ( RECOMPILE )      
           END
        RETURN
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*

*/
ALTER PROCEDURE [dbo].[Proc_GetBalanceAccountF01]
      ( @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT,
		@IsBalanceBothSide BIT
		)
AS
BEGIN
 
SELECT t.AccountID ,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName,
			   sum(t.OpeningDebitAmount) OpeningDebitAmount,
			   sum(t.OpeningCreditAmount) OpeningCreditAmount,
			   sum(t.DebitAmount) DebitAmount,
			   sum(t.CreditAmount) CreditAmount,
			   sum(t.DebitAmountAccum) DebitAmountAccum,
			   sum(t.CreditAmountAccum) CreditAmountAccum,
			   sum(t.ClosingDebitAmount) ClosingDebitAmount,
			   sum(t.ClosingCreditAmount) ClosingCreditAmount
			   FROM [dbo].[Func_GetBalanceAccountF01] (
   @FromDate
  ,@ToDate
  ,@MaxAccountGrade
  ,@IsBalanceBothSide) t
  group by t.AccountID,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName
  order by t.AccountNumber
end

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*

*/
ALTER PROCEDURE [dbo].[Proc_GetB02_DN]
      (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @PrevFromDate DATETIME ,
      @PrevToDate DATETIME
    )
AS
BEGIN
  
SELECT * FROM [dbo].[Func_GetB02_DN] (
    @BranchID  ,
      @IncludeDependentBranch  ,
      @FromDate  ,
      @ToDate  ,
      @PrevFromDate  ,
      @PrevToDate )
order by ItemIndex
PRINT @PrevFromDate
	PRINT @PrevToDate 
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

CREATE PROCEDURE [dbo].[UpdateImport]

	@FromDate datetime,
	@ToDate	datetime,
	@CPPeriodID uniqueidentifier
AS

    BEGIN
        SET NOCOUNT ON;
		Begin Transaction

		declare @rsinwardoutward table
		(
			ID uniqueidentifier,
			TotalAmount money ,
			TotalAmountOriginal money ,
			TypeID int, 
			PostedDate datetime
		)
		Insert into @rsinwardoutward select rs.ID, rs.TotalAmount, rs.TotalAmountOriginal, rs.TypeID, rs.PostedDate from RSInwardOutward rs where rs.Recorded = 1
		declare @rsinwardoutwarddetail table
		(
			ID uniqueidentifier,
			MaterialGoodsID uniqueidentifier,
			UnitPriceOriginal money ,
			UnitPrice money ,
			Amount money ,
			AmountOriginal money ,
			RSInwardOutwardID uniqueidentifier,
			Quantity decimal(25,10) ,
			NumberRow int IDENTITY(1,1)
		)
		Insert into @rsinwardoutwarddetail select rsd.ID, rsd.MaterialGoodsID, rsd.UnitPriceOriginal, rsd.UnitPrice, rsd.Amount, rsd.AmountOriginal, rsd.RSInwardOutwardID, rsd.Quantity from RSInwardOutwardDetail rsd join @rsinwardoutward rs
		on rsd.RSInwardOutwardID = rs.ID where rs.TypeID = 400 and rs.PostedDate >= @FromDate and rs.PostedDate <= @ToDate and rsd.CreditAccount like '154%' and rsd.DebitAccount like '155%'
		and rsd.CostSetID in (Select CostSetID from CPPeriodDetail where CPPeriodID = @CPPeriodID)
		
		declare @result table
		(
			ID uniqueidentifier,
			MaterialGoodsID uniqueidentifier,
			UnitPrice money, 
			NumberRow int IDENTITY(1,1)
		)
		insert into @result select re.ID, re.MaterialGoodsID, re.UnitPrice from CPResult re where re.CPPeriodID = @CPPeriodID
		select * from @result

		declare @rsid table
		(
			ID uniqueidentifier
		)

		declare @repositoryledger table
		(
			ID uniqueidentifier,
			MaterialGoodsID uniqueidentifier,
			UnitPrice money ,
			IWAmount money ,
			IWAmountBalance money ,
			IWQuantity decimal(25,10) ,
			IWQuantityBalance decimal(25,10) ,
			CostSetID uniqueidentifier,
			NumberRow int IDENTITY(1,1)
		)
		Insert into @repositoryledger select ID, MaterialGoodsID, UnitPrice, IWAmount, IWAmountBalance, IWQuantity, IWQuantityBalance, CostSetID from RepositoryLedger
		where TypeID = 400 and PostedDate >= @FromDate and PostedDate <= @ToDate and CostSetID in (Select CostSetID from CPPeriodDetail where CPPeriodID = @CPPeriodID)

		declare @generalledgers table
		(
			ID uniqueidentifier,
			CreditAmount money ,
			CreditAmountOriginal money ,
			DebitAmount money ,
			DebitAmountOriginal money ,
			Account nvarchar(25),
			DetailID uniqueidentifier,
			MaterialGoodsID uniqueidentifier,
			NumberRow int IDENTITY(1,1)
		)
		Insert into @generalledgers select gl.ID, gl.CreditAmount, gl.CreditAmountOriginal, gl.DebitAmount, gl.DebitAmountOriginal, gl.Account, gl.DetailID, rsd.MaterialGoodsID from GeneralLedger gl join @rsinwardoutwarddetail rsd on gl.DetailID = rsd.ID

		declare @i int
		declare @numrow int
		declare @unitprice money 
		declare @materialgoodsid uniqueidentifier
		Set @numrow = (Select count(*) from @result)
		Set @i = 1
		
		if(@numrow > 0)
		Begin
			While(@i <= @numrow)
			Begin
				set @unitprice = (Select UnitPrice from @result where NumberRow = @i)
				set @materialgoodsid = (Select MaterialGoodsID from @result where NumberRow = @i)

				declare @y int
				declare @numrow1 int 
				declare @quantity decimal(25,10) 
				declare @id uniqueidentifier
				declare @rsinwardoutwardid uniqueidentifier
				set @y = 1
				set @numrow1 = (select count(*) from @rsinwardoutwarddetail)

				if(@numrow1 > 0)
				Begin
					While(@y <= @numrow1)
					Begin
						set @quantity = (Select Quantity from @rsinwardoutwarddetail where NumberRow = @y and MaterialGoodsID = @materialgoodsid)
						set @rsinwardoutwardid = (Select RSInwardOutwardID from @rsinwardoutwarddetail where NumberRow = @y and MaterialGoodsID = @materialgoodsid)
						set @id = (Select ID from @rsinwardoutwarddetail where NumberRow = @y and MaterialGoodsID = @materialgoodsid)

						if(@id is not null)
						Begin
							Update @rsinwardoutwarddetail set UnitPriceOriginal = @unitprice, UnitPrice = @unitprice, Amount = ROUND(@unitprice * @quantity, 0), AmountOriginal = ROUND(@unitprice * @quantity, 0)
							where ID = @id and MaterialGoodsID = @materialgoodsid
							Update RSInwardOutwardDetail set UnitPriceOriginal = @unitprice, UnitPrice = @unitprice, Amount = ROUND(@unitprice * @quantity, 0), AmountOriginal = ROUND(@unitprice * @quantity, 0)
							where ID = @id and MaterialGoodsID = @materialgoodsid
						End
						Insert into @rsid(ID) values(@rsinwardoutwardid)

						set @y = @y + 1
					End
				End

				declare @z int
				declare @numrow2 int 
				declare @iwquantity decimal(25,10) 
				declare @iwquantitybalance decimal(25,10) 
				declare @id1 uniqueidentifier
				set @y = 1
				set @numrow2 = (select count(*) from @repositoryledger)

				if(@numrow2 > 0)
				Begin
					While(@z <= @numrow2)
					Begin
						set @iwquantity = (Select IWQuantity from @repositoryledger where NumberRow = @z and MaterialGoodsID = @materialgoodsid)
						set @iwquantitybalance = (Select IWQuantityBalance from @repositoryledger where NumberRow = @z and MaterialGoodsID = @materialgoodsid)
						set @id1 = (Select ID from @repositoryledger where NumberRow = @z and MaterialGoodsID = @materialgoodsid)

						if(@id1 is not null)
						Begin
							Update @repositoryledger set UnitPrice = @unitprice, IWAmount = ROUND(@unitprice * @iwquantity, 0), IWAmountBalance = ROUND(@unitprice * @iwquantitybalance, 0)
							where NumberRow = @z and MaterialGoodsID = @materialgoodsid
							Update RepositoryLedger set UnitPrice = @unitprice, IWAmount = ROUND(@unitprice * @iwquantity, 0), IWAmountBalance = ROUND(@unitprice * @iwquantitybalance, 0)
							where ID = @id1 and MaterialGoodsID = @materialgoodsid
						End

						set @z = @z + 1
					End
				End		

				declare @x int
				declare @numrow3 int 
				declare @id2 uniqueidentifier
				declare @account nvarchar(25)
				declare @amount money
				set @x = 1
				set @numrow3 = (select count(*) from @generalledgers)

				if(@numrow3 > 0)
				Begin
					While(@x <= @numrow3)
					Begin						
						set @id2 = (Select ID from @generalledgers where NumberRow = @x and MaterialGoodsID = @materialgoodsid)
						set @account = (Select Account from @generalledgers where NumberRow = @x and MaterialGoodsID = @materialgoodsid)
						set @amount = (Select top 1 rsd.Amount from @rsinwardoutwarddetail rsd join @generalledgers gl on rsd.ID = gl.DetailID and gl.MaterialGoodsID = @materialgoodsid) 

						if(@id2 is not null)
						Begin
							if(@account like '154%')
							Begin
								Update GeneralLedger set CreditAmount = @amount, CreditAmountOriginal = @amount, DebitAmount = 0, DebitAmountOriginal = 0 where ID = @id2
							End
							else if(@account like '155%')
							Begin
								Update GeneralLedger set CreditAmount = 0, CreditAmountOriginal = 0, DebitAmount = @amount, DebitAmountOriginal = @amount where ID = @id2
							End
						End

						set @x = @x + 1
					End
				End

				set @i = @i + 1
			End
		End

		select * from @rsinwardoutwarddetail

		declare @rsids table
		(
			ID uniqueidentifier,
			NumberRow int IDENTITY(1,1)
		)
		Insert into @rsids select distinct ID from @rsid where ID is not null

		declare @w int
		declare @numrow4 int
		declare @totalAmount money
		declare @rsid1 uniqueidentifier
		set @w = 1
		set @numrow4 = (select count(*) from @rsids)
		if(@numrow4 > 0)
		Begin
			while(@w <= @numrow4)
			Begin
				set @rsid1 = (Select ID from @rsids where NumberRow = @w)
				set @totalAmount = (Select SUM(Amount) from RSInwardOutwardDetail where RSInwardOutwardID = @rsid1)

				Update RSInwardOutward set TotalAmount = @totalAmount, TotalAmountOriginal = @totalAmount where ID = @rsid1
				set @w = @w + 1
			End
		End

			Update CPPeriod set IsDelete = 0 where ID = @CPPeriodID
	
		Commit Transaction
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

CREATE PROCEDURE [dbo].[UpdateExport]

	@FromDate datetime,
	@ToDate	datetime,
	@CPPeriodID uniqueidentifier
AS

    BEGIN
        SET NOCOUNT ON;
		Begin Transaction

		declare @typeids table 
		(
			TypeId int
		)
		Insert into @typeids(TypeId) values(410), (411), (412), (415), (414), (320), (321), (322), (323), (324), (325), (420)

		declare @rsinwardoutward table
		(
			ID uniqueidentifier,
			TotalAmount money ,
			TotalAmountOriginal money ,
			TypeID int, 
			PostedDate datetime
		)
		Insert into @rsinwardoutward select rs.ID, rs.TotalAmount, rs.TotalAmountOriginal, rs.TypeID, rs.PostedDate from RSInwardOutward rs where rs.Recorded = 1

		declare @rsinwardoutwarddetail table
		(
			ID uniqueidentifier,
			MaterialGoodsID uniqueidentifier,
			UnitPriceOriginal money ,
			UnitPrice money ,
			Amount money ,
			AmountOriginal money ,
			RSInwardOutwardID uniqueidentifier,
			Quantity decimal(25,10),
			NumberRow int IDENTITY(1,1) 
		)
		Insert into @rsinwardoutwarddetail select rsd.ID, rsd.MaterialGoodsID, rsd.UnitPriceOriginal, rsd.UnitPrice, rsd.Amount, rsd.AmountOriginal, rsd.RSInwardOutwardID, rsd.Quantity from RSInwardOutwardDetail rsd join @rsinwardoutward rs
		on rsd.RSInwardOutwardID = rs.ID where rs.TypeID in (Select TypeId from @typeids) and rs.PostedDate >= @FromDate and rs.PostedDate <= @ToDate 

		declare @sainvoice table
		(
			ID uniqueidentifier, 
			TotalCapitalAmount money,
			TotalCapitalAmountOriginal money
		)
		Insert into @sainvoice select ID, TotalCapitalAmount, TotalCapitalAmountOriginal from SAInvoice where PostedDate >= @FromDate and PostedDate <= @ToDate

		declare @sainvoicedetail table
		(
			ID uniqueidentifier,
			MaterialGoodsID uniqueidentifier,
			OWPrice money ,
			OWPriceOriginal money ,
			OWAmount money ,
			OWAmountOriginal money ,
			SAInvoiceID uniqueidentifier,
			Quantity decimal(25,10),
			NumberRow int IDENTITY(1,1)
		)
		Insert into @sainvoicedetail select sad.ID, sad.MaterialGoodsID, sad.OWPrice, sad.OWPriceOriginal, sad.OWAmount, sad.OWAmountOriginal, sad.SAInvoiceID, sad.Quantity 
		from SAInvoiceDetail sad join @sainvoice sa on sad.SAInvoiceId = sa.ID

		declare @result table
		(
			ID uniqueidentifier,
			MaterialGoodsID uniqueidentifier,
			UnitPrice money , 
			NumberRow int IDENTITY(1,1)
		)
		insert into @result select re.ID, re.MaterialGoodsID, re.UnitPrice from CPResult re where re.CPPeriodID = @CPPeriodID

		declare @rsid table
		(
			ID uniqueidentifier
		)

		declare @said table
		(
			ID uniqueidentifier
		)

		declare @repositoryledger table
		(
			ID uniqueidentifier,
			MaterialGoodsID uniqueidentifier,
			UnitPrice money ,
			OWAmount money ,
			OWQuantity decimal(25,10) ,
			ReferenceID uniqueidentifier,
			CostSetID uniqueidentifier,
			NumberRow int IDENTITY(1,1)
		)
		Insert into @repositoryledger select ID, MaterialGoodsID, UnitPrice, OWAmount, OWQuantity, ReferenceID, CostSetID from RepositoryLedger
		where PostedDate >= @FromDate and PostedDate <= @ToDate and CostSetID in (Select CostSetID from CPPeriodDetail where CPPeriodID = @CPPeriodID)
		and TypeID in (Select TypeID from @typeids)

		declare @generalledgers table
		(
			ID uniqueidentifier,
			CreditAmount money ,
			CreditAmountOriginal money ,
			DebitAmount money ,
			DebitAmountOriginal money ,
			Account nvarchar(25),
			AccountCorresponding nvarchar(25), 
			ReferenceID uniqueidentifier,
			DetailID uniqueidentifier,
			NumberRow int IDENTITY(1,1)
		)
		Insert into @generalledgers select gl.ID, gl.CreditAmount, gl.CreditAmountOriginal, gl.DebitAmount, gl.DebitAmountOriginal, gl.Account, gl.AccountCorresponding,
		gl.ReferenceID, gl.DetailID from GeneralLedger gl join @repositoryledger rsd on gl.ReferenceID = rsd.ReferenceID

		declare @i int
		declare @numrow int
		declare @unitprice money 
		declare @materialgoodsid uniqueidentifier
		Set @numrow = (Select count(*) from @result)
		Set @i = 1
		
		if(@numrow > 0)
		Begin
			While(@i <= @numrow)
			Begin
				set @unitprice = (Select UnitPrice from @result where NumberRow = @i)
				set @materialgoodsid = (Select MaterialGoodsID from @result where NumberRow = @i)

				declare @y int
				declare @numrow1 int 
				declare @quantity decimal(25,10) 
				declare @id uniqueidentifier
				declare @rsinwardoutwardid uniqueidentifier
				set @y = 1
				set @numrow1 = (select count(*) from @rsinwardoutwarddetail)

				if(@numrow1 > 0)
				Begin
					While(@y <= @numrow1)
					Begin
						set @quantity = (Select Quantity from @rsinwardoutwarddetail where NumberRow = @y and MaterialGoodsID = @materialgoodsid)
						set @rsinwardoutwardid = (Select RSInwardOutwardID from @rsinwardoutwarddetail where NumberRow = @y and MaterialGoodsID = @materialgoodsid)
						set @id = (Select ID from @rsinwardoutwarddetail where NumberRow = @y and MaterialGoodsID = @materialgoodsid)

						if(@id is not null)
						Begin
							Update @rsinwardoutwarddetail set UnitPriceOriginal = @unitprice, UnitPrice = @unitprice, Amount = ROUND(@unitprice * @quantity, 0), AmountOriginal = ROUND(@unitprice * @quantity, 0)
							where ID = @id and MaterialGoodsID = @materialgoodsid
							Update RSInwardOutwardDetail set UnitPriceOriginal = @unitprice, UnitPrice = @unitprice, Amount = ROUND(@unitprice * @quantity, 0), AmountOriginal = ROUND(@unitprice * @quantity, 0)
							where ID = @id and MaterialGoodsID = @materialgoodsid
						End

						Insert into @rsid(ID) values(@rsinwardoutwardid)

						set @y = @y + 1
					End
				End
				
				declare @z int
				declare @numrow2 int 
				declare @quantity1 decimal(25,10) 
				declare @id1 uniqueidentifier
				declare @sainvoiceid uniqueidentifier
				set @z = 1
				set @numrow2 = (select count(*) from @sainvoicedetail)

				if(@numrow2 > 0)
				Begin
					While(@z <= @numrow2)
					Begin
						set @quantity = (Select Quantity from @sainvoicedetail where NumberRow = @z and MaterialGoodsID = @materialgoodsid)
						set @sainvoiceid = (Select SAInvoiceId from @sainvoicedetail where NumberRow = @z and MaterialGoodsID = @materialgoodsid)
						set @id1 = (Select ID from @sainvoicedetail where NumberRow = @z and MaterialGoodsID = @materialgoodsid)

						if(@id1 is not null)
						Begin
							Update @sainvoicedetail set OWPrice = @unitprice, OWPriceOriginal = @unitprice, OWAmount = ROUND(@unitprice * @quantity, 0), OWAmountOriginal = ROUND(@unitprice * @quantity, 0)
							where ID = @id1
							Update SAInvoiceDetail set OWPrice = @unitprice, OWPriceOriginal = @unitprice, OWAmount = ROUND(@unitprice * @quantity, 0), OWAmountOriginal = ROUND(@unitprice * @quantity, 0)
							where ID = @id1 
						End

						Insert into @said(ID) values(@sainvoiceid)

						set @z = @z + 1
					End
				End

				declare @x int
				declare @numrow3 int 
				declare @owquantity decimal(25,10) 
				declare @id2 uniqueidentifier
				set @x = 1
				set @numrow3 = (select count(*) from @repositoryledger)

				if(@numrow3 > 0)
				Begin
					While(@x <= @numrow3)
					Begin
						set @owquantity = (Select OWQuantity from @repositoryledger where NumberRow = @x and MaterialGoodsID = @materialgoodsid)
						set @id2 = (Select ID from @repositoryledger where NumberRow = @x and MaterialGoodsID = @materialgoodsid)

						if(@id2 is not null)
						Begin
							Update @repositoryledger set UnitPrice = @unitprice, OWAmount = ROUND(@unitprice * @owquantity, 0)
							where NumberRow = @x and MaterialGoodsID = @materialgoodsid
							Update RepositoryLedger set UnitPrice = @unitprice, OWAmount = ROUND(@unitprice * @owquantity, 0)
							where ID = @id2 and MaterialGoodsID = @materialgoodsid
						End 

						set @x = @x + 1
					End
				End

				declare @t int
				declare @numrow4 int 
				declare @id3 uniqueidentifier
				declare @account nvarchar(25)
				declare @accountcorresponding nvarchar(25)
				declare @owamount money
				declare @owamountoriginal money
				set @t = 1
				set @numrow4 = (select count(*) from @generalledgers)

				if(@numrow4 > 0)
				Begin
					While(@t <= @numrow4)
					Begin						
						set @id3 = (Select ID from @generalledgers where NumberRow = @t)
						set @account = (Select Account from @generalledgers where NumberRow = @t)
						set @accountcorresponding = (Select AccountCorresponding from @generalledgers where NumberRow = @t)
						set @owamount = (Select top 1 sad.OWAmount from @sainvoicedetail sad join @generalledgers gl on sad.ID = gl.DetailID) 
						set @owamountoriginal = (Select top 1 sad.OWAmountOriginal from @sainvoicedetail sad join @generalledgers gl on sad.ID = gl.DetailID)

						if(@account like '632%')
						Begin
							Update GeneralLedger set CreditAmount = 0, CreditAmountOriginal = 0, DebitAmount = @owamount, DebitAmountOriginal = @owamountoriginal where ID = @id3							
						End
						else if(@accountcorresponding like '632%')
						Begin
							Update GeneralLedger set CreditAmount = @owamount, CreditAmountOriginal = @owamountoriginal, DebitAmount = 0, DebitAmountOriginal = 0 where ID = @id3
						End

						set @t = @t + 1
					End
				End

				set @i = @i + 1
			End
		End

			declare @rsids table
			(
				ID uniqueidentifier,
				NumberRow int IDENTITY(1,1)
			)
			Insert into @rsids select distinct ID from @rsid where ID is not null

			declare @k int
			declare @numrow5 int
			declare @totalAmount money
			declare @rsid1 uniqueidentifier
			set @k = 1
			set @numrow5 = (select count(*) from @rsids)
			if(@numrow5 > 0)
			Begin
				while(@k <= @numrow5)
				Begin
					set @rsid1 = (Select ID from @rsids where NumberRow = @k)
					set @totalAmount = (Select SUM(Amount) from RSInwardOutwardDetail where RSInwardOutwardID = @rsid1)

					Update @rsinwardoutward set TotalAmount = @totalAmount, TotalAmountOriginal = @totalAmount where ID = @rsid1
					Update RSInwardOutward set TotalAmount = @totalAmount, TotalAmountOriginal = @totalAmount where ID = @rsid1

					set @k = @k + 1
				End
			End

			declare @rsinwardoutward1 table
			(
				ID uniqueidentifier,
				TotalAmount money ,
				TotalAmountOriginal money ,
				NumberRow int IDENTITY(1,1)
			)
			Insert into @rsinwardoutward1 select rs.ID, rs.TotalAmount, rs.TotalAmountOriginal from @rsinwardoutward rs 
			where rs.TypeID in (Select TypeID from @typeids) and rs.PostedDate >= @FromDate and rs.PostedDate <= @ToDate 

			declare @n int 
			declare @numrow7 int
			declare @numrow8 int
			declare @rsid2 uniqueidentifier
			set @n = 1
			set @numrow7 = (select count(*) from @rsinwardoutward1)
			if(@numrow7 > 0)
			Begin
				while(@n <= @numrow7)
				Begin
					set @rsid2 = (Select ID from @rsinwardoutward1 where NumberRow = @n)
					set @numrow8 = (select count(*) from RSInwardOutwardDetail where RSInwardOutwardID = @rsid2)
					if(@numrow8 > 0)
					Begin
						Delete from @rsinwardoutward1 where ID = @rsid2
					End

					set @n = @n + 1
				End
			End

			declare @saids table
			(
				ID uniqueidentifier,
				NumberRow int IDENTITY(1,1)
			)
			Insert into @saids select distinct ID from @said where ID is not null

			declare @m int
			declare @numrow6 int
			declare @totalAmount1 money
			declare @said1 uniqueidentifier
			set @m = 1
			set @numrow6 = (select count(*) from @saids)
			if(@numrow6 > 0)
			Begin
				while(@m <= @numrow6)
				Begin
					set @said1 = (Select ID from @saids where NumberRow = @m)
					set @totalAmount1 = (Select SUM(OWAmount) from SAInvoiceDetail where SAInvoiceID = @said1)

					Update SAInvoice set TotalCapitalAmount = @totalAmount1, TotalCapitalAmountOriginal = @totalAmount1 where ID = @said1
					Update RSInwardOutward set TotalAmount = @totalAmount1, TotalAmountOriginal = @totalAmount1 where ID in (Select ID from @rsinwardoutward1) and ID = @said1

					set @m = @m + 1
				End
			End

			Update CPPeriod set IsDelete = 0 where ID = @CPPeriodID

		Commit Transaction
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*





*/
ALTER PROCEDURE [dbo].[Proc_GL_GetBookDetailPaymentByAccountNumber]
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(MAX) ,
	@CurrencyId NVARCHAR(10) ,
    @GroupTheSameItem BIT
AS
    BEGIN

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1) PRIMARY KEY,
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              InvDate DATETIME ,
              InvNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255) ,
              AccountNumber NVARCHAR(25) ,
              CorrespondingAccountNumber NVARCHAR(25) ,
              DebitAmount DECIMAL(22,4) ,
              CreditAmount DECIMAL(22,4) ,
              ClosingDebitAmount DECIMAL(22,4) ,
              ClosingCreditAmount DECIMAL(22,4) ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              OrderType INT , 
              IsBold BIT ,
              BranchName NVARCHAR(128),
			  AccountGroupKind NVARCHAR(25) 
            )      
       
        
        DECLARE @tblAccountNumber TABLE
            (              
			  AccountNumber  NVARCHAR(25) ,  
              AccountNumberPercent NVARCHAR(25)   , 
			  AccountGroupKind NVARCHAR(25)           
            )
        INSERT  @tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountNumber + '%',
						A1.AccountGroupKind
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        IF @GroupTheSameItem = 1
            BEGIN
                INSERT  #Result					
				SELECT
					RefID ,
                    PostedDate ,
                    RefDate ,
                    RefNo ,
                    InvDate ,
                    InvNo ,
                    RefType ,
                    JournalMemo ,
                    AccountNumber,
                    CorrespondingAccountNumber ,
                    ISNULL(DebitAmount,0) ,
                    ISNULL(CreditAmount,0) ,
                    ISNULL(ClosingDebitAmount,0),
                    ISNULL(ClosingCreditAmount,0),
                    AccountingObjectCode,
                    AccountObjectName,
                    OrderType ,
                    IsBold ,
                    BranchName,
					AccountGroupKind	
                 FROM
					(SELECT  
								NULL as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư đầu kỳ' as JournalMemo ,
                                A.AccountNumber,
                                NULL as CorrespondingAccountNumber ,
								$0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
									 THEN SUM(GL.DebitAmount - GL.CreditAmount)
									 ELSE 0
								END AS ClosingDebitAmount ,
								CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
									 THEN SUM(GL.CreditAmount - GL.DebitAmount)
									 ELSE 0
								END AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                0 AS OrderType ,
                                1 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
								,A.AccountGroupKind
                            FROM    dbo.GeneralLedger AS GL
							INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber
							WHERE   ( GL.PostedDate < @FromDate )  
							and GL.CurrencyID = @CurrencyId                    
							GROUP BY 
								A.AccountNumber,A.AccountGroupKind
							HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Description,
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                SUM(GL.DebitAmount) AS DebitAmount ,
                                SUM(GL.CreditAmount) AS CreditAmount ,
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
                                null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 As isBold,
                                '' as BranchName
								,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 As EntryType
								,A.AccountGroupKind
                        FROM    dbo.GeneralLedger AS GL
                                INNER JOIN @tblAccountNumber A ON GL.Account LIKE A.AccountNumberPercent
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )    
						and GL.CurrencyID = @CurrencyId 
						and GL.AccountCorresponding <> ''      
                        GROUP BY GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                GL.Description ,
                                A.AccountNumber,
                                GL.AccountCorresponding,
								A.AccountGroupKind
                        HAVING  SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0                       
                      )T
                      ORDER BY 
						AccountNumber,
                        OrderType ,
                        postedDate ,
                        RefDate ,
                        RefNo      
                        ,SortOrder,DetailPostOrder,EntryType            
            END
            
        ELSE
            BEGIN             
                
                INSERT  #Result					
				SELECT
					RefID ,
                    PostedDate ,
                    RefDate ,
                    RefNo ,
                    InvDate ,
                    InvNo ,
                    RefType ,
                    JournalMemo ,
                    AccountNumber,
                    CorrespondingAccountNumber ,
                    DebitAmount ,
                    CreditAmount ,
                    ClosingDebitAmount ,
                    ClosingCreditAmount ,
                    AccountingObjectCode,
                    AccountObjectName,
                    OrderType ,
                    IsBold ,
                    BranchName,
					AccountGroupKind
                 FROM
					(SELECT  
								null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư đầu kỳ' as JournalMemo ,
                                A.AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
									 THEN SUM(GL.DebitAmount - GL.CreditAmount)
									 ELSE 0
								END AS ClosingDebitAmount ,
								CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
									 THEN SUM(GL.CreditAmount - GL.DebitAmount)
									 ELSE 0
								END AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                0 AS OrderType ,
                                1 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType
								,A.AccountGroupKind
                            FROM    dbo.GeneralLedger AS GL
							INNER JOIN @tblAccountNumber A ON GL.Account LIKE A.AccountNumberPercent
							WHERE   ( GL.PostedDate < @FromDate )   
							and GL.CurrencyID = @CurrencyId                    
							GROUP BY 
								A.AccountNumber,A.AccountGroupKind
							HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
						UNION ALL
                        SELECT  GL.ReferenceID ,
                                GL.PostedDate ,
                                GL.RefDate ,
                                GL.RefNo ,
                                gl.InvoiceDate ,
                                gl.InvoiceNo ,
                                GL.TypeID ,
                                CASE WHEN GL.DESCRIPTION IS NULL
                                          OR LTRIM(GL.Description) = ''
                                     THEN GL.Reason
                                     ELSE GL.Description
                                END AS JournalMemo ,
                                A.AccountNumber,
                                GL.AccountCorresponding ,
                                GL.DebitAmount ,
                                GL.CreditAmount ,
                                $0 AS ClosingDebitAmount ,
                                $0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                1 AS OrderType ,
                                0 ,
                                null BranchName
								,null
								,null
								,null
								,A.AccountGroupKind
                        FROM    dbo.GeneralLedger AS GL
                                INNER JOIN @tblAccountNumber A ON GL.Account LIKE A.AccountNumberPercent
                        WHERE   (GL.PostedDate BETWEEN @FromDate AND @ToDate )                              
                                AND ( GL.DebitAmount <> 0
                                      OR GL.CreditAmount <> 0
                                    )   	
									and GL.CurrencyID = @CurrencyId	
									and GL.AccountCorresponding <> ''                     
                      )T
                      ORDER BY
						AccountNumber,
                        OrderType ,
                        postedDate ,
                        RefDate ,
                        RefNo,SortOrder,DetailPostOrder,EntryType                                                          
               
            END         
       
			 

		DECLARE @AccountNumber1 NVARCHAR(25)

		DECLARE C1 CURSOR FOR  
		SELECT  F.Value
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
	    order by F.Value
		OPEN c1;  
		FETCH NEXT FROM c1 INTO @AccountNumber1  ;  
		WHILE @@FETCH_STATUS = 0  
		   BEGIN  
		      PRINT @AccountNumber1
			  DECLARE @AccountObjectCode NVARCHAR(25)
			 ,@ClosingDebitAmount DECIMAL(22,4)
			 
		      SELECT @ClosingDebitAmount = 0
			  UPDATE  #Result
			  SET     @ClosingDebitAmount = @ClosingDebitAmount + ISNULL(ClosingDebitAmount,0) - 
		                              ISNULL(ClosingCreditAmount,0) + ISNULL(DebitAmount,0) - ISNULL(CreditAmount,0),
		
                ClosingDebitAmount = CASE WHEN @ClosingDebitAmount > 0
                                          THEN @ClosingDebitAmount
                                          ELSE 0
                                     END ,
                ClosingCreditAmount = CASE WHEN @ClosingDebitAmount < 0
                                           THEN - @ClosingDebitAmount
                                           ELSE 0
                                      END 
               where AccountNumber = @AccountNumber1
			   FETCH NEXT FROM c1 INTO @AccountNumber1  ; 
			   
		   END;  
		CLOSE c1;  
		DEALLOCATE c1;  
		
        DECLARE @DebitAmount DECIMAL(22,4) 
		DECLARE @CreditAmount DECIMAL(22,4)  
		DECLARE @ClosingDeditAmount_SDDK DECIMAL(22,4)  
		DECLARE @ClosingCreditAmount_SDDK DECIMAL(22,4)  
	    select @DebitAmount = sum(DebitAmount),
		       @CreditAmount = sum(CreditAmount)
		from #Result Res
		INNER JOIN @tblAccountNumber A ON Res.AccountNumber LIKE A.AccountNumberPercent
		where OrderType = 1
		select @ClosingDeditAmount_SDDK = sum(ClosingDebitAmount),
		       @ClosingCreditAmount_SDDK = sum(ClosingCreditAmount)
		from #Result where OrderType = 0
        SELECT  RowNum ,
                RefID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountNumber,
                PostedDate ,
                RefDate ,
                RefNo ,
                InvDate ,
                InvNo ,
                RefType ,
                JournalMemo ,
                CorrespondingAccountNumber ,
                DebitAmount ,
                CreditAmount ,
                ClosingDebitAmount ,
                ClosingCreditAmount ,               
                OrderType AS InvoiceOrder ,
                IsBold ,
                BranchName,
				AccountGroupKind
        FROM    #Result
        ORDER BY RowNum
		select 
		null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư cuối kỳ' as JournalMemo ,
                                AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 DebitAmount ,
								$0 CreditAmount ,
								case 
								when A1.AccountGroupKind=0 or (A1.AccountGroupKind=2 and (A1.AccountNumber like '1%' or A1.AccountNumber like '2%' or A1.AccountNumber like '6%' 
								or A1.AccountNumber like '8%')) then
								sum(ClosingDebitAmount) + sum(sumDebitAmount) - sum(sumCreditAmount) else $0 
								end as ClosingDebitAmount,
								case 
								when A1.AccountGroupKind=1 or (A1.AccountGroupKind=2 and (A1.AccountNumber like '3%' or A1.AccountNumber like '4%' or A1.AccountNumber like '5%' 
								or A1.AccountNumber like '7%')) then
								sum(ClosingCreditAmount) + sum(sumCreditAmount) - sum(sumDebitAmount) else $0 
								end AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                2 AS OrderType ,
                                2 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType,
								A1.AccountGroupKind
        from (SELECT  
								null as RefID ,
                                NULL as PostedDate ,
                                NULL as RefDate ,
                                NULL as RefNo ,
                                NULL as InvDate ,
                                NULL as InvNo ,
                                NULL as RefType ,
                                N'Số dư cuối kỳ' as JournalMemo ,
                                A.AccountNumber,
                                null as CorrespondingAccountNumber ,
                                $0 AS DebitAmount ,
								$0 AS CreditAmount ,
								CASE WHEN GL.OrderType = 0 and gl.IsBold = 1 THEN sum(ClosingDebitAmount)
									 ELSE 0 
								END
								AS ClosingDebitAmount,
								sum(DebitAmount) sumDebitAmount,
								sum(CreditAmount) sumCreditAmount,
								0 AS ClosingCreditAmount ,
								null as AccountingObjectCode,
                                null as AccountObjectName,
                                2 AS OrderType ,
                                2 As isBold,
                                null as BranchName
                                ,0 as sortOrder
                                ,0 as DetailPostOrder
                                ,0 as EntryType,
								A.AccountGroupKind
                            FROM    #Result AS GL
							INNER JOIN @tblAccountNumber A ON GL.AccountNumber = A.AccountNumber
							GROUP BY OrderType,IsBold,
								A.AccountNumber,A.AccountGroupKind) as A1
        group by AccountNumber,AccountGroupKind
        DROP TABLE #Result 
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_FU_GetCashDetailBook]
    (
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @AccountNumber NVARCHAR(20) ,
      @BranchID UNIQUEIDENTIFIER ,
      @CurrencyID NVARCHAR(3)
    )
AS 
    BEGIN

        CREATE  TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
			  AccountNumber NVARCHAR(15),
              PostedDate DATETIME ,
              RefDate DATETIME ,
              ReceiptRefNo NVARCHAR(25) ,
              PaymentRefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255) ,
              CorrespondingAccountNumber NVARCHAR(20) ,
              ExchangeRate Numeric ,
              DebitAmount Numeric ,
              DebitAmountOC Numeric ,
              CreditAmount Numeric ,
              CreditAmountOC Numeric ,
              ClosingAmount Numeric ,
              ClosingAmountOC Numeric ,
              ContactName NVARCHAR(255) ,
			  AccountObjectId NVARCHAR(255),
              AccountObjectCode NVARCHAR(255) ,
              AccountObjectName NVARCHAR(255) , 
              EmployeeCode NVARCHAR(255) ,
              EmployeeName NVARCHAR(255) , 
              InvoiceOrder INT , 
              BranchID UNIQUEIDENTIFIER ,
              BranchName NVARCHAR(255) ,
              IsBold BIT
            )
         DECLARE @tblAccountNumber TABLE
            (              
			  AccountNumber  NVARCHAR(25),  
              AccountNumberPercent NVARCHAR(25)              
            )
        INSERT  @tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountNumber + '%'
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        DECLARE @AccountNumberPercent NVARCHAR(25)
        SET @AccountNumberPercent = @AccountNumber + '%'
        
        DECLARE @DiscountNumber NVARCHAR(25)       
        SET @DiscountNumber = '5211%'
   
        DECLARE @ClosingAmountOC MONEY
        DECLARE @ClosingAmount MONEY
   
 INSERT  #Result
	          (RefID ,
			  AccountNumber,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  CorrespondingAccountNumber)
			  select ReferenceID ,
			  Account,
              PostedDate ,
              RefDate ,
              ReceiptRefNo  ,
              PaymentRefNo,
              RefType ,
              JournalMemo,
              ExchangeRate ,
              DebitAmount  ,
              DebitAmountOC  ,
              CreditAmount  ,
              CreditAmountOC  ,
              ClosingAmount  ,
              ClosingAmountOC  ,
              ContactName,
              AccountObjectId,
              InvoiceOrder  , 
              BranchID  ,
              BranchName,
			  AccountCorresponding from 
			            (SELECT null as ReferenceID,
						    Acc.AccountNumber as Account,
							null as PostedDate,
							null as RefDate,
							null as ReceiptRefNo,
							null as PaymentRefNo,
							null as RefType,
					        N'Số tồn đầu kỳ' AS JournalMemo ,
                            $0 AS ExchangeRate ,
                            $0 AS DebitAmount ,
                            $0 AS DebitAmountOC ,
                            $0 AS CreditAmount ,
                            $0 AS CreditAmountOC ,
							SUM(GL.DebitAmount - GL.CreditAmount) as ClosingAmount,
							SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) as ClosingAmountOC,
							null as ContactName,
							null as AccountObjectId,
                            0 AS InvoiceOrder ,
                            null BranchID ,
                            0 as BranchName,
                            null AccountCorresponding
							 from GeneralLedger gl
							 inner join @tblAccountNumber Acc on GL.Account = Acc.AccountNumber
							 where ( GL.PostedDate < @FromDate )
								AND ( @CurrencyID IS NULL
								OR GL.CurrencyID = @CurrencyID)
						    group by Acc.AccountNumber
				        union all
                        SELECT  GL.ReferenceID,
						        GL.Account,
                                GL.PostedDate ,
                                GL.RefDate ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS ReceiptRefNo ,
                                CASE WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0
                                     THEN GL.RefNo
                                     ELSE ''
                                END AS PaymentRefNo ,
                                GL.TypeID ,
								case when GL.Reason is null then
                                GL.Description 
								else GL.Reason 
								end as Description, 
                                GL.ExchangeRate ,
                                GL.DebitAmount ,
                                GL.DebitAmountOriginal ,
                                GL.CreditAmount ,
                                GL.CreditAmountOriginal ,
                                $0 AS ClosingAmount ,
                                $0 AS ClosingAmountOC ,
                                GL.ContactName ,
                                GL.AccountingObjectID ,
                                CASE WHEN GL.DebitAmount <> 0
                                          OR GL.DebitAmountOriginal <> 0 THEN 1
                                     WHEN GL.CreditAmount <> 0
                                          OR GL.CreditAmountOriginal <> 0 THEN 2
                                     ELSE 3
                                END AS InvoiceOrder ,
                                GL.BranchID ,
                                0,
                               GL.AccountCorresponding
                        FROM    dbo.GeneralLedger AS GL ,
						        @tblAccountNumber Acc
                        WHERE   ( GL.PostedDate BETWEEN @FromDate AND @ToDate )
                                AND GL.Account = Acc.AccountNumber
                                AND ( @CurrencyID IS NULL
                                      OR GL.CurrencyID = @CurrencyID
                                    )
                                AND ( GL.CreditAmount <> 0
                                      OR GL.DebitAmount <> 0
                                      OR GL.CreditAmountOriginal <> 0
                                      OR GL.DebitAmountOriginal <> 0
                                    )) T
                        ORDER BY Account,
						        PostedDate ,
                                RefDate ,
                                InvoiceOrder 

		
        SET @ClosingAmount = 0
        SET @ClosingAmountOC = 0
        UPDATE  #Result
        SET     @ClosingAmount = @ClosingAmount  + ClosingAmount +ISNULL(DebitAmount, 0)
                - ISNULL(CreditAmount, 0) ,
                ClosingAmount = @ClosingAmount ,
                @ClosingAmountOC = @ClosingAmountOC + ClosingAmountOC + ISNULL(DebitAmountOC, 0)
                - ISNULL(CreditAmountOC, 0) ,
                ClosingAmountOC = @ClosingAmountOC 
        SELECT  *
        FROM    #Result R   
		order by rownum
        DROP TABLE #Result 
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
CREATE PROCEDURE [dbo].[Proc_PO_SummaryPayable]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS
    BEGIN
	    DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              AccountObjectAddress NVARCHAR(255) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        null AccountingObjectGroupCode ,
                        null AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
      
                 
		
		DECLARE @StartDateOfPeriod DATETIME
		SET @StartDateOfPeriod = '1/1/'+CAST(Year(@FromDate) AS NVARCHAR(5))
                         
           
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(255) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
	    
        SELECT  ROW_NUMBER() OVER ( ORDER BY AccountObjectCode ) AS RowNum ,
                AccountingObjectID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountObjectAddress ,
                AccountObjectTaxCode ,
                AccountNumber ,
                AccountCategoryKind ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC)
                            - SUM(OpenningCreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) ) > 0
                                 THEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN ( SUM(OpenningDebitAmount - OpenningCreditAmount) )
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmount
                                            - OpenningCreditAmount) ) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount)
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmountOC
                                  - OpenningDebitAmountOC) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) ) > 0
                                 THEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmount - OpenningDebitAmount) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) ) > 0
                                 THEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmount ,
                SUM(DebitAmountOC) AS DebitAmountOC ,
                SUM(DebitAmount) AS DebitAmount ,
                SUM(CreditAmountOC) AS CreditAmountOC ,
                SUM(CreditAmount) AS CreditAmount ,
                SUM(AccumDebitAmountOC) AS AccumDebitAmountOC ,
                SUM(AccumDebitAmount) AS AccumDebitAmount ,
                SUM(AccumCreditAmountOC) AS AccumCreditAmountOC ,
                SUM(AccumCreditAmount) AS AccumCreditAmount ,
                /* Số dư cuối kỳ = Dư Có đầu kỳ - Dư Nợ đầu kỳ + Phát sinh Có – Phát sinh Nợ
				Nếu Số dư cuối kỳ >0 thì hiển bên cột Dư Có cuối kỳ 
				Nếu số dư cuối kỳ <0 thì hiển thị bên cột Dư Nợ cuối kỳ */
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC
                                + DebitAmountOC - CreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC) > 0
                                 THEN $0
                                 ELSE SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC)
                            END
                  END ) AS CloseDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC
                                + CreditAmountOC - DebitAmountOC)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC
                                            + CreditAmountOC - DebitAmountOC) ) > 0
                                 THEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                                + DebitAmount - CreditAmount)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount) > 0 THEN $0
                                 ELSE SUM(OpenningDebitAmount
                                          - OpenningCreditAmount + DebitAmount
                                          - CreditAmount)
                            END
                  END ) AS ClosingDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                                + CreditAmount - DebitAmount)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount
                                            + CreditAmount - DebitAmount) ) > 0
                                 THEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)
                                 ELSE $0
                            END
                  END ) AS ClosingCreditAmount ,
                AccountObjectGroupListCode ,
                AccountObjectGroupListName
        FROM    ( SELECT    AOL.AccountingObjectID ,
                            LAOI.AccountObjectCode ,
                            LAOI.AccountObjectName ,
                            LAOI.AccountObjectAddress,
                            LAOI.AccountObjectTaxCode ,
                            TBAN.AccountNumber ,
                            TBAN.AccountCategoryKind ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmount
                                 ELSE $0
                            END AS OpenningDebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmount
                                 ELSE $0
                            END AS OpenningCreditAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmountOriginal
                            END AS DebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmount
                            END AS DebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmountOriginal
                            END AS CreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmount
                            END AS CreditAmount ,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmountOriginal ELSE 0 END AS AccumDebitAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmount ELSE 0 END AS AccumDebitAmount,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmountOriginal ELSE 0 END AS AccumCreditAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmount ELSE 0 END AS AccumCreditAmount,
                            LAOI.AccountObjectGroupListCode ,
                            LAOI.AccountObjectGroupListName
                  FROM      dbo.GeneralLedger AS AOL
                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                  WHERE     AOL.PostedDate <= @ToDate
                            AND ( @CurrencyID IS NULL
                                  OR AOL.CurrencyID = @CurrencyID
                                )
                ) AS RSNS
        GROUP BY RSNS.AccountingObjectID ,
                RSNS.AccountObjectCode ,
                RSNS.AccountObjectName ,
                RSNS.AccountObjectAddress ,
                RSNS.AccountObjectTaxCode ,
                RSNS.AccountNumber ,
                RSNS.AccountCategoryKind ,
                RSNS.AccountObjectGroupListCode ,
                RSNS.AccountObjectGroupListName 
        HAVING 
				SUM(DebitAmountOC) <> 0
                OR SUM(DebitAmount) <> 0
                OR SUM(CreditAmountOC) <> 0
                OR SUM(CreditAmount) <> 0
                OR SUM(OpenningDebitAmount - OpenningCreditAmount) <> 0 
                OR SUM(OpenningDebitAmountOC - OpenningCreditAmountOC) <> 0 
				OR ( 
					(SUM(AccumDebitAmountOC) <> 0
					OR SUM(AccumDebitAmount) <> 0
					OR SUM(AccumCreditAmountOC) <> 0
					OR SUM(AccumCreditAmount) <> 0) )
        ORDER BY RSNS.AccountObjectCode
        OPTION (RECOMPILE)
		
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER procedure [dbo].[Proc_SA_GetDetailPayS12] 
(@fromdate DATETIME,
 @todate DATETIME,
 @account nvarchar(10),
 @currency nvarchar(3),
 @AccountObjectID AS NVARCHAR(MAX))
as
begin
DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(15),
			  AccountGroupKind int
            )
INSERT  INTO @tblAccountNumber
                SELECT  AO.AccountNumber,
				        AO.AccountGroupKind
                FROM    Account AS AO
                        inner JOIN dbo.Func_ConvertStringIntoTable_Nvarchar(@account,
                                                              ',') AS TLAO ON AO.AccountNumber = TLAO.Value
DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectGroupListCode NVARCHAR(MAX) ,
              AccountObjectCategoryName NVARCHAR(MAX)
            ) 
             
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID ,
                        AO.AccountObjectGroupID ,
						null
                FROM    AccountingObject AS AO
                        inner JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                              ',') AS TLAO ON AO.ID = TLAO.Value
                        
CREATE TABLE #tblResult
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              AccountNumber NVARCHAR(20)  ,
			  AccountGroupKind int, 
              AccountObjectID UNIQUEIDENTIFIER ,
			  RefDate DATETIME , 
			  RefNo NVARCHAR(25)  ,
			  PostedDate DATETIME ,
			  JournalMemo NVARCHAR(255)  ,
			  CorrespondingAccountNumber NVARCHAR(20)  , 
			  DueDate DATETIME,
			  DebitAmount MONEY , 
              CreditAmount MONEY , 
			  ClosingDebitAmount MONEY , 
              ClosingCreditAmount MONEY,
			  OrderType int, 
            )

if @account = '331'
insert into #tblResult
SELECT  a.AccountNumber,
        A.AccountGroupKind,
        LAOI.AccountObjectID,
		null as NgayThangGS,
		null as Sohieu, 
		null as ngayCTu,
        N'Số dư đầu kỳ' as Diengiai ,
		null as TKDoiUng, 
		null as ThoiHanDuocCKhau, 
        $0 AS PSNO ,
		$0 AS PSCO ,
		CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
				THEN SUM(GL.DebitAmount - GL.CreditAmount)
				ELSE 0
		END AS DuNo ,
		CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
				THEN SUM(GL.CreditAmount - GL.DebitAmount)
				ELSE 0
		END AS DuCo,
		0 AS OrderType
    FROM    dbo.GeneralLedger AS GL
	INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber 
	INNER JOIN @tblListAccountObjectID AS LAOI ON GL.AccountingObjectID = LAOI.AccountObjectID
	WHERE   ( GL.PostedDate < @FromDate )  
	and GL.CurrencyID = @currency                    
	GROUP BY 
		A.AccountNumber,LAOI.AccountObjectID,A.AccountGroupKind
	HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
UNION ALL
select  an.AccountNumber,
        An.AccountGroupKind,
        LAOI.AccountObjectID,
        a.PostedDate as NgayThangGS, 
		a.No as Sohieu, 
		a.Date as ngayCTu,
		a.Description as Diengiai, 
		a.AccountCorresponding as TKDoiUng, 
		DueDate as ThoiHanDuocCKhau, 
		sum(a.DebitAmount) as PSNO, 
		sum(a.CreditAmount) as PSCO,
		sum(fnsd.DebitAmount) DuNo,
		sum(fnsd.CreditAmount) DuCo,
		1 AS OrderType
 from GeneralLedger a									
INNER join PPInvoice c on c.id = a.ReferenceID
inner join @tblAccountNumber as an on a.Account = an.AccountNumber
INNER JOIN @tblListAccountObjectID AS LAOI ON a.AccountingObjectID = LAOI.AccountObjectID
INNER JOIN Func_SoDu (
   @fromdate
  ,@todate
  ,1) AS fnsd ON LAOI.AccountObjectID = fnsd.AccountObjectID	and an.AccountNumber = fnsd.AccountNumber			
where a.PostedDate between @fromdate and @todate
and  a.CurrencyID = @currency  
group by an.AccountNumber,
        LAOI.AccountObjectID,
		a.PostedDate,
		a.No , 
		a.Date ,
		a.Description, 
		a.AccountCorresponding, 
		DueDate,
		An.AccountGroupKind
order by AccountNumber,NgayThangGS	
else
insert into #tblResult
    SELECT  a.AccountNumber,
	    A.AccountGroupKind,
        LAOI.AccountObjectID,
		null as NgayThangGS,
		null as Sohieu, 
		null as ngayCTu,
        N'Số dư đầu kỳ' as Diengiai ,
		null as TKDoiUng, 
		null as ThoiHanDuocCKhau, 
        $0 AS PSNO ,
		$0 AS PSCO ,
		CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
				THEN SUM(GL.DebitAmount - GL.CreditAmount)
				ELSE 0
		END AS DuNo ,
		CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
				THEN SUM(GL.CreditAmount - GL.DebitAmount)
				ELSE 0
		END AS DuCo,
		0 AS OrderType 
    FROM    dbo.GeneralLedger AS GL
	INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber 
	INNER JOIN @tblListAccountObjectID AS LAOI ON GL.AccountingObjectID = LAOI.AccountObjectID
	WHERE   ( GL.PostedDate < @FromDate )  
	and GL.CurrencyID = @currency                    
	GROUP BY 
		A.AccountNumber,LAOI.AccountObjectID,A.AccountGroupKind
	HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
UNION ALL
	select  an.AccountNumber,
	        An.AccountGroupKind,
			LAOI.AccountObjectID,
			a.PostedDate as NgayThangGS, 
			a.No as Sohieu, 
			a.Date as ngayCTu,
			a.Description as Diengiai, 
			a.AccountCorresponding as TKDoiUng, 
			DueDate as ThoiHanDuocCKhau, 
			sum(a.DebitAmount) as PSNO, 
			sum(a.CreditAmount) as PSCO,
			sum(fnsd.DebitAmount) DuNo,
			sum(fnsd.CreditAmount) DuCo,
			1 AS OrderType
	 from GeneralLedger a	
	inner join SAInvoice c on c.id = a.ReferenceID 								
	inner join @tblAccountNumber as an on a.Account = an.AccountNumber
	INNER JOIN @tblListAccountObjectID AS LAOI ON a.AccountingObjectID = LAOI.AccountObjectID
	INNER JOIN Func_SoDu (
	   @fromdate
	  ,@todate
	  ,1) AS fnsd ON LAOI.AccountObjectID = fnsd.AccountObjectID	and an.AccountNumber = fnsd.AccountNumber			
	where a.PostedDate between @fromdate and @todate
	and  a.CurrencyID = @currency  
	group by an.AccountNumber,
			LAOI.AccountObjectID,
			a.PostedDate,
			a.No , 
			a.Date ,
			a.Description, 
			a.AccountCorresponding, 
			DueDate,An.AccountGroupKind
	order by AccountNumber,AccountObjectID,NgayThangGS
DECLARE @CloseAmount AS DECIMAL(22, 8) ,
        @AccountObjectCode_tmp NVARCHAR(100) ,
        @AccountNumber_tmp NVARCHAR(20)
SELECT  @CloseAmount = 0 ,
        @AccountObjectCode_tmp = N'' ,
        @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     
                 @CloseAmount = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmount = 0 THEN ClosingCreditAmount
                                                                     ELSE -1 * ClosingDebitAmount
                                                                END )
                                      WHEN @AccountNumber_tmp <> AccountNumber THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountGroupKind = 0 THEN -1 * @CloseAmount
                                            WHEN AccountGroupKind = 1 THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0 THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountGroupKind = 1 THEN @CloseAmount
                                             WHEN AccountGroupKind = 0 THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0 THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
				@AccountNumber_tmp = AccountNumber							
select AccountNumber,
			AccountObjectID,
			RefDate as NgayThangGS, 
			RefNo as Sohieu, 
			PostedDate as ngayCTu,
			JournalMemo as Diengiai, 
			CorrespondingAccountNumber as TKDoiUng, 
			DueDate as ThoiHanDuocCKhau, 
			DebitAmount as PSNO, 
			CreditAmount as PSCO,
			ClosingDebitAmount as DuNo,
			ClosingCreditAmount as DuCo
from #tblResult 
order by RowNum
end				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*

*/
ALTER PROCEDURE [dbo].[Proc_PO_PayDetailNCC]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS 
    BEGIN     
        DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              AccountObjectAddress NVARCHAR(255) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        null AccountingObjectGroupCode ,
                        null AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
      
        CREATE TABLE #tblResult
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100)  ,
              AccountObjectName NVARCHAR(255)  ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL ,             
              AccountObjectAddress NVARCHAR(255)  ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25)  ,
              DueDate DATETIME , 
              InvDate DATETIME ,
              InvNo NVARCHAR(25)  ,

              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              JournalMemo NVARCHAR(255)  , 
              AccountNumber NVARCHAR(20)  , 
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20)  , 
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,	
              DebitAmount MONEY , 
              CreditAmountOC MONEY , 
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY ,
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,	
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50)  ,
              InventoryItemName NVARCHAR(255)  ,
              UnitName NVARCHAR(20)  ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(18, 4) , 
              BranchName NVARCHAR(128)  ,
              IsBold BIT ,	
              OrderType INT ,
              GLJournalMemo NVARCHAR(255)  ,
              DocumentIncluded NVARCHAR(255)  ,
              CustomField1 NVARCHAR(255)  ,
              CustomField2 NVARCHAR(255)  ,
              CustomField3 NVARCHAR(255)  ,
              CustomField4 NVARCHAR(255)  ,
              CustomField5 NVARCHAR(255)  ,
              CustomField6 NVARCHAR(255)  ,
              CustomField7 NVARCHAR(255)  ,
              CustomField8 NVARCHAR(255)  ,
              CustomField9 NVARCHAR(255)  ,
              CustomField10 NVARCHAR(255) ,
               MainUnitName NVARCHAR(20)  ,
              MainQuantity DECIMAL(22, 8) ,
              MainUnitPrice DECIMAL(18, 4),
			  OrderPriority int  
              
            )
        
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(255) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
            
                
/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode , 
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,      
                          AccountObjectAddress , 
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID , 
                          JournalMemo , 				  
                          AccountNumber , 
                          AccountCategoryKind ,
                          CorrespondingAccountNumber ,	  			  
                          DebitAmountOC ,	
                          DebitAmount , 
                          CreditAmountOC ,
                          CreditAmount ,
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount ,
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount,
						  OrderType ,
						  OrderPriority  
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,
                                AccountObjectName ,	
                                AccountObjectGroupListCode , 
                                AccountObjectGroupListName ,            
                                AccountObjectAddress , 
                                RSNS.PostedDate ,
                                NgayCtu ,
                                SoCtu ,
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RSNS.RefID , 
                                JournalMemo ,			  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  			  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) , 
                                SUM(CreditAmount) , 
                                ( CASE WHEN AccountCategoryKind = 0 THEN SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) ) > 0 THEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,      
                                ( CASE WHEN AccountCategoryKind = 0 THEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) ) > 0 THEN SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) ) > 0 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) ) > 0 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount,  
								  OrderType,
								  OrderPriority
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,
                                            AccountObjectGroupListCode ,
                                            AccountObjectGroupListName ,         
                                            LAOI.AccountObjectAddress ,
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,
											CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.Date
                                            END AS NgayCtu ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.No
                                            END AS SoCtu ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType , 
                                            CASE WHEN AOL.PostedDate < @FromDate 
												      THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN N'Số dư đầu kỳ'
                                                 ELSE AOL.Description
                                            END AS JournalMemo , 
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                             
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                  
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount,
											CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType,
											OrderPriority 
                                  FROM      dbo.GeneralLedger AS AOL
                                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                                         
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )
                                ) AS RSNS                               
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode ,  
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,
                                RSNS.AccountObjectAddress ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu , 
                                RSNS.SoCtu , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,
                                RSNS.JournalMemo , 		  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber,
								OrderType ,
								OrderPriority		   
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount - ClosingCreditAmount) <> 0
                        ORDER BY 
						        RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu ,
                                RSNS.SoCtu,
								OrderPriority
                OPTION  ( RECOMPILE )                           
/* Tính số tồn */
        DECLARE @CloseAmountOC AS DECIMAL(22, 8) ,
            @CloseAmount AS DECIMAL(22, 8) ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmountOC = 0 THEN ClosingCreditAmountOC
                                                                       ELSE -1 * ClosingDebitAmountOC
                                                                  END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1 THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0 THEN -1 * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0 THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0 THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmount = 0 THEN ClosingCreditAmount
                                                                     ELSE -1 * ClosingDebitAmount
                                                                END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1 THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0 THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0 THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0 THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
        SELECT  RS.PostedDate as Ngay_HT, 
		        RS.RefDate as ngayCtu,
				RS.RefNo as SoCtu,
				Rs.JournalMemo as DienGiai,
				'331' as TK_CONGNO,
				Rs.CorrespondingAccountNumber as TkDoiUng,
                RS.AccountObjectCode,
				RS.AccountObjectName,
				RS.AccountNumber,
				fn.OpeningDebitAmount as OpeningDebitAmount,
				fn.OpeningCreditAmount as OpeningCreditAmount,
				Rs.DebitAmount as DebitAmount,
				Rs.CreditAmount as CreditAmount,
				Rs.ClosingDebitAmount as ClosingDebitAmount,
				Rs.ClosingCreditAmount as ClosingCreditAmount
        FROM    #tblResult RS ,
		        [dbo].[Func_SoDu] (
   @FromDate
  ,@ToDate
  ,3) fn
  where rs.AccountNumber = fn.AccountNumber
  and rs.AccountObjectID = fn.AccountObjectID
  order by RS.AccountObjectCode ,
                                RS.AccountNumber ,
                                RS.PostedDate ,
                                RS.RefDate ,
                                RS.RefNo,
								OrderPriority
        DROP TABLE #tblResult
    END				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER procedure [dbo].[Proc_PO_GetDetailBook]
@FromDate DATETIME ,
    @ToDate DATETIME,
    @AccountObjectID AS NVARCHAR(MAX),
	@MaterialGoods as NVARCHAR(MAX)
as
begin
    CREATE TABLE #tblListAccountObjectID  
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25)
                 ,
              AccountObjectName NVARCHAR(255)
                 
            ) 
    CREATE TABLE #tblListMaterialGoods  
            (
              MaterialGoodsID UNIQUEIDENTIFIER ,
              MaterialGoodsCode NVARCHAR(25)
                 ,
              MaterialGoodsName NVARCHAR(255)
                 ,
			  Unit NVARCHAR(25)  ,
			 Quantity decimal,
			 UnitPrice Money 
            ) 
        INSERT  INTO #tblListAccountObjectID
                SELECT  AO.ID ,
                        ao.AccountingObjectCode ,
                        ao.AccountingObjectName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                           ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value 
        INSERT  INTO #tblListMaterialGoods
                SELECT  MG.ID ,
                        MG.MaterialGoodsCode ,
                        MG.MaterialGoodsName,
						MG.Unit,
						MG.Quantity,
						MG.UnitPrice 
                FROM    dbo.Func_ConvertStringIntoTable(@MaterialGoods,
                                                           ',') tblAccountObjectSelected
                        INNER JOIN dbo.MaterialGoods MG ON MG.ID = tblAccountObjectSelected.Value 

	select d.AccountObjectCode as MaKH, 
		   d.AccountObjectName as TenKH, 
		   a.Date as NgayHachToan, 
		   a.PostedDate as NgayCTu, 
		   a.No as SoCTu, 
		   b.InvoiceNo as SoHoaDon, 
		   b.InvoiceDate as NgayHoaDon, 
		   c.MaterialGoodsCode as Mahang, 
		   c.MaterialGoodsName as Tenhang, 
		   b.Unit as DVT,
		   b.Quantity as SoLuongMua, 
		   b.UnitPrice as DonGia, 
		   b.AmountOriginal as GiaTriMua, 
		   b.DiscountAmount as ChietKhau,
		   null as SoLuongTraLai, 
		   null as GiaTriTraLai,
		   null as GiaTriGiamGia  		 
	   from PPInvoice a join PPInvoiceDetail b
	   on a.id = b.PPInvoiceID 
	   left join #tblListMaterialGoods c on c.MaterialGoodsID = b.MaterialGoodsID
	   left join #tblListAccountObjectID d on d.AccountObjectID = a.AccountingObjectID
	   where PostedDate between @FromDate and @ToDate
	   and Recorded = '1' 
union all
	   select  d.AccountObjectCode as MaKH, 
		   d.AccountObjectName as TenKH, 
			   Date as NgayHachToan, 
			   PostedDate as NgayCTu, 
			   No as SoCTu, 
			   InvoiceNo as SoHoaDon, 
			   InvoiceDate as NgayHoaDon, 
			   MaterialGoodsCode as Mahang, 
			   MaterialGoodsName as Tenhang, 
			   c.Unit as DVT,		
			   c.Quantity as SoLuongMua, 
			   c.UnitPrice as DonGia, 
			   AmountOriginal as GiaTriMua, 
			   DiscountAmount as ChietKhau,
			   null as SoLuongTraLai, 
			   null as GiaTriTraLai,
			   null as GiaTriGiamGia  		
	   from PPService a join PPServiceDetail b		
	   on a.id = b.PPServiceID 		
       left join #tblListMaterialGoods c on c.MaterialGoodsID = b.MaterialGoodsID
	   left join #tblListAccountObjectID d on d.AccountObjectID = a.AccountingObjectID
	   where PostedDate between @FromDate and @ToDate
	   and Recorded = '1' 
union all
	   select d.AccountObjectCode as MaKH, 
		   d.AccountObjectName as TenKH, 
			  a.Date as NgayHachToan, 
			  PostedDate as NgayCTu, 
			  a.No as SoCTu, 
			  InvoiceNo as SoHoaDon, 
			  InvoiceDate as NgayHoaDon, 
			  MaterialGoodsCode as Mahang, 
			  MaterialGoodsName as Tenhang, 
			  b.Unit as DVT,
			  null SoLuongMua,	
			  b.UnitPrice as DonGia,	
			  AmountOriginal as GiaTriMua, 
			  null ChietKhau,
		      b.Quantity as SoLuongTraLai, 
			  b.Amount as GiaTriTraLai,
			  null as GiaTriGiamGia 		
		from PPDiscountReturn a join PPDiscountReturnDetail b		
		on a.id = b.PPDiscountReturnID 		
		left join #tblListMaterialGoods c on c.MaterialGoodsID = b.MaterialGoodsID
	    left join #tblListAccountObjectID d on d.AccountObjectID = a.AccountingObjectID
		where PostedDate between @FromDate and @ToDate and Recorded = '1' and TypeID = '220'		
union all
		select d.AccountObjectCode as MaKH, 
		   d.AccountObjectName as TenKH, 
			   a.Date as NgayHachToan, 
			   PostedDate as NgayCTu, 
			   a.No as SoCTu, 
			   InvoiceNo as SoHoaDon, 
			   InvoiceDate as NgayHoaDon, 
			   MaterialGoodsCode as Mahang, 
			   MaterialGoodsName as Tenhang, 
			   b.Unit as DVT,
			   null SoLuongMua,	
			   b.UnitPrice as DonGia,
			   AmountOriginal as GiaTriMua, 
			   TotalDiscountAmount as Chietkhau,		
		       null as SoLuongTraLai,
			   null as GiaTriTraLai,   
			   b.Amount as GiaTriGiamGia	   	
		from PPDiscountReturn a join PPDiscountReturnDetail b		
		on a.id = b.PPDiscountReturnID 		
		left join #tblListMaterialGoods c on c.MaterialGoodsID = b.MaterialGoodsID
	    left join #tblListAccountObjectID d on d.AccountObjectID = a.AccountingObjectID
		where PostedDate between @FromDate and @ToDate and TypeID = '230';		

end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_GetGLBookDetailMoneyBorrow]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountID NVARCHAR(MAX) ,
    @AccountObjectID NVARCHAR(MAX)
AS 
    BEGIN
	
        CREATE  TABLE #Result
            (
              RefID UNIQUEIDENTIFIER ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(255) ,
              AccountNumber NVARCHAR(25) ,
              AccountName NVARCHAR(255) ,
              CorrespondingAccountNumber NVARCHAR(25) ,
              DueDate DATETIME ,
              DebitAmount DECIMAL(22, 4) ,
              CreditAmount DECIMAL(22, 4) ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              OrderType INT ,
              IsBold BIT ,
              BranchName NVARCHAR(128) ,
              SortOrder INT ,
              DetailPostOrder INT
            )      

        DECLARE @tblAccountObject TABLE
            (
              AccountObjectID UNIQUEIDENTIFIER PRIMARY KEY,
			  AccountObjectCode NVARCHAR(25)
                 ,
              AccountObjectName NVARCHAR(255)
                
            )                       
        INSERT  INTO @tblAccountObject
                SELECT  f.value,A.AccountingObjectCode,A.AccountingObjectName
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                           ',') f
                 INNER JOIN dbo.AccountingObject A ON A.ID = F.Value 	
        CREATE TABLE #tblSelectedAccountNumber
            (
              AccountNumber NVARCHAR(20) 
                                         NOT NULL
                                         PRIMARY KEY ,
              AccountName NVARCHAR(255) 
                                        NULL
            )
        INSERT  INTO #tblSelectedAccountNumber
                SELECT  A.AccountNumber ,
                        A.AccountName
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountID, ',') F
                        INNER JOIN dbo.Account A ON A.AccountNumber = F.Value 			        
        BEGIN             
                
            INSERT  #Result
                    SELECT  RefID ,
                            PostedDate ,
                            RefDate ,
                            RefNo ,
                            RefType ,
                            JournalMemo ,
                            AccountNumber ,
                            AccountName ,
                            CorrespondingAccountNumber ,
                            DueDate ,
                            DebitAmount ,
                            CreditAmount ,
                            AccountObjectCode ,
                            AccountObjectName ,
                            OrderType ,
                            IsBold ,
                            BranchName ,
                            SortOrder ,
                            DetailPostOrder
                    FROM    ( SELECT    NULL AS RefID ,
                                        NULL AS PostedDate ,
                                        NULL AS RefDate ,
                                        NULL AS RefNo ,
                                        NULL AS RefType ,
                                        N'Số dư đầu kỳ' AS JournalMemo ,
                                        GL.Account AccountNumber,
                                        AC.AccountName ,
                                        NULL AS CorrespondingAccountNumber ,
                                        NULL DueDate ,
                                        CASE WHEN SUM(GL.DebitAmount
                                                      - GL.CreditAmount) > 0
                                             THEN SUM(GL.DebitAmount
                                                      - GL.CreditAmount)
                                             ELSE 0
                                        END AS DebitAmount ,
                                        CASE WHEN SUM(GL.CreditAmount
                                                      - GL.DebitAmount) > 0
                                             THEN SUM(GL.CreditAmount
                                                      - GL.DebitAmount)
                                             ELSE 0
                                        END AS CreditAmount ,
                                        AO.AccountObjectCode ,
                                        AO.AccountObjectName AS AccountObjectName ,
                                        0 AS OrderType ,
                                        1 AS isBold ,
                                        null  AS BranchName,
                                        0 AS sortOrder ,
                                        0 AS DetailPostOrder ,
                                        0 AS EntryType
                              FROM      dbo.GeneralLedger AS GL
                                        left JOIN #tblSelectedAccountNumber AC ON GL.Account LIKE AC.AccountNumber
                                                              + '%'
                                        left JOIN @tblAccountObject AO ON AO.AccountObjectID = GL.AccountingObjectID
                              WHERE     ( GL.PostedDate < @FromDate )
                              GROUP BY  GL.Account ,
                                        AC.AccountName ,
                                        AO.AccountObjectCode ,
                                        AO.AccountObjectName
                              HAVING    SUM(GL.DebitAmount - GL.CreditAmount) <> 0
                              UNION ALL
                              SELECT    GL.ReferenceID ,
                                        GL.PostedDate ,
                                        GL.RefDate ,
                                        GL.RefNo ,
                                        GL.TypeID ,
                                        CASE WHEN ( GL.Description IS NOT NULL
                                                    AND GL.Description <> ''
                                                  ) THEN GL.Description
                                             ELSE GL.Reason
                                        END AS JournalMemo ,
                                        GL.Account AccountNumber,
                                        AC.AccountName ,
                                        GL.AccountCorresponding,
                                        null as DueDate ,
                                        GL.DebitAmount ,
                                        GL.CreditAmount ,
                                        AO.AccountObjectCode ,
                                        AO.AccountObjectName AS AccountObjectName ,
                                        2 AS OrderType ,
                                        0 ,
                                        null AS BranchName,
                                        GL.OrderPriority,
                                        null,
                                        null
                              FROM      dbo.GeneralLedger AS GL
                                        left JOIN #tblSelectedAccountNumber AC ON GL.Account LIKE AC.AccountNumber
                                                              + '%'
                                        left JOIN @tblAccountObject AO ON AO.AccountObjectID = GL.AccountingObjectID
                              WHERE     ( GL.PostedDate BETWEEN @FromDate AND @ToDate )
                                        AND ( GL.DebitAmount <> 0
                                              OR GL.CreditAmount <> 0
                                            )
                            ) T
                    ORDER BY AccountObjectName ,
                            AccountObjectCode ,
                            OrderType ,
                            postedDate ,
                            RefDate ,
                            RefNo ,
                            DetailPostOrder ,
                            SortOrder ,
                            EntryType                                                          
               
        END         
            
       
      /*Insert lable*/
        INSERT  #Result
                ( JournalMemo ,
                  AccountNumber ,
                  AccountName ,
                  DebitAmount ,
                  CreditAmount ,
                  AccountObjectCode ,
                  AccountObjectName ,
                  OrderType ,
                  IsBold ,
                  BranchName
		        )                                                
                SELECT  N'Số dư cuối kỳ' JournalMemo ,
                        AccountNumber ,
                        AccountName ,
                        CASE WHEN SUM(DebitAmount - CreditAmount) > 0
                             THEN SUM(DebitAmount - CreditAmount)
                             ELSE 0
                        END AS DebitAmount ,
                        CASE WHEN SUM(CreditAmount - DebitAmount) > 0
                             THEN SUM(CreditAmount - DebitAmount)
                             ELSE 0
                        END AS CreditAmount ,
                        AccountObjectCode ,
                        AccountObjectName ,
                        4 AS OrderType ,
                        1 ,
                        null as BranchName
                FROM    #Result
                WHERE   OrderType = 2
                        OR OrderType = 0
                GROUP BY AccountNumber ,
                        AccountName ,
                        AccountObjectCode ,
                        AccountObjectName
                HAVING  ( SUM(DebitAmount) <> 0
                          OR SUM(CreditAmount) <> 0
                        )                        
	     IF EXISTS ( SELECT  * FROM    #Result ) 
			SELECT  * ,
                ROW_NUMBER() OVER ( ORDER BY AccountNumber,AccountObjectName,AccountObjectCode,  OrderType , PostedDate , RefNo , RefDate, SortOrder, DetailPostOrder ) AS RowNum
			FROM    #Result  
        
        ELSE 
           BEGIN
                INSERT  #Result
                        ( JournalMemo ,
                          OrderType ,
                          IsBold ,
                          BranchName
		                )                        
                        SELECT  N'' JournalMemo ,
                                2 ,
                                1 ,
                                ''                                                                     
                SELECT  * ,
                        ROW_NUMBER() OVER ( ORDER BY AccountObjectName, AccountObjectCode , OrderType , PostedDate , RefDate, RefNo, SortOrder, DetailPostOrder ) AS RowNum
                FROM    #Result         
            END                       
        DELETE  #Result
        DELETE  #tblSelectedAccountNumber        
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO
Update dbo.SAInvoice set IsBill = 1 where InvoiceForm IS NOT NULL and InvoiceTypeID IS NOT NULL and InvoiceTemplate IS NOT NULL;
Update dbo.PPDiscountReturn set IsBill = 1 where InvoiceForm IS NOT NULL and InvoiceTypeID IS NOT NULL and InvoiceTemplate IS NOT NULL and TypeID = 220;
Update dbo.SAReturn set IsBill = 1 where InvoiceForm IS NOT NULL and InvoiceTypeID IS NOT NULL and InvoiceTemplate IS NOT NULL and TypeID = 340;
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO


GO
DISABLE TRIGGER ALL ON dbo.TemplateColumn
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateDetail
  DROP CONSTRAINT FK_TemplateDetail_Template
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Type
  DROP CONSTRAINT FK_Type_TypeGroup
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Template
  DROP CONSTRAINT FK_Template_Type
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF (OBJECT_ID('dbo.FK__TT153Adju__TypeI__25869641', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.TT153AdjustAnnouncement DROP CONSTRAINT FK__TT153Adju__TypeI__25869641
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF (OBJECT_ID('dbo.FK__TT153Dele__TypeI__61F08603', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.TT153DeletedInvoice DROP CONSTRAINT FK__TT153Dele__TypeI__61F08603
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF (OBJECT_ID('dbo.FK__TT153Dest__TypeI__22AA2996', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.TT153DestructionInvoice DROP CONSTRAINT FK__TT153Dest__TypeI__22AA2996
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF (OBJECT_ID('dbo.FK__TT153Lost__TypeI__1FCDBCEB', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.TT153LostInvoice DROP CONSTRAINT FK__TT153Lost__TypeI__1FCDBCEB
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF (OBJECT_ID('dbo.FK__TT153Publ__TypeI__1CF15040', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.TT153PublishInvoice DROP CONSTRAINT FK__TT153Publ__TypeI__1CF15040
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF (OBJECT_ID('dbo.FK__TT153Regi__TypeI__1A14E395', 'F') IS NOT NULL)
BEGIN
    ALTER TABLE dbo.TT153RegisterInvoice DROP CONSTRAINT FK__TT153Regi__TypeI__1A14E395
END
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.Account SET IsParentNode = 1 WHERE ID = '8eed1e1f-fab1-45e5-8750-1f7de5b8b2f7'
UPDATE dbo.Account SET IsParentNode = 1 WHERE ID = '747727cd-3d84-49e6-befd-2a549a62ff65'
UPDATE dbo.Account SET IsParentNode = 1 WHERE ID = '399f97ce-8ca2-46c2-9069-2fe7a0d854cc'
UPDATE dbo.Account SET IsParentNode = 1 WHERE ID = '4a0760b1-fdfa-4439-b0ee-3f6d5fdabc42'
UPDATE dbo.Account SET DetailByAccountObject = 1 WHERE ID = '00e0acfe-02b2-476b-b99e-457065523383'
UPDATE dbo.Account SET IsParentNode = 1 WHERE ID = '04cfeb78-c9d3-47e4-94e1-461e00e98926'
UPDATE dbo.Account SET IsParentNode = 1 WHERE ID = '05d067f4-5cd2-49a1-9c5f-70e03d927cf4'
UPDATE dbo.Account SET IsParentNode = 1 WHERE ID = 'd303c286-c4a6-496d-ad6f-9899e23050ea'
UPDATE dbo.Account SET DetailByAccountObject = 1 WHERE ID = '398ac7af-432c-4e2f-84ee-9b02aeeebb2b'
UPDATE dbo.Account SET IsParentNode = 1 WHERE ID = '8cb3fef4-5ee4-413c-bd1a-a19d0d668301'
UPDATE dbo.Account SET IsParentNode = 1 WHERE ID = '580759e3-3322-4058-9e6d-d303df12f9f3'
UPDATE dbo.Account SET DetailByAccountObject = 1 WHERE ID = 'df4adbb7-0dc7-4e23-ab82-e011bd40d93d'
UPDATE dbo.Account SET IsParentNode = 1 WHERE ID = 'cfa92323-a8b2-4355-b0a3-e868f54e3aed'
UPDATE dbo.Account SET IsParentNode = 1 WHERE ID = '2c2f861c-d50d-4b10-929d-fdcf63e0c2b4'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '42264cb6-b102-4598-a864-1ab819f4f5c6'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'a80e0cfd-d3d8-409d-be9f-26e7f2a5fe6d'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '5d7cb490-96fb-4aac-832b-2fd091ed727c'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '97d62fb0-b345-4f30-a54e-4d676500e1aa'
UPDATE dbo.AccountDefault SET FilterAccount = N'112', DefaultAccount = N'' WHERE ID = '259cb47b-f744-4531-9749-6d6d6e394501'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = 'c9e12075-6570-4e1e-a068-728137522234'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '2f8a3363-59c2-4064-b33a-a6443809ecf5'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '87c2d73c-f9eb-4278-b2bd-c04d665525f1'
UPDATE dbo.AccountDefault SET FilterAccount = N'111' WHERE ID = '82855550-4169-4ce0-adbf-d7f451aa472f'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '2d6eb113-0131-4e5e-a238-ecad55b24792'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '6b8f1136-01b7-4203-8ee8-f1598fbb8a7b'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '7e06903b-da7b-4232-97ac-f2010f7e0a6a'
UPDATE dbo.AccountDefault SET DefaultAccount = N'' WHERE ID = '3fa88405-e232-44aa-940d-f823858376f8'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

SET IDENTITY_INSERT dbo.ContractState ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.ContractState(ID, ContractStateCode, ContractStateName, Description, IsSecurity) VALUES (1, N'1', N'Chưa thực hiện', N'Chưa thực hiện', 0)
INSERT dbo.ContractState(ID, ContractStateCode, ContractStateName, Description, IsSecurity) VALUES (2, N'2', N'Đang thực hiện', N'Đang thực hiện', 0)
INSERT dbo.ContractState(ID, ContractStateCode, ContractStateName, Description, IsSecurity) VALUES (3, N'3', N'Tạm ngừng thực hiện', N'Tạm ngừng thực hiện', 0)
INSERT dbo.ContractState(ID, ContractStateCode, ContractStateName, Description, IsSecurity) VALUES (4, N'4', N'Đình chỉ', N'Đình chỉ', 0)
INSERT dbo.ContractState(ID, ContractStateCode, ContractStateName, Description, IsSecurity) VALUES (5, N'5', N'Hủy bỏ', N'Hủy bỏ', 0)
INSERT dbo.ContractState(ID, ContractStateCode, ContractStateName, Description, IsSecurity) VALUES (6, N'6', N'Hoàn thành chưa thanh lý', N'Hoàn thành chưa thanh lý', 0)
INSERT dbo.ContractState(ID, ContractStateCode, ContractStateName, Description, IsSecurity) VALUES (7, N'7', N'Thanh lý', N'Thanh lý', 0)
INSERT dbo.ContractState(ID, ContractStateCode, ContractStateName, Description, IsSecurity) VALUES (9, N'8', N'Kết thúc', N'Kết thúc', 0)
GO
SET IDENTITY_INSERT dbo.ContractState OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DBCC CHECKIDENT('dbo.ContractState', RESEED, 10)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'AED', N'United Arab Emirates Dirham
', 5918.6700000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'AFN', N'Afghan Afghani
', 372.7500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'ALL', N'Albanian Lek
', 175.1400000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'AMD', N'Armenian Dram
', 45.3100000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'ANG', N'Netherlands Antillean Guilder
', 12151.9000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'AOA', N'Angola
', 199.1800000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'ARS', N'Argentine Peso
', 2434.4900000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'AUD', N'Đô la Úc', 21876.0000000000, N'', N'', N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'AWG', N'Aruban Florin
', 12128.3100000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'AZN', N'Azerbaijani Manat
', 20719.5700000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'BAM', N'Bosnia-Herzegovina Convertible Mark
', 12576.0200000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'BBD', N'Barbadian Dollar
', 10870.0000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'BDT', N'Bangladeshi Taka
', 281.2100000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'BGN', N'Bulgarian Lev
', 12581.0900000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'BHD', N'Bahraini Dinar
', 57634.4400000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'BIF', N'Burundian Franc
', 13.8000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'BMD', N'Bermudan Dollar
', 21740.0000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'BND', N'Brunei Dollar
', 16401.8700000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'BOB', N'Bolivian Boliviano
', 3164.7100000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'BRL', N'Brazilian Real
', 7157.3900000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'BSD', N'Bahamian Dollar
', 21740.0000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'BTC', N'Bitcoin
', 5007448.0500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'BTN', N'Bhutanese Ngultrum
', 342.2100000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'BWP', N'Botswanan Pula
', 2228.1100000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'BYR', N'Belarusian Ruble
', 1.5200000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'BZD', N'Belize Dollar
', 10934.6100000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'CAD', N'Canadian Dollar
', 18145.7500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'CDF', N'Congolese Franc
', 23.5000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'CLF', N'Chilean Unit of Account (UF)
', 883668.0000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'CLP', N'Chilean Peso
', 35.5400000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'CNY', N'Nhân dân tệ Trung Quốc', 3388.0000000000, N'1112', N'1122', N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'COP', N'Colombian Peso
', 9.1700000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'CRC', N'Costa Rican Colón
', 41.0000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'CUC', N'Cuban Convertible Peso
', 21740.0000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'CUP', N'Cuban Peso
', 21792.0200000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'CVE', N'Cape Verdean Escudo
', 223.0500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'CZK', N'Czech Republic Koruna
', 897.9900000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'CHF', N'Swiss Franc
', 23783.6500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'DJF', N'Djiboutian Franc
', 122.5700000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'DKK', N'Danish Krone
', 3347.8700000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'DOP', N'Dominican Peso
', 487.6300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'DZD', N'Algerian Dinar
', 224.6000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'EEK', N'Estonian Kroon
', 1569.3900000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'EGP', N'Egyptian Pound
', 2852.7900000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'ERN', N'Eritrean Nakfa
', 1438.3000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'ETB', N'Ethiopian Birr
', 1063.2800000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'EUR', N'Đồng tiền chung châu Âu', 27272.0000000000, N'1112', N'1122', N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'FJD', N'Fijian Dollar
', 10742.9100000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'FKP', N'Falkland Islands Pound
', 33124.1300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'GBP', N'Bảng Anh', 31814.0000000000, N'1112', N'1122', N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'GEL', N'Georgian Lari
', 9339.9800000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'GGP', N'Guernsey Pound
', 33124.1300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'GHS', N'Ghanaian Cedi
', 5649.2500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'GMD', N'Gambian Dalasi
', 505.4300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'GNF', N'Guinean Franc
', 3.0000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'GTQ', N'Guatemalan Quetzal
', 2816.5000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'GYD', N'Guyanaese Dollar
', 105.5000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'GIP', N'Gibraltar Pound
', 33124.1300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'HKD', N'Đô La Hồng Kong', 2722.0000000000, N'1112', N'1122', N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'HNL', N'Honduran Lempira
', 995.3000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'HRK', N'Croatian Kuna
', 3246.0200000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'HTG', N'Haitian Gourde
', 460.2600000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'HUF', N'Hungarian Forint
', 80.5900000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'IDR', N'Indonesian Rupiah
', 1.6700000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'ILS', N'Israeli New Sheqel
', 5643.0300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'IMP', N'Manx pound
', 33124.1300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'INR', N'Indian Rupee
', 347.7300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'IQD', N'Iraqi Dinar
', 18.4600000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'IRR', N'Iranian Rial
', 0.7600000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'ISK', N'Icelandic Króna
', 166.2500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'JEP', N'Jersey Pound
', 33124.1300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'JMD', N'Jamaican Dollar
', 189.3300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'JOD', N'Jordanian Dinar
', 30685.7500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'JPY', N'Yên Nhật', 221.5400000000, N'1112', N'1122', N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'KES', N'Kenyan Shilling
', 228.5700000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'KGS', N'Kyrgystani Som
', 365.2500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'KMF', N'Comorian Franc
', 50.0200000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'KPW', N'North Korean Won
', 24.1500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'KRW', N'South Korean Won
', 20.0500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'KWD', N'Kuwaiti Dinar
', 73282.6900000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'KYD', N'Cayman Islands Dollar
', 26449.6200000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'KZT', N'Kazakhstani Tenge
', 117.2700000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'KHR', N'Riêl Cămpuchia', 5.2300000000, N'1112', N'1122', N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'LAK', N'Kíp Lào', 2.6500000000, N'1112', N'1122', N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'LBP', N'Lebanese Pound
', 14.4400000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'LKR', N'Sri Lankan Rupee
', 163.9400000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'LRD', N'Liberian Dollar
', 257.1200000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'LSL', N'Lesotho Loti
', 1811.8100000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'LTL', N'Lithuanian Litas
', 7287.7900000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'LVL', N'Latvian Lats
', 34980.2300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'LYD', N'Libyan Dinar
', 16082.7100000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'MAD', N'Moroccan Dirham
', 2254.5500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'MDL', N'Moldovan Leu
', 1211.0300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'MGA', N'Malagasy Ariary
', 7.0400000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'MKD', N'Macedonian Denar
', 399.6000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'MMK', N'Myanma Kyat
', 20.0900000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'MNT', N'Mongolian Tugrik
', 11.1300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'MOP', N'Macanese Pataca
', 2729.1500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'MRO', N'Mauritanian Ouguiya
', 70.5700000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'MTL', N'Maltese Lira
', 57393.5800000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'MUR', N'Mauritian Rupee
', 620.9400000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'MVR', N'Maldivian Rufiyaa
', 1423.1700000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'MWK', N'Malawian Kwacha
', 50.3700000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'MXN', N'Mexican Peso
', 1415.4500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'MYR', N'Malaysian Ringgit
', 6090.0500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'MZN', N'Mozambican Metical
', 615.8700000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'NAD', N'Namibian Dollar
', 1811.8100000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'NIO', N'Nicaraguan Córdoba
', 814.3200000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'NOK', N'Norwegian Krone
', 2967.7200000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'NPR', N'Nepalese Rupee
', 215.0500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'NZD', N'New Zealand Dollar
', 16308.6100000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'NGN', N'Nigerian Naira
', 109.4800000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'OMR', N'Omani Rial
', 56479.8500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'PAB', N'Panamanian Balboa
', 21740.0000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'PEN', N'Peruvian Nuevo Sol
', 6921.1500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'PGK', N'Papua New Guinean Kina
', 8078.7400000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'PKR', N'Pakistani Rupee
', 214.2000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'PLN', N'Polish Zloty
', 6083.8100000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'PYG', N'Paraguayan Guarani
', 4.3500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'PHP', N'Philippine Peso
', 487.0800000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'QAR', N'Qatari Rial
', 5971.8300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'RON', N'Romanian Leu
', 5540.1600000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'RSD', N'Serbian Dinar
', 204.0500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'RUB', N'Ruble Nga
', 435.9700000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'RWF', N'Rwandan Franc
', 31.6300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'SAR', N'Saudi Riyal
', 5971.2900000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'SBD', N'Solomon Islands Dollar
', 2820.4200000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'SCR', N'Seychellois Rupee
', 1602.2400000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'SDG', N'Sudanese Pound
', 3662.0200000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'SEK', N'Swedish Krona
', 2670.9500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'SGD', N'Đô la Singapore', 16911.0000000000, N'1112', N'1122', N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'SHP', N'Saint Helena Pound
', 33124.1300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'SLL', N'Sierra Leonean Leone
', 5.0200000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'SOS', N'Somali Shilling
', 31.1100000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'SRD', N'Surinamese Dollar
', 6554.1100000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'STD', N'São Tomé and Príncipe Dobra
', 1.0000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'SVC', N'Salvadoran Colón
', 2502.4300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'SYP', N'Syrian Pound
', 115.0700000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'SZL', N'Swazi Lilangeni
', 1811.7000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'TJS', N'Tajikistani Somoni
', 3459.8100000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'TMT', N'Turkmenistani Manat
', 6211.3800000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'TND', N'Tunisian Dinar
', 11421.3700000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'TOP', N'Tongan Paʻanga
', 10731.1700000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'TTD', N'Trinidad and Tobago Dollar
', 3439.1200000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'TWD', N'New Taiwan Dollar
', 709.2500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'TZS', N'Tanzanian Shilling
', 11.0200000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'THB', N'Bath Thái', 735.0000000000, N'1112', N'1122', N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'TRY', N'Turkish Lira
', 8054.7600000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'UAH', N'Gríp-na Ucraina', 2600.0000000000, N'1112', N'1122', N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'UGX', N'Ugandan Shilling
', 7.2600000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'UYU', N'Uruguayan Peso
', 826.1900000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'UZS', N'Uzbekistan Som
', 8.6500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'VEF', N'Venezuelan Bolívar Fuerte
', 3440.7000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'VUV', N'Vanuatu Vatu
', 206.7500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'WST', N'Samoan Tala
', 8900.6200000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'XAF', N'CFA Franc BEAC
', 37.4400000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'XAG', N'Silver (troy ounce)
', 358008.1700000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'XAU', N'Gold (troy ounce)
', 25841574.7500000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'XCD', N'East Caribbean Dollar
', 8048.8200000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'XDR', N'Special Drawing Rights
', 30596.5300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'XOF', N'CFA Franc BCEAO
', 37.4800000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'XPF', N'CFP Franc
', 206.1000000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'YER', N'Yemeni Rial
', 101.1300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'ZAR', N'South African Rand
', 1808.5700000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'ZMK', N'Zambian Kwacha (pre-2013)
', 4.1300000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'ZMW', N'Zambian Kwacha
', 2968.1200000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
INSERT dbo.Currency(ID, CurrencyName, ExchangeRate, CashAccount, BankAccount, FirstCharater, FirstCharacterDefault, SeparatorCharater, SeparatorCharaterDefault, AfterCharater, AfterCharaterDefault, IsActive) VALUES (N'ZWL', N'Zimbabwean Dollar
', 67.4400000000, NULL, NULL, N'', NULL, N'', NULL, N'', NULL, 1)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.FixedAssetCategory(ID, FixedAssetCategoryCode, FixedAssetCategoryName, Description, ParentID, OrderFixCode, Grade, UsedTime, DepreciationRate, OriginalPriceAccount, DepreciationAccount, ExpenditureAccount, IsActive, IsParentNode) VALUES ('cda02dd8-2d61-46eb-adee-1fef53b79334', N'22', N'Quyền phát hành', N'', '6e941fb3-4cd7-4fb0-af15-5dd7500f2cbd', N'2-2', 2, NULL, NULL, N'2113', N'2143', NULL, 1, 0)
INSERT dbo.FixedAssetCategory(ID, FixedAssetCategoryCode, FixedAssetCategoryName, Description, ParentID, OrderFixCode, Grade, UsedTime, DepreciationRate, OriginalPriceAccount, DepreciationAccount, ExpenditureAccount, IsActive, IsParentNode) VALUES ('cc2f9867-001e-48ff-bf12-375ac5020bdf', N'11', N'Nhà cửa, vật kiến trúc', N'', '92346b42-3d3b-4734-b23e-82acdcfa774f', N'1-1', 2, 2.0000000000, NULL, N'2111', N'2141', NULL, 1, 0)
INSERT dbo.FixedAssetCategory(ID, FixedAssetCategoryCode, FixedAssetCategoryName, Description, ParentID, OrderFixCode, Grade, UsedTime, DepreciationRate, OriginalPriceAccount, DepreciationAccount, ExpenditureAccount, IsActive, IsParentNode) VALUES ('0a42d9ea-e2b6-407a-a29a-3b5fd0d1bf30', N'17', N'Tài sản cố định khác', N'', '92346b42-3d3b-4734-b23e-82acdcfa774f', N'1-7', 2, NULL, NULL, N'2111', N'2141', NULL, 1, 0)
INSERT dbo.FixedAssetCategory(ID, FixedAssetCategoryCode, FixedAssetCategoryName, Description, ParentID, OrderFixCode, Grade, UsedTime, DepreciationRate, OriginalPriceAccount, DepreciationAccount, ExpenditureAccount, IsActive, IsParentNode) VALUES ('c8ec1552-f6fb-46f5-8b45-41e437f0378d', N'21', N'Quyền sử dụng đất', N'', '6e941fb3-4cd7-4fb0-af15-5dd7500f2cbd', N'2-1', 2, NULL, NULL, N'2113', N'2143', NULL, 1, 0)
INSERT dbo.FixedAssetCategory(ID, FixedAssetCategoryCode, FixedAssetCategoryName, Description, ParentID, OrderFixCode, Grade, UsedTime, DepreciationRate, OriginalPriceAccount, DepreciationAccount, ExpenditureAccount, IsActive, IsParentNode) VALUES ('c5939bbe-66f6-4b0f-a77e-4a0c12e62518', N'14', N'Thiết bị, dụng cụ quản lý', N'', '92346b42-3d3b-4734-b23e-82acdcfa774f', N'1-4', 2, NULL, NULL, N'2111', N'2141', NULL, 1, 0)
INSERT dbo.FixedAssetCategory(ID, FixedAssetCategoryCode, FixedAssetCategoryName, Description, ParentID, OrderFixCode, Grade, UsedTime, DepreciationRate, OriginalPriceAccount, DepreciationAccount, ExpenditureAccount, IsActive, IsParentNode) VALUES ('6e941fb3-4cd7-4fb0-af15-5dd7500f2cbd', N'2', N'Tài sản cố định vô hình', N'', NULL, N'2', 1, NULL, NULL, NULL, NULL, NULL, 1, 1)
INSERT dbo.FixedAssetCategory(ID, FixedAssetCategoryCode, FixedAssetCategoryName, Description, ParentID, OrderFixCode, Grade, UsedTime, DepreciationRate, OriginalPriceAccount, DepreciationAccount, ExpenditureAccount, IsActive, IsParentNode) VALUES ('c899ce6b-8007-4bc0-8709-6647d70c2868', N'16', N'Các TSCĐ là kết cấu hạ tầng, có giá trị lớn do Nhà nước ĐTXD từ NSNN giao cho các tổ chức kinh tế quản lý, khai thác, sử dụng', N'', '92346b42-3d3b-4734-b23e-82acdcfa774f', N'1-6', 2, NULL, NULL, N'2111', NULL, NULL, 1, 0)
INSERT dbo.FixedAssetCategory(ID, FixedAssetCategoryCode, FixedAssetCategoryName, Description, ParentID, OrderFixCode, Grade, UsedTime, DepreciationRate, OriginalPriceAccount, DepreciationAccount, ExpenditureAccount, IsActive, IsParentNode) VALUES ('5e7eb477-8e1d-41f5-85e5-78e1884180b0', N'12', N'Máy móc, thiết bị', N'', '92346b42-3d3b-4734-b23e-82acdcfa774f', N'1-2', 2, NULL, NULL, N'2111', N'2141', NULL, 1, 0)
INSERT dbo.FixedAssetCategory(ID, FixedAssetCategoryCode, FixedAssetCategoryName, Description, ParentID, OrderFixCode, Grade, UsedTime, DepreciationRate, OriginalPriceAccount, DepreciationAccount, ExpenditureAccount, IsActive, IsParentNode) VALUES ('6429eba0-8c16-492d-b34c-7acde0ab9065', N'27', N'Tài sản cố định vô hình khác', N'', '6e941fb3-4cd7-4fb0-af15-5dd7500f2cbd', N'2-7', 2, NULL, NULL, N'2113', N'2143', NULL, 1, 0)
INSERT dbo.FixedAssetCategory(ID, FixedAssetCategoryCode, FixedAssetCategoryName, Description, ParentID, OrderFixCode, Grade, UsedTime, DepreciationRate, OriginalPriceAccount, DepreciationAccount, ExpenditureAccount, IsActive, IsParentNode) VALUES ('92346b42-3d3b-4734-b23e-82acdcfa774f', N'1', N'Tài sản cố định hữu hình ', N'', NULL, N'1', 1, NULL, NULL, NULL, NULL, NULL, 1, 1)
INSERT dbo.FixedAssetCategory(ID, FixedAssetCategoryCode, FixedAssetCategoryName, Description, ParentID, OrderFixCode, Grade, UsedTime, DepreciationRate, OriginalPriceAccount, DepreciationAccount, ExpenditureAccount, IsActive, IsParentNode) VALUES ('f96e6bfc-15e4-407f-bbfc-ab9bc609f01b', N'23', N'Bản quyền, bằng sáng chế', N'', '6e941fb3-4cd7-4fb0-af15-5dd7500f2cbd', N'2-3', 2, NULL, NULL, N'2113', N'2143', NULL, 1, 0)
INSERT dbo.FixedAssetCategory(ID, FixedAssetCategoryCode, FixedAssetCategoryName, Description, ParentID, OrderFixCode, Grade, UsedTime, DepreciationRate, OriginalPriceAccount, DepreciationAccount, ExpenditureAccount, IsActive, IsParentNode) VALUES ('f4ffe65d-cc76-4733-84a9-c1f9c81f7483', N'26', N'Giấy phép và giấy phép nhượng quyền', N'', '6e941fb3-4cd7-4fb0-af15-5dd7500f2cbd', N'2-6', 2, NULL, NULL, N'2113', N'2143', NULL, 1, 0)
INSERT dbo.FixedAssetCategory(ID, FixedAssetCategoryCode, FixedAssetCategoryName, Description, ParentID, OrderFixCode, Grade, UsedTime, DepreciationRate, OriginalPriceAccount, DepreciationAccount, ExpenditureAccount, IsActive, IsParentNode) VALUES ('dacf94da-1597-4612-b28c-c845d30ec0a2', N'24', N'Nhãn hiệu hàng hóa', N'', '6e941fb3-4cd7-4fb0-af15-5dd7500f2cbd', N'2-4', 2, NULL, NULL, N'2113', N'2143', NULL, 1, 0)
INSERT dbo.FixedAssetCategory(ID, FixedAssetCategoryCode, FixedAssetCategoryName, Description, ParentID, OrderFixCode, Grade, UsedTime, DepreciationRate, OriginalPriceAccount, DepreciationAccount, ExpenditureAccount, IsActive, IsParentNode) VALUES ('eb28bc32-dd9b-4e96-85ce-d024e1086d6a', N'25', N'Phần mềm máy vi tính', N'', '6e941fb3-4cd7-4fb0-af15-5dd7500f2cbd', N'2-5', 2, NULL, NULL, N'2113', N'2143', NULL, 1, 0)
INSERT dbo.FixedAssetCategory(ID, FixedAssetCategoryCode, FixedAssetCategoryName, Description, ParentID, OrderFixCode, Grade, UsedTime, DepreciationRate, OriginalPriceAccount, DepreciationAccount, ExpenditureAccount, IsActive, IsParentNode) VALUES ('c259032f-b398-4e23-890f-d63ab091a7c4', N'15', N'Vườn cây lâu năm, súc vật làm việc và cho sản phẩm', N'', '92346b42-3d3b-4734-b23e-82acdcfa774f', N'1-5', 2, NULL, NULL, N'2111', N'2141', NULL, 1, 0)
INSERT dbo.FixedAssetCategory(ID, FixedAssetCategoryCode, FixedAssetCategoryName, Description, ParentID, OrderFixCode, Grade, UsedTime, DepreciationRate, OriginalPriceAccount, DepreciationAccount, ExpenditureAccount, IsActive, IsParentNode) VALUES ('272ef913-0ae7-4cac-84f2-ea94b3bedd72', N'13', N'Phương tiện vận tải, truyền dẫn', N'', '92346b42-3d3b-4734-b23e-82acdcfa774f', N'1-3', 2, NULL, NULL, N'2111', N'2141', NULL, 1, 0)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

INSERT dbo.GoodsServicePurchase(ID, GoodsServicePurchaseCode, GoodsServicePurchaseName, Description, IsActive, IsSecurity) VALUES ('f315b983-d06e-4137-aad1-4c5a1cb59d7d', N'2', N'Hàng hóa, dịch vụ dùng chung cho SXKD chịu thuế và không chịu thuế đủ điều kiện khấu trừ thuế', N'Hàng hóa, dịch vụ dùng chung cho SXKD chịu thuế và không chịu thuế đủ điều kiện khấu trừ thuế', 1, 0)
INSERT dbo.GoodsServicePurchase(ID, GoodsServicePurchaseCode, GoodsServicePurchaseName, Description, IsActive, IsSecurity) VALUES ('9fa9d9e2-97e9-4d5b-a2c0-6978ed67da92', N'1', N'Hàng hoá, dịch vụ dùng riêng cho SXKD chịu thuế GTGT và sử dụng cho các hoạt động cung cấp hàng hoá, dịch vụ không kê khai, nộp thuế GTGT đủ điều kiện khấu trừ thuế', N'Hàng hoá, dịch vụ dùng riêng cho SXKD chịu thuế GTGT và sử dụng cho các hoạt động cung cấp hàng hoá, dịch vụ không kê khai, nộp thuế GTGT đủ điều kiện khấu trừ thuế', 1, 0)
INSERT dbo.GoodsServicePurchase(ID, GoodsServicePurchaseCode, GoodsServicePurchaseName, Description, IsActive, IsSecurity) VALUES ('35d6384d-097c-45ca-ba5e-9ac5bee09372', N'4', N'Hàng hóa, dịch vụ không đủ điều kiện khấu trừ', N'Hàng hóa, dịch vụ không đủ điều kiện khấu trừ', 1, 0)
INSERT dbo.GoodsServicePurchase(ID, GoodsServicePurchaseCode, GoodsServicePurchaseName, Description, IsActive, IsSecurity) VALUES ('f013d981-8c0f-48a3-b51a-c484356a828b', N'5', N'Hàng hóa, dịch vụ không phải tổng hợp trên tờ khai 01/GTGT', N'Hàng hóa, dịch vụ không phải tổng hợp trên tờ khai 01/GTGT', 1, 0)
INSERT dbo.GoodsServicePurchase(ID, GoodsServicePurchaseCode, GoodsServicePurchaseName, Description, IsActive, IsSecurity) VALUES ('9852da2a-671b-41c3-bb0a-cead0ed66878', N'3', N'Hàng hóa, dịch vụ dùng cho dự án đầu tư đủ điều kiện khấu trừ thuế', N'Hàng hóa, dịch vụ dùng cho dự án đầu tư đủ điều kiện khấu trừ thuế', 1, 0)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.PersonalSalaryTax WHERE ID = '2c69576f-946c-438d-a671-0471c5596844'
DELETE dbo.PersonalSalaryTax WHERE ID = '3f12764c-c4b7-4a23-ba29-14d88d18c294'
DELETE dbo.PersonalSalaryTax WHERE ID = 'ece149ef-c962-446a-882c-18df3c4a7765'
DELETE dbo.PersonalSalaryTax WHERE ID = '925f2688-4ce6-42cd-9085-2645b4532608'
DELETE dbo.PersonalSalaryTax WHERE ID = '9be78b0f-7d88-475a-a2e4-28113e4dd23f'
DELETE dbo.PersonalSalaryTax WHERE ID = '7ace19eb-49d5-4fc7-b1a0-2b6088e458f7'
DELETE dbo.PersonalSalaryTax WHERE ID = '1a7e4230-18ab-423e-95e1-4f7ef5aa60b7'
DELETE dbo.PersonalSalaryTax WHERE ID = '05dc805e-eca5-4cbd-b373-6fba72bf89c8'
DELETE dbo.PersonalSalaryTax WHERE ID = '249f0a3f-6112-4790-8d8d-72b869771ea1'
DELETE dbo.PersonalSalaryTax WHERE ID = '7c9df1c0-db66-42b6-b275-97dfb713ec5e'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.Sys_User SET Description = N'', Province = N'', City = N'' WHERE userid = '5c2f6d39-e0c4-4d3e-8ab1-e00d51867ccc'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.Sys_UserRole WHERE ID = '2c79598a-c285-4d9e-aaec-fd31d1947ceb'

INSERT dbo.Sys_UserRole(ID, UserID, RoleID, BranchID) VALUES ('efa6a6fa-5540-4168-946c-337d2e660197', '5c2f6d39-e0c4-4d3e-8ab1-e00d51867ccc', 'b7e767cb-2731-434c-a513-61ed7497db6f', '00000000-0000-0000-0000-000000000000')
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.TemplateColumn WHERE ID = '658d3814-9e5d-497e-a5c2-0fa8f56fdedc'
DELETE dbo.TemplateColumn WHERE ID = '0fa3aaf1-ceb9-46f5-b2bc-1e87bb08db86'
DELETE dbo.TemplateColumn WHERE ID = '80240d0a-187a-4b3d-8a8e-2860aad13430'
DELETE dbo.TemplateColumn WHERE ID = '574fb1b9-cbcd-4cba-ae45-4e95663589de'
DELETE dbo.TemplateColumn WHERE ID = 'ddb58ce7-4e0e-4b72-8ff8-5b880052f6e5'
DELETE dbo.TemplateColumn WHERE ID = '4bc53312-3de1-48cb-81c9-777cc5f08dde'
DELETE dbo.TemplateColumn WHERE ID = '976b9c10-baa0-4778-a360-9b3430d26c38'
DELETE dbo.TemplateColumn WHERE ID = '0430b08b-88a8-411e-9ee0-b8925b2e86db'
DELETE dbo.TemplateColumn WHERE ID = '6151b83e-c060-49b0-92b8-c8b73c9fafe6'
DELETE dbo.TemplateColumn WHERE ID = '45490de6-7d7a-405c-8466-cb387c786e5c'
DELETE dbo.TemplateColumn WHERE ID = '8a23caae-3556-4890-beae-d56fbb052b5b'
DELETE dbo.TemplateColumn WHERE ID = '61a33a19-28de-4a88-bbd0-d90805c19b9c'
DELETE dbo.TemplateColumn WHERE ID = '108bab12-05a6-42f1-bfc0-e90ff0fbc393'
DELETE dbo.TemplateColumn WHERE ID = '8f1ab7de-193b-4e12-b3e6-ed3794d9579a'
DELETE dbo.TemplateColumn WHERE ID = '30f86513-14d7-49e5-a61f-f119ba8bc889'

UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = '139e47d2-3df1-4841-b9ac-04018f334e81'
UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = '0dc1209d-1490-4c95-9a67-042e8065228d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 19 WHERE ID = '8e7a294c-7ed3-4e17-8074-05563e40bcb3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 9 WHERE ID = 'dd6c6692-403d-4db8-88ce-0640cf0107e3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 22 WHERE ID = '92761a9f-c59f-49cb-82bb-07638b500229'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = '4d1db915-c1a5-412f-92a5-0bdb83e81ada'
UPDATE dbo.TemplateColumn SET VisiblePosition = 24 WHERE ID = 'be8ea4a2-295d-4882-9613-0bf345201ab3'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'fe79ec2b-c2d2-48ee-a002-0d543c6ecf41'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '7f12934b-fd26-4744-ab71-0e14ed174880'
UPDATE dbo.TemplateColumn SET VisiblePosition = 27 WHERE ID = '7cb93597-55d9-4bdb-aa0d-0f7e6117f454'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '6f54657d-2107-4f56-b403-105048a07bb1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 9 WHERE ID = '4d018c67-b2d7-4431-a2c5-1851bfabd241'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAmountOriginal' WHERE ID = '8f7033a4-7abd-4c26-8fed-189d11ed3cf7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 20 WHERE ID = 'b6ac3a74-ba68-4c19-8059-1c687299b9db'
UPDATE dbo.TemplateColumn SET VisiblePosition = 22 WHERE ID = 'f7e4247d-2f12-49d2-8101-1cdceb413ec1'
UPDATE dbo.TemplateColumn SET VisiblePosition = 25 WHERE ID = 'c8d414a7-5541-4659-ae3a-1d35f832ce3d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 29 WHERE ID = 'dc5b3231-0f54-4691-9777-1e6017c9604a'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'f9c8a8d9-582d-44ff-a40c-1ed5f2a08ccd'
UPDATE dbo.TemplateColumn SET VisiblePosition = 8 WHERE ID = '280779f5-10e4-4603-b799-22df6d8a1c54'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = '58af84c8-aa3e-40fb-8b5c-22e534c3a882'
UPDATE dbo.TemplateColumn SET VisiblePosition = 34 WHERE ID = '7800a513-ef4d-411c-9ca2-2708c5c46edf'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = 'c1094b5a-0eef-4c1c-bf1a-270d9b94062e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 32 WHERE ID = '672ede71-3b0a-48df-815e-2a1a374e0562'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Giá trị nhập kho ', ColumnToolTip = N'Giá trị nhập kho ', IsVisible = 1 WHERE ID = '7cb91b13-0672-471c-a220-2b59c62e8258'
UPDATE dbo.TemplateColumn SET VisiblePosition = 15 WHERE ID = 'c900564d-e5eb-4adf-a9e8-2bed8fa4a5f6'
UPDATE dbo.TemplateColumn SET VisiblePosition = 15 WHERE ID = 'b7e52dcd-b929-4e85-be61-2c4d11378102'
UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = 'be1fd93a-1e04-47e4-b184-2c6618ab135d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 20 WHERE ID = 'd82761ca-e628-4b12-ae04-2dde74aeb43c'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = 'f2fa8caa-78a6-41e6-bdcd-2f188727ef9f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = '755d5ecc-4187-4156-96ae-314c25879e94'
UPDATE dbo.TemplateColumn SET VisiblePosition = 7 WHERE ID = '533a0f89-122f-4109-b0c5-3566c696edb8'
UPDATE dbo.TemplateColumn SET VisiblePosition = 36 WHERE ID = '562b4802-daf5-4d11-9c8d-3ca413833c97'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Mức lương cơ sở', IsReadOnly = 1 WHERE ID = '386c9143-eb29-497e-bdaf-3cbabc7156ea'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = 'f32a711a-21d8-4fda-8069-3fe6a24daff6'
UPDATE dbo.TemplateColumn SET VisiblePosition = 6 WHERE ID = 'ddfdd7d1-0f18-475b-956f-401552337f40'
UPDATE dbo.TemplateColumn SET VisiblePosition = 0 WHERE ID = 'aacc7541-a1f0-4146-8e94-43e6f63f4349'
UPDATE dbo.TemplateColumn SET VisiblePosition = 30 WHERE ID = '9696b28b-2dc4-4d83-a187-460cf0b35836'
UPDATE dbo.TemplateColumn SET VisiblePosition = 4 WHERE ID = 'e787e987-4155-4e24-bd00-480c1402706e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = 'd5737345-8d36-46fc-8af8-50e32cb47e99'
UPDATE dbo.TemplateColumn SET VisiblePosition = 23 WHERE ID = 'b2487f36-3b79-4f37-a7ef-5455bf1dedd6'
UPDATE dbo.TemplateColumn SET VisiblePosition = 33 WHERE ID = '7abec48f-9876-472b-ad00-5488963f326d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = 'c32cb10a-3302-4c64-9e39-5523bd3139f5'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '6934c2d7-883e-4e63-8b1e-562c04ef044f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 28 WHERE ID = 'da02973a-b1fa-4e7f-82aa-573196806d90'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = 'ae0fdecf-4e35-483c-a256-578210e58c49'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '8489248c-ae8a-4900-a7e8-5836ce48fa67'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = '9a1bdad2-ddac-45c5-8d13-5afa6a9813ca'
UPDATE dbo.TemplateColumn SET VisiblePosition = 47 WHERE ID = '2ac09628-c69a-4379-b646-5b435968b31b'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Số tiền', IsVisible = 1 WHERE ID = 'bd0d1e1f-4b50-4af4-9853-5ed00bc9faea'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '9ec390c9-dcba-4946-af99-60713edf288e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = 'c8ffde3d-01b5-469e-b27c-613d8b05529e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 19 WHERE ID = '6bfe81bb-dbec-42da-86cb-625a6f655a46'
UPDATE dbo.TemplateColumn SET VisiblePosition = 20 WHERE ID = '8279b7a8-e3cc-445a-8587-64c93dcfcbba'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '83590253-5dfc-4e98-b9c4-6b5f8d18fd9a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 29 WHERE ID = 'd02f9e6c-9df4-4d91-a0dd-6c4d74205dbe'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Tên dịch vụ' WHERE ID = '6026aabc-326d-4b8c-abe7-6f3fb17dc132'
UPDATE dbo.TemplateColumn SET VisiblePosition = 31 WHERE ID = '24d541e7-db81-424f-87a5-702027a1e995'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = 'd2f3a587-f000-4591-89f8-72216cf04195'
UPDATE dbo.TemplateColumn SET VisiblePosition = 5 WHERE ID = 'c448401a-b3b8-4511-8286-72bf1197d09d'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '76b5ac8c-4640-4d34-9861-74373aee49c5'
UPDATE dbo.TemplateColumn SET VisiblePosition = 9 WHERE ID = 'b0b6a41b-db98-4f89-9f9a-7902c14d408e'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '66f65468-2152-4edb-a7db-7b39a783800d'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '55a2efdf-6869-4e8d-89f6-7f8fa6e79093'
UPDATE dbo.TemplateColumn SET VisiblePosition = 30 WHERE ID = '68d49081-1eb2-4dff-8f17-817727ca1fb8'
UPDATE dbo.TemplateColumn SET IsVisible = 1 WHERE ID = '6623c11e-a24c-45de-8905-81bd89efada0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 35 WHERE ID = 'ca26f1ff-e258-46e1-9a8c-82582cf15000'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '28c67fa5-678f-418e-a275-82602fb7b870'
UPDATE dbo.TemplateColumn SET VisiblePosition = 25 WHERE ID = 'd9ae52b2-424c-49c0-9160-83b4c18cd843'
UPDATE dbo.TemplateColumn SET VisiblePosition = 33 WHERE ID = '90f241c5-eeea-48e3-984f-85488c91281a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = 'b5eb2a1a-6dfb-4d31-9229-8592b551e377'
UPDATE dbo.TemplateColumn SET VisiblePosition = 19 WHERE ID = 'e87f47f2-b220-4798-92fc-891f6a0396e5'
UPDATE dbo.TemplateColumn SET VisiblePosition = 30 WHERE ID = 'dc4e00b5-dd8e-4ba5-9261-8986c201a3da'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'e089cf5c-c6f0-438e-b5ec-8a281a9cf4b0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 34 WHERE ID = '50e17f90-84f8-4653-b1cd-8bb3baeccc23'
UPDATE dbo.TemplateColumn SET VisiblePosition = 14 WHERE ID = '2dfe9015-2047-4d6a-9c45-8d3514ef41cf'
UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = '1214afc8-f06f-4f87-908c-8f4710629546'
UPDATE dbo.TemplateColumn SET VisiblePosition = 24 WHERE ID = '9b357bcb-e995-4aa8-bba5-920093aee74e'
UPDATE dbo.TemplateColumn SET VisiblePosition = 15 WHERE ID = '162518c2-1e1e-4faf-a130-926f69499c57'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '0dc8cc25-cf86-41ff-8b6c-95a3dc5eb964'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = 'deb7c8dd-c4fd-46c0-bff5-9723eddddab3'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '69a4ce7d-35ed-4b15-9927-98b31a2917e3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 22 WHERE ID = '802a2757-c1be-4a46-b548-9a994be324f8'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'b82c9686-de94-4708-a6b8-9fe815634407'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = 'd0684e3f-bb9e-414a-bee0-a0efa552be2c'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '1896faa5-53d2-4694-a305-a21435af97e9'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '43d64c40-9209-4681-a9ac-a22d3f24fc61'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = '06e7d4e3-eb13-46df-973c-a455428f9bb4'
UPDATE dbo.TemplateColumn SET VisiblePosition = 1 WHERE ID = 'd7e98032-f35a-49b1-92ab-a47db50a3fbd'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '46ae1068-1674-45fd-990b-a5effb3fcb7b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 11 WHERE ID = '7a0db77b-770b-43e4-a960-a83c039b9408'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = '3416992f-3c51-4319-a17a-a950a34a4eb0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 37 WHERE ID = '1bb44356-6d17-4008-9bdb-aa3df69a7c1d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 26 WHERE ID = '338b9132-a978-47cc-964c-ac74025f15a7'
UPDATE dbo.TemplateColumn SET VisiblePosition = 32 WHERE ID = 'fe65b3f3-4ec7-438c-8b9e-ac849bc67cc9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 18 WHERE ID = 'abb2d902-4294-4e65-a3e0-ade131eee2e2'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '80c3f29d-ae58-4816-a1da-adf57b2abd9a'
UPDATE dbo.TemplateColumn SET VisiblePosition = 26 WHERE ID = '4404c097-2e03-4eb3-96bf-ae57a9732a70'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'c2e6bbe8-e661-40b5-8127-b410c2aaf7ce'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Giá trị nhập kho', ColumnToolTip = N'Giá trị nhập kho', IsVisible = 1 WHERE ID = '8af69184-3e90-4148-9f12-b4abda0020ba'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'de3d8e9d-483d-4a1e-97ae-b84585d83265'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = 'dea0700a-4c13-41c3-bcae-b8a9e0191a08'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '6df8607a-9834-4857-99b7-bf995176779f'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Mức lương cơ sở', IsReadOnly = 1 WHERE ID = '05eb1c88-0ff3-49c5-a65c-c2bf246affb5'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '90077261-f9cf-4e97-abae-c2f9c7394d18'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Chi phí mua', ColumnToolTip = N'', IsVisible = 1 WHERE ID = '57b66c5c-bef5-463f-b3b0-c41b264d08a4'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Giá trị nhập kho ', IsVisible = 1 WHERE ID = '19510d22-abb1-4b6c-b149-c6d830f59bdc'
UPDATE dbo.TemplateColumn SET VisiblePosition = 16 WHERE ID = '5045b3df-620d-41d3-baaa-c939a9b7e0a0'
UPDATE dbo.TemplateColumn SET VisiblePosition = 35 WHERE ID = 'bc5d9beb-2603-488a-a9f5-c9dc19a42c01'
UPDATE dbo.TemplateColumn SET IsReadOnly = 1 WHERE ID = 'ba090443-4451-4838-ae4b-ca9982be3edf'
UPDATE dbo.TemplateColumn SET VisiblePosition = 28 WHERE ID = 'ca492947-a31d-40e6-81fa-cbd81a8fad2b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 25 WHERE ID = '9d5c0985-d5ec-4ed1-9449-cc18e6910283'
UPDATE dbo.TemplateColumn SET VisiblePosition = 21 WHERE ID = '4829b99f-f7d0-4b6e-a500-cc97a3026d84'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '50c063c5-cfaa-4258-a74c-cf6defc07065'
UPDATE dbo.TemplateColumn SET VisiblePosition = 37 WHERE ID = '1f77c143-24ec-466d-8b3c-d41f1d34928b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 31 WHERE ID = 'bc45d7d8-25f7-4953-b064-dba86b1469fd'
UPDATE dbo.TemplateColumn SET ColumnName = N'OWPrice' WHERE ID = 'a3ead1a9-d839-4722-9940-dbb82d1e7193'
UPDATE dbo.TemplateColumn SET VisiblePosition = 3 WHERE ID = '0d42d44e-8713-4b53-801d-dca4b0619121'
UPDATE dbo.TemplateColumn SET ColumnName = N'TotalAmount', ColumnWidth = 120 WHERE ID = '52462b65-0eca-430c-b7a5-df853921e2be'
UPDATE dbo.TemplateColumn SET VisiblePosition = 13 WHERE ID = '876b9945-84f0-4892-9a2c-dfb55dd6e361'
UPDATE dbo.TemplateColumn SET IsVisible = 0 WHERE ID = '2fee27be-1f8c-497d-b49c-e0f9c171b720'
UPDATE dbo.TemplateColumn SET VisiblePosition = 12 WHERE ID = 'ef635838-c1f6-4437-8c55-e2f98fd93423'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '29eb13d8-0461-4d13-82e7-e42f3fc77c03'
UPDATE dbo.TemplateColumn SET VisiblePosition = 28 WHERE ID = 'c7ee5893-b26c-4c37-a4e0-e5475c279e7f'
UPDATE dbo.TemplateColumn SET IsVisible = 0, VisiblePosition = 48 WHERE ID = '972e547e-cb1a-461f-9b9d-e839bca07fb3'
UPDATE dbo.TemplateColumn SET VisiblePosition = 10 WHERE ID = '6d20c34a-9093-4507-a937-ebb069643d63'
UPDATE dbo.TemplateColumn SET VisiblePosition = 27 WHERE ID = '4bdb1cb0-bb85-4cec-99bd-ec1f74234ecc'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '9fabfc23-8f96-4587-8354-ed02a89df8b5'
UPDATE dbo.TemplateColumn SET ColumnCaption = N'Mức lương cơ sở', IsReadOnly = 1 WHERE ID = '665dd1ee-b304-440f-b513-edc6177226ac'
UPDATE dbo.TemplateColumn SET VisiblePosition = 23 WHERE ID = '09bcfe80-a8d6-4332-9fd5-f02f03e4c71d'
UPDATE dbo.TemplateColumn SET VisiblePosition = 24 WHERE ID = '529e059c-21e5-48b8-8494-f0c782b19dd9'
UPDATE dbo.TemplateColumn SET VisiblePosition = 2 WHERE ID = '0485fccf-741b-453e-90ce-f107cbf79114'
UPDATE dbo.TemplateColumn SET ColumnName = N'OWAmount' WHERE ID = 'bdc8c8e1-e66e-4d49-9ea9-f3c649c75a1b'
UPDATE dbo.TemplateColumn SET VisiblePosition = 23 WHERE ID = 'af6cea6d-3d81-46a7-80bc-f710c956699f'
UPDATE dbo.TemplateColumn SET VisiblePosition = 26 WHERE ID = '5b958334-92f3-4672-914a-f82d3cd49541'
UPDATE dbo.TemplateColumn SET VisiblePosition = 29 WHERE ID = 'e760739e-79ee-4637-83b5-f880fe51d828'
UPDATE dbo.TemplateColumn SET IsColumnHeader = 1 WHERE ID = '8bb0965e-9510-4be4-9450-fa1a5a3cde08'
UPDATE dbo.TemplateColumn SET VisiblePosition = 17 WHERE ID = '570b0a1d-f146-45ea-aa98-fa60717b1277'
UPDATE dbo.TemplateColumn SET VisiblePosition = 27 WHERE ID = '7f2d02a1-29be-459e-933f-fd5688260071'

INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('e96eeaba-32be-43dd-8a55-417dd409fd3f', '3e593632-00fe-4f80-8a2e-ade50cf8393f', N'BasicWageAmount', N'Lương cơ bản', NULL, 0, 1, 150, 0, 0, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('8a270a37-5820-4193-90c1-54faa00d40b2', '88cbea4d-8a5d-4308-82b4-32d9ccf3d6dc', N'BasicWageAmount', N'Lương cơ bản', NULL, 0, 1, 150, 0, 0, 1, 8)
INSERT dbo.TemplateColumn(ID, TemplateDetailID, ColumnName, ColumnCaption, ColumnToolTip, IsColumnHeader, IsReadOnly, ColumnWidth, ColumnMaxWidth, ColumnMinWidth, IsVisible, VisiblePosition) VALUES ('d6b0d0bf-6840-4b45-aea3-b07c16447077', '7ed3ec3d-d7c0-4216-8e9d-e9f9182482e2', N'BasicWageAmount', N'Lương cơ bản', NULL, 0, 1, 150, 0, 0, 1, 8)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.TemplateDetail SET TabIndex = 0, TabCaption = N'&1. Hóa đơn' WHERE ID = '2ff0b5d1-d31d-4ffc-9e0e-8e9d88d6f7ff'
UPDATE dbo.TemplateDetail SET TabIndex = 1, TabCaption = N'&2. Hạch toán' WHERE ID = '68ef2038-352d-4297-834d-a3e7b41d6438'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.TimeSheetSymbols SET SalaryRate = 200.0000000000 WHERE ID = '766b5dcf-acda-4966-b450-1f6960d75e8c'
UPDATE dbo.TimeSheetSymbols SET SalaryRate = 300.0000000000 WHERE ID = 'a85bbb53-094a-4891-94f8-2e6dbd3da02a'
UPDATE dbo.TimeSheetSymbols SET SalaryRate = 300.0000000000 WHERE ID = '49f691a7-16f0-4bc9-99a7-420738acc241'
UPDATE dbo.TimeSheetSymbols SET OverTimeSymbol = -1 WHERE ID = 'b809cddd-4785-45d1-b0d4-5dcdd62c1701'
UPDATE dbo.TimeSheetSymbols SET SalaryRate = 200.0000000000 WHERE ID = 'e8926ef4-35f4-4dcc-878e-70068739cac9'
UPDATE dbo.TimeSheetSymbols SET SalaryRate = 200.0000000000 WHERE ID = '91e7f7de-0a7e-4a19-9528-89848fa7cdfa'
UPDATE dbo.TimeSheetSymbols SET SalaryRate = 100.0000000000 WHERE ID = 'd5cce21b-f81e-49e7-81ce-9ef3adca908d'
UPDATE dbo.TimeSheetSymbols SET SalaryRate = 100.0000000000, IsOverTime = 1, OverTimeSymbol = -1 WHERE ID = '7070de97-02fd-46ee-909c-a391a4a91b5d'
UPDATE dbo.TimeSheetSymbols SET SalaryRate = 50.0000000000, OverTimeSymbol = -1 WHERE ID = 'ff8193eb-8792-4c40-9996-d58bc9463bbc'
UPDATE dbo.TimeSheetSymbols SET SalaryRate = 50.0000000000, OverTimeSymbol = -1 WHERE ID = 'f7c73a86-9fc9-4228-a464-e64ed369fe50'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.Type SET TypeGroupID = 50 WHERE ID = 119
UPDATE dbo.Type SET TypeGroupID = 50 WHERE ID = 129
UPDATE dbo.Type SET TypeGroupID = 50 WHERE ID = 132
UPDATE dbo.Type SET TypeGroupID = 50 WHERE ID = 142
UPDATE dbo.Type SET TypeGroupID = 50 WHERE ID = 172
UPDATE dbo.Type SET TypeGroupID = 60 WHERE ID = 840
UPDATE dbo.Type SET TypeGroupID = 430 WHERE ID = 902
UPDATE dbo.Type SET TypeGroupID = 430 WHERE ID = 903
UPDATE dbo.Type SET TypeGroupID = 430 WHERE ID = 904
UPDATE dbo.Type SET TypeGroupID = 430 WHERE ID = 905
UPDATE dbo.Type SET TypeGroupID = 430 WHERE ID = 906
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

SET IDENTITY_INSERT dbo.VoucherPatternsReport ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (207, 0, 0, N'FADepreciation-ChungTuKT', N'Chứng từ kế toán', N'FADepreciation-ChungTuKT.rst', 1, N'540')
GO
SET IDENTITY_INSERT dbo.VoucherPatternsReport OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DBCC CHECKIDENT('dbo.VoucherPatternsReport', RESEED, 207)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateDetail WITH NOCHECK
  ADD CONSTRAINT FK_TemplateDetail_Template FOREIGN KEY (TemplateID) REFERENCES dbo.Template(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Type WITH NOCHECK
  ADD CONSTRAINT FK_Type_TypeGroup FOREIGN KEY (TypeGroupID) REFERENCES dbo.TypeGroup(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.Template WITH NOCHECK
  ADD CONSTRAINT FK_Template_Type FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153AdjustAnnouncement WITH NOCHECK
  ADD CONSTRAINT FK__TT153Adju__TypeI__25869641 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153DeletedInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Dele__TypeI__61F08603 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153DestructionInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Dest__TypeI__22AA2996 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153LostInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Lost__TypeI__1FCDBCEB FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153PublishInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Publ__TypeI__1CF15040 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TT153RegisterInvoice WITH NOCHECK
  ADD CONSTRAINT FK__TT153Regi__TypeI__1A14E395 FOREIGN KEY (TypeID) REFERENCES dbo.Type(ID) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

GO
ENABLE TRIGGER ALL ON dbo.TemplateColumn
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END


IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF
GO
