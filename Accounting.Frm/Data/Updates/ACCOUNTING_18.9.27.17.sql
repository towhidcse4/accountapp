SET CONCAT_NULL_YIELDS_NULL, ANSI_NULLS, ANSI_PADDING, QUOTED_IDENTIFIER, ANSI_WARNINGS, ARITHABORT, XACT_ABORT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS OFF
GO

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO

DROP FUNCTION [dbo].[XXXXXXXXX]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[SupplierService] (
  [ID] [uniqueidentifier] NOT NULL,
  [SupplierServiceCode] [nvarchar](512) NULL,
  [SupplierServiceName] [nvarchar](512) NULL,
  [PathAccess] [nvarchar](512) NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

CREATE TABLE [dbo].[ApiService] (
  [ID] [uniqueidentifier] NOT NULL,
  [SupplierServiceID] [uniqueidentifier] NOT NULL,
  [SupplierServiceCode] [nvarchar](512) NULL,
  [ApiName] [nvarchar](512) NULL,
  [ApiPath] [nvarchar](512) NULL,
  PRIMARY KEY CLUSTERED ([ID])
)
ON [PRIMARY]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153RegisterInvoice]
  DROP CONSTRAINT [FK__TT153Regi__TypeI__1A14E395]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153RegisterInvoice]
  ADD FOREIGN KEY ([TypeID]) REFERENCES [dbo].[Type] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153RegisterInvoice] WITH NOCHECK
  ADD CONSTRAINT [FK__TT153Regi__TypeI__01F34141] FOREIGN KEY ([TypeID]) REFERENCES [dbo].[Type] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153PublishInvoice]
  DROP CONSTRAINT [FK__TT153Publ__TypeI__1CF15040]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153PublishInvoice]
  ADD FOREIGN KEY ([TypeID]) REFERENCES [dbo].[Type] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153PublishInvoice] WITH NOCHECK
  ADD CONSTRAINT [FK__TT153Publ__TypeI__04CFADEC] FOREIGN KEY ([TypeID]) REFERENCES [dbo].[Type] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153LostInvoice]
  DROP CONSTRAINT [FK__TT153Lost__TypeI__1FCDBCEB]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153LostInvoice]
  ADD FOREIGN KEY ([TypeID]) REFERENCES [dbo].[Type] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153LostInvoice] WITH NOCHECK
  ADD CONSTRAINT [FK__TT153Lost__TypeI__07AC1A97] FOREIGN KEY ([TypeID]) REFERENCES [dbo].[Type] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153DestructionInvoice]
  DROP CONSTRAINT [FK__TT153Dest__TypeI__22AA2996]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153DestructionInvoice]
  ADD [Hour] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153DestructionInvoice]
  ADD [Min] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153DestructionInvoice]
  ADD [Scheduler] [nvarchar](512) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153DestructionInvoice]
  ADD FOREIGN KEY ([TypeID]) REFERENCES [dbo].[Type] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153DestructionInvoice] WITH NOCHECK
  ADD CONSTRAINT [FK__TT153Dest__TypeI__0A888742] FOREIGN KEY ([TypeID]) REFERENCES [dbo].[Type] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153AdjustAnnouncement]
  DROP CONSTRAINT [FK__TT153Adju__TypeI__25869641]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153AdjustAnnouncement]
  ADD FOREIGN KEY ([TypeID]) REFERENCES [dbo].[Type] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153AdjustAnnouncement] WITH NOCHECK
  ADD CONSTRAINT [FK__TT153Adju__TypeI__0D64F3ED] FOREIGN KEY ([TypeID]) REFERENCES [dbo].[Type] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TIIncrementDetail]
  ADD [TaxExchangeRate] [decimal](25, 10) NULL CONSTRAINT [DF_TIIncrementDetail_TaxExchangeRate] DEFAULT (NULL)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'TIIncrementDetail', 'COLUMN', N'TaxExchangeRate'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoice]
  ADD [StatusInvoice] [int] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoice]
  ADD [StatusSendMail] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SAInvoice]
  ADD [StatusCusView] [bit] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[MBCreditCardDetailTax]
  ADD [InvoiceTemplate] [nvarchar](25) NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153Report]
  DROP CONSTRAINT [FK__TT153Repo__Invoi__473C8FC7]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153Report] WITH NOCHECK
  ADD CONSTRAINT [FK__TT153Repo__Invoi__31F75A1E] FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153Report]
  ADD FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153PublishInvoiceDetail]
  DROP CONSTRAINT [FK__TT153Publ__Invoi__4DE98D56]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153PublishInvoiceDetail] WITH NOCHECK
  ADD CONSTRAINT [FK__TT153Publ__Invoi__38A457AD] FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153PublishInvoiceDetail]
  ADD FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153LostInvoiceDetail]
  DROP CONSTRAINT [FK__TT153Lost__Invoi__52AE4273]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153LostInvoiceDetail] WITH NOCHECK
  ADD CONSTRAINT [FK__TT153Lost__Invoi__3D690CCA] FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153LostInvoiceDetail]
  ADD FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153DeletedInvoice]
  DROP CONSTRAINT [FK__TT153Dele__Invoi__5772F790]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153DeletedInvoice] WITH NOCHECK
  ADD CONSTRAINT [FK__TT153Dele__Invoi__422DC1E7] FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153DeletedInvoice]
  ADD FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153AdjustAnnouncementDetailListInvoice]
  DROP CONSTRAINT [FK__TT153Adju__Invoi__5C37ACAD]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153AdjustAnnouncementDetailListInvoice] WITH NOCHECK
  ADD CONSTRAINT [FK__TT153Adju__Invoi__46F27704] FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[TT153AdjustAnnouncementDetailListInvoice]
  ADD FOREIGN KEY ([InvoiceTypeID]) REFERENCES [dbo].[InvoiceType] ([ID])
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[GOtherVoucher]
  ADD [CPAcceptanceID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER FUNCTION [dbo].[Func_GetB02_DN]
    (
      @BranchID UNIQUEIDENTIFIER ,
      @IncludeDependentBranch BIT ,
      @FromDate DATETIME ,
      @ToDate DATETIME ,
      @PrevFromDate DATETIME ,
      @PrevToDate DATETIME
    )
RETURNS @Result TABLE
    (
      ItemID UNIQUEIDENTIFIER ,
      ItemCode NVARCHAR(25) ,
      ItemName NVARCHAR(255) ,
      ItemNameEnglish NVARCHAR(255) ,
      ItemIndex INT ,
      Description NVARCHAR(255) ,
      FormulaType INT ,
      FormulaFrontEnd NVARCHAR(MAX) ,
      Formula XML ,
      Hidden BIT ,
      IsBold BIT ,
      IsItalic BIT
      ,
      Amount DECIMAL(25, 4) ,
      PrevAmount DECIMAL(25, 4)
    )
AS 
    BEGIN

        DECLARE @AccountingSystem INT	
            SET @AccountingSystem = 48
        DECLARE @ReportID NVARCHAR(100)
        SET @ReportID = '2'/*báo cáo kết quả HĐ kinh doanh*/
	
        DECLARE @tblItem TABLE
            (
              ItemID UNIQUEIDENTIFIER ,
              ItemName NVARCHAR(255) ,
              OperationSign INT ,
              OperandString NVARCHAR(255) ,
              AccountNumber VARCHAR(25) ,
              CorrespondingAccountNumber VARCHAR(25) ,
              IsDetailGreaterThanZero BIT
	      )
	
        INSERT  @tblItem
                SELECT  ItemID ,
                        ItemName ,
                        x.r.value('@OperationSign', 'INT') ,
                        x.r.value('@OperandString', 'nvarchar(255)') ,
                        x.r.value('@AccountNumber', 'nvarchar(25)') + '%' ,
                        CASE WHEN x.r.value('@CorrespondingAccountNumber',
                                            'nvarchar(25)') <> ''
                             THEN x.r.value('@CorrespondingAccountNumber',
                                            'nvarchar(25)') + '%'
                             ELSE ''
                        END ,
                        CASE WHEN FormulaType = 0 THEN CAST(0 AS BIT)
                             ELSE CAST(1 AS BIT)
                        END
                FROM    FRTemplate
                        CROSS APPLY Formula.nodes('/root/DetailFormula') AS x ( r )
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
                        AND ( FormulaType = 0/*chỉ tiêu chi tiết*/
                              OR FormulaType = 2/*chỉ tiêu chi tiết chỉ được lấy số liệu khi kết quả>0*/
                            )
                        AND Formula IS NOT NULL
						and itemcode not in ('31.1', '31.2', '32.1' , '32.2')
	
        DECLARE @Balance TABLE
            (
              AccountNumber NVARCHAR(25) ,
              CorrespondingAccountNumber NVARCHAR(25) ,
              PostedDate DATETIME ,
              CreditAmount DECIMAL(25, 4) ,
              DebitAmount DECIMAL(25, 4) ,
              CreditAmountDetailBy DECIMAL(25, 4) ,
              DebitAmountDetailBy DECIMAL(25, 4) ,
              IsDetailBy BIT,
              /*add by hoant 16.09.2016 theo cr 116741*/
                CreditAmountByBussinessType0 DECIMAL(25, 4) ,/*Có Chi tiết theo loại  0-Chiết khấu thương mai*/
               CreditAmountByBussinessType1 DECIMAL(25, 4) ,/*Có Chi tiết theo loại  1 - Giảm giá hàng bán*/
               CreditAmountByBussinessType2 DECIMAL(25, 4) ,/*Có Chi tiết theo loại   2- trả lại hàng bán*/
              DebitAmountByBussinessType0 DECIMAL(25, 4) ,/*Nợ Chi tiết theo loại  0-Chiết khấu thương mai*/
              DebitAmountByBussinessType1 DECIMAL(25, 4) ,/*Nợ Chi tiết theo loại  1 - Giảm giá hàng bán*/
              DebitAmountByBussinessType2 DECIMAL(25, 4) /*Nợ Chi tiết theo loại   2- trả lại hàng bán*/
            )			
/*
1. Doanh thu bán hàng và cung cấp dịch vụ

PS Có TK 511 (không kể PSĐƯ N911/C511, không kể các nghiệp vụ: Chiết khấu thương mại (bán hàng),

 Giảm giá hàng bán, Trả lại hàng bán)
  – PS Nợ TK 511 (không kể PSĐƯ N511/911, không kể các nghiệp vụ: Chiết khấu thương mại (bán hàng), 
  Giảm giá hàng bán, Trả lại hàng bán)
*/
		
        INSERT  INTO @Balance
                SELECT  GL.Account ,
                        GL.AccountCorresponding ,
                        GL.PostedDate ,
                        SUM(ISNULL(CreditAmount, 0)) AS CreditAmount ,
                        SUM(ISNULL(DebitAmount, 0)) AS DebitAmount ,
                        0 AS CreditAmountDetailBy ,
                        0 AS DebitAmountDetailBy ,
                        0,
                        SUM(CASE WHEN (GL.TypeID in ('320','321','322','323','324','325','350','360') and gl.Account like '511%')
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType0 ,
                        SUM(CASE WHEN GL.TypeID =340
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType1 
                            ,
                        SUM(CASE WHEN GL.TypeID =330
                                 THEN ISNULL(CreditAmount, 0)
                                 ELSE 0
                            END) AS CreditAmountByBussinessType2 ,
                        SUM(CASE WHEN (GL.TypeID in ('320','321','322','323','324','325','350','360') and gl.Account like '511%')
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType0
                            ,
                        SUM(CASE WHEN GL.TypeID =340
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType1
                            ,
                        SUM(CASE WHEN GL.TypeID =330
                                 THEN ISNULL(DebitAmount, 0)
                                 ELSE 0
                            END) AS DebitAmountByBussinessType2
                FROM    dbo.GeneralLedger GL
                WHERE   PostedDate BETWEEN @PrevFromDate AND @ToDate
                GROUP BY GL.Account ,
                        GL.AccountCorresponding,
                        GL.PostedDate                     
        OPTION  ( RECOMPILE )


        DECLARE @tblMasterDetail TABLE
            (
              ItemID UNIQUEIDENTIFIER ,
              DetailItemID UNIQUEIDENTIFIER ,
              OperationSign INT ,
              Grade INT ,
              DebitAmount DECIMAL(25, 4) ,
              PrevDebitAmount DECIMAL(25, 4)
            )	
	
	
	/*
	PhatsinhCO(511) - PhatsinhDU(911/511) + PhatsinhCO_ChitietChietKhauThuongmai(511)
	 + PhatsinhCO_ChitietGiamgiaHangBan(511) + PhatsinhCO_ChitietTralaiHangBan(511) +
	  PhatsinhNO(511) - PhatsinhDU(511/911) - PhatsinhNO_ChitietChietKhauThuongmai(511) - PhatsinhNO_ChitietGiamgiaHangBan(511) - PhatsinhNO_ChitietTralaiHangBan(511)
	*/
        INSERT  INTO @tblMasterDetail
                SELECT  I.ItemID ,
                        NULL ,
                        1 ,
                        -1
                        ,
                        CASE WHEN I.IsDetailGreaterThanZero = 0
                                  OR ( SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                                THEN ( CASE WHEN I.OperandString = 'PhatsinhCO'
                                                            THEN GL.CreditAmount
                                                            WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                            THEN GL.DebitAmount
                                                        
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                        
                                                              
                                                            WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                              AND GL.IsDetailBy = 1
                                                            THEN GL.CreditAmountDetailBy
                                                            WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                              AND GL.IsDetailBy = 1
                                                            THEN GL.DebitAmountDetailBy
                                                            ELSE 0
                                                       END ) * I.OperationSign
                                                ELSE 0
                                           END) ) > 0
                             THEN ( SUM(CASE WHEN GL.PostedDate BETWEEN @FromDate AND @ToDate
                                             THEN ( CASE WHEN I.OperandString = 'PhatsinhCO'
                                                         THEN GL.CreditAmount
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                         THEN GL.DebitAmount
                                                    
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                     
                                                              
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                              AND GL.IsDetailBy = 1
                                                         THEN GL.CreditAmountDetailBy
                                                         WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                              AND GL.IsDetailBy = 1
                                                         THEN GL.DebitAmountDetailBy
                                                         ELSE 0
                                                    END ) * I.OperationSign
                                             ELSE 0
                                        END) )
                             ELSE 0
                        END AS DebitAmount ,
                        CASE WHEN I.IsDetailGreaterThanZero = 0
                                  OR ( SUM(CASE WHEN GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                                THEN CASE WHEN I.OperandString = 'PhatsinhCO'
                                                          THEN GL.CreditAmount
                                                          WHEN I.OperandString IN (
                                                              'PhatsinhNO',
                                                              'PhatsinhDU' )
                                                          THEN GL.DebitAmount
            
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                                         
                                                              
                                                              
                                                          WHEN I.OperandString IN (
                                                              'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                              AND GL.IsDetailBy = 1
                                                          THEN GL.CreditAmountDetailBy
                                                          WHEN I.OperandString IN (
                                                              'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                              AND GL.IsDetailBy = 1
                                                          THEN GL.DebitAmountDetailBy
                                                          ELSE 0
                                                     END * I.OperationSign
                                                ELSE 0
                                           END) ) > 0
                             THEN SUM(CASE WHEN GL.PostedDate BETWEEN @PrevFromDate
                                                              AND
                                                              @PrevToDate
                                           THEN CASE WHEN I.OperandString = 'PhatsinhCO'
                                                     THEN GL.CreditAmount
                                                     WHEN I.OperandString IN (
                                                          'PhatsinhNO',
                                                          'PhatsinhDU' )
                                                     THEN GL.DebitAmount
                                                     
                                                            WHEN I.OperandString = 'PhatsinhNO_ChitietChietKhauThuongmai' THEN 
                                                              GL.DebitAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietGiamgiaHangBan' THEN 
                                                              GL.DebitAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhNO_ChitietTralaiHangBan' THEN 
                                                              GL.DebitAmountByBussinessType2
                                                            WHEN I.OperandString = 'PhatsinhCO_ChitietChietKhauThuongmai1111' THEN 
                                                              GL.CreditAmountByBussinessType0
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietGiamgiaHangBan' THEN 
                                                              GL.CreditAmountByBussinessType1
                                                              WHEN I.OperandString = 'PhatsinhCO_ChitietTralaiHangBan' THEN 
                                                              GL.CreditAmountByBussinessType2
                                              
                                                              
                                                              
                                                     WHEN I.OperandString IN (
                                                          'PhatsinhCO_ChiTietThanhlyTSCD_BDSDT' )
                                                          AND GL.IsDetailBy = 1
                                                     THEN GL.CreditAmountDetailBy
                                                     WHEN I.OperandString IN (
                                                          'PhatsinhNO_ChiTietThanhlyTSCD_BDSDT' )
                                                          AND GL.IsDetailBy = 1
                                                     THEN GL.DebitAmountDetailBy
                                                     ELSE 0
                                                END * I.OperationSign
                                           ELSE 0
                                      END)
                             ELSE 0
                        END AS DebitAmount
                FROM    @tblItem I
                        INNER JOIN @Balance AS GL ON ( GL.AccountNumber LIKE I.AccountNumber )
                                                     AND ( I.OperandString <> 'PhatsinhDU'
                                                           OR ( I.OperandString = 'PhatsinhDU'
                                                              AND GL.CorrespondingAccountNumber LIKE I.CorrespondingAccountNumber
                                                              )
                                                         )
                GROUP BY I.ItemID ,
                        I.IsDetailGreaterThanZero
        OPTION  ( RECOMPILE )
			
        INSERT  @tblMasterDetail
                SELECT  ItemID ,
                        x.r.value('@ItemID', 'UNIQUEIDENTIFIER') ,
                        x.r.value('@OperationSign', 'INT') ,
                        0 ,
                        0.0 ,
                        0.0
                FROM    FRTemplate
                        CROSS APPLY Formula.nodes('/root/MasterFormula') AS x ( r )
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
                        AND FormulaType = 1
                        AND Formula IS NOT NULL 
						and itemcode not in ('31.1', '31.2', '32.1' , '32.2')
	
	

	;
        WITH    V ( ItemID, DetailItemID, DebitAmount, PrevDebitAmount, OperationSign )
                  AS ( SELECT   ItemID ,
                                DetailItemID ,
                                DebitAmount ,
                                PrevDebitAmount ,
                                OperationSign
                       FROM     @tblMasterDetail
                       WHERE    Grade = -1
                       UNION ALL
                       SELECT   B.ItemID ,
                                B.DetailItemID ,
                                V.DebitAmount ,
                                V.PrevDebitAmount ,
                                B.OperationSign * V.OperationSign AS OperationSign
                       FROM     @tblMasterDetail B ,
                                V
                       WHERE    B.DetailItemID = V.ItemID
                     )
	INSERT    @Result
                SELECT  FR.ItemID ,
                        FR.ItemCode ,
                        FR.ItemName ,
                        FR.ItemNameEnglish ,
                        FR.ItemIndex ,
                        FR.Description ,
                        FR.FormulaType ,
                        CASE WHEN FR.FormulaType = 1 THEN FR.FormulaFrontEnd
                             ELSE ''
                        END AS FormulaFrontEnd ,
                        CASE WHEN FR.FormulaType = 1 THEN FR.Formula
                             ELSE NULL
                        END AS Formula ,
                        FR.Hidden ,
                        FR.IsBold ,
                        FR.IsItalic ,
                        ISNULL(X.Amount, 0) AS Amount ,
                        ISNULL(X.PrevAmount, 0) AS PrevAmount
                FROM    ( SELECT    V.ItemID ,
                                    SUM(V.OperationSign * V.DebitAmount) AS Amount ,
                                    SUM(V.OperationSign * V.PrevDebitAmount) AS PrevAmount
                          FROM      V
                          GROUP BY  ItemID
                        ) AS X
                        RIGHT JOIN dbo.FRTemplate FR ON FR.ItemID = X.ItemID
                WHERE   AccountingSystem = @AccountingSystem
                        AND ReportID = @ReportID
						and itemcode not in ('31.1', '31.2', '32.1' , '32.2')
                ORDER BY ItemIndex
	
        RETURN 
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[FAIncrementDetail]
  ADD [TaxExchangeRate] [decimal](25, 10) NULL CONSTRAINT [DF_FAIncrementDetail_TaxExchangeRate] DEFAULT (NULL)
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

DECLARE @value SQL_VARIANT = CAST('' AS varchar(1))
EXEC sys.sp_addextendedproperty N'MS_Description', @value, 'SCHEMA', N'dbo', 'TABLE', N'FAIncrementDetail', 'COLUMN', N'TaxExchangeRate'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

EXEC sp_refreshview '[dbo].[ViewVouchersCloseBook]'
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[CPAcceptanceDetail]
  ADD [ContractID] [uniqueidentifier] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[CPAcceptance]
  ADD [OrderPriority] [int] NULL
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_PO_DiaryBook]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @BranchID UNIQUEIDENTIFIER = NULL ,
    @IncludeDependentBranch BIT = 0 ,
    @IsNotPaid BIT = 0
AS 
    BEGIN
        
        DECLARE @listRefType NVARCHAR(MAX)
        IF @IsNotPaid = 1
            SET @listRefType = '210, 240, 430, 500'
        ELSE 
            SET @listRefType = '210,116,117,119, 126, 127, 129,131, 132, 141, 142, 143, 171, 172, 173, 260, 261, 262, 263, 264, 902, 903, 904, 905, 906'
           
        SELECT  ROW_NUMBER() OVER ( ORDER BY PL.PostedDate , PL.RefDate , PL.RefNo ) AS RowNum ,
                PL.ReferenceID ,
                TypeID ,
                PL.PostedDate ,
                PL.RefDate ,
                PL.RefNo ,
                PL.InvoiceDate ,
                PL.InvoiceNo ,
                PL.Description ,
                SUM(CASE WHEN LEFT(Account, 3) = N'156'
                              
                         THEN DebitAmount
                         ELSE $0
                    END) AS GoodsAmount ,
                SUM(CASE WHEN LEFT(Account, 3) = N'152'
                         THEN ( DebitAmount )
                         ELSE $0
                    END) AS EquipmentAmount ,
                ( CASE WHEN LEFT(Account, 3) <> N'152'
                            AND LEFT(Account, 3) <> N'156'
                            AND LEFT(Account, 3) <> N'1331'
                       THEN Account
                       ELSE NULL
                  END ) AS AnotherAccount , 
                SUM(CASE WHEN LEFT(Account, 3) <> N'152'
                              AND LEFT(Account, 3) <> N'156'
                              AND LEFT(Account, 3) <> N'1331'
                         THEN (DebitAmount)
                         ELSE $0
                    END) AS AnotherAmount , 
                SUM(CASE WHEN LEFT(Account, 3) = N'156'
                         THEN DebitAmount
                         ELSE $0
                    END
                    + CASE WHEN LEFT(Account, 3) = N'152'
                         THEN ( DebitAmount )
                         ELSE $0
                    END
                    + CASE WHEN LEFT(Account, 3) <> N'152'
                              AND LEFT(Account, 3) <> N'156'
                              AND LEFT(Account, 3) <> N'1331'
                         THEN (DebitAmount)
                         ELSE $0
                    END) AS PaymentableAmount ,
                AO.AccountingObjectCode , 
                CASE WHEN PL.TypeID IN ( 330, 331, 332, 333, 334, 352, 357,
                                          358, 359, 360, 362, 363, 364, 365,
                                          366, 368, 369, 370, 371, 372, 374,
                                          375, 376, 377, 378 )
                     THEN null 
                     ELSE AO.AccountingObjectName
                END AS AccountObjectNameDI 
        FROM    dbo.GeneralLedger AS PL,
		        dbo.AccountingObject as AO
        WHERE   PL.PostedDate BETWEEN @FromDate AND @ToDate
		and PL.AccountingObjectID = Ao.ID
                AND @listRefType LIKE N'%' + CAST(PL.TypeID AS NVARCHAR(4))
                + N'%'
        and Account not in ('1331')
        GROUP BY PL.ReferenceID ,
                PL.TypeID ,
                PL.PostedDate ,
                PL.RefDate ,
                PL.RefNo ,
                PL.InvoiceDate ,
                PL.InvoiceNo ,
                PL.Description ,
                AO.AccountingObjectCode ,
				 ( CASE WHEN LEFT(Account, 3) <> N'152'
                            AND LEFT(Account, 3) <> N'156'
                            AND LEFT(Account, 3) <> N'1331'
                       THEN Account
                       ELSE NULL
                  END ),
				CASE WHEN PL.TypeID IN ( 330, 331, 332, 333, 334, 352, 357,
                                          358, 359, 360, 362, 363, 364, 365,
                                          366, 368, 369, 370, 371, 372, 374,
                                          375, 376, 377, 378 )
                     THEN null
                     ELSE AO.AccountingObjectName
                END 
        HAVING  sum(CASE WHEN LEFT(Account, 3) = N'156'
                              
                         THEN DebitAmount
                         ELSE $0
                    END) <> 0
                OR SUM(CASE WHEN LEFT(Account, 3) = N'152'
                              
                         THEN ( DebitAmount )
                         ELSE $0
                    END) <> 0
                OR SUM(CASE WHEN LEFT(Account, 3) <> N'152'
                              AND LEFT(Account, 3) <> N'156'
                              AND LEFT(Account, 3) <> N'1331'
                         THEN (DebitAmount)
                         ELSE $0
                    END) <> 0
        ORDER BY PL.PostedDate ,
                PL.RefDate ,
                PL.RefNo           
    END
    


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER procedure [dbo].[Proc_SA_GetDetailPayS12] 
(@fromdate DATETIME,
 @todate DATETIME,
 @account nvarchar(10),
 @currency nvarchar(3),
 @AccountObjectID AS NVARCHAR(MAX))
as
begin
DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(15)
            )
INSERT  INTO @tblAccountNumber
                SELECT  AO.AccountNumber
                FROM    Account AS AO
                        inner JOIN dbo.Func_ConvertStringIntoTable_Nvarchar(@account,
                                                              ',') AS TLAO ON AO.AccountNumber = TLAO.Value
DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectGroupListCode NVARCHAR(MAX) ,
              AccountObjectCategoryName NVARCHAR(MAX)
            ) 
             
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID ,
                        AO.AccountObjectGroupID ,
						null
                FROM    AccountingObject AS AO
                        inner JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                              ',') AS TLAO ON AO.ID = TLAO.Value
                        
                WHERE  
				      ( @AccountObjectID IS NULL
                              OR TLAO.Value IS NOT NULL
                            )
if @account = '331'
select  an.AccountNumber,
        LAOI.AccountObjectID,
        a.PostedDate as NgayThangGS, 
		a.No as Sohieu, 
		a.Date as ngayCTu,
		a.Description as Diengiai, 
		a.AccountCorresponding as TKDoiUng, 
		DueDate as ThoiHanDuocCKhau, 
		sum(a.DebitAmount) as PSNO, 
		sum(a.CreditAmount) as PSCO,
		sum(fnsd.DebitAmount) DuNo,
		sum(fnsd.CreditAmount) DuCo
 from GeneralLedger a									
INNER join PPInvoice c on c.id = a.ReferenceID
inner join @tblAccountNumber as an on a.Account = an.AccountNumber
INNER JOIN @tblListAccountObjectID AS LAOI ON a.AccountingObjectID = LAOI.AccountObjectID
INNER JOIN Func_SoDu (
   @fromdate
  ,@todate
  ,1) AS fnsd ON LAOI.AccountObjectID = fnsd.AccountObjectID	and an.AccountNumber = fnsd.AccountNumber			
where a.PostedDate between @fromdate and @todate
and  a.CurrencyID = @currency  
group by an.AccountNumber,
        LAOI.AccountObjectID,
		a.PostedDate,
		a.No , 
		a.Date ,
		a.Description, 
		a.AccountCorresponding, 
		DueDate
order by AccountNumber,NgayThangGS	
else
select  an.AccountNumber,
        LAOI.AccountObjectID,
        a.PostedDate as NgayThangGS, 
		a.No as Sohieu, 
		a.Date as ngayCTu,
		a.Description as Diengiai, 
		a.AccountCorresponding as TKDoiUng, 
		DueDate as ThoiHanDuocCKhau, 
		sum(a.DebitAmount) as PSNO, 
		sum(a.CreditAmount) as PSCO,
		sum(fnsd.DebitAmount) DuNo,
		sum(fnsd.CreditAmount) DuCo
 from GeneralLedger a	
inner join SAInvoice c on c.id = a.ReferenceID 								
inner join @tblAccountNumber as an on a.Account = an.AccountNumber
INNER JOIN @tblListAccountObjectID AS LAOI ON a.AccountingObjectID = LAOI.AccountObjectID
INNER JOIN Func_SoDu (
   @fromdate
  ,@todate
  ,1) AS fnsd ON LAOI.AccountObjectID = fnsd.AccountObjectID	and an.AccountNumber = fnsd.AccountNumber			
where a.PostedDate between @fromdate and @todate
and  a.CurrencyID = @currency  
group by an.AccountNumber,
        LAOI.AccountObjectID,
		a.PostedDate,
		a.No , 
		a.Date ,
		a.Description, 
		a.AccountCorresponding, 
		DueDate
order by AccountNumber,NgayThangGS	
end				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*

*/
ALTER PROCEDURE [dbo].[Proc_PO_PayDetailNCC]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS 
    BEGIN     
        DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              AccountObjectAddress NVARCHAR(255) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        null AccountingObjectGroupCode ,
                        null AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
      
        CREATE TABLE #tblResult
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100)  ,
              AccountObjectName NVARCHAR(255)  ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL ,             
              AccountObjectAddress NVARCHAR(255)  ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25)  ,
              DueDate DATETIME , 
              InvDate DATETIME ,
              InvNo NVARCHAR(25)  ,

              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              JournalMemo NVARCHAR(255)  , 
              AccountNumber NVARCHAR(20)  , 
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20)  , 
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,	
              DebitAmount MONEY , 
              CreditAmountOC MONEY , 
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY ,
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,	
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50)  ,
              InventoryItemName NVARCHAR(255)  ,
              UnitName NVARCHAR(20)  ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(18, 4) , 
              BranchName NVARCHAR(128)  ,
              IsBold BIT ,	
              OrderType INT ,
              GLJournalMemo NVARCHAR(255)  ,
              DocumentIncluded NVARCHAR(255)  ,
              CustomField1 NVARCHAR(255)  ,
              CustomField2 NVARCHAR(255)  ,
              CustomField3 NVARCHAR(255)  ,
              CustomField4 NVARCHAR(255)  ,
              CustomField5 NVARCHAR(255)  ,
              CustomField6 NVARCHAR(255)  ,
              CustomField7 NVARCHAR(255)  ,
              CustomField8 NVARCHAR(255)  ,
              CustomField9 NVARCHAR(255)  ,
              CustomField10 NVARCHAR(255) ,
               MainUnitName NVARCHAR(20)  ,
              MainQuantity DECIMAL(22, 8) ,
              MainUnitPrice DECIMAL(18, 4)  
              
            )
        
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(255) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
            
                
/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode , 
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,      
                          AccountObjectAddress , 
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID , 
                          JournalMemo , 				  
                          AccountNumber , 
                          AccountCategoryKind ,
                          CorrespondingAccountNumber ,	  			  
                          DebitAmountOC ,	
                          DebitAmount , 
                          CreditAmountOC ,
                          CreditAmount ,
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount ,
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount,
						  OrderType   
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,
                                AccountObjectName ,	
                                AccountObjectGroupListCode , 
                                AccountObjectGroupListName ,            
                                AccountObjectAddress , 
                                RSNS.PostedDate ,
                                NgayCtu ,
                                SoCtu ,
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RSNS.RefID , 
                                JournalMemo ,			  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  			  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) , 
                                SUM(CreditAmount) , 
                                ( CASE WHEN AccountCategoryKind = 0 THEN SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) ) > 0 THEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,      
                                ( CASE WHEN AccountCategoryKind = 0 THEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) ) > 0 THEN SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) ) > 0 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) ) > 0 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount,  
								  OrderType
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,
                                            AccountObjectGroupListCode ,
                                            AccountObjectGroupListName ,         
                                            LAOI.AccountObjectAddress ,
                                            AOL.PostedDate as PostedDate ,  
											AOL.Date as NgayCtu ,     
											AOL.No as SoCtu,                    
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType , 
                                            CASE WHEN AOL.PostedDate < @FromDate 
												      THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN N'Số dư đầu kỳ'
                                                 ELSE AOL.Description
                                            END AS JournalMemo , 
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                             
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                  
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount,
											CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType 
                                  FROM      dbo.GeneralLedger AS AOL
                                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                                         
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )
                                ) AS RSNS                               
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode ,  
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,
                                RSNS.AccountObjectAddress ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu , 
                                RSNS.SoCtu , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,
                                RSNS.JournalMemo , 		  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber,
								OrderType 		   
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount - ClosingCreditAmount) <> 0
                        ORDER BY RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.PostedDate ,
                                RSNS.NgayCtu ,
                                RSNS.SoCtu
                OPTION  ( RECOMPILE )    
                        
/* Tính số tồn */
        DECLARE @CloseAmountOC AS DECIMAL(22, 8) ,
            @CloseAmount AS DECIMAL(22, 8) ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmountOC = 0 THEN ClosingCreditAmountOC
                                                                       ELSE -1 * ClosingDebitAmountOC
                                                                  END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1 THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0 THEN -1 * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0 THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0 THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmount = 0 THEN ClosingCreditAmount
                                                                     ELSE -1 * ClosingDebitAmount
                                                                END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1 THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0 THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0 THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0 THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
        SELECT  RS.PostedDate as Ngay_HT, 
		        RS.RefDate as ngayCtu,
				RS.RefNo as SoCtu,
				Rs.JournalMemo as DienGiai,
				'331' as TK_CONGNO,
				Rs.CorrespondingAccountNumber as TkDoiUng,
                RS.AccountObjectCode,
				RS.AccountObjectName,
				RS.AccountNumber,
				fn.OpeningDebitAmount as OpeningDebitAmount,
				fn.OpeningCreditAmount as OpeningCreditAmount,
				Rs.DebitAmount as DebitAmount,
				Rs.CreditAmount as CreditAmount,
				Rs.ClosingDebitAmount as ClosingDebitAmount,
				Rs.ClosingCreditAmount as ClosingCreditAmount
        FROM    #tblResult RS ,
		        [dbo].[Func_SoDu] (
   @FromDate
  ,@ToDate
  ,3) fn
  where rs.AccountNumber = fn.AccountNumber
  and rs.AccountObjectID = fn.AccountObjectID
        DROP TABLE #tblResult
    END				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*

*/
ALTER PROCEDURE [dbo].[Proc_PO_PayableDetail]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS 
    BEGIN     
        DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(255) ,
              AccountObjectAddress NVARCHAR(255) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        null AccountingObjectGroupCode ,
                        null AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
      
        CREATE TABLE #tblResult
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(100)  ,
              AccountObjectName NVARCHAR(255)  ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL ,             
              AccountObjectAddress NVARCHAR(255)  ,
              PostedDate DATETIME ,
              RefDate DATETIME , 
              RefNo NVARCHAR(25)  ,
              DueDate DATETIME , 
              InvDate DATETIME ,
              InvNo NVARCHAR(25)  ,

              RefType INT ,
              RefID UNIQUEIDENTIFIER ,
              JournalMemo NVARCHAR(255)  , 
              AccountNumber NVARCHAR(20)  , 
              AccountCategoryKind INT ,
              CorrespondingAccountNumber NVARCHAR(20)  , 
              ExchangeRate DECIMAL(18, 4) , 
              DebitAmountOC MONEY ,	
              DebitAmount MONEY , 
              CreditAmountOC MONEY , 
              CreditAmount MONEY , 
              ClosingDebitAmountOC MONEY ,
              ClosingDebitAmount MONEY , 
              ClosingCreditAmountOC MONEY ,	
              ClosingCreditAmount MONEY ,
              InventoryItemCode NVARCHAR(50)  ,
              InventoryItemName NVARCHAR(255)  ,
              UnitName NVARCHAR(20)  ,
              Quantity DECIMAL(22, 8) ,
              UnitPrice DECIMAL(18, 4) , 
              BranchName NVARCHAR(128)  ,
              IsBold BIT ,	
              OrderType INT ,
              GLJournalMemo NVARCHAR(255)  ,
              DocumentIncluded NVARCHAR(255)  ,
              CustomField1 NVARCHAR(255)  ,
              CustomField2 NVARCHAR(255)  ,
              CustomField3 NVARCHAR(255)  ,
              CustomField4 NVARCHAR(255)  ,
              CustomField5 NVARCHAR(255)  ,
              CustomField6 NVARCHAR(255)  ,
              CustomField7 NVARCHAR(255)  ,
              CustomField8 NVARCHAR(255)  ,
              CustomField9 NVARCHAR(255)  ,
              CustomField10 NVARCHAR(255) ,
               MainUnitName NVARCHAR(20)  ,
              MainQuantity DECIMAL(22, 8) ,
              MainUnitPrice DECIMAL(18, 4)  
              
            )
        
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(255) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
            
                
/*Lấy số dư đầu kỳ và dữ liệu phát sinh trong kỳ:*/ 
INSERT  INTO #tblResult
                        ( AccountObjectID ,
                          AccountObjectCode , 
                          AccountObjectName ,
                          AccountObjectGroupListCode , 
                          AccountObjectGroupListName ,      
                          AccountObjectAddress , 
                          PostedDate ,
                          RefDate ,
                          RefNo ,
                          InvDate ,
                          InvNo ,
                          RefType ,
                          RefID , 
                          JournalMemo , 				  
                          AccountNumber , 
                          AccountCategoryKind ,
                          CorrespondingAccountNumber ,	  			  
                          DebitAmountOC ,	
                          DebitAmount , 
                          CreditAmountOC ,
                          CreditAmount ,
                          ClosingDebitAmountOC , 
                          ClosingDebitAmount ,
                          ClosingCreditAmountOC ,
                          ClosingCreditAmount,
						  OrderType   
                        )
                        SELECT  AccountingObjectID ,
                                AccountObjectCode ,
                                AccountObjectName ,	
                                AccountObjectGroupListCode , 
                                AccountObjectGroupListName ,            
                                AccountObjectAddress , 
                                RSNS.PostedDate ,
                                RefDate ,
                                RefNo ,
                                InvDate ,
                                InvNo ,
                                RefType ,
                                RSNS.RefID , 
                                JournalMemo ,			  
                                AccountNumber , 
                                AccountCategoryKind , 
                                CorrespondingAccountNumber , 		  			  
                                SUM(DebitAmountOC) ,
                                SUM(DebitAmount) , 
                                SUM(CreditAmountOC) , 
                                SUM(CreditAmount) , 
                                ( CASE WHEN AccountCategoryKind = 0 THEN SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC)
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) ) > 0 THEN ( SUM(ClosingDebitAmountOC) - SUM(ClosingCreditAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmountOC ,      
                                ( CASE WHEN AccountCategoryKind = 0 THEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) )
                                       WHEN AccountCategoryKind = 1 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount) ) > 0 THEN SUM(ClosingDebitAmount) - SUM(ClosingCreditAmount)
                                                 ELSE $0
                                            END
                                  END ) AS ClosingDebitAmount , 
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) ) > 0 THEN ( SUM(ClosingCreditAmountOC) - SUM(ClosingDebitAmountOC) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmountOC ,
                                ( CASE WHEN AccountCategoryKind = 1 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                       WHEN AccountCategoryKind = 0 THEN $0
                                       ELSE CASE WHEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) ) > 0 THEN ( SUM(ClosingCreditAmount) - SUM(ClosingDebitAmount) )
                                                 ELSE $0
                                            END
                                  END ) AS ClosingCreditAmount,  
								  OrderType
                        FROM    ( SELECT    AOL.AccountingObjectID ,
                                            LAOI.AccountObjectCode ,   
                                            LAOI.AccountObjectName ,
                                            AccountObjectGroupListCode ,
                                            AccountObjectGroupListName ,         
                                            LAOI.AccountObjectAddress ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.PostedDate
                                            END AS PostedDate ,                      
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE RefDate
                                            END AS RefDate , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.RefNo
                                            END AS RefNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceDate
                                            END AS InvDate ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.InvoiceNo
                                            END AS InvNo ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.ReferenceID
                                            END AS RefID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.TypeID
                                            END AS RefType , 
                                            CASE WHEN AOL.PostedDate < @FromDate 
												      THEN NULL
                                                 ELSE CAST(AOL.DetailID AS NVARCHAR(50))
                                            END AS RefDetailID ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN N'Số dư đầu kỳ'
                                                 ELSE AOL.Description
                                            END AS JournalMemo , 
                                            TBAN.AccountNumber ,
                                            TBAN.AccountCategoryKind ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN NULL
                                                 ELSE AOL.AccountCorresponding
                                            END AS CorrespondingAccountNumber , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmountOriginal
                                            END AS DebitAmountOC ,                             
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.DebitAmount
                                            END AS DebitAmount ,                                  
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmountOriginal
                                            END AS CreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                                 ELSE AOL.CreditAmount
                                            END AS CreditAmount , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmountOriginal
                                                 ELSE $0
                                            END AS ClosingDebitAmountOC , 
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.DebitAmount
                                                 ELSE $0
                                            END AS ClosingDebitAmount ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmountOriginal
                                                 ELSE $0
                                            END AS ClosingCreditAmountOC ,
                                            CASE WHEN AOL.PostedDate < @FromDate THEN AOL.CreditAmount
                                                 ELSE $0
                                            END AS ClosingCreditAmount,
											CASE WHEN AOL.PostedDate < @FromDate
                                                 THEN 0
                                                 ELSE 1
                                            END AS OrderType 
                                  FROM      dbo.GeneralLedger AS AOL
                                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID                                         
                                  WHERE     AOL.PostedDate <= @ToDate
                                            AND ( @CurrencyID IS NULL
                                                  OR AOL.CurrencyID = @CurrencyID
                                                )
                                ) AS RSNS                               
                        GROUP BY RSNS.AccountingObjectID ,
                                RSNS.AccountObjectCode ,  
                                RSNS.AccountObjectName ,
                                AccountObjectGroupListCode ,
                                AccountObjectGroupListName ,
                                RSNS.AccountObjectAddress ,
                                RSNS.PostedDate ,
                                RSNS.RefDate , 
                                RSNS.RefNo , 
                                RSNS.InvDate ,
                                RSNS.InvNo ,
                                RSNS.RefType ,
                                RSNS.RefID ,
                                RSNS.JournalMemo , 		  
                                RSNS.AccountNumber ,
                                RSNS.AccountCategoryKind ,
                                RSNS.CorrespondingAccountNumber,
								OrderType 		   
                        HAVING  SUM(DebitAmountOC) <> 0
                                OR SUM(DebitAmount) <> 0
                                OR SUM(CreditAmountOC) <> 0
                                OR SUM(CreditAmount) <> 0
                                OR SUM(ClosingDebitAmountOC - ClosingCreditAmountOC) <> 0
                                OR SUM(ClosingDebitAmount - ClosingCreditAmount) <> 0
                        ORDER BY RSNS.AccountObjectCode ,
                                RSNS.AccountNumber ,
                                RSNS.PostedDate ,
                                RSNS.RefDate ,
                                RSNS.RefNo
                OPTION  ( RECOMPILE )    
                        
/* Tính số tồn */
        DECLARE @CloseAmountOC AS DECIMAL(22, 8) ,
            @CloseAmount AS DECIMAL(22, 8) ,
            @AccountObjectCode_tmp NVARCHAR(100) ,
            @AccountNumber_tmp NVARCHAR(20)
        SELECT  @CloseAmountOC = 0 ,
                @CloseAmount = 0 ,
                @AccountObjectCode_tmp = N'' ,
                @AccountNumber_tmp = N''
        UPDATE  #tblResult
        SET     @CloseAmountOC = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmountOC = 0 THEN ClosingCreditAmountOC
                                                                       ELSE -1 * ClosingDebitAmountOC
                                                                  END )
                                        WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                             OR @AccountNumber_tmp <> AccountNumber THEN CreditAmountOC - DebitAmountOC
                                        ELSE @CloseAmountOC + CreditAmountOC - DebitAmountOC
                                   END ) ,
                ClosingDebitAmountOC = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmountOC
                                              WHEN AccountCategoryKind = 1 THEN $0
                                              ELSE CASE WHEN @CloseAmountOC < 0 THEN -1 * @CloseAmountOC
                                                        ELSE $0
                                                   END
                                         END ) ,
                ClosingCreditAmountOC = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmountOC
                                               WHEN AccountCategoryKind = 0 THEN $0
                                               ELSE CASE WHEN @CloseAmountOC > 0 THEN @CloseAmountOC
                                                         ELSE $0
                                                    END
                                          END ) ,
                @CloseAmount = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmount = 0 THEN ClosingCreditAmount
                                                                     ELSE -1 * ClosingDebitAmount
                                                                END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode
                                           OR @AccountNumber_tmp <> AccountNumber THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountCategoryKind = 0 THEN -1 * @CloseAmount
                                            WHEN AccountCategoryKind = 1 THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0 THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountCategoryKind = 1 THEN @CloseAmount
                                             WHEN AccountCategoryKind = 0 THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0 THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
                @AccountNumber_tmp = AccountNumber
        WHERE   OrderType <> 2
/*Lấy số liệu*/				
        SELECT  
                RS.AccountObjectCode,
				RS.AccountObjectName,
				RS.AccountNumber,
				sum(fn.OpeningDebitAmount) OpeningDebitAmount,
				sum(fn.OpeningCreditAmount) OpeningCreditAmount,
				sum(Rs.DebitAmount) DebitAmount,
				sum(Rs.CreditAmount) CreditAmount,
				sum(Rs.ClosingDebitAmount) ClosingDebitAmount,
				sum(Rs.ClosingCreditAmount) ClosingCreditAmount
        FROM    #tblResult RS ,
		        [dbo].[Func_SoDu] (
   @FromDate
  ,@ToDate
  ,3) fn
  where rs.AccountNumber = fn.AccountNumber
  and rs.AccountObjectID = fn.AccountObjectID
  group by RS.AccountObjectCode,
				RS.AccountObjectName,
				RS.AccountNumber
        DROP TABLE #tblResult
    END				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO


GO
DISABLE TRIGGER ALL ON dbo.TemplateColumn
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.Account WHERE ID = '73afca6e-92b4-4d86-b43b-0321083fb0ad'
DELETE dbo.Account WHERE ID = '90af297c-b2f5-47a8-9b5c-1c5b94752263'
DELETE dbo.Account WHERE ID = 'a89575bd-6224-4298-b3f1-363e4a7ae481'
DELETE dbo.Account WHERE ID = '823d84e9-f096-42a6-a676-3964ee2bc408'
DELETE dbo.Account WHERE ID = 'bf1b8906-eae4-48aa-8b3f-55a8051635ef'
DELETE dbo.Account WHERE ID = 'c005c550-2d48-4f30-94e6-71db25fe2d82'
DELETE dbo.Account WHERE ID = 'df9bf405-18a3-4a18-8e7e-9ada05c08149'
DELETE dbo.Account WHERE ID = 'f28093a6-285e-469e-9bff-a2a0b6424f2b'
DELETE dbo.Account WHERE ID = '537608c4-6cdf-40e5-a9ca-a324644e6cff'
DELETE dbo.Account WHERE ID = '8b0785ef-8580-4fd0-9d53-a8fe01ecd715'
DELETE dbo.Account WHERE ID = 'f45ff65c-4123-4037-ae17-b2e1b6491349'
DELETE dbo.Account WHERE ID = '80c7787f-ec70-40ff-8259-dcf913821b77'
DELETE dbo.Account WHERE ID = '0478ca57-b2ca-47e3-ae59-ee0e6c202ee2'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.TemplateColumn WHERE ID = '4e0c60e4-9a7c-4299-bcc7-3318a8cf1acd'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

GO
ENABLE TRIGGER ALL ON dbo.TemplateColumn
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF
GO
