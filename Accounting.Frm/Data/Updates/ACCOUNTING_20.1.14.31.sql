SET CONCAT_NULL_YIELDS_NULL, ANSI_NULLS, ANSI_PADDING, QUOTED_IDENTIFIER, ANSI_WARNINGS, ARITHABORT, XACT_ABORT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS OFF
GO

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

BEGIN TRANSACTION
GO

ALTER TABLE [dbo].[SABill]
  ALTER
    COLUMN [InvoiceDate] [datetime2]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE [dbo].[SABill]
  ALTER
    COLUMN [RefDateTime] [datetime2]
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
Proc_GetBalanceAccountF01 '2019-01-01','2019-12-31',3,0
*/
ALTER PROCEDURE [dbo].[Proc_GetBalanceAccountF01]
      ( @FromDate DATETIME ,
        @ToDate DATETIME ,
        @MaxAccountGrade INT,
		@IsBalanceBothSide BIT
		)
AS
BEGIN
 
SELECT t.AccountID ,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName,
			   sum(t.OpeningDebitAmount) OpeningDebitAmount,
			   sum(t.OpeningCreditAmount) OpeningCreditAmount,
			   sum(t.DebitAmount) DebitAmount,
			   sum(t.CreditAmount) CreditAmount,
			   sum(t.DebitAmountAccum) DebitAmountAccum,
			   sum(t.CreditAmountAccum) CreditAmountAccum,
			   sum(t.ClosingDebitAmount) ClosingDebitAmount,
			   sum(t.ClosingCreditAmount) ClosingCreditAmount
			   FROM [dbo].[Func_GetBalanceAccountF01] (
   @FromDate
  ,@ToDate
  ,@MaxAccountGrade
  ,@IsBalanceBothSide) t
  group by t.AccountID,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName
  order by t.AccountNumber
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GetHealthFinanceMoney]
      ( @FromDate DATETIME ,
        @ToDate DATETIME
		)
AS
BEGIN
	DECLARE @tbDataReturn TABLE(
		tySoNo decimal(18,2),
		tyTaiTro decimal(18,2),
		vonLuanChuyen decimal(18,2),
		hsTTNganHan decimal(18,2),
		hsTTNhanh decimal(18,2),
		hsTTTucThoi decimal(18,2),
		hsTTChung decimal(18,2),
		hsQVHangTonKho decimal(18,2),
		hsLNTrenVonKD decimal(18,2),
		hsLNTrenDTThuan decimal(18,2),
		hsLNTrenVonCSH decimal(18,2),
		tienMat money,
		tienGui money,
		doanhThu money,
		chiPhi money,
		loiNhuanTruocThue money,
		phaiThu money,
		phaiTra money,
		hangTonKho money
	)
	
	INSERT INTO @tbDataReturn(tySoNo,tyTaiTro,vonLuanChuyen,hsTTNganHan,hsTTNhanh,hsTTTucThoi,hsTTChung,hsQVHangTonKho,hsLNTrenVonKD,hsLNTrenDTThuan,
	hsLNTrenVonCSH,tienMat,tienGui,doanhThu,chiPhi,loiNhuanTruocThue,phaiThu,phaiTra,hangTonKho)
	Values(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null)
	
	DECLARE @tbDataGetB01DN TABLE(
	  ItemID UNIQUEIDENTIFIER ,
      ItemCode NVARCHAR(25) ,
      ItemName NVARCHAR(512) ,
      ItemNameEnglish NVARCHAR(512) ,
      ItemIndex INT ,
      Description NVARCHAR(512) ,
      FormulaType INT ,
      FormulaFrontEnd NVARCHAR(MAX) ,
      Formula XML ,
      Hidden BIT ,
      IsBold BIT ,
      IsItalic BIT ,
      Category INT ,
      SortOrder INT,
      Amount DECIMAL(25, 4) ,
      PrevAmount DECIMAL(25, 4)
	)
	
	INSERT INTO @tbDataGetB01DN
	SELECT * FROM [dbo].[Func_GetB01_DN] (null,null,@FromDate,@ToDate,0,1,0,@FromDate,@ToDate)
	
	
	DECLARE @tbDataGetBalanceAccountF01 TABLE(
		AccountID UNIQUEIDENTIFIER ,
        AccountCategoryKind INT ,
        AccountNumber NVARCHAR(50) ,
        AccountName NVARCHAR(512) ,
        OpeningDebitAmount DECIMAL(25, 4) ,
        OpeningCreditAmount DECIMAL(25, 4) ,
        DebitAmount DECIMAL(25, 4) ,
        CreditAmount DECIMAL(25, 4) ,
        DebitAmountAccum DECIMAL(25, 4) ,
        CreditAmountAccum DECIMAL(25, 4) ,
        ClosingDebitAmount DECIMAL(25, 4) ,
        ClosingCreditAmount DECIMAL(25, 4)
	)
	
	INSERT INTO @tbDataGetBalanceAccountF01
	SELECT t.AccountID ,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName,
			   sum(t.OpeningDebitAmount) OpeningDebitAmount,
			   sum(t.OpeningCreditAmount) OpeningCreditAmount,
			   sum(t.DebitAmount) DebitAmount,
			   sum(t.CreditAmount) CreditAmount,
			   sum(t.DebitAmountAccum) DebitAmountAccum,
			   sum(t.CreditAmountAccum) CreditAmountAccum,
			   sum(t.ClosingDebitAmount) ClosingDebitAmount,
			   sum(t.ClosingCreditAmount) ClosingCreditAmount
			   FROM [dbo].[Func_GetBalanceAccountF01] (@FromDate,@ToDate,1,0) t
	GROUP BY t.AccountID,
		       t.AccountCategoryKind,
			   t.AccountNumber,
			   t.AccountName
	ORDER BY t.AccountNumber
	
	
	Declare @ct400 decimal(25, 4)
	Set @ct400=(select Amount from @tbDataGetB01DN where ItemCode='400')
	
	Declare @ct600 decimal(25, 4)
	Set @ct600=(select Amount from @tbDataGetB01DN where ItemCode='600')
	
	if(ISNULL(@ct600,0)=0)
	begin
		UPDATE @tbDataReturn SET tySoNo = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET tySoNo = (ISNULL(@ct400,0)/ISNULL(@ct600,0))*100
	end
	
	Declare @ct500 decimal(25, 4)
	Set @ct500=(select Amount from @tbDataGetB01DN where ItemCode='500')
	
	if(ISNULL(@ct600,0)=0)
	begin
		UPDATE @tbDataReturn SET tyTaiTro = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET tyTaiTro = (ISNULL(@ct500,0)/ISNULL(@ct600,0))*100
	end
	
	Declare @ct100 decimal(25, 4)
	Set @ct100=(select Amount from @tbDataGetB01DN where ItemCode='100')
	
	Declare @ct410 decimal(25, 4)
	Set @ct410=(select Amount from @tbDataGetB01DN where ItemCode='410')
	
	UPDATE @tbDataReturn SET vonLuanChuyen = (ISNULL(@ct100,0)-ISNULL(@ct410,0))
	
	if(ISNULL(@ct410,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTNganHan = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTNganHan = (ISNULL(@ct100,0)/ISNULL(@ct410,0))
	end
	
	Declare @ct140 decimal(25, 4)
	Set @ct140=(select Amount from @tbDataGetB01DN where ItemCode='140')
	
	if(ISNULL(@ct410,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTNhanh = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTNhanh = (ISNULL(@ct100,0)-ISNULL(@ct140,0))/ISNULL(@ct410,0)
	end
	
	Declare @ct110 decimal(25, 4)
	Set @ct110=(select Amount from @tbDataGetB01DN where ItemCode='110')
	
	Declare @ct120 decimal(25, 4)
	Set @ct120=(select Amount from @tbDataGetB01DN where ItemCode='120')
	
	if(ISNULL(@ct410,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTTucThoi = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTTucThoi = (ISNULL(@ct110,0)+ISNULL(@ct120,0))/ISNULL(@ct410,0)
	end
	
	Declare @ct300 decimal(25, 4)
	Set @ct300=(select Amount from @tbDataGetB01DN where ItemCode='300')
	
	if(ISNULL(@ct400,0)=0)
	begin
		UPDATE @tbDataReturn SET hsTTChung = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsTTChung = (ISNULL(@ct300,0)/ISNULL(@ct400,0))
	end
	
	Declare @vonHH decimal(25, 4)
	Set @vonHH = (select SUM(DebitAmount) from GeneralLedger where Account='632' AND AccountCorresponding='911' AND (PostedDate between @FromDate and @ToDate))
	
	Declare @hangHoaTon decimal(25,4)
	set @hangHoaTon = (select SUM(ISNULL(OpeningDebitAmount,0)+ISNULL(ClosingDebitAmount,0)) from @tbDataGetBalanceAccountF01 
						where AccountNumber in ('152','153','154','155','156','157'))/2
	
	if(ISNULL(@hangHoaTon,0)=0)
	begin
		UPDATE @tbDataReturn SET hsQVHangTonKho = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsQVHangTonKho=(ISNULL(@vonHH,0)/ISNULL(@hangHoaTon,0))*100
	end
	
	Declare @LNsauThue decimal(25,4)
	set @LNsauThue = (select SUM(CreditAmount) from GeneralLedger where Account='4212' AND AccountCorresponding='911' AND (PostedDate between @FromDate and @ToDate))
	
	Declare @ct600namTruoc decimal(25, 4)
	Set @ct600namTruoc=(select PrevAmount from @tbDataGetB01DN where ItemCode='600')
	
	Declare @vonKdBq decimal(25,4)
	set @vonKdBq = (ISNULL(@ct600,0)+ISNULL(@ct600namTruoc,0))/2
	
	if(ISNULL(@vonKdBq,0)=0)
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonKD = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonKD = (ISNULL(@LNsauThue,0)/ISNULL(@vonKdBq,0))
	end
	
	
	Declare @dtThuan decimal(25,4)
	set @dtThuan = (select SUM(CreditAmount) from GeneralLedger where Account like'511%' AND AccountCorresponding='911' AND (PostedDate between @FromDate and @ToDate))
	
	if(ISNULL(@dtThuan,0)=0)
	begin
		UPDATE @tbDataReturn SET hsLNTrenDTThuan = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsLNTrenDTThuan = (ISNULL(@LNsauThue,0)/ISNULL(@dtThuan,0))
	end
	
	Declare @ct500namtruoc decimal(25, 4)
	Set @ct500namtruoc=(select PrevAmount from @tbDataGetB01DN where ItemCode='500')
	
	Declare @voncsh decimal(25,4)
	set @voncsh = (ISNULL(@ct500,0)+ISNULL(@ct500namtruoc,0))/2
	
	if(ISNULL(@voncsh,0)=0)
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonCSH = 0
	end
	else
	begin
		UPDATE @tbDataReturn SET hsLNTrenVonCSH = (ISNULL(@LNsauThue,0)/ISNULL(@voncsh,0))
	end
	
	Declare @duNoCuoiKy111 money
	set @duNoCuoiKy111 = (select ClosingDebitAmount from @tbDataGetBalanceAccountF01 where AccountNumber='111')
	
	UPDATE @tbDataReturn SET tienMat = ISNULL(@duNoCuoiKy111,0)
	
	Declare @duNoCuoiKy112 money
	set @duNoCuoiKy112 = (select ClosingDebitAmount from @tbDataGetBalanceAccountF01 where AccountNumber='112')
	
	UPDATE @tbDataReturn SET tienGui = ISNULL(@duNoCuoiKy112,0)
	
	Declare @doanhThu money
	set @doanhThu = (select SUM(CreditAmount - DebitAmount) from GeneralLedger 
						where ((Account like '511%') or (Account like '515%') or (Account like '711%'))
								and (AccountCorresponding<>'911') and (PostedDate between @FromDate and @ToDate))
	
	UPDATE @tbDataReturn SET doanhThu = ISNULL(@doanhThu,0)
	
	Declare @chiPhi money
	set @chiPhi = (select SUM(DebitAmount - CreditAmount) from GeneralLedger
						where ((Account like '632%') or (Account like '642%') or (Account like '635%') or (Account like '811%') or (Account like '821%'))
						and (AccountCorresponding<>'911') and (PostedDate between @FromDate and @ToDate))
	
	UPDATE @tbDataReturn SET chiPhi = ISNULL(@chiPhi,0)
	
	Declare @chiPhiko821 money
	set @chiPhiko821 = (select SUM(DebitAmount - CreditAmount) from GeneralLedger
						where ((Account like '632%') or (Account like '642%') or (Account like '635%') or (Account like '811%'))
						and (AccountCorresponding<>'911') and (PostedDate between @FromDate and @ToDate))
	
	UPDATE @tbDataReturn SET loiNhuanTruocThue = (ISNULL(@doanhThu,0) - ISNULL(@chiPhiko821,0))
	
	Declare @phaiThu money
	set @phaiThu = (select SUM(ClosingDebitAmount) from @tbDataGetBalanceAccountF01 where AccountNumber='131')
	
	UPDATE @tbDataReturn SET phaiThu = (ISNULL(@phaiThu,0))
	
	Declare @phaiTra money
	set @phaiTra = (select SUM(ClosingCreditAmount) from @tbDataGetBalanceAccountF01 where AccountNumber='331')
	
	UPDATE @tbDataReturn SET phaiTra = (ISNULL(@phaiTra,0))
	
	Declare @hangTonKho money
	set @hangTonKho = (select SUM(ClosingDebitAmount) from @tbDataGetBalanceAccountF01 
						where AccountNumber in ('152','153','154','155','156','157'))
	
	UPDATE @tbDataReturn SET hangTonKho = (ISNULL(@hangTonKho,0))
	
	SELECT * FROM @tbDataReturn
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GetPPPayVendorBill]
	@AccountingObjectID UNIQUEIDENTIFIER,
	@FromDate DATETIME,
    @ToDate DATETIME
AS
	BEGIN
		DECLARE @Account NVARCHAR(MAX) = (SELECT TOP 1 AccountNumber FROM Account WHERE DetailType = '0')
		DECLARE @tbDataGL TABLE(
				ID uniqueidentifier,
				BranchID uniqueidentifier,
				ReferenceID uniqueidentifier,
				TypeID int,
				Date datetime,
				PostedDate datetime,
				No nvarchar(25),
				InvoiceDate datetime,
				InvoiceNo nvarchar(25),
				Account nvarchar(25),
				AccountCorresponding nvarchar(25),
				BankAccountDetailID uniqueidentifier,
				CurrencyID nvarchar(3),
				ExchangeRate decimal(25, 10),
				DebitAmount decimal(25,0),
				DebitAmountOriginal decimal(25,2),
				CreditAmount decimal(25,0),
				CreditAmountOriginal decimal(25,2),
				Reason nvarchar(512),
				Description nvarchar(512),
				VATDescription nvarchar(512),
				AccountingObjectID uniqueidentifier,
				EmployeeID uniqueidentifier,
				BudgetItemID uniqueidentifier,
				CostSetID uniqueidentifier,
				ContractID uniqueidentifier,
				StatisticsCodeID uniqueidentifier,
				InvoiceSeries nvarchar(25),
				ContactName nvarchar(512),
				DetailID uniqueidentifier,
				RefNo nvarchar(25),
				RefDate datetime,
				DepartmentID uniqueidentifier,
				ExpenseItemID uniqueidentifier,
				OrderPriority int,
				IsIrrationalCost bit
			)

			INSERT INTO @tbDataGL
			SELECT GL.* FROM dbo.GeneralLedger GL 
			LEFT JOIN dbo.AccountingObject AO ON GL.AccountingObjectID = AO.ID
			WHERE GL.AccountingObjectID = @AccountingObjectID AND GL.Account LIKE '331%'
			AND  GL.TypeID IN (210, 230, 240, 430, 500, 701, 601) AND AO.ObjectType IN (1, 2) AND AO.IsActive = 1 AND GL.Date <= @ToDate

			(SELECT g.AccountingObjectID, a.AccountingObjectCode, a.AccountingObjectName, g.No,
			g.ReferenceID, g.PostedDate AS Date, g.InvoiceNo, g.EmployeeID, g.CurrencyID, g.ExchangeRate AS RefVoucherExchangeRate,
			TotalDebit = (SELECT ISNULL(SUM(CreditAmount - DebitAmount),0)
					FROM @tbDataGL WHERE PostedDate > @FromDate AND ReferenceID = g.ReferenceID),
			TotalDebitOriginal = (SELECT ISNULL(SUM(CreditAmountOriginal - DebitAmountOriginal),0)
			FROM @tbDataGL WHERE PostedDate > @FromDate AND ReferenceID = g.ReferenceID),
			DebitAmount = ((SELECT ISNULL(SUM(CreditAmount - DebitAmount),0)
					FROM @tbDataGL WHERE PostedDate > @FromDate AND ReferenceID = g.ReferenceID) - 
					(SELECT ISNULL(SUM(Amount),0)
					FROM MCPaymentDetailVendor mcp WHERE mcp.AccountingObjectID = @AccountingObjectID AND mcp.PPInvoiceID = g.ReferenceID) -
					(SELECT ISNULL(SUM(Amount),0)
					FROM MBTellerPaperDetailVendor mbt WHERE mbt.AccountingObjectID = @AccountingObjectID AND mbt.PPInvoiceID = g.ReferenceID) - 
					(SELECT ISNULL(SUM(Amount),0)
					FROM MBCreditCardDetailVendor mbc WHERE mbc.AccountingObjectID = @AccountingObjectID AND mbc.PPInvoiceID = g.ReferenceID) - 
					(SELECT ISNULL(SUM(Amount),0)
					FROM ExceptVoucher exv WHERE exv.AccountingObjectID = @AccountingObjectID AND exv.DebitAccount = @Account AND exv.GLVoucherID = g.ReferenceID)),
			DebitAmountOriginal = ((SELECT ISNULL(SUM(CreditAmountOriginal - DebitAmountOriginal),0)
					FROM @tbDataGL WHERE PostedDate > @FromDate AND ReferenceID = g.ReferenceID) - 
					(SELECT ISNULL(SUM(AmountOriginal),0)
					FROM MCPaymentDetailVendor mcp WHERE mcp.AccountingObjectID = @AccountingObjectID AND mcp.PPInvoiceID = g.ReferenceID) -
					(SELECT ISNULL(SUM(AmountOriginal),0)
					FROM MBTellerPaperDetailVendor mbt WHERE mbt.AccountingObjectID = @AccountingObjectID AND mbt.PPInvoiceID = g.ReferenceID) - 
					(SELECT ISNULL(SUM(AmountOriginal),0)
					FROM MBCreditCardDetailVendor mbc WHERE mbc.AccountingObjectID = @AccountingObjectID AND mbc.PPInvoiceID = g.ReferenceID) - 
					(SELECT ISNULL(SUM(AmountOriginal),0)
					FROM ExceptVoucher exv WHERE exv.AccountingObjectID = @AccountingObjectID AND exv.DebitAccount = @Account AND exv.GLVoucherID = g.ReferenceID)),
			Account = @Account
		FROM @tbDataGL g
		LEFT JOIN AccountingObject a on g.AccountingObjectID = a.ID
		GROUP BY g.AccountingObjectID, a.AccountingObjectCode, a.AccountingObjectName, g.ReferenceID, g.No, g.InvoiceNo, g.PostedDate, g.TypeID, g.EmployeeID, g.ExchangeRate, g.CurrencyID) ORDER BY g.No
	END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GetPPPayVendor]
	@FromDate DATETIME,
    @ToDate DATETIME
AS
BEGIN
	DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL 
		LEFT JOIN dbo.AccountingObject AO ON GL.AccountingObjectID = AO.ID
		WHERE GL.AccountingObjectID IS NOT NULL AND GL.Account LIKE '331%'
		AND  GL.TypeID IN (110, 111, 112, 113, 114, 115, 116, 117, 119, 118, 128, 134, 144, 174
		, 210, 230, 240, 430, 500, 701, 601) AND AO.ObjectType IN (1, 2) AND AO.IsActive = 1 AND GL.Date <= @ToDate

		(SELECT g.AccountingObjectID, a.AccountingObjectCode, a.AccountingObjectName,
		SoDuDauNam = ((SELECT ISNULL(SUM(CreditAmount - DebitAmount),0)
                FROM GeneralLedger WHERE AccountingObjectID = g.AccountingObjectID AND TypeID = 701
                AND PostedDate < @FromDate)
				+ (SELECT ISNULL(SUM(CreditAmount),0)
                FROM @tbDataGL WHERE AccountingObjectID = g.AccountingObjectID AND TypeID != 701
                AND PostedDate < @FromDate)),
		SoDaTra = ((SELECT ISNULL(SUM(DebitAmount),0)
                FROM @tbDataGL WHERE AccountingObjectID = g.AccountingObjectID AND TypeID IN
				(110, 111, 112, 113, 114, 115, 116, 117, 119, 118, 128, 134, 144, 174))
				+ (SELECT ISNULL(SUM(Amount),0)
                FROM ExceptVoucher WHERE AccountingObjectID = g.AccountingObjectID)),
		SoPhatSinh = (SELECT ISNULL(SUM(CreditAmount - DebitAmount),0)
				FROM @tbDataGL WHERE AccountingObjectID = g.AccountingObjectID AND TypeID IN
				(210, 230, 240, 430, 500, 701, 601) AND PostedDate >= @FromDate),
		SoConPhaiTra = (((SELECT ISNULL(SUM(CreditAmount - DebitAmount),0)
                FROM GeneralLedger WHERE AccountingObjectID = g.AccountingObjectID AND TypeID = 701
                AND PostedDate < @FromDate)
				+ (SELECT ISNULL(SUM(CreditAmount),0)
                FROM @tbDataGL WHERE AccountingObjectID = g.AccountingObjectID AND TypeID != 701
                AND PostedDate < @FromDate)) + 
				(SELECT ISNULL(SUM(CreditAmount - DebitAmount),0)
				FROM @tbDataGL WHERE AccountingObjectID = g.AccountingObjectID AND TypeID IN
				(210, 230, 240, 430, 500, 701, 601) AND PostedDate >= @FromDate) -
				((SELECT ISNULL(SUM(DebitAmount),0)
                FROM @tbDataGL WHERE AccountingObjectID = g.AccountingObjectID AND TypeID IN
				(110, 111, 112, 113, 114, 115, 116, 117, 119, 118, 128, 134, 144, 174))
				+ (SELECT ISNULL(SUM(Amount),0)
                FROM ExceptVoucher WHERE AccountingObjectID = g.AccountingObjectID)))
		FROM @tbDataGL g
		LEFT JOIN AccountingObject a on g.AccountingObjectID = a.ID
		GROUP BY g.AccountingObjectID, a.AccountingObjectCode, a.AccountingObjectName) ORDER BY a.AccountingObjectCode
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_GetHealthFinanceChart]
      ( @FromDate DATETIME ,
        @ToDate DATETIME
		)
AS
BEGIN
	DECLARE @tbTemp TABLE(
		PostedDate datetime,
		PostedDay int,
		PostedMonth int,
		PostedYear int,
		Account nvarchar(25),
		AccountCorresponding nvarchar(25),
		DebitAmount decimal(25,0),
		CreditAmount decimal(25,0)
	)
	
	INSERT INTO @tbTemp
	SELECT PostedDate, DAY(PostedDate) as Ngay, MONTH(PostedDate) as Thang, YEAR(PostedDate) as Nam, Account, AccountCorresponding, 
	DebitAmount, CreditAmount
	FROM GeneralLedger
	WHERE PostedDate between @FromDate AND @ToDate
	
	Declare @tbDataTempChart TABLE(
		PostedMonth int,
		PostedYear int,
		TurnoverTotal money,
		CostsTotal money
	)
	
	INSERT INTO @tbDataTempChart(PostedMonth,PostedYear,TurnoverTotal)
	SELECT PostedMonth, PostedYear, SUM(CreditAmount-DebitAmount) as TurnoverTotal
	FROM @tbTemp
	WHERE ((Account like '511%') OR (Account like '515%') OR (Account like '711%')) AND AccountCorresponding <> '911'
	GROUP BY PostedMonth, PostedYear
	
	INSERT INTO @tbDataTempChart(PostedMonth,PostedYear,CostsTotal)
	SELECT PostedMonth, PostedYear, SUM(DebitAmount-CreditAmount) as CostsTotal
	FROM @tbTemp
	WHERE ((Account like '632%') OR (Account like '642%') OR (Account like '635%') OR (Account like '811%') OR (Account like '821%')) 
	AND AccountCorresponding <> '911'
	GROUP BY PostedMonth, PostedYear
	
	Declare @tbDataChart TABLE(
		PostedMonthYear nvarchar(15),
		PostedMonth int,
		PostedYear int,
		TurnoverTotal money,
		CostsTotal money
	)
	
	INSERT INTO @tbDataChart(PostedMonth,PostedYear,TurnoverTotal,CostsTotal)
	SELECT PostedMonth,PostedYear,SUM(TurnoverTotal) as TurnoverTotal, SUM(CostsTotal) as CostsTotal
	FROM @tbDataTempChart
	GROUP BY PostedMonth,PostedYear
	
	UPDATE @tbDataChart SET TurnoverTotal=0 WHERE  TurnoverTotal<0
	UPDATE @tbDataChart SET CostsTotal=0 WHERE  CostsTotal<0
	
	UPDATE @tbDataChart SET PostedMonthYear=CONVERT(nvarchar(2),PostedMonth)+'/'+CONVERT(nvarchar(4),PostedYear)
	
	SELECT PostedMonthYear,TurnoverTotal,CostsTotal FROM @tbDataChart
end
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO



ALTER PROCEDURE [dbo].[Proc_BA_GetBookDepositListDetail]
    @BranchID UNIQUEIDENTIFIER ,
    @IncludeDependentBranch BIT ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @CurrencyID NVARCHAR(3) ,
    @AccountNumber NVARCHAR(25) ,
    @BankAccountID UNIQUEIDENTIFIER ,
    @IsSimilarSum BIT = 0 ,
    @IsSoftOrderVoucher BIT = 0
AS
    BEGIN
        SET NOCOUNT ON
        
		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		/*end add by cuongpv*/

        CREATE TABLE #Result
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
              BankAccount NVARCHAR(500)  ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25)  ,
              RefType INT ,
              JournalMemo NVARCHAR(512)  ,
              CorrespondingAccountNumber NVARCHAR(20)
                 ,
              ExchangeRate DECIMAL ,
              DebitAmountOC DECIMAL(25,4) ,
              DebitAmount DECIMAL(25,0) ,
              CreditAmountOC DECIMAL(25,4) ,
              CreditAmount DECIMAL(25,0) ,
              ClosingAmountOC DECIMAL(25,4) ,
              ClosingAmount DECIMAL(25,0) ,
              AccountObjectCode NVARCHAR(255)
                 ,
              AccountObjectName NVARCHAR(512)
                 ,
              EmployeeCode NVARCHAR(255)  ,
              EmployeeName NVARCHAR(255)  ,
              ProjectWorkCode NVARCHAR(20)
                 ,
              ProjectWorkName NVARCHAR(128)
                 ,
              

              ExpenseItemCode NVARCHAR(20)
                 ,
              ExpenseItemName NVARCHAR(128)
                 ,
              ListItemCode NVARCHAR(20)  ,
              ListItemName NVARCHAR(128)  ,
              ContractCode NVARCHAR(50)  ,

              BranchID UNIQUEIDENTIFIER ,
              BranchCode NVARCHAR(25)  ,
              BranchName NVARCHAR(255)  ,
              IsBold BIT ,
              PaymentType INT ,
              OrderType INT ,
              RefOrder INT,
			  OrderPriority INT
            )	
		
        CREATE TABLE #Result1
            (
              RowNum INT PRIMARY KEY ,
              RefID UNIQUEIDENTIFIER ,
              BankAccount NVARCHAR(500)  ,
              PostedDate DATETIME ,
              RefDate DATETIME ,
              RefNo NVARCHAR(25) ,
              RefType INT ,
              JournalMemo NVARCHAR(512),
              CorrespondingAccountNumber NVARCHAR(20) ,
              ExchangeRate DECIMAL ,
              DebitAmountOC DECIMAL(25,4),
              DebitAmount DECIMAL(25,0) ,
              CreditAmountOC DECIMAL(25,4) ,
              CreditAmount DECIMAL(25,0) ,
              ClosingAmountOC DECIMAL(25,4) ,
              ClosingAmount DECIMAL(25,0) ,
              AccountObjectCode NVARCHAR(255)
                 ,
              AccountObjectName NVARCHAR(512)
                 ,
              EmployeeCode NVARCHAR(255)  ,
              EmployeeName NVARCHAR(255)  ,

              ProjectWorkCode NVARCHAR(20) ,
              ProjectWorkName NVARCHAR(128) ,
              

              ExpenseItemCode NVARCHAR(20) ,
              ExpenseItemName NVARCHAR(128) ,
              ListItemCode NVARCHAR(20) ,
              ListItemName NVARCHAR(128)  ,
              ContractCode NVARCHAR(50) ,

              BranchID UNIQUEIDENTIFIER ,
              BranchCode NVARCHAR(25)  ,
              BranchName NVARCHAR(255)  ,
              IsBold BIT ,
              PaymentType INT ,
              OrderType INT ,
              RefOrder INT,
			  OrderPriority INT
            )	
		
        DECLARE @BankAccountAll UNIQUEIDENTIFIER/*tất cả tài khoản ngân hàng*/
        SET @BankAccountAll = 'A0624CFA-D105-422f-BF20-11F246704DC3'
		
        DECLARE @AccountNumberPercent NVARCHAR(25)
        SET @AccountNumberPercent = @AccountNumber + '%'
    
        DECLARE @ClosingAmountOC DECIMAL(25, 4)
        DECLARE @ClosingAmount DECIMAL(25, 4)
   	    DECLARE @tblListBankAccountDetail TABLE
            (
			  ID UNIQUEIDENTIFIER,
              BankAccount NVARCHAR(Max) ,
              BankAccountName NVARCHAR(MAX) 
            ) 
        if(@BankAccountID is null)     
        INSERT  INTO @tblListBankAccountDetail
                SELECT  BAD.ID,
				        BAD.BankAccount,
				        BAD.BankName
                FROM    BankAccountDetail BAD,
				        Bank BA
                WHERE BAD.BankID = BA.ID    
		else 	
			INSERT  INTO @tblListBankAccountDetail
                SELECT  BAD.ID,
				        BAD.BankAccount,
				        BAD.BankName
                FROM    BankAccountDetail BAD,
				        Bank BA
                WHERE BAD.BankID = BA.ID  
				and BAD.ID =  @BankAccountID 
	

        IF @ClosingAmount IS NULL
            SET @ClosingAmount = 0
        IF @ClosingAmountOC IS NULL
            SET @ClosingAmountOC = 0
            DECLARE @BankAccount NVARCHAR(500)
        DECLARE @CloseAmountOC DECIMAL(25,4)
        DECLARE @CloseAmount DECIMAL(25,0)
	  IF(@CurrencyID = 'VND')
	  BEGIN
	   INSERT  #Result
                ( BankAccount ,
                  JournalMemo ,
                  DebitAmount ,
                  DebitAmountOC ,
                  CreditAmount ,
                  CreditAmountOC ,
                  ClosingAmountOC ,
                  ClosingAmount ,
                  IsBold ,
                  OrderType   
                )
                SELECT  
		                BA.BankAccount + ' - ' + BA.BankAccountName,
                        N'Số dư đầu kỳ' AS JournalMemo ,
                        $0 AS DebitAmountOC ,
                        $0 AS DebitAmount ,
                        $0 AS CreditAmountOC ,
                        $0 AS CreditAmount ,
                        ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) ,
                        ISNULL(SUM(GL.DebitAmount - GL.CreditAmount), 0) ,
                        1 ,
                        0
                FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
				        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID
                WHERE   ( GL.PostedDate < @FromDate )
                        AND GL.Account LIKE @AccountNumberPercent
                GROUP BY BA.BankAccount , BA.BankAccountName
                HAVING  ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) <> 0
                        OR ISNULL(SUM(GL.DebitAmount - GL.CreditAmount), 0) <> 0


       INSERT  #Result
	   (
              RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
			  RefType,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder,
			  OrderPriority )
                    SELECT  GL.ReferenceID ,
	                        BA.BankAccount + ' - ' + BA.BankAccountName,
                            GL.PostedDate ,
                            GL.RefDate ,
                            GL.RefNo AS RefNoFinance ,
							GL.TypeID,
                            GL.Description,
                            GL.AccountCorresponding ,
                            GL.ExchangeRate ,
                            ISNULL(GL.DebitAmountOriginal, 0) AS DebitAmountOC ,
                            ISNULL(GL.DebitAmount, 0) AS DebitAmount ,
                            ISNULL(GL.CreditAmountOriginal, 0) AS CreditAmountOC ,
                            ISNULL(GL.CreditAmount, 0) AS CreditAmount ,
                            $0 AS ClosingAmountOC ,
                            $0 AS ClosingAmount ,
                            AO.AccountingObjectCode ,
                            AO.AccountingObjectName AS AccountObjectName ,
                            EI.ExpenseItemCode ,
                            EI.ExpenseItemName ,
                            GL.BranchID ,
                            0 ,
                            CASE WHEN GL.DebitAmount <> 0 THEN 0
                                 ELSE 1
                            END AS PaymentType ,
                            1 ,
                            0,
							null
                    FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
					        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID


                            LEFT JOIN dbo.AccountingObject AO ON GL.AccountingObjectID = AO.ID
                            LEFT JOIN dbo.ExpenseItem EI ON EI.ID = GL.ExpenseItemID
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND GL.Account LIKE @AccountNumberPercent
                           
                            AND ( GL.DebitAmountOriginal <> 0
                                  OR GL.CreditAmountOriginal <> 0
                                  OR GL.DebitAmount <> 0
                                  OR GL.CreditAmount <> 0
                                )
                    ORDER BY BA.BankAccount ,
                            PostedDate ,OrderPriority,
                            RefDate ,
                            PaymentType ,
                            RefNo
                    
  
        SET @BankAccount = NULL
        INSERT  INTO #Result1
		(     RowNum,
		      RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
			  RefType,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder,
			  OrderPriority)
                SELECT  ROW_NUMBER() OVER ( ORDER BY BankAccount , OrderType, PostedDate , CASE
                                                              WHEN @IsSoftOrderVoucher = 1
                                                              THEN RefOrder
                                                              ELSE 0
                                                              END, RefDate , PaymentType , RefNo ) ,
                        RefID ,
                        BankAccount ,
                        PostedDate ,
                        RefDate ,
                        RefNo ,
						RefType,
                        JournalMemo ,
                        CorrespondingAccountNumber ,
                        ExchangeRate ,
                        DebitAmountOC ,
                        DebitAmount ,
                        CreditAmountOC ,
                        CreditAmount ,
                        ClosingAmountOC ,
                        ClosingAmount ,
                        AccountObjectCode ,
                        AccountObjectName ,

                        ExpenseItemCode ,
                        ExpenseItemName ,
                        BranchID ,
						IsBold  ,
					    PaymentType  ,
						OrderType  ,
						RefOrder,
						OrderPriority
                FROM    #Result
                ORDER BY BankAccount ,
                        OrderType ,
                        PostedDate ,OrderPriority,
                        CASE WHEN @IsSoftOrderVoucher = 1 THEN RefOrder
                             ELSE 0
                        END ,
                        RefDate ,
                        PaymentType ,
                        RefNo
	  END
	  ELSE
	  BEGIN
        INSERT  #Result
                ( BankAccount ,
                  JournalMemo ,
                  DebitAmount ,
                  DebitAmountOC ,
                  CreditAmount ,
                  CreditAmountOC ,
                  ClosingAmountOC ,
                  ClosingAmount ,
                  IsBold ,
                  OrderType   
                )
                SELECT  
		                BA.BankAccount + ' - ' + BA.BankAccountName,
                        N'Số dư đầu kỳ' AS JournalMemo ,
                        $0 AS DebitAmountOC ,
                        $0 AS DebitAmount ,
                        $0 AS CreditAmountOC ,
                        $0 AS CreditAmount ,
                        ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) ,
                        ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) ,
                        1 ,
                        0
                FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
				        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID
                WHERE   ( GL.PostedDate < @FromDate )
                        AND GL.Account LIKE @AccountNumberPercent
                        AND ( @CurrencyID IS NULL
                              OR GL.CurrencyID = @CurrencyID
                            )
                GROUP BY BA.BankAccount , BA.BankAccountName
                HAVING  ISNULL(SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal), 0) <> 0
                        OR ISNULL(SUM(GL.DebitAmount - GL.CreditAmount), 0) <> 0

       INSERT  #Result
	   (
              RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
			  RefType,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder,
			  OrderPriority )
                    SELECT  GL.ReferenceID ,
	                        BA.BankAccount + ' - ' + BA.BankAccountName,
                            GL.PostedDate ,
                            GL.RefDate ,
                            GL.RefNo AS RefNoFinance ,
							GL.TypeID,
                            GL.Description,
                            GL.AccountCorresponding ,
                            GL.ExchangeRate ,
                            ISNULL(GL.DebitAmountOriginal, 0) AS DebitAmountOC ,
                            ISNULL(GL.DebitAmountOriginal, 0) AS DebitAmount ,
                            ISNULL(GL.CreditAmountOriginal, 0) AS CreditAmountOC ,
                            ISNULL(GL.CreditAmountOriginal, 0) AS CreditAmount ,
                            $0 AS ClosingAmountOC ,
                            $0 AS ClosingAmount ,
                            AO.AccountingObjectCode ,
                            AO.AccountingObjectName AS AccountObjectName ,
                            EI.ExpenseItemCode ,
                            EI.ExpenseItemName ,
                            GL.BranchID ,
                            0 ,
                            CASE WHEN GL.DebitAmount <> 0 THEN 0
                                 ELSE 1
                            END AS PaymentType ,
                            1 ,
                            0,
							null
                    FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
					        JOIN @tblListBankAccountDetail BA on BA.ID = GL.BankAccountDetailID


                            LEFT JOIN dbo.AccountingObject AO ON GL.AccountingObjectID = AO.ID
                            LEFT JOIN dbo.ExpenseItem EI ON EI.ID = GL.ExpenseItemID
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND GL.Account LIKE @AccountNumberPercent
                            AND ( @CurrencyID IS NULL
                                  OR GL.CurrencyID = @CurrencyID
                                )
                            AND ( GL.DebitAmountOriginal <> 0
                                  OR GL.CreditAmountOriginal <> 0
                                  OR GL.DebitAmount <> 0
                                  OR GL.CreditAmount <> 0
                                )
                    ORDER BY BA.BankAccount ,
                            PostedDate ,OrderPriority,
                            RefDate ,
                            PaymentType ,
                            RefNo
                  
        SET @BankAccount = NULL
        INSERT  INTO #Result1
		(     RowNum,
		      RefID ,
              BankAccount,
              PostedDate  ,
              RefDate  ,
              RefNo  ,
			  RefType,
              JournalMemo ,
              CorrespondingAccountNumber,
              ExchangeRate ,
              DebitAmountOC ,
              DebitAmount,
              CreditAmountOC ,
              CreditAmount,
              ClosingAmountOC ,
              ClosingAmount ,
              AccountObjectCode ,
              AccountObjectName ,
              ExpenseItemCode,
              ExpenseItemName,
              BranchID  ,
              IsBold  ,
              PaymentType  ,
              OrderType  ,
              RefOrder,
			  OrderPriority)
                SELECT  ROW_NUMBER() OVER ( ORDER BY BankAccount , OrderType, PostedDate , CASE
                                                              WHEN @IsSoftOrderVoucher = 1
                                                              THEN RefOrder
                                                              ELSE 0
                                                              END, RefDate , PaymentType , RefNo ) ,
                        RefID ,
                        BankAccount ,
                        PostedDate ,
                        RefDate ,
                        RefNo ,
						RefType,
                        JournalMemo ,
                        CorrespondingAccountNumber ,
                        ExchangeRate ,
                        DebitAmountOC ,
                        DebitAmount ,
                        CreditAmountOC ,
                        CreditAmount ,
                        ClosingAmountOC ,
                        ClosingAmount ,
                        AccountObjectCode ,
                        AccountObjectName ,

                        ExpenseItemCode ,
                        ExpenseItemName ,
                        BranchID ,
						IsBold  ,
					    PaymentType  ,
						OrderType  ,
						RefOrder,
						OrderPriority
                FROM    #Result
                ORDER BY BankAccount ,
                        OrderType ,
                        PostedDate ,OrderPriority,
                        CASE WHEN @IsSoftOrderVoucher = 1 THEN RefOrder
                             ELSE 0
                        END ,
                        RefDate ,
                        PaymentType ,
                        RefNo
        END
	  
        SET @CloseAmount = 0
        SET @CloseAmountOC = 0

        UPDATE  #Result1
        SET     @CloseAmount = ( CASE WHEN OrderType = 0
                                      THEN ( CASE WHEN ClosingAmount = 0
                                                  THEN 0
                                                  ELSE ClosingAmount
                                             END )
                                      WHEN @BankAccount <> BankAccount
                                      THEN DebitAmount - CreditAmount
                                      ELSE @CloseAmount + DebitAmount
                                           - CreditAmount
                                 END ) ,
                @CloseAmountOC = ( CASE WHEN OrderType = 0
                                        THEN ( CASE WHEN ClosingAmountOC = 0
                                                    THEN 0
                                                    ELSE ClosingAmountOC
                                               END )
                                        WHEN @BankAccount <> BankAccount
                                        THEN DebitAmountOC - CreditAmountOC
                                        ELSE @CloseAmountOC + DebitAmountOC
                                             - CreditAmountOC
                                   END ) ,
                ClosingAmount = @CloseAmount ,
                ClosingAmountOC = @CloseAmountOC ,
                @BankAccount = BankAccount

        SELECT  RefID,RefType,BankAccount,PostedDate,RefDate,RefNo,JournalMemo,CorrespondingAccountNumber,DebitAmountOC,DebitAmount,CreditAmountOC,CreditAmount,ClosingAmountOC,ClosingAmount,AccountObjectCode,AccountObjectName,2 as ordercode 
		into #tg1
        FROM    #Result1
        ORDER BY BankAccount ,
                OrderType ,
                PostedDate ,OrderPriority,
                CASE WHEN @IsSoftOrderVoucher = 1 THEN RefOrder
                     ELSE 0
                END ,
                RefDate ,
                PaymentType ,
                RefNo
			INSERT INTO #tg1 (RefID,RefType,BankAccount,PostedDate,RefDate,RefNo,JournalMemo,CorrespondingAccountNumber,DebitAmountOC,DebitAmount,CreditAmountOC,CreditAmount,ClosingAmountOC,ClosingAmount,AccountObjectCode,AccountObjectName,ordercode)  
			SELECT  null,null,BankAccount,null,null,null,N'Tài khoản ngân hàng: ' + BankAccount,null,null,null,null,null,null,null,null,null,1
			FROM #Result1
			group by BankAccount
				
			SELECT TOP 0 *
			into #tg2
			FROM #tg1

			INSERT INTO #tg2 (RefID,RefType,BankAccount,PostedDate,RefDate,RefNo,JournalMemo,CorrespondingAccountNumber,DebitAmountOC,DebitAmount,CreditAmountOC,CreditAmount,ClosingAmountOC,ClosingAmount,AccountObjectCode,AccountObjectName,ordercode)  
			SELECT  null,null,BankAccount,null,null,null,N'Cộng nhóm',null,Sum(DebitAmountOC),Sum(DebitAmount),Sum(CreditAmountOC),Sum(CreditAmount),0,0,null,null,3
			FROM #Result1
			group by BankAccount
			
			
			SELECT  BankAccount into #bankAccount from #tg1

			DECLARE @BankAcc nvarchar(512)
			DECLARE @COUNT INT
			DECLARE @SODUDAUKY Decimal(25,0)
			DECLARE @SODUDAUKYOC Decimal(25,4)
			SET @COUNT = (SELECT COUNT(*) From #bankAccount)
			WHILE(@COUNT > 0)
			BEGIN
			SET @BankAcc = (SELECT TOP 1 BankAccount FROM #bankAccount)
			SET @SODUDAUKY = ISNULL((select Sum(ClosingAmount) from #tg1 where JournalMemo like N'%Số dư đầu kỳ%' AND  BankAccount = @BankAcc ),0)
			SET @SODUDAUKYOC = ISNULL((select Sum(ClosingAmountOC) from #tg1 where JournalMemo like N'%Số dư đầu kỳ%' AND  BankAccount = @BankAcc ),0)
			UPDATE #tg2
			SET ClosingAmount  =  @SODUDAUKY
			+ (SELECT SUM(DebitAmount) FROM #tg1 WHERE BankAccount = @BankAcc)
			- (SELECT SUM(CreditAmount) FROM #tg1 WHERE BankAccount = @BankAcc),
 
			ClosingAmountOC  =  @SODUDAUKYOC
			+ (SELECT SUM(DebitAmountOC) FROM #tg1 WHERE BankAccount = @BankAcc)
			- (SELECT SUM(CreditAmountOC) FROM #tg1 WHERE BankAccount = @BankAcc) 

			WHERE BankAccount = @BankAcc
			SET @COUNT = @COUNT -1;
			DELETE #bankAccount where BankAccount = @BankAcc
			END
		
		INSERT INTO #tg1 SELECT * from #tg2

			
			SELECT * from #tg1 order by BankAccount,ordercode
        DROP TABLE #Result
        DROP TABLE #Result1
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_S02C1DNN]
    @BranchID UNIQUEIDENTIFIER,
    @IncludeDependentBranch BIT,
    @StartDate DATETIME,
    @FromDate DATETIME,
    @ToDate DATETIME,
    @AccountNumber NVARCHAR(MAX),
    @IsSimilarSum BIT
AS
    BEGIN
        SET NOCOUNT ON
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)
		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
		CREATE TABLE #Result
            (
             RefID UNIQUEIDENTIFIER,
             RefType INT,
             PostedDate DATETIME,
             RefNo NVARCHAR(25),
             RefDate DATETIME,
             InvDate DATETIME,
             InvNo NVARCHAR(MAX),
             JournalMemo NVARCHAR(512),
             AccountNumber NVARCHAR(25),
			 DetailAccountNumber NVARCHAR(25) ,
             AccountCategoryKind INT,
             AccountName NVARCHAR(512),
             CorrespondingAccountNumber NVARCHAR(25),
             DebitAmount MONEY,
             CreditAmount MONEY,
             OrderType INT,
             IsBold BIT,
             OrderNumber int
            )
        CREATE TABLE #tblAccountNumber
            (
             AccountNumber NVARCHAR(25)    PRIMARY KEY,
             AccountName NVARCHAR(512)  ,
             AccountNumberPercent NVARCHAR(25)  ,
             AccountCategoryKind INT,
			 IsParent BIT
            )
        INSERT  #tblAccountNumber
                SELECT  A1.AccountNumber,
                        A1.AccountName,
                        A1.AccountNumber + '%',
                        A1.AccountGroupKind,
						A1.IsParentNode
                FROM    dbo.Func_ConvertStringIntoTable_Nvarchar(@AccountNumber,',') F
                        INNER JOIN dbo.Account A1 ON A1.AccountNumber = F.Value
        IF @IsSimilarSum = 0 /*khong cong gop but ke toan*/
		Begin
            INSERT  #Result
                    (
                     RefID,
                     RefType,
                     PostedDate,
                     RefNo,
                     RefDate,
                     InvDate,
                     InvNo,
                     JournalMemo,
                     AccountNumber,
					 DetailAccountNumber,
                     AccountName,
                     CorrespondingAccountNumber,
                     AccountCategoryKind,
                     DebitAmount,
                     CreditAmount,
                     ORDERType,
                     IsBold,
                     OrderNumber
                    )
                    SELECT
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Description AS JournalMemo,
                            AC.AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind,
                            DebitAmount,
                            CreditAmount,
                            1 AS ORDERType,
                            0,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%')
											AND GL.DebitAmount <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
                            AND (GL.DebitAmount <> 0
                                 OR GL.CreditAmount <> 0
                                 or GL.DebitAmountOriginal <> 0
                                 OR GL.CreditAmountOriginal <> 0
                                )
              End
			  Else
			  Begin /*cong gop but toan thue*/
				INSERT  #Result(RefID, RefType, PostedDate, RefNo, RefDate, InvDate, InvNo, JournalMemo, AccountNumber, DetailAccountNumber, AccountName, CorrespondingAccountNumber,
                     AccountCategoryKind, DebitAmount, CreditAmount, ORDERType, IsBold, OrderNumber)
                SELECT RefID, RefType, PostedDate, RefNo, RefDate, InvDate, InvNo, JournalMemo, AccountNumber, DetailAccountNumber, AccountName, CorrespondingAccountNumber,
                     AccountCategoryKind, DebitAmount, CreditAmount, ORDERType, IsBold, OrderNumber
				FROM
				    (SELECT
                            GL.ReferenceID as RefID,
                            GL.TypeID as RefType,
                            GL.PostedDate as PostedDate,
                            RefNo,
                            GL.RefDate as RefDate,
                            Gl.InvoiceDate as InvDate,
                            GL.InvoiceNo as InvNo,
                            GL.Reason AS JournalMemo,
                            AC.AccountNumber as AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName as AccountName,
                            GL.AccountCorresponding as CorrespondingAccountNumber,
                            AC.AccountCategoryKind as AccountCategoryKind,
                            SUM(GL.DebitAmount) as DebitAmount,
                            0 as CreditAmount,
                            1 AS ORDERType,
                            0 as IsBold,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%')
											AND SUM(GL.DebitAmount) <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
					GROUP BY
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Reason,
                            AC.AccountNumber,
							GL.Account,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind
					HAVING SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0
				UNION ALL
				SELECT
                            GL.ReferenceID as RefID,
                            GL.TypeID as RefType,
                            GL.PostedDate as PostedDate,
                            RefNo,
                            GL.RefDate as RefDate,
                            Gl.InvoiceDate as InvDate,
                            GL.InvoiceNo as InvNo,
                            GL.Reason AS JournalMemo,
                            AC.AccountNumber as AccountNumber,
							GL.Account AS DetailAccountNumber,
                            AC.AccountName as AccountName,
                            GL.AccountCorresponding as CorrespondingAccountNumber,
                            AC.AccountCategoryKind as AccountCategoryKind,
                            0 as DebitAmount,
                            SUM(GL.CreditAmount) as CreditAmount,
                            1 AS ORDERType,
                            0 as IsBold,
                            CASE WHEN (AC.AccountNumber LIKE N'11%' OR AC.AccountNumber LIKE N'15%' AND AC.AccountNumber NOT LIKE N'154%')
											AND SUM(GL.CreditAmount) <> 0
                                             THEN 0
                                             ELSE 1
                                        END AS OrderNumber
                    FROM    @tbDataGL AS GL
                            INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                    WHERE   GL.PostedDate BETWEEN @FromDate AND @ToDate
					GROUP BY
                            GL.ReferenceID,
                            GL.TypeID,
                            GL.PostedDate,
                            RefNo,
                            GL.RefDate,
                            Gl.InvoiceDate,
                            GL.InvoiceNo,
                            GL.Reason,
                            AC.AccountNumber,
							GL.Account,
                            AC.AccountName,
                            GL.AccountCorresponding,
                            AC.AccountCategoryKind
					HAVING SUM(GL.DebitAmount) <> 0 OR SUM(GL.CreditAmount) <> 0
				) T
				where T.DebitAmount > 0 OR T.CreditAmount>0
			  End
        SELECT  AccountNumber,
                AccountName,
				IsParent,
                SUM(OpenningDebitAmount) AS OpenningDebitAmount,
                SUM(OpenningCreditAmount) AS OpenningCreditAmount,
                SUM(ClosingDebitAmount) AS ClosingDebitAmount,
                SUM(ClosingCreditAmount) AS ClosingCreditAmount,
                SUM(AccumDebitAmount) AS AccumDebitAmount,
                SUM(AccumCreditAmount) AS AccumCreditAmount,
				AccountCategoryKind
        FROM    (
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS OpenningDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate < @FromDate
                 GROUP BY GL.BranchID,
                        AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        CASE WHEN AC.AccountCategoryKind = 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             WHEN ac.AccountCategoryKind = 2
                                  AND SUM(GL.DebitAmount - GL.CreditAmount) > 0
                             THEN SUM(GL.DebitAmount - GL.CreditAmount)
                             ELSE 0
                        END AS ClosingDebitAmount,
                        CASE WHEN AC.AccountCategoryKind = 1
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             WHEN AC.AccountCategoryKind = 2
                                  AND SUM(GL.CreditAmount - GL.DebitAmount) > 0
                             THEN SUM(GL.CreditAmount - GL.DebitAmount)
                             ELSE 0
                        END AS ClosingCreditAmount,
                        0 AS AccumDebitAmount,
                        0 AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate <= @ToDate
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                 UNION ALL
                 SELECT AC.AccountNumber,
                        AC.AccountName,
                        0 AS OpenningDebitAmount,
                        0 AS OpenningCreditAmount,
                        0 AS ClosingDebitAmount,
                        0 AS ClosingCreditAmount,
                        SUM(GL.DebitAmount) AS AccumDebitAmount,
                        SUM(GL.CreditAmount) AS AccumCreditAmount,
						AC.IsParent,
						AC.AccountCategoryKind
                 FROM   @tbDataGL GL
                        INNER JOIN #tblAccountNumber AC ON GL.Account LIKE AC.AccountNumberPercent
                 WHERE  GL.PostedDate BETWEEN @StartDate AND @ToDate
                 GROUP BY AC.AccountNumber,
                        AC.AccountName,
                        AC.AccountCategoryKind,
						AC.IsParent
                ) AS A
        GROUP BY AccountNumber,
                AccountName,
				IsParent ,
				AccountCategoryKind
        HAVING
				SUM(OpenningDebitAmount) <>0 OR
                SUM(OpenningCreditAmount)  <>0 OR
                SUM(ClosingDebitAmount)  <>0 OR
                SUM(ClosingCreditAmount) <>0 OR
                SUM(AccumDebitAmount) <>0 OR
                SUM(AccumCreditAmount) <>0
        ORDER BY AccountNumber
		SELECT  ROW_NUMBER() OVER (ORDER BY AccountNumber,DetailAccountNumber, OrderType, PostedDate, RefDate,OrderNumber, RefNo) AS RowNum,
				RefID,
				RefType,
				PostedDate,
				RefDate,
				RefNo,
				InvDate,
				InvNo,
				JournalMemo,
				AccountNumber,
				DetailAccountNumber,
				CorrespondingAccountNumber,
				DebitAmount,
				CreditAmount,
				IsBold,
				AccountCategoryKind
		FROM    #Result
    END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_SoTongHopLuongNhanVien]
	@BeginMonth int,
	@BeginYear int,
	@EndMonth int,
	@EndYear int,
    @AccountingObjectCode NVARCHAR(MAX)
AS
BEGIN     
	DECLARE @tblSoTongHopLuongNhanVien TABLE(
			ID uniqueidentifier,
			AccountingObjectCode  NVARCHAR(25),
			AccountingObjectName NVARCHAR(512),
			InMonth int,
			InYear int,
			TaxCode NVARCHAR(50),
			IdentificationNo NVARCHAR(25),
			TotalPersonalTaxIncomeAmount decimal(25,0),
			GiamTruGiaCanh decimal(25,0),
			BaoHiemDuocTru decimal(25,0),
			IncomeForTaxCalcuation  decimal(25,0),
			IncomeTaxAmount  decimal(25,0),
			NetAmount  decimal(25,0),
			dateSheet datetime		
			)
	Declare @NgayThang Date
	DECLARE @Count int
	DECLARE @Count2 int
	DECLARE @Count3 int
	DECLARE @Count4 int
	DECLARE @tblListAccountingObjectCode TABLE
	(
	AccountingObjectCode NVARCHAR(MAX)
	)

		 INSERT  INTO @tblListAccountingObjectCode
         SELECT  TG.ID
         FROM    AccountingObject AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountingObjectCode,',') AS AccountingObjectCode ON TG.ID = AccountingObjectCode.Value
         WHERE  AccountingObjectCode.Value IS NOT NULL
		 
		 SELECT a.EmployeeID,b.AccountingObjectCode , b.AccountingObjectName ,c.Month as InMonth, c.Year as InYear,b.TaxCode,b.IdentificationNo, a.TotalPersonalTaxIncomeAmount ,(a.ReduceSelfTaxAmount + a.ReduceDependTaxAmount ) as GiamTruGiaCanh,(a.EmployeeSocityInsuranceAmount  + a.EmployeeAccidentInsuranceAmount  + a.EmployeeMedicalInsuranceAmount + a.EmployeeUnEmployeeInsuranceAmount + a.EmployeeTradeUnionInsuranceAmount ) as BaoHiemDuocTru , a.IncomeForTaxCalcuation  , a.IncomeTaxAmount  ,a.NetAmount, (CONVERT(nvarchar(4),c.Year)+'-'+CONVERT(nvarchar(2),c.Month)+'-01') as dateSheet
		 into #tg
		FROM [PSSalarySheetDetail] a join [AccountingObject] b on a.EmployeeID = b.ID join [PSSalarySheet] c on a.PSSalarySheetID=c.ID
		 where employeeid in (Select AccountingObjectCode From @tblListAccountingObjectCode)

		 DECLARE @FromDate datetime
		 DECLARE @ToDate datetime
		 SET @FromDate = (CONVERT(nvarchar(4),@BeginYear)+'-'+CONVERT(nvarchar(2),@BeginMonth)+'-01')
		 SET @ToDate = (CONVERT(nvarchar(4),@EndYear)+'-'+CONVERT(nvarchar(2),@EndMonth)+'-01')

		INSERT INTO @tblSoTongHopLuongNhanVien
		SELECT * FROM #tg where dateSheet between @FromDate AND @ToDate	

		Select ID,AccountingObjectCode,AccountingObjectName, TaxCode,IdentificationNo,Sum(TotalPersonalTaxIncomeAmount) as TotalPersonalTaxIncomeAmount ,Sum(GiamTruGiaCanh) as GiamTruGiaCanh,Sum(BaoHiemDuocTru) as BaoHiemDuocTru,Sum(IncomeForTaxCalcuation) as IncomeForTaxCalcuation,Sum(IncomeTaxAmount) as IncomeTaxAmount,Sum(NetAmount) as NetAmount 
		From @tblSoTongHopLuongNhanVien 
		where TotalPersonalTaxIncomeAmount >0
		group by ID,AccountingObjectCode,AccountingObjectName,TaxCode,IdentificationNo 
		order by AccountingObjectCode
				
END




		


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_SoTheoDoiChiTietTheoMaThongKe]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @StatisticsCodeID  NVARCHAR(MAX),
	@Account NVARCHAR(MAX)
AS
BEGIN     
	DECLARE @tblSoTheoDoiTheoMTK TABLE(
			StatisticsCodeID uniqueidentifier,
			StatisticsCode  NVARCHAR(25),
			StatisticsCodeName NVARCHAR(512),
			NgayChungTu Date,
			 SoChungTu NCHAR(20),
			 DienGiai NVARCHAR(MAX),
			 TK NVARCHAR(50),
			 TKDoiUng NVARCHAR(50),
			 SoTienNo decimal(25,0),
			 SoTienCo decimal(25,0),
			 OrderPriority int,
			 RefID uniqueidentifier,
			 RefType int
			 )
			 
	
	
	DECLARE @tblListStatisticsCodeID TABLE
	(
	StatisticsCodeID uniqueidentifier
	)
	DECLARE @tblListAccount TABLE
	(
	Account NVARCHAR(25)
	)

		 INSERT  INTO @tblListStatisticsCodeID
         SELECT  TG.id
         FROM    StatisticsCode AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@StatisticsCodeID,',') AS StatisticCodeID ON TG.ID = StatisticCodeID.Value
         WHERE  StatisticCodeID.Value IS NOT NULL

		 INSERT  INTO @tblListAccount
         SELECT  TG.AccountNumber
         FROM    Account AS TG
                 LEFT JOIN dbo.Func_SplitString(@Account,',') AS Account ON TG.AccountNumber = Account.splitdata
         WHERE  Account.splitdata IS NOT NULL
                              			
		INSERT INTO @tblSoTheoDoiTheoMTK
		SELECT a.StatisticsCodeID, b.StatisticsCode, b.StatisticsCodeName ,Date as NgayChungTu , No as SoChungTu, Reason as DienGiai, Account as TK, AccountCorresponding as TKDoiUng, DebitAmount as SoTienNo, CreditAmount as SoTienCo, a.OrderPriority, a.ReferenceID as RefID, a.TypeID as RefType
		FROM [GeneralLedger] a 
		LEFT JOIN [StatisticsCode] b ON a.StatisticsCodeID = b.ID
		where StatisticsCodeID in (select StatisticsCodeID from @tblListStatisticsCodeID)   AND a.Account in (select Account from @tblListAccount)
		and Date > = @FromDate and Date < = @ToDate
		order by Date, a.OrderPriority

		Select * From @tblSoTheoDoiTheoMTK order by NgayChungTu,SoChungTu,OrderPriority
END


GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

ALTER PROCEDURE [dbo].[Proc_SoChiTietCongTrinh]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @CostSetID  NVARCHAR(MAX)
AS
BEGIN     
	DECLARE @tblSoChiTietCongTrinh TABLE(
			CostSetID uniqueidentifier,
			CostSetCode  NVARCHAR(25),
			CostSetName NVARCHAR(512),	
			No Nvarchar(25),
			ExpenseItemID uniqueidentifier,
			PostedDate date,
			Date date,
			Reason NVARCHAR(512),
			DoanhThu decimal(25,0),
			 GiamTru decimal(25,0),
			NVLTT decimal(25,0),
			NCTT decimal(25,0),
			CPSXC decimal(25,0),
			Cong decimal(25,0),
			 GiaVon decimal(25,0),
			 LaiLo decimal(25,0),
			 ExpenseType int,
			 CostSetType int,
			 RefID uniqueidentifier,
			 RefType int) 
	

				DECLARE @tbDataGL TABLE(
				ID uniqueidentifier,
				BranchID uniqueidentifier,
				ReferenceID uniqueidentifier,
				TypeID int,
				Date datetime,
				PostedDate datetime,
				No nvarchar(25),
				InvoiceDate datetime,
				InvoiceNo nvarchar(25),
				Account nvarchar(25),
				AccountCorresponding nvarchar(25),
				BankAccountDetailID uniqueidentifier,
				CurrencyID nvarchar(3),
				ExchangeRate decimal(25, 10),
				DebitAmount decimal(25,0),
				DebitAmountOriginal decimal(25,0),
				CreditAmount decimal(25,0),
				CreditAmountOriginal decimal(25,0),
				Reason nvarchar(512),
				Description nvarchar(512),
				VATDescription nvarchar(512),
				AccountingObjectID uniqueidentifier,
				EmployeeID uniqueidentifier,
				BudgetItemID uniqueidentifier,
				CostSetID uniqueidentifier,
				ContractID uniqueidentifier,
				StatisticsCodeID uniqueidentifier,
				InvoiceSeries nvarchar(25),
				ContactName nvarchar(512),
				DetailID uniqueidentifier,
				RefNo nvarchar(25),
				RefDate datetime,
				DepartmentID uniqueidentifier,
				ExpenseItemID uniqueidentifier,
				OrderPriority int,
				IsIrrationalCost bit
			)

			INSERT INTO @tbDataGL
			SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
	
	DECLARE @tblListCostSetID TABLE
	(
	CostSetID uniqueidentifier
	)

		 INSERT  INTO @tblListCostSetID
         SELECT  TG.id
         FROM    CostSet AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@CostSetID,',') AS CostSetID ON TG.ID = CostSetID.Value
         WHERE  CostSetID.Value IS NOT NULL

		 INSERT INTO @tblSoChiTietCongTrinh SELECT DISTINCT(CostSetID),null,null,a.No,a.ExpenseItemID,null,null,null,0,0,0,0,0,0,0,0,0,0,a.ReferenceID as RefID, a.TypeID as RefType
		 From @tbDataGL a
		 WHERE  a.CostSetID IN (Select * From @tblListCostSetID)
		 AND PostedDate >= @FromDate AND PostedDate<=@ToDate
	DECLARE @CostSetType TABLE
	(
	CostSetID uniqueidentifier,
	CostSetCode NVARCHAR(25),
	CostSetName NVARCHAR(512),
	CostSetType int
	)
	INSERT INTO @CostSetType SELECT ID,CostSetCode,CostSetName,CostSetType from CostSet a 
	WHERE ID IN (SELECT  * from @tblListCostSetID) AND CostSetType = 1
	UPDATE @tblSoChiTietCongTrinh SET CostSetType = f.CostSetType, CostSetCode = f.CostSetCode,CostSetName = f.CostSetName
	FROM (SELECT CostSetID as CSI,CostSetCode,CostSetName, CostSetType FROM @CostSetType) f
	WHERE CostSetID = f.CSI

	DECLARE @ExpenseType TABLE
	(
	ExpenseItemID uniqueidentifier,
	ExpenseType int
	)
	INSERT INTO @ExpenseType SELECT ID,ExpenseType from ExpenseItem a 
	WHERE ExpenseType IN (0,1,2)
	UPDATE @tblSoChiTietCongTrinh SET ExpenseType = f.ExpenseType
	FROM (SELECT ExpenseItemID as EII, ExpenseType FROM @ExpenseType) f
	WHERE ExpenseItemID = f.EII

	
		 DECLARE @tblDoanhThu TABLE
		 (
			CostSetID uniqueidentifier,
			No Nvarchar(25),
			DoanhThu decimal(25,0)
		 )
		 INSERT INTO @tblDoanhThu Select CostSetID,No,CreditAmount
		 From @tbDataGL a
		 WHERE a.CostSetID IN (Select * From @tblListCostSetID)  AND Account LIKE '511%' AND PostedDate >= @FromDate AND PostedDate<@ToDate
		

	
	UPDATE @tblSoChiTietCongTrinh SET DoanhThu = f.DoanhThu
	FROM (select CostSetID as csid,No as N, DoanhThu from @tblDoanhThu) f
	WHERE CostSetID = f.csid AND  No = f.N
	

		
		 DECLARE @tblGiamTru TABLE
		 (
			CostSetID uniqueidentifier,
			No Nvarchar(25),
			GiamTru decimal(25,0)
		 )
		 INSERT INTO @tblGiamTru Select CostSetID,No,DebitAmount
		 From @tbDataGL a
		 WHERE a.CostSetID IN (Select * From @tblListCostSetID)  AND Account LIKE '511%' AND PostedDate >= @FromDate AND PostedDate<@ToDate
		

	UPDATE @tblSoChiTietCongTrinh SET GiamTru = f.GiamTru
	FROM (select CostSetID as csid,No as N, GiamTru from @tblGiamTru) f
	WHERE CostSetID = f.csid AND  No = f.N
	
	
	
		 DECLARE @tblGiaVon TABLE
		 (
			CostSetID uniqueidentifier,
			No Nvarchar(25),
			GiaVon decimal(25,0)
		 )
		 INSERT INTO @tblGiaVon Select CostSetID,No,DebitAmount
		 From @tbDataGL a
		  WHERE a.CostSetID IN (SELECT * from @tblListCostSetID) AND Account LIKE '632%' AND PostedDate >= @FromDate AND PostedDate<=@ToDate AND AccountCorresponding  LIKE '154%'
		

	UPDATE @tblSoChiTietCongTrinh SET GiaVon = f.GiaVon
	FROM (select CostSetID as csid,No as N, GiaVon from @tblGiaVon) f
	WHERE CostSetID = f.csid AND  No = f.N
	
		

	
	

	 DECLARE @tblInfo TABLE
		 (
			CostSetID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			PostedDate Date,
			Date Date,
			No NVARCHAR(25),
			Reason NVARCHAR(512)
		 )
		 INSERT INTO @tblInfo Select CostSetID,a.ExpenseItemID,PostedDate,Date,No,Reason
		 From @tbDataGL a
		 WHERE a.CostSetID IN (SELECT * from @tblListCostSetID)  AND PostedDate >= @FromDate AND PostedDate<=@ToDate 
	
	
	UPDATE @tblSoChiTietCongTrinh SET  PostedDate = f.PostedDate,Date = f.Date,No = f.N,Reason = f.Reason
	FROM (select CostSetID as csid,ExpenseItemID as EII, PostedDate,Date,No as N,Reason from @tblInfo) f
	WHERE CostSetID = f.csid AND No = f.N

	UPDATE @tblSoChiTietCongTrinh SET LaiLo = (DoanhThu - GiamTru - GiaVon)
		 DECLARE @ChiPhiPhatSinh TABLE
		 (
			CostSetID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			SumDebit decimal(25,0),
			ExpenseType int
		 )
		  INSERT INTO @ChiPhiPhatSinh Select CostSetID,ExpenseItemID,Sum(DebitAmount) as SumDebit,b.ExpenseType
		 From @tbDataGL a, ExpenseItem b
		 WHERE a.CostSetID IN (select * from @tblListCostSetID) AND a.ExpenseItemID = b.ID  AND Account LIKE '154%' AND PostedDate >= @FromDate AND PostedDate<=@ToDate
		 group by CostSetID,ExpenseItemID,ExpenseType
		
		
	UPDATE @tblSoChiTietCongTrinh SET NVLTT = ISNULL(f.SumDebit,0)
	FROM (select CostSetID as csid,ExpenseItemID as  EII,SumDebit,ExpenseType as ET  from @ChiPhiPhatSinh) f
	WHERE CostSetID = f.csid AND ExpenseItemID = f.EII AND ExpenseType = f.ET AND ExpenseType = 0

	UPDATE @tblSoChiTietCongTrinh SET NCTT = ISNULL(f.SumDebit,0)
	FROM (select CostSetID as csid,ExpenseItemID as  EII,SumDebit,ExpenseType as ET  from @ChiPhiPhatSinh) f
	WHERE CostSetID = f.csid AND ExpenseItemID = f.EII AND ExpenseType = f.ET AND ExpenseType = 1

		UPDATE @tblSoChiTietCongTrinh SET CPSXC = ISNULL(f.SumDebit,0)
	FROM (select CostSetID as csid,ExpenseItemID as  EII,SumDebit,ExpenseType as ET  from @ChiPhiPhatSinh) f
	WHERE CostSetID = f.csid AND ExpenseItemID = f.EII AND ExpenseType = f.ET AND ExpenseType = 2

	DECLARE @tblCPChung TABLE
	(
	CostSetId uniqueidentifier,
	ReferenceID uniqueidentifier,
	CPAllocationGeneralExpenseID uniqueidentifier,
	AllocatedAmount decimal(25,0),
	PostedDate Date
	)
	INSERT INTO @tblCPChung 
	Select CostSetID,null,CPAllocationGeneralExpenseID,AllocatedAmount,null From CPAllocationGeneralExpenseDetail WHERE CostSetID IN (SELECT * from @tblListCostSetID)

	DECLARE @ReferenceID TABLE
	(
	ID uniqueidentifier,
	ReferenceID uniqueidentifier
	)
	INSERT INTO @ReferenceID 
	SELECT ID,ReferenceID From [CPAllocationGeneralExpense]

	UPDATE @tblCPChung SET ReferenceID = f.ReferenceID
	FROM (SELECT ID,ReferenceID From @ReferenceID) f
	WHERE CPAllocationGeneralExpenseID = f.ID

	DECLARE @DetailID TABLE 
	(
	CostSetID uniqueidentifier,
	DetailID uniqueidentifier,
	PostedDate date	
	)
	INSERT INTO @DetailID 
	SELECT CostSetID,DetailId,PostedDate From [GeneralLedger] 
	WHERE PostedDate >=@FromDate AND PostedDate <=@ToDate AND CostSetID IN (SELECT * from @tblListCostSetID)


	UPDATE @tblCPChung SET PostedDate = f.PostedDate
	FROM (SELECT CostSetID as CSI,DetailID as DI,PostedDate From @DetailID) f
	WHERE  ReferenceID = f.DI
	Select CostSetId,Sum(AllocatedAmount) as AllocatedAmount into #tg  from @tblCPChung group by CostSetId
	Insert into @tblSoChiTietCongTrinh
	Select CostSetID,null,null,null,null,PostedDate,Date,(N'Kỳ tính giá thành từ ' + CONVERT(NVARCHAR,@FromDate,103) + N' đến ' + CONVERT(NVARCHAR,PostedDate,103)) as Reason,0,0,0,0,0,0,0,0,2,1,RefID,RefType From @tblSoChiTietCongTrinh WHERE Reason LIKE N'%Nghiệm thu kỳ%' group by CostSetId,PostedDate,Date,RefID,RefType
	UPDATE @tblSoChiTietCongTrinh SET CPSXC = f.AllocatedAmount
	FROM (Select CostSetID as CSI,AllocatedAmount From #tg) f
	WHERE CostSetID = f.CSI AND NO IS NULL
	UPDATE @tblSoChiTietCongTrinh SET Cong = (NVLTT + NCTT + CPSXC)
	 
	SELECT RefID, RefType, CostSetCode, CostSetName, PostedDate, [Date], [No], Reason, DoanhThu, GiamTru, NVLTT, NCTT, CPSXC, Cong, GiaVon, LaiLo, 2 as ordercode
	into #tg1
	from @tblSoChiTietCongTrinh 	
	order by CostSetID,Date,No

	insert into #tg1 (RefID, RefType, CostSetCode, CostSetName, PostedDate, [Date], [No], Reason, DoanhThu, GiamTru, NVLTT, NCTT, CPSXC, Cong, GiaVon, LaiLo, ordercode)
	SELECT null,null,CostSetCode,CostSetName,null,null,null,N'Mã công trình: ' + CostSetCode + ' - ' + N'Tên công trình: ' + CostSetName, null,null,null,null,null,null,null,null,1
	From @tblSoChiTietCongTrinh
	group by CostSetCode,CostSetName

	SELECT TOP 0 *
			into #tg2
			FROM #tg1

			INSERT INTO #tg2 (RefID, RefType, CostSetCode, CostSetName, PostedDate, [Date], [No], Reason, DoanhThu, GiamTru, NVLTT, NCTT, CPSXC, Cong, GiaVon, LaiLo, ordercode)  
			SELECT null,null,CostSetCode,CostSetName,null,null,null,N'Cộng nhóm', Sum(DoanhThu),Sum(GiamTru),Sum(NVLTT),Sum(NCTT),Sum(CPSXC),Sum(Cong),Sum(GiaVon),Sum(LaiLo),3
			FROM @tblSoChiTietCongTrinh
			group by CostSetCode, CostSetName

			INSERT INTO #tg1 SELECT * from #tg2

			
			SELECT * from #tg1 order by CostSetCode,ordercode

END






GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO

/*
*/
ALTER PROCEDURE [dbo].[Proc_SoChiTietCongNoNhanVien]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX)

AS
    BEGIN

		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)
		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE PostedDate<@ToDate
		
		DECLARE @tblListAccountingObjectID TABLE
		(
		AccountObjectID uniqueidentifier,
		AccountObjectCode NVARCHAR(25),
		AccountObjectName NVARCHAR(512)
		)

		 INSERT  INTO @tblListAccountingObjectID
         SELECT  TG.id,TG.AccountingObjectCode,AccountingObjectName
         FROM    AccountingObject AS TG
                 LEFT JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,',') AS AccountingObjectID ON TG.ID = AccountingObjectID.Value
         WHERE  AccountingObjectID.Value IS NOT NULL

		DECLARE @tblSochitietcongnonhanvien TABLE 
		(
		AccountObjectID uniqueidentifier,
		AccountObjectCode NVARCHAR(25),
		AccountObjectName NVARCHAR(512),
		PostedDate Date,
		RefDate Date,
		RefNo NCHAR(25),
		JournalMemo NVARCHAR(512),
		AccountNumber NVARCHAR(25),
		CorrespondingAccountNumber NVARCHAR(25),
		DebitAmount decimal(25,0),
		CreditAmount decimal (25,0),
		ClosingDebitAmount decimal(25,0),
		ClosingCreditAmount decimal(25,0),
		RefType int,
		RefID uniqueidentifier
		)
			DECLARE @tblSochitietcongnonhanvien2 TABLE 
		(
		ID int,
		AccountObjectID uniqueidentifier,
		AccountObjectCode NVARCHAR(25),
		AccountObjectName NVARCHAR(512),
		PostedDate Date,
		RefDate Date,
		RefNo NCHAR(25),
		JournalMemo NVARCHAR(512),
		AccountNumber NVARCHAR(25),
		CorrespondingAccountNumber NVARCHAR(25),
		DebitAmount decimal(25,0),
		CreditAmount decimal (25,0),
		ClosingDebitAmount decimal(25,0),
		ClosingCreditAmount decimal(25,0),
		RefType int,
		RefID uniqueidentifier
		)

				DECLARE @tg TABLE 
		(
		AccountObjectID uniqueidentifier,
		AccountObjectCode NVARCHAR(25),
		AccountObjectName NVARCHAR(512),
		PostedDate Date,
		RefDate Date,
		RefNo NCHAR(25),
		JournalMemo NVARCHAR(512),
		AccountNumber NVARCHAR(25),
		CorrespondingAccountNumber NVARCHAR(25),
		DebitAmount decimal(25,0),
		CreditAmount decimal (25,0),
		ClosingDebitAmount decimal(25,0),
		ClosingCreditAmount decimal(25,0),
		ID int,
		RefType int,
		RefID uniqueidentifier
		)

		IF(@AccountNumber = 'all')
		BEGIN
		INSERT INTO @tblSochitietcongnonhanvien
		SELECT a.AccountingObjectID,b.AccountingObjectCode,b.AccountingObjectName as AccountingObjectName,null as PostedDate
		,null as RefDate,null as RefNo,N'Số dư đầu kỳ' as JournalMemo, Account as AccountNumber, null as CorrespondingAccountNumber, Sum(DebitAmount) as DebitAmount, Sum(CreditAmount) as CreditAmount,
		0 as ClosingDebitAmount,0 as ClosingCreditAmount,null,null
		FROM @tbDataGL a
		LEFT JOIN AccountingObject b ON a.AccountingObjectID = b.ID
		 where Account IN (SELECT AccountNumber From Account WHERE DetailType LIKE '%2%')
		and a.AccountingObjectID in (SELECT AccountObjectID from @tblListAccountingObjectID)  AND PostedDate <@FromDate
		group by a.AccountingObjectID,b.AccountingObjectName,b.AccountingObjectCode,Account,AccountCorresponding

		INSERT INTO @tblSochitietcongnonhanvien
		SELECT a.AccountingObjectID,b.AccountObjectCode, b.AccountObjectName, postedDate as PostedDate, Date as RefDate, No as RefNo, Reason as JournalMemo, Account as AccountNumber, AccountCorresponding as CorrespondingAccountNumber, DebitAmount as DebitAmount, CreditAmount as CreditAmount,
		null,null,a.TypeID,a.ReferenceID
		FROM @tbDataGL a
		LEFT JOIN @tblListAccountingObjectID b ON a.AccountingObjectID = b.AccountObjectID
		 where Account IN (SELECT AccountNumber From Account WHERE DetailType LIKE '%2%')
		and a.AccountingObjectID in (SELECT AccountObjectID from @tblListAccountingObjectID) AND PostedDate>=@FromDate AND PostedDate<=@ToDate
		INSERT INTO @tg
			Select  AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,DebitAmount,CreditAmount,ClosingDebitAmount,ClosingCreditAmount,ROW_NUMBER() OVER(Order by PostedDate) ID,RefType,RefID  from @tblSochitietcongnonhanvien
		END
		ELSE
		BEGIN
		INSERT INTO @tblSochitietcongnonhanvien
		SELECT a.AccountingObjectID,b.AccountingObjectCode,b.AccountingObjectName as AccountingObjectName,null as PostedDate
		,null as RefDate,null as RefNo,N'Số dư đầu kỳ' as JournalMemo, Account as AccountNumber,null   as CorrespondingAccountNumber, Sum(DebitAmount) as DebitAmount, Sum(CreditAmount) as CreditAmount,
		0 as ClosingDebitAmount,0 as ClosingCreditAmount,null,null
		FROM @tbDataGL a
		LEFT JOIN AccountingObject b ON a.AccountingObjectID = b.ID
		 where Account = @AccountNumber
		and a.AccountingObjectID in (SELECT AccountObjectID from @tblListAccountingObjectID)  AND PostedDate <@FromDate
		group by a.AccountingObjectID,b.AccountingObjectName,b.AccountingObjectCode,Account,AccountCorresponding

		INSERT INTO @tblSochitietcongnonhanvien
		SELECT a.AccountingObjectID,b.AccountObjectCode, b.AccountObjectName, postedDate as PostedDate, Date as RefDate, No as RefNo, Reason as JournalMemo, Account as AccountNumber, AccountCorresponding as CorrespondingAccountNumber, DebitAmount as DebitAmount, CreditAmount as CreditAmount,
		null,null,a.TypeID,a.ReferenceID
		FROM @tbDataGL a
		LEFT JOIN @tblListAccountingObjectID b ON a.AccountingObjectID = b.AccountObjectID
		 where Account = @AccountNumber
		and a.AccountingObjectID in (SELECT AccountObjectID from @tblListAccountingObjectID) AND PostedDate>=@FromDate AND PostedDate<=@ToDate
		INSERT INTO @tg
			Select  AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,DebitAmount,CreditAmount,ClosingDebitAmount,ClosingCreditAmount,ROW_NUMBER() OVER(Order by PostedDate) ID,RefType,RefID  from @tblSochitietcongnonhanvien
		END
		DECLARE @COUNT int,@temp int, @COUNT2 int, @AccountObjectCode NVARCHAR(25), @AccountObjectName NVARCHAR(512),@Sodu decimal(25,0), @DebitAmount decimal(25,0),@CreditAmount decimal(25,0),@i int
		DECLARE @tbltg TABLE
		(
		ID int,
		AccountObjectID uniqueidentifier,
		AccountObjectCode NVARCHAR(25),
		AccountObjectName NVARCHAR(512),
		PostedDate Date,
		RefDate Date,
		RefNo NCHAR(25),
		JournalMemo NVARCHAR(512),
		AccountNumber NVARCHAR(25),
		CorrespondingAccountNumber NVARCHAR(25),
		DebitAmount decimal(25,0),
		CreditAmount decimal (25,0),
		ClosingDebitAmount decimal(25,0),
		ClosingCreditAmount decimal (25,0),
		RefType int,
		RefID uniqueidentifier
		)
		
		Select Top 0 * into #temp from @tbltg
		SELECT DISTINCT(AccountObjectCode) into #tg2 FROM @tg
		SET @COUNT = (SELECT COUNT (*) from #tg2)
		SET @COUNT2 = 0
		WHILE(@COUNT >0)
		BEGIN
			SET @AccountObjectCode = (SELECT TOP 1 AccountObjectCode FROM @tg)
			SET @COUNT2 = (SELECT COUNT (*) FROM @tg WHERE AccountObjectCode = @AccountObjectCode)
		
			SET @temp = (SELECT COUNT (*) FROM @tg WHERE AccountObjectCode = @AccountObjectCode)
			INSERT INTO @tbltg
			SELECT ID,AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,DebitAmount,CreditAmount,null,null,RefType,RefID FROM @tg WHERE AccountObjectCode = @AccountObjectCode ORDER BY ID
			SET @Sodu = 0
			WHILE(@COUNT2 >0)
			BEGIN
				DECLARE @JournalMemo NVARCHAR(MAX)
				SET @i = (SELECT TOP 1 ID FROM @tbltg Order by ID)
				SET @DebitAmount = (SELECT TOP 1 (DebitAmount) FROM @tbltg Order by ID)
				SET @CreditAmount = (SELECT TOP 1 (CreditAmount) FROM  @tbltg Order by ID)
				SET @Sodu = @Sodu + @DebitAmount - @CreditAmount
				IF(@COUNT2=@temp)
				BEGIN
					IF((@DebitAmount - @CreditAmount)>0)
					BEGIN
					SET @JournalMemo =  (SELECT TOP 1 (JournalMemo) FROM  @tbltg Order by ID)
					IF(@JournalMemo=N'Số dư đầu kỳ')
					INSERT INTO #temp SELECT ID,AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,0,0,ABS(@Sodu),null,RefType,RefID FROM @tbltg WHERE ID = @i
					ELSE
					INSERT INTO #temp SELECT ID,AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,@DebitAmount,@CreditAmount,ABS(@Sodu),null,RefType,RefID FROM @tbltg WHERE ID = @i
					END
					ELSE
					BEGIN
					SET @JournalMemo =  (SELECT TOP 1 (JournalMemo) FROM  @tbltg Order by ID)
					IF(@JournalMemo=N'Số dư đầu kỳ')
					INSERT INTO #temp SELECT ID,AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,0,0,null,ABS(@Sodu),RefType,RefID FROM @tbltg WHERE ID = @i
					ELSE
					INSERT INTO #temp SELECT ID,AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,@DebitAmount,@CreditAmount,null,ABS(@Sodu),RefType,RefID FROM @tbltg WHERE ID = @i
					END
				END
					ELSE
					BEGIN
						IF(@Sodu>0)
						BEGIN
							INSERT INTO #temp SELECT ID,AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,DebitAmount,CreditAmount,ABS(@Sodu),null,RefType,RefID FROM @tbltg WHERE ID = @i
						END
					ELSE
					INSERT INTO #temp SELECT ID,AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,DebitAmount,CreditAmount,null,ABS(@Sodu),RefType,RefID FROM @tbltg WHERE ID = @i
					END
				DELETE FROM @tbltg WHERE ID = @i
				SET @COUNT2 = @COUNT2 -1
				END
			INSERT INTO @tblSochitietcongnonhanvien2 
			SELECT * from #temp
			DELETE FROM @tg WHERE AccountObjectCode = @AccountObjectCode 
			DELETE FROM #temp
			SET @COUNT = @COUNT - 1
		END

		DECLARE @tbltg2 TABLE
		(
		ID int,
		IDGroup int,
		AccountObjectID uniqueidentifier,
		AccountObjectCode NVARCHAR(25),
		AccountObjectName NVARCHAR(512),
		PostedDate Date,
		RefDate Date,
		RefNo NCHAR(25),
		JournalMemo NVARCHAR(512),
		AccountNumber NVARCHAR(25),
		CorrespondingAccountNumber NVARCHAR(25),
		DebitAmount decimal(25,0),
		CreditAmount decimal (25,0),
		ClosingDebitAmount decimal(25,0),
		ClosingCreditAmount decimal (25,0),
		RefType int,
		RefID uniqueidentifier
		)
		INSERT INTO @tbltg2
		SELECT ID,2,AccountObjectID,AccountObjectCode,AccountObjectName,PostedDate,RefDate,RefNo,JournalMemo,AccountNumber,CorrespondingAccountNumber,DebitAmount,CreditAmount,ClosingDebitAmount,ClosingCreditAmount,RefType,RefID 
		from @tblSochitietcongnonhanvien2
		SELECT * into #tg3 from @tbltg2
		SET @i = 0
		DECLARE @id int
		DECLARE @SumDebitAmount decimal(25,0), @SumCreditAmount decimal(25,0)
		DECLARE @PostedDate Datetime
		SET @COUNT = (SELECT COUNT(*) FROM #tg2)
		WHILE(@COUNT >0)
		BEGIN 
		SET @AccountObjectCode = (SELECT TOP 1 AccountObjectCode FROM @tblSochitietcongnonhanvien2 ORDER BY Id)
		SET @id = (SELECT MAX(ID) FROM #tg3 WHERE AccountObjectCode = @AccountObjectCode)
		print @id
		SET @SumDebitAmount = (SELECT Sum(DebitAmount) from #tg3 WHERE AccountObjectCode = @AccountObjectCode)
		SET @SumCreditAmount = (SELECT Sum(CreditAmount) from #tg3 WHERE AccountObjectCode = @AccountObjectCode)
		SET @PostedDate = (SELECT PostedDate from #tg3 WHERE ID = @id)
		INSERT INTO @tbltg2
		SELECT null,1,AccountObjectID,AccountObjectCode,AccountObjectName,null,null,null,null,null,null,@SumDebitAmount,@SumCreditAmount,ClosingDebitAmount,ClosingCreditAmount,null,null from @tblSochitietcongnonhanvien2 
		WHERE ID = @id 
		group by Id,AccountObjectID,AccountObjectCode,AccountObjectName,ClosingDebitAmount,ClosingCreditAmount
		DELETE FROM @tblSochitietcongnonhanvien2 WHERE AccountObjectCode = @AccountObjectCode
		SET @COUNT = @COUNT - 1

		END
		Select * From @tbltg2 Order by ID
		
	END
	
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*Edit by: cuongpv
*Edit Date: 07/05/2019
*Description: lay them du lieu tu cac bang PPService, PPDiscountReturn, SAReturn
*/
ALTER procedure [dbo].[Proc_SA_GetDetailPayS12] 
(@fromdate DATETIME,
 @todate DATETIME,
 @account nvarchar(255),
 @currency nvarchar(3),
 @AccountObjectID AS NVARCHAR(MAX)
)
as
begin
DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(15),
			  AccountGroupKind int
            )
INSERT  INTO @tblAccountNumber
                SELECT  AO.AccountNumber,
				        AO.AccountGroupKind
                FROM    Account AS AO
                        inner JOIN dbo.Func_ConvertStringIntoTable_Nvarchar(@account,
                                                              ',') AS TLAO ON AO.AccountNumber = TLAO.Value
DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
			  AccountObjectCode NVARCHAR(100),
              AccountObjectGroupListCode NVARCHAR(MAX) ,
              AccountObjectCategoryName NVARCHAR(MAX)
            ) 
             
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID ,
				        AO.AccountingObjectCode,
                        AO.AccountObjectGroupID ,
						null
                FROM    AccountingObject AS AO
                        inner JOIN dbo.Func_ConvertStringIntoTable(@AccountObjectID,
                                                              ',') AS TLAO ON AO.ID = TLAO.Value
                        

		/*Add by cuongpv de sua cach lam tron*/
		DECLARE @tbDataGL TABLE(
			ID uniqueidentifier,
			BranchID uniqueidentifier,
			ReferenceID uniqueidentifier,
			TypeID int,
			Date datetime,
			PostedDate datetime,
			No nvarchar(25),
			InvoiceDate datetime,
			InvoiceNo nvarchar(25),
			Account nvarchar(25),
			AccountCorresponding nvarchar(25),
			BankAccountDetailID uniqueidentifier,
			CurrencyID nvarchar(3),
			ExchangeRate decimal(25, 10),
			DebitAmount decimal(25,0),
			DebitAmountOriginal decimal(25,2),
			CreditAmount decimal(25,0),
			CreditAmountOriginal decimal(25,2),
			Reason nvarchar(512),
			Description nvarchar(512),
			VATDescription nvarchar(512),
			AccountingObjectID uniqueidentifier,
			EmployeeID uniqueidentifier,
			BudgetItemID uniqueidentifier,
			CostSetID uniqueidentifier,
			ContractID uniqueidentifier,
			StatisticsCodeID uniqueidentifier,
			InvoiceSeries nvarchar(25),
			ContactName nvarchar(512),
			DetailID uniqueidentifier,
			RefNo nvarchar(25),
			RefDate datetime,
			DepartmentID uniqueidentifier,
			ExpenseItemID uniqueidentifier,
			OrderPriority int,
			IsIrrationalCost bit
		)

		INSERT INTO @tbDataGL
		SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @todate
		/*end add by cuongpv*/

DECLARE @tblResult TABLE
            (
              RowNum INT IDENTITY(1, 1)
                         PRIMARY KEY ,
              AccountNumber NVARCHAR(20)  ,
			  AccountGroupKind int, 
              AccountObjectID UNIQUEIDENTIFIER ,
			  AccountObjectCode NVARCHAR(100)  ,
			  RefDate DATETIME , 
			  RefNo NVARCHAR(25)  ,
			  PostedDate DATETIME ,
			  JournalMemo NVARCHAR(512)  ,
			  CorrespondingAccountNumber NVARCHAR(20)  , 
			  DueDate DATETIME,
			  DebitAmount MONEY , 
              CreditAmount MONEY , 
			  ClosingDebitAmount MONEY , 
              ClosingCreditAmount MONEY,
			  OrderType int,
			  OrderPriority int, 
			  RefID UNIQUEIDENTIFIER,
			  RefType int
            )

if @account = '331'
Begin
	/*add by cuongpv tao bang du lieu DueDate tu cac bang*/
	DECLARE @tbDataDueDate TABLE(
		ID UNIQUEIDENTIFIER,
		IDDetail UNIQUEIDENTIFIER,
		DueDate DATETIME
	)
	/*end add by cuongpv*/
	/*add by cuongpv lay du lieu DueDate tu cac bang PPinvoice, PPservice, PPDiscountReturn*/
	INSERT INTO @tbDataDueDate
	SELECT Piv.ID, Pivdt.ID, Piv.DueDate FROM PPInvoiceDetail Pivdt LEFT JOIN PPInvoice Piv ON Piv.ID = Pivdt.PPInvoiceID
	WHERE Pivdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)
	
	UNION ALL

	SELECT Psv.ID, Psvdt.ID, Psv.DueDate FROM PPServiceDetail Psvdt LEFT JOIN PPService Psv ON Psv.ID = Psvdt.PPServiceID
	WHERE Psvdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)

	UNION ALL

	SELECT Pdcrt.ID, Pdcrtdt.ID, Pdcrt.DueDate FROM PPDiscountReturnDetail Pdcrtdt LEFT JOIN PPDiscountReturn Pdcrt ON Pdcrt.ID = Pdcrtdt.PPDiscountReturnID
	WHERE Pdcrtdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)
	/*end add by cuongpv*/
	insert into @tblResult
	SELECT  a.AccountNumber,
			A.AccountGroupKind,
			LAOI.AccountObjectID,
			LAOI.AccountObjectCode,
			null as NgayThangGS,
			null as Sohieu, 
			null as ngayCTu,
			N'Số dư đầu kỳ' as Diengiai ,
			null as TKDoiUng, 
			null as ThoiHanDuocCKhau, 
			$0 AS PSNO ,
			$0 AS PSCO ,
			CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
					THEN SUM(GL.DebitAmount - GL.CreditAmount)
					ELSE 0
			END AS DuNo ,
			CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
					THEN SUM(GL.CreditAmount - GL.DebitAmount)
					ELSE 0
			END AS DuCo,
			0 AS OrderType,
			null as OrderPriority,
			null as RefID,
			null as RefType
		FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
		INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber 
		INNER JOIN @tblListAccountObjectID AS LAOI ON GL.AccountingObjectID = LAOI.AccountObjectID
		WHERE   ( GL.PostedDate < @FromDate )  
		and GL.CurrencyID = @currency                    
		GROUP BY 
			A.AccountNumber,LAOI.AccountObjectID,A.AccountGroupKind,LAOI.AccountObjectCode
		HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
	UNION ALL
	select  an.AccountNumber,
			An.AccountGroupKind,
			LAOI.AccountObjectID,
			LAOI.AccountObjectCode,
			a.PostedDate as NgayThangGS, 
			a.No as Sohieu, 
			a.Date as ngayCTu,
			a.Description as Diengiai, 
			a.AccountCorresponding as TKDoiUng, 
			DueDate as ThoiHanDuocCKhau, 
			sum(a.DebitAmount) as PSNO, 
			sum(a.CreditAmount) as PSCO,
			sum(fnsd.DebitAmount) DuNo,
			sum(fnsd.CreditAmount) DuCo,
			1 AS OrderType,
			OrderPriority,
			a.ReferenceID as RefID,
			a.TypeID as RefType
	 from @tbDataGL a /*edit by cuongpv GeneralLedger -> @tbDataGL*/		
	LEFT JOIN @tbDataDueDate tbDuedate ON tbDuedate.IDDetail = a.DetailID
	inner join @tblAccountNumber as an on a.Account = an.AccountNumber
	INNER JOIN @tblListAccountObjectID AS LAOI ON a.AccountingObjectID = LAOI.AccountObjectID
	INNER JOIN Func_SoDu (
	   @fromdate
	  ,@todate
	  ,1) AS fnsd ON LAOI.AccountObjectID = fnsd.AccountObjectID and an.AccountNumber = fnsd.AccountNumber			
	where a.PostedDate between @fromdate and @todate
	and  a.CurrencyID = @currency  
	group by an.AccountNumber,
			LAOI.AccountObjectID,
			a.PostedDate,
			a.No , 
			a.Date ,
			a.Description, 
			a.AccountCorresponding, 
			DueDate,
			An.AccountGroupKind,
			LAOI.AccountObjectCode,
			OrderPriority,
			a.ReferenceID,
			a.TypeID
	order by AccountNumber,NgayThangGS,OrderPriority
End
else
Begin
	/*add by cuongpv tao bang du lieu DueDate tu cac bang*/
	DECLARE @tbDataDueDate1 TABLE(
		ID UNIQUEIDENTIFIER,
		IDDetail UNIQUEIDENTIFIER,
		DueDate DATETIME
	)
	/*end add by cuongpv*/
	/*add by cuongpv lay du lieu DueDate tu cac bang SAInvoice, SAReturn*/
	INSERT INTO @tbDataDueDate1
	SELECT SAiv.ID, SAivdt.ID, SAiv.DueDate FROM SAInvoiceDetail SAivdt LEFT JOIN SAInvoice SAiv ON SAiv.ID = SAivdt.SAInvoiceID
	WHERE SAivdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)
	
	UNION ALL

	SELECT SArt.ID, SArtdt.ID, SArt.DueDate FROM SAReturnDetail SArtdt LEFT JOIN SAReturn SArt ON SArt.ID = SArtdt.SAReturnID
	WHERE SArtdt.ID IN (Select DetailID From @tbDataGL Where PostedDate between @fromdate and @todate)
	/*end add by cuongpv*/
	insert into @tblResult
		SELECT  a.AccountNumber,
			A.AccountGroupKind,
			LAOI.AccountObjectID,
			LAOI.AccountObjectCode,
			null as NgayThangGS,
			null as Sohieu, 
			null as ngayCTu,
			N'Số dư đầu kỳ' as Diengiai ,
			null as TKDoiUng, 
			null as ThoiHanDuocCKhau, 
			$0 AS PSNO ,
			$0 AS PSCO ,
			CASE WHEN SUM(GL.DebitAmount - GL.CreditAmount) > 0
					THEN SUM(GL.DebitAmount - GL.CreditAmount)
					ELSE 0
			END AS DuNo ,
			CASE WHEN SUM(GL.CreditAmount - GL.DebitAmount) > 0
					THEN SUM(GL.CreditAmount - GL.DebitAmount)
					ELSE 0
			END AS DuCo,
			0 AS OrderType,
			null as OrderPriority,
			null as RefID,
			null as RefType
		FROM    @tbDataGL AS GL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
		INNER JOIN @tblAccountNumber A ON GL.Account = A.AccountNumber 
		INNER JOIN @tblListAccountObjectID AS LAOI ON GL.AccountingObjectID = LAOI.AccountObjectID
		WHERE   ( GL.PostedDate < @FromDate )  
		and GL.CurrencyID = @currency                    
		GROUP BY 
			A.AccountNumber,LAOI.AccountObjectID,A.AccountGroupKind,LAOI.AccountObjectCode
		HAVING  SUM(GL.DebitAmount - GL.CreditAmount) <> 0
	UNION ALL
		select  an.AccountNumber,
				An.AccountGroupKind,
				LAOI.AccountObjectID,
				LAOI.AccountObjectCode,
				a.PostedDate as NgayThangGS, 
				a.No as Sohieu, 
				a.Date as ngayCTu,
				a.Description as Diengiai, 
				a.AccountCorresponding as TKDoiUng, 
				DueDate as ThoiHanDuocCKhau, 
				sum(a.DebitAmount) as PSNO, 
				sum(a.CreditAmount) as PSCO,
				sum(fnsd.DebitAmount) DuNo,
				sum(fnsd.CreditAmount) DuCo,
				1 AS OrderType,
				OrderPriority,
				a.ReferenceID as RefID,
				a.TypeID as RefType
		 from @tbDataGL a	/*edit by cuongpv GeneralLedger -> @tbDataGL*/
		LEFT JOIN @tbDataDueDate1 tbDuedate ON tbDuedate.IDDetail = a.DetailID
		inner join @tblAccountNumber as an on a.Account = an.AccountNumber
		INNER JOIN @tblListAccountObjectID AS LAOI ON a.AccountingObjectID = LAOI.AccountObjectID
		INNER JOIN Func_SoDu (
		   @fromdate
		  ,@todate
		  ,1) AS fnsd ON LAOI.AccountObjectID = fnsd.AccountObjectID	and an.AccountNumber = fnsd.AccountNumber			
		where a.PostedDate between @fromdate and @todate
		and  a.CurrencyID = @currency  
		group by an.AccountNumber,
				LAOI.AccountObjectID,
				a.PostedDate,
				a.No , 
				a.Date ,
				a.Description, 
				a.AccountCorresponding, 
				DueDate,An.AccountGroupKind,LAOI.AccountObjectCode,OrderPriority, a.ReferenceID, a.TypeID
		order by AccountNumber,AccountObjectID,NgayThangGS ,OrderPriority 
End

DECLARE @CloseAmount AS Money ,
        @AccountObjectCode_tmp NVARCHAR(100) ,
        @AccountNumber_tmp NVARCHAR(20)
SELECT  @CloseAmount = 0 ,
        @AccountObjectCode_tmp = N'' ,
        @AccountNumber_tmp = N''
UPDATE  @tblResult
        SET     
                 @CloseAmount = ( CASE WHEN OrderType = 0 THEN ( CASE WHEN ClosingDebitAmount = 0 THEN ClosingCreditAmount
                                                                     ELSE -1 * ClosingDebitAmount
                                                                END )
                                      WHEN @AccountObjectCode_tmp <> AccountObjectCode or @AccountNumber_tmp <> AccountNumber THEN CreditAmount - DebitAmount
                                      ELSE @CloseAmount + CreditAmount - DebitAmount
                                 END ) ,
                ClosingDebitAmount = ( CASE WHEN AccountGroupKind = 0 THEN -1 * @CloseAmount
                                            WHEN AccountGroupKind = 1 THEN $0
                                            ELSE CASE WHEN @CloseAmount < 0 THEN -1 * @CloseAmount
                                                      ELSE $0
                                                 END
                                       END ) ,
                ClosingCreditAmount = ( CASE WHEN AccountGroupKind = 1 THEN @CloseAmount
                                             WHEN AccountGroupKind = 0 THEN $0
                                             ELSE CASE WHEN @CloseAmount > 0 THEN @CloseAmount
                                                       ELSE $0
                                                  END
                                        END ) ,
                @AccountObjectCode_tmp = AccountObjectCode ,
				@AccountNumber_tmp = AccountNumber							
select AccountNumber,
			AccountObjectID,
			RefDate as NgayThangGS, 
			RefNo as Sohieu, 
			PostedDate as ngayCTu,
			JournalMemo as Diengiai, 
			CorrespondingAccountNumber as TKDoiUng, 
			DueDate as ThoiHanDuocCKhau, 
			DebitAmount as PSNO, 
			CreditAmount as PSCO,
			ClosingDebitAmount as DuNo,
			ClosingCreditAmount as DuCo,
			RefID as RefID,
			RefType as RefType
from @tblResult 
order by RowNum
end				
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
ALTER PROCEDURE [dbo].[Proc_PO_SummaryPayable]
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @AccountNumber NVARCHAR(20) ,
    @AccountObjectID AS NVARCHAR(MAX) ,
    @CurrencyID NVARCHAR(3)
AS
    BEGIN
	    
		/*Add by cuongpv de sua cach lam tron*/
	DECLARE @tbDataGL TABLE(
		ID uniqueidentifier,
		BranchID uniqueidentifier,
		ReferenceID uniqueidentifier,
		TypeID int,
		Date datetime,
		PostedDate datetime,
		No nvarchar(25),
		InvoiceDate datetime,
		InvoiceNo nvarchar(25),
		Account nvarchar(25),
		AccountCorresponding nvarchar(25),
		BankAccountDetailID uniqueidentifier,
		CurrencyID nvarchar(3),
		ExchangeRate decimal(25, 10),
		DebitAmount decimal(25,0),
		DebitAmountOriginal decimal(25,2),
		CreditAmount decimal(25,0),
		CreditAmountOriginal decimal(25,2),
		Reason nvarchar(512),
		Description nvarchar(512),
		VATDescription nvarchar(512),
		AccountingObjectID uniqueidentifier,
		EmployeeID uniqueidentifier,
		BudgetItemID uniqueidentifier,
		CostSetID uniqueidentifier,
		ContractID uniqueidentifier,
		StatisticsCodeID uniqueidentifier,
		InvoiceSeries nvarchar(25),
		ContactName nvarchar(512),
		DetailID uniqueidentifier,
		RefNo nvarchar(25),
		RefDate datetime,
		DepartmentID uniqueidentifier,
		ExpenseItemID uniqueidentifier,
		OrderPriority int,
		IsIrrationalCost bit
	)

	INSERT INTO @tbDataGL
	SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= @ToDate
	/*end add by cuongpv*/
		
		DECLARE @tblListAccountObjectID TABLE 
            (
              AccountObjectID UNIQUEIDENTIFIER ,
              AccountObjectCode NVARCHAR(25) ,
              AccountObjectName NVARCHAR(512) ,
              AccountObjectAddress NVARCHAR(512) ,
              AccountObjectTaxCode NVARCHAR(200) ,
              AccountObjectGroupListCode NVARCHAR(MAX) 
                                                       NULL ,
              AccountObjectGroupListName NVARCHAR(MAX) 
                                                       NULL 
            ) 
        INSERT  INTO @tblListAccountObjectID
                SELECT  AO.ID as AccountObjectID ,
                        AO.AccountingObjectCode ,
                        AO.AccountingObjectName ,
                        AO.Address ,
                        AO.TaxCode ,
                        null AccountingObjectGroupCode ,
                        null AccountingObjectGroupName 
                FROM    dbo.Func_ConvertStringIntoTable(@AccountObjectID, ',') tblAccountObjectSelected
                        INNER JOIN dbo.AccountingObject AO ON AO.ID = tblAccountObjectSelected.Value		
      
                 
		
		DECLARE @StartDateOfPeriod DATETIME
		SET @StartDateOfPeriod = '1/1/'+CAST(Year(@FromDate) AS NVARCHAR(5))
                         
           
        DECLARE @tblAccountNumber TABLE
            (
              AccountNumber NVARCHAR(20) PRIMARY KEY ,
              AccountName NVARCHAR(512) ,
              AccountNumberPercent NVARCHAR(25) ,
              AccountCategoryKind INT
            )
        IF @AccountNumber IS NOT NULL 
            BEGIN
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = @AccountNumber
                        ORDER BY A.AccountNumber ,
                                A.AccountName 
                
            END
        ELSE 
            BEGIN 
                INSERT  INTO @tblAccountNumber
                        SELECT  A.AccountNumber ,
                                A.AccountName ,
                                A.AccountNumber + '%' ,
                                A.AccountGroupKind
                        FROM    dbo.Account AS A
                        WHERE   AccountNumber = '331'
                        ORDER BY A.AccountNumber ,
                                A.AccountName
            END
	    IF(@CurrencyID = 'VND')
		BEGIN
		 SELECT  ROW_NUMBER() OVER ( ORDER BY AccountObjectCode ) AS RowNum ,
                AccountingObjectID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountObjectAddress ,
                AccountObjectTaxCode ,
                AccountNumber ,
                AccountCategoryKind ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC)
                            - SUM(OpenningCreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) ) > 0
                                 THEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN ( SUM(OpenningDebitAmount - OpenningCreditAmount) )
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmount
                                            - OpenningCreditAmount) ) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount)
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmountOC
                                  - OpenningDebitAmountOC) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) ) > 0
                                 THEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmount - OpenningDebitAmount) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) ) > 0
                                 THEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmount ,
                SUM(DebitAmountOC) AS DebitAmountOC ,
                SUM(DebitAmount) AS DebitAmount ,
                SUM(CreditAmountOC) AS CreditAmountOC ,
                SUM(CreditAmount) AS CreditAmount ,
                SUM(AccumDebitAmountOC) AS AccumDebitAmountOC ,
                SUM(AccumDebitAmount) AS AccumDebitAmount ,
                SUM(AccumCreditAmountOC) AS AccumCreditAmountOC ,
                SUM(AccumCreditAmount) AS AccumCreditAmount ,
                /* Số dư cuối kỳ = Dư Có đầu kỳ - Dư Nợ đầu kỳ + Phát sinh Có – Phát sinh Nợ
				Nếu Số dư cuối kỳ >0 thì hiển bên cột Dư Có cuối kỳ 
				Nếu số dư cuối kỳ <0 thì hiển thị bên cột Dư Nợ cuối kỳ */
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC
                                + DebitAmountOC - CreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC) > 0
                                 THEN $0
                                 ELSE SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC)
                            END
                  END ) AS CloseDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC
                                + CreditAmountOC - DebitAmountOC)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC
                                            + CreditAmountOC - DebitAmountOC) ) > 0
                                 THEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                                + DebitAmount - CreditAmount)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount) > 0 THEN $0
                                 ELSE SUM(OpenningDebitAmount
                                          - OpenningCreditAmount + DebitAmount
                                          - CreditAmount)
                            END
                  END ) AS ClosingDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                                + CreditAmount - DebitAmount)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount
                                            + CreditAmount - DebitAmount) ) > 0
                                 THEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)
                                 ELSE $0
                            END
                  END ) AS ClosingCreditAmount ,
                AccountObjectGroupListCode ,
                AccountObjectGroupListName
        FROM    ( SELECT    AOL.AccountingObjectID ,
                            LAOI.AccountObjectCode ,
                            LAOI.AccountObjectName ,
                            LAOI.AccountObjectAddress,
                            LAOI.AccountObjectTaxCode ,
                            TBAN.AccountNumber ,
                            TBAN.AccountCategoryKind ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmount
                                 ELSE $0
                            END AS OpenningDebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmount
                                 ELSE $0
                            END AS OpenningCreditAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmountOriginal
                            END AS DebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmount
                            END AS DebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmountOriginal
                            END AS CreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmount
                            END AS CreditAmount ,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmountOriginal ELSE 0 END AS AccumDebitAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmount ELSE 0 END AS AccumDebitAmount,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmountOriginal ELSE 0 END AS AccumCreditAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmount ELSE 0 END AS AccumCreditAmount,
                            LAOI.AccountObjectGroupListCode ,
                            LAOI.AccountObjectGroupListName
                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                  WHERE     AOL.PostedDate <= @ToDate
                           
                ) AS RSNS
        GROUP BY RSNS.AccountingObjectID ,
                RSNS.AccountObjectCode ,
                RSNS.AccountObjectName ,
                RSNS.AccountObjectAddress ,
                RSNS.AccountObjectTaxCode ,
                RSNS.AccountNumber ,
                RSNS.AccountCategoryKind ,
                RSNS.AccountObjectGroupListCode ,
                RSNS.AccountObjectGroupListName 
        HAVING 
				SUM(DebitAmountOC) <> 0
                OR SUM(DebitAmount) <> 0
                OR SUM(CreditAmountOC) <> 0
                OR SUM(CreditAmount) <> 0
                OR SUM(OpenningDebitAmount - OpenningCreditAmount) <> 0 
                OR SUM(OpenningDebitAmountOC - OpenningCreditAmountOC) <> 0 
				OR ( 
					(SUM(AccumDebitAmountOC) <> 0
					OR SUM(AccumDebitAmount) <> 0
					OR SUM(AccumCreditAmountOC) <> 0
					OR SUM(AccumCreditAmount) <> 0) )
        ORDER BY RSNS.AccountObjectCode
        OPTION (RECOMPILE)
		END
		ELSE
		BEGIN
        SELECT  ROW_NUMBER() OVER ( ORDER BY AccountObjectCode ) AS RowNum ,
                AccountingObjectID ,
                AccountObjectCode ,
                AccountObjectName ,
                AccountObjectAddress ,
                AccountObjectTaxCode ,
                AccountNumber ,
                AccountCategoryKind ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC)
                            - SUM(OpenningCreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) ) > 0
                                 THEN ( SUM(OpenningDebitAmountOC
                                            - OpenningCreditAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN ( SUM(OpenningDebitAmount - OpenningCreditAmount) )
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningDebitAmount
                                            - OpenningCreditAmount) ) > 0
                                 THEN SUM(OpenningDebitAmount
                                          - OpenningCreditAmount)
                                 ELSE $0
                            END
                  END ) AS OpeningDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmountOC
                                  - OpenningDebitAmountOC) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) ) > 0
                                 THEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN ( SUM(OpenningCreditAmount - OpenningDebitAmount) )
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) ) > 0
                                 THEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount) )
                                 ELSE $0
                            END
                  END ) AS OpeningCreditAmount ,
                SUM(DebitAmountOC) AS DebitAmountOC ,
                SUM(DebitAmount) AS DebitAmount ,
                SUM(CreditAmountOC) AS CreditAmountOC ,
                SUM(CreditAmount) AS CreditAmount ,
                SUM(AccumDebitAmountOC) AS AccumDebitAmountOC ,
                SUM(AccumDebitAmount) AS AccumDebitAmount ,
                SUM(AccumCreditAmountOC) AS AccumCreditAmountOC ,
                SUM(AccumCreditAmount) AS AccumCreditAmount ,
                /* Số dư cuối kỳ = Dư Có đầu kỳ - Dư Nợ đầu kỳ + Phát sinh Có – Phát sinh Nợ
				Nếu Số dư cuối kỳ >0 thì hiển bên cột Dư Có cuối kỳ 
				Nếu số dư cuối kỳ <0 thì hiển thị bên cột Dư Nợ cuối kỳ */
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmountOC - OpenningCreditAmountOC
                                + DebitAmountOC - CreditAmountOC)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC) > 0
                                 THEN $0
                                 ELSE SUM(OpenningDebitAmountOC
                                          - OpenningCreditAmountOC
                                          + DebitAmountOC - CreditAmountOC)
                            END
                  END ) AS CloseDebitAmountOC ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmountOC - OpenningDebitAmountOC
                                + CreditAmountOC - DebitAmountOC)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmountOC
                                            - OpenningDebitAmountOC
                                            + CreditAmountOC - DebitAmountOC) ) > 0
                                 THEN SUM(OpenningCreditAmountOC
                                          - OpenningDebitAmountOC
                                          + CreditAmountOC - DebitAmountOC)
                                 ELSE $0
                            END
                  END ) AS CloseCreditAmountOC ,
                ( CASE WHEN AccountCategoryKind = 0
                       THEN SUM(OpenningDebitAmount - OpenningCreditAmount
                                + DebitAmount - CreditAmount)
                       WHEN AccountCategoryKind = 1 THEN $0
                       ELSE CASE WHEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount) > 0 THEN $0
                                 ELSE SUM(OpenningDebitAmount
                                          - OpenningCreditAmount + DebitAmount
                                          - CreditAmount)
                            END
                  END ) AS ClosingDebitAmount ,
                ( CASE WHEN AccountCategoryKind = 1
                       THEN SUM(OpenningCreditAmount - OpenningDebitAmount
                                + CreditAmount - DebitAmount)
                       WHEN AccountCategoryKind = 0 THEN $0
                       ELSE CASE WHEN ( SUM(OpenningCreditAmount
                                            - OpenningDebitAmount
                                            + CreditAmount - DebitAmount) ) > 0
                                 THEN SUM(OpenningCreditAmount
                                          - OpenningDebitAmount + CreditAmount
                                          - DebitAmount)
                                 ELSE $0
                            END
                  END ) AS ClosingCreditAmount ,
                AccountObjectGroupListCode ,
                AccountObjectGroupListName
        FROM    ( SELECT    AOL.AccountingObjectID ,
                            LAOI.AccountObjectCode ,
                            LAOI.AccountObjectName ,
                            LAOI.AccountObjectAddress,
                            LAOI.AccountObjectTaxCode ,
                            TBAN.AccountNumber ,
                            TBAN.AccountCategoryKind ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.DebitAmountOriginal
                                 ELSE $0
                            END AS OpenningDebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate
                                 THEN AOL.CreditAmountOriginal
                                 ELSE $0
                            END AS OpenningCreditAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmountOriginal
                            END AS DebitAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.DebitAmountOriginal
                            END AS DebitAmount ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmountOriginal
                            END AS CreditAmountOC ,
                            CASE WHEN AOL.PostedDate < @FromDate THEN $0
                                 ELSE AOL.CreditAmountOriginal
                            END AS CreditAmount ,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmountOriginal ELSE 0 END AS AccumDebitAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.DebitAmount ELSE 0 END AS AccumDebitAmount,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmountOriginal ELSE 0 END AS AccumCreditAmountOC,
							CASE WHEN PostedDate BETWEEN @StartDateOfPeriod AND @ToDate THEN AOL.CreditAmount ELSE 0 END AS AccumCreditAmount,
                            LAOI.AccountObjectGroupListCode ,
                            LAOI.AccountObjectGroupListName
                  FROM      @tbDataGL AS AOL /*edit by cuongpv dbo.GeneralLedger -> @tbDataGL*/
                            INNER JOIN @tblAccountNumber TBAN ON AOL.Account LIKE TBAN.AccountNumberPercent
                            INNER JOIN dbo.Account AS AN ON AOL.Account = AN.AccountNumber
                            INNER JOIN @tblListAccountObjectID AS LAOI ON AOL.AccountingObjectID = LAOI.AccountObjectID
                  WHERE     AOL.PostedDate <= @ToDate
                            AND ( @CurrencyID IS NULL
                                  OR AOL.CurrencyID = @CurrencyID
                                )
                ) AS RSNS
        GROUP BY RSNS.AccountingObjectID ,
                RSNS.AccountObjectCode ,
                RSNS.AccountObjectName ,
                RSNS.AccountObjectAddress ,
                RSNS.AccountObjectTaxCode ,
                RSNS.AccountNumber ,
                RSNS.AccountCategoryKind ,
                RSNS.AccountObjectGroupListCode ,
                RSNS.AccountObjectGroupListName 
        HAVING 
				SUM(DebitAmountOC) <> 0
                OR SUM(DebitAmount) <> 0
                OR SUM(CreditAmountOC) <> 0
                OR SUM(CreditAmount) <> 0
                OR SUM(OpenningDebitAmount - OpenningCreditAmount) <> 0 
                OR SUM(OpenningDebitAmountOC - OpenningCreditAmountOC) <> 0 
				OR ( 
					(SUM(AccumDebitAmountOC) <> 0
					OR SUM(AccumDebitAmount) <> 0
					OR SUM(AccumCreditAmountOC) <> 0
					OR SUM(AccumCreditAmount) <> 0) )
        ORDER BY RSNS.AccountObjectCode
        OPTION (RECOMPILE)
		END
		
    END

GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

GO
/*
*/
ALTER PROCEDURE [dbo].[Proc_SA_SoTheoDoiThanhToanBangNgoaiTe]
    @FromDate DATETIME,
    @ToDate DATETIME,
    @TypeMoney NVARCHAR(3),
    @Account NVARCHAR(25),
    @AccountObjectID AS NVARCHAR(MAX)
AS
BEGIN          
	DECLARE @sqlexc nvarchar(max)
	set @sqlexc = N''
	/*Add by cuongpv de sua cach lam tron*/
	select @sqlexc = @sqlexc + N' DECLARE @tbDataGL TABLE('
	select @sqlexc = @sqlexc + N' 	ID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	BranchID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	ReferenceID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	TypeID int,'
	select @sqlexc = @sqlexc + N' 	Date datetime,'
	select @sqlexc = @sqlexc + N' 	PostedDate datetime,'
	select @sqlexc = @sqlexc + N' 	No nvarchar(25),'
	select @sqlexc = @sqlexc + N' 	InvoiceDate datetime,'
	select @sqlexc = @sqlexc + N' 	InvoiceNo nvarchar(25),'
	select @sqlexc = @sqlexc + N' 	Account nvarchar(25),'
	select @sqlexc = @sqlexc + N' 	AccountCorresponding nvarchar(25),'
	select @sqlexc = @sqlexc + N' 	BankAccountDetailID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	CurrencyID nvarchar(3),'
	select @sqlexc = @sqlexc + N' 	ExchangeRate decimal(25, 10),'
	select @sqlexc = @sqlexc + N' 	DebitAmount decimal(25,0),'
	select @sqlexc = @sqlexc + N' 	DebitAmountOriginal decimal(25,2),'
	select @sqlexc = @sqlexc + N' 	CreditAmount decimal(25,0),'
	select @sqlexc = @sqlexc + N' 	CreditAmountOriginal decimal(25,2),'
	select @sqlexc = @sqlexc + N' 	Reason nvarchar(512),'
	select @sqlexc = @sqlexc + N' 	Description nvarchar(512),'
	select @sqlexc = @sqlexc + N' 	VATDescription nvarchar(512),'
	select @sqlexc = @sqlexc + N' 	AccountingObjectID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	EmployeeID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	BudgetItemID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	CostSetID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	ContractID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	StatisticsCodeID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	InvoiceSeries nvarchar(25),'
	select @sqlexc = @sqlexc + N' 	ContactName nvarchar(512),'
	select @sqlexc = @sqlexc + N' 	DetailID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	RefNo nvarchar(25),'
	select @sqlexc = @sqlexc + N' 	RefDate datetime,'
	select @sqlexc = @sqlexc + N' 	DepartmentID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	ExpenseItemID uniqueidentifier,'
	select @sqlexc = @sqlexc + N' 	OrderPriority int,'
	select @sqlexc = @sqlexc + N' 	IsIrrationalCost bit'
	select @sqlexc = @sqlexc + N' )'

	select @sqlexc = @sqlexc + N' INSERT INTO @tbDataGL'
	select @sqlexc = @sqlexc + N' SELECT GL.* FROM dbo.GeneralLedger GL WHERE GL.PostedDate <= '''+CONVERT(nvarchar(25),@ToDate,101)+''''
		/*end add by cuongpv*/
	set @sqlexc = @sqlexc + N' DECLARE @tbAccountObjectID TABLE('
	set @sqlexc = @sqlexc + N'	AccountingObjectID UNIQUEIDENTIFIER'
	set @sqlexc = @sqlexc + N')'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbAccountObjectID'
	set @sqlexc = @sqlexc + N' SELECT tblAccOjectSelect.Value as AccountingObjectID'
	set @sqlexc = @sqlexc + N'	FROM dbo.Func_ConvertStringIntoTable('''+@AccountObjectID+''','','') tblAccOjectSelect'
	set @sqlexc = @sqlexc + N'	WHERE tblAccOjectSelect.Value in (SELECT ID FROM AccountingObject)'
	
	select @sqlexc = @sqlexc + N' DECLARE @tbDataDauKy TABLE('
	select @sqlexc = @sqlexc + N' AccountingObjectID UNIQUEIDENTIFIER,'
	select @sqlexc = @sqlexc + N' Account nvarchar(25),'
	select @sqlexc = @sqlexc + N' SoDuDauKySoTien money,'
	select @sqlexc = @sqlexc + N' SoDuDauKyQuyDoi money'
	select @sqlexc = @sqlexc + N')'
	
	select @sqlexc = @sqlexc + N' INSERT INTO @tbDataDauKy(AccountingObjectID, Account, SoDuDauKySoTien, SoDuDauKyQuyDoi)'
	select @sqlexc = @sqlexc + N' SELECT GL.AccountingObjectID, GL.Account, SUM(GL.DebitAmountOriginal - GL.CreditAmountOriginal) as SoDuDauKySoTien,'
	select @sqlexc = @sqlexc + N' SUM(GL.DebitAmount - GL.CreditAmount) as SoDuDauKyQuyDoi'
	select @sqlexc = @sqlexc + N' FROM @tbDataGL GL'
	select @sqlexc = @sqlexc + N' WHERE (GL.AccountingObjectID in (Select AccountingObjectID from @tbAccountObjectID))'
	select @sqlexc = @sqlexc + N' AND GL.PostedDate < '''+CONVERT(nvarchar(25),@FromDate,101)+''' AND (GL.CurrencyID = '''+@TypeMoney+''')'
	
	if(@Account='all')
	begin
		set @sqlexc = @sqlexc + N' AND ((GL.Account like ''131%'') OR (GL.Account like ''141%'') OR (GL.Account like ''331%''))'
	end
	else
	begin
		set @sqlexc = @sqlexc + N' AND (GL.Account like '''+@Account+'%'')'
	end
	
	select @sqlexc = @sqlexc + N' GROUP BY GL.AccountingObjectID, GL.Account'
	
	
	set @sqlexc = @sqlexc + N' DECLARE @tbDataReturn TABLE('
	set @sqlexc = @sqlexc + N'	stt bigint,'
	set @sqlexc = @sqlexc + N'	IdGroup smallint,'
	set @sqlexc = @sqlexc + N'	AccountingObjectID UNIQUEIDENTIFIER,'
	set @sqlexc = @sqlexc + N'	AccountingObjectName nvarchar(512),'
	set @sqlexc = @sqlexc + N'	Account nvarchar(25),'
	set @sqlexc = @sqlexc + N'	NgayHoachToan Date,'
	set @sqlexc = @sqlexc + N'	NgayChungTu Date,'
	set @sqlexc = @sqlexc + N'	SoChungTu nvarchar(25),'
	set @sqlexc = @sqlexc + N'	DienGiai nvarchar(512),'
	set @sqlexc = @sqlexc + N'	TKDoiUng nvarchar(512),'
	set @sqlexc = @sqlexc + N'	TyGiaHoiDoai decimal(25,10),'
	set @sqlexc = @sqlexc + N'	PSNSoTien money,'
	set @sqlexc = @sqlexc + N'	PSNQuyDoi money,'
	set @sqlexc = @sqlexc + N'	PSCSoTien money,'
	set @sqlexc = @sqlexc + N'	PSCQuyDoi money,'
	set @sqlexc = @sqlexc + N'	DuNoSoTien money,'
	set @sqlexc = @sqlexc + N'	DuNoQuyDoi money,'
	set @sqlexc = @sqlexc + N'	DuCoSoTien money,'
	set @sqlexc = @sqlexc + N'	DuCoQuyDoi money,'
	set @sqlexc = @sqlexc + N'	TonSoTien money,'
	set @sqlexc = @sqlexc + N'	TonQuyDoi money,'
	set @sqlexc = @sqlexc + N'	OrderPriority int,'
	set @sqlexc = @sqlexc + N'	RefID UNIQUEIDENTIFIER,'
	set @sqlexc = @sqlexc + N'	RefType int'
	set @sqlexc = @sqlexc + N')'
	
	set @sqlexc = @sqlexc + N' DECLARE @IdGroup1 smallint'
	set @sqlexc = @sqlexc + N' set @IdGroup1 = 5;'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbDataReturn(stt,IdGroup, AccountingObjectID, Account, NgayHoachToan, NgayChungTu, SoChungTu, DienGiai, TKDoiUng, TyGiaHoiDoai,'
	set @sqlexc = @sqlexc + N' PSNSoTien, PSNQuyDoi, PSCSoTien, PSCQuyDoi,OrderPriority,RefID,RefType)'
	set @sqlexc = @sqlexc + N' SELECT ROW_NUMBER() OVER(ORDER BY GL.AccountingObjectID, GL.Account, GL.PostedDate) as stt, @IdGroup1 as IdGroup, GL.AccountingObjectID, Gl.Account, GL.PostedDate as NgayHoachToan, GL.Date as NgayChungTu, GL.No as SoChungTu,'
	set @sqlexc = @sqlexc + N' GL.Description as DienGiai, GL.AccountCorresponding as TKDoiUng, GL.ExchangeRate as TyGiaHoiDoai, GL.DebitAmountOriginal as PSNSoTien,'
	set @sqlexc = @sqlexc + N' GL.DebitAmount as PSNQuyDoi, Gl.CreditAmountOriginal as PSCSoTien, GL.CreditAmount as PSCQuyDoi,GL.OrderPriority as OrderPriority,GL.ReferenceID as RefID, GL.TypeID as RefType'
	set @sqlexc = @sqlexc + N' FROM @tbDataGL GL'
	set @sqlexc = @sqlexc + N' WHERE (GL.PostedDate Between '''+CONVERT(nvarchar(25),@FromDate,101)+''' And '''+CONVERT(nvarchar(25),@ToDate,101)+''') AND (GL.CurrencyID = '''+@TypeMoney+''')'
	set @sqlexc = @sqlexc + N' AND (GL.AccountingObjectID in (Select AccountingObjectID from @tbAccountObjectID))'
	
	if(@Account='all')
	begin
		set @sqlexc = @sqlexc + N' AND ((GL.Account like ''131%'') OR (GL.Account like ''141%'') OR (GL.Account like ''331%''))'
	end
	else
	begin
		set @sqlexc = @sqlexc + N' AND (GL.Account like '''+@Account+'%'')'
	end
	
	set @sqlexc = @sqlexc + N' DECLARE @stt1 smallint'
	set @sqlexc = @sqlexc + N' set @stt1 = 0;'
	
	
	set @sqlexc = @sqlexc + N' Declare @SoDuDauKySoTien money'
	set @sqlexc = @sqlexc + N' Set @SoDuDauKySoTien = 0'
	set @sqlexc = @sqlexc + N' Declare @SoDuDauKyQuyDoi money'
	set @sqlexc = @sqlexc + N' Set @SoDuDauKyQuyDoi = 0'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbDataReturn(stt,IdGroup, AccountingObjectID, Account, DienGiai, TonSoTien, TonQuyDoi)'
	set @sqlexc = @sqlexc + N' SELECT @stt1, @IdGroup1, AccountingObjectID, Account, N''Số dư đầu kỳ'''
	set @sqlexc = @sqlexc + N' ,@SoDuDauKySoTien, @SoDuDauKyQuyDoi'
	set @sqlexc = @sqlexc + N' FROM @tbDataReturn'
	set @sqlexc = @sqlexc + N' GROUP BY AccountingObjectID, Account'
	
	set @sqlexc = @sqlexc + N' DECLARE @iIDDauKy UNIQUEIDENTIFIER'
	set @sqlexc = @sqlexc + N' DECLARE @iAccountDauKy NVARCHAR(25)'
	set @sqlexc = @sqlexc + N' DECLARE @iTonSoTienDauKy money'
	set @sqlexc = @sqlexc + N' DECLARE @iTonQuyDoiDauKy money'
	
	set @sqlexc = @sqlexc + N' DECLARE cursorDauKy CURSOR FOR'
	set @sqlexc = @sqlexc + N' SELECT AccountingObjectID, Account, SoDuDauKySoTien, SoDuDauKyQuyDoi FROM @tbDataDauKy'
	set @sqlexc = @sqlexc + N' OPEN cursorDauKy'
	set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorDauKy INTO @iIDDauKy, @iAccountDauKy, @iTonSoTienDauKy, @iTonQuyDoiDauKy'
	set @sqlexc = @sqlexc + N' WHILE @@FETCH_STATUS = 0'
	set @sqlexc = @sqlexc + N' BEGIN'
		set @sqlexc = @sqlexc + N' Declare @countCheck int'
		set @sqlexc = @sqlexc + N' set @countCheck = (select count(AccountingObjectID) from @tbDataReturn where AccountingObjectID = @iIDDauKy and Account = @iAccountDauKy)'
		
		set @sqlexc = @sqlexc + N' If(@countCheck > 0)'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' Update @tbDataReturn Set TonSoTien = @iTonSoTienDauKy, TonQuyDoi = @iTonQuyDoiDauKy' 
			set @sqlexc = @sqlexc + N' where AccountingObjectID = @iIDDauKy and Account = @iAccountDauKy'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' Else'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' Insert Into @tbDataReturn(stt,IdGroup, AccountingObjectID, Account, DienGiai, TonSoTien, TonQuyDoi)'
			set @sqlexc = @sqlexc + N' Values(@stt1, @IdGroup1, @iIDDauKy, @iAccountDauKy, N''Số dư đầu kỳ'', @iTonSoTienDauKy, @iTonQuyDoiDauKy)'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorDauKy INTO @iIDDauKy, @iAccountDauKy, @iTonSoTienDauKy, @iTonQuyDoiDauKy'
	set @sqlexc = @sqlexc + N' END'
	set @sqlexc = @sqlexc + N' CLOSE cursorDauKy'
	set @sqlexc = @sqlexc + N' DEALLOCATE cursorDauKy'
	
	
	set @sqlexc = @sqlexc + N' DECLARE @iID UNIQUEIDENTIFIER'
	set @sqlexc = @sqlexc + N' DECLARE @iAccount NVARCHAR(25)'
	set @sqlexc = @sqlexc + N' DECLARE @iTonSoTien money'
	set @sqlexc = @sqlexc + N' DECLARE @iTonQuyDoi money'
	
	set @sqlexc = @sqlexc + N' DECLARE cursorTon CURSOR FOR'
	set @sqlexc = @sqlexc + N' SELECT AccountingObjectID, Account, TonSoTien, TonQuyDoi FROM @tbDataReturn WHERE stt = 0'
	set @sqlexc = @sqlexc + N' OPEN cursorTon'
	set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorTon INTO @iID, @iAccount, @iTonSoTien, @iTonQuyDoi'
	set @sqlexc = @sqlexc + N' WHILE @@FETCH_STATUS = 0'
	set @sqlexc = @sqlexc + N' BEGIN'
		set @sqlexc = @sqlexc + N' Declare @tonSoTien money'
		set @sqlexc = @sqlexc + N' set @tonSoTien = ISNULL(@iTonSoTien,0)'
		set @sqlexc = @sqlexc + N' Declare @tonQuyDoi money'
		set @sqlexc = @sqlexc + N' set @tonQuyDoi = ISNULL(@iTonQuyDoi,0)'
		
		set @sqlexc = @sqlexc + N' Declare @minStt bigint'
		set @sqlexc = @sqlexc + N' set @minStt = (select MIN(stt) from @tbDataReturn where AccountingObjectID = @iID and Account = @iAccount and stt > 0)'
		set @sqlexc = @sqlexc + N' Declare @maxStt bigint'
		set @sqlexc = @sqlexc + N' set @maxStt = (select MAX(stt) from @tbDataReturn where AccountingObjectID = @iID and Account = @iAccount and stt > 0)'
		set @sqlexc = @sqlexc + N' WHILE @minStt <= @maxStt'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' set @tonSoTien = @tonSoTien + ISNULL((select PSNSoTien from @tbDataReturn where stt = @minStt),0) - ISNULL((select PSCSoTien from @tbDataReturn where stt = @minStt),0)'
			set @sqlexc = @sqlexc + N' set @tonQuyDoi = @tonQuyDoi + ISNULL((select PSNQuyDoi from @tbDataReturn where stt = @minStt),0) - ISNULL((select PSCQuyDoi from @tbDataReturn where stt = @minStt),0)'
			set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET TonSoTien = @tonSoTien, TonQuyDoi = @tonQuyDoi WHERE stt = @minStt'
			set @sqlexc = @sqlexc + N' set @minStt = @minStt + 1'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorTon INTO @iID, @iAccount, @iTonSoTien, @iTonQuyDoi'
	set @sqlexc = @sqlexc + N' END'
	set @sqlexc = @sqlexc + N' CLOSE cursorTon'
	set @sqlexc = @sqlexc + N' DEALLOCATE cursorTon'
	
	set @sqlexc = @sqlexc + N' DECLARE @IdGroup3 smallint'
	set @sqlexc = @sqlexc + N' SET @IdGroup3 = 3'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbDataReturn(IdGroup, AccountingObjectID, Account, PSNSoTien, PSNQuyDoi, PSCSoTien, PSCQuyDoi)'
	set @sqlexc = @sqlexc + N' SELECT @IdGroup3 as IdGroup, AccountingObjectID, Account, SUM(PSNSoTien) as PSNSoTien, SUM(PSNQuyDoi) as PSNQuyDoi'
	set @sqlexc = @sqlexc + N' , SUM(PSCSoTien) as PSCSoTien, SUM(PSCQuyDoi) as PSCQuyDoi'
	set @sqlexc = @sqlexc + N' FROM @tbDataReturn WHERE IdGroup = 5'
	set @sqlexc = @sqlexc + N' GROUP BY AccountingObjectID, Account'
	
	set @sqlexc = @sqlexc + N' DECLARE @iID1 UNIQUEIDENTIFIER'
	set @sqlexc = @sqlexc + N' DECLARE @iAccount1 NVARCHAR(25)'
	
	set @sqlexc = @sqlexc + N' DECLARE cursorTon1 CURSOR FOR'
	set @sqlexc = @sqlexc + N' SELECT AccountingObjectID, Account FROM @tbDataReturn WHERE stt = 0'
	set @sqlexc = @sqlexc + N' OPEN cursorTon1'
	set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorTon1 INTO @iID1, @iAccount1'
	set @sqlexc = @sqlexc + N' WHILE @@FETCH_STATUS = 0'
	set @sqlexc = @sqlexc + N' BEGIN'
		set @sqlexc = @sqlexc + N' Declare @maxStt1 bigint'
		set @sqlexc = @sqlexc + N' set @maxStt1 = (select MAX(stt) from @tbDataReturn where AccountingObjectID = @iID1 and Account = @iAccount1 and stt > 0)'
		set @sqlexc = @sqlexc + N' If(@maxStt1 > 0)'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET TonSoTien = (select TonSoTien from @tbDataReturn where stt = @maxStt1)'
			set @sqlexc = @sqlexc + N' ,TonQuyDoi = (select TonQuyDoi from @tbDataReturn where stt = @maxStt1) WHERE AccountingObjectID = @iID1'
			set @sqlexc = @sqlexc + N' AND Account = @iAccount1 AND IdGroup = 3'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' Else'
		set @sqlexc = @sqlexc + N' Begin'
			set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET TonSoTien = (select TonSoTien from @tbDataReturn where stt = 0 and AccountingObjectID = @iID1 and Account = @iAccount1)'
			set @sqlexc = @sqlexc + N' ,TonQuyDoi = (select TonQuyDoi from @tbDataReturn where stt = 0 and AccountingObjectID = @iID1 and Account = @iAccount1) WHERE AccountingObjectID = @iID1'
			set @sqlexc = @sqlexc + N' AND Account = @iAccount1 AND IdGroup = 3'
		set @sqlexc = @sqlexc + N' End'
		set @sqlexc = @sqlexc + N' FETCH NEXT FROM cursorTon1 INTO @iID1, @iAccount1'
	set @sqlexc = @sqlexc + N' END'
	set @sqlexc = @sqlexc + N' CLOSE cursorTon1'
	set @sqlexc = @sqlexc + N' DEALLOCATE cursorTon1'
	
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuNoSoTien = TonSoTien WHERE TonSoTien > 0'
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuCoSoTien = TonSoTien WHERE TonSoTien < 0'
	
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuNoQuyDoi = TonQuyDoi WHERE TonQuyDoi > 0'
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuCoQuyDoi = TonQuyDoi WHERE TonQuyDoi < 0'
	
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuCoSoTien = (-1 * DuCoSoTien) WHERE DuCoSoTien < 0'
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET DuCoQuyDoi = (-1 * DuCoQuyDoi) WHERE DuCoQuyDoi < 0'
	
	set @sqlexc = @sqlexc + N' DECLARE @IdGroup2 smallint'
	set @sqlexc = @sqlexc + N' SET @IdGroup2 = 1'
	
	set @sqlexc = @sqlexc + N' INSERT INTO @tbDataReturn(IdGroup, AccountingObjectID, PSNSoTien, PSNQuyDoi, PSCSoTien, PSCQuyDoi'
	set @sqlexc = @sqlexc + N' , DuNoSoTien, DuNoQuyDoi, DuCoSoTien, DuCoQuyDoi)'
	set @sqlexc = @sqlexc + N' SELECT @IdGroup2 as IdGroup, AccountingObjectID, SUM(PSNSoTien) as PSNSoTien, SUM(PSNQuyDoi) as PSNQuyDoi'
	set @sqlexc = @sqlexc + N' , SUM(PSCSoTien) as PSCSoTien, SUM(PSCQuyDoi) as PSCQuyDoi, SUM(DuNoSoTien) as DuNoSoTien'
	set @sqlexc = @sqlexc + N' , SUM(DuNoQuyDoi) as DuNoQuyDoi, SUM(DuCoSoTien) as DuCoSoTien, SUM(DuCoQuyDoi) as DuCoQuyDoi'
	set @sqlexc = @sqlexc + N' FROM @tbDataReturn WHERE IdGroup = 3'
	set @sqlexc = @sqlexc + N' GROUP BY AccountingObjectID'
	
	set @sqlexc = @sqlexc + N' UPDATE @tbDataReturn SET AccountingObjectName = AJ.AccountingObjectName'
	set @sqlexc = @sqlexc + N' FROM ( select ID,AccountingObjectName from AccountingObject) AJ'
	set @sqlexc = @sqlexc + N' where AccountingObjectID = AJ.ID'
	
	set @sqlexc = @sqlexc + N' SELECT stt,IdGroup, AccountingObjectID, AccountingObjectName, Account, NgayHoachToan, NgayChungTu, SoChungTu, DienGiai, TKDoiUng, TyGiaHoiDoai'
	set @sqlexc = @sqlexc + N',PSNSoTien, PSNQuyDoi, PSCSoTien, PSCQuyDoi, DuNoSoTien, DuNoQuyDoi, DuCoSoTien, DuCoQuyDoi,RefID,RefType'
	set @sqlexc = @sqlexc + N' FROM @tbDataReturn'
	set @sqlexc = @sqlexc + N' WHERE (ISNULL(PSNSoTien,0) > 0) OR (ISNULL(PSNQuyDoi,0) > 0) OR (ISNULL(PSCSoTien,0) > 0) OR (ISNULL(PSCQuyDoi,0) > 0)'
	set @sqlexc = @sqlexc + N' OR (ISNULL(DuNoSoTien,0) > 0) OR (ISNULL(DuNoQuyDoi,0) > 0) OR (ISNULL(DuCoSoTien,0) > 0) OR (ISNULL(DuCoQuyDoi,0) > 0)'
	set @sqlexc = @sqlexc + N' ORDER BY AccountingObjectID, OrderPriority, Account, IdGroup, stt'
	
	
	EXECUTE sp_executesql @sqlexc
END
GO
IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
GO

ALTER TABLE dbo.TemplateColumn
  DROP CONSTRAINT FK_TemplateColumn_TemplateDetail
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.Account SET DetailType = N'11' WHERE ID = 'df6c256f-3d79-42c3-9658-8cee022bdbeb'
UPDATE dbo.Account SET DetailType = N'1' WHERE ID = '89d80be7-29fb-49c7-be72-bb7ce995d577'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.AccountDefault SET FilterAccount = N'133' WHERE ID = 'eca5f9c4-2c23-4287-8c81-cc6529878864'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DELETE dbo.Bank WHERE ID = '80515148-e26b-435d-9cbc-cdf10c5deb21'

UPDATE dbo.Bank SET Address = N'' WHERE ID = '095ddb71-5b3b-4fde-b3d9-397b85fe8c88'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '1e45aab7-c0c0-4991-a6d0-192b14fa609e'
UPDATE dbo.TemplateColumn SET ColumnWidth = 250 WHERE ID = 'd09a7a67-3498-43d6-87bc-29f824411061'
UPDATE dbo.TemplateColumn SET ColumnWidth = 250 WHERE ID = '9972a1fe-3ba5-47af-b0ab-5ebe4693b1ac'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '151947c3-6a45-465e-ad4a-7e2c02df26ac'
UPDATE dbo.TemplateColumn SET IsReadOnly = 0 WHERE ID = '57d33bda-190c-4006-a744-ab1d2db27ee1'
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 99
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 101
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 103
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 104
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 106
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0, ListTypeID = N'120,121,122,123,124,125,128' WHERE ID = 107
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0, ListTypeID = N'120,121,122,123,124,125,128' WHERE ID = 108
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 109
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 110
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 111
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 112
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 116
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 117
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 118
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 119
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 120
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 122
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 124
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 125
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 126
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 127
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 128
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 129
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 52 WHERE ID = 133
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 50 WHERE ID = 134
UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'131,141,127' WHERE ID = 140
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 32 WHERE ID = 142
UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'133,143,126' WHERE ID = 147
UPDATE dbo.VoucherPatternsReport SET ListTypeID = N'133,143,173,126' WHERE ID = 148
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 33 WHERE ID = 149
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 42 WHERE ID = 155
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 42 WHERE ID = 156
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 42 WHERE ID = 157
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 22 WHERE ID = 158
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 24 WHERE ID = 162
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 176
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 431 WHERE ID = 179
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 434 WHERE ID = 183
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 430 WHERE ID = 184
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 90 WHERE ID = 185
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 90 WHERE ID = 186
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 193
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 194
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 54 WHERE ID = 207
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 236
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 237
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 282
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 283
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 284
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 285
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 286
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 287
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 289
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 290
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 0 WHERE ID = 291
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 10 WHERE ID = 292
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 11 WHERE ID = 293
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 10 WHERE ID = 294
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 11 WHERE ID = 295
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 10 WHERE ID = 296
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 11 WHERE ID = 297
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 60 WHERE ID = 298
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 40 WHERE ID = 299
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 41 WHERE ID = 300
UPDATE dbo.VoucherPatternsReport SET TypeGroup = 43 WHERE ID = 301

GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
SET IDENTITY_INSERT dbo.VoucherPatternsReport ON
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (304, 0, 12, N'UNC-Voucher', N'Chứng từ kế toán', N'UNC-Voucher.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (305, 0, 0, N'UNC_Agribank1', N'Ủy nhiệm chi ngân hàng Agribank', N'MBTellerPaper_UNC_Agribank1.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (306, 0, 0, N'UNC_BIDV1', N'Ủy nhiệm chi ngân hàng BIDV', N'MBTellerPaper_UNC_BIDV1.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (307, 0, 0, N'UNC_Techcombank1', N'Ủy nhiệm chi ngân hàng Techcombank', N'MBTellerPaper_UNC_Techcombank1.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (308, 0, 0, N'UNC_Vietinbank1', N'Ủy nhiệm chi ngân hàng Vietinbank', N'MBTellerPaper_UNC_Vietinbank1.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (309, 0, 0, N'UNC_Vietcombank1', N'Ủy nhiệm chi ngân hàng Vietcombank', N'MBTellerPaper_UNC_Vietcombank1.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (310, 0, 12, N'UNC-GBN', N'Giấy báo nợ', N'UNC-GBN.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (311, 0, 13, N'SEC-Voucher', N'Chứng từ kế toán', N'SEC-Voucher.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (312, 0, 13, N'SEC-GBN', N'Giấy báo nợ', N'SEC-GBN.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (313, 0, 16, N'MBDeposit_Voucher', N'Chứng từ kế toán', N'MBDeposit_Voucher.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (314, 0, 16, N'MBDeposit_GBC', N'Giấy báo có', N'MBDeposit_GBC.rst', 1, NULL)
INSERT dbo.VoucherPatternsReport(ID, TypeID, TypeGroup, VoucherPatternsCode, VoucherPatternsName, FilePath, IsStartup, ListTypeID) VALUES (315, 0, 323, N'SAQuote-BH-CT1', N'Chứng từ kế toán', N'SAQuote-BH-CT1.rst', 1, N'')
GO
SET IDENTITY_INSERT dbo.VoucherPatternsReport OFF
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

DBCC CHECKIDENT('dbo.VoucherPatternsReport', RESEED, 316)
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

ALTER TABLE dbo.TemplateColumn WITH NOCHECK
  ADD CONSTRAINT FK_TemplateColumn_TemplateDetail FOREIGN KEY (TemplateDetailID) REFERENCES dbo.TemplateDetail(ID) ON DELETE CASCADE ON UPDATE NO ACTION
GO

IF @@ERROR<>0 OR @@TRANCOUNT=0 BEGIN IF @@TRANCOUNT>0 ROLLBACK SET NOEXEC ON END

IF @@TRANCOUNT>0 COMMIT TRANSACTION
GO

SET NOEXEC OFF