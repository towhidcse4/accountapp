﻿namespace Accounting
{
    partial class FLoading<T>
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uProgressBar = new Infragistics.Win.UltraWinProgressBar.UltraProgressBar();
            this.bgwLoadData = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // uProgressBar
            // 
            this.uProgressBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uProgressBar.Location = new System.Drawing.Point(0, 0);
            this.uProgressBar.Name = "uProgressBar";
            this.uProgressBar.Size = new System.Drawing.Size(368, 23);
            this.uProgressBar.TabIndex = 0;
            this.uProgressBar.Text = "[Formatted]";
            this.uProgressBar.UseWaitCursor = true;
            // 
            // bgwLoadData
            // 
            this.bgwLoadData.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwLoadData_DoWork);
            this.bgwLoadData.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwLoadData_ProgressChanged);
            this.bgwLoadData.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwLoadData_RunWorkerCompleted);
            // 
            // FLoading
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(368, 23);
            this.ControlBox = false;
            this.Controls.Add(this.uProgressBar);
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FLoading";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đang tải dữ liệu...";
            this.TopMost = true;
            this.UseWaitCursor = true;
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinProgressBar.UltraProgressBar uProgressBar;
        private System.ComponentModel.BackgroundWorker bgwLoadData;
    }
}