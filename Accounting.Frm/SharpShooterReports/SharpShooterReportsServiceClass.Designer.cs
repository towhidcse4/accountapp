using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel.Activation;
using System.Data;
using PerpetuumSoft.Reporting.WebViewer.Server;

namespace Accounting.SharpShooterReports
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public partial class SharpShooterReportsServiceClass : ReportServiceBase
    {
        private DataTable customers;
        private DataColumn dataColumn1;
        private DataColumn dataColumn2;
        private PerpetuumSoft.Reporting.Components.ReportManager reportManager1;
        private System.ComponentModel.IContainer components;
        private PerpetuumSoft.Reporting.Components.InlineReportSlot inlineReportSlot1;
        private DataSet dataSet1;
    
        public SharpShooterReportsServiceClass()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {            
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SharpShooterReportsServiceClass));
            this.dataSet1 = new System.Data.DataSet();
            this.customers = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.reportManager1 = new PerpetuumSoft.Reporting.Components.ReportManager(this.components);
            this.inlineReportSlot1 = new PerpetuumSoft.Reporting.Components.InlineReportSlot(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.customers)).BeginInit();
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Tables.AddRange(new System.Data.DataTable[] {
            this.customers});
            // 
            // customers
            // 
            this.customers.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2});
            this.customers.TableName = "Customers";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "Name";
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "Phone";
            // 
            // reportManager1
            // 
            this.reportManager1.DataSources = new PerpetuumSoft.Reporting.Components.ObjectPointerCollection(new string[] {
            "Customers"}, new object[] {
            ((object)(this.customers))});
            this.reportManager1.Reports.AddRange(new PerpetuumSoft.Reporting.Components.ReportSlot[] {
            this.inlineReportSlot1});
            // 
            // inlineReportSlot1
            // 
            this.inlineReportSlot1.DocumentStream = resources.GetString("inlineReportSlot1.DocumentStream");
            this.inlineReportSlot1.ReportName = "CustomersReport";
            // 
            // ServiceClass
            // 
            this.ReportManager = this.reportManager1;
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.customers)).EndInit();

}

        protected override void OnLoadData(IDictionary<string, object> parameters, string reportName, PerpetuumSoft.Reporting.Components.ReportSlot reportSlot)
        {
            base.OnLoadData(parameters, reportName, reportSlot);

            DataRow row = customers.NewRow();
            row["Name"] = "Johnson Leslie";
            row["Phone"] = "613-442-7654";
            customers.Rows.Add(row);
            row = customers.NewRow();
            row["Name"] = "Fisher Pete";
            row["Phone"] = "401-609-7623";
            customers.Rows.Add(row);
            row = customers.NewRow();
            row["Name"] = "Brown Kelly";
            row["Phone"] = "803-438-2771";
            customers.Rows.Add(row);

        }
    }
}