﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Accounting.Core.Domain;
using Accounting.Core.IService;
using FX.Core;

namespace Accounting
{
    public partial class TreeTest : Form
    {
        private readonly IRegistrationGroupService _IRegistrationGroupService;
        Infragistics.Win.UltraWinTree.UltraTreeNode ParentNode = new Infragistics.Win.UltraWinTree.UltraTreeNode();
        public TreeTest()
        {
            _IRegistrationGroupService = IoC.Resolve<IRegistrationGroupService>();
            InitializeComponent();
            
            ultraTree1.Nodes.Add(ParentNode);
            LoadSubFolders(null, ParentNode);
        }

        private void LoadSubFolders(RegistrationGroup Parent, Infragistics.Win.UltraWinTree.UltraTreeNode ParentNode)
        {
            
            if (Parent != null)
            {
                string ParentName = Parent.RegistrationGroupName;
                string ParentKey = Parent.ID.ToString();

                ParentNode = ParentNode.Nodes.Add(ParentKey, ParentName);
                //ultraTree1.Nodes.Add(ParentNode); 
            }

            if (Parent == null)
            {
                foreach (RegistrationGroup SubFolder in _IRegistrationGroupService.Query.Where(a => a.ParentID == null).OrderBy(a=>a.OrderFixCode))
                {
                    try
                    {
                        LoadSubFolders(SubFolder, ParentNode);
                    }
                    catch (Exception)
                    { }
                }
            }
            else
            {
                foreach (RegistrationGroup SubFolder in _IRegistrationGroupService.Query.Where(a => a.ParentID == Parent.ID).OrderBy(a => a.OrderFixCode))
                {
                    try
                    {
                        LoadSubFolders(SubFolder, ParentNode);
                    }
                    catch (Exception)
                    { }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new FRegistrationGroup().Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new TreeTest().Show();
        }

        //private void LoadSubFolders(System.IO.DirectoryInfo ParentFolder, Infragistics.Win.UltraWinTree.UltraTreeNode ParentNode)
        //{
        //    string ParentFolderName = ParentFolder.Name;
        //    string ParentFolderKey = ParentFolder.FullName;

        //    if (ParentNode == null)
        //    {
        //        ParentNode = ultraTree1.Nodes.Add(ParentFolderKey, ParentFolderName);
        //    }
        //    else
        //    {
        //        ParentNode = ParentNode.Nodes.Add(ParentFolderKey, ParentFolderName);
        //        if (ParentNode.Level >= DrillDownLevels) return;
        //    }

        //    foreach (System.IO.DirectoryInfo SubFolder in ParentFolder.GetDirectories())
        //    {
        //        try
        //        {
        //            LoadSubFolders(SubFolder, ParentNode);
        //        }
        //        catch (Exception)
        //        { }
        //    }
        //}

    }
}
