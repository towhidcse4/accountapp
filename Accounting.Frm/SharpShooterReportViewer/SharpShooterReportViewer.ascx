<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SharpShooterReportViewer.ascx.cs" Inherits="Accounting.SharpShooterReportViewer.SharpShooterReportViewer" %>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />    
    <link href="../Styles/SharpShooterReports/WebViewerSkin.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/SharpShooterReports/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/SharpShooterReports/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="../Scripts/SharpShooterReports/jquery-ui-1.10.4.js" type="text/javascript" defer></script>
    <script src="../Scripts/SharpShooterReports/jquery.treeview.js" type="text/javascript" defer></script>
    <script src="../Scripts/SharpShooterReports/mscorlib.js" type="text/javascript" defer></script>
    <script src="../Scripts/SharpShooterReports/json2.js" type="text/javascript" defer></script>
    <script src="../Scripts/SharpShooterReports/PerpetuumSoft.Reporting.WebViewer.Client.Model.js" type="text/javascript" defer></script>
    <script src="../Scripts/SharpShooterReports/PerpetuumSoft.Reporting.WebViewer.Client.js" type="text/javascript" defer></script>
    <script src="../Scripts/SharpShooterReports/WebViewerSkin.js" type="text/javascript" defer></script>

    <script type="text/javascript">
        var reportViewer = null;

        $(document).ready(function () {
            var reportViewer = ReportViewerConstructor.Create("#reportViewer");
            var hostName = window.location.host;
            reportViewer.setServiceUrl("http://" + hostName + "/SharpShooterReports/SharpShooterReportsWcfReportService.svc");
            reportViewer.reportName = "CustomersReport";
            reportViewer.renderDocument();
        });
    </script>

    <div id="reportViewer">
    </div>